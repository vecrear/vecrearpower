package com.fidz.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum TravelType {
	MOVEMENT,IDLE
}
