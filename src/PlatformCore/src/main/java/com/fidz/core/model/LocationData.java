package com.fidz.core.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.core.model.TravelType;

import lombok.Data;

@Data
@Document(collection = "gpsdata")
@JsonIgnoreProperties(value = { "target" })
public class LocationData {
	
	@NotNull
	private String deviceID;
	
	private Object gpsData;
	
	@NotNull
	private String schemaName;
	
	private Double distance;
	
	private TravelType travelType;
	
	public LocationData() {
		
	}

	public LocationData(@NotNull String deviceID, Object gpsData, @NotNull String schemaName, Double distance,
			TravelType travelType) {
		super();
		this.deviceID = deviceID;
		this.gpsData = gpsData;
		this.schemaName = schemaName;
		this.distance = distance;
		this.travelType = travelType;
	}

	@Override
	public String toString() {
		return "LocationData [deviceID=" + deviceID + ", gpsData=" + gpsData + ", schemaName=" + schemaName
				+ ", distance=" + distance + ", travelType=" + travelType + "]";
	}

}

