/*
 * package com.fidz.core.business;
 * 
 * import java.io.BufferedReader; import java.io.InputStreamReader; import
 * java.net.URL; import java.net.URLConnection; import
 * java.text.SimpleDateFormat; import java.util.Date; import java.util.List;
 * 
 * import org.slf4j.Logger; import org.slf4j.LoggerFactory;
 * 
 * public class DistanceCalculation {
 * 
 * private static final Logger logger =
 * LoggerFactory.getLogger(DistanceCalculation.class); private static final
 * String uriprefix = "https://maps.googleapis.com/maps/api/directions/json";
 * private static int distanceExtCount = 0; public static SimpleDateFormat
 * dateFormatdd = new SimpleDateFormat("dd-MM-yyyy"); private static String
 * currentDate = dateFormatdd.format(new Date());
 * 
 * public Double getDistance(List<Double> listlat, List<Double> listlong, String
 * travelMode) { logger.info("Distance.java :: getDistance -- " + listlat);
 * logger.info("latitude :: " + listlat + ", longitude :: " + listlong); String
 * uri = null; int i, j = 0; boolean gotDistance = false; double d = 0.0; j =
 * listlat.size() - 1; for (i = 0; i < listlat.size() - 1; i++) {
 * 
 * if (listlat.get(j - 1) == listlat.get(j) && listlong.get(j - 1) ==
 * listlong.get(j)) { logger.
 * info("in getDistance: Distance lat long are same so distance calculated is 0 :\t "
 * ); return 0.0; }
 * 
 * if (listlat.get(j - 1) == 0.0 || listlat.get(j) == 0.0 || listlong.get(j - 1)
 * == 0.0 || listlong.get(j) == 0) { logger.
 * info("in getDistance: Distance lat long contains 0.0 so distance calculated is 0 :\t "
 * ); return 0.0; }
 * 
 * uri = uriprefix + "?origin=" + listlat.get(j - 1) + "," + listlong.get(j - 1)
 * + "&destination=" + listlat.get(j) + "," + listlong.get(j) + "&mode=" +
 * travelMode + "&sensor=false&key=AIzaSyCgxnHRjkQkbyXOuuUjFl7NsXMRD56Xurg";
 * 
 * logger.info("distance calculation uri :: " + uri); try { URL uriAddress = new
 * URL(uri); URLConnection res = uriAddress.openConnection(); BufferedReader in
 * = new BufferedReader(new InputStreamReader(res.getInputStream())); String
 * inputLine = null; while ((inputLine = in.readLine()) != null) { if
 * (inputLine.indexOf("distance") != -1) { inputLine = in.readLine(); inputLine
 * = in.readLine(); String distanceValue = "\"value\" : "; int beginIndex =
 * inputLine.indexOf(distanceValue) + distanceValue.length(); String distPoints
 * = inputLine.substring(beginIndex); d = Double.parseDouble(distPoints); d = d
 * / 1000; gotDistance = true; break; } } if (!gotDistance) { logger.
 * info("Distance.jav :: getDistance() -- distance din't got distance from google API"
 * );
 * 
 * distance.add(0.0); d=-1.0;
 * 
 * d = getDistanceBetweenTwoPoints(listlat.get(j - 1), listlong.get(j - 1),
 * listlat.get(j), listlong.get(j));
 * logger.info("Distance calculated from the algorithm :\t " + d); } else {
 * double algoDistance = getDistanceBetweenTwoPoints(listlat.get(j - 1),
 * listlong.get(j - 1), listlat.get(j), listlong.get(j)); if (algoDistance > d)
 * { logger.info(
 * "Algo distance calculated is more than the google distnace so return algo distance "
 * ); d = algoDistance; } } String tempDate = dateFormatdd.format(new Date());
 * if (!tempDate.equalsIgnoreCase(currentDate)) { currentDate = tempDate;
 * distanceExtCount = 0; }
 * 
 * logger.info("Call to Google to get Address: count on the day :: " +
 * ++distanceExtCount + " on date " + new Date()); } catch (Exception ex) {
 * logger.
 * error("Distance.jav :: getDistance() -- Google API fail to calculate distance :: "
 * , ex); d = getDistanceBetweenTwoPoints(listlat.get(j - 1), listlong.get(j -
 * 1), listlat.get(j), listlong.get(j));
 * logger.info("Distance calculated from the algorithm :\t " + d); return d; }
 * j--; } logger.
 * info("Distance.jav :: getDistance() -- Distance responded back to the caller is:\t:: "
 * + d); return d; }
 * 
 * public Double getDistanceBetweenTwoPoints(Double latitude1, Double
 * longitude1, Double latitude2,Double longitude2) { final int RADIUS_EARTH =
 * 6373; double dLat = getRad(latitude2 - latitude1); double dLong =
 * getRad(longitude2 - longitude1); if (dLat != 0.0 && dLong != 0.0 && latitude1
 * != 0.0 && longitude1 != 0.0) { double a = Math.sin(dLat / 2) * Math.sin(dLat
 * / 2) + Math.cos(getRad(latitude1)) Math.cos(getRad(latitude2)) *
 * Math.sin(dLong / 2) * Math.sin(dLong / 2); double c = 2 *
 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); return (RADIUS_EARTH * c); } else
 * { return (RADIUS_EARTH * 0.0); } }
 * 
 * private Double getRad(Double x) { return x * Math.PI / 180; }
 * 
 * public boolean isSameLocation(Double firstLat, Double firstLng, Double
 * secondLat, Double secondLng) { logger.info("firstLat :: "+firstLat);
 * logger.info("firstLng :: "+firstLng); logger.info("secondLat :: "+secondLat);
 * logger.info("secondLng :: "+secondLng); return (isNearBy(firstLat, secondLat)
 * && isNearBy(firstLng, secondLng)); }
 * 
 * private boolean isNearBy(Double first, Double second) { double diff; if
 * (first >= second) { diff = first - second; } else { diff = second - first; }
 * return !(diff > 0.0009); }
 * 
 * }
 */