package com.fidz.core.comp;

import java.io.IOException;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fidz.base.service.DeviceService;
import com.fidz.mongodb.MongoDataStore;
import com.fidz.services.location.service.GpsDataService;

@Configuration
@PropertySource(value="classpath:webproperty.properties",ignoreResourceNotFound=true)
public class MqttRecevier {

	private static final Logger logger = LoggerFactory.getLogger(MqttRecevier.class);

	
	
	@Autowired
	GpsDataService gpsDataService;
	
	@Autowired
	DeviceService deviceService;
	
	 Object objectResponse = null;

	@Value("${platform.mongo.protocol}")
	private String protocol;
	
	@Value("${platform.mongo.host}")
    private String host;
	
    @Value("${platform.mongo.port}")
    private int port;
    
    @Value("${platform.server.port}")
    private int serverPort;
    
    @Value("${platform.mqtt.host}")
    private String mqttHost;
    
    @Value("${platform.mqtt.port}")
    private int mqttPort;

    public MqttRecevier (@Value("${platform.mongo.port}") int port,
            @Value("${platform.mongo.protocol}") String protocol,
            @Value("${platform.mongo.host}") String host,
            @Value("${platform.mongo.port}") int serverPort) {
    	
    		this.protocol = protocol;
    		this.host = host;
    		this.port = port;
    		this.serverPort = serverPort;
	}
	public MqttRecevier () {
		
	}
	
	public String getProtocol() {
        return protocol;
    }
 
    public String getHost() {
        return host;
    }
 
    public int getPort() {
        return port;
    }
    
    public int getserverPort() {
        return serverPort;
    }
	 
    @Autowired
	public void MqttRecevier() {
    
    	logger.info("ServerPort is----  :: "+serverPort);
    	logger.info("MqttHost is ----   :: "+mqttHost);
    	logger.info("MqttPort is----    :: "+mqttPort);

		
		MqttClient client = null;

		try {
		//client = new MqttClient("tcp://13.234.172.250:2181", MqttClient.generateClientId(), new MemoryPersistence());
			logger.info("tcp://"+mqttHost+":"+mqttPort+"");
			client = new MqttClient("tcp://"+mqttHost+":"+mqttPort+"",MqttClient.generateClientId());  //192.168.1.70:1883
			MqttConnectOptions options = new MqttConnectOptions();
			//options.setAutomaticReconnect(true);
			options.setCleanSession(true);
			client.connect(options);
			logger.info("Check is Mqtt Connected---"+client.isConnected());
			// client.setCallback(this);
			logger.info("Check is started subscribing-------"+client.getServerURI());
			client.subscribe("madhuletstrack");
			client.setCallback(new MqttCallback() {
				@Override
				public void connectionLost(Throwable throwable) {
					logger.info("Connection lost to MQTT Broker");
				}

				@Override
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					
					try {
							Thread one = new Thread() {
						    public void run() {
						        try {
						        	logger.info("| -------------------Received---------//mqtt device location payload Madhu Valegowda");
									logger.info("| Topic:   {}", topic);
									logger.info("| Message: {}", new String(message.getPayload()));
									logger.info("| dataaaa" + new String(message.getPayload()));
									logger.info("| QoS:     {}", message.getQos());
									logger.info("----Processing Request---------------------------------");
									try {
										objectResponse = new MongoDataStore().insertGpsData(message.getPayload(),host,port,protocol,serverPort,gpsDataService,deviceService);
										//logger.info(objectResponse.toString());
									} catch (JsonParseException e) {
										logger.info(e.toString());
									} catch (JsonMappingException e) {
										logger.info(e.toString());
										e.printStackTrace();
									} catch (IOException e) {
										logger.info(e.toString());
										e.printStackTrace();
									}				
									Thread.sleep(15);
						        } catch(InterruptedException v) {
						        	logger.info(v.toString());
						            System.out.println(v);
	
						        }
						    }  
						};

						one.start();
						
		
		
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
					if(iMqttDeliveryToken != null) {
						logger.info("------------------Delivery Complete------------------------------"+iMqttDeliveryToken.toString());	
					}
				
				}
			});
		} catch (MqttException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
}
