package com.fidz.core;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableAutoConfiguration
@EnableReactiveMongoRepositories
@ComponentScan(  basePackages = {"com.fidz.core","com.fidz.base","com.fidz.services.location"})
@EnableMongoRepositories
@SpringBootApplication
public class PlatformCoreApplication {
	private static final Logger logger = LoggerFactory.getLogger(PlatformCoreApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(PlatformCoreApplication.class, args);
	}
}
