package com.fidz.core.exception;

public class LocationDataNotFoundException extends RuntimeException{
	

    /**
	 * 
	 */
	private static final long serialVersionUID = -946118179731022497L;

	public LocationDataNotFoundException (String msg) {
           super("Location Data not found with id/name "+msg);
    }

}
