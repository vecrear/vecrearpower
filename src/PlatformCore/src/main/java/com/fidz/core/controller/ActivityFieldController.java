//package com.fidz.core.controller;
//
//import java.net.UnknownHostException;
//import java.util.List;
//import java.util.Map;
//
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@CrossOrigin(maxAge = 3600)
//@RestController("FEAActivityFieldController")
//@RequestMapping(value="/activityfields")
//public class ActivityFieldController {/*
//	@Autowired
//	private LocationDataService  activityFieldService;
//	
//	@PostMapping("/get/all")
//	public List<ActivityField> getAllActivityFields(@RequestHeader(value="schemaName") String schemaName) throws UnknownHostException{
//		return activityFieldService.getAllActivityFields(schemaName);
//	}
//	@PostMapping
//	public ActivityField createActivityField(@Valid @RequestBody ActivityField activity){
//		return activityFieldService.createActivityField(activity);
//	}
//	@PostMapping("/creates")
//	public void createActivityFields(@Valid @RequestBody ActivityField activity){
//		activityFieldService.createActivityFields(activity);
//	}
//	@PostMapping("/get/id/{id}")
//	public ResponseEntity<ActivityField> getActivityFieldById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
//		return activityFieldService.getActivityFieldById(id, schemaName);
//		
//	}
//	@GetMapping("/activityname/{name}")
//    public Mono<Activity> getActivityByName(@PathVariable(value = "name") String name) {
//		return activityService.getActivityByName(name);
//    }
//	
//	@PostMapping("/update/id/{id}")
//    public ActivityField updateActivityField(@PathVariable(value = "id") String id,
//                                                   @Valid @RequestBody ActivityField activity) {
//        return activityFieldService.updateActivityField(id, activity);
//    }
//
//
//	@PostMapping("/delete/id/{id}")
//    public Map<String, String> deleteActivityField(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
//        return activityFieldService.deleteActivityField(id, schemaName);
//    }
//    @PostMapping("/delete/soft/id/{id}")
//    public Map<String, String> softDeleteActivityField(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
//        return activityFieldService.deleteSoftActivityField(id, schemaName, timeUpdate);
//    }
//    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
//    public List<ActivityField> streamAllActivities() {
//        return activityFieldService.streamAllActivityFields();
//    }
//
//    // Exception Handling Examples
//    @ExceptionHandler
//    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
//        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Activity Field with the same details already exists"));
//    }
//
//    @ExceptionHandler
//    public ResponseEntity<?> handleNotFoundException(ActivityFieldNotFoundException ex) {
//        return ResponseEntity.notFound().build();
//    }
//	
//   
//	
//*/}
