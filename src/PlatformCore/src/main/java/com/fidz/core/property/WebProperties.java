package com.fidz.core.property;

public class WebProperties {
	
    private String protocol;
 
    private String serverHost;
 
    private int serverPort;
    public String getProtocol() {
        return protocol;
    }
 
    public String getServerHost() {
        return serverHost;
    }
 
    public int getServerPort() {
        return serverPort;
    }
    
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
 
    public void setHost(String host) {
    	this.serverHost = host;
    }
 
    public void setServerPort(int port) {
    	this.serverPort= port;
    }
   
}