package com.fidz.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum ObjectActionType {
	create,
	getById,
	updateById,
	deleteById,
	getAll
}
