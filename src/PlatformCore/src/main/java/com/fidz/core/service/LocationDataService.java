//package com.fidz.core.service;
//
//import java.net.UnknownHostException;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fidz.base.config.MultiTenantMongoDbFactory;
//import com.fidz.core.exception.LocationDataNotFoundException;
//import com.fidz.base.config.GenericAppINterface;
//import com.fidz.core.model.LocationData;
//import com.fidz.core.service.LocationDataService;
//import com.fidz.core.business.DistanceCalculation;
//import com.fidz.core.model.TravelType;
//
//@Service("LocationService")
//@Configuration
//public class LocationDataService {
//	
//	
//	
//	private static final Logger logger = LoggerFactory.getLogger(LocationDataService.class);
//	private GenericAppINterface<LocationDataService> genericINterface;
//	@Autowired
//	public LocationDataService(GenericAppINterface<LocationDataService> genericINterface){
//		this.genericINterface=genericINterface;
//	}
//
//	public List<LocationData> getAllActivityFields(String schemaName) throws UnknownHostException{
//		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		 logger.info("schemaName is :: "+schemaName);
//		return this.genericINterface.findAll(LocationData.class);
//	}
//	
//	public void createActivityFields(LocationData locationData){
//		List<Double> lat = new ArrayList<Double>();
//		List<Double> lng = new ArrayList<Double>();
//		Double distance = 0.0;
//	//	JSONObject preObj = new JSONObject(locationData.getGpsData());
//		logger.info("Inside Service Class :: ");
//        String schemaName=locationData.getSchemaName();
//        logger.info("schemaName is :: "+schemaName);
//	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//	    LocationData previousLocation = null;
//	    long count = this.genericINterface.findAll(LocationData.class).stream().filter(loc -> loc.getDeviceID().contains(locationData.getDeviceID())).count();
//	    System.out.println("Count is :: "+count);
//		previousLocation = this.genericINterface.findAll(LocationData.class).stream().filter(loc -> loc.getDeviceID().contains(locationData.getDeviceID())).skip(count - 1).findFirst().get();
//		System.out.println("previousLocation ::: "+previousLocation.getGpsData());
//		ObjectMapper Obj = new ObjectMapper(); 
//		String jsonStrPre = null;
//		String jsonStrPost = null;
//		try {
//			jsonStrPre = Obj.writeValueAsString(previousLocation);
//			jsonStrPost = Obj.writeValueAsString(locationData);
//		    JSONObject preJsonObject = new JSONObject(jsonStrPre);
//		    logger.info("preJsonObject :: "+preJsonObject);
//			JSONObject postJsonObject = new JSONObject(jsonStrPost);
//			logger.info("postJsonObject :: "+postJsonObject);
//			//logger.info("Gps Value is :: "+Double.parseDouble(preJsonObject.getString("latitude")));
//	    	lat.add(preJsonObject.getJSONObject("gpsData").getDouble("latitude"));
//			lat.add(postJsonObject.getJSONObject("gpsData").getDouble("latitude"));
//			lng.add(preJsonObject.getJSONObject("gpsData").getDouble("longitude"));
//			lng.add(postJsonObject.getJSONObject("gpsData").getDouble("longitude"));
//			if (new DistanceCalculation().isSameLocation(preJsonObject.getJSONObject("gpsData").getDouble("latitude"), preJsonObject.getJSONObject("gpsData").getDouble("longitude"), postJsonObject.getJSONObject("gpsData").getDouble("latitude"), postJsonObject.getJSONObject("gpsData").getDouble("longitude"))) {
//				logger.info("Inside Idle ::::  ");
//				locationData.setTravelType(TravelType.valueOf("IDLE"));
//				locationData.setDistance(0.00);
//			} else {
//				logger.info("Inside Movement ::::  ");
//				locationData.setTravelType(TravelType.valueOf("MOVEMENT"));
//				distance = new DistanceCalculation().getDistance(lat, lng, "DRIVING");
//				locationData.setDistance(distance);
//			}
//				logger.info("Distance is :: "+distance);	
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		this.genericINterface.saveName(locationData);
//		logger.info("Data stored ");
//	}
//	
//	public  LocationData updateCurrentData(String id, Object currentData, String schemaName) {
//		LocationData locationData=null;
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		logger.info("schemaName is :: "+schemaName);
//		locationData = this.genericINterface.findById(id,LocationData.class);
//	//	Mono<Device> deviceMono=deviceRepository.findById(id);
//		LocationData device = null;
//		/*if (locationData != null) {
//			device = locationData.block();
//		}
//		device.setCurrentData(currentData);*/
//		return locationData;
//		
//	}
//	
//	public ResponseEntity<LocationData> getActivityFieldById(String id, String schemaName){
//		LocationData locationData=null;
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		logger.info("schemaName is :: "+schemaName);
//		locationData=this.genericINterface.findById(id, LocationData.class);
//		if (locationData == null) {
//			throw new LocationDataNotFoundException(id);
//        } else {
//            return ResponseEntity.ok(locationData);
//        }
//	}
//
//}
