package com.fidz.core.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum ObjectType {
	DEVICE,
	GROUP,
	SCHEDULE,
	DEVICESTATUS,
	USER,
	GATEWAY,
	MAINGATE,
	SECURITYLOCK,
	DEVICEPOWERMASTER,
	OTHER
}
