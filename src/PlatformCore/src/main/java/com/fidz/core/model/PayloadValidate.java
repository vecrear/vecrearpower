package com.fidz.core.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
@Data
@JsonIgnoreProperties(value = { "target" })
public class PayloadValidate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2027608115537403381L;

	@NotNull
	private String objName;			//name of the object in the models
	
	@NotNull
	private String objAction;     //what is the action that object is going to do
	@NotNull
	private String objId; 
								//mongo db id of the specific object
	private String scName;
	
	@NotNull
	private String clientId;  // iP address of the device
	
	@NotNull
	private Object msgPayload;    //  mqtt message payload we are going to communicate
	
	public static PayloadValidate instance = new PayloadValidate();
	
	public PayloadValidate() {
	
	}


	public static PayloadValidate getInstance() {
		return instance;
	}

	public static void setInstance(PayloadValidate instance) {
		PayloadValidate.instance = instance;
	}


	public PayloadValidate(@NotNull String objName, @NotNull String objAction, @NotNull String objId, String scName,
			@NotNull String clientId, @NotNull Object msgPayload) {
		super();
		this.objName = objName;
		this.objAction = objAction;
		this.objId = objId;
		this.scName = scName;
		this.clientId = clientId;
		this.msgPayload = msgPayload;
	}


	@Override
	public String toString() {
		return "PayloadValidate [objName=" + objName + ", objAction=" + objAction + ", objId=" + objId + ", scName="
				+ scName + ", clientId=" + clientId + ", msgPayload=" + msgPayload + "]";
	}


	

	
	

}
