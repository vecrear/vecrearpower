package com.fidz.core.validation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public class Validation {
	public static void main(String[] args) {
			String a = "{\r\n" + 
					"    \"$schema\": \"http://json-schema.org/draft-04/schema#\",\r\n" + 
					"    \"title\": \"Product\",\r\n" + 
					"    \"description\": \"A product from the catalog\",\r\n" + 
					"    \"type\": \"object\",\r\n" + 
					"    \"properties\": {\r\n" + 
					"        \"id\": {\r\n" + 
					"            \"description\": \"The unique identifier for a product\",\r\n" + 
					"            \"type\": \"integer\"\r\n" + 
					"        },\r\n" + 
					"        \"name\": {\r\n" + 
					"            \"description\": \"Name of the product\",\r\n" + 
					"            \"type\": \"string\"\r\n" + 
					"        },\r\n" + 
					"        \"price\": {\r\n" + 
					"            \"type\": \"number\",\r\n" + 
					"            \"minimum\": 0,\r\n" + 
					"            \"exclusiveMinimum\": true\r\n" + 
					"        }\r\n" + 
					"    },\r\n" + 
					"    \"required\": [\"id\", \"name\", \"price\"]\r\n" + 
					"}";
			String b ="{\r\n" + 
					"    \"id\": 1,\r\n" + 
					"    \"name\": \"Lampshade\",\r\n" + 
					"    \"price\": 0\r\n" + 
					"}";
			new Validation().validateSchema(a,b);
	}

	public boolean validateSchema(String schema, String data) {
		boolean flag = true;
		try {
			ObjectMapper mapper = new ObjectMapper();
			final JsonNode jsonNodeData = mapper.readTree(data);
			final JsonNode jsonNodeSchema = mapper.readTree(schema);
			System.out.println("good :: "+jsonNodeData);
			System.out.println("jsonNodeSchema :: "+jsonNodeSchema);
			final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
			final JsonSchema finalSchema = factory.getJsonSchema(jsonNodeSchema);

			ProcessingReport report;
			report = finalSchema.validate(jsonNodeData);
			System.out.println(report);
			flag = report.isSuccess();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return flag;
	}
}
