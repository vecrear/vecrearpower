package com.fidz.mongodb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.util.Date;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fidz.base.model.Device;
import com.fidz.base.repository.BaseDeviceRepository;
import com.fidz.base.service.DeviceService;
import com.fidz.services.location.model.GpsData;
import com.fidz.services.location.model.TravelType;
import com.fidz.services.location.repository.GpsDataRepository;
import com.fidz.services.location.service.GpsDataService;


public class MongoDataStore {
	private static final Logger logger = LoggerFactory.getLogger(MongoDataStore.class);

	@Autowired
	private GpsDataRepository gpsDataRepository;

	@Autowired
	public BaseDeviceRepository baseDeviceRepository;

	@Autowired
	private GpsDataService gpsDataService;

	@Autowired
	private DeviceService deviceService;
	
//	GpsData gpsDataDAO = new GpsData();

	private static final String urlPrefix ="https://maps.googleapis.com/maps/api/geocode/json?";
//	public void insertData(byte[] data, String host, int port, String protocol, int serverPort,LocationDataService locationDataService, DeviceService deviceService) {
//		try {	
//			JSONObject dataObj = new JSONObject(new String(data));
//			LocationData obj1 = null;
//			try {
//				ObjectMapper mapper = new ObjectMapper();
//				obj1 = mapper.readValue(data, LocationData.class);
//				System.out.println("obj1 :: " + obj1);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}		
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	public Device updateDeviceCurrentLocation(Object gpsData, String id, String schemaName,
			DeviceService deviceService) {
		// new LocationDataDAO().updateDeviceCurrentLocation(gpsData, schemaName, id,
		// deviceService);
		Device device = null;
		try {
			device = deviceService.updateCurrentData(id, gpsData, schemaName);
			if (device != null) {
				logger.info("--------------Current data updation-----");
				logger.info("------Gps Data to Device data updated  succesfully-----");
			} else {
				logger.info("-----Gps Data to Device data uis null-----");
			}

		} catch (Exception e) {
			logger.info("Exception in the " + e);
			return null;
		}
		return device;

	}

	public Object insertGpsData(byte[] payload, String host, int port, String protocol, int serverPort,
			GpsDataService gpsDataService, DeviceService deviceService)
			throws JsonParseException, JsonMappingException, IOException {

		Object objResponse = null;
		ObjectMapper mapper = new ObjectMapper();
		GpsData gpsDataDAO = new GpsData();
		JSONObject dataObj = new JSONObject(new String(payload));
		String deviceId = dataObj.getString("deviceID");
		String schemaName = dataObj.getString("schemaName");
		
		Double lat = 0.0;
		Double lng = 0.0;
		String address = "";
		 lat = dataObj.getJSONObject("gpsData").getDouble("latitude");
		 lng = dataObj.getJSONObject("gpsData").getDouble("longitude");		
				
		 address = reverseGeoCoding(lat,lng);
		 logger.info("dataObj --address---from device :: " + dataObj.getString("address"));
		logger.info("dataObj --gpsdata :: " + dataObj.getJSONObject("gpsData"));
		logger.info("schemaName -------:: " + dataObj.getString("schemaName"));
		logger.info("deviceID------ ---:: " + dataObj.getString("deviceID"));
		logger.info("dateTime--------- :: " + dataObj.getString("dateTime"));
		logger.info("travelType -------:: " + dataObj.getString("travelType"));
		logger.info("distance ---------:: " + dataObj.getDouble("distance"));

		Instant timestamp = getDateFromString(dataObj.getString("dateTime"));
		String travelType = dataObj.getString("travelType");
		Date myDate = Date.from(timestamp);
		logger.info("myDate---------- :: " + myDate);
		// logger.info("travelType ------:: "+travelType);
		gpsDataDAO.setGpsData(dataObj.getJSONObject("gpsData"));
		gpsDataDAO.setSchemaName(dataObj.getString("schemaName"));
		gpsDataDAO.setDeviceID(dataObj.getString("deviceID"));
		gpsDataDAO.setDistance(dataObj.getDouble("distance"));
		gpsDataDAO.setDateTime(myDate);
		gpsDataDAO.setTravelType(TravelType.valueOf(travelType));
		gpsDataDAO.setAddress(address);
	
		Device device1 = deviceService.getDeviceDataByDeviceId(deviceId, schemaName);

		logger.info("mongoDeviceId ---->:: " + device1.getId());
		if (device1.getId() == null) {
			logger.info("Device does not exist. Please create the device and try again");
			logger.info("Data not Stored");
		} else {
			try {
				updateDeviceCurrentLocation(dataObj.getJSONObject("gpsData"), device1.getId(),
						dataObj.getString("schemaName"), deviceService);
				logger.info("Device data successfully stored in Mongodb");
				logger.info("--------------creating the new Gps Data-----");
				objResponse = gpsDataService.createGpsData(gpsDataDAO);
				if (objResponse != null) {

					GpsData gpsData = (GpsData) objResponse;
					logger.info("--Gps Data created ID -----" + gpsData.getId(), "+\n");

				}
				// logger.info("objResponse ---->:: "+objResponse.toString());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			// logger.info("Device data successfully stored in Mongodb");

		}

		return objResponse;
	}

	private String getMongoDeviceId(String deviceID, String schemaName) {
		Device device = null;
		try {

			logger.info("deviceID------schemaName-------" + deviceID, "" + schemaName);

			device = deviceService.getDeviceDataByDeviceId(deviceID, schemaName);
			return device.getId();
		} catch (Exception e) {
			return device.getId();
		}

	}

	private Instant getDateFromString(String string) {
		// Create an Instant object
		Instant timestamp = null;
		// Parse the String to Date
		timestamp = Instant.parse(string);
		// return the converted timestamp
		return timestamp;
	}
	
	private static String reverseGeoCoding(double latitude, double longitude) {
		System.out.println("\nLatitude: " + latitude +"\nLongitude: " + longitude);
		String address = "";
		String status = "";
		String url = urlPrefix+"latlng="+latitude+","+longitude+"&key=AIzaSyDNMKPzkkKtjipjb3YIyXEEXh-Piq7qbbU";
		System.out.println("\nurl: " + url);
		try {
			URL uriAddress = new URL(url);
			URLConnection res = uriAddress.openConnection();
			BufferedReader bufferedReader  =  new BufferedReader(new InputStreamReader(res.getInputStream()));
		    StringBuilder builder = new StringBuilder();
		    String line;
		    while ((line = bufferedReader.readLine()) != null) {
		        builder.append(line + "\n");
		    }	
		    String builderArray = builder.toString();
		    //System.out.println("\nbuilderArray: \n" + builderArray);
		    JSONObject root = (JSONObject)JSONValue.parseWithException(builderArray);
		    //System.out.println("\nresult: \n" + root);
		    JSONArray resultArray = (JSONArray) root.get("results");
		    JSONObject obj0 = (JSONObject) resultArray.get(0);
		    //System.out.println("\nobj0: \n" + obj0);
		    String formattedAddress = (String) obj0.get("formatted_address");
		    System.out.println("\nFormattedAddress: \n" + formattedAddress);
		    address = formattedAddress;
		    
		    // Accessing status
		    String stat = (String) root.get("status");
		    status = stat;
		    System.out.println("\nStatus: " + status);
		}
		catch(Exception e) {
			System.out.println("\nException: " + e);
		}
		if(status.contentEquals("ZERO_RESULTS")) {
			address = "No address";
			return address;
		}
		if(status.contentEquals("OVER_QUERY_LIMIT")) {
			address = "Crossed your quota of api calls";
			return address;
		}
		if(status.contentEquals("REQUEST_DENIED")) {
			address = "Request denied";
			return address;
		}
		if(status.contentEquals("INVALID_REQUEST")) {
			address = "Invalid query";
			return address;
		}		
		if(status.contentEquals("UNKNOWN_ERROR")) {
			address = "Server Error";
			return address;
		}		
		if(address.contentEquals("")) {
			address = "Blank Address";
			return address;
		}
		else {
			return address;
		}
	}

}
