package com.fidz.services.authentication.security;


import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class Encryption {
	private static final String ALGORITHM = "AES";
	private static byte[] key;
    public Encryption(byte[] key)
    {
        this.key = key;
    }

    /**
     * Encrypts the given plain text
     *
     * @param plainText The plain text to encrypt
     */
    public String decrypt(String strToDecrypt, String  securityKey) {
    	
		return strToDecrypt;
    	
    }
    public static byte[] encrypt(byte[] plainText) throws Exception
    {
        SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
    	
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        return cipher.doFinal(plainText);
    }

    /**
     * Decrypts the given byte array
     *
     * @param cipherText The data to decrypt
     */
    public static byte[] decrypt(byte[] cipherText) throws Exception
    {
        SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        return cipher.doFinal(cipherText);
    }
		 public static void main(String[] args) throws Exception {
			 byte[] encryptionKey = "MZygpewJsCpRrfOr".getBytes(StandardCharsets.UTF_8);
			 byte[] plainText = "Hello world!".getBytes(StandardCharsets.UTF_8);
			 Encryption advancedEncryptionStandard = new Encryption(
			         encryptionKey);
			 byte[] cipherText = Encryption.encrypt(plainText);
			 byte[] decryptedCipherText = Encryption.decrypt(cipherText);

			 System.out.println(new String(plainText));
			 System.out.println(new String(cipherText));
			 System.out.println(new String(decryptedCipherText));
		}
		    
		 public SecretKeySpec getSecurityKey(String myKey){
	         MessageDigest sha = null;
	         byte[] keys;
	         SecretKeySpec secretKey = null;
	         try {
	             keys = myKey.getBytes("UTF-8");
	             sha = MessageDigest.getInstance("SHA-1");
	             keys = sha.digest(keys);
	             keys = Arrays.copyOf(keys, 16); // use only first 128 bit
	             //System.out.println(keys.length);
	             //System.out.println(new String(keys,"UTF-8"));
	             secretKey = new SecretKeySpec(keys, "AES");
	         } catch (NoSuchAlgorithmException e) {
	             // TODO Auto-generated catch block
	             e.printStackTrace();
	         } catch (UnsupportedEncodingException e) {
	             // TODO Auto-generated catch block
	             e.printStackTrace();
	         }
	         return secretKey;
	     }
}
