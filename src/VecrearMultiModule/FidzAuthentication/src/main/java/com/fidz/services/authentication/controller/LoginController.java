/*package com.fidz.services.authentication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.exception.UserException;
import com.fidz.base.exception.UserNotFoundException;
import com.fidz.base.model.User;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.services.authentication.model.Login;
import com.fidz.services.authentication.service.LoginService;

import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/login")
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	 
	@PostMapping("/authenticate")
	 public Mono<ResponseEntity<User>> authenticateUser(@Valid @RequestBody Login login) {
	     return loginService.authenticateUser(login);
	 }
	
	
	@ExceptionHandler
	public ResponseEntity<?> handleNotFoundException(UserNotFoundException ex) {
	    return ResponseEntity.notFound().build();
	}
	
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> handleUserException(UserException ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex.getMessage()));
	}	
}
*/