package com.fidz.services.authentication.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.fidz.base.validator.Constant;

public class EncriptionUtil {
	public String encrypt(String strToEncrypt, SecretKeySpec securityKey) {
		Constant.LOGGER.info("inside encrypt EncriptionUtil.java");
		String encryptPwd = "";
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, securityKey);
            encryptPwd = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
            
        }
        catch (Exception e) {
           Constant.LOGGER.error("Error while encrypting: "+e.getMessage());
        }
        return encryptPwd;
    }
 
	public String decrypt(String strToDecrypt, SecretKeySpec securityKey) {
		Constant.LOGGER.info("inside decrypt EncriptionUtil.java");
		String decryptPwd = "";
        try {
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, securityKey);
            decryptPwd = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
        } catch (Exception e) {
           Constant.LOGGER.error("Error while decrypting: "+e.getMessage());
        }
        return decryptPwd;
    }
	 
	 public SecretKeySpec getSecurityKey(String myKey){
		 Constant.LOGGER.info("inside SecretKeySpec getSecurityKey EncriptionUtil.java");
		 MessageDigest sha = null;
         byte[] keys;
         SecretKeySpec secretKey = null;
         try {
             keys = myKey.getBytes("UTF-8");
             sha = MessageDigest.getInstance("SHA-1");
             keys = sha.digest(keys);
             keys = Arrays.copyOf(keys, 16); // use only first 128 bit
             //System.out.println(keys.length);
             //System.out.println(new String(keys,"UTF-8"));
             secretKey = new SecretKeySpec(keys, "AES");
         } catch (NoSuchAlgorithmException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         } catch (UnsupportedEncodingException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         }
         return secretKey;
     }
	 
	 
	 /*public static void main(String[] args) {
		System.out.println("Encryption :: "+new EncriptionUtill().encrypt("12345667890", new EncriptionUtill().getSecurityKey("99985475854")));
		System.out.println("decyption  :: "+new EncriptionUtill().decrypt("8y7LMuK0G11PFpPPZaOlBQ==", new EncriptionUtill().getSecurityKey("99985475854")));
	}*/
}
