package com.fidz.services.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.fidz.base", "com.fidz.services.authentication"})
@EnableReactiveMongoRepositories
public class FidzAuthenticationApplication {
	public static void main(String[] args) {
		SpringApplication.run(FidzAuthenticationApplication.class, args);
	}
}
