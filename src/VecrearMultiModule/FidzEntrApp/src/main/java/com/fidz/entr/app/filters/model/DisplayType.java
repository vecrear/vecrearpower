package com.fidz.entr.app.filters.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class DisplayType {

	private Boolean daily;
	private Boolean weekly;
	private Boolean forenight;
	private Boolean monthly;
	private Boolean quarterly;
	private Boolean annually;
	
	public DisplayType(Boolean daily, Boolean weekly, Boolean forenight, Boolean monthly, Boolean quarterly,
			Boolean annually) {
		super();
		this.daily = daily;
		this.weekly = weekly;
		this.forenight = forenight;
		this.monthly = monthly;
		this.quarterly = quarterly;
		this.annually = annually;
	}
	
	public DisplayType() {
		super();
		
	}
	
	@Override
	public String toString() {
		return "DisplayType [daily=" + daily + ", weekly=" + weekly + ", forenight=" + forenight + ", monthly="
				+ monthly + ", quarterly=" + quarterly + ", annually=" + annually + "]";
	}
	
	

}
