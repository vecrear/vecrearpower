package com.fidz.entr.app.filters.service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.StringOperators;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.ActivityMap;
import com.fidz.entr.app.filters.model.LastpunchinDate;
import com.fidz.entr.app.filters.model.PunchInInfo;
import com.fidz.entr.app.filters.model.PunchOutInfo;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.repository.AttendanceRepository;
import com.fidz.entr.app.repository.UserRepository;

@Service("LandingPageService")
public class LandingPageService {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	AttendanceRepository attendanceRepository;
	
	@Autowired
	UserRepository userRepository;

	@SuppressWarnings("unchecked")
	public JSONObject getlandingPage(String schemaName, String userid) throws ParseException {
		 Constant.LOGGER.info("Inside get getlandingPage()");
       	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       	 Instant instant = Instant.ofEpochMilli(new Date().getTime());
      	 ZonedDateTime TimeinIST = instant.atZone(ZoneId.of("Asia/Kolkata"));
      	 ZonedDateTime zdtpreviousDay = TimeinIST.plusDays(1);

     	 Constant.LOGGER.info("Date_time"+TimeinIST);
      	 Date registerTimeBeginDate = Date.from(zdtpreviousDay.toInstant());

      	 Instant instant1 = Instant.ofEpochMilli(new Date().getTime()+86400000);
      	 Constant.LOGGER.info("instant 1"+instant1);
      	 ZonedDateTime TimeinIST1 = instant.atZone(ZoneId.of("Asia/Kolkata"));
      	 ZonedDateTime zdtNextDay = TimeinIST1.minusMonths(1);
     	 Constant.LOGGER.info("Date_time"+zdtNextDay);
      	 Date registerTimeBeginDate1 = Date.from(zdtNextDay.toInstant());
      	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
    	 String strDate = format.format(registerTimeBeginDate);
         String endDate1 = format.format(registerTimeBeginDate1);
         Date endDate =format.parse(endDate1+" "+"23:59");

		 List<User> users= this.userRepository.findAll();
	     List<String> user= users.stream().map(p->p.getId()).collect(Collectors.toList());
       
         JSONObject obj = new JSONObject();
         
         
         Constant.LOGGER.info("strDate"+strDate);
         Date startDate =format.parse(strDate+" "+"00:00");
         Constant.LOGGER.info("endDate"+endDate);
         
       	 Aggregation agg1 = newAggregation(
       			  match(Criteria.where("user.id").is(userid).andOperator
       					  (Criteria.where("punchInDateTimeStamp").lte(startDate).gte(endDate))),
		       			  group("punchInDateTimeStamp").last("punchInDateTimeStamp").as("lastpunchinDate"),
       			  project("lastpunchinDate"),
       			  sort(Sort.Direction.DESC,"lastpunchinDate")
       			 );
       	 
	       	Constant.LOGGER.info("&&&&&&&&&&&&&&&&&&&&&"+agg1.toString());
   
        	 
       	AggregationResults<LastpunchinDate> groupResults = mongoTemplate.aggregate(agg1,Attendance.class,LastpunchinDate.class );
       
       	Constant.LOGGER.info("&&&&&&&&&&&&&&&&&&&&&"+groupResults);
          List<LastpunchinDate> result 	=groupResults.getMappedResults();
          if(result.size()>0) {
        	  result =groupResults.getMappedResults().subList(0, 1);
          }

        	
        	  
        
        Constant.LOGGER.info("Aggregate result"+agg1);
    Constant.LOGGER.info("Aggregation Results"+result);
           
	      
      Collection<String> type = new ArrayList<String>();
	     //type.add("POSTPONED");
	     //type.add("OPEN");
	     type.add("CANCELLED");
	     type.add("COMPLETED");
	   
     
	     Aggregation agg2 = newAggregation( 
	     			match(Criteria.where("activityStatus").in(type).andOperator(
     					Criteria.where("resource.id").is(userid),
     					Criteria.where("actualStartDateTime").lte(startDate).gte(endDate))
     					),
	     			group("actualStartDateTime","activityStatus").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 10)).as("actualStartDateTime"),
	     			group("activityStatus","actualStartDateTime").count().as("totalcount"),
	     			project("totalcount").and("activityStatus").previousOperation(),
	     		    sort(Sort.Direction.DESC,"totalcount"), 
	     		    sort(Sort.Direction.ASC,"activityStatus.actualStartDateTime")
     			);  
        
      AggregationResults<ActivityMap> groupResults1 = mongoTemplate.aggregate(agg2,Activity.class,ActivityMap.class );
      List<ActivityMap> result1 = groupResults1.getMappedResults();

      Aggregation agg3 = newAggregation( 
	    		 match(Criteria.where("activityStatus").in(type).andOperator(
	     					(Criteria.where("resource.id").is(userid)),
	     					 Criteria.where("actualStartDateTime").lte(startDate),
	     					 Criteria.where("actualStartDateTime").gte(endDate))
	     					),
	     			group("actualStartDateTime","activityStatus").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 7)).as("actualStartDateTime"),
	     			group("activityStatus","actualStartDateTime").count().as("totalcount"),
	     			project("totalcount").and("activityStatus").previousOperation(),
	     		    sort(Sort.Direction.DESC,"totalcount"), 
	     		    sort(Sort.Direction.ASC,"activityStatus.actualStartDateTime")
	     			);
	      
	      AggregationResults<ActivityMap> groupResults2 = mongoTemplate.aggregate(agg3,Activity.class,ActivityMap.class);
	      List<ActivityMap> result2 = groupResults2.getMappedResults();
	     
	      
	      Constant.LOGGER.info("Inside get userdetails*******");
			 User use=mongoTemplate.findById(userid, User.class);
			   //Constant.LOGGER.info("userdetails"+use);
		   String use1= use.getName().getFirstName();

			   String use2=use.getName().getMiddleName();
			   String use3=use.getName().getLastName();
		   String use4=use.getContact().getPhoneNo().getPrimary();
		   Constant.LOGGER.info("******************************************"+use4);

			   String use5=use.getAddress().getLine1();
			
			   Constant.LOGGER.info("******************************************"+use5);

		   String use6=use.getCompany().getName();
		   Constant.LOGGER.info("******************************************"+use6);

		   String use7=use.getDepartment().getName();
		   Constant.LOGGER.info("******************************************"+use7);

		   String use8=use.getUserRole().getName();
		   Constant.LOGGER.info("******************************************"+use8);

		   
		   String use10=use.getCreatedByUser();
		   Constant.LOGGER.info("******************************************"+use8);
		   String use11 =null;
		   for(int i = 0;i < use.getCitylist().size();i++) {
			   Constant.LOGGER.info("*****************use.getCity().get(i).getName()*************************"+use.getCitylist().get(i).getName());
			   String cityName = use.getCitylist().get(i).getName();
			   
			
			    
			   use11 = cityName+",";
		   }
		//   String use11 = use.getCity().getName();
		   Constant.LOGGER.info("******************************************"+use8);

		   
			   String use9=String.valueOf(use.getStatus());
			   Constant.LOGGER.info("******************************************"+use9);

			   StringBuilder builder = new StringBuilder();
				   builder.append(use1);
              
			   if(use2.isEmpty()==false) {
				   Constant.LOGGER.info("inside first if");
				   builder.append(' ');
				   builder.append(use2);
				   
			  }
			   
			   if(use3.isEmpty()==false){
				   Constant.LOGGER.info("inside second if");
				   builder.append(' ');
				   builder.append(use3);
			   }
				   
			   
		   String name=builder.toString();
			   Constant.LOGGER.info("******************************************"+name);
			   Map<String,String> userdetails = new HashMap<String, String>(); 
			    userdetails.put("username",name);
			    userdetails.put("phoneno",use4 );
		        userdetails.put("address",use5 );
			    userdetails.put("company",use6 );
			    userdetails.put("department",use7 );
			    userdetails.put("userrole",use8 );
			    userdetails.put("createdByUser",use10);
			    userdetails.put("cityName", use11);
			    userdetails.put("status",use9 );
	      



		    Aggregation agg4 = newAggregation( 
			 match(Criteria.where("user.id").in(user).andOperator(
			  Criteria.where("punchOutDateTimeStamp").is(null),
			  Criteria.where("punchInDateTimeStamp").lte(startDate).gte(endDate))),
			  group("user.id","punchInDateTimeStamp").count().as("punchInCount"),
			 group("punchInCount","user.id").sum("punchInCount").as("punchInCount"),
			 project("punchInCount")
			    //  sort(Sort.Direction.DESC,"punchInCount")
			     );
		    
		    Constant.LOGGER.info("PunchIn aggregation"+agg4);

		     AggregationResults<PunchInInfo> groupResults3 = mongoTemplate.aggregate(agg4,Attendance.class,PunchInInfo.class);
		     List<PunchInInfo> result3 = groupResults3.getMappedResults();
		     
		    

	        Aggregation agg = newAggregation( 
		    		 match(Criteria.where("user.id").in(user).andOperator(
		    				     Criteria.where("punchInDateTimeStamp").lte(startDate).gte(endDate),
		     					 Criteria.where("punchOutDateTimeStamp").lte(startDate).gte(endDate))),
		     			group("user.id","punchOutDateTimeStamp").count().as("punchOutCount"),
		     			group("punchOutCount","user.id").sum("punchOutCount").as("punchOutCount"),
		     			project("punchOutCount")
		     			);
	    Constant.LOGGER.info("PunchOut aggregation"+agg);
	        
	     AggregationResults<PunchOutInfo> groupResults5 = mongoTemplate.aggregate(agg,Attendance.class,PunchOutInfo.class);
	     List<PunchOutInfo> result5 = groupResults5.getMappedResults();
	      
	      obj.put("lastLoginDetails", result);
          obj.put("activityCurrentDay", result1);
          obj.put("activityCurrentMonth",result2);
          obj.put("userdetails", userdetails);
          obj.put("punchIn", result3);
          obj.put("punchOut", result5);


      return obj;
    
   

	}
////to get latest 5 attendance records based on user
//	public List<Attendance> getbyUserlatestFive(String schemaName, String userid) {
//      	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//      	 Constant.LOGGER.info("inside getbyUserlatestFive LandingPageService.java");
//      	 List<Attendance> attendances =null;
//      	try {
//      		attendances = this.attendanceRepository.findAll().subList(0, 5);
//      	}catch(Exception ex) {
//      		Constant.LOGGER.error("errror while fetching in getbyUserlatestFive LandingPageService.java"+ex.getMessage());
//      	}
//		return attendances;
//	}
	
	//to get latest 5 attendance records based on user
		public List<Attendance> getbyUserlatestFive(String schemaName, String userid) {
	      	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	      	 Constant.LOGGER.info("inside getbyUserlatestFive LandingPageService.java");
	      	 List<Attendance> attendances =null;
	      	 Query query = new Query();
	      	try {
	      		query.addCriteria(Criteria.where("user.id").is(userid));
	      		attendances = this.mongoTemplate.find(query,Attendance.class).subList(0, 5);
	      	}catch(Exception ex) {
	      		Constant.LOGGER.error("errror while fetching in getbyUserlatestFive LandingPageService.java"+ex.getMessage());
	      	}
			return attendances;
		}

	

}
