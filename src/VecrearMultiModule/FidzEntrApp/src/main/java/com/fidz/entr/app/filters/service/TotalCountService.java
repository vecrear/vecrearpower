package com.fidz.entr.app.filters.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.model.Application;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Group;
import com.fidz.base.model.Role;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.MultiUserReportAttendance;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.CustomerCategory;
import com.fidz.entr.app.model.GeoStatusHistory;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.DeviceType;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.Stream;
import com.fidz.entr.base.model.UserHierarchyLevel;
import com.fidz.entr.superadmin.model.RoleFeatureConfig;
import com.fidz.services.location.model.GpsData;

@Service("TotalCountService")
public class TotalCountService {

	@Autowired
	MongoTemplate mongoTemplate;

	@SuppressWarnings("unchecked")
	public JSONObject getCount(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	Constant.LOGGER.info("inside getCount TotalCountService.java");	
		Map<String,Long> atr = new HashMap<String, Long>();
		JSONObject obj = new JSONObject();
        Query q = new Query();		
	long x1=mongoTemplate.count(q, Company.class);
	long x2=mongoTemplate.count(q,City.class);
	long x3=mongoTemplate.count(q,Region.class);
	long x5=mongoTemplate.count(q,Country.class);
	long x6=mongoTemplate.count(q,Group.class);
	long x7=mongoTemplate.count(q,Feature.class);
    long x8=mongoTemplate.count(q,Role.class);
    long x9=mongoTemplate.count(q,Application.class);
    long x10=mongoTemplate.count(q,DeviceType.class);
    long x12=mongoTemplate.count(q,Device.class);
    long x13=mongoTemplate.count(q,Department.class);
    long x14=mongoTemplate.count(q,UserHierarchyLevel.class);
    long x15=mongoTemplate.count(q,UserRole.class);
    long x16=mongoTemplate.count(q,User.class);
    long x17=mongoTemplate.count(q,CustomerCategory.class);
    long x18=mongoTemplate.count(q,Customer.class);
    long x19=mongoTemplate.count(q,Facility.class);
    long x20=mongoTemplate.count(q,ActivityField.class);
    long x21=mongoTemplate.count(q,ActivityType.class);
    long x22=mongoTemplate.count(q,Activity.class);
    long x23=mongoTemplate.count(q,Attendance.class);
    long x24=mongoTemplate.count(q, GeoStatusHistory.class);
    long x25=mongoTemplate.count(q, State.class);
    long x26=mongoTemplate.count(q, RoleFeatureConfig.class);
    long x27=mongoTemplate.count(q,Stream.class);


        atr.put("companyCount", x1);
        atr.put("cityCount", x2);
        atr.put("regionCount", x3);
        atr.put("countryCount", x5);
        atr.put("groupCount", x6);
        atr.put("featureCount", x7);
        atr.put("roleCount", x8);
        atr.put("applicationCount", x9);
        atr.put("deviceTypeCount", x10);
        atr.put("deviceCount", x12);
        atr.put("departmentCount", x13);
        atr.put("userHierarchyLevel", x14);
        atr.put("userRoleCount", x15);
        atr.put("userCount", x16);
        atr.put("customerCategoryCount", x17);
        atr.put("customerCount", x18);
        atr.put("facilityCount", x19);
        atr.put("activityFieldCount", x20);
        atr.put("activityTypeCount", x21);
        atr.put("activityCount", x22);
        atr.put("attendanceCount", x23);
        atr.put("geoStatusHistoryCount", x24);
        atr.put("stateCount", x25);
        atr.put("roleFeatureConfig", x26);
        atr.put("streamCount", x27);



		
		obj.put("totalCount", atr);
		 
		
		return obj;
	}

	@SuppressWarnings("unchecked")
	public JSONObject getByDateCount(String schemaName, String startdate, String enddate) throws ParseException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getByDateCount() TotalCountService.java");	
		Map<String,Long> atr = new HashMap<String, Long>();
		JSONObject obj = new JSONObject();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date startDate = format.parse(startdate+" "+"00:00");
        Date endDate = format.parse(enddate+" "+"23:59");
        //end date as plus 1
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusHours(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        
        Query q = new Query();
        q.addCriteria(Criteria.where("plannedStartDateTime").gte(startDate).lte(currentDatePlusOneDay));
		long x1=mongoTemplate.count(q, Activity.class);
		
		
		Query query = new Query();
    	query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(currentDatePlusOneDay));
        long x2 = mongoTemplate.count(query, Attendance.class);
        
        atr.put("activityCount", x1);
        atr.put("attendanceCount", x2);

		obj.put("datewiseCount", atr);

        
        return obj;
	}
	
	
	@SuppressWarnings("unchecked")
    public JSONObject getByUserDateCount(String schemaName, String id, String startdate, String enddate) throws ParseException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getByDateCount() TotalCountService.java");	
		Map<String,Long> atr = new HashMap<String, Long>();
		JSONObject obj = new JSONObject();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date startDate = format.parse(startdate+" "+"00:00");
        Date endDate = format.parse(enddate+" "+"23:59");
        //end date as plus 1
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusHours(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        
        Query q = new Query();
        q.addCriteria(Criteria.where("resource.id").is(id));
        q.addCriteria(Criteria.where("plannedStartDateTime").gte(startDate).lte(currentDatePlusOneDay));
		long x1=mongoTemplate.count(q, Activity.class);
		
		
		Query query = new Query();
        query.addCriteria(Criteria.where("user.id").is(id));
        query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(currentDatePlusOneDay));
        long x2 = mongoTemplate.count(query, Attendance.class);
        
        atr.put("activityCount", x1);
        atr.put("attendanceCount", x2);

		obj.put("userdateCount", atr);

        
        return obj;
	}



	@SuppressWarnings("unchecked")
	public JSONObject getMultiUserReportAttendance(String schemaName, MultiUserReportAttendance mult) throws ParseException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getMultiUserReportAttendanceGpsData TotalCountService.java");	
		Map<String,Long> atr = new HashMap<String, Long>();
		JSONObject obj = new JSONObject();
        List<String> userid = mult.getUsers().stream().map(user-> user.getId()).collect(Collectors.toList());
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date startDate = format.parse(mult.getStartDate()+" "+"00:00");
        Date endDate = format.parse(mult.getEndDate()+" "+"23:59");
        //end date as plus 1
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusHours(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        		
        Query q = new Query();
        q.addCriteria(Criteria.where("resource.id").in(userid));
        q.addCriteria(Criteria.where("actualStartDateTime").gte(startDate).lte(currentDatePlusOneDay));
		long x1=mongoTemplate.count(q, Activity.class);
		
		
		Query query = new Query();
        query.addCriteria(Criteria.where("user.id").in(userid));
        query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(currentDatePlusOneDay));
        long x2 = mongoTemplate.count(query, Attendance.class);
        
        Query query1 = new Query();
        query1.addCriteria(Criteria.where("dateTime").gte(startDate).lte(currentDatePlusOneDay).and("gpsData.map.userId").in(userid));
        long x3 = mongoTemplate.count(query1, GpsData.class);
        
        Query query2 = new Query();
    	query2.addCriteria(Criteria.where("userId").in(userid));
    	query2.addCriteria(Criteria.where("createdDate").gte(startDate).lte(currentDatePlusOneDay));
        long x4 = mongoTemplate.count(query2, GeoStatusHistory.class);
        
        atr.put("activityCount", x1);
        atr.put("attendanceCount", x2);
        atr.put("gpsDataCount", x3);
        atr.put("geoStatusHistoryCount",x4);
        obj.put("multiUserCount", atr);
        
        
        return obj;
	}
	
	public String sendSms(String schemaName,String phone,String message) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside sendSms TotalCountService.java");	
		Constant.LOGGER.info("send sms"); 
		Constant.LOGGER.info("phone"+phone);
		Constant.LOGGER.info("message"+message);
		String response = "";
     	String  URLs = "https://smsleads.in/pushsms.php";
     	String builderArray = null;
     	try {
     		URL uriAddress = new URL(URLs);
			URLConnection res = uriAddress.openConnection();

			
			
			Map<String, String> parameters = new HashMap<>();//todo from Database
			parameters.put("username", "vecrear");
			parameters.put("password", "Vecrear123555");
			parameters.put("sender", "VTRACK");
			parameters.put("numbers",phone );
			parameters.put("message", message);
			 
			res.setDoOutput(true);
			DataOutputStream out = new DataOutputStream(res.getOutputStream());
			 Constant.LOGGER.info("parameters----------"+parameters.toString()); 
			out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
			out.flush();
			out.close();
     	      // give it 15 seconds to respond
     	      res.setReadTimeout(15*1000);
     	      res.connect();
			BufferedReader bufferedReader  =  new BufferedReader(new InputStreamReader(res.getInputStream()));
		    StringBuilder builder = new StringBuilder();
       	    String line;
		    while ((line = bufferedReader.readLine()) != null) {
		        builder.append(line);
		    	
		    }
		    builderArray = builder.toString();
		    Constant.LOGGER.info("builder array"+builderArray); 
		
        }catch(Exception ex) {
        	Constant.LOGGER.error("Error inside modes SMS"+ ex.getMessage());
        }
     	if(builderArray.contentEquals("YOUR SMS HAS BEEN SENT<br>")) {
			response = "SMS successfully sent";
			return response;
		}
		if(builderArray.contentEquals("INVALID<br>")) {
			response = "Invalid Phone Number";
			return response;
		}
		else {
		return builderArray;
		}
	}

}
