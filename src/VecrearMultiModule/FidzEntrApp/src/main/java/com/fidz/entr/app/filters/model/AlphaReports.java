package com.fidz.entr.app.filters.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.Customer;
import com.google.firebase.database.annotations.NotNull;

import lombok.Data;

@Data
@Document(collection = "alphaReportScheduler")
@JsonIgnoreProperties(value = { "target" })
public class AlphaReports {
	
	@Id
	private String id;
	
	private String schemaName;
	
	
	private String createdUserName;
	
	private String createdUserId;
	
	private String companyId;
	
	private String departmentId;
	
	private String countryId;
	
	private String regionId;
	
	private String stateId;
	
	private String cityId;
	
	private List<UserInfo> sendEmailTo;
	
	private List<UserInfo> users;
	
	private String subject;
	
	private String message;
	
	private List<String> customerId;
	
	private String startDate;
	
	private String endDate;
	
	private List<String> activityTypesId;
	
	private String scheduleDateTime;
	
	private String emailSent;

}
