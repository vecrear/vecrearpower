package com.fidz.entr.app.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.GeoStatusHistory;
import com.fidz.entr.app.model.ResourceAttendanceReport;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.service.GeoStatusHistoryService;

@RestController("GeoStatusHistoryController")
@CrossOrigin
@RequestMapping("/geostatushistory")
public class GeoStatusHistoryController {

	
	
	@Autowired
	GeoStatusHistoryService geoStatusHistoryService;
	
	
	@PostMapping
	public GeoStatusHistory save(@RequestHeader(value="schemaName")String schemaName,@Valid @RequestBody GeoStatusHistory geoStatusHistory) {
		return geoStatusHistoryService.save(schemaName,geoStatusHistory);
		
	}
		@PostMapping("/get/all")
		public List<GeoStatusHistory> getAll(@RequestHeader(value="schemaName")String schemaName){
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return geoStatusHistoryService.getAll(schemaName);
		}
	
		
		@PostMapping("get/all/web/{pageNumber}/{pageSize}")
		public List<GeoStatusHistory> getAllUsersWebPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return geoStatusHistoryService.getAllGeoWebPaginated(pageNumber, pageSize, schemaName);
		}
		
		@PostMapping("search/text/{text}/get/all/web/{pageNumber}/{pageSize}")
		public List<GeoStatusHistory> getAllSearchUsersWebPaginated(@PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return geoStatusHistoryService.getAllSearchUsersWebPaginated(pageNumber, pageSize, schemaName,text);
		}
	
		@PostMapping("/get/all/web")
		public List<GeoStatusHistory> getAllActivityPaginatedWeb( @Valid @RequestBody ResourceAttendanceReport report,@RequestHeader(value="schemaName") String schemaName) throws ParseException{
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return geoStatusHistoryService.getAllGeoPaginatedWeb(report, schemaName);
		}
		
		@PostMapping("/get/id/{id}")
		public GeoStatusHistory getGeoStatusHistoryById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return geoStatusHistoryService.getGeoStatusHistoryById(id, schemaName);
			
		}
		
		@PostMapping("/update/id/{id}")
	    public GeoStatusHistory updateGeoStatusHistory(@PathVariable(value = "id") String id,@Valid @RequestBody GeoStatusHistory geoStatusHistory,@RequestHeader(value="schemaName") String schemaName) {
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return geoStatusHistoryService.updateGeoStatusHistory(id, geoStatusHistory,schemaName);
	    }

	    @PostMapping("/delete/id/{id}")
	    public Map<String, String> deleteGeoStatusHistory(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
	    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
	    	return geoStatusHistoryService.deleteGeoStatusHistory(id, schemaName);
	    }
	    @PostMapping("/delete/soft/id/{id}")
	    public Map<String, String> softDeleteGeoStatusHistory(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
	    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
	    	return geoStatusHistoryService.softDeleteGeoStatusHistory(id, schemaName, timeUpdate);
	    }
}
