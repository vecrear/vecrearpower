package com.fidz.entr.app.model;
import java.util.List;
import lombok.Data;

@Data
public class ResourceAttendanceReport {
	
	private List<User> users;

    private String startDate;

	private String endDate;

    private int pageNum;
	
	private int pageSiz;

}
