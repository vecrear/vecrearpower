package com.fidz.entr.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Service;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.entr.app.exception.ActivityNotFoundException;
import com.fidz.entr.app.model.Activity;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FEAUserDataService")
public class UserDataService {
	
	private UserDataInterface userDataInterface;
	@Autowired
	private ReactiveMongoTemplate reactiveMongoTemplate;
	
	@Autowired
	public UserDataService(UserDataInterface userDataInterface){
		this.userDataInterface=userDataInterface;
	}
	/*private GenericBaseINterface<UserData> genericINterface;
	@Autowired
	public UserDataService(GenericBaseINterface<UserData> genericINterface){
		this.genericINterface=genericINterface;
	}*/
	public Flux<UserData> getAllUserDatas(){
		return  userDataInterface.findAll();
	}
    /*public Flux<UserData> getAllUserDatas(String schemaName){
    	schemaName="fidzentrapp2";
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findAll(UserData.class);
		//return  userDataInterface.findAll();
	}*/
	/*public Mono<UserData> createUserData(UserData userData){
		String schemaName=userData.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//return this.userDataInterface.saveName(userData);
    	return this.genericINterface.saveName(userData);
		//return reactiveMongoTemplate.save(userData);
	}*/
	
	public Mono<UserData> createUserData(UserData userData){
		String schemaName=userData.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.userDataInterface.saveName(userData);
    	//return this.genericINterface.saveName(userData);
		//return reactiveMongoTemplate.save(userData);
	}
	
	
}
