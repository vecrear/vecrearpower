package com.fidz.entr.app.model;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@Document(collection = "GeoStatusHistory")   
@JsonIgnoreProperties(value = { "target" })
public class GeoStatusHistory {
	
	@DateTimeFormat
	private Date createdDate;
	
	private String userId;
	
    private String username;	
	
    private String address;
    
    private String deviceId;
	
	private String geoStatus;

	public GeoStatusHistory(Date createdDate, String userId, String username, String address, String deviceId,
			String geoStatus) {
		super();
		this.createdDate = createdDate;
		this.userId = userId;
		this.username = username;
		this.address = address;
		this.deviceId = deviceId;
		this.geoStatus = geoStatus;
	}

	@Override
	public String toString() {
		return "GeoStatusHistory [createdDate=" + createdDate + ", userId=" + userId + ", username=" + username
				+ ", address=" + address + ", deviceId=" + deviceId + ", geoStatus=" + geoStatus + "]";
	}



	

}
