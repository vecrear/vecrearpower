package com.fidz.entr.app.awsconfig.model;

import java.util.List;


import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Group;
import com.fidz.entr.app.config.UserData;

import lombok.Data;
@Data
@Document(collection = "awsInfo")
@JsonIgnoreProperties(value = { "target" })
public class AwsInfo {
	
	protected String accessKey;
	protected String secretKey ;
	protected String bucketName ;
	protected String region ;
	protected String schemaName ;
	protected String geoAddressKey ;
	protected String geoDistKey;
	protected String smsUrl ;
	protected String geoKey;
	
	
	

	public AwsInfo(String accessKey, String secretKey, String bucketName) {
		super();
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		this.bucketName = bucketName;
	}
	
	public AwsInfo(String accessKey, String secretKey, String bucketName,String region) {
		super();
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		this.bucketName = bucketName;
		this.region = region;
	}

	public AwsInfo() {
		super();
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		this.bucketName = bucketName;
		this.region = region;
	}

	@Override
	public String toString() {
		return "AwsInfo [accessKey=" + accessKey + ", secretKey=" + secretKey + ", bucketName=" + bucketName
				+ ", region=" + region + ", schemaName=" + schemaName + ", geoAddressKey=" + geoAddressKey
				+ ", geoDistKey=" + geoDistKey + ", smsUrl=" + smsUrl + ", geoKey=" + geoKey + "]";
	}

	
	
	
	
	

}
