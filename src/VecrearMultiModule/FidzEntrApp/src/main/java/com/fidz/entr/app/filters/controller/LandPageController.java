package com.fidz.entr.app.filters.controller;

import java.text.ParseException;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.service.LandingPageService;
import com.fidz.entr.app.model.Attendance;

@CrossOrigin(maxAge = 3600)
@RestController("FEALaandingPageController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/filter")
public class LandPageController {

	@Autowired
	LandingPageService landingPageService;
	
	
	@PostMapping("landingpage/id/{userid}")
	 public JSONObject getlandingPage(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="userid")String userid) throws ParseException {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return landingPageService.getlandingPage(schemaName,userid);
}
	@PostMapping("landingpage/attendance/{userid}")
	public List<Attendance> getbyUserlatestFive(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="userid")String userid) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return landingPageService.getbyUserlatestFive(schemaName,userid);
	}
	
}
