package com.fidz.entr.app.awsconfig.service;

import static org.hamcrest.CoreMatchers.allOf;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.awsconfig.model.AwsInfo;

import com.fidz.entr.app.repository.AwsInfoRepository;
import com.fidz.entr.base.model.City;
import com.fidz.services.authentication.security.EncriptionUtil;

@Service("FEAAwsInfoService")
public class AwsInfoService {
	
	
	private GenericAppINterface<AwsInfo> genericINterface;
	@Autowired
	public AwsInfoService(GenericAppINterface<AwsInfo> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	@Autowired
	private AwsInfoRepository awsInfoRepository;

	public List<AwsInfo> getAllInfo(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findAll(AwsInfo.class);
	}

	public AwsInfo createInfo(@Valid AwsInfo info,String SchemaName) {
		 AwsInfo awsInfo = null;	
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(SchemaName);
		 awsInfo = this.awsInfoRepository.save(info);
		 return awsInfo;
	}

	public AwsInfo createawsInfo(@Valid AwsInfo info, String schemaName) {
		 AwsInfo awsInfo = null;
		 
		 String encryptedAccessKey = null;
		 String encryptedSecretKey = null;
		 String encryptedBucketName = null;
		 String encryptedRegion = null;
		 
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 
		 encryptedAccessKey = new EncriptionUtil().encrypt(info.getAccessKey(), new EncriptionUtil().getSecurityKey(schemaName));
		 encryptedSecretKey = new EncriptionUtil().encrypt(info.getSecretKey(), new EncriptionUtil().getSecurityKey(schemaName));
		 encryptedBucketName = new EncriptionUtil().encrypt(info.getBucketName(), new EncriptionUtil().getSecurityKey(schemaName));
		 encryptedRegion    = new EncriptionUtil().encrypt(info.getRegion(), new EncriptionUtil().getSecurityKey(schemaName));
		 Constant.LOGGER.info("encryptedAccessKey ----	"+    encryptedAccessKey);
		 Constant.LOGGER.info("encryptedSecretKey ----	"+    encryptedSecretKey);
		 Constant.LOGGER.info("encryptedBucketName ----	"+   encryptedBucketName);
		 Constant.LOGGER.info("encryptedRegion ----		"+       encryptedRegion);
		 
		 Constant.LOGGER.info("info ----				"+ info.toString());
		 
		 info.setAccessKey(encryptedAccessKey);
		 info.setSecretKey(encryptedSecretKey);
		 info.setBucketName(encryptedBucketName);
		 info.setRegion(encryptedRegion);
		 info.setSchemaName(schemaName);
		 Constant.LOGGER.info("info ----				"+ info.toString());
		 
		 
		 String dcptAccessKey = null;
		 String dcptSecretKey = null;
		 String dcptBucketName = null;
		 String dcptRegion = null;
		 
		 dcptAccessKey = new EncriptionUtil().decrypt(encryptedAccessKey, new EncriptionUtil().getSecurityKey(schemaName));
		 dcptSecretKey = new EncriptionUtil().decrypt(encryptedSecretKey, new EncriptionUtil().getSecurityKey(schemaName));
		 dcptBucketName = new EncriptionUtil().decrypt(encryptedBucketName, new EncriptionUtil().getSecurityKey(schemaName));
		 dcptRegion    = new EncriptionUtil().decrypt(encryptedRegion, new EncriptionUtil().getSecurityKey(schemaName));
		 
		 
		 Constant.LOGGER.info("dcptAccessKey ----after				"+ dcptAccessKey);
		 Constant.LOGGER.info("dcptSecretKey ----after				"+ dcptSecretKey);
		 Constant.LOGGER.info("dcptBucketName ----after				"+ dcptBucketName);
		 Constant.LOGGER.info("dcptRegion ----after				"+ dcptRegion);
		 
		 awsInfo = this.awsInfoRepository.save(info);
		 return awsInfo;
	}

	public AwsInfo decrptedInfo(@Valid AwsInfo info, String schemaName) {
		 AwsInfo awsInfo = new AwsInfo();
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		
		 String dcptAccessKey = null;
		 String dcptSecretKey = null;
		 String dcptBucketName = null;
		 String dcptRegion = null;
		 
		 dcptAccessKey 	= new EncriptionUtil().decrypt(info.getAccessKey(), new EncriptionUtil().getSecurityKey(schemaName));
		 dcptSecretKey  = new EncriptionUtil().decrypt(info.getSecretKey(), new EncriptionUtil().getSecurityKey(schemaName));
		 dcptBucketName = new EncriptionUtil().decrypt(info.getBucketName(), new EncriptionUtil().getSecurityKey(schemaName));
		 dcptRegion     = new EncriptionUtil().decrypt(info.getRegion(), new EncriptionUtil().getSecurityKey(schemaName));
		 	 
		 awsInfo.setAccessKey(dcptAccessKey);
		 awsInfo.setSecretKey(dcptSecretKey);
		 awsInfo.setBucketName(dcptBucketName);
		 awsInfo.setRegion(dcptRegion);
		
		 Constant.LOGGER.info("dcptAccessKey ----after				"+ dcptAccessKey);
		 Constant.LOGGER.info("dcptSecretKey ----after				"+ dcptSecretKey);
		 Constant.LOGGER.info("dcptBucketName ----after				"+ dcptBucketName);
		 Constant.LOGGER.info("dcptRegion ----after				"+ dcptRegion);
		 Constant.LOGGER.info("dcptRegion ----after				"+ awsInfo.toString());
		return awsInfo;
		 
	}

	public List<AwsInfo> getAWSnfo(@Valid AwsInfo info, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<AwsInfo> awsInfo = null;
		Query query = new Query();
		query.addCriteria(Criteria.where("schemaName").is(schemaName));
		awsInfo= this.genericINterface.find(query, AwsInfo.class);
		return awsInfo;
	}
	
	public List<AwsInfo> getAWSnfoObject(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<AwsInfo> awsInfo = null;
		Query query = new Query();
		query.addCriteria(Criteria.where("schemaName").is(schemaName));
		awsInfo= this.genericINterface.find(query, AwsInfo.class);
		return awsInfo;
	}


	


}
