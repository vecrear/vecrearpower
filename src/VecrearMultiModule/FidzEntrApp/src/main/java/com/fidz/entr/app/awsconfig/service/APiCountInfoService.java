package com.fidz.entr.app.awsconfig.service;


import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.awsconfig.model.APiCountInfo;
import com.fidz.entr.app.awsconfig.model.APiCountLimit;
import com.fidz.entr.app.awsconfig.model.AwsInfo;
import com.fidz.entr.app.exception.ApplicationConfigNotFoundException;
import com.fidz.entr.app.exception.CustomerNotFoundException;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.model.GeoFence;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.repository.ApiCountInfoRepository;
import com.fidz.entr.app.repository.ApiCountLimitRepository;

@Service("FEAAPiCountInfoService")
public class APiCountInfoService {
	
	
	private GenericAppINterface<APiCountInfo> genericINterface;

	@Autowired
	public APiCountInfoService(GenericAppINterface<APiCountInfo> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	
	
	@Autowired
	private ApiCountInfoRepository apiCountInfoRepository;
	
	
	@Autowired
	private ApiCountLimitRepository apiCountLimitRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired 
	AwsInfoService awsInfoService;
	
	@Autowired
	APiCountMaxLimitService aPiCountMaxLimitService;
	
	public List<APiCountInfo> getAllInfo(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		
		return this.genericINterface.findAll(APiCountInfo.class);
	}
	public APiCountInfo createInfo(@Valid APiCountInfo info, String schemaName) {
			APiCountInfo infoAPicount = null;	
		   MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		   infoAPicount = this.apiCountInfoRepository.save(info);
		  return infoAPicount;
	}
	public Map<String, String> getAPiCountInfo(@Valid APiCountInfo info, String urltype) throws ApiCountInfoNotFoundException {
		String resultString = null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		String schemaName = "VTrackSuperAdmin";
		List<AwsInfo> awsInfolist = awsInfoService.getAllInfo("VTrackSuperAdmin");
		
		
		
		Map<String, String> response = new HashMap<String, String>();
		
		List<APiCountInfo> apiCountInfoList = null;
		APiCountInfo aPiCountInfo = null;
		APiCountLimit aPiCountLimitObject = null;
		
		if(urltype == null || urltype.isEmpty() ) {
			Constant.LOGGER.info("urltype---------------------");
			resultString = "Please provide your API URL type";
			response.put("result", resultString);
	  		return response;
		}
		
		if(info.getCompanyId() != null) {
			try {
				 aPiCountLimitObject =  aPiCountMaxLimitService.getAPiCountLimitbycompanyId(info.getCompanyId(), schemaName);
				 if (aPiCountLimitObject == null) {
					 	resultString = "Company info is not available in the database";
						response.put("result", resultString);
				  		return response;
				 }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		Constant.LOGGER.info("APiCountInfo info---------------------"+info.toString());
		
		if(info.getUserId() != null) {
			Constant.LOGGER.info("started ---"+info.getUserId());
			
			try {
				Query query = new Query();
				//	APiCountInfo aPiCountInfo = this.genericINterface.findById(info.getUserId(), APiCountInfo.class);
					query.addCriteria(Criteria.where("userId").in(info.getUserId()));
					apiCountInfoList = this.mongoTemplate.find(query, APiCountInfo.class);
				
					if(apiCountInfoList.size() == 0) {
						Constant.LOGGER.info("aPiCountInfo aPiCountInfo----aPiCountInfo--------------"+apiCountInfoList.size());
						aPiCountInfo =  this.apiCountInfoRepository.save(info);	
						Constant.LOGGER.info("aPiCountInfo info-----------------after----"+aPiCountInfo.toString());
						
					}else {
						aPiCountInfo = this.genericINterface.findById(info.getUserId(), APiCountInfo.class);
					}
					
					int count = 0;
					
					APiCountInfo aPiCountInfoUpdated = null;
					if(urltype.equals("SMSAPI")) {
						count = aPiCountInfo.getSmsUrlCount();
						aPiCountInfo.setSmsUrlCount(count +1);
						aPiCountInfo.setUserId(info.getUserId());
						aPiCountInfoUpdated = this.apiCountInfoRepository.save(aPiCountInfo);
						awsInfolist.get(0).getSmsUrl();
						//resultSting = aPiCountInfoUpdated.getSmsUrl();
						if(aPiCountLimitObject.getMaxiSmsUrlCount() <= aPiCountInfoUpdated.getSmsUrlCount()) {
							
							resultString = "Your SMS API reached Maximum Limit please contact Admin";
							response.put("result", resultString);
					  		return response;
						}
						resultString =awsInfolist.get(0).getSmsUrl();
						Constant.LOGGER.info("APiCountInfo info-----resultSting----------------"+resultString);
						
					}else if(urltype.equals("GEOADDRESS")) {
						count = aPiCountInfo.getGeoAddresscount();
						aPiCountInfo.setGeoAddresscount(count +1);
						aPiCountInfo.setUserId(info.getUserId());
						aPiCountInfoUpdated = this.apiCountInfoRepository.save(aPiCountInfo);
						//resultSting = aPiCountInfoUpdated.getGeoAddressKey();
//						if(aPiCountLimitObject.getMaxiGoAddresscount() <= aPiCountInfoUpdated.getGeoAddresscount()) {
//							
//							resultString = "Your GEOADDRESS API reached Maximum Limit please contact Admin";
//							response.put("result", resultString);
//					  		return response;
//						}
						resultString = awsInfolist.get(0).getGeoAddressKey();
						Constant.LOGGER.info("APiCountInfo info-----resultSting----------------"+resultString);
						
					}else if (urltype.equals("GEODIST")) {
						count = aPiCountInfo.getGeoDistCount();
						aPiCountInfo.setGeoDistCount(count +1);
						aPiCountInfo.setUserId(info.getUserId());
						aPiCountInfoUpdated = this.apiCountInfoRepository.save(aPiCountInfo);
						//resultSting = aPiCountInfoUpdated.getGeoDistKey();
//						if(aPiCountLimitObject.getMaxiGeoDistCount() <= aPiCountInfoUpdated.getGeoDistCount()) {
//							
//							resultString = "Your GEODIST API reached Maximum Limit please contact Admin";
//							response.put("result", resultString);
//					  		return response;
//						}
						resultString = awsInfolist.get(0).getGeoDistKey();;
						Constant.LOGGER.info("APiCountInfo info-----resultSting----------------"+resultString);
						
					}else if(urltype.equals("GEOMAP")) {
						count = aPiCountInfo.getVecMapKeycount();
						aPiCountInfo.setVecMapKeycount(count +1);
						aPiCountInfo.setUserId(info.getUserId());
						aPiCountInfoUpdated = this.apiCountInfoRepository.save(aPiCountInfo);
						//resultSting = aPiCountInfoUpdated.getVecMapkey();
//						if(aPiCountLimitObject.getMaxVecMapKeycount() <= aPiCountInfoUpdated.getVecMapKeycount()) {
//							
//							resultString = "Your GEOMAP API reached Maximum Limit please contact Admin";
//							response.put("result", resultString);
//					  		return response;
//						}
						resultString = awsInfolist.get(0).getGeoKey();
						Constant.LOGGER.info("APiCountInfo info-----resultSting----------------"+resultString);
						
					}else {
						resultString = "Please provide your Correct API URL type";
						Constant.LOGGER.info("APiCountInfo info-----resultSting----------------"+resultString);
					}
					
			}catch (Exception e) {
				// TODO: handle exception
			}
		}else {
		//	APiCountInfo aPiCountInfo =  this.apiCountInfoRepository.save(info);
			resultString = "Please send your  User details";
			response.put("result", resultString);
			Constant.LOGGER.info("APiCountInfo info-----for first time user----------------");
		}
		
		
  		response.put("result", resultString);
  		return response;
	}
	public Map<String, String> getAPiCountInfoByCompany(String comanyid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Map<String, String> response = new HashMap<String, String>();
		List<APiCountInfo> listAPiCountInfo = null;
		Constant.LOGGER.info("Inside  APiCountInfo");
        Query q = new Query();
        int smsUrlCount = 0;
        int geoAddresscount = 0;
        int geoDistCount = 0;
        int vecMapKeycount = 0;
        
        try {
        	q.addCriteria(Criteria.where("companyId").in(comanyid));
        	listAPiCountInfo = this.genericINterface.find(q,APiCountInfo.class );
        	Constant.LOGGER.info("listAPiCountInfo-------------"+listAPiCountInfo.size());
        	
        	if(listAPiCountInfo.size() > 0) {
        		
        		for (int i=0; i<listAPiCountInfo.size();i++) {
        			
        			smsUrlCount = smsUrlCount+listAPiCountInfo.get(i).getSmsUrlCount();
        		
        		      geoAddresscount = geoAddresscount+listAPiCountInfo.get(i).getGeoAddresscount();
        		      geoDistCount = geoDistCount+listAPiCountInfo.get(i).getGeoDistCount();
        		      vecMapKeycount = vecMapKeycount+listAPiCountInfo.get(i).getVecMapKeycount();
        			 
        		}
        		
        	}
        	
        }catch(Exception ex) {
			Constant.LOGGER.error("listAPiCountInfo-------------------");

        }
		
        response.put("smsUrlCount", String.valueOf(smsUrlCount));
        response.put("geoAddresscount", String.valueOf(geoAddresscount));
        response.put("geoDistCount", String.valueOf(geoDistCount));
        response.put("vecMapKeycount", String.valueOf(vecMapKeycount));
  		return response;
	}
	public Map<String, String> getAPiCountInfoByDept(String deptid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Map<String, String> response = new HashMap<String, String>();
		List<APiCountInfo> listAPiCountInfo = null;
		Constant.LOGGER.info("Inside  APiCountInfo");
        Query q = new Query();
        int smsUrlCount = 0;
        int geoAddresscount = 0;
        int geoDistCount = 0;
        int vecMapKeycount = 0;
        
        try {
        	q.addCriteria(Criteria.where("departmentId").in(deptid));
        	listAPiCountInfo = this.genericINterface.find(q,APiCountInfo.class );
        	Constant.LOGGER.info("listAPiCountInfo-------------"+listAPiCountInfo.size());
        	
        	if(listAPiCountInfo.size() > 0) {
        		
        		for (int i=0; i<listAPiCountInfo.size();i++) {
        			
        			smsUrlCount = smsUrlCount+listAPiCountInfo.get(i).getSmsUrlCount();
        		
        		      geoAddresscount = geoAddresscount+listAPiCountInfo.get(i).getGeoAddresscount();
        		      geoDistCount = geoDistCount+listAPiCountInfo.get(i).getGeoDistCount();
        		      vecMapKeycount = vecMapKeycount+listAPiCountInfo.get(i).getVecMapKeycount();
        			 
        		}
        		
        	}
        	
        }catch(Exception ex) {
			Constant.LOGGER.error("listAPiCountInfo-------------------");

        }
		
        response.put("smsUrlCount", String.valueOf(smsUrlCount));
        response.put("geoAddresscount", String.valueOf(geoAddresscount));
        response.put("geoDistCount", String.valueOf(geoDistCount));
        response.put("vecMapKeycount", String.valueOf(vecMapKeycount));
  		return response;
	}



}
