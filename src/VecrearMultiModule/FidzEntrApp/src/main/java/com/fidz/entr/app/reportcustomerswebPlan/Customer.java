package com.fidz.entr.app.reportcustomerswebPlan;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Contact;
import com.fidz.base.model.PersonName;
import com.fidz.entr.app.model.CustomerType;
import com.fidz.entr.app.reportcustomersweb.City;
import com.fidz.entr.app.reportcustomersweb.Company;
import com.fidz.entr.app.reportcustomersweb.Country;
import com.fidz.entr.app.reportcustomersweb.CustomerCategory;
import com.fidz.entr.app.reportcustomersweb.Department;
import com.fidz.entr.app.reportcustomersweb.Region;
import com.fidz.entr.app.reportcustomersweb.SimplifiedBase;
import com.fidz.entr.app.reportcustomersweb.State;
import com.fidz.entr.app.reportcustomersweb.User;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Customer extends SimplifiedBase {
	@Id
	protected String id;

	@NotNull
	protected String name;

	protected String customerId;

	protected PersonName contactName;

	protected Contact contact;

	protected Address address;

	// @NotNull
	// @DBRef(lazy = true)
	// protected CustomerTypeField customerType;

	protected String customerDepartment;

	@NotNull
	@DBRef(lazy = true)
	protected Company company;

	@NotNull
	@DBRef(lazy = true)
	protected com.fidz.entr.base.model.Department department;


	@DBRef(lazy = true)
	protected City city;

	protected CustomerType customerType;

}
