package com.fidz.entr.app.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.FidzEntrApplication;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.model.UserDepartmentConfig;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.app.repository.UserRoleRepository;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Device;


@Service("FEBUserRoleService")
public class UserRoleService {
	
	private static final String GET_URL = "https://localhost:8188/start";
	private static final String GET_URLs = "https://localhost:8188/stop";


	private GenericAppINterface<UserRole> genericINterface;
	@Autowired
	public UserRoleService(GenericAppINterface<UserRole> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private static String name = "UserRole";
	
    public List<UserRole> getAllUserRoles(String schemaName) throws IOException {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	Constant.LOGGER.info("Inside getAllUserRoles UserRoleService.java");
	   	List<UserRole> userRoles=null;
	   	try {
	   	userRoles=this.genericINterface.findAll(UserRole.class);
	   	}catch(Exception ex) {
	   	Constant.LOGGER.error("Error while getting data from getAllUserRoles UserRoleService.java"+ex.getMessage());
	       }
	   	Constant.LOGGER.info("**************************************************************************");
	   	Constant.LOGGER.info("successfully fetched data from getAllUserRoles UserRoleService.java");
	   	return userRoles;
    }
	

    public UserRole createUserRole(UserRole userRole) {
    	UserRole userRoles=null;
      	
    		Constant.LOGGER.info(" Inside UserRoleService.java Value for insert UserRole record: createUserRole  :: " + userRole);
    		String schemaName=userRole.getSchemaName();
    	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	    
    	    //--------------------TO CHECK THE SAME IN SAME UserRole--------IN SAME Department
    	    
    	    Query query = new Query();
        	List<UserRole> usList = null;

        		query.addCriteria(Criteria.where("department.id").is(userRole.getDepartment().getId()));
        	
        		usList= this.genericINterface.find(query, UserRole.class);	
        		if(usList.size()>0) {
        			List<String> names = usList.stream().map(p->p.getName()).collect(Collectors.toList());
            		Constant.LOGGER.info("Country List data"+names);
           	   	 
        	   		if (names.stream().anyMatch(s -> s.equals(userRole.getName())==true)) {
        	   			Constant.LOGGER.info("Inside if statement");
        	   			throw new NameFoundException(name+ExceptionConstant.SAME_NAME_SAME_USER_ROLE+": "+userRole.getName());
        	   	 }
        	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(userRole.getName())==true)) {
        	   				Constant.LOGGER.info("Inside else if statement");
            	   		throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+userRole.getName());
        	   		}else{
        	   			userRoles=this.userRoleRepository.save(userRole);
        	    	    
        	   		}
        			
        		}else {
        			
        			userRoles=this.userRoleRepository.save(userRole);
        		}

       return userRoles;
    }
    
    public void createUserRoles(UserRole userRole) {
        String schemaName=userRole.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        //return userRoleRepository.save(userRole);
	    this.genericINterface.saveName(userRole);
    }
    public UserRole getUserRoleById(String id, String schemaName) {
	   	Constant.LOGGER.info("Inside getUserRoleById UserRoleService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, UserRole.class);
		
    }
	
    public UserRole getUserRoleByName(String name, String schemaName) {
	   	Constant.LOGGER.info("Inside getUserRoleByName UserRoleService.java");
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.userRoleRepository.findByName(name);
		
    }
	
    public UserRole updateUserRole(String id, UserRole userRole) {
        String schemaName=userRole.getSchemaName();
	   	Constant.LOGGER.info("Inside updateUserRole UserRoleService.java");
    	UserRole userRoles=null;

	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    
	    Query query = new Query();
    	List<UserRole> usList = null;

    		query.addCriteria(Criteria.where("department.id").is(userRole.getDepartment().getId()));
    	
    		usList= this.genericINterface.find(query, UserRole.class);
    		UserRole ctr = getUserRoleById(id, schemaName);
    		if(usList.size()>0) {
    			List<String> names = usList.stream().map(p->p.getName()).collect(Collectors.toList());
        		Constant.LOGGER.info("Country List data"+names);
       	   	 names.remove(ctr.getName());
    	   		if (names.stream().anyMatch(s -> s.equals(userRole.getName())==true)) {
    	   			Constant.LOGGER.info("Inside if statement");
    	   			throw new NameFoundException(name+ExceptionConstant.SAME_NAME_SAME_USER_ROLE+": "+userRole.getName());
    	   	 }
    	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(userRole.getName())==true)) {
    	   				Constant.LOGGER.info("Inside else if statement");
        	   		throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+userRole.getName());
    	   		}else{
    	   			userRole.setId(id);
    	   			userRoles=this.userRoleRepository.save(userRole);
    	    	    
    	   		}
    	   	 
    		}else {
    			userRole.setId(id);
    			userRoles=this.userRoleRepository.save(userRole);
    		}
	   // userRole.setId(id);
	    return userRoles;
       
    }

   
  
    public Map<String, String> deleteUserRole(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	Constant.LOGGER.info("Inside deleteUserRole UserRoleService.java");
        this.userRoleRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserRole deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftUserRole(String id, String schemaName, TimeUpdate timeUpdate){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	Constant.LOGGER.info("Inside deleteSoftUserRole UserRoleService.java");
      UserRole userRole = this.genericINterface.findById(id, UserRole.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		userRole.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		userRole.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		userRole.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(userRole);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserRole deleted successfully");
		return response;

	}
    public List<UserRole> streamAllUserRoles() {
        return userRoleRepository.findAll();
    }

//TO get all user roles paginated
	public List<UserRole> getAllUserRolesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserRolesPaginated UserRoleService.java");
    	Query query = new Query();
    	List<UserRole> userRoleServices =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	userRoleServices= this.genericINterface.find(query, UserRole.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserRolesPaginated UserRoleService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllUserRolesPaginated "+query);
        return userRoleServices;
	}


	public List<UserRole> getAllUserRolesByDepartment(String deptid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserRolesByDepartment UserRoleService.java");
    	Query query = new Query();
    	List<UserRole> userRoles =null;

    	try {
    	query.addCriteria(Criteria.where("department.id").in(deptid));
    	userRoles= this.genericINterface.find(query, UserRole.class);	
    	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserRolesByDepartment UserRoleService.java "+ex.getMessage());
        }
    	
        return userRoles;
	}


	public List<UserRole> getAllUserRolesPaginatedSearch(int pageNumber, int pageSize, String schemaName, @Valid String text) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<UserRole>  userrole = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
       Constant.LOGGER.info("Inside getAllUserRolesPaginatedSearch DepartmentService.java");
    	try {
    		
    		textIndex = new TextIndexDefinitionBuilder()
    				.onAllFields()
    				.build();
    		mongoTemplate.indexOps(UserRole.class).ensureIndex(textIndex);
    	    criteria = TextCriteria.forDefaultLanguage()
    	    		.matchingAny(text).caseSensitive(false).diacriticSensitive(false);
              query = TextQuery.queryText(criteria)
    	    		  .sortByScore()
    	    		  .with(PageRequest.of(pageNumber, pageSize));
              userrole = mongoTemplate.find(query, UserRole.class);
    		
    					
	}catch(Exception ex) {
    	Constant.LOGGER.error("Inside departmentTextSearch CompanyService.java"+ ex.getMessage());

	}
		return userrole;
	}
	
	


}
