package com.fidz.entr.app.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Device;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.repository.DeviceRepository;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.GeoFence;
import com.fidz.entr.app.service.GeofenceService;
import com.fidz.services.location.exception.GeofenceNotFoundException;

@CrossOrigin(maxAge = 3600)
@RestController
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/geofences")

public class GeofenceController {
	@Autowired
	private GeofenceService geofenceService;
	
	@Autowired
	private DeviceRepository deviceRepository;
	
	@Value("${geofenceservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all")
    public List<GeoFence> getAllGeofence(@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.getAllGeofence(schemaName);
    }
	
	@PostMapping("search/{text}/byname")
    public List<GeoFence> getAllGeofenceByNameSearch(@PathVariable(value = "text") String text,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.getAllGeofenceByNameSearch(text,schemaName);
    }
	
	
	@PostMapping("/geofenceOwner/{geofenceOwnerId}")
    public List<GeoFence> getAllGeofenceByOwner(@PathVariable(value = "geofenceOwnerId") String geofenceOwnerId,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.getAllGeofenceByOwner(geofenceOwnerId,schemaName);
    }
	
    @PostMapping
    public GeoFence createGeoFence(@Valid @RequestBody GeoFence geofence) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.createGeoFence(geofence);
        //return geofenceRepository.save(geofence);
    }
    @PostMapping("/creates")
    public void createGeoFences(@Valid @RequestBody GeoFence geofence) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		geofenceService.createGeoFences(geofence);
    }
    
    @PostMapping("/get/id/{id}")
    public GeoFence getGeoFenceById(@PathVariable(value = "id") final String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.getGeoFenceById(id, schemaName);
		
    }
    
    @PostMapping("/get/id/device/{deviceid}")
    public List<GeoFence> getGeoFenceByDeviceIds(
    		@PathVariable(value = "deviceid") final String deviceId, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.getGeoFenceByDeviceIds(deviceId, schemaName);
		
    }
    
    @PostMapping("/get/id/device/deviceid/{deviceid}")
    public List<GeoFence> getGeoFenceByDeviceId(
    		@PathVariable(value = "deviceid") final String deviceId, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.getGeoFenceByDeviceId(deviceId, schemaName);

    }
    
    @PostMapping("/get/id/user/{userid}")
    public List<GeoFence> getGeoFenceByUserid(
    		@PathVariable(value = "userid") final String deviceId, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.getGeoFenceByUserId(deviceId, schemaName);

    }
    //Get deviceList which does not have geofence.
    @PostMapping("/get/devices")
    public Stream<Device> getDeviceListNoGeofence(@RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return geofenceService.getDeviceListNoGeofence(schemaName);
    }

    @PostMapping("/update/id/{id}")
    public GeoFence updateGeoFenceData(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody GeoFence geoFence) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.updateGeoFenceData(id, geoFence);
    }
	
	
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<GeoFence> streamAllGeoFenceData() {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.streamAllGeoFenceData();
    }
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteGeoFenceData(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return geofenceService.deleteGeoFenceData(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteGeoFenceData(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return geofenceService.deleteSoftGeoFenceData(id, schemaName, timeUpdate);
    }
   
 // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("Geofence details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(GeofenceNotFoundException ex) {
    	System.out.println("property value******"+property);
		String message="Geofenceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return ResponseEntity.notFound().build();
    }
}
