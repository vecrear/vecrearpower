package com.fidz.entr.app.exception;

public class UserFeedBackNoteFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4307672966858246454L;
	
	public UserFeedBackNoteFoundException(String username) {
		super("UserFeedBack not found with id/name " + username);
	}

}
