package com.fidz.entr.app.filters.model;

import lombok.Data;


@Data
public class PostponedActivity {

	private String totalPostponeddActivity;
	
	private Object activityStatus;

	public PostponedActivity(String totalPostponeddActivity, String activityStatus) {
		super();
		this.totalPostponeddActivity = totalPostponeddActivity;
		this.activityStatus = activityStatus;
	}

	@Override
	public String toString() {
		return "PostponedActivity [totalPostponeddActivity=" + totalPostponeddActivity + ", activityStatus="
				+ activityStatus + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
}
