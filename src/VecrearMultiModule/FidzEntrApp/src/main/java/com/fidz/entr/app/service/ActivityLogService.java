package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityLog;
import com.fidz.entr.app.repository.ActivityLogRepository;
import com.fidz.entr.base.model.Facility;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FEAActivityLogService")
public class ActivityLogService {

	private GenericAppINterface<ActivityLog> genericINterface;
	@Autowired
	public ActivityLogService(GenericAppINterface<ActivityLog> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	public ActivityLogService() {
		
	}

	@Autowired
	private ActivityLogRepository  activityLogRepository;

	public List<ActivityLog> getAllActivityLog( String schemaName){
		Constant.LOGGER.info("Inside getAllActivityLog ActivityLogService.java");
    	List<ActivityLog> activityLogs=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	activityLogs=this.genericINterface.findAll(ActivityLog.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllActivityLog ActivityLogService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllActivityLog ActivityLogService.java");
    	return activityLogs;
	}

	public ActivityLog createActivityLog(ActivityLog activityLog) {
		ActivityLog activityLogs=null;
		try{
			Constant.LOGGER.debug(" Inside ActivityLogService.java Value for insert ActivityLog record:createActivityLog :: " + activityLog);
			System.out.println("@#######################" + activityLog.toString());
			String schemaName = activityLog.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			activityLogs=this.activityLogRepository.save(activityLog);
		}catch(Exception e) {
			Constant.LOGGER.debug(" Inside ActivityLogService.java Value for insert ActivityLog record:createActivityLog :: " + activityLog);
		}
		return activityLogs;
	}


	public List<ActivityLog> createActivityLogForList(List<ActivityLog> activityLog) {
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@" + activityLog.toString());
		//activityLog.forEach(schema -> schema.getSchemaName());

		for (ActivityLog activityLogs : activityLog)
			// activityLogs.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(activityLogs.getSchemaName());
		// return activityLogRepository.save(activityLog);
		return activityLogRepository.saveAll(activityLog);
	}
	public void createActivityLogForLists(List<ActivityLog> activityLog) {
		System.out.println("@#######################" + activityLog.toString());
		for (ActivityLog activityLogs : activityLog)
		
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(activityLogs.getSchemaName());
		this.genericINterface.saveName(activityLog);
		
	}

	
	/*public Flux<ActivityLog> createActivityLogForList(List<ActivityLog> activityLog){
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@"+activityLog.toString());
		//return activityLogRepository.saveAll(activityLog);
		return activityLogRepository.save(activityLog);
	}*/
	

	public ActivityLog getActivityLogById(String id, String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, ActivityLog.class);
		
	}
	
    public ActivityLog updateActivityLog(String id, ActivityLog activityLog) {
    	String schemaName = activityLog.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		activityLog.setId(id);
		return this.activityLogRepository.save(activityLog);
        
    }

    
  //hard delete
  	public Map<String, String> deleteActivityLog(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.activityLogRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("ActivityLog", "Company deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftActivityLog(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		ActivityLog activityLog = this.genericINterface.findById(id, ActivityLog.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		activityLog.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		activityLog.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		activityLog.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(activityLog);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "ActivityLog deleted successfully");
  		return response;

  	}
     
    public List<ActivityLog> streamAllActivityLogs() {
        return activityLogRepository.findAll();
    }

	
	

	


}
