package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.base.model.Device;

@Repository("fidzApprepository")
public interface DeviceRepository extends MongoRepository<Device, String>{
	//public Mono<Device> findByDeviceId(String deviceId);
	public Device findByDeviceId(String deviceId);
	
}
