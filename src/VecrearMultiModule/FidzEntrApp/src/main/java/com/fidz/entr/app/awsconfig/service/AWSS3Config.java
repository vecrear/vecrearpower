package com.fidz.entr.app.awsconfig.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.fidz.base.validator.Constant;

@Configuration
public class AWSS3Config {
	
    // Access key id will be read from the application.properties file during the application intialization.
    private String accessKeyId ;
    // Secret access key will be read from the application.properties file during the application intialization.
    private String secretAccessKey; 
    // Region will be read from the application.properties file  during the application intialization.
   
    private String region = "ap-south-1";
 
    @Bean
    public AmazonS3 getAmazonS3Cient() {
        final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
        
        Constant.LOGGER.info("-----------------------basicAWSCredentials"+basicAWSCredentials.toString());
        // Get AmazonS3 client and return the s3Client object.
        return AmazonS3ClientBuilder
                .standard()
                .withRegion(Regions.fromName(region))
                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
                .build();
    }

}
