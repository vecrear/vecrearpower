package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "attendance")
@JsonIgnoreProperties(value = { "target" })
public class Attendance extends Base{
	
	@Id
	protected String id;
	
	@NotNull
	@DBRef(lazy = true)
	protected User user;
	
	//protected long calenderDatestamp;
	
	//protected long punchInDateTimeStamp;
    protected Date calenderDatestamp;
	
	protected Date punchInDateTimeStamp;
	
	@DBRef(lazy = true)
	protected Location punchInLocation;
	
	//protected long punchOutDateTimeStamp;
	protected Date punchOutDateTimeStamp;
	
	@DBRef(lazy = true)
	protected Location punchOutLocation;
	
	protected String workingDuration;
	
	protected List<LogOffTime> logOffTime;
	
	protected List<IdleTime> idleTime;
	

	protected boolean punchInPerTheDay = false;
	
	public static Attendance instances = new Attendance();

	public static Attendance getInstances() {
		return instances;
	}

	public static void setInstances(Attendance instances) {
		Attendance.instances = instances;
	}

	

	


	
}
