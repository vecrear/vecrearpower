package com.fidz.entr.app.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;

import com.fidz.base.validator.Constant;

import com.fidz.services.location.model.GpsData;

import com.fidz.services.location.repository.GpsDataRepository;

@Service
public class GeoAddressService {

	 String urlPrefix ="https://maps.googleapis.com/maps/api/geocode/json?";
	 
	 public GeoAddressService() {
		 
	 }
	
	public String reverseGeoCoding(double latitude, double longitude) {
		Constant.LOGGER.info("Inside GeoAddressController.java"+latitude);
		Constant.LOGGER.info("Inside GeoAddressController.java"+longitude);
		 String urlPrefix ="https://maps.googleapis.com/maps/api/geocode/json?";
		System.out.println("\nLatitude: " + latitude +"\nLongitude: " + longitude);
		String address = "";
		String status = "";
		String url = "https://maps.googleapis.com/maps/api/geocode/json?"+"latlng="+latitude+","+longitude+"&key=AIzaSyDNMKPzkkKtjipjb3YIyXEEXh-Piq7qbbU";
		System.out.println("\nurl: " + url);
		try {
			URL uriAddress = new URL(url);
			URLConnection res = uriAddress.openConnection();
			BufferedReader bufferedReader  =  new BufferedReader(new InputStreamReader(res.getInputStream()));
		    StringBuilder builder = new StringBuilder();
		    String line;
		    while ((line = bufferedReader.readLine()) != null) {
		        builder.append(line + "\n");
		    }	
		    String builderArray = builder.toString();
		    //System.out.println("\nbuilderArray: \n" + builderArray);
		    JSONObject root = (JSONObject)JSONValue.parseWithException(builderArray);
		    //System.out.println("\nresult: \n" + root);
		    JSONArray resultArray = (JSONArray) root.get("results");
		    JSONObject obj0 = (JSONObject) resultArray.get(0);
		    //System.out.println("\nobj0: \n" + obj0);
		    String formattedAddress = (String) obj0.get("formatted_address");
		    System.out.println("\nFormattedAddress: \n" + formattedAddress);
		    address = formattedAddress;
		    
		    // Accessing status
		    String stat = (String) root.get("status");
		    status = stat;
		    System.out.println("\nStatus: " + status);
		}
		catch(Exception e) {
			System.out.println("\nException: " + e);
		}
		if(status.contentEquals("ZERO_RESULTS")) {
			address = "No address";
			return address;
		}
		if(status.contentEquals("OVER_QUERY_LIMIT")) {
			address = "Crossed your quota of api calls";
			return address;
		}
		if(status.contentEquals("REQUEST_DENIED")) {
			address = "Request denied";
			return address;
		}
		if(status.contentEquals("INVALID_REQUEST")) {
			address = "Invalid query";
			return address;
		}		
		if(status.contentEquals("UNKNOWN_ERROR")) {
			address = "Server Error";
			return address;
		}		
		if(address.contentEquals("")) {
			address = "Blank Address";
			return address;
		}
		else {
			return address;
		}
	}
	
//	
//	public String getPostWithCustomHeaders(double latitude, double longitude) {
//	   
//
//	    // create headers
//	    HttpHeaders headers = new HttpHeaders();
//	    // set `accept` header
//	    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//	    // set custom header
//	    headers.set("x-request-source", "desktop");
//
//	    // build the request
//	    HttpEntity request = new HttpEntity(headers);
//
//	    // use `exchange` method for HTTP call
//	    ResponseEntity<JSONObject> response = this.restTemplate.exchange(url, HttpMethod.GET, request, Post.class, 1);
//	    if(response.getStatusCode() == HttpStatus.OK) {
//	        return response.getBody();
//	    } else {
//	        return null;
//	    }
//	}
//	
	
}
