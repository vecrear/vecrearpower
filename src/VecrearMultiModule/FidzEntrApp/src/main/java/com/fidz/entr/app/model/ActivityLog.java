package com.fidz.entr.app.model;

import java.util.Date;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "activitylog")
@JsonIgnoreProperties(value = { "target" })
public class ActivityLog extends Base {
	@Id
	protected String id;
	
	protected Map<String, Object> activityLogValues; //value is stored as Object. Key is fieldname of CustomField.

	
	
	
	
}
