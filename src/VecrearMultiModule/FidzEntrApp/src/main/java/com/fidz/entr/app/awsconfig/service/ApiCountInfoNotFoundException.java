package com.fidz.entr.app.awsconfig.service;

public class ApiCountInfoNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4053146522220579412L;
	public ApiCountInfoNotFoundException(String name) {
		super("region not found with " + name);
	}

}
