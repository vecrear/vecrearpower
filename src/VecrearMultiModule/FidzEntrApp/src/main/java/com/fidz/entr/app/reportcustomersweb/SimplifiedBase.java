package com.fidz.entr.app.reportcustomersweb;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class SimplifiedBase {
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Object status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	
	

	public SimplifiedBase() {
		super();
	}






	

	
	

}
