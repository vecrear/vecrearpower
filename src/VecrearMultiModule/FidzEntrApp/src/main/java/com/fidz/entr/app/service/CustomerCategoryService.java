package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.ApplicationNameFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.CustomerCategoryNameFoundException;
import com.fidz.entr.app.filters.model.BroadCastScheduler;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.CustomerCategory;
import com.fidz.entr.app.repository.CustomerCategoryRepository;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.Department;


@Service("FEACustomerCategoryService")
public class CustomerCategoryService {
	private GenericAppINterface<CustomerCategory> genericINterface;
	@Autowired
	public CustomerCategoryService(GenericAppINterface<CustomerCategory> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private CustomerCategoryRepository  customerCategoryRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public static String name = "Customer Category";

	public List<CustomerCategory> getAllCustomerCategories(String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCustomerCategories CustomerCategoryService.java");
    	List<CustomerCategory> customerCategories=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	customerCategories=this.genericINterface.findAll(CustomerCategory.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCustomerCategories CustomerCategoryService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCustomerCategories CustomerCategoryService.java");
    	return customerCategories;
	}
	
	public CustomerCategory createCustomerCategory(CustomerCategory customerCategory){
		Constant.LOGGER.info("Inside createCustomerCategory CustomerCategoryService.java");    
		String schemaName=customerCategory.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    
	    	
	   		 Query query = new Query();
	        	List<CustomerCategory> customerCategoryList = null;

	        		query.addCriteria(Criteria.where("company.id").is(customerCategory.getCompany().getId()));
	        	
	        		customerCategoryList= this.genericINterface.find(query, CustomerCategory.class);
	        		if(customerCategoryList.size() > 0){
	        			List<String> names = customerCategoryList.stream().map(p->p.getName()).collect(Collectors.toList());
	        			Constant.LOGGER.info("Application List data"+names);
	        			if (names.stream().anyMatch(s -> s.equals(customerCategory.getName())==true)) {
	    	    			Constant.LOGGER.info("Inside if statement");

	    					throw new NameFoundException(name+ExceptionConstant.SAME_NAME_SAME_CUSTOMER_CATEGORY+": "+customerCategory.getName());
	    	    		}else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(customerCategory.getName())==true)) {
	    	    			Constant.LOGGER.info("Inside else if statement");
	    					throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+customerCategory.getName());
	    	    		}else{
	    	    			Constant.LOGGER.info("Inside else statement with same company");
	    	    			Constant.LOGGER.info("Success Customer Category CustomerCategoryService");
	    	    		    	return this.customerCategoryRepository.save(customerCategory);
	    	    		}
	        		}else {
    	    			Constant.LOGGER.info("Inside else statement");
    	    			
    	    		    	return this.customerCategoryRepository.save(customerCategory);
	        		}
	    		
	}
	
	public void createCustomerCategorys(CustomerCategory customerCategory){
	    String schemaName=customerCategory.getSchemaName();
		Constant.LOGGER.info("Inside createCustomerCategorys CustomerCategoryService.java");    
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    
	    //return customerCategoryRepository.save(customerCategory);
	  this.genericINterface.saveName(customerCategory);
}
	public CustomerCategory getCustomerCategoryById(String id, String schemaName){
		Constant.LOGGER.info("Inside getCustomerCategoryById CustomerCategoryService.java");    
         MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 return this.genericINterface.findById(id, CustomerCategory.class);
		
	}
	
    public CustomerCategory updateCustomerCategory(String id, CustomerCategory customerCategory) {
    	 String schemaName=customerCategory.getSchemaName();
 		Constant.LOGGER.info("Inside updateCustomerCategory CustomerCategoryService.java");    
	      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	      Query query = new Query();
      	List<CustomerCategory> customerCategoryList = null;
        CustomerCategory category = null;
      		query.addCriteria(Criteria.where("company.id").is(customerCategory.getCompany().getId()));
      	
      		customerCategoryList= this.genericINterface.find(query, CustomerCategory.class);
      		CustomerCategory ctr = getCustomerCategoryById(id, schemaName);
      		if(customerCategoryList.size() > 0){
      			List<String> names = customerCategoryList.stream().map(p->p.getName()).collect(Collectors.toList());
      			names.remove(ctr.getName());
      			Constant.LOGGER.info("Application List data"+names);
      			if (names.stream().anyMatch(s -> s.equals(customerCategory.getName())==true)) {
  	    			Constant.LOGGER.info("Inside if statement");

  					throw new NameFoundException(name+ExceptionConstant.SAME_NAME_SAME_CUSTOMER_CATEGORY+": "+customerCategory.getName());
  	    		}else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(customerCategory.getName())==true)) {
  	    			Constant.LOGGER.info("Inside else if statement");
  					throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+customerCategory.getName());
  	    		}else{
		 customerCategory.setId(id);
		 category = this.customerCategoryRepository.save(customerCategory);
		 return category;
  	    		}
      		}
      	 customerCategory.setId(id);
   		 category = this.customerCategoryRepository.save(customerCategory);
   		 return category;
  	    	
    }

   
    //hard delete
  	public Map<String, String> deleteCustomerCategory(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		Constant.LOGGER.info("Inside deleteCustomerCategory CustomerCategoryService.java");    
        this.customerCategoryRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "CustomerCategory deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftCustomerCategory(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		Constant.LOGGER.info("Inside deleteSoftCustomerCategory CustomerCategoryService.java");    
        CustomerCategory customerCategory = this.genericINterface.findById(id, CustomerCategory.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		customerCategory.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		customerCategory.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		customerCategory.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(customerCategory);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "CustomerCategory deleted successfully");
  		return response;

  	}
    public List<CustomerCategory> streamAllCustomerCategories() {
        return customerCategoryRepository.findAll();
    }
  //To get all CustomerCategory paginated
   //page number starts from zero
	public List<CustomerCategory> getAllCustomersPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllCustomersPaginated CustomerCategoryService.java");
    	Query query = new Query();
    	List<CustomerCategory> customerCategories =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	customerCategories= this.genericINterface.find(query, CustomerCategory.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllCustomersPaginated CustomerCategoryService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllCustomersPaginated "+query);
        return customerCategories;	
	}
//Based on company getting customer categories
	public List<CustomerCategory> getAllCustomersByCompany(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllCustomersByCompany CustomerCategoryService.java");
    	Query query = new Query();
    	List<CustomerCategory> customerCategories =null;
    	
    	try {
    		query.addCriteria(Criteria.where("company.id").is(id));
    		customerCategories= this.genericINterface.find(query, CustomerCategory.class);
    	}catch(Exception ex) {
    	    Constant.LOGGER.error("Error occurred inside getAllCustomersByCompany CustomerCategoryService.java "+ex.getMessage());
    	}
    	return customerCategories;
	}

	public List<CustomerCategory> getAllCustomersSearchPaginated(String text, int pageNumber, int pageSize,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<CustomerCategory> customerCategories = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside getAllCustomersSearchPaginated BroadCastSchedulerService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(CustomerCategory.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			customerCategories = mongoTemplate.find(query, CustomerCategory.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside getAllCustomersSearchPaginated BroadCastSchedulerService.java" + ex.getMessage());

		}
		return customerCategories;
	}

}
