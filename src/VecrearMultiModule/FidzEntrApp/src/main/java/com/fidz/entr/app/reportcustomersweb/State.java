package com.fidz.entr.app.reportcustomersweb;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "state")
@JsonIgnoreProperties(value = { "target" })
public class State extends SimplifiedBase {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	@JsonIgnore
	protected boolean stateTagged;
	@JsonIgnore
	protected String taggedRegionName;
	@JsonIgnore
	protected String code;
	@JsonIgnore
	protected String description;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<City> cities;
	@JsonIgnore
	protected List<Object> taggedCities;

	
	

	

}
