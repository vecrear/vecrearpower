package com.fidz.entr.app.controller;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.CustomerTypeField;
import com.fidz.entr.app.service.ActivityFieldService;
import com.fidz.entr.app.service.CustomerTypeFieldService;

@CrossOrigin(maxAge = 3600)
@RestController("FEAcustomertypeController")
@RequestMapping(value="/customertypefields")
public class CustomerTypeFieldController {
	@Autowired
	private CustomerTypeFieldService  customerTypeFieldService;
	
	@PostMapping("/get/all")
	public List<CustomerTypeField> getAllcustomertypeFields(@RequestHeader(value="schemaName") String schemaName) throws UnknownHostException{
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerTypeFieldService.getAllCustomertypeFields(schemaName);
	}
	
	//Post request to get all CustomerTypeField  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<CustomerTypeField> getAllcustomertypeFieldsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerTypeFieldService.getAllcustomertypeFieldsPaginated(pageNumber, pageSize, schemaName);
	}
	
	@PostMapping
	public CustomerTypeField createCustomertypeField(@Valid @RequestBody CustomerTypeField customerTypeField){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerTypeFieldService.createCustomertypeField(customerTypeField);
	}
	@PostMapping("/creates")
	public void createCustomertypeFields(@Valid @RequestBody CustomerTypeField customerTypeField){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		customerTypeFieldService.createCustomertypeFields(customerTypeField);
	}
	@PostMapping("/get/id/{id}")
	public ResponseEntity<CustomerTypeField> getCustomertypeFieldById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerTypeFieldService.getCustomertypeFieldById(id, schemaName);
		
	}
	/*@GetMapping("/activityname/{name}")
    public Mono<Activity> getActivityByName(@PathVariable(value = "name") String name) {
		return activityService.getActivityByName(name);
    }
	*/
	@PostMapping("/update/id/{id}")
    public CustomerTypeField updateActivityField(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody CustomerTypeField activity) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerTypeFieldService.updateCustomertypeField(id, activity);
    }


	@PostMapping("/delete/id/{id}")
    public Map<String, String> deleteCustomertypeField(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerTypeFieldService.deleteCustomertypeField(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteActivityField(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return customerTypeFieldService.deleteSoftCustomertypeField(id, schemaName, timeUpdate);
    }
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<CustomerTypeField> streamAllCustomertypeFields() {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return customerTypeFieldService.streamAllCustomertypeFields();
    }

    // Exception Handling Examples
   /* @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Activity Field with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ActivityFieldNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/
	
   
	
}
