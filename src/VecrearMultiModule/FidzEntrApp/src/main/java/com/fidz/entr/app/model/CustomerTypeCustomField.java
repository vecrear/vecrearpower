package com.fidz.entr.app.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.base.model.DataType;
import com.fidz.entr.base.model.Validation;
import com.fidz.entr.base.model.Visibility;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class CustomerTypeCustomField extends com.fidz.entr.base.model.CustomField {
    protected CustomerUIComponentType customerUIComponentType; //TEXT BOX, RADIO BUTTON, ETC
    protected List<String> options; //value options yesorno
    protected int sortOrder; //sort order
    protected String range; //data range
    protected String hint; //UI Hint
    
	
    

    
}
