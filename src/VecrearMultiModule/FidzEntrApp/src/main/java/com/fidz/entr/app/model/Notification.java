package com.fidz.entr.app.model;

import lombok.Data;

@Data
public class Notification {

	private String message;
	
	private String title;
	
	private String token;
	
	private String topic;
	
	
}
