package com.fidz.entr.app.filters.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.AlphaReports;
import com.fidz.entr.app.filters.repository.AlphaReportsRepository;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.ActivityType;

@Service
public class AlphaReportsService {

	private GenericAppINterface<AlphaReports> genericINterface;

	@Autowired
	public AlphaReportsService(GenericAppINterface<AlphaReports> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private AlphaReportsRepository alphaReportsRepository;

	@Autowired
	private GenerateExcelService generateExcelService;

	@Autowired
	private MongoTemplate mongoTemplate;

	public List<AlphaReports> getAllAlphaReports(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		Constant.LOGGER.info("Inside getAllAlphaReports AlphaReportsService.java");
		List<AlphaReports> alphaReports = null;
		Query q = new Query();

		try {
			q.addCriteria(Criteria.where("schemaName").is(schemaName));
			alphaReports = this.genericINterface.find(q, AlphaReports.class);
		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error while getting data from getAllAlphaReports AlphaReportsService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully fetched data from getAllAlphaReports AlphaReportsService.java");
		return alphaReports;
	}

	// getting the List of Alpha ReportsList

	public List<AlphaReports> getAllAlphaReportsByUserID(String userid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		Constant.LOGGER.info("Inside getAllAlphaReports AlphaReportsService.java");
		List<AlphaReports> alphaReports = null;
		Query q = new Query();

		try {
			q.addCriteria(Criteria.where("createdUserId").is(userid));
			alphaReports = this.genericINterface.find(q, AlphaReports.class);
		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error while getting data from getAllAlphaReports AlphaReportsService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully fetched data from getAllAlphaReports AlphaReportsService.java");
		return alphaReports;
	}

	// getting the List of Alpha ReportsList

	public AlphaReports createAlphaReports(@Valid AlphaReports alphaReports) {
		AlphaReports aReports = null;
		try {
			Constant.LOGGER.info(
					" Inside AlphaReportsService.java Value for insert createAlphaReports record :: " + alphaReports);
			String schemaName = alphaReports.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
			aReports = this.alphaReportsRepository.save(alphaReports);
		} catch (Exception e) {
			Constant.LOGGER.info(
					" Inside AlphaReportsService.java Value for insert createAlphaReports record :: " + alphaReports);
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully created data from BroadCastTemplate AlphaReportsService.java");
		return aReports;
	}

	public AlphaReports getAlphaReportsById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		Constant.LOGGER.info("Inside getAlphaReportsById AlphaReportsService.java");
		return this.genericINterface.findById(id, AlphaReports.class);
	}

	public AlphaReports updateAlphaReportsById(String id, @Valid AlphaReports alphaReports) {
		Constant.LOGGER.info("Inside updateAlphaReportsById AlphaReportsService.java");
		String schemaName = alphaReports.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		String alphaReportsId = alphaReports.getId();
		alphaReports.setId(id);
		return this.alphaReportsRepository.save(alphaReports);
	}

	public Map<String, String> deleteAlphaReportsById(String id, String schemaName) {
		Constant.LOGGER.info("Inside deleteAlphaReportsById AlphaReportsService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		this.alphaReportsRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "AlphaReports deleted successfully");
		return response;
	}

	public List<Activity> filterAlphaReports(@Valid AlphaReports alphaReports) throws ParseException {
		String schemaName = alphaReports.getSchemaName();
		Constant.LOGGER.info("Inside filterAlphaReports AlphaReportsService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<String> userid = alphaReports.getUsers().stream().map(user -> user.getUserid())
				.collect(Collectors.toList());
		Constant.LOGGER.info("userid" + userid);
		List<String> customers = alphaReports.getCustomerId();
		Constant.LOGGER.info("customers" + customers);
		List<com.fidz.entr.app.reportactivities.Activity> result = null;
		List<String> activityTypes = alphaReports.getActivityTypesId();
		Constant.LOGGER.info("activityTypes" + activityTypes);

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
		Constant.LOGGER.info("startdate" + alphaReports.getStartDate());
		String startDate = alphaReports.getStartDate();
		String endDate = alphaReports.getEndDate();

		Date start = format.parse(startDate + " " + "00:00");
		Date end = format.parse(endDate + " " + "23:59");
		LocalDateTime localDateTime = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.plusHours(1);
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		Constant.LOGGER.info("######startdate" + startDate);
		Constant.LOGGER.info("######enddate" + currentDatePlusOneDay);
		Query q = new Query();
		List<Activity> alList = null;

		try {

			q.addCriteria(Criteria.where("resource.id").in(userid).andOperator(
					Criteria.where("customer.id").in(customers), Criteria.where("activityType.id").in(activityTypes),
					Criteria.where("actualStartDateTime").gte(start).lte(currentDatePlusOneDay)));

			alList = this.mongoTemplate.find(q, Activity.class);
		} catch (Exception ex) {
			Constant.LOGGER.error("Error while filtering AlphaReports" + ex.getMessage());
		}

		return alList;
	}

	public AlphaReports updateAlphaReportsTrue(@Valid AlphaReports alphaReports) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		Constant.LOGGER.info("inside updateAlphaReportsTrue AlphaReportSerivce.java");
		String ids = alphaReports.getId();
		alphaReports.setEmailSent("true");
		alphaReports.setId(ids);
		return this.alphaReportsRepository.save(alphaReports);
	}

	public List<AlphaReports> getAllAlphaReportsSearchPaginated(String text, int pageNumber, int pageSize,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<AlphaReports> alphaReports = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside getAllAlphaReportsSearchPaginated CustomerService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(AlphaReports.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			alphaReports = mongoTemplate.find(query, AlphaReports.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside getAllAlphaReportsSearchPaginated CustomerService.java" + ex.getMessage());

		}
		return alphaReports;
	}

	public List<AlphaReports> getAllAlphaReportsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllAlphaReportsPaginated AlphaReportService.java");
		Query query = new Query();
		List<AlphaReports> alphaReports = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest);
			alphaReports = this.genericINterface.find(query, AlphaReports.class);

		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error occurred inside getAllAlphaReportsPaginated AlphaReportService.java " + ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllAlphaReportsPaginated " + query);
		return alphaReports;
	}

	public List<AlphaReports> getAllAlphaReportsByUserIDPaginated(int pageNumber, int pageSize, String userid,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllAlphaReportsByUserIDPaginated AlphaReportService.java");
		Query query = new Query();
		List<AlphaReports> alphaReports = null;

		try {
			query.addCriteria(Criteria.where("createdUserId").is(userid));
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest);
			alphaReports = this.genericINterface.find(query, AlphaReports.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred inside getAllAlphaReportsByUserIDPaginated AlphaReportService.java "
					+ ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllAlphaReportsByUserIDPaginated " + query);
		return alphaReports;
	}

	public List<AlphaReports> getAllAlphaReportsByUserIDSearchPaginated(String text, int pageNumber, int pageSize,
			String userid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<AlphaReports> alphaReports = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = new Query();
		Constant.LOGGER.info("Inside getAllAlphaReportsByUserIDSearchPaginated CustomerService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(AlphaReports.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query.addCriteria(Criteria.where("createdUserId").is(userid));
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			alphaReports = mongoTemplate.find(query, AlphaReports.class);

		} catch (Exception ex) {
			Constant.LOGGER
					.error("Inside getAllAlphaReportsByUserIDSearchPaginated CustomerService.java" + ex.getMessage());

		}

		return alphaReports;
	}
}
