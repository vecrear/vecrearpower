package com.fidz.entr.app.reportcustomerswebPlan;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.app.reportusers.Address;
import com.fidz.entr.app.reportusers.Company;
import com.fidz.entr.app.reportusers.Contact;
import com.fidz.entr.app.reportusers.Country;
import com.fidz.entr.app.reportusers.Department;
import com.fidz.entr.app.reportusers.Device;
import com.fidz.entr.app.reportusers.PersonName;
import com.fidz.entr.app.reportusers.Region;
import com.fidz.entr.app.reportusers.State;
import com.fidz.entr.app.reportusers.Status;
import com.fidz.entr.app.reportusers.UserRole;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.TaggedDevices;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class User {
	
	protected Date createdTimestamp;
	
	protected String createdByUser;
	
	protected Date updatedTimestamp;
	
	protected String updatedByUser;
	
	protected Date deletedTimestamp;
	
	protected String deletedByUser;
	
	protected Status status;

	protected String schemaName;
	@Id
	protected String id;
	
	protected String userName;
	protected String password; //should be encrypted password
	protected PersonName name;
	protected Address address;
	protected Contact contact;
	@JsonIgnore
	protected PersonName emergencyContactName;
	@JsonIgnore
	protected Contact emergencyContact;
	@JsonIgnore
	protected List<Object> appRoles;
	@JsonIgnore
	protected List<Object> reportsTo;
	@JsonIgnore
	protected List<Object> additionalDepartments;
	@DBRef(lazy = true)
	protected Device device;
	@DBRef(lazy = true)
	protected Country country;
	@DBRef(lazy = true)
	protected Region region;
	@DBRef(lazy = true)
	protected State state;
	
	@DBRef(lazy = true)
	protected City city;

	@DBRef(lazy = true)
	protected List<City> citylist;
	@JsonIgnore
	protected Map<String, Object> additionalFields;
	@DBRef(lazy = true)
	protected UserRole userRole;
	@DBRef(lazy = true)
	protected Company company;
	@DBRef(lazy = true)
	protected Department department;
	protected String notificationId;
	protected int userRoleId;
	protected String managerName;
	protected String managerMobile;
	protected String managerEmail;
	protected List<TaggedDevices> taggedDevices;

	@DBRef(lazy = true)
	protected com.fidz.entr.app.usermanagers.User userManager;
	protected boolean geofenceTagged = false;
}
