package com.fidz.entr.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.repository.CountryRepository;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.TaggedRegions;
import com.fidz.entr.base.repository.RegionRepository;
import com.fidz.entr.base.service.RegionService;

@Service("FEACountryService")
public class CountryService {
	private GenericAppINterface<Country> genericINterface;

	@Autowired
	public CountryService(GenericAppINterface<Country> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private RegionService regionService;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	private static String name = "Country";

	public List<Country> getAllCountries(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllCountries CountryService.java");
		List<Country> countries = null;
		try {
			countries = this.genericINterface.findAll(Country.class);
		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error while getting data from getAllCountries CountryService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully fetched data from getAllCountries CountryService.java");
		return countries;

	}

	public Country createCountry(Country country) {
		String schemaName = country.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside createCountry CountryService.java");

		// List<Country> countries = this.genericINterface.findAll(Country.class);
		List<Country> countries = null;
		Query q = new Query();
		q.addCriteria(Criteria.where("name").regex(country.getName(), "i"));
		countries = this.mongoTemplate.find(q, Country.class);
		List<String> names = countries.stream().map(p -> p.getName()).collect(Collectors.toList());
		Constant.LOGGER.info("Country List data" + names);

		if (names.stream().anyMatch(s -> s.equals(country.getName()) == true)) {
			Constant.LOGGER.info("Inside if statement");

			throw new NameFoundException(name + ExceptionConstant.SAME_NAME + ": " + country.getName());
		} else if (names.stream().anyMatch(s -> s.equalsIgnoreCase(country.getName()) == true)) {
			Constant.LOGGER.info("Inside else if statement");
			throw new NameFoundException(name + ExceptionConstant.SAME_NAME_DIFFERENT_CASE + ": " + country.getName());
		} else {
			Constant.LOGGER.info("Inside else statement");
			Constant.LOGGER.info("Successfull country creation" + country.toString());

			List<TaggedRegions> taggedregions = new ArrayList<TaggedRegions>();

			try {
				taggedregions = country.getTaggedRegions();
				Constant.LOGGER.info("taggedCities---size-" + taggedregions.size());
				Region region = null;
				for (int i = 0; i < taggedregions.size(); i++) {
					region = regionService.getRegionById(taggedregions.get(i).getId(), schemaName);
					region.setId(taggedregions.get(i).getId());
					region.setRegionTagged(taggedregions.get(i).isRegionTagged());
					region.setTaggedCountryName(taggedregions.get(i).getTaggedCountryName());
					this.regionRepository.save(region);

				}

			} catch (Exception e) {
				Constant.LOGGER.error("Error Inside createCountry CountryService.java" + e.getMessage());

			}

			return this.countryRepository.save(country);
		}
	}

	public void createCountrys(Country country) {
		String schemaName = country.getSchemaName();

		Constant.LOGGER.info("Inside createCountrys CountryService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// return countryRepository.save(country);
		this.genericINterface.saveName(country);
	}

	public Country getCountryById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getCountryById CountryService.java");
		return this.genericINterface.findById(id, Country.class);

	}

	public Country updateCountry(String id, Country country) {
		String schemaName = country.getSchemaName();
		Constant.LOGGER.info("Inside updateCountry CountryService.java");
		// List<Country> countries = new ArrayList<Country>();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<TaggedRegions> taggedregions = new ArrayList<TaggedRegions>();
		// countries = this.genericINterface.findAll(Country.class);
		// countries = this.countryRepository.findByName(country.getName());
		List<Country> countries = null;
		Query q = new Query();
		q.addCriteria(Criteria.where("name").regex(country.getName(), "i"));
		countries = this.mongoTemplate.find(q, Country.class);

		Country ctr = getCountryById(id, schemaName);

		Constant.LOGGER.info("Country List data" + countries);
		List<String> names = countries.stream().map(p -> p.getName()).collect(Collectors.toList());
		names.remove(ctr.getName());
		Constant.LOGGER.info("Country List data" + names);

		if (names.stream().anyMatch(s -> s.equals(country.getName()) == true)) {
			Constant.LOGGER.info("Inside if statement");
			throw new NameFoundException(name + ExceptionConstant.SAME_NAME + ": " + country.getName());
		} else if (names.stream().anyMatch(s -> s.equalsIgnoreCase(country.getName()) == true)) {
			Constant.LOGGER.info("Inside else if statement");
			throw new NameFoundException(name + ExceptionConstant.SAME_NAME_DIFFERENT_CASE + ": " + country.getName());
		} else {
			Constant.LOGGER.info("Inside else statement");
			Constant.LOGGER.info("Successfull country creation" + country.toString());
			country.setId(id);

			try {
				taggedregions = country.getTaggedRegions();
				Constant.LOGGER.info("taggedCities---size-" + taggedregions.size());
				Region region = null;
				for (int i = 0; i < taggedregions.size(); i++) {
					region = regionService.getRegionById(taggedregions.get(i).getId(), schemaName);
					region.setId(taggedregions.get(i).getId());
					region.setRegionTagged(taggedregions.get(i).isRegionTagged());
					region.setTaggedCountryName(taggedregions.get(i).getTaggedCountryName());
					this.regionRepository.save(region);

				}

			} catch (Exception e) {
				Constant.LOGGER.error("Error Inside updateCountry CountryService.java" + e.getMessage());
			}
		}

		return this.countryRepository.save(country);

	}

	// hard delete
	public Map<String, String> deleteCountry(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside deleteCountry CountryService.java");

		this.countryRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Country deleted successfully");
		return response;

	}

	// soft delete
	public Map<String, String> deleteSoftCountry(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside deleteSoftCountry CountryService.java");

		Country country = this.genericINterface.findById(id, Country.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		country.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		country.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		country.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(country);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Country deleted successfully");
		return response;

	}

	public List<Country> streamAllCountries() {
		return countryRepository.findAll();
	}

	// To get all countries paginated
	// page number starts from zero
	public List<Country> getAllCountriesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllCountriesPaginated CountryService.java");
		Query query = new Query();
		List<Country> countries = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest);
			countries = this.genericINterface.find(query, Country.class);

		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error occurred inside getAllCountriesPaginated CountryService.java " + ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllCountriesPaginated " + query);
		return countries;
	}

	// --------------------//countries--to get based on the tagged and untagged

	public List<Country> getAllTagUnTaggedCountry(String variableMap, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllTagUnTaggedCountry Country.java");
		Query query = new Query();
		List<Country> countries = null;

		try {
			if (variableMap.equalsIgnoreCase("tagged")) {
				query.addCriteria(Criteria.where("countryTagged").in(true));
			} else if (variableMap.equalsIgnoreCase("untagged")) {
				query.addCriteria(Criteria.where("countryTagged").in(false));
			}

			countries = this.genericINterface.find(query, Country.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error inside getAllTagUnTaggedCountry Country.java " + ex.getMessage());
		}
		return countries;
	}

	public List<Country> countryTextSearch(@Valid String text, String schemaName, int pageNumber, int pageSize) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<Country> country = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside countryTextSearch CountryService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(Country.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			country = mongoTemplate.find(query, Country.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside countryTextSearch CountryService.java" + ex.getMessage());

		}
		return country;
	}

	public List<Country> countryTextByNameSearch(@Valid String text, String schemaName, int pageNumber, int pageSize) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<Country> countries = null;
		Query q = new Query();

		try {
			q.addCriteria(Criteria.where("name").regex(text, "i"));
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			q.with(pageableRequest);
			countries = this.genericINterface.find(q, Country.class);

		} catch (Exception ex) {
			Constant.LOGGER.info("Error occured inside countryTextByNameSearch" + ex.getLocalizedMessage());
		}
		return countries;

	}

}
