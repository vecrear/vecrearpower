package com.fidz.entr.app.awsconfig.model;

import java.util.List;


import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Group;
import com.fidz.entr.app.config.UserData;

import lombok.Data;
@Data
@JsonIgnoreProperties(value = { "target" })
public class APiUrlType {
	
	protected String geoAddressKey ;
	protected int geoAddresscount;
	
	protected String geoDistKey;
	protected int geoDistCount;
	
	protected String smsUrl ;
	protected int smsUrlCount;
	
	protected String vecMapkey ;
	protected int vecMapKeycount;
	
	
	
	

	
	
	
	

}
