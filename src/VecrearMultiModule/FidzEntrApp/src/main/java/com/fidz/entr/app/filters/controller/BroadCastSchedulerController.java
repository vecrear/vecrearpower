package com.fidz.entr.app.filters.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.AlphaReports;
import com.fidz.entr.app.filters.model.BroadCastScheduler;
import com.fidz.entr.app.filters.model.BroadCastTemplate;
import com.fidz.entr.app.filters.service.BroadCastSchedulerService;


@CrossOrigin(maxAge = 3600)
@RestController("BroadCastSchedulerController")
@Configuration
@RequestMapping(value="/broadcastschedulers")
public class BroadCastSchedulerController {
	
	@Autowired
	BroadCastSchedulerService broadCastSchedulerService;
	
	@PostMapping("/get/all")
	public List<BroadCastScheduler> getAllBroadCastScheduler(@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.getAllBroadCastScheduler(schemaName);
	    }
	
	@PostMapping("/get/all/{pageNumber}/{pageSize}")
	public List<BroadCastScheduler> getAllBroadCastSchedulerPaginated(@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.getAllBroadCastSchedulerPaginated(pageNumber,pageSize,schemaName);
	    }
	
	@PostMapping("/search/{text}/get/all/{pageNumber}/{pageSize}")
	public List<BroadCastScheduler> getAllBroadCastSchedulerSearchPaginated(@PathVariable(value = "text") String text,@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.getAllBroadCastSchedulerSearchPaginated(text,pageNumber,pageSize,schemaName);
	    }
	
	@PostMapping("/get/all/byuserid/{userid}")
	public List<BroadCastScheduler> getAllBroadCastSchedulerByUserID(@PathVariable(value = "userid") String userid,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.getAllBroadCastSchedulerByUserID(userid,schemaName);
	}
	
	@PostMapping("/get/all/byuserid/{userid}/{pageNumber}/{pageSize}")
	public List<BroadCastScheduler> getAllBroadCastSchedulerUserIdPaginated(@PathVariable(value = "userid") String userid,@PathVariable(value = "text") String text,@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.getAllBroadCastSchedulerUserIdPaginated(userid,pageNumber,pageSize,schemaName);
	    }
	
	@PostMapping("/get/all/byuserid/{userid}/search/{text}/{pageNumber}/{pageSize}")
	public List<BroadCastScheduler> getAllBroadCastSchedulerUserIdSearchPaginated(@PathVariable(value = "text") String text,@PathVariable(value = "userid") String userid,@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.getAllBroadCastSchedulerUserIdSearchPaginated(text,userid,pageNumber,pageSize,schemaName);
	    }
	
	
	@PostMapping
	public BroadCastScheduler createBroadCastScheduler(@Valid @RequestBody BroadCastScheduler broadCastScheduler) throws ParseException{
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.createBroadCastScheduler(broadCastScheduler);
	}

	@PostMapping("/get/id/{id}")
    public BroadCastScheduler getBroadCastSchedulerById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.getBroadCastSchedulerById(id, schemaName);
    }
	
	@PostMapping("/update/id/{id}")
    public BroadCastScheduler updateBroadCastScheduler(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody BroadCastScheduler broadCastScheduler) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.updateBroadCastScheduler(id, broadCastScheduler);   
        
    }
	
	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteBroadCastSchedulerById(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return broadCastSchedulerService.deleteBroadCastSchedulerById(id, schemaName);
	}
}
