package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum UIComponentType {
	TEXT, RADIO, CHECKBOX, IMAGE, SPINNER,SIGNATURE, DATE, TIME, DATETIME, SINGLE_SELECT, MULTI_SELECT, SEEK_BAR, QR_CODE_SCANNER,DOCUMENT
}
