package com.fidz.entr.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ActivityTypeNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6594871590584275501L;
	public ActivityTypeNotFoundException(String activitytype) {
		super("Activity Type not found with id/name " + activitytype);
	}
}
