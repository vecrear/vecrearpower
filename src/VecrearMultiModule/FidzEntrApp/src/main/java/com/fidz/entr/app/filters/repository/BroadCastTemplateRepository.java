package com.fidz.entr.app.filters.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fidz.entr.app.filters.model.BroadCastTemplate;

public interface BroadCastTemplateRepository extends MongoRepository<BroadCastTemplate, String> {

}
