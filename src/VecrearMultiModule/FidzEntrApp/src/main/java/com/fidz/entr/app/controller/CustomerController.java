package com.fidz.entr.app.controller;

import java.text.ParseException;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.CustomerNotFoundException;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.PlannedUsers;
import com.fidz.entr.app.model.PlannedUsersAndroid;
import com.fidz.entr.app.service.CustomerService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEACustomerController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/customers")
public class CustomerController {
	
	@Autowired
	private CustomerService  customerService;
	
	@Value("${customerservices.enabled}")
	private Boolean property;
	
	
	@PostMapping("/get/all")
	//@Cacheable(value = "customers")
	public List<Customer> getAllCustomers(@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getAllCustomers(schemaName);
	}
	@PostMapping("/get/all/deptid/{deptid}/cityid/{cityid}")
	public List<Customer> getAllCustomersBydeptandCity(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="deptid") String deptid,@PathVariable(value="cityid") String cityid){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getAllCustomersBydeptandCity(deptid,cityid,schemaName);
	}
	
	
	// getting the webList Customers
	@PostMapping("/get/all/web")
	//@Cacheable(value = "customers")
	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomersweb(@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getAllCustomersWeb(schemaName);
	}
	
	
	@PostMapping("/get/all/web/custcategoryid/{custcategoryid}/custtype/{custtype}")
	//@Cacheable(value = "customers")
	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomerswebByCutomerType(@PathVariable(value="custcategoryid") String custcategoryid,@PathVariable(value="custtype") String custtype,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getAllCustomerswebByCutomerType(custcategoryid,custtype,schemaName);
	}
	
	//Post request to get all Customersweb  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomerswebPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getAllCustomersPaginated(pageNumber, pageSize, schemaName);
	}
		
	
	@PostMapping
	public Customer createCustomer(@Valid @RequestBody Customer customer){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.createCustomer(customer);
	}
	@PostMapping("/creates")
	public void createCustomers(@Valid @RequestBody Customer customer){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		customerService.createCustomer(customer);
	}
	
	@PostMapping("/get/id/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getCustomerById(id, schemaName);
		
		
	}
	
	@PostMapping("/get/id/userid/{id}")
	public Stream<Customer> getCustomerByUserId(@PathVariable(value="id") String userId, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getCustomerByUserId(userId, schemaName);		
	}
	
	@PostMapping("/update/id/{id}")
    public Customer updateCustomer(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Customer customer) throws ParseException {
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		String validate = "others";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.updateCustomer(id, customer, validate);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteCustomer(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.deleteCustomer(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteCustomer(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.deleteSoftCustomer(id, schemaName, timeUpdate);
    }
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Customer> streamAllCustomers() {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.streamAllCustomers();
    }
    
    
    //to get customers based on users, customercategory and customertype
    @PostMapping("/byusers/web")
	public List<com.fidz.entr.app.reportcustomers.Customer> customerByusersWeb(@Valid @RequestBody PlannedUsers plannedUsers,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.customerFilters(plannedUsers,schemaName);
	}

    //to customers based on users for device
    @PostMapping("/byusers/device")
	public List<com.fidz.entr.app.reportcustomers.Customer> customerByuserDevice(@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.customerByuserDevice(plannedUsers,schemaName);
	}
    
    @PostMapping("/byusers/webalpha/simplifycustomer")
  	public List<com.fidz.entr.app.reportusers.User> cutomersByUser(@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.cutomersByUser(plannedUsers,schemaName);
  	}
    
    
    @PostMapping("/byusers/device/simplifycustomer")
  	public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDeviceSimplifyCustomer(@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserDeviceSimplifyCustomer(plannedUsers,schemaName);
  	}
    
    @PostMapping("/byusers/devicebycity/simplifycustomer")
  	public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomer(@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserDeviceBycitySimplifyCustomer(plannedUsers,schemaName);
  	}
    
    @PostMapping("/byusers/devicebycity/simplifycustomer/{pageNumber}/{pageSize}")
  	public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomerBatchWise(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize,@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserDevicebycitySimplifyCustomerBatchWise(plannedUsers,schemaName,pageNumber,pageSize);
  	}
    
    @PostMapping("/byusers/devicebycity/simplifycustomer/type/{type}/{checkDate}")
  	public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomerByDate(@PathVariable(value="checkDate") String checkDate,@PathVariable(value="type") String type,@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName) throws ParseException{
    	System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserDevicebycitySimplifyCustomerByDate(type,plannedUsers,schemaName,checkDate);
  	}
    
    
    @PostMapping("/byusers/devicebycity/simplifycustomer/type/{type}")
  	public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomerByType(@PathVariable(value="type") String type,@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserDeviceBycitySimplifyCustomerByType(type,plannedUsers,schemaName);
  	}
    
    @PostMapping("/byusers/devicebycity/simplifycustomer/pages/{pageNumber}/{pageSize}/type/{type}")
  	public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomerByTypeBatchWise(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize,@PathVariable(value="type") String type,@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserDevicebycitySimplifyCustomerByTypeBatchWise(type,plannedUsers,schemaName,pageNumber,pageSize);
  	}
    
    @PostMapping("/byusers/devicebycity/simplifycustomer/count/type/{type}")
  	public JSONObject customerByuserDevicebycitySimplifyCustomerByTypeCount(@PathVariable(value="type") String type,@Valid @RequestBody PlannedUsersAndroid plannedUsers,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserDevicebycitySimplifyCustomerByTypeCount(type,plannedUsers,schemaName);
  	}
    

    @PostMapping("/search/text/{text}/{pageNumber}/{pageSize}")
	public List<com.fidz.entr.app.reportcustomers.Customer> customerTextSearch(@Valid @PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.customerTextSearch(text,schemaName,pageNumber,pageSize);
	}
    
    
    @PostMapping("/web/simplifycustomer")
  	public List<com.fidz.entr.app.reportcustomersweb.Customer> customerByuserWebSimplifyCustomer(@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserWebSimplifyCustomer(schemaName);
  	}
    
    @PostMapping("/web/simplifycustomer/deptid/{deptid}/cityId/{cityId}")
  	public List<com.fidz.entr.app.reportcustomersweb.Customer> customerByuserWebSimplifyCustomer(@PathVariable(value="deptid") String deptid,@PathVariable(value="cityId") String  cityId,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserWebSimplifyCustomerBydeptid(deptid,cityId,schemaName);
  	}
    
    @PostMapping("/web/simplifycustomer/deptid/{deptid}/cityId/{cityId}/{pageNumber}/{pageSize}")
  	public List<com.fidz.entr.app.reportcustomerswebPlan.Customer> customerByuserWebSimplifyCustomerwithPagination(@PathVariable(value="deptid") String deptid,@PathVariable(value="cityId") String  cityId,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
  		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.customerByuserWebSimplifyCustomerwithPagination(deptid,cityId,pageNumber,pageSize,schemaName);
  	}
    
    @PostMapping("/search/text/{text}/cityId/{departId}/departId/{cityId}/{pageNumber}/{pageSize}")
   	public List<com.fidz.entr.app.reportcustomerswebPlan.Customer> customerTextSearch(@Valid @PathVariable(value="text") String text,@PathVariable(value="departId") String departId,@PathVariable(value="cityId") String cityId,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize){
   		System.out.println("property value******"+property);
   		
   		return customerService.customerTextSearchBasedOnDepartmentandCity(text,schemaName,departId,cityId,pageNumber,pageSize);
   	}
    
    
    @PostMapping("/status/customerid/{id}")
	//@Cacheable(value = "users")
	public String getStatusofObjectMappedOrNot(@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return customerService.getStatusofObjectMappedOrNot(id,schemaName);
	}
    
    
    @PostMapping("/get/all/customercatgoryid/{id}")
  	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomerswebByCutomerCategotyID(@PathVariable(value = "id") String id,@RequestHeader(value="schemaName") String schemaName){
  		System.out.println("property value******"+property);
  		String message="Customerservices are disabled";
  		if(!property) 
  		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
  		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
  		return customerService.getAllCustomerswebByCutomerCategotyID(id,schemaName);
  	}
    
    
    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Customer with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(CustomerNotFoundException ex) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.notFound().build();
    }

}
