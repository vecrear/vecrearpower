package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.UserRole;


@Repository
public interface UserRoleRepository extends MongoRepository<UserRole, String>{
	//Mono<UserRole> findByName(String name);
	UserRole findByName(String name);
}
