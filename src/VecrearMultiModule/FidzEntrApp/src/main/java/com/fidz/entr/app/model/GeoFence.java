package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;

import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.TaggedDevices;

import lombok.Data;

@Data
@Document(collection = "geofence")
@JsonIgnoreProperties(value = { "target" })
public class GeoFence extends Base {

	@Id
	private String id;

	@Indexed(unique = true)
	@NotNull
	@NotBlank
	private String name;

	private String type;

	private String geofenceOwnerId;
	/*
	 * geoCoordinates - List of GeoPoints which makes up the Geo Fence. For Circle -
	 * Only one value would be present which would be the center's location
	 */
	private List<GeoPoint> geoCoordinates;
	/*
	 * radius - Radius of the Geo Fence. Applicable only for GeoFence Type - Circle
	 */
	private int radius;

	/* for rectangle */
	private Boundary boundary;

	/*
	 * deviceList - List of devices mapped to the Geo Fence
	 */
//	@DBRef(lazy = true)
//	private List<Device> deviceList;

	@DBRef(lazy = true)
	private List<com.fidz.entr.app.reportusers.User> usersDeviceList;

	protected List<TaggedUsers> taggedUsers;

}
