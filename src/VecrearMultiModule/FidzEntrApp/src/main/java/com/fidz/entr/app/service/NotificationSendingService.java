package com.fidz.entr.app.service;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.model.Notification;
import com.fidz.entr.app.model.PushNotificationRequest;

@Service
public class NotificationSendingService {

	 @Autowired
	 private  FCMServic fcmServic;
	
	 public  String sendNotification(Notification noty) {
		
		 try {
			 PushNotificationRequest ppp = PushNotificationRequest.getInstance(); 
			    ppp.setMessage(noty.getMessage());
			  	ppp.setTitle("Vecrear-Trac");
			  	ppp.setToken(noty.getToken());
			  	ppp.setTopic("vecrear");  //vecrear topic subscrption handling in the next build
			  	
				Thread geoThread = new Thread() {
				    public void run() {
				    	   try {  	
				    		   fcmServic.sendMessageToToken(ppp);
							Thread.sleep(3);
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (Exception e) {
												e.printStackTrace();
						}	
				    }  
				};
		     	
				geoThread.start();
			  	
			  	
			  	return "sent notification successfully";
		 }catch (Exception e) {
			 e.toString();
			 return null;
		}
		
		
	}
	
	
}
