package com.fidz.entr.app.reportattendance;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "logofftime")
@JsonIgnoreProperties(value = { "target" })
public class LogOffTime extends Base {
	
	@Id
	protected String id;
	
	@NotNull
	//protected long logOutDateTimeStamp;
	protected Date logOutDateTimeStamp;
	
	@NotNull
	//protected long logInDateTimeStamp;
	protected Date logInDateTimeStamp;
	
	@NotNull
	protected String duration;
	
	protected String reason;

	public LogOffTime(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull Date logOutDateTimeStamp, @NotNull Date logInDateTimeStamp,
			@NotNull String duration, String reason) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.logOutDateTimeStamp = logOutDateTimeStamp;
		this.logInDateTimeStamp = logInDateTimeStamp;
		this.duration = duration;
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "LogOffTime [id=" + id + ", logOutDateTimeStamp=" + logOutDateTimeStamp + ", logInDateTimeStamp="
				+ logInDateTimeStamp + ", duration=" + duration + ", reason=" + reason + ", createdTimestamp="
				+ createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp
				+ ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser="
				+ deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	
	

	
}
