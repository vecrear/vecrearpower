package com.fidz.entr.app.reportactivities;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Location;
import com.fidz.base.model.Status;
import com.fidz.entr.app.model.ActivityUserList;
import com.fidz.entr.app.model.ActivityUsersList;
import com.fidz.entr.app.model.Customer;

import lombok.Data;
@Data
@JsonIgnoreProperties(value = { "target" })
public class Activity {
	
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	
	@LastModifiedBy
	protected String updatedByUser;
	
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	
	@LastModifiedBy
	protected String deletedByUser;
	
	@NotNull
	protected Status status;
	@NotNull
	protected String schemaName;
	
	@Id
	protected String id;
	

	@DBRef(lazy = true)
	protected Company company;
	

	@DBRef(lazy = true)
	protected Department department;
	
	@DBRef(lazy = true)
	protected ActivityType activityType;
	

	protected ActivityConfigurationType activityConfigurationType;
	

	@DBRef(lazy = true)
	protected User resource;
	
	protected List<ActivityUserList> resourceList;

	@DBRef(lazy = true)
	protected com.fidz.entr.app.reportcustomerswebPlan.Customer customer;
	
	/**
	 * Activity Details is configured for Single Entry or Multiple Entry. For Single Entry, there would be only one
	 * item in this list. For Multiple Entry, there would be multiple entries in this list.
	 */

	//@DBRef(lazy = true)
	protected List<ActivityLog> activityLogs;
	
	@NotNull
	protected ActivityStatus activityStatus;

    protected Date plannedStartDateTime;
	
	protected Date plannedEndDateTime;
	
	protected Date actualStartDateTime;
	
	protected Date actualEndDateTime;
	
	protected Location plannedLocation;
	
	protected Location performedLocation;
	
	protected String comments;
	
	@JsonIgnore
	protected List<Object> activityStatusChangeLog;
	protected Customer prospectCustomer;


}
