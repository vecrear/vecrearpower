package com.fidz.entr.app.reportactivities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum DistanceCalculationMethod {
	ACTIVITY, MOVEMENT, NO_TRACKING
}
