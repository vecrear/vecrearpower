package com.fidz.entr.app.repository;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.User;
@Repository("CustomerRepository")
public interface  CustomerRepository extends MongoRepository<Customer, String>{
	public List<Customer> findByName(String userName);

	//public Stream<Customer> findByName(String userName);
}
