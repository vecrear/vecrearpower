package com.fidz.entr.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomerNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7932180292589959996L;

	public CustomerNotFoundException(String customer) {
		super("Customer not found with id/name " + customer);
	}
	public CustomerNotFoundException(String customer, String data) {
		super(data);
	}
}
