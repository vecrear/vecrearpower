package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.User;
@Repository("FEAUserRepository")
public interface UserRepository extends MongoRepository<User, String>{
	//public Mono<User> findByUserName(String userName);
	public User findByUserName(String userName);
	public User findByDepartment(String departmentName);
	
}
