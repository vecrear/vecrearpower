package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;

import lombok.Data;

@Data
@Document(collection = "country")
@JsonIgnoreProperties(value = { "target" })
public class Country extends com.fidz.entr.base.model.Country {
	
	
	@DBRef(lazy = true)
	protected Company company;

	
	
	
	
}
