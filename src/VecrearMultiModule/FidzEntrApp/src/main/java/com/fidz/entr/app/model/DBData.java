package com.fidz.entr.app.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;
@Data
@Document(collection = "dbdata")
@JsonIgnoreProperties(value = { "target" })
public class DBData extends Base{
	@Id
	protected String id;
	
	@Indexed(unique = true)
	@NotNull
	protected String clientId;
	
	@NotNull
	protected String host;
	
	@NotNull
	protected String dbName;
	
	@NotNull
	protected int port;
	
	
	protected String dbUserName;
	
	
	protected String dbPassword;


	public DBData(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String clientId, @NotNull String host,
			@NotNull String dbName, @NotNull int port, String dbUserName, String dbPassword) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.clientId = clientId;
		this.host = host;
		this.dbName = dbName;
		this.port = port;
		this.dbUserName = dbUserName;
		this.dbPassword = dbPassword;
	}


	@Override
	public String toString() {
		return "DBData [id=" + id + ", clientId=" + clientId + ", host=" + host + ", dbName=" + dbName + ", port="
				+ port + ", dbUserName=" + dbUserName + ", dbPassword=" + dbPassword + ", createdTimestamp="
				+ createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp
				+ ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser="
				+ deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}





}
