package com.fidz.entr.app.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "activitystatuschangelog")
@JsonIgnoreProperties(value = { "target" })
public class ActivityStatusChangeLog extends Base {
	@Id
	protected String id;
	
	@NotNull
	protected ActivityStatus activityStatus;
	
	@NotNull
	protected String reason;


	

	


	

}
