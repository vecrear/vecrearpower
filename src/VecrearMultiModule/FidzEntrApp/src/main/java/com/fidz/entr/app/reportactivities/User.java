package com.fidz.entr.app.reportactivities;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.reportattendance.PersonName;
import com.fidz.entr.app.reportattendance.Status;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.TaggedDevices;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class User {
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	
	
	@Id
	protected String id;
	
	@Indexed(unique = true)
	protected String userName;
	@JsonIgnore
	protected String password; //should be encrypted password
	
	protected PersonName name;
	@JsonIgnore
	protected Object address;
	@JsonIgnore
	protected Object contact;
	@JsonIgnore
	protected Object emergencyContactName;
	@JsonIgnore
	protected Object emergencyContact;
	
	@DBRef(lazy = true)
	protected UserRole userRole;

	@JsonIgnore
	protected List<Object> appRoles;
	@JsonIgnore
	protected List<Object> reportsTo;
	@JsonIgnore
	protected List<Object> additionalDepartments;
	@JsonIgnore
	protected Object device;
	@JsonIgnore
	protected Object country;
	@JsonIgnore
	protected Object region;
	@JsonIgnore
	protected Object state;
//	@DBRef(lazy = true)
//	protected City city;
	@DBRef(lazy = true)
	protected City city;
	@DBRef(lazy = true)
	protected List<City> citylist;
	@JsonIgnore
	protected Map<String, Object> additionalFields;
	protected int userRoleId;
	protected String managerName;
	protected String managerMobile;
	protected String managerEmail;
	protected List<TaggedDevices> taggedDevices;
	
	//@DBRef(lazy = true)
	//protected com.fidz.entr.app.usermanagers.User userManager;

	
}
