package com.fidz.entr.app.controller;

import java.text.ParseException;


import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.ActivityLog;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.MultiUserActivity;
import com.fidz.entr.app.model.ResourceAttendanceReport;
import com.fidz.entr.app.service.ActivityService;
import com.google.api.gax.rpc.NotFoundException;
import com.google.api.gax.rpc.StatusCode;
import com.google.firebase.auth.internal.HttpErrorResponse;


@CrossOrigin(maxAge = 3600)
@RestController("FEAActivityController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/activities")

public class ActivityController {
	@Autowired
	private ActivityService  activityService;
	
	// post request for get all activities
	// @Cacheable("activities")
	// @CachePut(value = "activities", key = "#id")
	//@Cacheable(value = "activities")
	
	@Value("${activityservices.enabled}")
	private Boolean property;
	
	
	//@ConditionalOnProperty(prefix="activityservices", name="enabled", havingValue = "true")
   //@ConditionalOnProperty(name = "${activityservices.enabled}",havingValue = "true")
    @PostMapping("/get/all")
	public List<Activity> getAllActivities(@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******"+property);
	    String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getAllActivities(schemaName);
	}
		
	
	//getting the web call list
	@RequestMapping(value = "/get/all/web",method = RequestMethod.POST , produces="application/json")
	public List<com.fidz.entr.app.reportactivities.Activity> getAllActivitiesweb(@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getAllActivitiesWeb(schemaName);
	}
	
	
	//----------------------------------------status of the user and customer for pending activities----------------
	
	
	
	 @PostMapping("/status/customerid/{customerid}")
	    public Map<String, String> getCustomerActivityStatus(@PathVariable(value = "customerid") String customerid, @RequestHeader(value="schemaName") String schemaName) {
	    	System.out.println("property value******"+property);
			String message="getCustomerActivityStatus are disabled";
			if(!property) 
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return activityService.getCustomerActivityStatus(customerid, schemaName);
	    }
	 
	 @PostMapping("/status/userid/{userid}")
	    public Map<String, String> getUserActivityStatus(@PathVariable(value = "userid") String userid, @RequestHeader(value="schemaName") String schemaName) {
	    	System.out.println("property value******"+property);
			String message="getCustomerActivityStatus are disabled";
			if(!property) 
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return activityService.getUserActivityStatus(userid, schemaName);
	    }
	
	 @PostMapping("/status/activityTypeid/{activityTypeid}")
	    public Map<String, String> getActivityTypeStatus(@PathVariable(value = "activityTypeid") String activityTypeid, @RequestHeader(value="schemaName") String schemaName) {
	    	System.out.println("property value******"+property);
			String message="getCustomerActivityStatus are disabled";
			if(!property) 
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return activityService.getActivityTypeStatus(activityTypeid, schemaName);
	    }
	
	
	//reports to get the based on the ID
	@PostMapping("/reports/{userId}/sdate={startdate}|edate={enddate}")
    public List<com.fidz.entr.app.reportactivities.Activity> getAllActivitiesBasedOnDate(@PathVariable(value="userId") String userId,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getActivitiesBasedOnDate(schemaName,userId,startdate,enddate);
    }
	
	@PostMapping
	public Activity createActivity(@Valid @RequestBody Activity activity){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.createActivity(activity);
	}
	
	
	
	
	@PostMapping("/prospectcustomer")
	public Activity createActivityForProspect(@Valid @RequestBody Activity activity){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.createActivityForProspect(activity);
	}
	
	
	
	

	@PostMapping("/plan/multiuser")
	public MultiUserActivity createActivityForMultiUser(@Valid @RequestBody Activity activity){

		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.createActivityFoMultiUser(activity);
	}
	
	@PostMapping("/creates")
	public void createActivitys(@Valid @RequestBody Activity activity){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		activityService.createActivitys(activity);
	}
	
	//post request for get activity by id
	@PostMapping("/get/id/{id}")
	public ResponseEntity<Activity> getActivityById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getActivityById(id,schemaName);
		
	}
	
	@PostMapping("/get/id/act/{id}")
	//@Cacheable("customers")
	public String getCustomerByActivityId(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getCustomerByActivityId(id,schemaName);
		
	}
	@PostMapping("/startdate/{startdate}/enddate/{enddate}")
	public List<Activity> getActivityByDate(@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getActivityByDate(startdate,enddate,schemaName);
		
	}
	@PostMapping("/pagenumber/{pageNumber}/pageSize/{pageSize}")
	public List<Activity> getAllActivityPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getAllActivityPaginated(pageNumber, pageSize, schemaName);
 	}
	
	@PostMapping("/get/all/web/sdate={startdate}|edate={enddate}/{pageNumber}/{pageSize}")
	public List<com.fidz.entr.app.reportactivities.Activity> getAllActivityPaginatedWeb(@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getAllActivityPaginatedWeb(startdate,enddate,pageNumber,pageSize,schemaName);
	}
	
	@PostMapping("/get/id/department/{id}")
	public List<Activity> getActivityByDepartment(@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getActivityByDepartment(id,schemaName);
		
	}
	@PostMapping("/range/startdate/{startdate}/enddate/{enddate}")
	//@Cacheable("plannedStartDateTime")
	 public List<Activity> findByDateRange(@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate, @RequestHeader(value="schemaName") String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.findByDateRange(startdate, enddate, schemaName);
	    	}
	
	@PostMapping("/get/id/user/{id}")
	//@Cacheable(value = "id", key = "#id")
	public List<Activity> getActivityByUser(@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getActivityByUser(id,schemaName);
		
	}
	//post request for get activity PlannedActivities By UserId
//	----------------------------------------with all the data---------------------
	@PostMapping("/get/id/planned/{userId}")
	//@Cacheable(value = "Activity", key = "#Activity.id")
	public List<Activity> getPlannedActivitiesByUserId(@PathVariable(value="userId") String userId, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getPlannedActivitiesByUserId(userId,schemaName);
		
	}
//------------------------------------------------------------with simplyfied data-----------------------------
	
	@PostMapping("/get/id/planned/{userId}/web")
	//@Cacheable(value = "Activity", key = "#Activity.id")
	public List<com.fidz.entr.app.reportactivities.Activity>  getTinyPlannedActivitiesByUserId(@PathVariable(value="userId") String userId, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getTinyPlannedActivitiesByUserId(userId,schemaName);
		
	}
	
	
	//patch request submit planned activities
	@PatchMapping("/patch/id/planned/{id}")
	public Activity submitPlannedActivity(@PathVariable(value="id") String id,@Valid @RequestBody List<ActivityLog> activityLog, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.submitPlannedActivity(id, activityLog,schemaName);
		
	}
	//update activity
	@PostMapping("/update/id/{id}")
    public Activity updateActivity(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Activity activity) throws ParseException {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.updateActivity(id, activity);
    }
	
	
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteActivity(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.deleteActivity(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteActivity(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.deleteSoftActivity(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Activity> streamAllActivities() {
    	System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.streamAllActivities();
    }
    
    
    @PostMapping("/get/resource/{id}/sdate={startdate}|edate={enddate}/{pageNumber}/{pageSize}")
	//@Cacheable(value = "id", key = "#id")
	public List<com.fidz.entr.app.reportactivities.Activity> getActivityByResourse(@PathVariable(value="id") String id,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize,@RequestHeader(value="schemaName") String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getActivityByResourse(startdate,enddate,pageNumber,pageSize,id,schemaName);
		
	}
    
    @PostMapping("/get/resource")
	//@Cacheable(value = "id", key = "#id")
	public List<com.fidz.entr.app.reportactivities.Activity> getActivityByMultiResourse(@Valid @RequestBody ResourceAttendanceReport report,@RequestHeader(value="schemaName") String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityService.getActivityByMultiResourse(report, schemaName);
		
	}

    // Exception Handling Examples
    /*@ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Activity with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ActivityNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/
	

	
}
