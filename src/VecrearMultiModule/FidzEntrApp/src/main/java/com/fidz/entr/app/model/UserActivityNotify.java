package com.fidz.entr.app.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "UserActivityNotify")
@JsonIgnoreProperties(value = { "target" })
public class UserActivityNotify {

	
	
	
	@Id
	private String notificationId;
	private String userActivityStatus;
	
	public static UserActivityNotify userActivityNotifyStatus = new UserActivityNotify();

	

	public UserActivityNotify() {
		

	}



	public static UserActivityNotify getUserActivityNotifyStatus() {
		return userActivityNotifyStatus;
	}



	public static void setUserActivityNotifyStatus(UserActivityNotify userActivityNotifyStatus) {
		UserActivityNotify.userActivityNotifyStatus = userActivityNotifyStatus;
	}




	@Override
	public String toString() {
		return "UserActivityNotify [notificationId=" + notificationId + ", userActivityStatus=" + userActivityStatus
				+ "]";
	}



	public UserActivityNotify(String notificationId, String userActivityStatus) {
		super();
		this.notificationId = notificationId;
		this.userActivityStatus = userActivityStatus;
	}


	
	
	

	
	
}
