package com.fidz.entr.app.filters.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.filters.model.DashboardSetting;

@Repository("DasboardSettingRepository")
public interface DasboardSettingRepository  extends MongoRepository<DashboardSetting , String>{
	
	

}
