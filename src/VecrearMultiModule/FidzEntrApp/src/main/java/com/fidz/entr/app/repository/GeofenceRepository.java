package com.fidz.entr.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.GeoFence;



@Repository
public interface GeofenceRepository extends MongoRepository<GeoFence, String> {

	public List<GeoFence> findByName(String name);

}
