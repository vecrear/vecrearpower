package com.fidz.entr.app.filters.model;

import lombok.Data;

@Data
public class ActivityMap{

private String	totalcount;

private Object activityStatus;

public ActivityMap(String totalcount, Object activityStatus) {
	super();
	this.totalcount = totalcount;
	this.activityStatus = activityStatus;
}

@Override
public String toString() {
	return "ActivityMap [totalcount=" + totalcount + ", activityStatus=" + activityStatus + "]";
}




	


}
