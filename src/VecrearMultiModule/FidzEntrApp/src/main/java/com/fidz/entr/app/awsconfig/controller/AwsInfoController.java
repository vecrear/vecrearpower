package com.fidz.entr.app.awsconfig.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.entr.app.awsconfig.model.AwsInfo;
import com.fidz.entr.app.awsconfig.service.AwsInfoNotFoundException;
import com.fidz.entr.app.awsconfig.service.AwsInfoService;

@CrossOrigin(maxAge = 3600)
@RestController("FEAAwsInfoController")
@RequestMapping(value = "/awsinfo")
public class AwsInfoController {

	@Autowired
	private AwsInfoService awsInfoService;

	@PostMapping("/get/all")
	// @Cacheable(value = "users")
	public List<AwsInfo> getAllInfo(@RequestHeader(value = "schemaName") String schemaName) {
		return awsInfoService.getAllInfo(schemaName);
	}

	@PostMapping
	public AwsInfo createInfo(@Valid @RequestBody AwsInfo info,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return awsInfoService.createInfo(info, schemaName);
	}

	@PostMapping("/get/byschemaname")
	public List<AwsInfo> getAWSnfo(@Valid @RequestBody AwsInfo info,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return awsInfoService.getAWSnfo(info, schemaName);
	}
	
	
	
	@PostMapping("/encptdata")
	public AwsInfo createenccyptedInfo(@Valid @RequestBody AwsInfo info,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return awsInfoService.createawsInfo(info, schemaName);
	}
	
	@PostMapping("/decptdata")
	public AwsInfo decrptedInfo(@Valid @RequestBody AwsInfo info,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return awsInfoService.decrptedInfo(info, schemaName);
	}

}
