package com.fidz.entr.app.service;

import java.util.HashMap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.exception.UserException;

import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.app.model.AuthenticationResponse;
import com.fidz.entr.app.model.Login;
import com.fidz.entr.app.model.ResetPassword;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.repository.DeviceRepository;
import com.fidz.entr.app.repository.UserRepository;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.superadmin.model.SuperAdmin;
import com.fidz.entr.superadmin.repository.SuperAdminRepository;

@Service
public class LoginService {
	
	@Autowired
	private SuperAdminRepository superAdminRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private DeviceRepository deviceRepository;
	public ResponseEntity<AuthenticationResponse> authenticateUser(Login authenticate) throws UserException, UserNotFoundException
    {
	   	Constant.LOGGER.info("Inside authenticateUser LoginService.java");
        //MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaNames);
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
    	System.out.println("login.getUserName()"+authenticate.getUserName());
    	System.out.println("login.getPassword()"+authenticate.getPassword());
    	System.out.println("@@@login Mode"+authenticate.getLoginMode());
    	//SuperAdmin superAdmin = superAdminRepository.findByPhoneNumber(authenticate.getUserName()).block();
    	SuperAdmin superAdmin = this.superAdminRepository.findByPhoneNumber(authenticate.getUserName());
    	System.out.println("UserMono: " + superAdmin.toString());
    //	SuperAdmin superAdmin = superAdminMono.block(); //TODO: replace block here - important
    //	String schemaName = superAdmin.getSchemaName();
    //	String userName = superAdmin.getUserName();=	
    	System.out.println("superAdmin :: "+superAdmin);
    	if (superAdmin == null){
            throw new UserNotFoundException("While Login","User name is not found");
        }
    	if(superAdmin.isSuperAdmin() == true) {
        	//TODO need to check Password as well
        	System.out.println("inside else if for super Admin");
        	AuthenticationResponse authenticationResponse = new AuthenticationResponse(null,null, true);
        	return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
    		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        }
    	else {
        	System.out.println("inside else");
        	String password = authenticate.getPassword();
        	String phoneNumber = superAdmin.getPhoneNumber();
        	String schemaName = superAdmin.getSchemaName();
        	
        	System.out.println(" schemaName :: "+schemaName);
        	///////new code added
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	//SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(phoneNumber);
        	//System.out.println("securityKey :: "+securityKey);
        	//String decreptPwdAuth = new EncriptionUtil().decrypt(password, securityKey);
        	//System.out.println("decreptPwdAuth :: "+decreptPwdAuth);
        	// TODO Take schema from selected User
        	
        	User user = this.userRepository.findByUserName(authenticate.getUserName());
        	System.out.println("Password is :: "+user.getPassword());
        	//String decreptPwdDb = new EncriptionUtil().decrypt(user.getPassword(), securityKey);
        	/*if (user.getDevice().getDeviceId()==null || user.getDevice().getDeviceId().length() == 0) {
        		Device device = user.getDevice();
        		device.setDeviceId(authenticate.getImei());
        		user.setDevice(device);
        		userRepository.save(user).block();
        	}
        	String userPwd=user.getPassword();
        	if(userPwd.equals(password)) {
        		
        		System.out.println("44444444444444444444444444444");
        		if (!authenticate.getImei().equalsIgnoreCase(user.getDevice().getDeviceId())) {
        			System.out.println("55555555555555555555");
        			throw new UserNotFoundException("IMEI got changed. Contact admin to reset the IMEI");
        		}
        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user);
        		return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        	}
        	else {
        		
        		throw new UserNotFoundException("User name / password not matching");
        	}*/
        	//checking login
        	
        	/*if (user.getDevice().getImei()==null || user.getDevice().getImei().length() == 0) {
        		Device device = user.getDevice();
        		device.setImei(authenticate.getImei());
        		//this.deviceRepository.save(device).block();
        		this.deviceRepository.save(device);
        	}
        	user.setNotificationId(authenticate.getNotificationId());*/
        	String userPwd=user.getPassword();
        	String userName=user.getUserName();
        	String imeiNumber = user.getDevice().getImei();
        	String loginUserName = authenticate.getUserName();
        	String loginPassword = authenticate.getPassword();
        	System.out.println(loginUserName+loginUserName+"datataaaa");
        	System.out.println("userPwd :: "+userPwd);
        	/*if(authenticate.getLoginMode().toString() == "ADMIN" &&loginUserName.equals(userName) && userPwd.equals(loginPassword)) {
        		System.out.println("inside after ADMIN");
        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false);
        		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
        	}
        	else */
        	if(authenticate.getLoginMode().toString() == "ANDROID_LOGIN" && userPwd.equals(password)&& user.getStatus().equals(Status.ACTIVE)) {
        			System.out.println("inside after ANDROID_LOGIN");
        			//boolean isImeiPresent = false;
        			Device deviceObject = deviceService.getDeviceDataByDeviceId(userName,schemaName);
        			List<Device> deviceListObjetByIMEI =  null;
        			try {
        					 deviceListObjetByIMEI =  deviceService.getDeviceDataByImei(authenticate.getImei(), schemaName);
        					 Constant.LOGGER.info("deviceListObjetByIMEI size-------"+deviceListObjetByIMEI.size());
            			if(deviceListObjetByIMEI.size() > 0) {
            				 if(authenticate.getUserName().equals(deviceListObjetByIMEI.get(0).getDeviceId())) {
            					Constant.LOGGER.info("deviceObjetByIMEI-------"+deviceListObjetByIMEI.get(0).getDeviceId());
            			    	user.setNotificationId(authenticate.getNotificationId());
              					this.userRepository.save(user);  // updation of notification ID
              					System.out.println("notify ID   :: "+user.getNotificationId());
              					System.out.println("userPwd 	:: "+userPwd);
              					Constant.LOGGER.info("Right Device------------------");
              					AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false,"success");
              					//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
              					return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
              			}else {
              					Constant.LOGGER.info(" Device Imei got changed------------------");
              					AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, null,false,"IMEI got changed. Contact admin to reset the IMEI");
              					//throw new UserNotFoundException("Check Imei","IMEI got changed. Contact admin to reset the IMEI");
              					return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.ACCEPTED);
              			}
            			}else {
            				if(deviceObject.getImei()!=null) {
            					Constant.LOGGER.info(" Device Imei got changed------------------");
              					AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, null,false,"IMEI got changed. Contact admin to reset the IMEI");
              					//throw new UserNotFoundException("Check Imei","IMEI got changed. Contact admin to reset the IMEI");
              					return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.ACCEPTED);
            				}
            				deviceObject.setImei(authenticate.getImei());
        				 	//this.deviceRepository.save(device).block();
 	            			this.deviceRepository.save(deviceObject);
        	            	user.setNotificationId(authenticate.getNotificationId());
        	            	//System.out.println("notify ID :: "+user.getNotificationId());
        	            	this.userRepository.save(user);  // updation of notification ID
        	            	System.out.println("notify ID   :: "+user.getNotificationId());
        	        		System.out.println("userPwd 	:: "+userPwd);
        	        		Constant.LOGGER.info("New Device-------when IMEI is null-----------");
        	        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false,"success");
        	        		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        	        		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
            			}
        		    	}catch(Exception ex) {
        			    Constant.LOGGER.error("deviceObjetByIMEI DeviceService.java "+ex.getMessage());
        		        }
        		    	
        			
        			
        			//List<Device> devicesList = deviceService.getAllDevicesByUserName(userName,schemaName);
        		//	List<Device> devicesList = deviceService.getAllDeviceData(schemaName);
        			//Constant.LOGGER.info("New Device--------1----------"+devicesList.size());
        		//	HashMap<String,String> deviceHashMapList = new HashMap<String, String>();
//        	if (devicesList.size() > 0 ){
//        		for(int i =0;i<devicesList.size();i++ ) {
//        			if(devicesList.get(i).getImei() != null) {
//        					 deviceHashMapList.put(devicesList.get(i).getImei(), devicesList.get(i).getDeviceId());
//        			} 
//        	}
//        			 
//        			Constant.LOGGER.info("New Device------------------"+deviceHashMapList.get(deviceObject.getImei()));
//        			if(deviceObject !=null && deviceObject.getImei() == null && !authenticate.getUserName().equals(deviceHashMapList.get(authenticate.getImei()))) {
//        				 	deviceObject.setImei(authenticate.getImei());
//        				 	//this.deviceRepository.save(device).block();
// 	            			this.deviceRepository.save(deviceObject);
//        	            	user.setNotificationId(authenticate.getNotificationId());
//        	            	//System.out.println("notify ID :: "+user.getNotificationId());
//        	            	this.userRepository.save(user);  // updation of notification ID
//        	            	System.out.println("notify ID   :: "+user.getNotificationId());
//        	        		System.out.println("userPwd 	:: "+userPwd);
//        	        		Constant.LOGGER.info("New Device-------when IMEI is null-----------");
//        	        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false,"success");
//        	        		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
//        	        		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
//         			}else if(authenticate.getUserName().equals(deviceHashMapList.get(authenticate.getImei()))) {
//         					deviceObject.setImei(authenticate.getImei());
//         					//this.deviceRepository.save(device).block();
//         					this.deviceRepository.save(deviceObject);
//         					user.setNotificationId(authenticate.getNotificationId());
//         					//System.out.println("notify ID :: "+user.getNotificationId());
//         					this.userRepository.save(user);  // updation of notification ID
//         					System.out.println("notify ID   :: "+user.getNotificationId());
//         					System.out.println("userPwd 	:: "+userPwd);
//         					Constant.LOGGER.info("Right Device------------------");
//         					AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false,"success");
//         					//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
//         					return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
//         			}else {
//         					Constant.LOGGER.info(" Device Imei got changed------------------");
//         					AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, null,false,"IMEI got changed. Contact admin to reset the IMEI");
//         					//throw new UserNotFoundException("Check Imei","IMEI got changed. Contact admin to reset the IMEI");
//         					return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.ACCEPTED);
//         			}
//        	}

//        			if(deviceObject !=null && deviceObject.getImei() == null &&(userName.equals(deviceObject.getDeviceId()))) {
//        				Constant.LOGGER.info("New Device------------------");
//        				 AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false,"success");
//                         //return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
//                         return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
//        			}else if(deviceObject.getImei().equals(authenticate.getImei()) && userName.equals(deviceObject.getDeviceId())) {
//        				Constant.LOGGER.info("Right Device------------------");
//        				  AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false,"success");
//                          //return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
//                          return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
//        			}else {
//        				  Constant.LOGGER.info(" Device Imei got changed------------------");
//        				  AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, null,false,"IMEI got changed. Contact admin to reset the IMEI");
//                          //throw new UserNotFoundException("Check Imei","IMEI got changed. Contact admin to reset the IMEI");
//                          return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.ACCEPTED);
//        			}
//        			
        		
        		
        		
        		
        		
        		
//        		   if(authenticate.getUserName().equals(user.getDevice().getDeviceId()) && (user.getDevice().getImei() == null)) {
//                	 
//                  }else if (authenticate.getUserName().equals(user.getDevice().getDeviceId()) && authenticate.getImei().equals(user.getDevice().getImei())) {
//        			
//                  }else {
//                	
//                  }
        	
        	}
        	else if(authenticate.getLoginMode().toString()== "WEB_LOGIN" && userPwd.equals(password)&& user.getStatus().equals(Status.ACTIVE)) {
        		System.out.println("inside after WEB_LOGIN");
        		System.out.println("inside after password");
        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false);
        		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
        	}
        	else if(authenticate.getLoginMode().toString()=="NON_ANDROID_LOGIN" && userPwd.equals(password)&& user.getStatus().equals(Status.ACTIVE)) {
        		System.out.println("inside after password");
        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false);
        		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
        	}
        	else if(authenticate.getLoginMode().toString()== "MINIREPORT_LOGIN" && userPwd.equals(password)&& user.getStatus().equals(Status.ACTIVE)) {
        		System.out.println("inside after password");
        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false);
        		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
        	}
        	else {
        		 System.out.println("inside else");
                 if(user.getStatus().equals(Status.INACTIVE))
                     throw new UserNotFoundException("No User"+"/Must be an ACTIVE User");
        	
        		
        		//throw new ResponseEntity < ErrorResponse > (new ErrorResponse("User name / password not matching"), HttpStatus.BAD_REQUEST);
        		throw new UserNotFoundException("No User"+"User Data not matching");
        	}
        	
        	
        	
        	////////////////////////////////////////////////////////////////////////
        	/*if (user.getDevice().getImei()==null || user.getDevice().getImei().length() == 0) {
        		Device device = user.getDevice();
        		device.setImei(authenticate.getImei());
        		//this.deviceRepository.save(device).block();
        		this.deviceRepository.save(device);
        	}
        	user.setNotificationId(authenticate.getNotificationId());
        	String userPwd=user.getPassword();
        	System.out.println("userPwd :: "+userPwd);
        	if(userPwd.equals(password)) {
        		System.out.println("inside after password");
        		if (!authenticate.getImei().equalsIgnoreCase(user.getDevice().getImei())) {
        			throw new UserNotFoundException("IMEI got changed. Contact admin to reset the IMEI");
        		}
        		AuthenticationResponse authenticationResponse = new AuthenticationResponse(schemaName, user,false);
        		//return Mono.just(new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK));
        		return new ResponseEntity<AuthenticationResponse>(authenticationResponse, HttpStatus.OK);
        	}
        	else {
        		System.out.println("inside else");
        		
        		//throw new ResponseEntity < ErrorResponse > (new ErrorResponse("User name / password not matching"), HttpStatus.BAD_REQUEST);
        		throw new UserNotFoundException("User name / password not matching");
        	}*/
        	
        }
		return null;
		
		
    }
	
	
	 public ResponseEntity<User> resetPassword(ResetPassword resetPassword) {
		   	Constant.LOGGER.info("Inside resetPassword LoginService.java");
          MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		 SuperAdmin superAdmin = this.superAdminRepository.findByPhoneNumber(resetPassword.getUserName());
		 User user =null;
		 if(superAdmin == null) {
			 throw new UserNotFoundException("Data","User Does Not Exit");
		 }else {
			 String schemaName=superAdmin.getSchemaName();
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			 user = this.userRepository.findByUserName(superAdmin.getUserName());
			 user.setPassword(resetPassword.getPassword());
			 this.userRepository.save(user);
			 return ResponseEntity.ok(user);
		 }
		
		
	 }
	
	
}
