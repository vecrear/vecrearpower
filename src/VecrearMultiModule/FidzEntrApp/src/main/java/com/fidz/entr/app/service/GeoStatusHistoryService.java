package com.fidz.entr.app.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.GeoStatusHistory;
import com.fidz.entr.app.model.ResourceAttendanceReport;
import com.fidz.entr.app.repository.GeoStatusHistoryRepository;

@Service("GeoStatusHistoryService")
public class GeoStatusHistoryService {

	private GenericAppINterface<GeoStatusHistory> genericINterface;

	@Autowired
	public GeoStatusHistoryService(GenericAppINterface<GeoStatusHistory> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private GeoStatusHistoryRepository geoStatusHistoryRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	public GeoStatusHistory save(String schemaName, GeoStatusHistory geoStatusHistory) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

		Constant.LOGGER.info("Inside save() GeoStatusHistoryService().java");
		GeoStatusHistory geStatusHistory = null;
		try {
			geStatusHistory = this.geoStatusHistoryRepository.save(geoStatusHistory);
			Constant.LOGGER.info("After save data" + geStatusHistory.toString());
		} catch (Exception ex) {
			Constant.LOGGER.error("Error while getting save() GeoStatusHistoryService().java");
		}
		return geStatusHistory;

	}

	public List<GeoStatusHistory> getAll(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAll() GeoStatusHistoryService().java");

		List<GeoStatusHistory> geoStatusHistories = null;
		geoStatusHistories = this.geoStatusHistoryRepository.findAll();
		return geoStatusHistories;

	}

	public List<GeoStatusHistory> getAllGeoWebPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllGeoWebPaginated GeoStatusHistoryService.java");
		Query query = new Query();
		List<GeoStatusHistory> users = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest);
			users = this.genericINterface.find(query, GeoStatusHistory.class);

		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error occurred inside getAllGeoWebPaginated GeoStatusHistoryService.java " + ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllGeoWebPaginated " + query);
		return users;
	}

	public List<GeoStatusHistory> getAllGeoPaginatedWeb(@Valid ResourceAttendanceReport report, String schemaName)
			throws ParseException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside  getAllGeoPaginatedWeb  according to date");
		// String date_string = "2015-04-17 11:02:49";
		List<String> userid = report.getUsers().stream().map(user -> user.getId()).collect(Collectors.toList());
		String start = report.getStartDate();
		String end = report.getEndDate();
		int page = report.getPageNum();
		int size = report.getPageSiz();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate = format.parse(start);
		Date endDate = format.parse(end);
		// end date as plus 1

		Constant.LOGGER.info("page" + page + "size" + size);
		LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.plusDays(1);
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

		final Pageable pageableRequest = PageRequest.of(page, size);
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").in(userid));
		query.addCriteria(Criteria.where("createdDate").gte(startDate).lte(currentDatePlusOneDay));
		query.with(pageableRequest);
		return this.genericINterface.find(query, GeoStatusHistory.class);
	}

	public GeoStatusHistory getGeoStatusHistoryById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getGeoStatusHistoryById GeoStatusHistoryService.java");
		return this.genericINterface.findById(id, GeoStatusHistory.class);
	}

	public GeoStatusHistory updateGeoStatusHistory(String id, @Valid GeoStatusHistory geoStatusHistory,
			String schemaName) {
		// String schemaName=geoStatusHistory.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		String userId = geoStatusHistory.getUserId();
		geoStatusHistory.setUserId(id);
		Constant.LOGGER.info("Inside updateGeoStatusHistory GeoStatusHistoryService.java");
		return this.geoStatusHistoryRepository.save(geoStatusHistory);
	}

	public Map<String, String> deleteGeoStatusHistory(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside deleteGeoStatusHistory GeoStatusHistoryService.java");
		this.geoStatusHistoryRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "GeoStatusHistory deleted successfully");
		return response;
	}

	public Map<String, String> softDeleteGeoStatusHistory(String id, String schemaName, @Valid TimeUpdate timeUpdate) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param schemaName
	 * @param text
	 * @return
	 */
	public List<GeoStatusHistory> getAllSearchUsersWebPaginated(int pageNumber, int pageSize, String schemaName,
			String text) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<GeoStatusHistory> geoStatusHistories = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside getAllSearchGeoHistoryWebPaginated GeoHistoryService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(GeoStatusHistory.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			geoStatusHistories = mongoTemplate.find(query, GeoStatusHistory.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside getAllSearchGeoHistoryWebPaginated GeoHistoryService.java" + ex.getMessage());

		}
		return geoStatusHistories;
	}
}
