package com.fidz.entr.app.filters.model;
import java.util.Date;
import java.util.List;

import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.User;

import lombok.Data;


@Data
public class CompareActivity {
 
private List<User> users;


private String activityId;

private List<Customer> customers;

private String startDate;


private String endDate;

private String type;


}
