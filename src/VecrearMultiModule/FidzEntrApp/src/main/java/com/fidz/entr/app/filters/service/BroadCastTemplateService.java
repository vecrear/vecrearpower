package com.fidz.entr.app.filters.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.BroadCastTemplate;
import com.fidz.entr.app.filters.repository.BroadCastTemplateRepository;


@Service
public class BroadCastTemplateService {

	private GenericAppINterface<BroadCastTemplate> genericINterface;
	@Autowired
	public BroadCastTemplateService(GenericAppINterface<BroadCastTemplate> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	@Autowired
	BroadCastTemplateRepository broadCastTemplateRepository;
	
	public List<BroadCastTemplate> getAllBroadCastTemplate(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllBroadCastTemplates BroadCastTemplateService.java");
    	List<BroadCastTemplate> broadCastTemplates=null;
    	try {
    		broadCastTemplates=this.genericINterface.findAll(BroadCastTemplate.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllBroadCastTemplates BroadCastTemplateService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllBroadCastTemplates BroadCastTemplateService.java");
    	return broadCastTemplates;
	}

	public BroadCastTemplate createBroadCastTemplate(@Valid BroadCastTemplate broadCastTemplate) {
		   BroadCastTemplate bCastTemplate=null;
		   	try {
		   		Constant.LOGGER.info(" Inside BroadCastTemplateService.java Value for insert BroadCastTemplate record:createBroadCastTemplate :: " + broadCastTemplate);
		   		String schemaName=broadCastTemplate.getSchemaName();
		   	   	
			    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			    bCastTemplate=this.broadCastTemplateRepository.save(broadCastTemplate);
		   	}catch(Exception e) {
		   		Constant.LOGGER.info(" Inside BroadCastTemplateService.java Value for insert BroadCastTemplate record:createBroadCastTemplate :: " + broadCastTemplate);
		   	}
		   	Constant.LOGGER.info("**************************************************************************");
	    	Constant.LOGGER.info("successfully created data from BroadCastTemplate BroadCastTemplateService.java");
		      return bCastTemplate; 
	}

	public BroadCastTemplate getBroadCastTemplateById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside getBroadCastTemplateById BroadCastTemplateService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, BroadCastTemplate.class);
	}

	public BroadCastTemplate updateBroadCastTemplate(String id, @Valid BroadCastTemplate broadCastTemplate) {
    	Constant.LOGGER.info("Inside updateBroadCastTemplate BroadCastTemplateService.java");
       String schemaName=broadCastTemplate.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    String broadCastTemplateId=broadCastTemplate.getId();
	    broadCastTemplate.setId(id);
	    return this.broadCastTemplateRepository.save(broadCastTemplate);
	}

	public Map<String, String> deleteBroadCastTemplate(String id, String schemaName) {
    	Constant.LOGGER.info("Inside deleteBroadCastTemplate BroadCastTemplateService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.broadCastTemplateRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "BroadCastTemplate deleted successfully");
		return response;
	}

	public Map<String, String> softDeletebroadcastTemplateService(String id, String schemaName, TimeUpdate timeUpdate){
    	Constant.LOGGER.info("Inside softDeletebroadcastTemplateService BroadCastTemplateService.java");
     MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	BroadCastTemplate broadCastTemplate = this.genericINterface.findById(id, BroadCastTemplate.class);
	broadCastTemplate.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
	broadCastTemplate.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
	broadCastTemplate.setStatus(Status.INACTIVE);
	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	this.genericINterface.saveName(broadCastTemplate);

	Map<String, String> response = new HashMap<String, String>();
	response.put("message", "broadCastTemplate deleted successfully");
	return response;

	}

}
