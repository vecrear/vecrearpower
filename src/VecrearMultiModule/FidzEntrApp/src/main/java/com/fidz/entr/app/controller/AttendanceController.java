package com.fidz.entr.app.controller;

import java.text.ParseException;


import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.AttendanceNotFoundException;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.IdleTime;
import com.fidz.entr.app.model.LogOffTime;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.model.PunchStatusCount;
import com.fidz.entr.app.model.ResourceAttendanceReport;
import com.fidz.entr.app.payload.ErrorResponse;
import com.fidz.entr.app.service.AttendanceService;
import com.fidz.entr.base.model.Device;

@CrossOrigin(maxAge = 3600)			
@RestController("FEAAttendanceController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/attendance")
public class AttendanceController {
	@Autowired
	private AttendanceService attendanceService;
	
	@Value("${attendanceservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all/web")
    public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendances(@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAllAttendancesweb(schemaName);
	}
	
	//Post request to get all Attendances PaginatedWeb with date
    @PostMapping("/get/all/web/sdate={startdate}|edate={enddate}/{pageNumber}/{pageSize}")
    public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancesPaginatedWeb(@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAllAttendancesPaginatedWeb(startdate,enddate,pageNumber,pageSize,schemaName);
    }
    
    
	@PostMapping("/get/all")
    public List<Attendance> getAllAttendancesweb(@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAllAttendances(schemaName);
    }
	
	@PostMapping("/reports/{userId}/sdate={startdate}|edate={enddate}")
    public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancebasedOnDate(@PathVariable(value="userId") String userId,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAllAttendancebasedOnDate(schemaName,userId,startdate,enddate);
    }
	
	
//	@PostMapping("/get/all/count")
//    public Integer getAllAttendancesCount(@RequestHeader(value="schemaName") String schemaName) {
//    	return attendanceService.getAllAttendancesCount(schemaName);
//    }
//	
//	@PostMapping("/get/all/0")
//    public List<Attendance> getAllAttendances111(@RequestHeader(value="schemaName") String schemaName) {
//    	return attendanceService.getAllAttendances11(schemaName);
//    }
	
	
	@PostMapping
    public Attendance createAttendance(@Valid @RequestBody Attendance attendance) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.createAttendance(attendance);       
    }
	@PostMapping("/creates")
    public void createAttendances(@Valid @RequestBody Attendance attendance) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		attendanceService.createAttendances(attendance);       
    }
	@PostMapping("/get/id/{id}")
    public Attendance getAttendanceById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAttendanceById(id, schemaName);
    }

	
	@PostMapping("/get/attendancestatus/userid/{id}")
    public String getAttendanceStatusById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAttendanceStatusById(id, schemaName);
    }
	
	@PostMapping("/get/punchstatus/{punchstatus}/userid/{userid}/date/{date}")
    public PunchStatusCount getpunchstatusById(@PathVariable(value = "userid") String userid, @PathVariable(value = "date") String date,@PathVariable(value = "punchstatus") String punchstatus,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getpunchstatusById(userid,punchstatus,date, schemaName);
    }
	
	
	@PostMapping("/get/punchstatus/{punchstatus}/admin/date/{date}")
    public PunchStatusCount getpunchstatusForAdmin(@PathVariable(value = "date") String date,@PathVariable(value = "punchstatus") String punchstatus,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getpunchstatusForAdmin(punchstatus,date, schemaName);
    }
	
	
	
	
	@PostMapping("/update/id/{id}")
    public Attendance updateAttendance(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Attendance attendance) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.updateAttendance(id, attendance);
    }
    
    @PatchMapping("/{id}/punchout")
    public Attendance updateAttendancePunchOut(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Attendance attendance,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.updateAttendancePunchOut(id, attendance, schemaName);
    }
    
    @PatchMapping("/{id}/logoff")
    public Attendance updateAttendanceSpecificLogOff(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody List<LogOffTime> logOffTime, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.updateAttendanceSpecificLogOff(id, logOffTime, schemaName);
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteAttendance(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.deleteAttendance(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteAttendance(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.deleteSoftAttendance(id, schemaName, timeUpdate);
    }
    
    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Attendance> streamAllCities() {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.streamAllAttendances();
    }
    
    @PostMapping("/get/user/{id}/sdate={startdate}|edate={enddate}/{pageNumber}/{pageSize}")
    public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancecByUser(@PathVariable(value="id") String id,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAllAttendancecByUser(id,startdate,enddate,pageNumber,pageSize,schemaName);
    }
    @PostMapping("/get/user")
	//@Cacheable(value = "id", key = "#id")
	public List<com.fidz.entr.app.reportattendance.Attendance> getAttendanceByMultiUser(@Valid @RequestBody ResourceAttendanceReport report,@RequestHeader(value="schemaName") String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getAttendanceByMultiUser(report, schemaName);
		
	}
    @PostMapping("/get/punchstatus/userid/{userid}")
    public PunchStatus getpunchstatusByUserId(@PathVariable(value = "userid") String userid,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.getpunchstatusByUserId(userid, schemaName);
    }
    @PostMapping("/get/idletime/userid/{userid}/startdate/{startdate}/endate/{endate}")
    public List<IdleTime> getIdleTimeConfg(@PathVariable(value = "userid") String userid,@PathVariable(value = "startdate") String startdate,@PathVariable(value = "endate")String endate,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Attendanceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.idleTimeConfigurator(userid, startdate, endate, schemaName);
    }  
    
    @PostMapping("/device/reset/id/{id}")
	public Device resetImei(@PathVariable(value = "id") String id,@Valid @RequestBody Device device) {
		System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return attendanceService.updateDeviceData(id,device);
	}

    // Exception Handling Examples
  /* @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A attendanceService with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(AttendanceNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/

}
