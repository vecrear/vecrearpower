package com.fidz.entr.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.Country;

@Repository("FEACountryRepository")
public interface CountryRepository extends MongoRepository<Country, String>{

	public List<Country> findByName(String name);

}
