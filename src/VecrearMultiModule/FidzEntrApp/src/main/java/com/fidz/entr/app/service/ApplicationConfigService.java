package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.ActivityFieldNotFoundException;
import com.fidz.entr.app.exception.ApplicationConfigNotFoundException;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.ApplicationConfig;
import com.fidz.entr.app.repository.ApplicationConfigRepository;
import com.fidz.entr.base.model.Country;


@Service("FEAApplicationConfigService")
public class ApplicationConfigService {
	private GenericAppINterface<ApplicationConfig> genericINterface;
	@Autowired
	public ApplicationConfigService(GenericAppINterface<ApplicationConfig> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private ApplicationConfigRepository applicationConfigRepository;
	
    public List<ApplicationConfig> getAllApplicationConfig(String schemaName) {
    	Constant.LOGGER.info("Inside getAllApplicationConfig ApplicationConfigService.java");
    	List<ApplicationConfig> applicationConfigs=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	applicationConfigs=this.genericINterface.findAll(ApplicationConfig.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllApplicationConfig ApplicationConfigService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllApplicationConfig ApplicationConfigService.java");
    	return applicationConfigs;
    }
	
	public ApplicationConfig createApplicationConfig(ApplicationConfig applicationConfig) {
		ApplicationConfig applicationConfigs = null;
		try {
			Constant.LOGGER.debug(
					" Inside ApplicationConfigService.java Value for insert ApplicationConfig record: createApplicationConfig  :: "
							+ applicationConfig);
			String schemaName = applicationConfig.getSchemaName();

			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

			applicationConfigs = this.applicationConfigRepository.save(applicationConfig);
		} catch (Exception e) {
			Constant.LOGGER.debug(
					" Inside ApplicationConfigService.java Value for insert ApplicationConfig record: createApplicationConfig  :: "
							+ applicationConfig);
		}

		return applicationConfigs;

	}
    public void createApplicationConfigs(ApplicationConfig applicationConfig) {
        String schemaName=applicationConfig.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    
        this.genericINterface.saveName(applicationConfig);
	   
    }
    
    public ApplicationConfig getApplicationConfigById(String id, String schemaName) {
    	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 return this.genericINterface.findById(id, ApplicationConfig.class);
		
    }
	
   /* public ApplicationConfig updateApplicationConfig(String id, ApplicationConfig applicationConfig) {
    	
		String schemaName = applicationConfig.getSchemaName();

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		applicationConfig.setId(id);
		return this.applicationConfigRepository.save(applicationConfig);
	       
        
    }*/
 public ApplicationConfig updateApplicationConfig(String id, ApplicationConfig applicationConfig) {
    	
		String schemaName = applicationConfig.getSchemaName();

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	
		ApplicationConfig applicationConfigs = null;
		applicationConfigs = this.genericINterface.findById(id, ApplicationConfig.class);
		if (applicationConfigs == null) {
			throw new ApplicationConfigNotFoundException(id);
		} else {
			applicationConfig.setId(id);
			return this.applicationConfigRepository.save(applicationConfig);
		}
	       
        
    }
   
   
    //hard delete
   	public Map<String, String> deleteApplicationConfig(String id, String schemaName) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.applicationConfigRepository.deleteById(id);
   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "ApplicationConfig deleted successfully");
   		return response;

   	}
        //soft delete
   	public Map<String, String> deleteSoftApplicationConfig(String id, String schemaName, TimeUpdate timeUpdate) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		ApplicationConfig applicationConfig = this.genericINterface.findById(id, ApplicationConfig.class);
   		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
   		applicationConfig.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
   		applicationConfig.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
   		applicationConfig.setStatus(Status.INACTIVE);
   		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.genericINterface.saveName(applicationConfig);

   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "ApplicationConfig deleted successfully");
   		return response;

   	}
    
    public List<ApplicationConfig> streamAllApplicationConfigs() {
        return applicationConfigRepository.findAll();
    }

    //To get all ApplicationConfigs paginated
    //page number starts from zero
	public List<ApplicationConfig> getAllApplicationConfigPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllApplicationConfigPaginated ApplicationConfigService.java");
    	Query query = new Query();
    	List<ApplicationConfig> applicationConfigs =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	applicationConfigs= this.genericINterface.find(query, ApplicationConfig.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllApplicationConfigPaginated ApplicationConfigService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllApplicationConfigPaginated "+query);
        return applicationConfigs;
	}

}
