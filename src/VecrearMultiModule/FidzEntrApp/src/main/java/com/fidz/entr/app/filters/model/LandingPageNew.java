package com.fidz.entr.app.filters.model;

import java.util.Date;
import java.util.List;

import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.User;

import lombok.Data;

		
@Data
public class LandingPageNew {

	
	private User userId;
	
	private  RequestType activityType;
	
	private ActivityType actId;
	
	private RequestType hierarchy;
	
	private User hierarchyId;
		
	private String startDate;
	
	private String endDate;
	
	
}
