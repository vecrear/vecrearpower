package com.fidz.entr.app.exception;

public class CountryNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3940446580332909180L;

	public CountryNotFoundException(String country) {
		super("Country not found with id/name " + country);
	}
}
