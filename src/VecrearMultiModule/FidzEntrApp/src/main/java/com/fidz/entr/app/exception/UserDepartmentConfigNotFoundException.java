package com.fidz.entr.app.exception;

public class UserDepartmentConfigNotFoundException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5203640118848133876L;

	public UserDepartmentConfigNotFoundException(String userDepartmentConfig) {
		super("UserDepartmentConfig not found with id/name " + userDepartmentConfig);
	}
}
