package com.fidz.entr.app.awsconfig.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.awsconfig.model.APiCountInfo;
import com.fidz.entr.app.awsconfig.model.APiCountLimit;
import com.fidz.entr.app.awsconfig.model.AwsInfo;
import com.fidz.entr.app.exception.ApplicationConfigNotFoundException;
import com.fidz.entr.app.exception.CustomerNotFoundException;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.repository.ApiCountInfoRepository;
import com.fidz.entr.app.repository.ApiCountLimitRepository;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.TaggedRegions;

@Service("FEAAPiCountMaxLimitService")
public class APiCountMaxLimitService {
	
	
	private GenericAppINterface<APiCountLimit> genericINterface;

	@Autowired
	public APiCountMaxLimitService(GenericAppINterface<APiCountLimit> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	@Autowired
	private ApiCountLimitRepository apiCountLimitRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired 
	AwsInfoService awsInfoService;
	

	
	public APiCountLimit maxLimitInfo(@Valid APiCountLimit maxLimitInfo, String schemaName) throws ApiCountLimitNotFoundException {
		APiCountLimit countLimit = null;	
		   MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		   if(maxLimitInfo.getCompanyId() != null) {
			   countLimit = this.apiCountLimitRepository.save(maxLimitInfo);
		   }else {
			   throw new ApiCountLimitNotFoundException("please provide the company deatils");
		   }
		  
		  return countLimit;
	}
	
	public List<APiCountLimit> getAllInfolimitinfo(String schemaName) {
	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
	return this.genericINterface.findAll(APiCountLimit.class);
	}
	
	
	public ResponseEntity<APiCountLimit> getAPiCountLimitId(String id, String schemaName) throws ApiCountLimitNotFoundException {
		APiCountLimit aPiCountLimit=null;
    	Constant.LOGGER.info("Inside getAPiCountLimitId APiCountLimit.java");
         MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
         aPiCountLimit=this.genericINterface.findById(id, APiCountLimit.class);
		if (aPiCountLimit == null) {
			throw new ApiCountLimitNotFoundException(id);
		} else {
			aPiCountLimit.setId(id);
			return ResponseEntity.ok(aPiCountLimit);
		}
	}
	
	
	public APiCountLimit getAPiCountLimitbycompanyId(String companyId, String schemaName) {
		APiCountLimit aPiCountLimit=null;
    	Constant.LOGGER.info("Inside getAPiCountLimitId APiCountLimit.java"+companyId+":"+":"+schemaName);
    	try {
    	      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	      aPiCountLimit = this.apiCountLimitRepository.findByCompanyId(companyId);
    	      return aPiCountLimit;
    	}catch (Exception e) {
			// TODO: handle exception
		}
		return aPiCountLimit;
   
		
	}
	
	

	public @Valid APiCountLimit updateAPiCountLimitId(String id, @Valid APiCountLimit aPiCountLimitinfo) {
		 String schemaName=aPiCountLimitinfo.getSchemaName();
	     	Constant.LOGGER.info("Inside updateCountry CountryService.java");
	     	
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			 aPiCountLimitinfo.setId(id);
			
			 return this.apiCountLimitRepository.save(aPiCountLimitinfo);
	}

	public Map<String, String> deleteAPiCountLimitId(String id,String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside APiCountLimit java");	
  		this.apiCountLimitRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "APiCountLimit  deleted successfully");
  		return response;
	}




}
