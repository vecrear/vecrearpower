package com.fidz.entr.app.awsconfig.model;

import java.util.Date;
import java.util.List;


import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Group;
import com.fidz.entr.app.config.UserData;

import lombok.Data;
@Data
@Document(collection = "APiCountLimit")
@JsonIgnoreProperties(value = { "target" })
public class APiCountLimit {
	@Id
	protected String id;
	protected String companyId;
	protected String schemaName;
	protected String companySchemaName;
	protected int maxiGoAddresscount;
	protected int maxiGeoDistCount;
	protected int maxiSmsUrlCount;
	protected int maxVecMapKeycount;


	
	
	
	

	
	
	
	

}
