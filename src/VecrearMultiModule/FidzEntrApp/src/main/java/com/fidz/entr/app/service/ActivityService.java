package com.fidz.entr.app.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.awsconfig.service.AwsInfoService;
import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.controller.UserNameAppender;
import com.fidz.entr.app.exception.ActivityNotFoundException;

import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.ActivityConfigurationType;
import com.fidz.entr.app.model.ActivityLog;
import com.fidz.entr.app.model.ActivityStatus;
import com.fidz.entr.app.model.ActivityStatusChangeLog;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.ActivityUserList;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.CustomerType;
import com.fidz.entr.app.model.MultiUserActivity;
import com.fidz.entr.app.model.PushNotificationRequest;
import com.fidz.entr.app.model.ResourceAttendanceReport;
import com.fidz.entr.app.model.ScheduleNotification;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserActivityNotify;
import com.fidz.entr.app.repository.ActivityRepository;
import com.fidz.entr.app.repository.ScheduleNotificationRepository;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.services.location.model.AwsInfo;

@Service("FEAActivityService")
public class ActivityService {
	private GenericAppINterface<Activity> genericINterface;

	@Autowired
	public ActivityService(GenericAppINterface<Activity> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private ActivityRepository activityRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private FCMServic fcmServic;

	@Autowired
	private ActivityLogService activityLogService;

	@Autowired
	private AwsInfoService awsInfoService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ScheduleNotificationService scheduleNotificationService;

	@Autowired
	private ScheduleNotificationRepository scheduleNotificationRepository;

	@Autowired
	MongoTemplate mongoTemplate;

	// get all activities(Post Request)

	public List<Activity> getAllActivities(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllActivities ActivityService.java");
		List<Activity> activities = null;
		try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
			activities = this.genericINterface.findAll(Activity.class);
		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error while getting data from getAllActivities ActivityService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully fetched data from getAllActivities ActivityService.java" + activities);
		return activities;
	}

	public Activity createActivity(Activity activity) {
		Activity activitys = null;
		try {
			Constant.LOGGER.info(
					" Inside ActivityLogService.java Value for insert Activity record:createActivity :: " + activity);
			ActivityLogService activityLogs = new ActivityLogService();
			System.out.println("instance created");
			System.out.println("activity log details" + activity.getActivityLogs());
			String schemaName = activity.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			String address = null;
			if (activity.getPerformedLocation().getAddressloc().equals("NA")) {
				String geoaddressKey = null;
				List<com.fidz.entr.app.awsconfig.model.AwsInfo> geokeyList = awsInfoService.getAWSnfoObject(schemaName);
				if (geokeyList.get(0).getGeoAddressKey() != null) {
					geoaddressKey = geokeyList.get(0).getGeoAddressKey();
				}
				address = reverseGeoCoding(activity.getPerformedLocation().getLatitude(),
						activity.getPerformedLocation().getLongitude(), geoaddressKey);
				activity.getPerformedLocation().setAddressloc(address);
				Constant.LOGGER.info("GpsAddress-----addressfrom API------" + address);
			} else {
				Constant.LOGGER.info(" else address is good-------------------------------- :: ");
			}

			activitys = this.activityRepository.save(activity);
			if (activity.getActivityConfigurationType().equals(ActivityConfigurationType.PLANNED)) {
				for (int i = 0; i < activity.getResourceList().size(); i++) {
					if (activity.getResourceList().get(i).getId() != null) {
						Constant.LOGGER
								.error("activity.getResource().getId()---" + activity.getResourceList().get(i).getId());
						User userObject = userService.getUserById(activity.getResourceList().get(i).getId(),
								schemaName);
						UserActivityNotify userActivityNoty = new UserActivityNotify();
						ScheduleNotification scheduleNotification = new ScheduleNotification();

						PushNotificationRequest ppp = PushNotificationRequest.getInstance();
						if ((userObject.getNotificationId() != null)
								&& activity.getActivityStatus().equals(ActivityStatus.OPEN)) {
							userObject = userService.getUserById(activity.getResource().getId(), schemaName);
							Constant.LOGGER.error("user.getNotificationId()----" + userObject.getNotificationId());
							userActivityNoty.setNotificationId(userObject.getNotificationId());
							userActivityNoty.setUserActivityStatus(
									"New Task has been mapped to you by " + activity.getCreatedByUser());
							// -------------------------------scheduler
							// notification-----------------------------------
							DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);

							DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
							Date ds = activity.getPlannedStartDateTime();

							LocalDateTime localDateTime = ds.toInstant().atZone(ZoneId.systemDefault())
									.toLocalDateTime();
							String d = dtf.format(localDateTime);

							LocalDateTime minusminutes = localDateTime.minusMinutes(1);
							Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
							LocalDateTime localDateTime1 = de.toInstant().atZone(ZoneId.systemDefault())
									.toLocalDateTime();
							String dss = dtf.format(localDateTime1);

							Date startDate = format.parse(d);
							long epoch = startDate.getTime();
							Constant.LOGGER.info("epoch" + epoch);
							Date dr = format.parse(dss);
							long epochs = dr.getTime();
							Constant.LOGGER.info("epochs" + epochs);

							scheduleNotification.setSchemaName(schemaName);
							scheduleNotification.setActivityId(activity.getId());
							scheduleNotification.setNotificationid(activity.getResource().getNotificationId());
							scheduleNotification.setUserid(activity.getResource().getId());
							scheduleNotification.setObjectType("Activity");
							scheduleNotification.setSchemaName(schemaName);
							scheduleNotification.setTime(String.valueOf(epoch));
							scheduleNotification.setBeforeTime(String.valueOf(epochs));
							scheduleNotification.setNotifyMessage(
									"Perform an activity before " + activity.getPlannedStartDateTime().toGMTString());
							MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
							this.scheduleNotificationRepository.save(scheduleNotification);
							MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

							Constant.LOGGER.info("userActivityNoty----" + userActivityNoty.toString());
							ppp.setMessage(userActivityNoty.getUserActivityStatus());
							ppp.setTitle("Vecrear-Trac");
							ppp.setToken(userActivityNoty.getNotificationId());
							ppp.setTopic("");
							Constant.LOGGER.info("user.getNotificationId()----" + ppp.toString());
							Thread geoThread = new Thread() {
								public void run() {
									try {
										try {
											fcmServic.sendMessageToToken(ppp);
										} catch (ExecutionException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										Thread.sleep(3);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}
							};

							geoThread.start();
						} else {
							Constant.LOGGER.error("User  not found");
						}

					}
				}
			}

		} catch (Exception e) {
			Constant.LOGGER.error(
					" Inside ActivityLogService.java Value for insert Activity record:createActivity :: " + activity);
		}
		return activitys;

	}

	private String reverseGeoCoding(double latitude, double longitude, String geoaddressKey) {
		String urlPrefix = "https://maps.googleapis.com/maps/api/geocode/json?";
		if (geoaddressKey == null) {
			geoaddressKey = "AIzaSyAlCECFt-Y2fubjPbyabfu0wNqWbyzdNw4";
		}

		Constant.LOGGER.info("\nLatitude: " + latitude + "\nLongitude: " + longitude);
		System.out.println("\nLatitude: " + latitude + "\nLongitude: " + longitude);
		String address = "";
		String status = "";
		String url = urlPrefix + "latlng=" + latitude + "," + longitude + "&key=" + geoaddressKey + "";// AIzaSyBc2Mb1NYQRJa1jKPE0zr-kuYmGxQnZ8js
		System.out.println("\nurl: " + url);
		try {
			URL uriAddress = new URL(url);
			URLConnection res = uriAddress.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(res.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				builder.append(line + "\n");
			}
			String builderArray = builder.toString();
			// System.out.println("\nbuilderArray: \n" + builderArray);
			JSONObject root = (JSONObject) JSONValue.parseWithException(builderArray);
			// System.out.println("\nresult: \n" + root);
			JSONArray resultArray = (JSONArray) root.get("results");
			JSONObject obj0 = (JSONObject) resultArray.get(0);
			// System.out.println("\nobj0: \n" + obj0);
			String formattedAddress = (String) obj0.get("formatted_address");
			Constant.LOGGER.info("formattedAddress------------------" + formattedAddress);
			System.out.println("\nFormattedAddress: \n" + formattedAddress);
			// Accessing status
			String stat = (String) root.get("status");
			status = stat;
			System.out.println("\nStatus: " + status);
			address = formattedAddress;
			return formattedAddress;

		} catch (Exception e) {
			System.out.println("\nException: " + e);
		}
		if (status.contentEquals("ZERO_RESULTS")) {
			address = "No address";
			return address;
		}
		if (status.contentEquals("OVER_QUERY_LIMIT")) {
			address = "Crossed your quota of api calls";
			return address;
		}
		if (status.contentEquals("REQUEST_DENIED")) {
			address = "Request denied";
			return address;
		}
		if (status.contentEquals("INVALID_REQUEST")) {
			address = "Invalid query";
			return address;
		}
		if (status.contentEquals("UNKNOWN_ERROR")) {
			address = "Server Error";
			return address;
		}
		if (address.contentEquals("")) {
			address = "Blank Address";
			return address;
		} else {
			Constant.LOGGER.info(address);
			return address;
		}
	}

	public MultiUserActivity createActivityFoMultiUser(@Valid Activity activity) {

		// List<Activity> act = new ArrayList<Activity>();
		MultiUserActivity multiUserActivity = new MultiUserActivity();
		String result = null;
		User user;
		int q = 1;
		// Activity activitysObject = null;
		try {
			Constant.LOGGER.info(
					" Inside ActivityLogService.java Value for insert Activity record:createActivityFoMultiUser :: "
							+ activity);
			// ActivityLogService activityLogs = new ActivityLogService();
			System.out.println("instance created");
			System.out.println("activity log details" + activity.getActivityLogs());
			String schemaName = activity.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			ScheduleNotification schedule = new ScheduleNotification();
			List<ScheduleNotification> scheduleNotification = new ArrayList<ScheduleNotification>();
			List<Activity> act = new ArrayList<Activity>();
			for (int i = 0; i < activity.getResourceList().size(); i++) {
				user = new User();
				user = this.userService.getUserById(activity.getResourceList().get(i).getId(), schemaName);
				activity.setResource(user);

				Date createdTimestamp = activity.getCreatedTimestamp();
				String createdByUser = activity.getCreatedByUser();
				Date updatedTimestamp = activity.getUpdatedTimestamp();
				String updatedByUser = activity.getUpdatedByUser();
				Date deletedTimestamp = activity.getDeletedTimestamp();
				String deletedByUser = activity.getDeletedByUser();
				Status status = activity.getStatus();
				String id = activity.getId();
				Company company = activity.getCompany();
				Department department = activity.getDepartment();
				ActivityType activityType = activity.getActivityType();
				ActivityConfigurationType activityConfigurationType = activity.getActivityConfigurationType();
				User resource = activity.getResource();
				List<ActivityUserList> resourceList = activity.getResourceList();

				Customer prospectCustomer = null;

				if (activity.getCustomer().getId() == null && activity.getProspectCustomer() != null) {

					Constant.LOGGER.info(" PROSPECTIVE:: ");
					List<User> usersList = userService.getAllUsersWebBasedOnDepartmentandCompanyLatest(
							activity.getDepartment().getId(), activity.getCompany().getId(), activity.getSchemaName(),
							"ACTIVE");
					Constant.LOGGER.info(" usersList:: " + usersList.size());
					if (usersList.size() > 0) {
						for (int k = 0; k < usersList.size(); k++) {
							Constant.LOGGER.info(" usersList.get(i).getId()--------:: " + usersList.get(k).getId());
							activity.getProspectCustomer().setUsers(usersList);
						}
					}

					Constant.LOGGER.info(" prospectCustomer---:: " + activity.getProspectCustomer().toString());
					prospectCustomer = customerService.createCustomer(activity.getProspectCustomer());

					activity.setCustomer(prospectCustomer);

					activity.setProspectCustomer(null);

					Customer customer = activity.getCustomer();
					List<ActivityLog> activityLogs = activity.getActivityLogs();
					ActivityStatus activityStatus = activity.getActivityStatus();
					Date plannedStartDateTime = activity.getPlannedStartDateTime();
					Date plannedEndDateTime = activity.getPlannedEndDateTime();
					Date actualStartDateTime = activity.getActualStartDateTime();
					LocalDateTime localDateTime = actualStartDateTime.toInstant().atZone(ZoneId.systemDefault())
							.toLocalDateTime();
					LocalDateTime minusminutes = localDateTime.plusSeconds(q);
					Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
					Date actualEndDateTime = activity.getActualEndDateTime();
					com.fidz.base.model.Location plannedLocation = activity.getPlannedLocation();
					com.fidz.base.model.Location performedLocation = activity.getPerformedLocation();
					String comments = activity.getComments();
					List<ActivityStatusChangeLog> activityStatusChangeLog = activity.getActivityStatusChangeLog();

					act.add(new Activity(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser,
							deletedTimestamp, deletedByUser, status, schemaName, id, company, department, activityType,
							activityConfigurationType, resource, customer, resourceList, activityLogs, activityStatus,
							plannedStartDateTime, plannedEndDateTime, de, actualEndDateTime, plannedLocation,
							performedLocation, comments, activityStatusChangeLog, null));

					Constant.LOGGER.info(" i:--------------- " + activity.getResourceList().get(i).getId() + " \n "
							+ activity.toString());
					Constant.LOGGER.info(" i:--------------- " + act);
					q++;

				} else if (activity.getCustomer().getId() != null && activity.getProspectCustomer() != null) {
					Constant.LOGGER.info(" PROSPECTIVE:: with existing customer updation ");
					ActivityLogService activityLogs1 = new ActivityLogService();
					System.out.println("instance created");
					System.out.println("activity log details" + activity.getActivityLogs());

					List<User> usersList = userService.getAllUsersWebBasedOnDepartmentandCompanyLatest(
							activity.getDepartment().getId(), activity.getCompany().getId(), activity.getSchemaName(),
							"ACTIVE");
					Constant.LOGGER.info(" usersList:: " + usersList.size());
					if (usersList.size() > 0) {
						for (int m = 0; m < usersList.size(); m++) {
							Constant.LOGGER.info(" usersList.get(i).getId()--------:: " + usersList.get(m).getId());
							activity.getProspectCustomer().setUsers(usersList);
						}
					}

					prospectCustomer = customerService.updateCustomer(activity.getCustomer().getId(),
							activity.getProspectCustomer(), "prospect");

					Constant.LOGGER.info(" prospectCustomer  ++" + prospectCustomer.toString());

					activity.setCustomer(prospectCustomer);
					activity.setProspectCustomer(null);

					Customer customer = activity.getCustomer();
					List<ActivityLog> activityLogs = activity.getActivityLogs();
					ActivityStatus activityStatus = activity.getActivityStatus();
					Date plannedStartDateTime = activity.getPlannedStartDateTime();
					Date plannedEndDateTime = activity.getPlannedEndDateTime();
					Date actualStartDateTime = activity.getActualStartDateTime();
					LocalDateTime localDateTime = actualStartDateTime.toInstant().atZone(ZoneId.systemDefault())
							.toLocalDateTime();
					LocalDateTime minusminutes = localDateTime.plusSeconds(q);
					Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
					Date actualEndDateTime = activity.getActualEndDateTime();
					com.fidz.base.model.Location plannedLocation = activity.getPlannedLocation();
					com.fidz.base.model.Location performedLocation = activity.getPerformedLocation();
					String comments = activity.getComments();
					List<ActivityStatusChangeLog> activityStatusChangeLog = activity.getActivityStatusChangeLog();

					act.add(new Activity(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser,
							deletedTimestamp, deletedByUser, status, schemaName, id, company, department, activityType,
							activityConfigurationType, resource, customer, resourceList, activityLogs, activityStatus,
							plannedStartDateTime, plannedEndDateTime, de, actualEndDateTime, plannedLocation,
							performedLocation, comments, activityStatusChangeLog, null));

					Constant.LOGGER.info(" i:--------------- " + activity.getResourceList().get(i).getId() + " \n "
							+ activity.toString());
					Constant.LOGGER.info(" i:--------------- " + act);
					q++;
				} else {
					Customer customer = activity.getCustomer();
					List<ActivityLog> activityLogs = activity.getActivityLogs();
					ActivityStatus activityStatus = activity.getActivityStatus();
					Date plannedStartDateTime = activity.getPlannedStartDateTime();
					Date plannedEndDateTime = activity.getPlannedEndDateTime();
					Date actualStartDateTime = activity.getActualStartDateTime();
					LocalDateTime localDateTime = actualStartDateTime.toInstant().atZone(ZoneId.systemDefault())
							.toLocalDateTime();
					LocalDateTime minusminutes = localDateTime.plusSeconds(q);
					Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
					Date actualEndDateTime = activity.getActualEndDateTime();
					com.fidz.base.model.Location plannedLocation = activity.getPlannedLocation();
					com.fidz.base.model.Location performedLocation = activity.getPerformedLocation();
					String comments = activity.getComments();
					List<ActivityStatusChangeLog> activityStatusChangeLog = activity.getActivityStatusChangeLog();

					act.add(new Activity(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser,
							deletedTimestamp, deletedByUser, status, schemaName, id, company, department, activityType,
							activityConfigurationType, resource, customer, resourceList, activityLogs, activityStatus,
							plannedStartDateTime, plannedEndDateTime, de, actualEndDateTime, plannedLocation,
							performedLocation, comments, activityStatusChangeLog, null));

					Constant.LOGGER.info(" i:--------------- " + activity.getResourceList().get(i).getId() + " \n "
							+ activity.toString());
					Constant.LOGGER.info(" i:--------------- " + act);
					q++;
				}

			}

			this.activityRepository.saveAll(act);

			for (int i = 0; i < act.size(); i++) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);

				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				Date ds = act.get(i).getPlannedStartDateTime();

				LocalDateTime localDateTime = ds.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				String d = dtf.format(localDateTime);

				LocalDateTime minusminutes = localDateTime.minusMinutes(1);
				Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
				LocalDateTime localDateTime1 = de.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				String dss = dtf.format(localDateTime1);

				Date startDate = format.parse(d);
				long epoch = startDate.getTime();
				Constant.LOGGER.info("epoch" + epoch);
				Date dr = format.parse(dss);
				long epochs = dr.getTime();
				Constant.LOGGER.info("epochs" + epochs);

				String ids = schedule.getId();
				String userid = act.get(i).getResource().getId();
				String activityId = act.get(i).getId();
				String notificationid = act.get(i).getResource().getNotificationId();
				String notifyMessage = "You need to perform an Activity Before" + " "
						+ act.get(i).getPlannedStartDateTime().toGMTString();
				String objectType = "Activity";
				String time = String.valueOf(epoch);
				String beforeTime = String.valueOf(epochs);
				scheduleNotification.add(new ScheduleNotification(ids, schemaName, userid, activityId, notificationid,
						notifyMessage, objectType, time, beforeTime));
			}
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
			this.scheduleNotificationRepository.saveAll(scheduleNotification);
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			// this.activityRepository.saveAll(act);

			result = "task has been created Successfully";

			// activitys=this.activityRepository.save(activity);

			if (activity.getActivityConfigurationType().equals(ActivityConfigurationType.PLANNED)) {
				for (int i = 0; i < activity.getResourceList().size(); i++) {
					if (activity.getResourceList().get(i).getId() != null) {
						Constant.LOGGER
								.info("activity.getResource().getId()---" + activity.getResourceList().get(i).getId());
						User userObject = userService.getUserById(activity.getResourceList().get(i).getId(),
								schemaName);
						UserActivityNotify userActivityNoty = new UserActivityNotify();

						PushNotificationRequest ppp = PushNotificationRequest.getInstance();
						if ((userObject.getNotificationId() != null)
								&& activity.getActivityStatus().equals(ActivityStatus.OPEN)) {
							// userObject = userService.getUserById(activity.getResource().getId(),
							// schemaName);
							Constant.LOGGER.info("user.getNotificationId()----" + userObject.getNotificationId());
							userActivityNoty.setNotificationId(userObject.getNotificationId());
							userActivityNoty.setUserActivityStatus(
									"New Task has been mapped to you by " + activity.getCreatedByUser());

							Constant.LOGGER.info("userActivityNoty----" + userActivityNoty.toString());
							ppp.setMessage(userActivityNoty.getUserActivityStatus());
							ppp.setTitle("Vecrear-Trac");
							ppp.setToken(userActivityNoty.getNotificationId());
							ppp.setTopic("");
							Constant.LOGGER.info("user.getNotificationId()----" + ppp.toString());
							Thread geoThread = new Thread() {
								public void run() {
									try {
										try {
											fcmServic.sendMessageToToken(ppp);
										} catch (ExecutionException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										Thread.sleep(1);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}
							};

							geoThread.start();
						} else {
							Constant.LOGGER.error("User  not found");
						}

					}
				}
			}

		} catch (Exception e) {
			Constant.LOGGER.error(" Inside ActivityLogService.java Value for insert Activity record:createActivity :: "
					+ e.getMessage());
		}

		multiUserActivity.setActivityResponse(result);
		Constant.LOGGER
				.info(" Success Inside ActivityLogService.java Value for insert Activity record:createActivity :: "
						+ multiUserActivity.toString());
		return multiUserActivity;

	}

	public void createActivitys(Activity activity) {
		// ActivityLogService activityLogs = new ActivityLogService();
		System.out.println("instance created");
		System.out.println("activity log details" + activity.getActivityLogs());
		String schemaName = activity.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(activity);

	}

	// get activity by id(Post Request)
	public ResponseEntity<Activity> getActivityById(String id, String schemaName) {
		Constant.LOGGER.info("inside getActivityById ActivityService.java ");
		Activity activity = null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		activity = this.genericINterface.findById(id, Activity.class);
		if (activity == null) {
			throw new ActivityNotFoundException(id);

		} else {
			Constant.LOGGER.info("Activity Data" + activity.toString());
			return ResponseEntity.ok(activity);
		}
		// return this.genericINterface.findById(id,Activity.class);

	}

	public List<Activity> getActivityByDate(String startdate, String enddate, String schemaName) {
		System.out.println("startdate" + startdate + "startdate" + startdate);
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = new Query();
		query.addCriteria(Criteria.where("startDate").gte(startdate).lt(enddate));
		/*
		 * Query query = new Query(Criteria.where("createdTimestamp").elemMatch(
		 * Criteria.where("startDate").lte(startdate) .and("endDate").gte(enddate) ));
		 */

		System.out.println("@@@@@@@queryquery" + query);
		List<Activity> act = this.activityRepository.findByCreatedTimestamp(startdate, enddate);
		System.out.println("@@@@@@@@@@@@@@@@" + act.toString() + "size" + act.size());

		return this.genericINterface.find(query, Activity.class);
		// return this.genericINterface.findOne(query, Activity.class);
		// return this.activityRepository.findByCreatedTimestamp(startdate, enddate);

	}

	public List<Activity> getActivityByDepartment(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getActivityByDepartment ActivityService.java ");
		return this.activityRepository.findByDepartment(id);

	}

	public List<Activity> getActivityByUser(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getActivityByUser ActivityService.java ");

		return this.activityRepository.findByResource(id);

	}

	// post request for get activity PlannedActivities By UserId
	public List<Activity> getPlannedActivitiesByUserId(String userId, String schemaName) {
		List<Activity> listplanned = null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getPlannedActivitiesByUserId ActivityService.java ");

		if (userId == null) {
			throw new ActivityNotFoundException(userId);
		} else {
//			 return ResponseEntity.ok(this.genericINterface.findAll(Activity.class).stream().filter(activity ->activity.getResource().getId().contains(userId) && activity.getActivityStatus().equals(ActivityStatus.valueOf("OPEN")) || activity.getActivityStatus().equals(ActivityStatus.valueOf("POSTPONED"))
//						&&  activity.getActivityConfigurationType().equals(ActivityConfigurationType.valueOf("PLANNED"))));
//		

			Collection type = new ArrayList();
			type.add("POSTPONED");
			type.add("OPEN");

			Collection plantype = new ArrayList();
			plantype.add("PLANNED");
			plantype.add("SELFPLANNED");

			Query query1 = new Query().addCriteria(Criteria.where("activityStatus").in(type)
					.andOperator(Criteria.where("activityConfigurationType").in(plantype)).and("resource.id")
					.is(userId));
			return listplanned = this.mongoTemplate.find(query1, Activity.class);
		}
	}

	public Activity submitPlannedActivity(String id, List<ActivityLog> activityLog, String schemaName) {
		Constant.LOGGER.info("inside submitPlannedActivity ActivityService.java ");

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// Activity activity = activityRepository.findById(id).block();
		Activity activity = this.genericINterface.findById(id, Activity.class);
		if (activity != null) {
			activity.setActivityLogs(activityLog);
		}

		return this.activityRepository.save(activity);

	}

	public Activity updateActivity(String id, Activity activity) throws ParseException {
		Constant.LOGGER.info("inside updateActivity ActivityService.java ");

		String schemaName = activity.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		activity.setId(id);

		// ---------------------to update the user activity status
		// Constant.LOGGER.error("activity.getResource().getId()---"+activity.getResource().getId());
		User userObject = null;

		PushNotificationRequest ppp = PushNotificationRequest.getInstance();
		UserActivityNotify userActivityNoty = new UserActivityNotify();
		ScheduleNotification scheduleNotification = null;
		Query q = new Query();
		if (activity.getActivityConfigurationType().equals(ActivityConfigurationType.PLANNED)) {

			userObject = userService.getUserById(activity.getResource().getId(), schemaName);

			if (userObject.getManagerMobile() != null) {
				User userManger = userService.getAllUserByuserName(userObject.getManagerMobile(), schemaName);
				// userObject = userService.getUserById(activity.getResource().getId(),
				// schemaName);
//					------------------------to get the manger details---------------------------
				if (userManger.getNotificationId() != null) {
					Constant.LOGGER.error("user.getNotificationId()----" + userManger.getNotificationId());
					userActivityNoty.setNotificationId(userManger.getNotificationId());
					if (activity.getActivityStatus().equals(ActivityStatus.POSTPONED)) {
						userActivityNoty
								.setUserActivityStatus(userObject.getName().getFirstName() + " Postponed the task");
						MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");

						q.addCriteria(Criteria.where("activityId").is(activity.getId()));
						scheduleNotification = this.mongoTemplate.findOne(q, ScheduleNotification.class);
						if (scheduleNotification != null) {
							DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);
							DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
							Date ds = activity.getPlannedStartDateTime();
							LocalDateTime localDateTime = ds.toInstant().atZone(ZoneId.systemDefault())
									.toLocalDateTime();
							String d = dtf.format(localDateTime);
							LocalDateTime minusminutes = localDateTime.minusMinutes(1);
							Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
							LocalDateTime localDateTime1 = de.toInstant().atZone(ZoneId.systemDefault())
									.toLocalDateTime();
							String dss = dtf.format(localDateTime1);
							Date startDate = format.parse(d);
							long epoch = startDate.getTime();
							Constant.LOGGER.info("epoch" + epoch);
							Date dr = format.parse(dss);
							long epochs = dr.getTime();
							Constant.LOGGER.info("epochs" + epochs);

							scheduleNotification.setNotifyMessage(ds.toGMTString());
							scheduleNotification.setTime(String.valueOf(epoch));
							scheduleNotification.setBeforeTime(String.valueOf(epochs));
							scheduleNotification.setId(scheduleNotification.getId());
							this.scheduleNotificationRepository.save(scheduleNotification);
							Constant.LOGGER.info("scheduleNotification updated successfully");

						}

						MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
					} else if (activity.getActivityStatus().equals(ActivityStatus.CANCELLED)) {
						userActivityNoty
								.setUserActivityStatus(userObject.getName().getFirstName() + " Cancelled the task");

						MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
						q.addCriteria(Criteria.where("activityId").is(activity.getId()));
						scheduleNotification = this.mongoTemplate.findOne(q, ScheduleNotification.class);
						if (scheduleNotification != null) {
							this.scheduleNotificationRepository.deleteById(scheduleNotification.getId());
							Constant.LOGGER.info("scheduleNotification deleted successfully");
						}

						MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

					} else if (activity.getActivityStatus().equals(ActivityStatus.COMPLETED)) {
						userActivityNoty
								.setUserActivityStatus(userObject.getName().getFirstName() + " Completed the task");
						scheduleNotification = this.mongoTemplate.findOne(q, ScheduleNotification.class);
						// this.scheduleNotificationRepository.deleteById(scheduleNotification.getId());
						if (scheduleNotification != null) {
							this.scheduleNotificationRepository.deleteById(scheduleNotification.getId());
							Constant.LOGGER.info("scheduleNotification deleted successfully");
						}
					}
					MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

					Constant.LOGGER.error("userActivityNoty----" + userActivityNoty.toString());
					ppp.setMessage(userActivityNoty.getUserActivityStatus());
					ppp.setTitle("Vecrear-Trac");
					ppp.setToken(userActivityNoty.getNotificationId());
					ppp.setTopic("");
					Constant.LOGGER.error("user.getNotificationId()----" + ppp.toString());
					Thread geoThread = new Thread() {
						public void run() {

							try {

								try {
									fcmServic.sendMessageToToken(ppp);
								} catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								Thread.sleep(3);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					};

					geoThread.start();

				} else {
					Constant.LOGGER.error("User manger   not found");
				}
			}

		} else {
			Constant.LOGGER.error("not planned");
		}

		return this.activityRepository.save(activity);

	}

	// hard delete
	public Map<String, String> deleteActivity(String id, String schemaName) {
		Constant.LOGGER.info("inside deleteActivity ActivityService.java ");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.activityRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Activity deleted successfully");
		return response;

	}

	// soft delete
	public Map<String, String> deleteSoftActivity(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside deleteSoftActivity ActivityService.java ");
		Activity activity = this.genericINterface.findById(id, Activity.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		activity.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		activity.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		activity.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(activity);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Activity deleted successfully");
		return response;

	}

	public List<Activity> streamAllActivities() {
		return activityRepository.findAll();
	}

	public List<Activity> getAllActivityPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllActivityPaginated ActivityService.java ");
		final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
		Query query = new Query();
		query.with(pageableRequest);
		return this.genericINterface.find(query, Activity.class);

	}

	// Query with date and Pagination to get activities for web

	public List<com.fidz.entr.app.reportactivities.Activity> getAllActivityPaginatedWeb(String startdate,
			String enddate, int pageNumber, int pageSize, String schemaName) throws ParseException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside get activities paginated according to date");
		// String date_string = "2015-04-17 11:02:49";
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate = format.parse(startdate);
		Date endDate = format.parse(enddate);
		// end date as plus 1
		LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.plusDays(1);
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

		final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
		Query query = new Query();
		query.addCriteria(Criteria.where("plannedStartDateTime").gte(startDate).lte(currentDatePlusOneDay));
		query.with(pageableRequest);
		return this.genericINterface.find(query, com.fidz.entr.app.reportactivities.Activity.class);
	}

	public List<Activity> findByDateRange(String startdate, String enddate, String schemaName) throws ParseException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside findByDateRange ActivityService.java ");

		String date_string = "2015-04-17 11:02:49";
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate = format.parse(startdate);
		Date endDate = format.parse(enddate);

		Query query1 = new Query().addCriteria(Criteria.where("plannedStartDateTime").gte(startDate).lte(endDate));

		Query query = new Query(Criteria.where("schemaName").is(schemaName).andOperator(
				Criteria.where("plannedStartDateTime").lte(endDate),
				Criteria.where("plannedStartDateTime").gte(startDate)));

		// Query query = new
		// Query().addCriteria(Criteria.where("updatedOn").gt(startdate).lte(enddate));
		return this.genericINterface.find(query, Activity.class);

		/*
		 * Query query = new Query();
		 * query.addCriteria(Criteria.where("date").gt(lowerBound)
		 * .andOperator(Criteria.where("date").lt(upperBound))); return
		 * this.genericINterface.find(query, Activity.class);
		 */
		// return this.activityRepository.findByPlannedStartDateTimeBetween(startdate,
		// enddate);
	}

	public String getCustomerByActivityId(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getCustomerByActivityId ActivityService.java ");

		Activity activity = null;
		activity = this.genericINterface.findById(id, Activity.class);

		String custName = activity.getCustomer().getName();
		System.out.println("$$$$$$$$$$$$$$$$$$$$$$$" + custName);
		return custName;
	}

	public List<com.fidz.entr.app.reportactivities.Activity> getAllActivitiesWeb(String schemaName) {
		List<com.fidz.entr.app.reportactivities.Activity> activitiesList = null;
//    	try {
//    		 Constant.LOGGER.info("Activities"); 
//    		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//    		Constant.LOGGER.info("Inside getAllActivitiesWeb ActivityService.java");
//    	    	
//    		Collection<String> type = new ArrayList<String>();
//		     type.add("ACTIVE");
//		     type.add("DRAFT");
//		     type.add("INACTIVE");
//    		//activitiesList = this.genericINterface.findAll(com.fidz.entr.app.reportactivities.Activity.class);
//    		
//    		Query query1 = new Query().addCriteria(Criteria.where("status").in(type));
//    		return  this.mongoTemplate.find(query1, com.fidz.entr.app.reportactivities.Activity.class);  
//    	}catch(Exception ex) {
//    		Constant.LOGGER.error("Error while getting data from getAllActivitiesWeb ActivityService.java"+ex.getMessage());
//        }
//
//    	return activitiesList;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllActivitiesWeb ActivityService.java ");

		activitiesList = this.genericINterface.findAll(com.fidz.entr.app.reportactivities.Activity.class);
		if (activitiesList == null) {
			Constant.LOGGER.info("Activities not found-----");
			throw new ActivityNotFoundException("Activities not found");
		}
		{
			Constant.LOGGER.info("Success");
			return activitiesList;
		}

		// return activitiesList ;

	}

	public List<com.fidz.entr.app.reportactivities.Activity> getActivitiesBasedOnDate(String schemaName, String userId,
			String startdate, String enddate) {
		List<com.fidz.entr.app.reportactivities.Activity> list = null;
		Constant.LOGGER.info("Activities");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getActivitiesBasedOnDate ActivityService.java ");

		Query query = null;

		try {
			// String date_string = "2019-10-17"; // the date format
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			Date startDate = format.parse(startdate);
			Date endDate = format.parse(enddate);

			// Query query1 = new
			// Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
			Query query1 = new Query().addCriteria(
					Criteria.where("actualStartDateTime").gte(startDate).lte(endDate).and("resource.id").is(userId));
			// query1.with(new Sort(Sort.Direction.DESC,"date_time"));
			Constant.LOGGER.info("Query" + query1);
			return this.mongoTemplate.find(query1, com.fidz.entr.app.reportactivities.Activity.class);
		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred" + ex);
		}
		return list;
	}

	public List<com.fidz.entr.app.reportactivities.Activity> getTinyPlannedActivitiesByUserId(String userId,
			String schemaName) {

		Constant.LOGGER.error("------------------userId--" + userId);
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getTinyPlannedActivitiesByUserId ActivityService.java ");

		List<com.fidz.entr.app.reportactivities.Activity> list = null;
		if (userId == null) {
			Constant.LOGGER.error("------------------entered to get the planned activities");
			throw new ActivityNotFoundException(userId);
		} else {
			Constant.LOGGER.error("------------------userId--" + userId);
			Collection<String> type = new ArrayList<String>();
			type.add("POSTPONED");
			type.add("OPEN");

			Collection plantype = new ArrayList();
			plantype.add("PLANNED");
			plantype.add("SELFPLANNED");

			Query query1 = new Query().addCriteria(Criteria.where("activityStatus").in(type)
					.andOperator(Criteria.where("activityConfigurationType").in(plantype))

					.and("resource.id").is(userId));
			list = this.mongoTemplate.find(query1, com.fidz.entr.app.reportactivities.Activity.class);
			if (list == null) {
				Constant.LOGGER.info("Activities not found-----");
				throw new ActivityNotFoundException("Activities not found");
			}
			
				Constant.LOGGER.info("Success" + list.toString());
				return list;
		

		}
	}

	public List<com.fidz.entr.app.reportactivities.Activity> getActivityByResourse(String startdate, String enddate,
			int pageNumber, int pageSize, String id, String schemaName) throws ParseException {

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside get getActivityByResourse paginated according to date");
		// String date_string = "2015-04-17 11:02:49";
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate = format.parse(startdate);
		Date endDate = format.parse(enddate);
		// end date as plus 1
		LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.plusDays(1);
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

		final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
		Query query = new Query();
		query.addCriteria(Criteria.where("resource.id").is(id));
		query.addCriteria(Criteria.where("plannedStartDateTime").gte(startDate).lte(currentDatePlusOneDay));
		query.with(pageableRequest);
		return this.genericINterface.find(query, com.fidz.entr.app.reportactivities.Activity.class);

	}

	public List<com.fidz.entr.app.reportactivities.Activity> getActivityByMultiResourse(
			@Valid ResourceAttendanceReport report, String schemaName) throws ParseException {

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside get getActivityByResourse userid based multiselect paginated according to date");
		// String date_string = "2015-04-17 11:02:49";
		List<String> userid = report.getUsers().stream().map(user -> user.getId()).collect(Collectors.toList());
		String start = report.getStartDate();
		String end = report.getEndDate();
		int page = report.getPageNum();
		int size = report.getPageSiz();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate = format.parse(start);
		Date endDate = format.parse(end);
		// end date as plus 1

		Constant.LOGGER.info("page" + page + "size" + size);
		LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.plusDays(1);
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

		final Pageable pageableRequest = PageRequest.of(page, size);
		Query query = new Query();
		query.addCriteria(Criteria.where("resource.id").in(userid));
		query.addCriteria(Criteria.where("actualStartDateTime").gte(startDate).lte(currentDatePlusOneDay));
		query.with(Sort.by(Sort.Direction.ASC, "plannedStartDateTime"));
		query.with(pageableRequest);
		return this.genericINterface.find(query, com.fidz.entr.app.reportactivities.Activity.class);

	}

	public Map<String, String> getCustomerActivityStatus(String customerid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("customerid---------------" + customerid + "schemaName------------" + schemaName);

		Collection<String> type = new ArrayList<String>();
		type.add("POSTPONED");
		type.add("OPEN");

		List<com.fidz.entr.app.reportactivities.Activity> listActivities = null;
		Map<String, String> response = new HashMap<String, String>();
		Query query = new Query();
		query.addCriteria(
				Criteria.where("customer.id").in(customerid).andOperator(Criteria.where("activityStatus").in(type)));
		try {
			listActivities = this.genericINterface.find(query, com.fidz.entr.app.reportactivities.Activity.class);
			Constant.LOGGER.info("listActivities---------------" + listActivities.size());
			if (listActivities.size() > 0) {
				Constant.LOGGER.info("listActivities---------------" + listActivities.size());
				response.put("message", "Customer is having Pending Activities");
			} else {

				response.put("message", "Customer is no more attached with activities ");
				return response;

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// TODO Auto-generated method stub
		return response;
	}

	public Map<String, String> getUserActivityStatus(String userid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Collection<String> type = new ArrayList<String>();
		type.add("POSTPONED");
		type.add("OPEN");

		List<com.fidz.entr.app.reportactivities.Activity> listActivities = null;
		Map<String, String> response = new HashMap<String, String>();
		Query query = new Query();

		query.addCriteria(
				Criteria.where("resource.id").in(userid).andOperator(Criteria.where("activityStatus").in(type)));
		try {
			listActivities = this.genericINterface.find(query, com.fidz.entr.app.reportactivities.Activity.class);
			if (listActivities.size() > 0) {
				response.put("message", "User is having Pending Activities");
				return response;

			} else {
				response.put("message", "User is no more attached with activities");
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// TODO Auto-generated method stub
		return response;
	}

	public Map<String, String> getActivityTypeStatus(String activityTypeid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Collection<String> type = new ArrayList<String>();
		type.add("POSTPONED");
		type.add("OPEN");

		List<Activity> listActivities = null;
		Map<String, String> response = new HashMap<String, String>();
		Query query = new Query();

		query.addCriteria(Criteria.where("activityType.id").in(activityTypeid)
				.andOperator(Criteria.where("activityStatus").in(type)));
		Constant.LOGGER.info("query status activity type" + query);
		try {
			listActivities = this.genericINterface.find(query, Activity.class);
			if (listActivities.size() > 0) {
				response.put("message", "ActivityType is having Pending Activities");
				return response;

			} else {
				response.put("message", "ActivityType is no more attached with activities");
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public Activity createActivityForProspect(@Valid Activity activity) {

		Activity activitys = null;
		Customer prospectCustomer = null;
		try {
			if (activity.getCustomer().getId() == null && activity.getProspectCustomer() != null) {
				Constant.LOGGER.info(" PROSPECTIVE:: ");
				List<User> usersList = userService.getAllUsersWebBasedOnDepartmentandCompanyLatest(
						activity.getDepartment().getId(), activity.getCompany().getId(), activity.getSchemaName(),
						"ACTIVE");
				Constant.LOGGER.info(" usersList:: " + usersList.size());
				if (usersList.size() > 0) {
					for (int i = 0; i < usersList.size(); i++) {
						Constant.LOGGER.info(" usersList.get(i).getId()--------:: " + usersList.get(i).getId());
						activity.getProspectCustomer().setUsers(usersList);
					}
				}

				Constant.LOGGER.info(" prospectCustomer---:: " + activity.getProspectCustomer().toString());
				prospectCustomer = customerService.createCustomer(activity.getProspectCustomer());

				activity.setCustomer(prospectCustomer);

				activity.setProspectCustomer(null);
				Constant.LOGGER.info(" activity  setting propspect ---:: " + activity.toString());
				ActivityLogService activityLogs = new ActivityLogService();
				System.out.println("instance created");
				System.out.println("activity log details" + activity.getActivityLogs());
				String schemaName = activity.getSchemaName();
				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

				if (activity.getPerformedLocation().getAddressloc().equals("NA")) {
					String geoaddressKey = null;
					List<com.fidz.entr.app.awsconfig.model.AwsInfo> geokeyList = awsInfoService
							.getAWSnfoObject(schemaName);
					if (geokeyList.get(0) != null) {
						geoaddressKey = geokeyList.get(0).getGeoAddressKey();
					}
					String address = reverseGeoCoding(activity.getPerformedLocation().getLatitude(),
							activity.getPerformedLocation().getLongitude(), geoaddressKey);
					activity.getPerformedLocation().setAddressloc(address);
					Constant.LOGGER.info("GpsAddress-----addressfrom API------" + address);
				} else {
					Constant.LOGGER.info(" else address is good-------------------------------- :: ");
				}

				activitys = this.activityRepository.save(activity);

				if (activity.getActivityConfigurationType().equals(ActivityConfigurationType.PLANNED)) {
					for (int i = 0; i < activity.getResourceList().size(); i++) {
						if (activity.getResourceList().get(i).getId() != null) {
							Constant.LOGGER.error(
									"activity.getResource().getId()---" + activity.getResourceList().get(i).getId());
							User userObject = userService.getUserById(activity.getResourceList().get(i).getId(),
									schemaName);
							UserActivityNotify userActivityNoty = new UserActivityNotify();
							ScheduleNotification scheduleNotification = new ScheduleNotification();

							PushNotificationRequest ppp = PushNotificationRequest.getInstance();
							if ((userObject.getNotificationId() != null)
									&& activity.getActivityStatus().equals(ActivityStatus.OPEN)) {
								userObject = userService.getUserById(activity.getResource().getId(), schemaName);
								Constant.LOGGER.error("user.getNotificationId()----" + userObject.getNotificationId());
								userActivityNoty.setNotificationId(userObject.getNotificationId());
								userActivityNoty.setUserActivityStatus(
										"New Task has been mapped to you by " + activity.getCreatedByUser());
								// -------------------------------scheduler
								// notification-----------------------------------
								DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);

								DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
								Date ds = activity.getPlannedStartDateTime();

								LocalDateTime localDateTime = ds.toInstant().atZone(ZoneId.systemDefault())
										.toLocalDateTime();
								String d = dtf.format(localDateTime);

								LocalDateTime minusminutes = localDateTime.minusMinutes(1);
								Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
								LocalDateTime localDateTime1 = de.toInstant().atZone(ZoneId.systemDefault())
										.toLocalDateTime();
								String dss = dtf.format(localDateTime1);
								Date startDate = format.parse(d);
								long epoch = startDate.getTime();
								Constant.LOGGER.info("epoch" + epoch);
								Date dr = format.parse(dss);
								long epochs = dr.getTime();
								Constant.LOGGER.info("epochs" + epochs);
								scheduleNotification.setSchemaName(schemaName);
								scheduleNotification.setActivityId(activity.getId());
								scheduleNotification.setNotificationid(activity.getResource().getNotificationId());
								scheduleNotification.setUserid(activity.getResource().getId());
								scheduleNotification.setObjectType("Activity");
								scheduleNotification.setSchemaName(schemaName);
								scheduleNotification.setTime(String.valueOf(epoch));
								scheduleNotification.setBeforeTime(String.valueOf(epochs));
								scheduleNotification.setNotifyMessage("Perform an activity before "
										+ activity.getPlannedStartDateTime().toGMTString());
								MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
								this.scheduleNotificationRepository.save(scheduleNotification);
								MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

								Constant.LOGGER.info("userActivityNoty----" + userActivityNoty.toString());
								ppp.setMessage(userActivityNoty.getUserActivityStatus());
								ppp.setTitle("Vecrear-Trac");
								ppp.setToken(userActivityNoty.getNotificationId());
								ppp.setTopic("");
								Constant.LOGGER.info("user.getNotificationId()----" + ppp.toString());
								Thread geoThread = new Thread() {
									public void run() {
										try {
											try {
												fcmServic.sendMessageToToken(ppp);
											} catch (ExecutionException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											Thread.sleep(3);
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								};

								geoThread.start();
							} else {
								Constant.LOGGER.error("User  not found");
							}

						}
					}
				}

			} else if (activity.getCustomer().getId() != null && activity.getProspectCustomer() != null) {

				Constant.LOGGER.info(" PROSPECTIVE:: with existing customer updation ");
				ActivityLogService activityLogs = new ActivityLogService();
				System.out.println("instance created");
				System.out.println("activity log details" + activity.getActivityLogs());

				List<User> usersList = userService.getAllUsersWebBasedOnDepartmentandCompanyLatest(
						activity.getDepartment().getId(), activity.getCompany().getId(), activity.getSchemaName(),
						"ACTIVE");
				Constant.LOGGER.info(" usersList:: " + usersList.size());
				if (usersList.size() > 0) {
					for (int i = 0; i < usersList.size(); i++) {
						Constant.LOGGER.info(" usersList.get(i).getId()--------:: " + usersList.get(i).getId());
						activity.getProspectCustomer().setUsers(usersList);
					}
				}

//				Customer validate = this.genericINterface.findById(activity.getCustomer().getId(), Customer.class);
//				Customer customer = activity.getProspectCustomer();
//		    	String cusname1 = UserNameAppender.userNameAppend(validate.getContactName().getFirstName(), validate.getContactName().getMiddleName(), validate.getContactName().getMiddleName());
//		    	String cusname2 = UserNameAppender.userNameAppend(customer.getContactName().getFirstName(), customer.getContactName().getMiddleName(), customer.getContactName().getMiddleName());
//
//		    	
//				Constant.LOGGER.info(" validate.getName()"+validate.getName());
//				Constant.LOGGER.info(" customer.getName()"+customer.getName());
//				Constant.LOGGER.info(" validate.getContact().getPhoneNo()"+validate.getContact().getPhoneNo());
//				Constant.LOGGER.info(" customer.getContact().getPhoneNo()"+customer.getContact().getPhoneNo() );
//				Constant.LOGGER.info(" cusname1.equalsIgnoreCase(cusname2)" +cusname1.equalsIgnoreCase(cusname2));
//				Constant.LOGGER.info(" validate.getContact().getEmail()"+validate.getContact().getEmail());
//				Constant.LOGGER.info(" validate .getAddress().getLine1()"+validate .getAddress().getLine1() );
//				Constant.LOGGER.info(" customer.getAddress().getLine1()"+customer.getAddress().getLine1());
//				Constant.LOGGER.info("validate.getCity().getId()"+validate.getCity().getId());
//				Constant.LOGGER.info("customer.getCity().getId()"+customer.getCity().getId());
//				Constant.LOGGER.info(" cusname1"+cusname1 );
//				Constant.LOGGER.info(" cusname2"+cusname2 );

//		    	if(validate.getName().equalsIgnoreCase(customer.getName())|| 
//		    			(validate.getContact().getPhoneNo().equals(customer.getContact().getPhoneNo())||
//		    					cusname1.equalsIgnoreCase(cusname2)||validate.getContact().getEmail().equalsIgnoreCase(customer.getContact().getEmail())|| validate .getAddress().getLine1().equalsIgnoreCase(customer.getAddress().getLine1()||validate.getCity().getId().equalsIgnoreCase(customer.getCity().getId())))

				prospectCustomer = customerService.updateCustomer(activity.getCustomer().getId(),
						activity.getProspectCustomer(), "prospect");

				Constant.LOGGER.info(" prospectCustomer  ++" + prospectCustomer.toString());

				activity.setCustomer(prospectCustomer);
				activity.setProspectCustomer(null);
				String schemaName = activity.getSchemaName();

				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

				if (activity.getPerformedLocation().getAddressloc().equals("NA")) {
					String geoaddressKey = null;
					List<com.fidz.entr.app.awsconfig.model.AwsInfo> geokeyList = awsInfoService
							.getAWSnfoObject(schemaName);
					if (geokeyList.get(0).getGeoAddressKey() != null) {
						geoaddressKey = geokeyList.get(0).getGeoAddressKey();
					}
					String address = reverseGeoCoding(activity.getPerformedLocation().getLatitude(),
							activity.getPerformedLocation().getLongitude(), geoaddressKey);
					activity.getPerformedLocation().setAddressloc(address);
					Constant.LOGGER.info("GpsAddress-----addressfrom API------" + address);
				} else {
					Constant.LOGGER.info(" else address is good-------------------------------- :: ");
				}

				activitys = this.activityRepository.save(activity);

				if (activity.getActivityConfigurationType().equals(ActivityConfigurationType.PLANNED)) {
					for (int i = 0; i < activity.getResourceList().size(); i++) {
						if (activity.getResourceList().get(i).getId() != null) {
							Constant.LOGGER.error(
									"activity.getResource().getId()---" + activity.getResourceList().get(i).getId());
							User userObject = userService.getUserById(activity.getResourceList().get(i).getId(),
									schemaName);
							UserActivityNotify userActivityNoty = new UserActivityNotify();
							ScheduleNotification scheduleNotification = new ScheduleNotification();

							PushNotificationRequest ppp = PushNotificationRequest.getInstance();
							if ((userObject.getNotificationId() != null)
									&& activity.getActivityStatus().equals(ActivityStatus.OPEN)) {
								userObject = userService.getUserById(activity.getResource().getId(), schemaName);
								Constant.LOGGER.error("user.getNotificationId()----" + userObject.getNotificationId());
								userActivityNoty.setNotificationId(userObject.getNotificationId());
								userActivityNoty.setUserActivityStatus(
										"New Task has been mapped to you by " + userObject.getManagerName());
								// -------------------------------scheduler
								// notification-----------------------------------
								DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);

								DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
								Date ds = activity.getPlannedStartDateTime();

								LocalDateTime localDateTime = ds.toInstant().atZone(ZoneId.systemDefault())
										.toLocalDateTime();
								String d = dtf.format(localDateTime);

								LocalDateTime minusminutes = localDateTime.minusMinutes(1);
								Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
								LocalDateTime localDateTime1 = de.toInstant().atZone(ZoneId.systemDefault())
										.toLocalDateTime();
								String dss = dtf.format(localDateTime1);
								Date startDate = format.parse(d);
								long epoch = startDate.getTime();
								Constant.LOGGER.info("epoch" + epoch);
								Date dr = format.parse(dss);
								long epochs = dr.getTime();
								Constant.LOGGER.info("epochs" + epochs);
								scheduleNotification.setSchemaName(schemaName);
								scheduleNotification.setActivityId(activity.getId());
								scheduleNotification.setNotificationid(activity.getResource().getNotificationId());
								scheduleNotification.setUserid(activity.getResource().getId());
								scheduleNotification.setObjectType("Activity");
								scheduleNotification.setSchemaName(schemaName);
								scheduleNotification.setTime(String.valueOf(epoch));
								scheduleNotification.setBeforeTime(String.valueOf(epochs));
								scheduleNotification.setNotifyMessage("Perform an activity before "
										+ activity.getPlannedStartDateTime().toGMTString());
								MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
								this.scheduleNotificationRepository.save(scheduleNotification);
								MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

								Constant.LOGGER.info("userActivityNoty----" + userActivityNoty.toString());
								ppp.setMessage(userActivityNoty.getUserActivityStatus());
								ppp.setTitle("Vecrear-Trac");
								ppp.setToken(userActivityNoty.getNotificationId());
								ppp.setTopic("");
								Constant.LOGGER.info("user.getNotificationId()----" + ppp.toString());
								Thread geoThread = new Thread() {
									public void run() {
										try {
											try {
												fcmServic.sendMessageToToken(ppp);
											} catch (ExecutionException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											Thread.sleep(3);
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								};

								geoThread.start();
							} else {
								Constant.LOGGER.error("User  not found");
							}

						}
					}
				}

			} else if (activity.getCustomer().getId() != null && activity.getProspectCustomer() == null) {

				Constant.LOGGER.info(" PROSPECTIVE:: with existing user ");
				ActivityLogService activityLogs = new ActivityLogService();
				System.out.println("instance created");
				System.out.println("activity log details" + activity.getActivityLogs());
				String schemaName = activity.getSchemaName();
				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

				if (activity.getPerformedLocation().getAddressloc().equals("NA")) {
					String geoaddressKey = null;
					List<com.fidz.entr.app.awsconfig.model.AwsInfo> geokeyList = awsInfoService
							.getAWSnfoObject(schemaName);
					if (geokeyList.get(0).getGeoAddressKey() != null) {
						geoaddressKey = geokeyList.get(0).getGeoAddressKey();
					}
					String address = reverseGeoCoding(activity.getPerformedLocation().getLatitude(),
							activity.getPerformedLocation().getLongitude(), geoaddressKey);
					activity.getPerformedLocation().setAddressloc(address);
					Constant.LOGGER.info("GpsAddress-----addressfrom API------" + address);
				} else {
					Constant.LOGGER.info(" else address is good-------------------------------- :: ");
				}
				activity.setProspectCustomer(null);
				activitys = this.activityRepository.save(activity);

				if (activity.getActivityConfigurationType().equals(ActivityConfigurationType.PLANNED)) {
					for (int i = 0; i < activity.getResourceList().size(); i++) {
						if (activity.getResourceList().get(i).getId() != null) {
							Constant.LOGGER.error(
									"activity.getResource().getId()---" + activity.getResourceList().get(i).getId());
							User userObject = userService.getUserById(activity.getResourceList().get(i).getId(),
									schemaName);
							UserActivityNotify userActivityNoty = new UserActivityNotify();
							ScheduleNotification scheduleNotification = new ScheduleNotification();

							PushNotificationRequest ppp = PushNotificationRequest.getInstance();
							if ((userObject.getNotificationId() != null)
									&& activity.getActivityStatus().equals(ActivityStatus.OPEN)) {
								userObject = userService.getUserById(activity.getResource().getId(), schemaName);
								Constant.LOGGER.error("user.getNotificationId()----" + userObject.getNotificationId());
								userActivityNoty.setNotificationId(userObject.getNotificationId());
								userActivityNoty.setUserActivityStatus(
										"New Task has been mapped to you by " + userObject.getManagerName());
								// -------------------------------scheduler
								// notification-----------------------------------
								DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);

								DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
								Date ds = activity.getPlannedStartDateTime();

								LocalDateTime localDateTime = ds.toInstant().atZone(ZoneId.systemDefault())
										.toLocalDateTime();
								String d = dtf.format(localDateTime);

								LocalDateTime minusminutes = localDateTime.minusMinutes(1);
								Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
								LocalDateTime localDateTime1 = de.toInstant().atZone(ZoneId.systemDefault())
										.toLocalDateTime();
								String dss = dtf.format(localDateTime1);
								Date startDate = format.parse(d);
								long epoch = startDate.getTime();
								Constant.LOGGER.info("epoch" + epoch);
								Date dr = format.parse(dss);
								long epochs = dr.getTime();
								Constant.LOGGER.info("epochs" + epochs);
								scheduleNotification.setSchemaName(schemaName);
								scheduleNotification.setActivityId(activity.getId());
								scheduleNotification.setNotificationid(activity.getResource().getNotificationId());
								scheduleNotification.setUserid(activity.getResource().getId());
								scheduleNotification.setObjectType("Activity");
								scheduleNotification.setSchemaName(schemaName);
								scheduleNotification.setTime(String.valueOf(epoch));
								scheduleNotification.setBeforeTime(String.valueOf(epochs));
								scheduleNotification.setNotifyMessage("Perform an activity before "
										+ activity.getPlannedStartDateTime().toGMTString());
								MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
								this.scheduleNotificationRepository.save(scheduleNotification);
								MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

								Constant.LOGGER.info("userActivityNoty----" + userActivityNoty.toString());
								ppp.setMessage(userActivityNoty.getUserActivityStatus());
								ppp.setTitle("Vecrear-Trac");
								ppp.setToken(userActivityNoty.getNotificationId());
								ppp.setTopic("");
								Constant.LOGGER.info("user.getNotificationId()----" + ppp.toString());
								Thread geoThread = new Thread() {
									public void run() {
										try {
											try {
												fcmServic.sendMessageToToken(ppp);
											} catch (ExecutionException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
											Thread.sleep(3);
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								};

								geoThread.start();
							} else {
								Constant.LOGGER.error("User  not found");
							}

						}
					}
				}

			} else {
				Constant.LOGGER.info("it is not of type customer PROSPECTIVE ");
			}

		} catch (Exception e) {
			Constant.LOGGER.error(
					" Inside ActivityLogService.java Value for insert Activity record:createActivity :: " + activity);
		}
		return activitys;

	}

}
