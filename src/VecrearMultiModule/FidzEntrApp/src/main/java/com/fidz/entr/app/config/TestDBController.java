package com.fidz.entr.app.config;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/dbtest")
public class TestDBController {
	@Autowired
	private TestDbService testDbService;

	@PostMapping("/get/all")
    public List<TestDB> getAllTestDBs(@RequestHeader(value="schemaName") String schemaName) {
    return testDbService.getAllTestDBs(schemaName);
    }
    @PostMapping
    public void createTestDB(@Valid @RequestBody TestDB testDB) {
    	testDbService.createTestDB(testDB);
    }
    @PostMapping("/creates")
    public TestDB createTestDBs(@Valid @RequestBody TestDB testDB) {
        return testDbService.createTestDBs(testDB);
    }

}
