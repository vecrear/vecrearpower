package com.fidz.entr.app.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "timezoneinfo")
@JsonIgnoreProperties(value = { "target" })
public class TimeZoneInfo {
	
	@Id
	private String id;
	
	private String schemaName;
	
	private String deptTimeZone;

}
