package com.fidz.entr.app.filters.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.BroadCastScheduler;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.Location;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.service.AttendanceService;
import com.fidz.entr.app.service.UserService;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.service.DeviceService;

@Service
public class ExceltoDbService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private AttendanceService attendanceService;
	
	public  Attendance createAttendance(String schemaName) throws IOException{
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Attendance attend = null;
		try {
		Constant.LOGGER.info("inside createAttendance ExceltoDbService.java");
			FileInputStream inp = new FileInputStream("E:\\Data Migration\\NewDataMig\\attendances.xlsx");
		Workbook wb = new XSSFWorkbook(inp); 
		Sheet sheet = wb.getSheet("Sheet1");
		Iterator<Row> rows = sheet.iterator();
	   List<Attendance> attendances2 = new ArrayList<Attendance>();
		 int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();

	        // skip header
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }

	        Iterator<Cell> cellsInRow = currentRow.iterator();
		
	        Attendance attendance = new Attendance();
	        Attendance attendances = new Attendance();
	        
	        
	       Location lc = new Location();
	       Location lcs = new Location();
	        
	        int cellIdx = 0;
	        while (cellsInRow.hasNext()) {
	        Cell currentCell = cellsInRow.next();
	        
	        switch (cellIdx) {
			case 2:
				Constant.LOGGER.info("case 2"+currentCell.getDateCellValue() );
				Date date = currentCell.getDateCellValue();
				
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
				LocalDateTime localDateTime1 = date.toInstant().atZone(ZoneId.of("GMT+05:30")).toLocalDateTime();
		        format.setTimeZone(TimeZone.getTimeZone("GMT"));
		        String dss = dtf.format(localDateTime1);
		        Date endDate = format.parse(dss);
		        Constant.LOGGER.info("Format"+format);
		       // Date dss = format.parse(source)
		          
				//Instant inst = getDate(currentCell.getDateCellValue());
				//Date date1 = Date.from(inst);
				//LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.of("IST")).toLocalDateTime();
               //  Date de = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()); 
				attendance.setCreatedTimestamp(endDate);
			     	attendance.setCalenderDatestamp(endDate);
			     	attendance.setPunchInDateTimeStamp(endDate);
				break;

			case 7:
				Constant.LOGGER.info("case 7"+currentCell.getNumericCellValue() );

		     	lc.setLatitude(currentCell.getNumericCellValue());
			break;
			
			case 8:
				Constant.LOGGER.info("case 8"+currentCell.getNumericCellValue() );

		     	lc.setLongitude(currentCell.getNumericCellValue());
			break;
			
			case 9:
				Constant.LOGGER.info("case 9"+currentCell.getStringCellValue());
		     	lc.setAddress(currentCell.getStringCellValue());
			break;
			case 3:
				
				Constant.LOGGER.info("case 3"+currentCell.getDateCellValue() );
				Date dates = currentCell.getDateCellValue();
				DateTimeFormatter dtfs = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
				SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
				LocalDateTime localDateTime1s = dates.toInstant().atZone(ZoneId.of("GMT+05:30")).toLocalDateTime();
		        formats.setTimeZone(TimeZone.getTimeZone("GMT"));
		        String dsss = dtfs.format(localDateTime1s);
		        Date endDates = formats.parse(dsss);
		        attendances.setPunchOutDateTimeStamp(endDates);
			     
				break;

			case 11:
				Constant.LOGGER.info("case 11"+currentCell.getNumericCellValue() );

		     	lcs.setLatitude(currentCell.getNumericCellValue());
			break;
			
			case 12:
				Constant.LOGGER.info("case 12"+currentCell.getNumericCellValue() );

		     	lcs.setLongitude(currentCell.getNumericCellValue());
			break;
			
			case 13:
				Constant.LOGGER.info("case 13"+currentCell.getStringCellValue());

		     	lcs.setAddress(currentCell.getStringCellValue());
			break;
			
			
			case 18:
				currentCell.setCellType(Cell.CELL_TYPE_STRING);
				String d =currentCell.getStringCellValue() ;
				//d.replaceAll("\\.", "");
				
				Constant.LOGGER.info("case 18"+" "+ d);
				Device devID = deviceService.getDeviceDataByDeviceId(d, schemaName);
				lc.setDeviceId(devID);
				
				User us = userService.getAllUserByuserName(d, schemaName);
				attendance.setSchemaName(schemaName);
				attendance.setCreatedByUser(devID.getCreatedByUser());
				attendance.setUser(us);
				attendance.setPunchInLocation(lc);
				attendances.setPunchInPerTheDay(true);
                attendance.setPunchOutLocation(lcs);
				attendances.setPunchInPerTheDay(false);
				break;
			
			
			default:
				break;
			}
	        
	       
	        
		 	      cellIdx++;
	      }
	        if(attendance!=null) 
	        
		 	       attend = attendanceService.createAttendance(attendance);
	               attendances = attend;
	           attendanceService.updateAttendance(attend.getId(), attendances);
	        Constant.LOGGER.info(" created Attendance Id"+ attend.getId());
		 	       
	        
		        }
	      for (Attendance attendance2 : attendances2) {
	    	  int i = 1;
			attend = attendanceService.createAttendance(attendance2);
			//updateAttendance(attend.getId(), schemaName,attend,i);
			i++;
		
	    
	      }
	      wb.close();
		}catch(Exception ex) {
			Constant.LOGGER.info("Error while creating Attendance"+ ex.getMessage());
		}
		return attend;
	}







public  Attendance updateAttendance(String id , String schemaName, Attendance attendance) throws IOException{
	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	Constant.LOGGER.info("schemaName"+schemaName);
	Attendance attends = null;
	try {
	//List<Attendance> attendances = null;
	InputStream inp = new FileInputStream("E:\\Data Migration\\NewDataMig\\attendances.xlsx");
	Workbook wb = new XSSFWorkbook(inp); 
	Sheet sheet = wb.getSheet("Sheet1");
	Iterator<Row> rows = sheet.iterator();
	//List<Attendance> attendances2 = new ArrayList<Attendance>();
	 int rowNumber = 0;
      while (rows.hasNext()) {
        Row currentRow = rows.next();

        // skip header
        if (rowNumber == 0) {
          rowNumber++;
          continue;
        }

        Iterator<Cell> cellsInRow = currentRow.iterator();
	
        //Attendance attendance = new Attendance();
        
       Location lc = new Location();
        
        int cellIdx = 0;
        while (cellsInRow.hasNext()) {
        Cell currentCell = cellsInRow.next();
        
        switch (cellIdx) {
		case 3:
			
			Constant.LOGGER.info("case 3"+currentCell.getDateCellValue() );
			Date date = currentCell.getDateCellValue();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
			LocalDateTime localDateTime1 = date.toInstant().atZone(ZoneId.of("GMT+05:30")).toLocalDateTime();
	        format.setTimeZone(TimeZone.getTimeZone("GMT"));
	        String dss = dtf.format(localDateTime1);
	        Date endDate = format.parse(dss);
		     attendance.setPunchOutDateTimeStamp(endDate);
		     
			break;

		case 11:
			Constant.LOGGER.info("case 11"+currentCell.getNumericCellValue() );

	     	lc.setLatitude(currentCell.getNumericCellValue());
		break;
		
		case 12:
			Constant.LOGGER.info("case 12"+currentCell.getNumericCellValue() );

	     	lc.setLongitude(currentCell.getNumericCellValue());
		break;
		
		case 13:
			Constant.LOGGER.info("case 13"+currentCell.getStringCellValue());

	     	lc.setAddress(currentCell.getStringCellValue());
		break;
		
		case 18:
			currentCell.setCellType(Cell.CELL_TYPE_STRING);
			Constant.LOGGER.info("case 18"+currentCell.getStringCellValue() );
			String d =currentCell.getStringCellValue();
			Device devID = deviceService.getDeviceDataByDeviceId(d, schemaName);
			Constant.LOGGER.info("******"+d);
			lc.setDeviceId(devID);
			attendance.setSchemaName(schemaName);
			attendance.setCreatedByUser(devID.getCreatedByUser());
			attendance.setPunchOutLocation(lc);
			attendance.setPunchInPerTheDay(false);
			break;
		
		
		default:
			break;
		}
        cellIdx++;
        
        }
        if(attendance!=null)  
	        Constant.LOGGER.info(" created Attendance Id"+ id);

            attends = attendanceService.updateAttendancePunchOut(id, attendance, schemaName);
            
      }
      wb.close();
	}catch(Exception ex) {
		Constant.LOGGER.info("Error while creating Attendance"+ ex.getMessage());
	}
	return attends;
}


}