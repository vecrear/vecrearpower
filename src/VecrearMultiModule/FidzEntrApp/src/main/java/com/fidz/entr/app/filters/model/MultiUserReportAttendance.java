package com.fidz.entr.app.filters.model;

import java.util.List;

import com.fidz.entr.app.model.User;

import lombok.Data;

@Data
public class MultiUserReportAttendance {

	
private List<User> users;

private String startDate;

private String endDate;

}
