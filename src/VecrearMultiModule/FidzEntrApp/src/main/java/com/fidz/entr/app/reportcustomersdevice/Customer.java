package com.fidz.entr.app.reportcustomersdevice;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Base;
import com.fidz.base.model.Contact;
import com.fidz.base.model.PersonName;
import com.fidz.base.model.Status;
import com.fidz.entr.app.model.CustomerType;
import com.fidz.entr.app.model.CustomerTypeField;
import com.fidz.entr.app.reportcustomersweb.SimplifiedBase;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Customer extends SimplifiedBase {
	@Id
	protected String id;
	
	@NotNull
	protected String name;
	
	protected String customerId;
	
	protected PersonName contactName;
	
	protected Contact contact;
	
	protected Address address;
	
	protected CustomerType customerType;
	
	protected String customerDepartment;
	
	@JsonIgnore
	@DBRef(lazy = true)
	protected Company company;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Department department;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Country country;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Region region;
	@JsonIgnore
	@DBRef(lazy = true)
	protected State state;
	
	@DBRef(lazy = true)
	protected City city;
	@DBRef(lazy = true)
	protected CustomerCategory customerCategory;
	//optional mapping based on customer type - for regular customer this field is mandatory
	@DBRef(lazy = true)
	protected List<User> users;
	@JsonIgnore
	protected Map<String, Object> additionalFields;

	
	



	
}
