package com.fidz.entr.app.exception;

public class CustomerCategoryNameFoundException  extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

 public CustomerCategoryNameFoundException(String custcategory) {
    super("Customer Category name already Exits " + custcategory);

}
	
	


}
