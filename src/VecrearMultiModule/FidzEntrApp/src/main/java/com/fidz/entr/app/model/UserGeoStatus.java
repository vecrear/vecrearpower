package com.fidz.entr.app.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "UserGeoStatus")   
@JsonIgnoreProperties(value = { "target" })
public class UserGeoStatus {

	
	@DateTimeFormat
	private Date createdDate;
	
	@Id
	private String userId;
	
	private String notificationId;
	
	private String deviceId;
	
	private String geoStatus;
	
	private String userMangermongoId;
	
	public static UserGeoStatus userGeoStatus = new UserGeoStatus();

	
	
	public UserGeoStatus(Date createdDate, String userId, String notificationId, String deviceId, String geoStatus,String userMangermongoId) {
		super();
		this.createdDate = createdDate;
		this.userId = userId;
		this.notificationId = notificationId;
		this.deviceId = deviceId;
		this.geoStatus = geoStatus;
		this.userMangermongoId = userMangermongoId;
	}


	public UserGeoStatus() {
		

	}

	public static UserGeoStatus getUserGeoStatus() {
		return userGeoStatus;
	}

	public static void setUserGeoStatus(UserGeoStatus userGeoStatus) {
		UserGeoStatus.userGeoStatus = userGeoStatus;
	}

	
	
	

	
	
}
