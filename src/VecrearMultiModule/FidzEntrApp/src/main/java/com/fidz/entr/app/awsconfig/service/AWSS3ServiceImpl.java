package com.fidz.entr.app.awsconfig.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fidz.base.validator.Constant;

@Service("FEAAwsAWSS3ServiceImpl")
public class AWSS3ServiceImpl implements AWSS3Service {
 
    //private static final Logger LOGGER = LoggerFactory.getLogger(AWSS3ServiceImpl.class);
 
    @Autowired
    private AmazonS3 amazonS3;
    
    @Value("${aws.s3.bucket}")
    private String bucketName = "fidzenterprisedev";
    
    
 
    @Override
    // @Async annotation ensures that the method is executed in a different background thread 
    // but not consume the main thread.
  //  @Async
    public String uploadFile(final MultipartFile multipartFile) {
    	
    	String Url = null; 
    	 Constant.LOGGER.info("File upload in progress.");
        try {
            final File file = convertMultiPartFileToFile(multipartFile);
            Url = uploadFileToS3Bucket(bucketName, file);
            Constant.LOGGER.info("File upload is completed.");
          
            file.delete();  // To remove the file locally created in the project folder.
        } catch (final AmazonServiceException ex) {
        	 Constant.LOGGER.info("File upload is failed.");
        	 Constant.LOGGER.error("Error= {} while uploading file.", ex.getMessage());
        }
        return Url;
    }
 
    private File convertMultiPartFileToFile(final MultipartFile multipartFile) {
        final File file = new File(multipartFile.getOriginalFilename());
        try (final FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(multipartFile.getBytes());
        } catch (final IOException ex) {
        	 Constant.LOGGER.error("Error converting the multi-part file to file= ", ex.getMessage());
        }
        return file;
    }
 
    private String uploadFileToS3Bucket(final String bucketName, final File file) {
    	
    	  String  uniqueFileId = UUID.randomUUID().toString();
        //final String uniqueFileName =  file.getName();
       final String uniqueFileName =uniqueFileId+ LocalDateTime.now() + "_" + file.getName();
        
        Constant.LOGGER.info("Uploading file with name= " + uniqueFileName);
        final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uniqueFileName, file);
        
        amazonS3.putObject(putObjectRequest);
        
        Constant.LOGGER.info("Uploading file with name= " + uniqueFileId+uniqueFileName);
		return uniqueFileName;
    }

	@Override
	public byte[] getTheConvertedImageFromByteCode(String imageConvertionFile) {
		// TODO Auto-generated method stub
		
		 byte[] base64Val = null;
		 
		 
		return base64Val;
	}

	@Override
	public String uploadFileforS3(File files) {
	  	String Url = null; 
   	 Constant.LOGGER.info("File upload in progress.");
       try {
           final File file = files;
           Url = uploadFileToS3Bucket(bucketName, file);
           Constant.LOGGER.info("File upload is completed.");
           file.delete();  // To remove the file locally created in the project folder.
       } catch (final AmazonServiceException ex) {
       	 Constant.LOGGER.info("File upload is failed.");
       
       }
       return Url;
	}

	@Override
	@Async
	public String uploadFiles(BufferedImage bufferedImage,String path) {
		String Url = null; 
   	 Constant.LOGGER.info("File upload in progress.");
       try {
           final File file = convertMultiBufToFile(bufferedImage);
           Url = uploadFileToS3Bucket("fidzenterprisedev/"+path, file);
           Url=path+"/"+UUID.randomUUID().toString()+"_"+LocalDateTime.now()+"_"+file.getName();
           Constant.LOGGER.info("File upload is completed.");
           file.delete();  // To remove the file locally created in the project folder.
       } catch (final AmazonServiceException ex) {
       	 Constant.LOGGER.info("File upload is failed.");
       	 Constant.LOGGER.error("Error= {} while uploading file.", ex.getMessage());
       }
       return Url;
	}

	
	
	 private File convertMultiBufToFile(final BufferedImage bufferedImage) {
		 
		 	File outputfile = new File("image.jpg");
		 	try {
				ImageIO.write(bufferedImage, "jpg", outputfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 
//	        final File file = new File(bufferedImage.getF());
//	        try (final FileOutputStream outputStream = new FileOutputStream(file)) {
//	            outputStream.write(bufferedImage.getBytes());
//	        } catch (final IOException ex) {
//	        	 Constant.LOGGER.error("Error converting the multi-part file to file= ", ex.getMessage());
	//        }
		 	Constant.LOGGER.info("outputfile-----------"+outputfile.getName());
	        return outputfile;
	    }



}
