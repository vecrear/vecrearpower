package com.fidz.entr.app.configs;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fidz.base.payload.ErrorResponse1;
import com.fidz.entr.app.exception.ActivityFieldNotFoundException;
import com.fidz.entr.app.exception.ActivityNotFoundException;
import com.fidz.entr.app.exception.ActivityStatusChangeLogNotFoundException;
import com.fidz.entr.app.exception.ActivityTypeNotFoundException;
import com.fidz.entr.app.exception.AttendanceNotFoundException;
import com.fidz.entr.app.exception.CustomerNotFoundException;
import com.fidz.entr.app.exception.ServerException;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.base.exception.CompanyNotFoundException;

@SuppressWarnings({"unchecked","rawtypes"})
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler{
	
    Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);
	
//	@ExceptionHandler(Exception.class)
    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		  logger.info("CustomExceptionHandler--"+request.toString());
        List<String> details = new ArrayList<>();
        logger.info("CustomExceptionHandler============"+ex.getLocalizedMessage());
        details.add(ex.getLocalizedMessage());
        logger.info("CustomExceptionHandler---------------"+ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("Server Error", details);
        logger.info("CustomExceptionHandler-------"+error.getMessage());
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
//	@ExceptionHandler(ServerException.class)
//	 public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
//		  logger.info("CustomExceptionHandler--"+request.toString());
//      List<String> details = new ArrayList<>();
//      logger.info("CustomExceptionHandler============"+ex.getLocalizedMessage());
//      details.add(ex.getLocalizedMessage());
//      logger.info("CustomExceptionHandler---------------"+ex.getLocalizedMessage());
//      ErrorResponse1 error = new ErrorResponse1("Server Error", details);
//      logger.info("CustomExceptionHandler-------"+error.getMessage());
//      return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
//  }
	
	
 
    @ExceptionHandler(ActivityFieldNotFoundException.class)
    public final ResponseEntity<Object> handleUserNotFoundException(ActivityFieldNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("ActivityField Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(ActivityTypeNotFoundException.class)
    public final ResponseEntity<Object> handleActivityTypeNotFoundException(ActivityTypeNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("ActivityType Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(ActivityNotFoundException.class)
    public final ResponseEntity<Object> handleActivityNotFoundException(ActivityNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("Activity Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(CustomerNotFoundException.class)
    public final ResponseEntity<Object> handleCustomerNotFoundException(ActivityNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("Customer Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    //UserNotFoundException
    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("User Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(CompanyNotFoundException.class)
    public final ResponseEntity<Object> handleCompanyNotFoundException(CompanyNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(ActivityStatusChangeLogNotFoundException.class)
    public final ResponseEntity<Object> handleActivityStatusChangeLogNotFoundException(ActivityStatusChangeLogNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("ActivityStatus Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(AttendanceNotFoundException.class)
    public final ResponseEntity<Object> handleAttendanceNotFoundException(AttendanceNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("AttendanceNot Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ErrorResponse1 error = new ErrorResponse1("Validation Failed", details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }
    
}
