package com.fidz.entr.app.filters.service;

import java.io.ByteArrayInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.AlphaReports;
import com.fidz.entr.app.filters.repository.AlphaReportsRepository;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.service.DepartmentService;
import com.fidz.entr.superadmin.service.SuperAdminService;

@Service
public class AlphaReportSchedulingService {

	



	 @Autowired
	 AlphaReportsRepository alphaReportsRepository;

	 @Autowired
	 private MailService mailService;
	 
	 @Autowired
	 private MongoTemplate mongoTemplate;
	 
	 @Autowired
	 private AlphaReportsService alphaReportsService;
	 
	 @Autowired
	 private GenerateExcelService generateExcelService;
	 
	 private DepartmentService departmentService;
	 
	
	private GenericAppINterface<AlphaReports> genericINterface;
	@Autowired
	public AlphaReportSchedulingService(GenericAppINterface<AlphaReports> genericINterface){
		this.genericINterface=genericINterface;	
		}
	
	 @Scheduled(fixedRate = 30000)
	public String alphReportScheduling() throws ParseException, FileNotFoundException, IOException {
		 
		
		   
		   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");  
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
         Query q = new Query();
         List<AlphaReports> alphaReports= null;
         //departmentService.getAllTimeZoneInfos(schemaName)
         //format.setTimeZone(TimeZone.getTimeZone("GMT"));
        // LocalDateTime now = LocalDateTime.now(ZoneId.of("GMT+05:30"));
         LocalDateTime now = LocalDateTime.now();
           String d = dtf.format(now);
           Date endDate = format.parse(d);
           long epoch = endDate.getTime();
           String epochTime = String.valueOf(epoch);
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");     
       q.addCriteria(Criteria.where("scheduleDateTime").is(epochTime));
       Constant.LOGGER.info("Query"+q);
       alphaReports = this.mongoTemplate.find(q, AlphaReports.class);
	        Constant.LOGGER.info("*********Time sceduler running*********"+String.valueOf(epoch));
	        
	        Constant.LOGGER.info("schedlerList*****"+alphaReports);
		
	        if(alphaReports.size()>0) {
	        	Constant.LOGGER.info("Found matching Date");
	        for(int i=0;i<alphaReports.size();i++) {
	        	if(alphaReports.get(i).getEmailSent().equals("false")) {
	        	for(int j=0;j<alphaReports.get(i).getSendEmailTo().size();j++) {
	               // MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(alphaReports.get(i).getSchemaName());     
			     	Constant.LOGGER.info("send email");  
			     	String email = alphaReports.get(i).getSendEmailTo().get(j).getEmail();
			     	String message = alphaReports.get(i).getMessage();
			     	String subject = alphaReports.get(i).getSubject();
			     	String userid  = alphaReports.get(i).getSendEmailTo().get(j).getUserid();
			     	String userMobile = alphaReports.get(i).getSendEmailTo().get(j).getPhoneNo();
                    String schemaNames = alphaReports.get(i).getSchemaName();
			     	List<Activity> alpha= this.alphaReportsService.filterAlphaReports(alphaReports.get(i));
			     	Constant.LOGGER.info("send email"+alpha.toString());
			    	Constant.LOGGER.info("send email"+alpha.toString());
			    	
			    	if(alpha.isEmpty()) {
			    		  Thread geoThread = new Thread() {
							    public void run() {
							    	   try {  	
							    		 mailService.sendEmail(email, subject, "Alpha Report - No data Avaialble");
										Thread.sleep(15);
									} catch (InterruptedException e) {
										e.printStackTrace();
									} catch (Exception e) {
										e.printStackTrace();
									}	
							    }  
							};
							geoThread.start();
			    	}
			    	
			    	else{
			    	generateExcelService.generateExcel(alpha,userid,userMobile,schemaNames);
                    Thread geoThread = new Thread() {
					    public void run() {
					    	   try {  	
					    		 mailService.sendEmailWithAttachment(email, subject, message,userid,userMobile,schemaNames);
								Thread.sleep(15);
							} catch (InterruptedException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}	
					    }  
					};
					geoThread.start();
                    }
                    
                    
                   
		     	}
		     	alphaReportsService.updateAlphaReportsTrue(alphaReports.get(i));

	        	}else if(alphaReports.get(i).getEmailSent().equals("true")) {
	        		alphaReportsService.deleteAlphaReportsById(alphaReports.get(i).getId(),"VTrackSuperAdmin");
	        	}
	        	}
	        
	        	}else {
			     		  Constant.LOGGER.error("its not of any type");
			     		  }
     return "success";
	}
	 
}
