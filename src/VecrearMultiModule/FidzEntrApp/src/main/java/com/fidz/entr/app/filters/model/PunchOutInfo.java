package com.fidz.entr.app.filters.model;

import lombok.Data;

@Data
public class PunchOutInfo {

private String punchOutCount;

@Override
public String toString() {
	return "PunchOutInfo [punchOutCount=" + punchOutCount + "]";
}

public PunchOutInfo(String punchOutCount) {
	super();
	this.punchOutCount = punchOutCount;
}



}
