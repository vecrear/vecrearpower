package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum CustomerUIComponentType {
	TEXT, RADIO,SPINNER, CHECKBOX, SINGLE_SELECT, MULTI_SELECT
}
