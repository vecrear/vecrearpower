package com.fidz.entr.app.reportcustomersweb;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Contact;
import com.fidz.base.model.Device;
import com.fidz.base.model.Role;
import com.fidz.base.model.Status;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.TaggedDevices;
import com.fidz.entr.base.model.UserHierarchyLevel;


import lombok.Data;

@Data
@Document(collection = "user")
@JsonIgnoreProperties(value = { "target" })
public class User  {
	
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	@Id
	protected String id;
	@JsonIgnore
	@Indexed(unique = true)
	protected String userName;
	@JsonIgnore
	protected String password; //should be encrypted password
	
	protected PersonName name;
	@JsonIgnore
	protected Address address;
	@JsonIgnore
	protected Contact contact;
	@JsonIgnore
	protected PersonName emergencyContactName;
	@JsonIgnore
	protected Contact emergencyContact;
	@JsonIgnore
	@DBRef(lazy = true)
	protected UserRole userRole;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<Role> appRoles;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<User> reportsTo;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<Department> additionalDepartments;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Device device;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Country country;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Region region;
	@JsonIgnore
	@DBRef(lazy = true)
	protected State state;
	@JsonIgnore
	@DBRef(lazy = true)
	protected City city;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<City> citylist;
	@JsonIgnore
	protected Map<String, Object> additionalFields;
	@JsonIgnore
	@NotNull
	protected int userRoleId;
	@JsonIgnore
	@NotNull
	protected String managerName;
	@JsonIgnore
	@NotNull
	protected String managerMobile;
	@JsonIgnore
	@NotNull
	protected String managerEmail;
	@JsonIgnore
	protected List<TaggedDevices> taggedDevices;
	
	@JsonIgnore
	@DBRef(lazy = true)
	protected com.fidz.entr.app.usermanagers.User userManager;
	@JsonIgnore
	protected boolean geofenceTagged = false;
	
	
	
	
}
