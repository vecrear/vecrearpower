package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.User;

import lombok.Data;

@Data
@Document(collection = "userdepartmentconfig")
@JsonIgnoreProperties(value = { "target" })
public class UserDepartmentConfig extends Base {
	@Id
	protected String id;
	
	@NotNull
	@DBRef(lazy = true)
	protected User user;
	
	@NotNull
	@DBRef(lazy = true)
	protected Company company;
	
	@NotNull
	@DBRef(lazy = true)
	protected Department department;
	
	protected List<ActivityPermission> activityPermissions;

	public UserDepartmentConfig(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull User user, @NotNull Company company,
			@NotNull Department department, List<ActivityPermission> activityPermissions) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.user = user;
		this.company = company;
		this.department = department;
		this.activityPermissions = activityPermissions;
	}

	@Override
	public String toString() {
		return "UserDepartmentConfig [id=" + id + ", user=" + user + ", company=" + company + ", department="
				+ department + ", activityPermissions=" + activityPermissions + ", createdTimestamp=" + createdTimestamp
				+ ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser="
				+ updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser
				+ ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	



	
}
