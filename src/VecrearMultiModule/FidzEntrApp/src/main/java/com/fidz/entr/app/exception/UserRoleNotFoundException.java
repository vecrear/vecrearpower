package com.fidz.entr.app.exception;

public class UserRoleNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1036141129257127556L;

	public UserRoleNotFoundException(String userRole) {
		super("UserRole not found with id/name " + userRole);
	}
}
