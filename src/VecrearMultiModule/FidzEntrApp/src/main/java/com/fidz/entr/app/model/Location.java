package com.fidz.entr.app.model;


import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Device;

import lombok.Data;

@Data
@Document(collection = "location")
@JsonIgnoreProperties(value = { "target" })
public class Location extends com.fidz.base.model.Location {
	
	

	@Id
	protected String id;
	
	protected String provider;
	
	protected String gpsFor;
	
	protected String altitude;
	
	protected String speed;
	
	protected String bearing;
	
	protected float accuracy;
	
	protected String address;
	
	protected Date dateTime;

	@NotNull
	@DBRef(lazy = true)
	protected Device deviceId;

	

	public Location(@NotNull double latitude, @NotNull double longitude, String addressloc, String id, String provider,
			String gpsFor, String altitude, String speed, String bearing, float accuracy, String address, Date dateTime,
			@NotNull Device deviceId) {
		super(latitude, longitude, addressloc);
		this.id = id;
		this.provider = provider;
		this.gpsFor = gpsFor;
		this.altitude = altitude;
		this.speed = speed;
		this.bearing = bearing;
		this.accuracy = accuracy;
		this.address = address;
		this.dateTime = dateTime;
		this.deviceId = deviceId;
	}






	@Override
	public String toString() {
		return "Location [id=" + id + ", provider=" + provider + ", gpsFor=" + gpsFor + ", altitude=" + altitude
				+ ", speed=" + speed + ", bearing=" + bearing + ", accuracy=" + accuracy + ", address=" + address
				+ ", dateTime=" + dateTime + ", deviceId=" + deviceId + "]";
	}






	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}














	
	
}
