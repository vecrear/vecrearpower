package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.IdleTime;

@Repository
public interface IdleTimeRepository extends MongoRepository<IdleTime, String> {

}
