package com.fidz.entr.app.validator;

import org.springframework.stereotype.Component;

import com.fidz.base.exception.ValidationException;
import com.fidz.base.validator.Validator;
import com.fidz.entr.app.model.ActivityType;
@Component
public class ActivityTypeValidator implements Validator<ActivityType>{

	@Override
	public boolean doValidate(boolean mandatory, ActivityType value) throws ValidationException {
		// TODO Auto-generated method stub
		if(value.getName()== null) {
			return false;
		}
		if(value.getCompany()== null) {
			return false;
		}
		if(value.getDepartment()== null) {
			return false;
		}
		
		return true;
	}

	@Override
	public boolean doMandatoryCheck(ActivityType value) throws ValidationException {
		// TODO Auto-generated method stub
		return false;
	}

}
