package com.fidz.entr.app.reportcustomersweb;

import java.util.Date;
import java.util.TimeZone;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Department extends SimplifiedBase {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	@JsonIgnore
	protected String description;
	
	@DBRef(lazy = true)
	protected CompanyInsideDept company;

	@JsonIgnore
	@NotNull
	protected Object punchInFrequency;
	
	/**
	 * gpsInterval - Should be in mins
	 */
	@JsonIgnore
	@NotNull
	protected int gpsInterval;
	
	@JsonIgnore
	protected String mqttTopicName;
	
	@JsonIgnore
	protected Object distanceCalculationMethod;
	@JsonIgnore
	protected String forcePunchOutTime;
	@JsonIgnore
	protected TimeZone forcePunchOutTimeZone;


	


}
