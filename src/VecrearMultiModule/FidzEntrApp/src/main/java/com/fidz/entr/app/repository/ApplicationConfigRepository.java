package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.fidz.entr.app.model.ApplicationConfig;

public interface ApplicationConfigRepository extends MongoRepository<ApplicationConfig, String>{

}
