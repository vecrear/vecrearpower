package com.fidz.entr.app.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.exception.UserException;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.app.model.AuthenticationResponse;
import com.fidz.entr.app.model.Login;
import com.fidz.entr.app.model.ResetPassword;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.payload.ErrorResponse;
import com.fidz.entr.app.service.LoginService;

import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/login")
public class LoginController {
	@Autowired
	private LoginService loginService;
	
	@Value("${loginservices.enabled}")
	private Boolean property;
	 
	@PostMapping("/authenticate")
	 public ResponseEntity<AuthenticationResponse> authenticateUser(@Valid @RequestBody Login login) {
	     //return authenticateService.authenticateUser(login);
		//System.out.println("login-----------"+login.toString());
		System.out.println("property value******"+property);
		String message="Loginservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return loginService.authenticateUser(login);
	 }
	
	/*@ExceptionHandler
	public ResponseEntity<?> handleNotFoundException(UserNotFoundException ex) {
	    return ResponseEntity.notFound().build();
	}
	
	@ExceptionHandler
	public ResponseEntity<ErrorResponse> handleUserException(UserException ex) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex.getMessage()));
	}*/
	
	@PatchMapping("/setpassword")
	 public ResponseEntity<User> resetPassword(@Valid @RequestBody ResetPassword password) {
	     //return authenticateService.authenticateUser(login);
		System.out.println("property value******"+property);
		String message="Loginservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return loginService.resetPassword(password);
	 }
	
	

}
