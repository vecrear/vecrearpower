package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Application;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "applicationconfig")
@JsonIgnoreProperties(value = { "target" })
public class ApplicationConfig extends Base {
	@Id
	protected String id;
	
	
	@DBRef(lazy = true)
	protected Application application;
	
	protected List<StaticField> staticUserFields;
	
	protected List<StaticField> staticCustomerFields;

	

	
}
