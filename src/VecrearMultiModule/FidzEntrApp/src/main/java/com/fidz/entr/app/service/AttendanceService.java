package com.fidz.entr.app.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTimeZone;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.awsconfig.service.AwsInfoService;
import com.fidz.entr.app.controller.UserNameAppender;
import com.fidz.entr.app.model.ApplicationConfig;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.IdleTime;
import com.fidz.entr.app.model.Location;
import com.fidz.entr.app.model.LogOffTime;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.model.PunchStatusCount;
import com.fidz.entr.app.model.ResourceAttendanceReport;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.reportattendance.PersonName;


import com.fidz.entr.app.repository.AttendanceRepository;
import com.fidz.entr.app.repository.DeviceRepository;
import com.fidz.entr.app.repository.IdleTimeRepository;
import com.fidz.entr.app.repository.LocationRepository;
import com.fidz.entr.app.repository.PunchInStatusRepository;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.DistanceCalculationMethod;
import com.fidz.entr.base.model.Facility;
import com.fidz.services.location.model.AwsInfo;
import com.fidz.services.location.model.GpsData;
import com.fidz.services.location.model.GpsDataDistnaceCal;
import com.fidz.services.location.model.GpsDataMultiUser;
import com.fidz.services.location.model.LatestGpsLatLong;
import com.fidz.services.location.model.TravelType;
import com.fidz.services.location.service.GpsDataService;
import com.fidz.services.location.service.LatestLatLongService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.mongodb.client.model.Collation;


		
@Service("FEAAttendanceService")
public class AttendanceService {
	private GenericAppINterface<Attendance> genericINterface;
	@Autowired
	public AttendanceService(GenericAppINterface<Attendance> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private AttendanceRepository attendanceRepository;
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private PunchInStatusRepository punchInStatusRepository;
	
	@Autowired
	private UserService  userService;
	
	@Autowired
	private AwsInfoService awsInfoService;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	@Autowired
	private GpsDataService gpsDataService;
	@Autowired
	private IdleTimeRepository idleTimeRepository;
	
	@Autowired
	private LatestLatLongService latestLatLongService;
	@Autowired
	private DeviceRepository deviceRepository;
	
    public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancesweb(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllAttendancesweb AttendanceService.java");
    	List<com.fidz.entr.app.reportattendance.Attendance> attendances=null;
    	try {
    	//Query q = new Query();
    	//q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	attendances=this.genericINterface.findAll(com.fidz.entr.app.reportattendance.Attendance.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllAttendancesweb AttendanceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllAttendancesweb AttendanceService.java");
    	return attendances;
    }
    
    
    
    public List<Attendance> getAllAttendances(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllAttendances AttendanceService.java");
    	List<Attendance> attendances=null;
    	try {
    	attendances=this.genericINterface.findAll(Attendance.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllAttendances AttendanceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllAttendances AttendanceService.java");
    	return attendances;
    }
	
    @SuppressWarnings("unchecked")
    public Attendance createAttendance(Attendance attendance) {
        Attendance attendances=null;
       int  punchInDayCount = 0;
        final String  msg = "User has been cleared the data / Uninstalled app before Punch-Out";
        try {
            Constant.LOGGER.info(" Inside AttendanceService.java Value for insert attendance record: createattendance  :: " + attendance);
            String schemaName=attendance.getSchemaName();
            MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
            Location a = null;
            Location location = null;
           // String address = null;
            if (attendance.getPunchInLocation() != null && attendance.getPunchOutLocation()== null) {
                a = createLocation(attendance.getPunchInLocation(), schemaName);
                location = a;
                attendance.setPunchInLocation(location);
                if(attendance.getPunchInLocation().getAddress().equals("NA")) {
    	    		String geoaddressKey = null;
    	    		List<com.fidz.entr.app.awsconfig.model.AwsInfo> geokeyList = awsInfoService.getAWSnfoObject(schemaName);
    	    		
    	    		if(geokeyList.get(0).getGeoAddressKey() != null) {
    	    			geoaddressKey = geokeyList.get(0).getGeoAddressKey();
    	    			Constant.LOGGER.info("geoaddressKey-----geoaddressKey------"+geoaddressKey);
    	    		}
    	    		Constant.LOGGER.info("entering-------------to geo");
    	    		String address  = reverseGeoCoding(attendance.getPunchInLocation().getLatitude(),attendance.getPunchInLocation().getLongitude(),geoaddressKey);
       				Constant.LOGGER.info("GpsAddress-----addressfrom API------"+address);
    	    		attendance.getPunchInLocation().setAddress(address);
    				attendance.setPunchInLocation(attendance.getPunchInLocation());
    				Constant.LOGGER.info("GpsAddress-----addressfrom API------"+attendance.toString());
    			    location.setAddress(address);
                    location  = createLocation(location, schemaName);
                    attendance.setPunchInLocation(location);
    				
    			}else {
    				
    				Constant.LOGGER.info(" else address is good-------------------------------- :: " );
    			}
                
            }     
          
           

            User user = userService.getUserById(attendance.getUser().getId(), schemaName);
  
            Constant.LOGGER.info(" user-------------------------------- :: " + user.toString());
           
            try {
                Constant.LOGGER.info("inside filtering");
//To check User has cleared data and update
            Query q = new Query();
            q.addCriteria(Criteria.where("user.id").is(attendance.getUser().getId()));
            q.with(new Sort(Sort.Direction.DESC,"punchInDateTimeStamp"));
            Attendance alist = mongoTemplate.findOne(q, Attendance.class);
            

        
            if(alist!=null) {
       
            String outtime = String.valueOf(alist.getPunchOutDateTimeStamp());
         
            Date de = new Date();

        if (outtime.equals("null")){
            Constant.LOGGER.info("alist condition true**********");

            Constant.LOGGER.info("alistl");
            LatestGpsLatLong latestGpsLatLong = latestLatLongService.getLatestLatLongByuserId(user.getId(), schemaName);
            Constant.LOGGER.info("latestGpsLatLong"+latestGpsLatLong);
           
            if(latestGpsLatLong!=null) {
                //alist.set
            	Constant.LOGGER.info("latestGpsLatLong!=null" + latestGpsLatLong.getLatestDate());


            Date punchout =  DateFormatConvertor(latestGpsLatLong.getLatestDate());
        	Constant.LOGGER.info("latestGpsLatLong!=null" + punchout);
            LocalDateTime localDateTime = punchout.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime minusminutes = localDateTime.plusSeconds(30);
             de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());

            }else {
            	Constant.LOGGER.info("inside else null");

	
            Date punchout =  attendance.getPunchInDateTimeStamp();
            LocalDateTime localDateTime = punchout.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime minusminutes = localDateTime.plusSeconds(30);
            de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
	
}
            alist.setPunchOutDateTimeStamp(de);
                 Location lrs = new Location();

                 lrs.setLatitude(0.0);
                 lrs.setLongitude(0.0);
                 lrs.setDeviceId(user.getDevice());
                 lrs.setAddress(msg);
                 
                 Location lrc = this.locationRepository.save(lrs);
                 
                 
                 alist.setPunchInPerTheDay(false);

                alist.setPunchOutLocation(lrc);
               
                updateAttendancePunchOut(alist.getId(), alist, schemaName);

                Constant.LOGGER.info("after updating attendance punchout**********");
 
                GpsData gps  =  new GpsData();
                JSONObject jsonObject = new JSONObject();
               JSONObject jsonObject1 = new JSONObject();
                gps.setSchemaName(schemaName);
                gps.setDeviceID(user.getUserName());
                gps.setTravelType(TravelType.MOVEMENT);
                jsonObject.put("gpsFor","PunchOut");
                gps.setDateTime(de);
                jsonObject.put("latitude",0);
                jsonObject.put("longitude",0);
                jsonObject.put("address",msg);
                gps.setDistance(0.0);
                jsonObject.put("userName", user.getUserName());
                jsonObject.put("userId",user.getId());
                jsonObject.put("firstName",user.getName().getFirstName());
                jsonObject.put("middleName",user.getName().getMiddleName());
                jsonObject.put("lastName",user.getName().getLastName());
                jsonObject1.put("map", jsonObject);
                gps.setGpsData(jsonObject1);
                mongoTemplate.save(gps, "gpsdata");
                

               
                
                
        }
        String name = UserNameAppender.userNameAppend(user.getName().getFirstName(), user.getName().getMiddleName(), user.getName().getLastName());
        PunchStatus punchStatus = new PunchStatus();
        punchStatus.setId(attendance.getUser().getId());
        punchStatus.setIspunchIn(true);
        punchStatus.setDeptId(user.getDepartment().getId());
        //punchStatus.setDeptId(attendance.getUser().getDepartment().getId());
        punchStatus.setPunchStartDateTime(attendance.getPunchInDateTimeStamp());
        punchStatus.setUsername(name);
        punchStatus.setSchemaName(schemaName);
        punchStatus = this.punchInStatusRepository.save(punchStatus);
        }
           
            attendances=this.attendanceRepository.save(attendance);
            }catch(Exception ex) {
                Constant.LOGGER.error("error while inside filtering" + ex.getMessage());
               
            }
        }catch(Exception e) {
            Constant.LOGGER.info(" Inside AttendanceService.java Value for insert facility record: createattendance  :: " + attendance);
        }
       
       
       
       
       
        return attendances;

    }
    public void createAttendances(Attendance attendance) {
    	String schemaName=attendance.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside createAttendances AttendanceService.java");

    	Location a = null;
    	Location location = null;
    	if (attendance.getPunchInLocation() != null && attendance.getPunchOutLocation()== null) {
    		a = createLocation(attendance.getPunchInLocation(),schemaName);
    		location = a;
    		attendance.setPunchInLocation(location);
    	}  	
    	 
    	//Mono<Attendance> b = attendanceRepository.save(attendance);
    	//Attendance b = this.genericINterface.saveName(attendance);
  //  	System.out.println("b :"+b.toString());   	
        //return b;
    	this.genericINterface.saveName(attendance);
    }
    public Location createLocation(Location location, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside createLocation AttendanceService.java");

    	Location a = this.locationRepository.save(location);
   // 	System.out.println("a :"+a.toString());
        return a;
    }
    
    public Attendance getAttendanceById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAttendanceById AttendanceService.java");

    	return this.genericINterface.findById(id, Attendance.class);
    	
    }
	
    public Attendance updateAttendance(String id, Attendance attendance) {
    	String schemaName=attendance.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside updateAttendance AttendanceService.java");

    	attendance.setId(id);
    	return this.attendanceRepository.save(attendance);
       
    }
    
//    public Attendance updateAttendancePunchOut(String id, Attendance attendanceforPunchOut, String schemaName) {
//    	System.out.println("%%%%%%%%%%%%%%"+attendanceforPunchOut.toString());
//    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//    	Constant.LOGGER.info("inside updateAttendancePunchOut AttendanceService.java");
//    	
//    	
//    	String address = null; 
//    	if(attendanceforPunchOut.getPunchOutLocation().getAddress().equals("NA")) {
//    		String geoaddressKey = null;
//    		List<com.fidz.entr.app.awsconfig.model.AwsInfo> geokeyList = awsInfoService.getAWSnfoObject(schemaName);
//    		
//    		if(geokeyList.get(0).getGeoAddressKey() != null) {
//    			geoaddressKey = geokeyList.get(0).getGeoAddressKey();
//    			Constant.LOGGER.info("geoaddressKey-----geoaddressKey------"+geoaddressKey);
//    		}
//			address = reverseGeoCoding(attendanceforPunchOut.getPunchOutLocation().getLatitude(),attendanceforPunchOut.getPunchOutLocation().getLongitude(),geoaddressKey);
//			attendanceforPunchOut.getPunchOutLocation().setAddress(address);
//			attendanceforPunchOut.setPunchOutLocation(attendanceforPunchOut.getPunchOutLocation());
//			Constant.LOGGER.info("GpsAddress-----addressfrom API------"+address);
//		}else {
//			Constant.LOGGER.info(" else address is good-------------------------------- :: " );
//		}
//
//    	Attendance attendanceMono=this.genericINterface.findById(id, Attendance.class);   
//    	Location a = null;
//    	Location location = null;
//    	Attendance attendance = null;
//  		if (attendanceMono != null) {
//  			attendance = attendanceMono;
//  		}
//  		a = createLocation(attendanceforPunchOut.getPunchOutLocation(), schemaName);
//		location = a;
//		Date date1= attendance.getPunchInDateTimeStamp();
//		LocalDateTime dateTime = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//		attendance.setPunchOutLocation(location);
//		attendance.setPunchOutDateTimeStamp(attendanceforPunchOut.getPunchOutDateTimeStamp());
//		Date date2=attendanceforPunchOut.getPunchOutDateTimeStamp();
//		LocalDateTime dateTime2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//	//TO get diff of hours:Minutes:Seconds between punchIn and punchOut
//		long diff = date1.getTime()-date2.getTime();
//
//        long diffSeconds = diff / 1000 % 60;
//        long diffMinutes = diff / (60 * 1000) % 60;
//        long diffHours = diff / (60 * 60 * 1000) % 24;
//		
//        String diffs = String.valueOf(diffHours)+":"+String.valueOf(diffMinutes)+":"+String.valueOf(diffSeconds);
//		diffs =diffs.replace("-", "");
//		 StringBuilder builder = new StringBuilder();
//		 builder.append(diffs);
//	     String workDuration=builder.toString();
//	     
//	     attendance.setPunchInPerTheDay(attendanceforPunchOut.isPunchInPerTheDay());
//		 attendance.setWorkingDuration(workDuration);
//		 
//		User user = userService.getUserById(attendanceforPunchOut.getUser().getId(), schemaName);
//		Constant.LOGGER.info(" user-------user------------------------- :: " + user.toString());
//	    String name = UserNameAppender.userNameAppend(user.getName().getFirstName(), user.getName().getMiddleName(), user.getName().getLastName());
//		Constant.LOGGER.info(" name-------------------------------- :: " + name);
//		//User user = userService.getUserById(attendanceforPunchOut.getUser().getId(), schemaName);
//    	Constant.LOGGER.info(" user-------------------------------- :: " + user.toString());
//		PunchStatus punchStatus = new PunchStatus();
//    	punchStatus.setId(attendanceforPunchOut.getUser().getId());
//    	punchStatus.setIspunchIn(false);
//    	punchStatus.setDeptId(user.getDepartment().getId());
//    	punchStatus.setPunchStartDateTime(attendanceforPunchOut.getPunchInDateTimeStamp());
//    	punchStatus.setUsername(name);
//    	punchStatus.setSchemaName(schemaName);
//    	punchStatus = this.punchInStatusRepository.save(punchStatus);
//  		//return attendanceRepository.save(attendance).map(updatelogoff-> new ResponseEntity<>(updatelogoff, HttpStatus.OK));
//		return this.attendanceRepository.save(attendance);
//    }
    
    public Attendance updateAttendanceSpecificLogOff(String id, List<LogOffTime> logOffTime, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside updateAttendanceSpecificLogOff AttendanceService.java");

    	Attendance attendanceMono=this.genericINterface.findById(id, Attendance.class);   	
    	Attendance attendance = null;
  		if (attendanceMono != null) {
  			attendance = attendanceMono;
  		}
  		if (attendance.getLogOffTime() == null) {
  			attendance.setLogOffTime(logOffTime);
  		} else {
  			List<LogOffTime> l = attendance.getLogOffTime();
  			l.addAll(logOffTime);
  			attendance.setLogOffTime(l);
  			//attendance.setLogOffTime(l);
  		} 				
  		//return attendanceRepository.save(attendance).map(updatelogoff-> new ResponseEntity<>(updatelogoff, HttpStatus.OK));
  		return this.attendanceRepository.save(attendance);
    }

   
  //hard delete
  	public Map<String, String> deleteAttendance(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside deleteAttendance AttendanceService.java");

  		this.attendanceRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Attendance deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftAttendance(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside deleteSoftAttendance AttendanceService.java");

  		Attendance attendance = this.genericINterface.findById(id, Attendance.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		attendance.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		attendance.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		attendance.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(attendance);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Attendance deleted successfully");
  		return response;

  	}
    
    public List<Attendance> streamAllAttendances() {
        return attendanceRepository.findAll();
    }

	public List<Attendance> getAllAttendances11(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllAttendances11 AttendanceService.java");
  
		return attendanceRepository.findAll();
	    

	}

	public Integer getAllAttendancesCount(String schemaName) {
		List<Attendance> list = null;
    	Constant.LOGGER.info("inside getAllAttendancesCount AttendanceService.java");

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		list =  attendanceRepository.findAll();
		return list.size();
		
	}

	

	public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancebasedOnDate(String schemaName, String userId, String startdate,
			String enddate) {
		List<com.fidz.entr.app.reportattendance.Attendance> list = null;
		

			 Constant.LOGGER.info("Attendance getAllAttendancebasedOnDate AttendanceService.java"); 
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			 Query query=new Query();
	    	
	      
	    	 try { 
	    		// String date_string = "2019-10-17";  // the date format
	    		 DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
	    		 Date startDate = format.parse(startdate +" "+"00:00");
	    	     Date endDate = format.parse(enddate+" "+"23:59");
	    		 
	    		 // Query query1 = new Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
	    		 query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(endDate).and("user.id").is(userId));
	    		 // query1.with(new Sort(Sort.Direction.DESC,"date_time"));
	    		 Constant.LOGGER.info("Query"+query);
	    		 list=this.mongoTemplate.find(query, com.fidz.entr.app.reportattendance.Attendance.class);  
	    	 }catch(Exception ex) {
	    		 Constant.LOGGER.error("Error occurred Attendance getAllAttendancebasedOnDate AttendanceService.java"+ex);
	   		}
	         return list;
	
	
		
	}


// TO get attendance list based on pagination and dates
	public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancesPaginatedWeb(String startdate,
			String enddate, int pageNumber, int pageSize, String schemaName) throws ParseException {
		
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllAttendancesPaginatedWeb AttendanceService.java ");
    	//String date_string = "2015-04-17 11:02:49";
		 DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
		 Date startDate = format.parse(startdate +" "+"00:00");
	     Date endDate = format.parse(enddate+" "+"23:59");
        //end date as plus 1
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusHours(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        
    	final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	Query query = new Query();
    	query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(currentDatePlusOneDay));
    	query.with(pageableRequest);
    	return this.genericINterface.find(query, com.fidz.entr.app.reportattendance.Attendance.class);
	}


//to get by single user
	public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancecByUser(String id, String startdate,
			String enddate, int pageNumber, int pageSize, String schemaName) throws ParseException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllAttendancesPaginatedWeb AttendanceService.java ");
    	//String date_string = "2015-04-17 11:02:49";
		 DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
		 Date startDate = format.parse(startdate +" "+"00:00");
	     Date endDate = format.parse(enddate+" "+"23:59");
        //end date as plus 1
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusHours(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        
    	final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	Query query = new Query();
    	query.addCriteria(Criteria.where("user.id").is(id));
    	query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(currentDatePlusOneDay));
    	query.with(pageableRequest);
    	return this.genericINterface.find(query, com.fidz.entr.app.reportattendance.Attendance.class);
		
	}


	/*
	 * //to get by multi user public
	 * List<com.fidz.entr.app.reportattendance.Attendance>
	 * getAttendanceByMultiUser(@Valid ResourceAttendanceReport report, String
	 * schemaName) throws ParseException {
	 * MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	 * Constant.LOGGER.
	 * info("Inside get getAttendanceByUser userid based multiselect paginated according to date"
	 * ); //String date_string = "2015-04-17 11:02:49";
	 * List<com.fidz.entr.app.reportattendance.Attendance> listattendance = null;
	 * List<String> userid = report.getUsers().stream().map(user->
	 * user.getId()).collect(Collectors.toList()); String start =
	 * report.getStartDate(); String end =report.getEndDate(); int page =
	 * report.getPageNum(); int size = report.getPageSiz(); DateFormat format = new
	 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); DateFormat
	 * dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 * 
	 * Date startDate = format.parse(start +" "+"00:00:00"); Date endDate =
	 * format.parse(end+" "+"23:59:00"); //end date as plus 1
	 * 
	 * 
	 * Constant.LOGGER.info("page"+page+"size"+size); LocalDateTime localDateTime =
	 * endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	 * localDateTime=localDateTime.plusHours(1); Date currentDatePlusOneDay =
	 * Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	 * 
	 * 
	 * final Pageable pageableRequest = PageRequest.of(page, size); Query query =
	 * new Query(); query.addCriteria(Criteria.where("user.id").in(userid));
	 * query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(
	 * currentDatePlusOneDay)); query.with(pageableRequest);
	 * 
	 * listattendance = this.genericINterface.find(query,
	 * com.fidz.entr.app.reportattendance.Attendance.class); if( listattendance !=
	 * null) { listattendance.forEach(lt ->{
	 * 
	 * if(lt.getPunchOutDateTimeStamp() == null) {
	 * Constant.LOGGER.info("Inside punchout is null");
	 * 
	 * String strDate = dateFormat.format(lt.getPunchInDateTimeStamp());
	 * LatestGpsLatLong latestGpsLatLong =
	 * latestLatLongService.getLatestLatLongByuserId(lt.getUser().getId(),
	 * schemaName); Constant.LOGGER.info("Inside punchout is null" +
	 * latestGpsLatLong.toString());
	 * 
	 * 
	 * if(latestGpsLatLong != null ) { if(latestGpsLatLong.getLatestDate() != null)
	 * {
	 * 
	 * String endDates = StringDateConvertor(latestGpsLatLong.getLatestDate());
	 * Attendance ad = getAttendanceById(lt.getId(), schemaName);
	 * Constant.LOGGER.info("Attendance object with latestlatlong" + ad.toString());
	 * 
	 * //Constant.LOGGER.
	 * info("Attendance object with latestlatlong ad.getIdleTime()" +
	 * ad.getIdleTime().size()); if(ad.getIdleTime()!=null || ad.getIdleTime() ==
	 * null) { List<IdleTime> idtime = idleTimeConfigurator(lt.getUser().getId(),
	 * strDate,endDates , schemaName); if(ad.getIdleTime() == null) { if(idtime !=
	 * null) { List<IdleTime> ids = idleTimeRepository.saveAll(idtime);
	 * ad.setIdleTime(ids); updateAttendance(ad.getId(), ad); }
	 * 
	 * } else if(idtime.size() >= ad.getIdleTime().size()) { if(idtime != null) {
	 * List<IdleTime> ids = idleTimeRepository.saveAll(idtime); ad.setIdleTime(ids);
	 * updateAttendance(ad.getId(), ad); } }else {
	 * Constant.LOGGER.info("Attendance object with latestlatlong else case " );
	 * 
	 * }
	 * 
	 * } } }
	 * 
	 * } else { Constant.LOGGER.info("Inside punchout is not null");
	 * 
	 * String strDate = dateFormat.format(lt.getPunchInDateTimeStamp()); String
	 * endDates = dateFormat.format(lt.getPunchOutDateTimeStamp());
	 * 
	 * Attendance ad = getAttendanceById(lt.getId(), schemaName);
	 * Constant.LOGGER.info("Attendance object with punchoutdatetime" +
	 * ad.toString());
	 * 
	 * if(ad.getIdleTime()!=null || ad.getIdleTime() == null) { List<IdleTime>
	 * idtime = idleTimeConfigurator(lt.getUser().getId(), strDate,endDates ,
	 * schemaName); if(ad.getIdleTime() == null) { if(idtime != null) {
	 * List<IdleTime> ids = idleTimeRepository.saveAll(idtime); ad.setIdleTime(ids);
	 * updateAttendance(ad.getId(), ad); }
	 * 
	 * } else if(idtime.size() >= ad.getIdleTime().size()) { if(idtime != null) {
	 * List<IdleTime> ids = idleTimeRepository.saveAll(idtime); ad.setIdleTime(ids);
	 * updateAttendance(ad.getId(), ad); } }else {
	 * Constant.LOGGER.info("Attendance object with latestlatlong else case " );
	 * 
	 * }
	 * 
	 * } }
	 * 
	 * 
	 * }); }
	 * 
	 * 
	 * listattendance = this.genericINterface.find(query,
	 * com.fidz.entr.app.reportattendance.Attendance.class);
	 * 
	 * 
	 * return listattendance;
	 * 
	 * }
	 */
	
	public List<com.fidz.entr.app.reportattendance.Attendance> getAttendanceByMultiUser(@Valid ResourceAttendanceReport report, String schemaName) throws ParseException {
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        Constant.LOGGER.info("Inside get getAttendanceByUser userid based multiselect paginated according to date");
        //String date_string = "2015-04-17 11:02:49";
        List<com.fidz.entr.app.reportattendance.Attendance> listattendance = null;
        List<String> userid = report.getUsers().stream().map(user-> user.getId()).collect(Collectors.toList());
        String start = report.getStartDate();
        String end =report.getEndDate();
        int page = report.getPageNum();
        int size = report.getPageSiz();
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

         Date startDate = format.parse(start +" "+"00:00");
         Date endDate = format.parse(end+" "+"23:59");
        //end date as plus 1
       
        
        Constant.LOGGER.info("page"+page+"size"+size);
        
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        
        localDateTime=localDateTime.plusHours(1);
        
        //ZonedDateTime zdtParis = ZonedDateTime.of(localDateTime, ZoneId.of("Europe/Paris"));
        
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
       // Date currentDatePlusOneDay = Date.from(zdtParis.toInstant());
        
       // Date currentDatePlusOneDay = endDate;
        //Constant.LOGGER.info("currentDatePlusOneDay-----------------"+currentDatePlusOneDay.toString());
        final Pageable pageableRequest = PageRequest.of(page, size);
        Query query = new Query();
        query.addCriteria(Criteria.where("user.id").in(userid));
        query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(currentDatePlusOneDay));
        query.with(pageableRequest);
       
        listattendance = this.genericINterface.find(query, com.fidz.entr.app.reportattendance.Attendance.class);
        idleTimeRepository.deleteAll();
        listattendance.forEach(lt ->{
           
        if(lt.getPunchOutDateTimeStamp()!= null) {
        	  Attendance ad = getAttendanceById(lt.getId(), schemaName);
            Constant.LOGGER.info("Inside punchout is not null");
          
            String strDate = dateFormat.format(lt.getPunchInDateTimeStamp());
            String endDates = dateFormat.format(lt.getPunchOutDateTimeStamp());
          
            Constant.LOGGER.info("Attendance object with punchoutdatetime" + ad.toString());
                   
                        Constant.LOGGER.info("StartDate---------------now" + strDate);
                   
                        Constant.LOGGER.info("EndDate---------------now" + endDates);
                      //  Constant.LOGGER.info("idtime" + idtime);
                        List<IdleTime> idtime = idleTimeConfigurator(lt.getUser().getId(), strDate,endDates , schemaName);

                              if(idtime != null) {
                                  List<IdleTime> ids = idleTimeRepository.saveAll(idtime);
                                ad.setIdleTime(ids);
                                  updateAttendance(ad.getId(), ad);
                                 }
                        
         } else {
                     Constant.LOGGER.info("Inside punchout is null");

                     String strDate = dateFormat.format(lt.getPunchInDateTimeStamp());
                     LatestGpsLatLong latestGpsLatLong = latestLatLongService.getLatestLatLongByuserId(lt.getUser().getId(), schemaName);
                     Constant.LOGGER.info("Inside punchout is null" + latestGpsLatLong.toString());

                   
                     if(latestGpsLatLong != null ) {
                         if(latestGpsLatLong.getLatestDate() != null) {
                        	 
                             
                       String endDates = StringDateConvertor(latestGpsLatLong.getLatestDate());
                 
                       Constant.LOGGER.info(" endDates after StringDateConvertor ----"+endDates);

                            Attendance ad = getAttendanceById(lt.getId(), schemaName);
                            Constant.LOGGER.info("Attendance object with latestlatlong" + ad.toString());
                           
                            //Constant.LOGGER.info("Attendance object with latestlatlong ad.getIdleTime()" + ad.getIdleTime().size());
                         
                            List<IdleTime> idtime = idleTimeConfigurator(lt.getUser().getId(), strDate,endDates , schemaName);
                            if(idtime != null) {
                                    List<IdleTime> ids = idleTimeRepository.saveAll(idtime);
                                    ad.setIdleTime(ids);
                                    updateAttendance(ad.getId(), ad);
                            }
                                       
                                   
                         }                  
                                   
                     }          
                 
                 
                  }               
                
                
        });
       
        listattendance = this.genericINterface.find(query, com.fidz.entr.app.reportattendance.Attendance.class);

       
        return listattendance;
       
    }
	public static String StringDateConvertor(String dateStr)  {
		
		Constant.LOGGER.info("inside StringDateConvertor--"+dateStr);
		//TimeZone time_zone= TimeZone.getTimeZone("Europe/Paris");
		TimeZone utc = TimeZone.getTimeZone("UTC");
	    SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	    //SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    sourceFormat.setTimeZone(utc);
	    Constant.LOGGER.info("sourceFormat--"+sourceFormat.toString());
	    Constant.LOGGER.info("destFormat--"+destFormat.toString());
	    Date convertedDate = new Date();
		try {
			convertedDate = sourceFormat.parse(dateStr);
			Constant.LOGGER.info("inside getAttendanceById AttendanceService.java-----convertedDate--"+convertedDate.toString());
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return destFormat.format(convertedDate);
    }

	public String  getAttendanceStatusById(String id, String schemaName) {
		PunchStatus punchStatus = null;
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAttendanceById AttendanceService.java");
    	Query query = new Query();
    	String userPunchInfortheDay =null;
    	boolean ispunchinpertheDay;
    	punchStatus = this.mongoTemplate.findById(id, PunchStatus.class) ;
    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus);//id is user here
    	//List<com.fidz.entr.app.reportattendance.Attendance> attendance = null;
    //	query.addCriteria(Criteria.where("user.id").in(id).andOperator(Criteria.where("punchInPerTheDay").is(true)));
    //	this.genericINterface.find(query, com.fidz.entr.app.reportattendance.Attendance.class);
    	//attendance = this.genericINterface.find(query, com.fidz.entr.app.reportattendance.Attendance.class);
    	
    	try {
    		if(punchStatus !=null && punchStatus.isIspunchIn()) {
    			return userPunchInfortheDay ="user is PunchedIn";
    		}else {
    			return userPunchInfortheDay ="user is PunchedOut";
    		}
    	
    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error inside City. City.java "+ex.getMessage());
        }
    	return userPunchInfortheDay ;
	}



	  public Attendance updateAttendancePunchOut(String id, Attendance attendanceforPunchOut, String schemaName) {
	    	System.out.println("%%%%%%%%%%%%%%"+attendanceforPunchOut.toString());
	    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside updateAttendancePunchOut AttendanceService.java");

	    	
	    	
	    	
	    	if(attendanceforPunchOut.getPunchOutLocation().getAddress().equals("NA")) {
	    		String geoaddressKey = null;
	    		List<com.fidz.entr.app.awsconfig.model.AwsInfo> geokeyList = awsInfoService.getAWSnfoObject(schemaName);
	    		
	    		if(geokeyList.get(0).getGeoAddressKey() != null) {
	    			geoaddressKey = geokeyList.get(0).getGeoAddressKey();
	    			Constant.LOGGER.info("geoaddressKey-----geoaddressKey------"+geoaddressKey);
	    		}
	    		String address  = reverseGeoCoding(attendanceforPunchOut.getPunchOutLocation().getLatitude(),attendanceforPunchOut.getPunchOutLocation().getLongitude(),geoaddressKey);
				attendanceforPunchOut.getPunchOutLocation().setAddress(address);
				attendanceforPunchOut.setPunchOutLocation(attendanceforPunchOut.getPunchOutLocation());
				Constant.LOGGER.info("GpsAddress-----addressfrom API------"+address);
			}else {
				Constant.LOGGER.info(" else address is good-------------------------------- :: " );
			}


	    	Attendance attendanceMono=this.genericINterface.findById(id, Attendance.class);   
	    	Location a = null;
	    	Location location = null;
	    	Attendance attendance = null;
	  		if (attendanceMono != null) {
	  			attendance = attendanceMono;
	  		}
	  		a = createLocation(attendanceforPunchOut.getPunchOutLocation(), schemaName);
			location = a;
			Date date1= attendance.getPunchInDateTimeStamp();
			LocalDateTime dateTime = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			attendance.setPunchOutLocation(location);
			attendance.setPunchOutDateTimeStamp(attendanceforPunchOut.getPunchOutDateTimeStamp());
			Date date2=attendanceforPunchOut.getPunchOutDateTimeStamp();
			LocalDateTime dateTime2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			//TO get diff of hours:Minutes:Seconds between punchIn and punchOut
			long diff = date1.getTime()-date2.getTime();

	        long diffSeconds = diff / 1000 % 60;
	        long diffMinutes = diff / (60 * 1000) % 60;
	        long diffHours = diff / (60 * 60 * 1000) % 24;
			
	        String diffs = String.valueOf(diffHours)+":"+String.valueOf(diffMinutes)+":"+String.valueOf(diffSeconds);
			diffs =diffs.replace("-", "");
			 StringBuilder builder = new StringBuilder();
			 builder.append(diffs);
		     String workDuration=builder.toString();
		     
		     attendance.setPunchInPerTheDay(attendanceforPunchOut.isPunchInPerTheDay());
			 attendance.setWorkingDuration(workDuration);
			
			 
			User user = userService.getUserById(attendanceforPunchOut.getUser().getId(), schemaName);
			Constant.LOGGER.info(" user-------user------------------------- :: " + user.toString());
		    String name = UserNameAppender.userNameAppend(user.getName().getFirstName(), user.getName().getMiddleName(), user.getName().getLastName());
			Constant.LOGGER.info(" name-------------------------------- :: " + name);
			//User user = userService.getUserById(attendanceforPunchOut.getUser().getId(), schemaName);
	    	Constant.LOGGER.info(" user-------------------------------- :: " + user.toString());
			PunchStatus punchStatus = new PunchStatus();
	    	punchStatus.setId(attendanceforPunchOut.getUser().getId());
	    	Constant.LOGGER.info(" userid-------------------------------- :: " + attendanceforPunchOut.getUser().getId());

	    	punchStatus.setIspunchIn(false);
	    	punchStatus.setDeptId(user.getDepartment().getId());
	    	Constant.LOGGER.info(" Department().getId()-------------------------------- :: " + user.getDepartment().getId());

	    	punchStatus.setPunchStartDateTime(attendanceforPunchOut.getPunchOutDateTimeStamp());
	    	Constant.LOGGER.info(" getPunchOutDateTimeStamp-------------------------------- :: " +attendanceforPunchOut.getPunchOutDateTimeStamp());

	    	punchStatus.setUsername(name);
	    	punchStatus.setSchemaName(schemaName);
	    	punchStatus = this.punchInStatusRepository.save(punchStatus);
	  		//return attendanceRepository.save(attendance).map(updatelogoff-> new ResponseEntity<>(updatelogoff, HttpStatus.OK));
			return this.attendanceRepository.save(attendance);
	    }
	    
	    
	  public  List<IdleTime> idleTimeConfigurator(String userd,String punchInDateTimeStamp, String punchOutDateTimeStamp,String schemaName) {
          MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
          Constant.LOGGER.info("inside idleTimeConfigurator() AttendanceService.java");
          List<IdleTime> ids = new ArrayList<IdleTime>();
          List<IdleTime> ideas = new ArrayList<IdleTime>();
         
          try {
        User user = userService.getUserById(userd, schemaName);
        List<com.fidz.base.model.User> users = new ArrayList<com.fidz.base.model.User>();
        users.add(user);
        Constant.LOGGER.info("userid"+userd);
        Constant.LOGGER.info("punchInDateTimeStamp"+punchInDateTimeStamp);

        Constant.LOGGER.info("punchOutDateTimeStamp"+punchOutDateTimeStamp);

        Constant.LOGGER.info("schemaName"+schemaName);

        GpsDataMultiUser gpsDataMultiUser = new  GpsDataMultiUser();
        gpsDataMultiUser.setStartDate(punchInDateTimeStamp);
        gpsDataMultiUser.setEndDate(punchOutDateTimeStamp);
        gpsDataMultiUser.setPageNum(0);
        gpsDataMultiUser.setPageSiz(10000);
        gpsDataMultiUser.setUsers(users);
       
        List<GpsDataDistnaceCal> gps = gpsDataService.getIdleTimeMultiUserNew(schemaName, gpsDataMultiUser);   
        Constant.LOGGER.info("gps"+gps.toString() + gps.size());
        int outerIndex = 0;
        int innerIndex = 0;;
       ArrayList<ArrayList<Integer>> x = new ArrayList<ArrayList<Integer>>();
       x.add(new ArrayList<Integer>());

        if(gps.size()>0){
             
            for(int  i = 0 ;i<gps.size();i++) {
         
              double dist = gps.get(i).getDistance();   
             
             
              if (dist == 0.0){
              Constant.LOGGER.info("inside idletime 0 forloop"+i);
               x.get(outerIndex).add(innerIndex ,i);
               innerIndex++;
                  Constant.LOGGER.info("outerIndex"+outerIndex);
                 
              }
         
              else {
                 
                  outerIndex++;    // will move to next outerIndex
                  innerIndex = 0;  // reset or you will be out of bounds
                 
                 if(i != gps.size()-1) {
                  x.add(new ArrayList<Integer>()); // create an new outer index until your list is empty
                 }
                  Constant.LOGGER.info("inside else");
                 
                 
              }
                 
                  }
           
            x.removeIf(vec->vec.size() == 0 || vec.size() == 1);
            Constant.LOGGER.info("MultiArray"+x);
         
      for(int j = 0 ; j<x.size(); j++) {
          long start = gps.get(x.get(j).get(0)).getDateTime().getTime();
          Constant.LOGGER.info("start date"+start);
          long end = gps.get(x.get(j).get(x.get(j).size()-1)).getDateTime().getTime();
          Constant.LOGGER.info("end date"+start);

          Constant.LOGGER.info("j"+j);

          int s = x.get(j).get(0);
          int t = x.get(j).get((x.get(j).size()-1));
          Constant.LOGGER.info("s"+s);
          Constant.LOGGER.info("t"+t);

          String address = gps.get(x.get(j).get(x.get(j).size()-1)).getAddress();
          long diff = start-end;

              long diffSeconds = diff / 1000 % 60;
              long diffMinutes = diff / (60 * 1000) % 60;
              long diffHours = diff / (60 * 60 * 1000) % 24;
             
              String diffs = String.valueOf(diffHours)+":"+String.valueOf(diffMinutes)+":"+String.valueOf(diffSeconds);
              diffs =diffs.replace("-", "");
             
              StringBuilder builder = new StringBuilder();
              builder.append(diffs);
              String workDuration=builder.toString();
              IdleTime id = new IdleTime();
              id.setAddress(address);
              id.setCreatedByUser(user.getCreatedByUser());
              id.setCreatedTimestamp(user.getCreatedTimestamp());
              id.setDuration(workDuration);
              id.setFromDateTimeStamp(start);
              id.setToDateTimeStamp(end);
              id.setSchemaName(schemaName);
              ids.add(id);
             
      }

             

        }   
     
        Constant.LOGGER.info("IdleTime List"+ ids.toString());

          //ideas = idleTimeRepository.saveAll(ids);
           
          }catch(Exception ex) {
              Constant.LOGGER.error("inside idleTimeConfigurator() AttendanceService.java"+ex.fillInStackTrace());

             
          }

         
          return ids;
      }




		public PunchStatusCount getpunchstatusById(String userid, String punchstatusID,String date, String schemaName) {
			List<PunchStatus> punchStatus = null;
	    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside getAttendanceById AttendanceService.java");
	    	Query query = new Query();
	        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	        //Date startDate = null;
	        PunchStatusCount punchStatusCount = new PunchStatusCount();
	  
	        Constant.LOGGER.info("punchStatus--------------online---------"+userid);//id is user here
	        Constant.LOGGER.info("punchStatus--------------punchstatusID---------"+punchstatusID);//id is user here
	        Constant.LOGGER.info("punchStatus--------------online---------"+date);//id is user here
	        Constant.LOGGER.info("punchStatus--------------online---------"+schemaName);//id is user here
			try {
				Date startDate = format.parse(date);
				
				  Date endDate = format.parse(date);
			        //end date as plus 1
			        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			        localDateTime=localDateTime.plusDays(1);
			        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
					List<com.fidz.entr.app.reportusers.User> userLevelList = userService.getAllUsersRoles( schemaName, userid, "ACTIVE");
					Constant.LOGGER.info("userLevelList-----------------------"+userLevelList.size());//id is user here
			        List<String> userids = userLevelList.stream().map(user-> user.getId()).collect(Collectors.toList());
			        userids.add(userid);
			        Constant.LOGGER.info("userLevelList-----------------------"+userids.size());//id is user here
				if(punchstatusID.equals("online")) {
					
					query.addCriteria(Criteria.where("id").in(userids).andOperator(Criteria.where("punchStartDateTime").lte(currentDatePlusOneDay).andOperator(Criteria.where("ispunchIn").is(true))));
					Constant.LOGGER.info("punchStatus--------------startDate---------"+query.toString());
			    	punchStatus = this.mongoTemplate.find(query, PunchStatus.class);
			    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.size());//id is user here
			    	
			    	punchStatusCount.setPunchStatusList(punchStatus);
			    	punchStatusCount.setCount(punchStatus.size()); 
			    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.toString());//id is user here
				}else if(punchstatusID.equals("punchout")) {
			    	Constant.LOGGER.info("punchStatusId-----------------------PUNCHOUT");

					query.addCriteria(Criteria.where("id").in(userids).andOperator(Criteria.where("punchStartDateTime").gte(startDate).lte(currentDatePlusOneDay).andOperator(Criteria.where("ispunchIn").is(false))));
			    	punchStatus = this.mongoTemplate.find(query, PunchStatus.class);
			    	
			    	punchStatusCount.setPunchStatusList(punchStatus);
			    	punchStatusCount.setCount(punchStatus.size());
			    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.toString());//id is user here
				}else if(punchstatusID.equals("offline") ){
			    	Constant.LOGGER.info("punchStatusId-----------------------offline");

					query.addCriteria(Criteria.where("id").in(userids).andOperator(Criteria.where("punchStartDateTime").lte(endDate).andOperator(Criteria.where("ispunchIn").is(false))));
			    	punchStatus = this.mongoTemplate.find(query, PunchStatus.class);
			    	
			    	punchStatusCount.setPunchStatusList(punchStatus);
			    	punchStatusCount.setCount(punchStatus.size());
			    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.toString());//id is user here
				}
			
		    
			} catch (ParseException e) {
				Constant.LOGGER.info("punchStatus-----------------------"+e.getMessage());//id is user here
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				return punchStatusCount ;
		}


	public PunchStatusCount getpunchstatusForAdmin(String punchstatusID, String date, String schemaName) {
		List<PunchStatus> punchStatus = null;
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAttendanceById AttendanceService.java");
    	Query query = new Query();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        //Date startDate = null;
        PunchStatusCount punchStatusCount = new PunchStatusCount();
        Constant.LOGGER.info("punchStatus--------------punchstatusID---------"+punchstatusID);//id is user here
        Constant.LOGGER.info("punchStatus--------------online---------"+date);//id is user here
        Constant.LOGGER.info("punchStatus--------------online---------"+schemaName);//id is user here
		try {
			Date startDate = format.parse(date);
			
			  Date endDate = format.parse(date);
		        //end date as plus 1
		        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		        localDateTime=localDateTime.plusDays(1);
		        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			if(punchstatusID.equals("online")) {
				
				query.addCriteria(Criteria.where("punchStartDateTime").lte(currentDatePlusOneDay).andOperator(Criteria.where("ispunchIn").is(true)));
				Constant.LOGGER.info("punchStatus--------------startDate---------"+query.toString());
		    	punchStatus = this.mongoTemplate.find(query, PunchStatus.class);
		    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.size());//id is user here
		    	
		    	punchStatusCount.setPunchStatusList(punchStatus);
		    	punchStatusCount.setCount(punchStatus.size()); 
		    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.toString());//id is user here
			}else if(punchstatusID.equals("punchout")) {
				
				query.addCriteria(Criteria.where("punchStartDateTime").gte(startDate).lte(currentDatePlusOneDay).andOperator(Criteria.where("ispunchIn").is(false)));
		    	punchStatus = this.mongoTemplate.find(query, PunchStatus.class);
		    	
		    	punchStatusCount.setPunchStatusList(punchStatus);
		    	punchStatusCount.setCount(punchStatus.size());
		    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.toString());//id is user here
			}else if(punchstatusID.equals("offline") ){
				
				query.addCriteria(Criteria.where("punchStartDateTime").lte(endDate).andOperator(Criteria.where("ispunchIn").is(false)));
		    	punchStatus = this.mongoTemplate.find(query, PunchStatus.class);
		    	
		    	punchStatusCount.setPunchStatusList(punchStatus);
		    	punchStatusCount.setCount(punchStatus.size());
		    	Constant.LOGGER.info("punchStatus-----------------------"+punchStatus.toString());//id is user here
			}
		
	    
		} catch (ParseException e) {
			Constant.LOGGER.info("punchStatus-----------------------"+e.getMessage());//id is user here
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	
    	

    
    	return punchStatusCount ;
	}
	
//	public List<Attendance> getAlllWebAttendances(String schemaName) {
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		Query query = new Query();
//
//		query.fields().include("createdTimestamp");
//		query.fields().include("createdByUser");
//		query.fields().include("updatedTimestamp");
//		query.fields().include("updatedByUser");
//		query.fields().include("deletedTimestamp");
//		query.fields().include("deletedByUser");
//		query.fields().include("status");
//		query.fields().include("schemaName");
//		query.fields().include("user").include("name").include("firstName");
//		query.fields().include("calenderDatestamp");
//		query.fields().include("punchInDateTimeStamp");
//		query.fields().include("punchInLocation");
//		query.fields().include("punchOutDateTimeStamp");
//		query.fields().include("punchOutLocation");
//		query.fields().include("workingDuration");
//		query.fields().include("logOffTime");
//		query.fields().include("idleTime");
//		
//		
//		return this.genericINterface.find(query,Attendance.class);
//	}
	
	

//	public List<ReportAttendance> getAlllWebAttendances(String schemaName) {
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		
//		List<ReportAttendance> webAttendances = null;
//		
//		List<Attendance> userattendances = this.genericINterface.findAll(Attendance.class);
//		System.out.print(userattendances.get(0).toString());
//		
//		//Constant.LOGGER.debug(" Inside AttendanceService.java Value for insert attendance record: createattendance  :: " + attendance);
//		
//		
//		for(int i = 0; i < userattendances.size();i++ ) {
//			PersonName  personName = new PersonName();
//			
//			personName.setFirstName(userattendances.get(i).getUser().getName().getFirstName());
//			personName.setLastName(userattendances.get(i).getUser().getName().getLastName());
//			personName.setMiddleName(userattendances.get(i).getUser().getName().getMiddleName());
//			
//			Contact contact = new Contact();
//					
//					contact.setPrimary(userattendances.get(i).getUser().getContact().getPhoneNo().getPrimary());
//					contact.setEmail(userattendances.get(i).getUser().getContact().getEmail());
//			
//					
//			Device deviceatten = new Device();
//			deviceatten.setId(userattendances.get(i).getUser().getDevice().getId());
//			deviceatten.setDeviceId(userattendances.get(i).getUser().getDevice().getDeviceId());
//				
//			
//			
//			Country country =  new Country();
//			country.setId(userattendances.get(i).getUser().getCountry().getId());
//			country.setName(userattendances.get(i).getUser().getCountry().getName());
//					
//			
//			User userdata = new User();
//			userdata.setId(userattendances.get(i).getUser().getId());
//			userdata.setUserName(userattendances.get(i).getUser().getUserName());
//			userdata.setName(personName);
//			userdata.setContact(contact);
//			userdata.setDevice(deviceatten);
//			userdata.setCountry(country);
//			
//			ReportAttendance reportAttendance = new ReportAttendance();
//			//LocationAttendance locationAttendance = new LocationAttendance(); 
//			
//			
////					(userattendances.getCreatedTimestamp(), 
////					userattendances.getCreatedByUser(), userattendances.getUpdatedTimestamp(), 
////					userattendances.getUpdatedByUser(), userattendances.getDeletedTimestamp(), 
////					userattendances.getDeletedByUser(), userattendances.getStatus(), 
////					userattendances.getSchemaName(), userattendances.getId(), 
////					userdata, 
////					userattendances.getCalenderDatestamp(), 
////					userattendances.getPunchInDateTimeStamp(), 
////					userattendances.getPunchInLocation(), 
////					userattendances.getPunchOutDateTimeStamp(), 
////					userattendances.getPunchOutLocation(), 
////					userattendances.getWorkingDuration(), 
////					userattendances.getLogOffTime(), 
////					userattendances.getIdleTime());
//					
//					reportAttendance.setCreatedTimestamp(userattendances.get(i).getCreatedTimestamp()); 
//					reportAttendance.setCreatedByUser(userattendances.get(i).getCreatedByUser());	
//					reportAttendance.setUpdatedTimestamp(userattendances.get(i).getUpdatedTimestamp());
//					reportAttendance.setUpdatedByUser(userattendances.get(i).getUpdatedByUser());
//					reportAttendance.setDeletedTimestamp(userattendances.get(i).getDeletedTimestamp());
//					reportAttendance.setDeletedByUser(userattendances.get(i).getDeletedByUser());
//					reportAttendance.setStatus(userattendances.get(i).getStatus());
//					reportAttendance.setSchemaName(userattendances.get(i).getSchemaName());
//					reportAttendance.setId(userattendances.get(i).getId());
//					reportAttendance.setUser(userdata);
//					reportAttendance.setCalenderDatestamp(userattendances.get(i).getCalenderDatestamp());
//					reportAttendance.setPunchInDateTimeStamp(userattendances.get(i).getPunchInDateTimeStamp());
//					//reportAttendance.setPunchInLocationWeb(userattendances.getPunchInLocation());
//					reportAttendance.setPunchOutDateTimeStamp(userattendances.get(i).getPunchOutDateTimeStamp());
//					//reportAttendance.setPunchOutLocationWeb(userattendances.getPunchOutLocation());
//					reportAttendance.setWorkingDuration(userattendances.get(i).getWorkingDuration());
//					//reportAttendance.setLogOffTime(userattendances.getLogOffTime());
//					//reportAttendance.setIdleTime(userattendances.getIdleTime());
//
//			webAttendances.add(reportAttendance);
//
//		}
//		
//	//	attendances.forEach(userattendances-> {
//
//	//		if (userattendances.getId() != null) {
//
////				Thread geoThread = new Thread() {
////					public void run() {
////						
////						
////			
////
////					}
////				};
////
////				geoThread.start();
///*				PersonName  personName = new PersonName(userattendances.getUser().getName().getFirstName(),
//						userattendances.getUser().getName().getMiddleName(), 
//						userattendances.getUser().getName().getLastName());
//				Contact contact = new Contact(userattendances.getUser().getContact().getPhoneNo().getPrimary(),
//						userattendances.getUser().getContact().getEmail());
//				Device deviceatten = new Device(userattendances.getUser().getDevice().getId(),
//						userattendances.getUser().getDevice().getDeviceId());
//				
//				
//				Country country =  new Country(userattendances.getUser().getCountry().getId(), 
//						userattendances.getUser().getCountry().getName());
//				
//				User userdata = new User();
//				userdata.setId(userattendances.getUser().getId());
//				userdata.setUserName(userattendances.getUser().getUserName());
//				userdata.setName(personName);
//				userdata.setContact(contact);
//				userdata.setDevice(deviceatten);
//				userdata.setCountry(country);
//				
//				ReportAttendance reportAttendance = new ReportAttendance();
//				//LocationAttendance locationAttendance = new LocationAttendance(); 
//				
//				
////						(userattendances.getCreatedTimestamp(), 
////						userattendances.getCreatedByUser(), userattendances.getUpdatedTimestamp(), 
////						userattendances.getUpdatedByUser(), userattendances.getDeletedTimestamp(), 
////						userattendances.getDeletedByUser(), userattendances.getStatus(), 
////						userattendances.getSchemaName(), userattendances.getId(), 
////						userdata, 
////						userattendances.getCalenderDatestamp(), 
////						userattendances.getPunchInDateTimeStamp(), 
////						userattendances.getPunchInLocation(), 
////						userattendances.getPunchOutDateTimeStamp(), 
////						userattendances.getPunchOutLocation(), 
////						userattendances.getWorkingDuration(), 
////						userattendances.getLogOffTime(), 
////						userattendances.getIdleTime());
//						
//						reportAttendance.setCreatedTimestamp(userattendances.getCreatedTimestamp()); 
//						reportAttendance.setCreatedByUser(userattendances.getCreatedByUser());	
//						reportAttendance.setUpdatedTimestamp(userattendances.getUpdatedTimestamp());
//						reportAttendance.setUpdatedByUser(userattendances.getUpdatedByUser());
//						reportAttendance.setDeletedTimestamp(userattendances.getDeletedTimestamp());
//						reportAttendance.setDeletedByUser(userattendances.getDeletedByUser());
//						reportAttendance.setStatus(userattendances.getStatus());
//						reportAttendance.setSchemaName(userattendances.getSchemaName());
//						reportAttendance.setId(userattendances.getId());
//						reportAttendance.setUser(userdata);
//						reportAttendance.setCalenderDatestamp(userattendances.getCalenderDatestamp());
//						reportAttendance.setPunchInDateTimeStamp(userattendances.getPunchInDateTimeStamp());
//						//reportAttendance.setPunchInLocationWeb(userattendances.getPunchInLocation());
//						reportAttendance.setPunchOutDateTimeStamp(userattendances.getPunchOutDateTimeStamp());
//						//reportAttendance.setPunchOutLocationWeb(userattendances.getPunchOutLocation());
//						reportAttendance.setWorkingDuration(userattendances.getWorkingDuration());
//						//reportAttendance.setLogOffTime(userattendances.getLogOffTime());
//						//reportAttendance.setIdleTime(userattendances.getIdleTime());
//
//				webAttendances.add(reportAttendance);
//
//			} else {
//				Constant.LOGGER.error("User Device not found");
//			}*/
//
//		//});
//
//    	return webAttendances;
//	}
	public Attendance createAttendances(Attendance attendance, Attendance attendance2) {
    	Attendance attendances=null;
    	String schemaName=attendance.getSchemaName();
    	try {
    		Constant.LOGGER.debug(" Inside AttendanceService.java Value for insert attendance record: createattendance  :: " + attendance);
    		//String schemaName=attendance.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	Location a = null;
        	Location location = null;
        	if (attendance.getPunchInLocation() != null && attendance.getPunchOutLocation()== null) {
        		a = createLocation(attendance.getPunchInLocation(), schemaName);
        		location = a;
        		attendance.setPunchInLocation(location);
        	}  	
     	   
        	//Mono<Attendance> b = attendanceRepository.save(attendance);
        	//Attendance b = this.attendanceRepository.save(attendance);
      //  	System.out.println("b :"+b.toString());   	
            //return b;
        	PunchStatus punchStatus = new PunchStatus();
        	punchStatus.setId(attendance.getUser().getId());
        	punchStatus.setIspunchIn(true);
        	punchStatus.setPunchStartDateTime(attendance.getPunchInDateTimeStamp());
        	punchStatus.setUsername(attendance.getUser().getUserName());
        	punchStatus.setSchemaName(schemaName);
        	punchStatus = this.punchInStatusRepository.save(punchStatus);
        	attendances=this.attendanceRepository.save(attendance);
    	}catch(Exception e) {
    		Constant.LOGGER.debug(" Inside AttendanceService.java Value for insert facility record: createattendance  :: " + attendance);
    	}
    	
    	attendance2.setId(attendance.getId());
    	attendance2.setUser(attendance.getUser());
    	attendance2.setCreatedByUser(attendance.getCreatedByUser());
    	attendance2.setSchemaName(schemaName);
    	attendance2.setPunchInDateTimeStamp(attendance.getPunchInDateTimeStamp());
    	attendance2.setPunchInLocation(attendance.getPunchInLocation());
    	Attendance attendance3 = updateAttendancePunchOut(attendances.getId(), attendance2, schemaName);
    	
    	return attendance3;
    }



	public PunchStatus getpunchstatusByUserId(String userid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside Punchstatus based on userid AttendanceService.java");
        PunchStatus punchStatus = null;
		try {
			Query q = new Query();
			q.addCriteria(Criteria.where("id").is(userid));
			punchStatus = this.mongoTemplate.findById(userid, PunchStatus.class);
			}catch(Exception ex) {
			Constant.LOGGER.error("Error while getting data Punchstatus based on userid AttendanceService.java");
		}
		return punchStatus;
	}
	
	
	
	public String reverseGeoCoding(double latitude, double longitude,String geoaddressKey) {
		String urlPrefix ="https://maps.googleapis.com/maps/api/geocode/json?";
		if(geoaddressKey == null) {
			geoaddressKey = "AIzaSyAlCECFt-Y2fubjPbyabfu0wNqWbyzdNw4";
		}
	
		Constant.LOGGER.info("\nLatitude: " + latitude +"\nLongitude: " + longitude);
		System.out.println("\nLatitude: " + longitude +"\nLongitude: " + longitude);
		String address = "";
		String status = "";
		String url = urlPrefix+"latlng="+latitude+","+longitude+"&key="+geoaddressKey+"";//AIzaSyBc2Mb1NYQRJa1jKPE0zr-kuYmGxQnZ8js
		System.out.println("\nurl: " + url);
		try {
			URL uriAddress = new URL(url);
			URLConnection res = uriAddress.openConnection();
			BufferedReader bufferedReader  =  new BufferedReader(new InputStreamReader(res.getInputStream()));
		    StringBuilder builder = new StringBuilder();
		    String line;
		    while ((line = bufferedReader.readLine()) != null) {
		        builder.append(line + "\n");
		    }	
		    String builderArray = builder.toString();
		    //System.out.println("\nbuilderArray: \n" + builderArray);
		    JSONObject root = (JSONObject)JSONValue.parseWithException(builderArray);
		    //System.out.println("\nresult: \n" + root);
		    JSONArray resultArray = (JSONArray) root.get("results");
		    JSONObject obj0 = (JSONObject) resultArray.get(0);
		    //System.out.println("\nobj0: \n" + obj0);
		    String formattedAddress = (String) obj0.get("formatted_address");
		    Constant.LOGGER.info("formattedAddress------------------"+formattedAddress);
		    System.out.println("\nFormattedAddress: \n" + formattedAddress);
		    // Accessing status
		    String stat = (String) root.get("status");
		    status = stat;
		    System.out.println("\nStatus: " + status);
		    address = formattedAddress;
		    return formattedAddress;
		  
		}
		catch(Exception e) {
			System.out.println("\nException: " + e);
		}
		if(status.contentEquals("ZERO_RESULTS")) {
			address = "No address";
			return address;
		}
		if(status.contentEquals("OVER_QUERY_LIMIT")) {
			address = "Crossed your quota of api calls";
			return address;
		}
		if(status.contentEquals("REQUEST_DENIED")) {
			address = "Request denied";
			return address;
		}
		if(status.contentEquals("INVALID_REQUEST")) {
			address = "Invalid query";
			return address;
		}		
		if(status.contentEquals("UNKNOWN_ERROR")) {
			address = "Server Error";
			return address;
		}		
		if(address.contentEquals("")) {
			address = "Blank Address";
			return address;
		}
		else {
			Constant.LOGGER.info(address);
			return address;
		}
	}



	@SuppressWarnings("unchecked")
	public Device updateDeviceData(String id, @Valid Device device) {
		Constant.LOGGER.info("Inside updateDeviceData AttendanceService.java");
	    String schemaName = device.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    final String msg = "Punched Out After Device RESET";
			
			
			User user = userService.getbyDeviceID(id, device.getSchemaName());
			
			if(user!=null) {
				
				String status =  getAttendanceStatusById(user.getId(), schemaName);
				
				if(status !=null && status.equalsIgnoreCase("user is PunchedIn")) {
					
					Query q = new Query();
		            q.addCriteria(Criteria.where("user.id").is(user.getId()));
		            q.with(new Sort(Sort.Direction.DESC,"punchInDateTimeStamp"));
		            Attendance alist = mongoTemplate.findOne(q, Attendance.class);
		            
		            if(alist!=null) {


		                  Constant.LOGGER.info("alist condition true**********");
		                  LatestGpsLatLong latestGpsLatLong = latestLatLongService.getLatestLatLongByuserId(user.getId(), schemaName);

		                      //alist.set
		                      Date punchout =  DateFormatConvertor(latestGpsLatLong.getLatestDate());
		                      LocalDateTime localDateTime = punchout.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		                      LocalDateTime minusminutes = localDateTime.plusSeconds(30);
		                      Date de = Date.from(minusminutes.atZone(ZoneId.systemDefault()).toInstant());
		                      
		                      alist.setPunchOutDateTimeStamp(de);
		                       
		                      Location lrs = new Location();

		                       lrs.setLatitude(0.0);
		                       lrs.setLongitude(0.0);
		                       lrs.setDeviceId(user.getDevice());
		                       lrs.setAddress(msg);
		                       
		                       Location lrc = this.locationRepository.save(lrs);
		                       
		                       
		                      alist.setPunchInPerTheDay(false);

		                      alist.setPunchOutLocation(lrc);
		                     
		                      updateAttendancePunchOut(alist.getId(), alist, schemaName);

		                      Constant.LOGGER.info("after updating attendance punchout**********");
		       
		                      GpsData gps  =  new GpsData();
		                      JSONObject jsonObject = new JSONObject();
		                      JSONObject jsonObject1 = new JSONObject();
		                      gps.setSchemaName(schemaName);
		                      gps.setDeviceID(user.getUserName());
		                      gps.setTravelType(TravelType.MOVEMENT);
		                      jsonObject.put("gpsFor","PunchOut");
		                      gps.setDateTime(de);
		                      jsonObject.put("latitude",0);
		                      jsonObject.put("longitude",0);
		                      jsonObject.put("address",msg);
		                      gps.setDistance(0.0);
		                      jsonObject.put("userName", user.getUserName());
		                      jsonObject.put("userId",user.getId());
		                      jsonObject.put("firstName",user.getName().getFirstName());
		                      jsonObject.put("middleName",user.getName().getMiddleName());
		                      jsonObject.put("lastName",user.getName().getLastName());
		                      jsonObject1.put("map", jsonObject);
		                      gps.setGpsData(jsonObject1);
		                      mongoTemplate.save(gps, "gpsdata");
		             
		              }
					
				}
				

				
				
	            
				
			}
		    
		    
		    
		    
		    device.setId(id);
		    return this.deviceRepository.save(device);
			
		}



	private static Date DateFormatConvertor(String latestDate) {
		TimeZone utc = TimeZone.getTimeZone("UTC");
	    SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	    sourceFormat.setTimeZone(utc);
	    Date convertedDate = new Date();
		try {
			convertedDate = sourceFormat.parse(latestDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return convertedDate;
    }


}
