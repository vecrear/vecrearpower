package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.UserFeedBack;

@Repository
public interface UserFeedBackRepository extends MongoRepository<UserFeedBack, String>{
	//Mono<UserRole> findByName(String name);
	UserFeedBack findByUserName(String name);

 

}
