package com.fidz.entr.app.config;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.fidz.entr.app.model.DBData;
import com.fidz.entr.app.repository.DBDataRepository;


public  class ClientConnectionManager {
	@Autowired
	DBDataRepository  dBDataRepository;
	private static volatile ClientConnectionManager mongoConnection=null;
	
	public ClientConnectionManager() throws UnknownHostException,UnsupportedOperationException {}
	
	Map<String, DBManager> myConnections=new HashMap<String, DBManager>();
	
	
	public static synchronized ClientConnectionManager getInstance() 
            throws UnknownHostException{

       if (mongoConnection == null){
    	   mongoConnection =  new ClientConnectionManager();
           }

      return mongoConnection;
      }
	
	public static synchronized ClientConnectionManager createInstance() 
            throws UnknownHostException{

       if (mongoConnection == null){
    	   mongoConnection =  new ClientConnectionManager();
           }

      return mongoConnection;
      }
	
	public DBManager getMyDB1(String clientId) {
		
	   	  DBData dbcon=dBDataRepository.findByClientId(clientId).block();
	   	  System.out.println("coneectiondataaaaaa"+dbcon);
			String mongohost=dbcon.getHost();
			int mongoPort=dbcon.getPort();
			String mongoDbName=dbcon.getDbName();
			//String mongoUserName=dbcon.getDbUserName();
			//String mongoPwd=dbcon.getDbPassword();
			
			System.out.println("coneectiondataaaaaa"+dbcon);
		    
			DBManager mongoDbManager=null;
		    if(dbcon != null) {
		    	
				try {
					mongoDbManager=new DBManager();
					System.out.println("inside try 1");
					
					System.out.println("inside try 2");
					mongoDbManager.setHost(mongohost);
					System.out.println("inside try 3");
					mongoDbManager.setPort(mongoPort);
					System.out.println("inside try 4");
					mongoDbManager.setDbName(mongoDbName);
					System.out.println("inside try 5");
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedOperationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	
		    	
		    }
			return mongoDbManager;
			
		}
		
	/*public Mono<DBData> getMyDB(String clientId) throws UnknownHostException {
		
		System.out.println("SSSSSSSSSSSSSSSSSSSSSSSSSSS"+clientId);
		//System.out.println("SSSSSSSSShgbhggh"+dBDataRepository.findByClientId(clientId).block());
		
		DBData dbcon=dBDataRepository.findByClientId(clientId).block();
		String mongohost=dbcon.getHost();
		int mongoPort=dbcon.getPort();
		String mongoDbName=dbcon.getDbName();
		String mongoUserName=dbcon.getDbUserName();
		String mongoPwd=dbcon.getDbPassword();
		
		Mono<DBData> mongoDbManager=null;
		Mono<DBManager> mongoDbManagers=null;
	   
	    if(dbcon != null) {
	    	
			try {
				DBData dbDatas=null;
				dbDatas.setHost(mongohost);
				dbDatas.setPort(mongoPort);
				dbDatas.setDbName(mongoDbName);
				dbDatas.setDbUserName(mongoUserName);
				dbDatas.setDbPassword(mongoPwd);
				mongoDbManagers.just(dbDatas);
			} catch (UnsupportedOperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    }
		return mongoDbManager;
		
	}*/
	public DBManager getMyDB(String clientId,DBDataRepository  dBDataRepository) throws UnknownHostException {
		
		System.out.println("SSSSSSSSSSSSSSSSSSSSSSSSSSS"+clientId);
		//System.out.println("SSSSSSSSShgbhggh"+dBDataRepository.findByClientId(clientId).block());
		DBData dbcon=dBDataRepository.findByClientId(clientId).block();
		System.out.println("dbcon ::"+dbcon);
		String mongohost=dbcon.getHost();
		int mongoPort=dbcon.getPort();
		String mongoDbName=dbcon.getDbName();
		System.out.println("mongoDbName  "+mongoDbName);
	//	String mongoUserName=dbcon.getDbUserName();
	//	String mongoPwd=dbcon.getDbPassword();
		
		DBManager manager=null;
	    if(dbcon != null) {
			try {
				System.out.println("inside try 1");
				manager = new DBManager();
				System.out.println("inside try 2");
				manager.setHost(mongohost);
				System.out.println("inside try 3");
				manager.setPort(mongoPort);
				System.out.println("inside try 4");
				manager.setDbName(mongoDbName);
				System.out.println("inside try 5");
		//		manager.setMongoDbUserName(mongoUserName);
		//		manager.setPwd(mongoPwd);
				//MongoDBConfig t=new MongoDBConfig(manager);
				boolean flag = manager.connect();
				System.out.println("connection Flag :: "+flag);
				
			} catch (UnsupportedOperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    }
		return manager;
		
	}
	
	
  /*  public static void main(String[] args) {
		
		try {
			ClientConnectionManager dd=new ClientConnectionManager();
			dd.getMyDB("fidzentrapp1");
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@sucesssssssss");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
}
