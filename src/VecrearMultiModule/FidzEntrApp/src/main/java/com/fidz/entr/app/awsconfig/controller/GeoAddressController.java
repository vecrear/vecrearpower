package com.fidz.entr.app.awsconfig.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.service.GeoAddressService;
import com.fidz.services.location.model.GpsData;

import com.fidz.services.location.service.GpsDataService;

@CrossOrigin(maxAge = 3600)
@RestController
public class GeoAddressController {
	
	@Autowired
	public GeoAddressService geoAddressService; //= new GeoAddressService();
	
	 String urlPrefix    = "https://maps.googleapis.com/maps/api/geocode/json?";
	 String googleAPiKey = "&key=AIzaSyDNMKPzkkKtjipjb3YIyXEEXh-Piq7qbbU";

	@RequestMapping (method = RequestMethod.GET, value = "/geoaddress/{latitude},{longitude}")
	public String reverseGeoCoding(@PathVariable(value = "latitude") double latitude,@PathVariable(value = "longitude") double longitude) {
		return geoAddressService.reverseGeoCoding(latitude, longitude);
	}
	
}
