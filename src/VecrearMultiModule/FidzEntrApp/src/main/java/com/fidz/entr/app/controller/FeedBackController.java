package com.fidz.entr.app.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(maxAge = 3600)
@RestController("FEAFeedbackController")
@RequestMapping(value="/feedbacks")
public class FeedBackController {

}
