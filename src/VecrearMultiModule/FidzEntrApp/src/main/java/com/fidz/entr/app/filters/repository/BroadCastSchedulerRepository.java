package com.fidz.entr.app.filters.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fidz.entr.app.filters.model.BroadCastScheduler;

public interface BroadCastSchedulerRepository extends MongoRepository<BroadCastScheduler, String>{

}
