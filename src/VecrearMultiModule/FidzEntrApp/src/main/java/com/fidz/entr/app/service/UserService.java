package com.fidz.entr.app.service;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.crypto.spec.SecretKeySpec;
import javax.naming.NameNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.repository.GenericBaseINterface;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.controller.CustomerReq;
import com.fidz.entr.app.controller.DateFormatter;
import com.fidz.entr.app.controller.UserNameAppender;
import com.fidz.entr.app.exception.CompanyNotFoundException;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.CustomerTypeField;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.model.SuperAdminUsersStatus;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.repository.DeviceRepository;
import com.fidz.entr.app.repository.PunchInStatusRepository;
import com.fidz.entr.app.repository.UserRepository;
import com.fidz.entr.base.exception.CityNameFoundException;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.TaggedCities;
import com.fidz.entr.base.model.TaggedDevices;
import com.fidz.entr.superadmin.model.SuperAdmin;
import com.fidz.entr.superadmin.repository.SuperAdminRepository;
import com.fidz.services.authentication.model.Login;
import com.fidz.services.authentication.security.EncriptionUtil;
import com.mongodb.client.result.UpdateResult;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FEAUserService")
public class UserService {
	private GenericAppINterface<User> genericINterface;

	@Autowired
	public UserService(GenericAppINterface<User> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DeviceRepository deviceRepository;

	@Autowired
	private DeviceService deviceServie;

	@Autowired
	private SuperAdminRepository superAdminRepository;

	@Autowired
	private PunchInStatusRepository punchInStatusRepository;;

	@Autowired
	MongoTemplate mongoTemplate;

	public List<User> getAllUsers(String schemaName, String status) {
		Constant.LOGGER.info("Inside getAllUsers UserService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is(status));
		return this.genericINterface.find(query, User.class);

	}

	public User createUser(User user) {

		System.out.println("inside ");
		User users = null;
		try {
			Constant.LOGGER.info(" Inside UserService.java Value for insert User record: createUser  :: " + user);
			// String schemaName=user.getSchemaName();
			// MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			// TODO put datetime according to there timezone , schema name.Both values
			// should not be hard coded.
			SuperAdmin superAdmin = new SuperAdmin();
			PunchStatus pStatus = new PunchStatus();
			String schemaNames = user.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaNames);
			System.out.println("inside 12344444444444444");
			Company company = this.genericINterface.findById(user.getCompany().getId(), Company.class);
			if (company == null) {
				System.out.println("Company Details Not Found");
				throw new CompanyNotFoundException("No Data", "Company Details Not Found");
			} else {
				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
				Query query3 = new Query();
				query3.addCriteria(Criteria.where("userName").is(user.getUserName()));
				List<SuperAdmin> superAdmins = this.mongoTemplate.find(query3, SuperAdmin.class);
				List<String> names = superAdmins.stream().map(p -> p.getUserName()).collect(Collectors.toList());

				if (superAdmins.size() > 0 && names.stream().anyMatch(s -> s.equals(user.getUserName()) == true)) {

					System.out.println("superAdmins------------size" + superAdmins.size());
					System.out.println("names------------------size" + names.size());
					// -------------------------------------------------userCheck-------------------------------------------------------------------------------------
					Constant.LOGGER.info("Inside if statement--------------------------------------------------------");
					throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + user.getUserName());

				} else if (superAdmins.size() > 0
						&& names.stream().anyMatch(s -> s.equalsIgnoreCase(user.getUserName()) == true)) {
					Constant.LOGGER.info("Inside else if statement");
					throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + user.getUserName());

				} else {

					String schemaName = user.getSchemaName();
					MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
					SecretKeySpec securityKey = new EncriptionUtil()
							.getSecurityKey(user.getContact().getPhoneNo().getPrimary());
					String encreptPwdAuth = new EncriptionUtil().encrypt(user.getPassword(), securityKey);
					user.setPassword(encreptPwdAuth);
					List<TaggedDevices> taggedDevices = new ArrayList<TaggedDevices>();
					try {
						taggedDevices = user.getTaggedDevices();
						Constant.LOGGER.info("taggedCities---size-" + taggedDevices.size());
						Device device = null;
						for (int k = 0; k < taggedDevices.size(); k++) {
							device = deviceServie.getDeviceDataById(taggedDevices.get(k).getId(), schemaName);
							device.setId(taggedDevices.get(k).getId());
							device.setDeviceTagged(taggedDevices.get(k).isDeviceTagged());
							device.setTaggedUserName(taggedDevices.get(k).getTaggedUserName());
							this.deviceRepository.save(device);

						}

					} catch (Exception e) {
						Constant.LOGGER.info("Exception---Exception-" + e.getMessage());
					}

					users = this.userRepository.save(user);
					superAdmin.setCompany(company);
					superAdmin.setCreatedByUser(user.getCreatedByUser());
					superAdmin.setCreatedTimestamp(user.getCreatedTimestamp());
					superAdmin.setDateTime(user.getCreatedTimestamp());
					superAdmin.setPhoneNumber(user.getContact().getPhoneNo().getPrimary());
					superAdmin.setSchemaName(user.getSchemaName());
					superAdmin.setStatus(user.getStatus());
					superAdmin.setUpdatedTimestamp(user.getUpdatedTimestamp());
					superAdmin.setUserName(user.getUserName());
					superAdmin.setPassword(user.getPassword());
					superAdmin.setSuperAdmin(false);
					superAdmin.setUserMongoId(users.getId());
					MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
					this.superAdminRepository.save(superAdmin);
					return users;
				}

			}
		} catch (Exception e) {
			Constant.LOGGER.error(" Error occured Inside UserService.java Value for insert User record: createUser  :: "
					+ e.getMessage());
			throw new NameFoundException(e.getMessage());
		}

	}

	public void createUsers(User user) {
		System.out.println("inside ");
		User users = null;
		try {
			Constant.LOGGER.info(" Inside UserService.java Value for insert User record: createUser  :: " + user);
			// String schemaName=user.getSchemaName();
			// MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			// TODO put datetime according to there timezone , schema name.Both values
			// should not be hard coded.
			SuperAdmin superAdmin = new SuperAdmin();
			String schemaNames = user.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaNames);
			System.out.println("inside 12344444444444444");
			Company company = this.genericINterface.findById(user.getCompany().getId(), Company.class);
			if (company == null) {
				System.out.println("Company Details Not Found");
				throw new CompanyNotFoundException("No Data", "Company Details Not Found");
			} else {
				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
				// System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@company"+company.toString());
				superAdmin.setCompany(company);
				// superAdmin.setCompany(user.getCompany());
				superAdmin.setCreatedByUser(user.getCreatedByUser());
				superAdmin.setCreatedTimestamp(user.getCreatedTimestamp());
				superAdmin.setDateTime(user.getCreatedTimestamp());
				superAdmin.setPhoneNumber(user.getContact().getPhoneNo().getPrimary());
				superAdmin.setSchemaName(user.getSchemaName());
				superAdmin.setStatus(user.getStatus());
				superAdmin.setUpdatedTimestamp(user.getUpdatedTimestamp());
				superAdmin.setUserName(user.getUserName());
				superAdmin.setPassword(user.getPassword());
				superAdmin.setSuperAdmin(false);

				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
				this.superAdminRepository.save(superAdmin);
				System.out.println("************");
				String schemaName = user.getSchemaName();
				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
				SecretKeySpec securityKey = new EncriptionUtil()
						.getSecurityKey(user.getContact().getPhoneNo().getPrimary());
				String encreptPwdAuth = new EncriptionUtil().encrypt(user.getPassword(), securityKey);
				user.setPassword(encreptPwdAuth);
				// return userRepository.save(user);
				this.userRepository.save(user);
			}
		} catch (Exception e) {
			Constant.LOGGER
					.error(" Error Inside UserService.java Value for insert User record: createUser  :: " + user);
		}

	}

	public User getUserById(String id, String schemaName) {
		Constant.LOGGER.info("Inside getUserById UserService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, User.class);

	}

	public User updateUser(String id, User user) {
//    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
//    	Query q = new Query();
//		q.limit(100);
//		Update update = new Update();
//		//update.set("userMongoId", " ");
//		update.setOnInsert("userMongoId", "1234567891234567");
//		UpdateResult updateResult = mongoTemplate.updateMulti(q, update, SuperAdmin.class);
//		Constant.LOGGER.info("userMongoId ----updated");
		TimeUpdate timeUpdate = new TimeUpdate();
		User updateduser = null;
		String schemaName = user.getSchemaName();
		Constant.LOGGER.info("Inside updateUser UserService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

		user.setId(id);
		SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(user.getContact().getPhoneNo().getPrimary());
		String encreptPwdAuth = new EncriptionUtil().encrypt(user.getPassword(), securityKey);
		user.setPassword(encreptPwdAuth);

		List<TaggedDevices> taggedDevices = new ArrayList<TaggedDevices>();
		try {
			taggedDevices = user.getTaggedDevices();
			Constant.LOGGER.info("---size-" + taggedDevices.size());
			Device device = null;
			for (int i = 0; i < taggedDevices.size(); i++) {
				device = deviceServie.getDeviceDataById(taggedDevices.get(i).getId(), schemaName);
				device.setId(taggedDevices.get(i).getId());
				device.setDeviceTagged(taggedDevices.get(i).isDeviceTagged());
				device.setTaggedUserName(taggedDevices.get(i).getTaggedUserName());
				this.deviceRepository.save(device);

			}

		} catch (Exception e) {
			// return null;
		}
		updateduser = this.userRepository.save(user);
		Company company = this.genericINterface.findById(updateduser.getCompany().getId(), Company.class);

		SuperAdmin superAdmin = null;
		Constant.LOGGER.info("---superAdmin------started");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		superAdmin = this.superAdminRepository.findByUserMongoId(updateduser.getId());
		superAdmin.setCompany(company);
		superAdmin.setCreatedByUser(updateduser.getCreatedByUser());
		superAdmin.setCreatedTimestamp(updateduser.getCreatedTimestamp());
		superAdmin.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		superAdmin.setDeletedByUser(updateduser.getName().getFirstName());
		superAdmin.setDateTime(updateduser.getCreatedTimestamp());
		superAdmin.setPhoneNumber(updateduser.getContact().getPhoneNo().getPrimary());
		superAdmin.setSchemaName(updateduser.getSchemaName());
		superAdmin.setStatus(updateduser.getStatus());
		superAdmin.setUpdatedTimestamp(updateduser.getUpdatedTimestamp());
		superAdmin.setUserName(updateduser.getUserName());
		superAdmin.setPassword(updateduser.getPassword());
		superAdmin.setSuperAdmin(false);
		superAdmin.setUserMongoId(updateduser.getId());
		this.superAdminRepository.save(superAdmin);

		return updateduser;

	}

	// hard delete
	public Map<String, String> deleteUser(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		TimeUpdate timeUpdate = new TimeUpdate();
		Constant.LOGGER.info("Inside deleteUser UserService.java");
		User updateduser = this.genericINterface.findById(id, User.class);
		String userName = updateduser.getUserName();
		// this.userRepository.deleteById(id);
		updateduser.setStatus(Status.INACTIVE);
		updateduser = this.userRepository.save(updateduser);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "User deleted successfully");

		Constant.LOGGER.info("userName------------------" + response.getOrDefault("message", " "));
		Constant.LOGGER.info("userName------------------" + userName);

		if (userName != null && response.getOrDefault("message", " ").equals("User deleted successfully")) {
			Company company = this.genericINterface.findById(updateduser.getCompany().getId(), Company.class);
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
			SuperAdmin superAdmin = superAdminRepository.findByUserName(userName);
			Constant.LOGGER.info("superAdmin------------------" + superAdmin.toString());

			superAdmin.setCompany(company);
			superAdmin.setCreatedByUser(updateduser.getCreatedByUser());
			superAdmin.setCreatedTimestamp(updateduser.getCreatedTimestamp());
			superAdmin.setDateTime(updateduser.getCreatedTimestamp());
			superAdmin.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
			superAdmin.setDeletedByUser(updateduser.getName().getFirstName());
			superAdmin.setPhoneNumber(updateduser.getContact().getPhoneNo().getPrimary());
			superAdmin.setSchemaName(updateduser.getSchemaName());
			superAdmin.setStatus(Status.INACTIVE);
			superAdmin.setUpdatedTimestamp(updateduser.getUpdatedTimestamp());
			superAdmin.setUserName(updateduser.getUserName());
			superAdmin.setPassword(updateduser.getPassword());
			superAdmin.setSuperAdmin(false);
			superAdmin.setUserMongoId(updateduser.getId());
			this.superAdminRepository.save(superAdmin);

			// superAdminService.deleteSuperAdmin(superAdmin.getId(), "VTrackSuperAdmin");
			// // now onwards we are not deleting the data just updating the status
			Constant.LOGGER.info("User deleted successfully from superadmin------------------");
			return response;
		} else {
			Constant.LOGGER.info("userName------------------null");
			return response;
		}

	}

	// soft delete
	public Map<String, String> deleteSoftUser(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside deleteSoftUser UserService.java");
		User user = this.genericINterface.findById(id, User.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		user.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		user.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(user);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "User deleted successfully");
		return response;

	}

	public List<User> streamAllUsers() {
		return userRepository.findAll();
	}

	public User updateUserPatch(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside updateUserPatch UserService.java");
		User userMono = this.genericINterface.findById(id, User.class);
		User user = null;
		if (userMono != null) {
			user = userMono;
		}
		user.setSchemaName(schemaName);

		return this.userRepository.save(user);
	}

	public String getDeycryptPassword(Login login, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getDeycryptPassword UserService.java");
		SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(login.getUserName());
		String decryptPwdAuth = new EncriptionUtil().decrypt(login.getPassword(), securityKey);
		return decryptPwdAuth;

	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersWeb(String schemaName, String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllUsersWeb UserService.java");
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is(status));
		return this.genericINterface.find(query, com.fidz.entr.app.reportusers.User.class);
	}

//To get all Users web Paginated
	public List<com.fidz.entr.app.reportusers.User> getAllUsersWebPaginated(int pageNumber, int pageSize,
			String schemaName, String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllUsersWebPaginated UserService.java");
		Query query = new Query();
		List<com.fidz.entr.app.reportusers.User> users = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest).addCriteria(Criteria.where("status").is(status));
			users = this.genericINterface.find(query, com.fidz.entr.app.reportusers.User.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred inside getAllUsersWebPaginated UserService.java " + ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllUsersWebPaginated " + users.toString());
		return users;
	}

	public User updatePunchStatus(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

		User userMono = this.genericINterface.findById(id, User.class);
		User user = null;
		if (userMono != null) {
			user = userMono;
		}
		// user.setPunchStatus(punchStatus);
		return this.userRepository.save(user);

	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRoles(String schemaName, String id, String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllUsersbaseRoles OneLevel UserService.java");
		List<com.fidz.entr.app.reportusers.User> users = null;
		com.fidz.entr.app.reportusers.User use = this.genericINterface.findById(id,
				com.fidz.entr.app.reportusers.User.class);
		Integer useid = use.getUserRoleId();
		String userMobile = use.getUserName();
		Constant.LOGGER.info("values$$$$$" + useid + "" + userMobile);
		Query q1 = new Query();
		if (useid == 2) {
			Constant.LOGGER.info("Inside if");
			q1.addCriteria(Criteria.where("status").is(status));
			users = this.genericINterface.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 3) {
			Constant.LOGGER.info("Inside else if for role id 3");
			q1.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 4) {
			Constant.LOGGER.info("Inside else if for role id 4");
			q1.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 5) {
			Constant.LOGGER.info("Inside else if for role id 5");
			q1.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 6) {
			Constant.LOGGER.info("Inside else if for role id 6");
			q1.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 7) {
			Constant.LOGGER.info("Inside else if for role id 7");
			q1.addCriteria(Criteria.where("userName").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);

		}

		return users;

	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersRoles(String schemaName, String id, String status) {
		Constant.LOGGER.info("Inside getAllUsersRoles Alllevels UserService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportusers.User> users = null;
		List<com.fidz.entr.app.reportusers.User> users1 = null;
		List<com.fidz.entr.app.reportusers.User> users3 = null;
		List<com.fidz.entr.app.reportusers.User> users4 = null;

		List<com.fidz.entr.app.reportusers.User> obj = new ArrayList<com.fidz.entr.app.reportusers.User>();
		com.fidz.entr.app.reportusers.User use = this.genericINterface.findById(id,
				com.fidz.entr.app.reportusers.User.class);
		Integer useid = use.getUserRoleId();
		String userMobile = use.getUserName();
		Constant.LOGGER.info("values$$$$$" + useid + "" + userMobile);
		Query q = new Query();
		Query q1 = new Query();
		Query q3 = new Query();
		Query q4 = new Query();
		if (useid == 2) {
			Constant.LOGGER.info("Inside if");
			q1.addCriteria(Criteria.where("status").is(status));
			users = this.genericINterface.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
			obj.addAll(users);
		} else if (useid == 3) {
			Constant.LOGGER.info("Inside else if for role id 3");

			q.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q, com.fidz.entr.app.reportusers.User.class);
			List<String> nos = users.stream().map(p -> p.getUserName()).collect(Collectors.toList());
			obj.addAll(users);
			q1.addCriteria(Criteria.where("managerMobile").in(nos).andOperator(Criteria.where("status").is(status)));
			users1 = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			obj.addAll(users1);
			List<String> nos1 = users1.stream().map(p -> p.getUserName()).collect(Collectors.toList());
			q3.addCriteria(Criteria.where("managerMobile").in(nos1).andOperator(Criteria.where("status").is(status)));
			users3 = this.mongoTemplate.find(q3, com.fidz.entr.app.reportusers.User.class);
			obj.addAll(users3);
			List<String> nos2 = users3.stream().map(p -> p.getUserName()).collect(Collectors.toList());
			q4.addCriteria(Criteria.where("managerMobile").in(nos2).andOperator(Criteria.where("status").is(status)));
			users4 = this.mongoTemplate.find(q4, com.fidz.entr.app.reportusers.User.class);
			obj.addAll(users4);

		} else if (useid == 4) {

			Constant.LOGGER.info("Inside else if for role id 4");
			q.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q, com.fidz.entr.app.reportusers.User.class);
			List<String> nos = users.stream().map(p -> p.getUserName()).collect(Collectors.toList());
			obj.addAll(users);
			q1.addCriteria(Criteria.where("managerMobile").in(nos).andOperator(Criteria.where("status").is(status)));
			users1 = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			obj.addAll(users1);
			List<String> nos1 = users1.stream().map(p -> p.getUserName()).collect(Collectors.toList());
			q3.addCriteria(Criteria.where("managerMobile").in(nos1).andOperator(Criteria.where("status").is(status)));
			users3 = this.mongoTemplate.find(q3, com.fidz.entr.app.reportusers.User.class);
			obj.addAll(users3);

		} else if (useid == 5) {
			Constant.LOGGER.info("Inside else if for role id 5");
			q.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q, com.fidz.entr.app.reportusers.User.class);
			List<String> nos = users.stream().map(p -> p.getUserName()).collect(Collectors.toList());
			obj.addAll(users);
			q1.addCriteria(Criteria.where("managerMobile").in(nos).andOperator(Criteria.where("status").is(status)));
			users1 = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			obj.addAll(users1);

		} else if (useid == 6) {
			Constant.LOGGER.info("Inside else if for role id 5");
			q.addCriteria(
					Criteria.where("managerMobile").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q, com.fidz.entr.app.reportusers.User.class);
			// List<String> nos =
			// users.stream().map(p->p.getUserName()).collect(Collectors.toList());
			obj.addAll(users);
		} else if (useid == 7) {
			Constant.LOGGER.info("Inside else if for role id 7");
			q.addCriteria(Criteria.where("userName").is(userMobile).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q, com.fidz.entr.app.reportusers.User.class);
			obj.addAll(users);
		}

		return obj;
	}

	public User getAllUserByuserName(String username, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllUserByuserName  UserService.java");
		User users = null;
		Query query = new Query();
		query.addCriteria(Criteria.where("status").is(Status.ACTIVE));
		try {
			users = this.userRepository.findByUserName(username);
		} catch (Exception ex) {
			Constant.LOGGER.error("error file fetching getAllUSerByUserName");
		}
		return users;

	}

//	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesAboveOnelevel(String schemaName, String id) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesAboveOnelevel(String schemaName, int roleid,
			String status) {
		Constant.LOGGER.info("Inside getAllUsersbaseRolesAboveOnelevel  UserService.java");

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportusers.User> users = null;
		// com.fidz.entr.app.reportusers.User use=
		// this.genericINterface.findById(id,com.fidz.entr.app.reportusers.User.class);
		// Integer useid = use.getUserRoleId();
		// String userMobile = use.getUserName();
		Constant.LOGGER.info("values$$$$$" + roleid);
		Query q1 = new Query();
		if (roleid == 2) {
			Constant.LOGGER.info("Inside if");
			q1.addCriteria(Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);

		} else if (roleid == 3) {
			Constant.LOGGER.info("Inside else if for role id 3");
			q1.addCriteria(Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 4) {
			Constant.LOGGER.info("Inside else if for role id 4");
			q1.addCriteria(Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 5) {
			Constant.LOGGER.info("Inside else if for role id 5");
			q1.addCriteria(Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 6) {
			Constant.LOGGER.info("Inside else if for role id 6");
			q1.addCriteria(Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 7) {
			Constant.LOGGER.info("Inside else if for role id 7");
			q1.addCriteria(Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status)));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);

		}

		return users;
	}

//To fetch users based on department and company
	public List<com.fidz.entr.app.reportusers.User> getAllUsersWebBasedOnDepartmentandCompany(String did, String cid,
			String schemaName, String status) {

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportusers.User> users = null;
		Constant.LOGGER.info("Inside getAllUsersWebBasedOnDepartmentandCompany UserService.java");
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("department.id").is(did).andOperator(
					Criteria.where("company.id").is(cid).andOperator(Criteria.where("status").is(status))));
			users = this.genericINterface.find(q, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info(
					"successfully fetched users in getAllUsersWebBasedOnDepartmentandCompany UserService.java" + users);
		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error while fetching users in getAllUsersWebBasedOnDepartmentandCompany UserService.java");

		}

		return users;
	}

	public List<User> getAllUsersWebBasedOnDepartmentandCompanyLatest(String did, String cid, String schemaName,
			String status) {

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<User> users = null;
		Constant.LOGGER.info("Inside getAllUsersWebBasedOnDepartmentandCompany UserService.java");
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("department.id").is(did).andOperator(
					Criteria.where("company.id").is(cid).andOperator(Criteria.where("status").is(status))));
			users = this.genericINterface.find(q, User.class);
			Constant.LOGGER.info(
					"successfully fetched users in getAllUsersWebBasedOnDepartmentandCompany UserService.java" + users);
		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error while fetching users in getAllUsersWebBasedOnDepartmentandCompany UserService.java");

		}

		return users;
	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesAboveOnelevelbyDeptandRoleId(String deptid,
			String schemaName, int roleid, String status) {
		Constant.LOGGER.info("Inside getAllUsersbaseRolesAboveOnelevelbyDeptandRoleId  UserService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportusers.User> users = null;
		Constant.LOGGER.info("values$$$$$" + roleid);
		Query q1 = new Query();
		if (roleid == 2) {
			Constant.LOGGER.info("Inside if");
			q1.addCriteria(Criteria.where("department.id").is(deptid).andOperator(
					Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);

		} else if (roleid == 3) {
			Constant.LOGGER.info("Inside else if for role id 3");
			q1.addCriteria(Criteria.where("department.id").is(deptid).andOperator(
					Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 4) {
			Constant.LOGGER.info("Inside else if for role id 4");
			q1.addCriteria(Criteria.where("department.id").is(deptid).andOperator(
					Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 5) {
			Constant.LOGGER.info("Inside else if for role id 5");
			q1.addCriteria(Criteria.where("department.id").is(deptid).andOperator(
					Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 6) {
			Constant.LOGGER.info("Inside else if for role id 6");
			q1.addCriteria(Criteria.where("department.id").is(deptid).andOperator(
					Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (roleid == 7) {
			Constant.LOGGER.info("Inside else if for role id 7");
			q1.addCriteria(Criteria.where("department.id").is(deptid).andOperator(
					Criteria.where("userRoleId").is(roleid).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);

		}

		return users;
	}

	public User getbyDeviceID(String id, String schemaName) {
		User user = null;
		Constant.LOGGER.info("Inside getbyDeviceID UserService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		try {

			Query query1 = new Query();
			query1.addCriteria(Criteria.where("device.id").is(id));
			user = this.mongoTemplate.findOne(query1, User.class);
			if (user != null) {
				return user;

			} else {
				throw new UserNotFoundException(id);
			}
		} catch (Exception ex) {
			Constant.LOGGER.error(" Error occured Inside getbyDeviceID: getbyDeviceID  :: " + ex.getMessage());

		}

		return user;

	}

	public String getStatusofObjectMappedOrNot(String type, String schemaName, String id, String status) {
		Constant.LOGGER.info("Inside getStatusofObjectMappedOrNot  UserService.java");

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query q1 = new Query();
		List<User> users = null;
		String message = "";
		switch (type.trim()) {
		case "country":
			Constant.LOGGER.info("inside type country");
			q1.addCriteria(Criteria.where("country.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			Constant.LOGGER.info("user size" + users.size());
			if (users.size() <= 0) {

				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		case "region":
			q1.addCriteria(Criteria.where("region.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			if (users.size() <= 0) {
				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		case "state":
			q1.addCriteria(Criteria.where("state.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			if (users.size() <= 0) {
				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		case "city":
			q1.addCriteria(Criteria.where("city.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			if (users.size() <= 0) {
				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		case "userRole":
			q1.addCriteria(Criteria.where("userRole.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			if (users.size() <= 0) {
				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		case "company":
			q1.addCriteria(Criteria.where("company.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			if (users.size() <= 0) {
				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		case "department":
			q1.addCriteria(Criteria.where("department.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			if (users.size() <= 0) {
				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		case "device":
			q1.addCriteria(Criteria.where("device.id").is(id).andOperator(Criteria.where("status").is(status)));
			users = this.genericINterface.find(q1, User.class);
			if (users.size() <= 0) {
				message = "your seleted " + type + " not mapped";
				break;
			} else {
				message = "your seleted " + type + " mapped";
				break;
			}

		default:
			message = "Give proper Type to check the validation";
			break;
		}

		return message;
	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesForGeofences(String schemaName, String id,
			String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllUsersbaseRoles OneLevel UserService.java");
		List<com.fidz.entr.app.reportusers.User> users = null;
		com.fidz.entr.app.reportusers.User use = this.genericINterface.findById(id,
				com.fidz.entr.app.reportusers.User.class);
		Integer useid = use.getUserRoleId();
		String userMobile = use.getUserName();
		Constant.LOGGER.info("values$$$$$" + useid + "" + userMobile);
		Query q1 = new Query();
		if (useid == 2) {
			Constant.LOGGER.info("Inside if");
			q1.addCriteria(Criteria.where("status").is(status).andOperator(Criteria.where("geofenceTagged").is(false)));
			users = this.genericINterface.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 3) {
			Constant.LOGGER.info("Inside else if for role id 3");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(false).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 4) {
			Constant.LOGGER.info("Inside else if for role id 4");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(false).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 5) {
			Constant.LOGGER.info("Inside else if for role id 5");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(false).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 6) {
			Constant.LOGGER.info("Inside else if for role id 6");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(false).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 7) {
			Constant.LOGGER.info("Inside else if for role id 7");
			q1.addCriteria(Criteria.where("userName").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(false).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);

		}

		return users;

	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesForGeofencesMapped(String schemaName, String id,
			String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllUsersbaseRoles OneLevel UserService.java");
		List<com.fidz.entr.app.reportusers.User> users = null;
		com.fidz.entr.app.reportusers.User use = this.genericINterface.findById(id,
				com.fidz.entr.app.reportusers.User.class);
		Integer useid = use.getUserRoleId();
		String userMobile = use.getUserName();
		Constant.LOGGER.info("values$$$$$" + useid + "" + userMobile);
		Query q1 = new Query();
		if (useid == 2) {
			Constant.LOGGER.info("Inside if");
			q1.addCriteria(Criteria.where("status").is(status).andOperator(Criteria.where("geofenceTagged").is(false)));
			users = this.genericINterface.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 3) {
			Constant.LOGGER.info("Inside else if for role id 3");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(true).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 4) {
			Constant.LOGGER.info("Inside else if for role id 4");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(true).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 5) {
			Constant.LOGGER.info("Inside else if for role id 5");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(true).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 6) {
			Constant.LOGGER.info("Inside else if for role id 6");
			q1.addCriteria(Criteria.where("managerMobile").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(true).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);
		} else if (useid == 7) {
			Constant.LOGGER.info("Inside else if for role id 7");
			q1.addCriteria(Criteria.where("userName").is(userMobile).andOperator(
					Criteria.where("geofenceTagged").is(true).andOperator(Criteria.where("status").is(status))));
			users = this.mongoTemplate.find(q1, com.fidz.entr.app.reportusers.User.class);
			Constant.LOGGER.info("Userssss" + users);

		}

		return users;
	}

	public List<com.fidz.entr.app.reportusers.User> getAllUsersWebBasedOnDepartmentandCompanyandCity(String did,
			String cid, String cityid, String schemaName, String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportusers.User> users = null;

		Constant.LOGGER.info("Inside did - " + did);
		Constant.LOGGER.info("Inside cid - " + cid);
		Constant.LOGGER.info("Inside cityid - " + cityid);
		Constant.LOGGER.info("Inside schemaName - " + schemaName);
		Constant.LOGGER.info("Inside status - " + status);

		Query q = new Query();
		try {

			q.addCriteria(Criteria.where("company.id").is(cid)
					.andOperator(Criteria.where("department.id").is(did).andOperator(Criteria.where("citylist.id")
							.in(cityid).andOperator(Criteria.where("status").is(status)))));

			users = this.mongoTemplate.find(q, com.fidz.entr.app.reportusers.User.class);

			Constant.LOGGER.info(
					"successfully fetched users in getAllUsersWebBasedOnDepartmentandCompany UserService.java" + users);
		} catch (Exception ex) {
			Constant.LOGGER.info("Exception -----");
			Constant.LOGGER
					.error("Error while fetching users in getAllUsersWebBasedOnDepartmentandCompany UserService.java");

		}

		return users;
	}

	public User updateUserMakeActive(String id, @Valid User user) throws NameNotFoundException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(user.getSchemaName());
		User updatedToActiveuser = null;
		List<com.fidz.entr.app.reportusers.User> usersList = null;
		Constant.LOGGER
				.error("Error while fetching users in getAllUsersWebBasedOnDepartmentandCompany UserService.java");

		if (user.getDepartment().getId() != null) {
			Constant.LOGGER
					.error("Error while fetching users in getAllUsersWebBasedOnDepartmentandCompany UserService.java");
			String did = user.getDepartment().getId();
			String comId = user.getCompany().getId();
			String schemaName = user.getSchemaName();
			String status = "ACTIVE";
			usersList = getAllUsersWebBasedOnDepartmentandCompany(did, comId, schemaName, status);
			if (usersList.size() > 0) {
				Constant.LOGGER.error("Department is fine ");
				Device existingDevice = deviceServie.getDeviceDataById(user.getDevice().getId(), schemaName);

				if (existingDevice != null) {
					Constant.LOGGER.error("device is fine ");
					updatedToActiveuser = updateUser(id, user);

				} else {
					throw new NameNotFoundException("Your Device is Not Exist please create a Device and Map newly");
				}
			} else {
				throw new NameNotFoundException(
						"Your Department is Not Exist please create a department and Map newly");
			}

		}

		return updatedToActiveuser;

	}

	public List<com.fidz.entr.app.reportcustomerswebPlan.User> getAllUsersWebPaginatedTextSearch(int pageNumber,
			int pageSize, String schemaName, String status, String text) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportcustomerswebPlan.User> user = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside com.fidz.entr.app.reportusers.User UserService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(com.fidz.entr.app.reportcustomerswebPlan.User.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			user = mongoTemplate.find(query, com.fidz.entr.app.reportcustomerswebPlan.User.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside com.fidz.entr.app.reportusers.User UserService.java" + ex.getMessage());

		}
		return user;
	}

	// public User updateUserMakeActive(String id, @Valid User user) {

//    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
//    	Query q = new Query();
//		q.limit(100);
//		Update update = new Update();
//		//update.set("userMongoId", " ");
//		update.setOnInsert("userMongoId", "1234567891234567");
//		UpdateResult updateResult = mongoTemplate.updateMulti(q, update, SuperAdmin.class);
//		Constant.LOGGER.info("userMongoId ----updated");\

//    	TimeUpdate timeUpdate = new TimeUpdate();
//    	User updateduser=null;
//    	String schemaName=user.getSchemaName();
//	   	Constant.LOGGER.info("Inside updateUser UserService.java");
//       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//
//    	user.setId(id);
//		SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(user.getContact().getPhoneNo().getPrimary());
//		String encreptPwdAuth = new EncriptionUtil().encrypt(user.getPassword(), securityKey);
//		user.setPassword(encreptPwdAuth);
//		
//		
//		List<TaggedDevices> taggedDevices = new ArrayList<TaggedDevices>();
//		try {
//			taggedDevices = user.getTaggedDevices();
//			Constant.LOGGER.info("---size-"+taggedDevices.size());
//			Device device =null;	
//    		for(int i = 0;i<taggedDevices.size();i++)
//    		{
//    			device = deviceServie.getDeviceDataById(taggedDevices.get(i).getId(), schemaName);
//    			device.setId(taggedDevices.get(i).getId());
//    			device.setDeviceTagged(taggedDevices.get(i).isDeviceTagged());
//    			device.setTaggedUserName(taggedDevices.get(i).getTaggedUserName());
//    			this.deviceRepository.save(device);
//    			
//    		}
//		
//		}catch(Exception e) {
//			//return null;
//		}
//		updateduser =  this.userRepository.save(user);
//		Company company=this.genericINterface.findById(updateduser.getCompany().getId(), Company.class);
//		
//		SuperAdmin superAdmin = null;
//		Constant.LOGGER.info("---superAdmin------started");
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
//		superAdmin = this.superAdminRepository.findByUserMongoId(updateduser.getId());
//    	superAdmin.setCompany(company);
//		superAdmin.setCreatedByUser(updateduser.getCreatedByUser());
//		superAdmin.setCreatedTimestamp(updateduser.getCreatedTimestamp());
//		superAdmin.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
//		superAdmin.setDeletedByUser(updateduser.getName().getFirstName());
//		superAdmin.setDateTime(updateduser.getCreatedTimestamp());
//		superAdmin.setPhoneNumber(updateduser.getContact().getPhoneNo().getPrimary());
//		superAdmin.setSchemaName(updateduser.getSchemaName());
//		superAdmin.setStatus(updateduser.getStatus());
//		superAdmin.setUpdatedTimestamp(updateduser.getUpdatedTimestamp());
//		superAdmin.setUserName(updateduser.getUserName());
//		superAdmin.setPassword(updateduser.getPassword());
//		superAdmin.setSuperAdmin(false);
//		superAdmin.setUserMongoId(updateduser.getId());
//		this.superAdminRepository.save(superAdmin);
//		
//		return updateduser;
//        

//	}

}
