package com.fidz.entr.app.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class PunchStatusCount {
	
	List<PunchStatus> punchStatusList = new ArrayList<PunchStatus>();
	protected int count ;
	
	
	public PunchStatusCount() {
		
	}
	
	
	
	
}



