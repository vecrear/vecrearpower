package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.TimeTamper;

@Repository
public interface TimeTamperRepository extends ReactiveMongoRepository<TimeTamper, String>{

}
