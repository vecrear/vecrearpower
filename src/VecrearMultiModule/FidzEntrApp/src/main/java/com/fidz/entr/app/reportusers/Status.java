package com.fidz.entr.app.reportusers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum Status {
	DRAFT, ACTIVE, INACTIVE, DELETED
}
