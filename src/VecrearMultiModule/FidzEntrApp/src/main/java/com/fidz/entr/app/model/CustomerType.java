package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum CustomerType {
	PROSPECTIVE, REGISTERED, BOTH, NONE
}
