package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Location;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;

import lombok.Data;

@Data
@Document(collection = "activity")
@JsonIgnoreProperties(value = { "target" })
public class Activity extends Base {
	@Id
	protected String id;
	
	@NotNull
	@DBRef(lazy = true)
	protected Company company;
	
	@NotNull
	@DBRef(lazy = true)
	protected Department department;
	
	@DBRef(lazy = true)
	protected ActivityType activityType;
	
	@NotNull
	protected ActivityConfigurationType activityConfigurationType;
	
	@DBRef(lazy = true)
	protected User resource;
	
	protected List<ActivityUserList> resourceList;

	@DBRef(lazy = true)
	protected Customer customer;
	
	/**
	 * Activity Details is configured for Single Entry or Multiple Entry. For Single Entry, there would be only one
	 * item in this list. For Multiple Entry, there would be multiple entries in this list.
	 */

	//@DBRef(lazy = true)
	protected List<ActivityLog> activityLogs;
	
	@NotNull
	protected ActivityStatus activityStatus;
	
	//protected long plannedStartDateTime;
	
	//protected long plannedEndDateTime;
	
	//protected long actualStartDateTime;
	
	//protected long actualEndDateTime;
    protected Date plannedStartDateTime;
	
	protected Date plannedEndDateTime;
	
	protected Date actualStartDateTime;
	
	protected Date actualEndDateTime;
	
	protected Location plannedLocation;
	
	protected Location performedLocation;
	
	protected String comments;
	
	//@DBRef(lazy = true)
	protected List<ActivityStatusChangeLog> activityStatusChangeLog;
	
	protected Customer prospectCustomer;
	
	public Activity(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull Company company, @NotNull Department department,
			ActivityType activityType, @NotNull ActivityConfigurationType activityConfigurationType, User resource,
			Customer customer, List<ActivityUserList> resourceList, List<ActivityLog> activityLogs,
			@NotNull ActivityStatus activityStatus, Date plannedStartDateTime, Date plannedEndDateTime,
			Date actualStartDateTime, Date actualEndDateTime, Location plannedLocation, Location performedLocation,
			String comments, List<ActivityStatusChangeLog> activityStatusChangeLog,Customer prospectCustomer) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.company = company;
		this.department = department;
		this.activityType = activityType;
		this.activityConfigurationType = activityConfigurationType;
		this.resource = resource;
		this.customer = customer;
		this.resourceList = resourceList;
		this.activityLogs = activityLogs;
		this.activityStatus = activityStatus;
		this.plannedStartDateTime = plannedStartDateTime;
		this.plannedEndDateTime = plannedEndDateTime;
		this.actualStartDateTime = actualStartDateTime;
		this.actualEndDateTime = actualEndDateTime;
		this.plannedLocation = plannedLocation;
		this.performedLocation = performedLocation;
		this.comments = comments;
		this.activityStatusChangeLog = activityStatusChangeLog;
		this.prospectCustomer = prospectCustomer;
	}

	public Activity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Activity(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		// TODO Auto-generated constructor stub
	}

	



	
	
	


	

	

	


	

	

	

}
