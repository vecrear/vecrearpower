package com.fidz.entr.app.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.Valid;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.controller.UserNameAppender;
import com.fidz.entr.app.exception.CustomerNotFoundException;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.Notification;
import com.fidz.entr.app.model.PlannedUsers;
import com.fidz.entr.app.model.PlannedUsersAndroid;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserActivityNotify;
import com.fidz.entr.app.repository.CustomerRepository;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;

@Service("FEACustomerService")
public class CustomerService {
	private GenericAppINterface<Customer> genericINterface;
	@Autowired
	public CustomerService(GenericAppINterface<Customer> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private CustomerRepository  customerRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	 @Autowired
	 private NotificationSendingService notificationSendingService;
	
	
	 @Autowired
	 private FCMServic fcmServic;
	 
	  public static String name = "Customer";
	
	public List<Customer> getAllCustomers(String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCustomers CustomerService.java");
    	List<Customer> customers=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	customers=this.genericINterface.findAll(Customer.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCustomers CustomerService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCustomers CustomerService.java");
    	return customers;
	}

	public Customer createCustomer(Customer customer) {
		Customer customerss = new Customer();
        Notification noty = new Notification();
          Customer cust = null;
			Constant.LOGGER.info(
					" Inside CustomerService.java Value for insert Customer record:createCustomer :: " + customer);
			String schemaName = customer.getSchemaName();
			System.out.print(schemaName);
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        //List<Customer> customers = this.genericINterface.findAll(Customer.class);
        //List<Customer> customers = this.customerRepository.findByName(customer.getName());
        List<Customer> customers =null;
      	Query q = new Query();
      	q.addCriteria(Criteria.where("name").regex(customer.getName(),"i"));
      	customers = this.mongoTemplate.find(q, Customer.class);
	        List<String> names = customers.stream().map(p->p.getName()).collect(Collectors.toList());
	          Constant.LOGGER.info("DeviceType List data"+names);

	          if (names.stream().anyMatch(s -> s.equals(customer.getName())==true)) {
	              Constant.LOGGER.info("Inside if statement");
	              throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+customer.getName());	       
	              }
	          else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(customer.getName())==true)) {
	                  Constant.LOGGER.info("Inside else if statement");
		          throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+customer.getName());	       
	          }else{
	              Constant.LOGGER.info("Inside else statement");
	              Constant.LOGGER.info(this.customerRepository.save(customer).toString());
	              customerss = this.customerRepository.save(customer);  
	              if(customerss!=null) {
	            	  cust = this.genericINterface.findById(customerss.getId(), Customer.class);
	            	  
	            	  List<User> notifiedUsers = cust.getUsers();
	            	  Constant.LOGGER.error("notifiedUsers----"+notifiedUsers.toString());
	                  UserActivityNotify userActivityNoty =  new UserActivityNotify();
	                 // notifiedUsers.forEach(user -> {
  						for(int i = 0; i<=notifiedUsers.size()-1;i++) {
  						if(notifiedUsers.get(i).getNotificationId() !=null) { 
  							Constant.LOGGER.error("user.getNotificationId()----"+notifiedUsers.get(i).getNotificationId());
  							userActivityNoty.setNotificationId(notifiedUsers.get(i).getNotificationId());
  							userActivityNoty.setUserActivityStatus("New Customer has been mapped to you! Please perform config sync");	
  							Constant.LOGGER.error("userActivityNoty----"+userActivityNoty.toString());
  							noty.setMessage(userActivityNoty.getUserActivityStatus());
							noty.setToken(userActivityNoty.getNotificationId());
							String res =this.notificationSendingService.sendNotification(noty);
							Constant.LOGGER.info("Notification Sending Response"+" "+res);
  	  	    			}else {
  	  	         			Constant.LOGGER.error("User Device not found");
  	  	         		}
	                  }//});
	                  
	              }
	              } 
		return customerss;
	}
	
	public void createCustomers(Customer customer){
        String schemaName=customer.getSchemaName();
    	Constant.LOGGER.info("Inside createCustomers CustomerService.java");

	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    this.genericINterface.saveName(customer);
	}
	
	public ResponseEntity<Customer> getCustomerById(String id, String schemaName){
		Customer customer=null;
    	Constant.LOGGER.info("Inside getCustomerById CustomerService.java");
         MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 customer=this.genericINterface.findById(id, Customer.class);
		if (customer == null) {
			throw new CustomerNotFoundException(id);
		} else {
			customer.setId(id);
			return ResponseEntity.ok(customer);
		}
		 //return this.genericINterface.findById(id, Customer.class);
	}
	
	public Stream<Customer> getCustomerByUserId(String userId, String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getCustomerByUserId CustomerService.java");

		//Stream<Customer> cus1=this.customerRepository.findByName(userId);
		Stream<Customer> cus=this.customerRepository.findAll().stream().filter(cust -> cust.getUsers().stream().anyMatch(x -> x.getId().equalsIgnoreCase(userId)));
		if(cus == null) {
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2");
			throw new CustomerNotFoundException("message","customer does not exist");
		}else {
			System.out.println("#################################");
			return cus;
		}
	}
	
    public Customer updateCustomer(String id, Customer customer , String validationStatus) throws ParseException {
        String schemaName=customer.getSchemaName();
    	Constant.LOGGER.info("Inside updateCustomer CustomerService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	        Notification noty = new Notification();

//	    List<User> userIds=customer.getUsers();
//	    List<Notification> nds=new ArrayList<Notification>() ;
//	    userIds.forEach(user -> {
//			if (user.getNotificationId()!=null) {
//				Notification notification = new Notification();
//				notification.setMessage("Customer Record Updated");
//				notification.setUniqueNotificationId(user.getNotificationId());
//				notification.setOsType("AndroidOS");
//				nds.add(notification);  // todo Notification
//			}
//		  });
//	    System.out.println("nds details"+nds);
//		NotificationForDeviceService.getInstance().sendNotification(nds);
	        //List<Customer> customers = this.customerRepository.findByName(customer.getName());
	        List<Customer> customers =null;
	      	Query q = new Query();
	      	q.addCriteria(Criteria.where("name").regex(customer.getName(),"i"));
	      	customers = this.mongoTemplate.find(q, Customer.class);
        //List<Customer> customers = this.genericINterface.findAll(Customer.class);
	        Customer ctr = this.genericINterface.findById(id, Customer.class);
	        List<String> names = customers.stream().map(p->p.getName()).collect(Collectors.toList());
	        names.remove(ctr.getName());  
	        Constant.LOGGER.info("DeviceType List datasss"+names);
	        
	        if (names.stream().anyMatch(s -> s.equals(customer.getName())==true)) {
	              Constant.LOGGER.info("Inside if statement");
	              throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+customer.getName());	       
	              }
	          else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(customer.getName())==true)) {
	                  Constant.LOGGER.info("Inside else if statement");
		          throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+customer.getName());	       
	          }else{
     
//  	------------------------------------notify Customers
//Ashok changes
  	
  	
  	Customer validate = this.genericINterface.findById(id, Customer.class);
  	String cusname1 = UserNameAppender.userNameAppend(validate.getContactName().getFirstName(), validate.getContactName().getMiddleName(), validate.getContactName().getMiddleName());
  	String cusname2 = UserNameAppender.userNameAppend(customer.getContactName().getFirstName(), customer.getContactName().getMiddleName(), customer.getContactName().getMiddleName());

		boolean cusnameValidation = validate.getName().equalsIgnoreCase(customer.getName());
		boolean phoneValidation = validate.getContact().getPhoneNo().getPrimary().equalsIgnoreCase(customer.getContact().getPhoneNo().getPrimary());
		boolean contactNameValidation = cusname1.equalsIgnoreCase(cusname2);
		boolean emailValidation = validate.getContact().getEmail().equalsIgnoreCase(customer.getContact().getEmail());
		boolean addressValidation = validate.getAddress().getLine1().equalsIgnoreCase(customer.getAddress().getLine1());
		boolean cityIdValidation = validate.getCity().getId().equalsIgnoreCase(customer.getCity().getId());
		
		Constant.LOGGER.info(" cusnameValidation"+cusnameValidation );
		Constant.LOGGER.info(" phoneValidation"+phoneValidation );
		Constant.LOGGER.info(" contactNameValidation"+contactNameValidation );
		Constant.LOGGER.info(" emailValidation"+emailValidation );
		Constant.LOGGER.info(" addressValidation"+addressValidation );
		Constant.LOGGER.info(" cityIdValidation"+cityIdValidation );
		
		
		 Customer customerss = new Customer();
	        Customer cust = null;
	    	customer.setId(id);
	    	customerss = this.customerRepository.save(customer);
	    	Constant.LOGGER.info("customers" +customerss);
//notifying only when there is change in prospect customer
		if(validationStatus.equalsIgnoreCase("prospect")) {
		if(cusnameValidation == false || phoneValidation == false || contactNameValidation == false || emailValidation == false || addressValidation == false || cityIdValidation == false) {
			
			
			if(customerss != null) {
				cust = this.genericINterface.findById(customerss.getId(), Customer.class);
          	  
          	  List<User> notifiedUsers = cust.getUsers();
          	  
				Constant.LOGGER.info("notifiedUsers----"+notifiedUsers.toString());
				UserActivityNotify userActivityNoty =  new UserActivityNotify();
				notifiedUsers.forEach(user -> {
						
						if(user.getNotificationId() !=null) { 
							Constant.LOGGER.error("user.getNotificationId()----"+user.getNotificationId());
							userActivityNoty.setNotificationId(user.getNotificationId());
							userActivityNoty.setUserActivityStatus("Customer has been updated! Please perform config sync");	
							Constant.LOGGER.error("userActivityNoty----"+userActivityNoty.toString());
							noty.setMessage(userActivityNoty.getUserActivityStatus());
							noty.setToken(userActivityNoty.getNotificationId());
							String res =this.notificationSendingService.sendNotification(noty);
							Constant.LOGGER.info("Notification Sending Response"+" "+res);
	  	    			
	  	         		}else {
	  	         			Constant.LOGGER.error("User Device not found");
	  	         		}
					});
				  

			}
        
			
			
			
		}
  	
		
		}else if(validationStatus.equalsIgnoreCase("others")) {
			if(customerss != null) {
				cust = this.genericINterface.findById(customerss.getId(), Customer.class);
          	  
          	  List<User> notifiedUsers = cust.getUsers();
          	  
				Constant.LOGGER.info("notifiedUsers----"+notifiedUsers.toString());
				UserActivityNotify userActivityNoty =  new UserActivityNotify();
				notifiedUsers.forEach(user -> {
						
						if(user.getNotificationId() !=null) { 
							Constant.LOGGER.error("user.getNotificationId()----"+user.getNotificationId());
							userActivityNoty.setNotificationId(user.getNotificationId());
							userActivityNoty.setUserActivityStatus("Customer has been updated! Please perform config sync");	
							Constant.LOGGER.error("userActivityNoty----"+userActivityNoty.toString());
							noty.setMessage(userActivityNoty.getUserActivityStatus());
							noty.setToken(userActivityNoty.getNotificationId());
							String res =this.notificationSendingService.sendNotification(noty);
							Constant.LOGGER.info("Notification Sending Response"+" "+res);
	  	    			
	  	         		}else {
	  	         			Constant.LOGGER.error("User Device not found");
	  	         		}
					});
				  

			}
			
		}
		
		

	          
  
	    return customerss;
	          }
    }

    
    //hard delete
   	public Map<String, String> deleteCustomer(String id, String schemaName) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside deleteCustomer CustomerService.java");
        this.customerRepository.deleteById(id);
   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "Customer deleted successfully");
   		return response;

   	}
        //soft delete
   	public Map<String, String> deleteSoftCustomer(String id, String schemaName, TimeUpdate timeUpdate) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside deleteSoftCustomer CustomerService.java");
       Customer customer = this.genericINterface.findById(id, Customer.class);
   		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
   		customer.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
   		customer.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
   		customer.setStatus(Status.INACTIVE);
   		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.genericINterface.saveName(customer);

   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "Customer deleted successfully");
   		return response;

   	}
    public List<Customer> streamAllCustomers() {
        return customerRepository.findAll();
    }

	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomersWeb(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCustomersWeb CustomerService.java");
    	List<com.fidz.entr.app.reportcustomers.Customer> customers=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	customers=this.genericINterface.findAll(com.fidz.entr.app.reportcustomers.Customer.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCustomersWeb CustomerService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCustomersWeb CustomerService.java");
    	return customers;
	}

	//To get all countries paginated
    //page number starts from zero
	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomersPaginated(int pageNumber, int pageSize,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllCustomersPaginated CustomerService.java");
    	Query query = new Query();
    	List<com.fidz.entr.app.reportcustomers.Customer> customers =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	customers= this.genericINterface.find(query, com.fidz.entr.app.reportcustomers.Customer.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllCustomersPaginated CustomerService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllCustomersPaginated "+query);
        return customers;
	}

	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomerswebByCutomerType(String custcategoryid,String custtype, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportcustomers.Customer> selectedCustomers = null;
		Constant.LOGGER.info("Inside getAllCustomerswebByCutomerType CustomerService.java");
		
		Collection type = new ArrayList();
	     type.add("REGISTERED");
	     type.add("PROSPECTIVE");
        Query q = new Query();
        try {
        	
        	
        	if(custtype.equals("BOTH")) {
        		q.addCriteria(Criteria.where("customerCategory.id").is(custcategoryid).andOperator(Criteria.where("customerType").is(type)));
        	}else  {
        		q.addCriteria(Criteria.where("customerCategory.id").is(custcategoryid).andOperator(Criteria.where("customerType").is(custtype)));
        	}
        	selectedCustomers = this.genericINterface.find(q,com.fidz.entr.app.reportcustomers.Customer.class );
        	Constant.LOGGER.info("successfully fetched users in getAllCustomers UserService.java"+selectedCustomers);
        }catch(Exception ex) {
			Constant.LOGGER.error("Error while fetching getAllCustomerswebByCutomerType Customer.java");

        }
		
		return selectedCustomers;
	}
	
	
	
	
	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomerswebByCutomerCategotyID(String custcategoryid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<com.fidz.entr.app.reportcustomers.Customer> selectedCustomers = null;
		Constant.LOGGER.info("Inside getAllCustomerswebByCutomerCategotyID CustomerService.java");
        Query q = new Query();
        try {
        	q.addCriteria(Criteria.where("customerCategory.id").is(custcategoryid));
        	selectedCustomers = this.genericINterface.find(q,com.fidz.entr.app.reportcustomers.Customer.class );
        	Constant.LOGGER.info("successfully fetched users in getAllCustomers UserService.java"+selectedCustomers);
        }catch(Exception ex) {
			Constant.LOGGER.error("Error while fetching Inside getAllCustomerswebByCutomerCategotyID CustomerService.java");

        }
		
		return selectedCustomers;
	}
	
	
	
	
	//to get customers based on users, customercategory and customertype
		public List<com.fidz.entr.app.reportcustomers.Customer> customerFilters(@Valid PlannedUsers plannedUsers, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerFilters CustomerService.java");
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomers.Customer> customers =null;
	    	List<String> userid= plannedUsers.getUserId();
	    	String custcatId= plannedUsers.getCustomerCategoryId();
	    	String custType=plannedUsers.getCustomerType();
	    	
	    	Collection type = new ArrayList();
		     type.add("REGISTERED");
		     type.add("PROSPECTIVE");
	       try {
	    	   if(custType.equals("BOTH")) {
	    		   query.addCriteria(Criteria.where("users.id").in(userid).andOperator
		    			   (Criteria.where("customerCategory.id").is(custcatId).andOperator
		    					   (Criteria.where("customerType").is(type))));
	    	   }else {
	    		   query.addCriteria(Criteria.where("users.id").in(userid).andOperator
		    			   (Criteria.where("customerCategory.id").is(custcatId).andOperator
		    					   (Criteria.where("customerType").is(custType))));
	    	   }
	    	  
	    	   
	    	   
	    		customers=this.genericINterface.find(query, com.fidz.entr.app.reportcustomers.Customer.class);
	    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerFilters CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return customers;
		}

		//to customers based on users for device
		public List<com.fidz.entr.app.reportcustomers.Customer> customerByuserDevice(@Valid PlannedUsersAndroid plannedUsers,
				String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java");
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomers.Customer> customers =null;
	    	List<String> userid= plannedUsers.getUserId();
	    	try {
	    	   query.addCriteria(Criteria.where("users.id").in(userid));
	    		customers=this.genericINterface.find(query, com.fidz.entr.app.reportcustomers.Customer.class);
	    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return customers;
		}

		public String getStatusofObjectMappedOrNot(String id, String schemaName) {
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		    	Constant.LOGGER.info("inside getStatusofObjectMappedOrNot CustomerService.java");
                 Query q1 = new  Query();
				com.fidz.entr.app.reportcustomers.Customer customer =null;
				String message = "";
				
				try {
					customer = this.genericINterface.findById(id, com.fidz.entr.app.reportcustomers.Customer.class);
					if (customer.getUsers()!= null && customer.getUsers().size()>0) {
						message = "Customer is mapped with Users";
					    Constant.LOGGER.error("activity is mapped with department  "+customer.getUsers().size());
					} else {
						message = "Customer is not mapped";
					}
			    	}catch(Exception ex) {
				    Constant.LOGGER.error("Error occurred inside getStatusofObjectMappedOrNot CustomerService.java  "+ex.getMessage());
			        }
					return message;
		}

		public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDeviceSimplifyCustomer(
				@Valid PlannedUsersAndroid plannedUsers, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java");
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomersdevice.Customer> customers =null;
	    	
	    	List<String> userid= plannedUsers.getUserId();
	    	try {
	    	   query.addCriteria(Criteria.where("users.id").in(userid));
	    		customers=this.genericINterface.find(query, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return customers;
		}

		public List<com.fidz.entr.app.reportcustomersweb.Customer> customerByuserWebSimplifyCustomer(
				String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("Inside getAllCustomersWeb CustomerService.java");
	    	List<com.fidz.entr.app.reportcustomersweb.Customer> customers=null;
	    	try {

	    	customers=this.genericINterface.findAll(com.fidz.entr.app.reportcustomersweb.Customer.class);
	    	}catch(Exception ex) {
	    	Constant.LOGGER.error("Error while getting data from getAllCustomersWeb CustomerService.java"+ex.getMessage());
	        }
	    	Constant.LOGGER.info("**************************************************************************");
	    	Constant.LOGGER.info("successfully fetched data from getAllCustomersWeb CustomerService.java");
	    	return customers;
		}

		public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDeviceBycitySimplifyCustomer(
				@Valid PlannedUsersAndroid plannedUsers, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java");
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomersdevice.Customer> customers =null;
	    	
	    	List<String> userid= plannedUsers.getUserId();
	    	List<String> cityIds = plannedUsers.getCityIds();
	    	Constant.LOGGER.info("plannedUsers.getUserId()----"+plannedUsers.getUserId());
	    	Constant.LOGGER.info("plannedUsers.getCityIds()----"+plannedUsers.getCityIds());
	    	Constant.LOGGER.info("deptId  deptId----"+plannedUsers.getDeptId());
	    	try {
	    			query.addCriteria(Criteria.where("city.id").in(cityIds).andOperator
    						 (Criteria.where("department.id").in(plannedUsers.getDeptId())));
	    		//query.addCriteria(Criteria.where("city.id").in(cityIds));
	    	   
	    		customers = this.genericINterface.find(query, com.fidz.entr.app.reportcustomersdevice.Customer.class);	
	    		
	    		
	    		Constant.LOGGER.info("customers----------size()"+customers.size());
	    		
	    		//Query query1 = new Query();
	    		
	    		//query1.addCriteria(Criteria.where("users.id").in(userid));
	    		
	    		//customers = this.genericINterface.find(query1, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return customers;
		}

		public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDeviceBycitySimplifyCustomerByType(
				String type, @Valid PlannedUsersAndroid plannedUsers, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java");
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomersdevice.Customer> customers =null;
	    	
	    	Collection custType = new ArrayList();
	    	custType.add("REGISTERED");
	    	custType.add("PROSPECTIVE");
	    	custType.add("BOTH");
	    	
	    	List<String> userid= plannedUsers.getUserId();
	    	List<String> cityIds = plannedUsers.getCityIds();
	    	Constant.LOGGER.info("plannedUsers.getUserId()----"+plannedUsers.getUserId().size());
	    	Constant.LOGGER.info("plannedUsers.getCityIds()----"+plannedUsers.getCityIds().size());
	    	Constant.LOGGER.info("deptId  deptId----"+plannedUsers.getDeptId());
	    	
	    	try {
	    		if(type.equals("BOTH")) {
    			   query.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
    	    			   			(Criteria.where("city.id").in(cityIds).andOperator
    	    			   			(Criteria.where("customerType").in(custType))));
//	    			 query.addCriteria(Criteria.where("users.id").in(userid).andOperator
//	    	    				   (Criteria.where("customerType").is(custType)));
	    		}else {
    			   query.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
    					   			(Criteria.where("city.id").in(cityIds).andOperator
    					   			(Criteria.where("customerType").in(type))));
//	    			query.addCriteria(Criteria.where("users.id").in(userid).andOperator
//	    					(Criteria.where("customerType").is(type)));
	    		}
	    
	    		customers = this.genericINterface.find(query, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return customers;
		}

		public List<com.fidz.entr.app.reportcustomersweb.Customer> customerByuserWebSimplifyCustomerBydeptid(
				String deptid,String cityId, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("Inside getAllCustomersWeb CustomerService.java"+deptid +"cityId"+cityId);
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomersweb.Customer> customers=null;
	    	try {
	    		query.addCriteria(Criteria.where("department.id").in(deptid).andOperator
 	    			   (Criteria.where("city.id").is(cityId)));
	    		
	    	customers=this.genericINterface.find(query,com.fidz.entr.app.reportcustomersweb.Customer.class);
	    	}catch(Exception ex) {
	    	Constant.LOGGER.error("Error while getting data from getAllCustomersWeb CustomerService.java"+ex.getMessage());
	        }
	    	Constant.LOGGER.info("**************************************************************************");
	    	Constant.LOGGER.info("successfully fetched data from getAllCustomersWeb CustomerService.java");
	    	return customers;
		}

		public List<Customer> getAllCustomersBydeptandCity(String deptid, String cityid, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("Inside getAllCustomersWeb CustomerService.java"+deptid +"cityId"+cityid);
	    	Query query = new Query();
	    	List<Customer> customers = null;
	    	try {
	    		query.addCriteria(Criteria.where("department.id").in(deptid).andOperator
 	    			   (Criteria.where("city.id").is(cityid)));
	    		
	    	customers = this.genericINterface.find(query,Customer.class);
	    	}catch(Exception ex) {
	    	Constant.LOGGER.error("Error while getting data from getAllCustomersWeb CustomerService.java"+ex.getMessage());
	        }
	    	Constant.LOGGER.info("**************************************************************************");
	    	Constant.LOGGER.info("successfully fetched data from getAllCustomersWeb CustomerService.java");
	    	return customers;
		}

		public List<com.fidz.entr.app.reportusers.User> cutomersByUser(@Valid PlannedUsersAndroid plannedUsers, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("Inside cutomersByUser CustomerService.java");
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportusers.User> userss = null;

	    try {	
	    	List<String> us = new ArrayList<>();
	    	query.addCriteria(Criteria.where("id").in(plannedUsers.getUserId()));
	    	List<com.fidz.entr.app.reportcustomersdevice.Customer> cus = this.genericINterface.find(query,com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    	cus.forEach(p->{
	    	p.getUsers().forEach(v->{
	    		us.add(v.getId());
	    	});
	    	});
	    	
	    	Query query1 = new Query();
	    	
	    	if(!us.isEmpty()) {
	        query1.addCriteria(Criteria.where("id").in(us));    
	    	userss = this.genericINterface.find(query1,com.fidz.entr.app.reportusers.User.class);
	    		

	    	}else {
	    		throw new UserNotFoundException("No users found for the customers");
	    	}
	    }catch(Exception ex){
	    	Constant.LOGGER.error("Inside cutomersByUser CustomerService.java"+ ex.getMessage());

	    	
	    }
	    	
	    	return userss;
		}

		public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomerBatchWise(
				@Valid PlannedUsersAndroid plannedUsers, String schemaName, int pageNumber, int pageSize) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java" + plannedUsers.toString());
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomersdevice.Customer> customers =null;
	    	
	    	List<String> userid= plannedUsers.getUserId();
	    	List<String> cityIds = plannedUsers.getCityIds();
	    	Constant.LOGGER.info("plannedUsers.getUserId()----"+plannedUsers.getUserId());
	    	Constant.LOGGER.info("plannedUsers.getCityIds()----"+plannedUsers.getCityIds());
	    	Constant.LOGGER.info("deptId  deptId----"+plannedUsers.getDeptId());
	    	try {
	    		final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
	    			query.addCriteria(Criteria.where("city.id").in(cityIds).andOperator
    						 (Criteria.where("department.id").in(plannedUsers.getDeptId())));
	    		//query.addCriteria(Criteria.where("city.id").in(cityIds));
	    	    query.with(pageableRequest);
	    		customers = this.genericINterface.find(query, com.fidz.entr.app.reportcustomersdevice.Customer.class);	
	    		
	    		
	    		Constant.LOGGER.info("customers----------size()"+customers.size());
	    		
	    		//Query query1 = new Query();
	    		
	    		//query1.addCriteria(Criteria.where("users.id").in(userid));
	    		
	    		//customers = this.genericINterface.find(query1, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return customers;
		}

		public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomerByTypeBatchWise(
				String type, @Valid PlannedUsersAndroid plannedUsers, String schemaName, int pageNumber, int pageSize) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java");
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomersdevice.Customer> customers =null;
	    	
	    	Collection custType = new ArrayList();
	    	custType.add("REGISTERED");
	    	custType.add("PROSPECTIVE");
	    	custType.add("BOTH");
	    	
	    	List<String> userid= plannedUsers.getUserId();
	    	List<String> cityIds = plannedUsers.getCityIds();
	    	Constant.LOGGER.info("plannedUsers.getUserId()----"+plannedUsers.getUserId().size());
	    	Constant.LOGGER.info("plannedUsers.getCityIds()----"+plannedUsers.getCityIds().size());
	    	Constant.LOGGER.info("deptId  deptId----"+plannedUsers.getDeptId());
	    	
	    	try {
	    		if(type.equals("BOTH")) {
    			   query.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
    	    			   			(Criteria.where("city.id").in(cityIds).andOperator
    	    			   			(Criteria.where("customerType").in(custType))));
//	    			 query.addCriteria(Criteria.where("users.id").in(userid).andOperator
//	    	    				   (Criteria.where("customerType").is(custType)));
	    		}else {
    			   query.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
    					   			(Criteria.where("city.id").in(cityIds).andOperator
    					   			(Criteria.where("customerType").in(type))));
//	    			query.addCriteria(Criteria.where("users.id").in(userid).andOperator
//	    					(Criteria.where("customerType").is(type)));
	    		}
	    		final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
	    	    query.with(pageableRequest);


	    		customers = this.genericINterface.find(query, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return customers;
		}

		@SuppressWarnings("unchecked")
		public JSONObject customerByuserDevicebycitySimplifyCustomerByTypeCount(
				String type, @Valid PlannedUsersAndroid plannedUsers, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java");
	    	Query query = new Query();	
             List<com.fidz.entr.app.reportcustomersdevice.Customer> customers =null;
	    	
	    	Collection custType = new ArrayList();
	    	custType.add("REGISTERED");
	    	custType.add("PROSPECTIVE");
	    	custType.add("BOTH");
	    	Map<String,Long> atr = new HashMap<String, Long>();
			JSONObject obj = new JSONObject();
	    	List<String> userid= plannedUsers.getUserId();
	    	List<String> cityIds = plannedUsers.getCityIds();
	    	Constant.LOGGER.info("plannedUsers.getUserId()----"+plannedUsers.getUserId().size());
	    	Constant.LOGGER.info("plannedUsers.getCityIds()----"+plannedUsers.getCityIds().size());
	    	Constant.LOGGER.info("deptId  deptId----"+plannedUsers.getDeptId());
	    	
	    	try {
	    		if(type.equals("BOTH")) {
    			   query.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
    	    			   			(Criteria.where("city.id").in(cityIds).andOperator
    	    			   			(Criteria.where("customerType").in(custType))));
//	    			 query.addCriteria(Criteria.where("users.id").in(userid).andOperator
//	    	    				   (Criteria.where("customerType").is(custType)));
	    		}else {
    			   query.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
    					   			(Criteria.where("city.id").in(cityIds).andOperator
    					   			(Criteria.where("customerType").in(type))));
//	    			query.addCriteria(Criteria.where("users.id").in(userid).andOperator
//	    					(Criteria.where("customerType").is(type)));
	    		}
	    		long x1=mongoTemplate.count(query, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		 atr.put("Total Customer Count", x1);
	    		 obj.put("totalCount", atr);
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    		
	    		
	    	return obj;
		}

		
		public List<com.fidz.entr.app.reportcustomers.Customer> customerTextSearch(@Valid String text,
				String schemaName,int pageNumber,int pageSize) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			List<com.fidz.entr.app.reportcustomers.Customer>  cus = null;
			TextIndexDefinition textIndex = null;
			TextCriteria criteria = null;
			Query query = null;
           Constant.LOGGER.info("Inside customerTextSearch CustomerService.java");
	    	try {
	    		
	    		textIndex = new TextIndexDefinitionBuilder()
	    				.onAllFields()
	    				.build();
	    		mongoTemplate.indexOps(com.fidz.entr.app.reportcustomers.Customer.class).ensureIndex(textIndex);
	    	    criteria = TextCriteria.forDefaultLanguage()
	    	    		.matchingAny(text).caseSensitive(false).diacriticSensitive(false);
                  query = TextQuery.queryText(criteria)
	    	    		  .sortByScore()
	    	    		  .with(PageRequest.of(pageNumber, pageSize));
	    		cus = mongoTemplate.find(query, com.fidz.entr.app.reportcustomers.Customer.class);
	    		
	    					
		}catch(Exception ex) {
	    	Constant.LOGGER.error("Inside customerTextSearch CustomerService.java"+ ex.getMessage());

		}
			return cus;
		}

		public List<com.fidz.entr.app.reportcustomersdevice.Customer> customerByuserDevicebycitySimplifyCustomerByDate(
				String type, @Valid PlannedUsersAndroid plannedUsers, String schemaName, String checkDate) throws ParseException {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside customerByuserDevice CustomerService.java");
	    	Query query = new Query();
	    	Query query1 = new Query();
	    	List<com.fidz.entr.app.reportcustomersdevice.Customer> uniqueList = null;

	    	Collection custType = new ArrayList();
	    	custType.add("REGISTERED");
	    	custType.add("PROSPECTIVE");
	    	custType.add("BOTH");
	    	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
   		    Date startDate = format.parse(checkDate);
	    	List<String> userid= plannedUsers.getUserId();
	    	List<String> cityIds = plannedUsers.getCityIds();
	    	Constant.LOGGER.info("plannedUsers.getUserId()----"+plannedUsers.getUserId().size());
	    	Constant.LOGGER.info("plannedUsers.getCityIds()----"+plannedUsers.getCityIds().size());
	    	Constant.LOGGER.info("deptId  deptId----"+plannedUsers.getDeptId());
	    	try {
	    		List<com.fidz.entr.app.reportcustomersdevice.Customer> customers =null;
		    	List<com.fidz.entr.app.reportcustomersdevice.Customer> customerss =null;
    			   query.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
    					   			(Criteria.where("city.id").in(cityIds).andOperator
    					   			(Criteria.where("customerType").in(type).andOperator(Criteria.where("createdTimestamp").gte(startDate)))));
//	    			query.addCriteria(Criteria.where("users.id").in(userid).andOperator
//	    					(Criteria.where("customerType").is(type)));
	    		
    			   query1.addCriteria(Criteria.where("department.id").in(plannedUsers.getDeptId()).andOperator
				   			(Criteria.where("city.id").in(cityIds).andOperator
				   			(Criteria.where("customerType").in(type).andOperator(Criteria.where("updatedTimestamp").gte(startDate)))));
	    		customers = this.genericINterface.find(query, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		   		
		    	Constant.LOGGER.info("Query"+query);
		    	
		    	List<com.fidz.entr.app.reportcustomersdevice.Customer> upd = new ArrayList<>();

	    		customerss = this.genericINterface.find(query1, com.fidz.entr.app.reportcustomersdevice.Customer.class);
	    		Constant.LOGGER.info("Query1"+query1);

	    		if(!customers.isEmpty()) {
	    			upd.addAll(customers);
	    			
		    		
		    		}
		    		if(!customerss.isEmpty()) {
		    	  

			    	upd.addAll(customerss);		    			

		    		}
		    		 uniqueList = customerss
		                    .stream() 
		                    .distinct() 
		                    .collect(Collectors.toList());
		    		
		    		
	    	}catch(Exception ex) {
	    		Constant.LOGGER.error("Error ouccured while fetching customerByuserDevice CustomerService.java");
	    		
	    	}
	    	
	    	
	    	return uniqueList;
		}
		public String convertDateToDepartmentTimezone(String deptTimezone){
			Constant.LOGGER.info("convertDateToDepartmentTimezone: deptTimezone "+deptTimezone);
	        String time = "NA";
	        Date dt = new Date();

	        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	        dateFormatter.setTimeZone(TimeZone.getTimeZone(deptTimezone));
	      //dateFormatter.setTimeZone(TimeZone.getTimeZone("Europe/Luxembourg"));
	        String strUTCDate = dateFormatter.format(dt);

	        System.out.println(strUTCDate);
	        //Log.i(TAG,"time----------"+String.valueOf(strUTCDate));
	        time = String.valueOf(strUTCDate);
	        return time;
	    }

		public List<com.fidz.entr.app.reportcustomerswebPlan.Customer> customerByuserWebSimplifyCustomerwithPagination(
				String deptid, String cityId, int pageNumber, int pageSize, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("Inside customerByuserWebSimplifyCustomerwithPagination CustomerService.java"+deptid +"cityId"+cityId);
	    	Query query = new Query();
	    	List<com.fidz.entr.app.reportcustomerswebPlan.Customer> customers=null;
	    	try {
	    		query.addCriteria(Criteria.where("department.id").in(deptid).andOperator
 	    			   (Criteria.where("city.id").is(cityId)));
	    		final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
	    	    query.with(pageableRequest);
	
	    	customers=this.genericINterface.find(query,com.fidz.entr.app.reportcustomerswebPlan.Customer.class);
	    	}catch(Exception ex) {
	    	Constant.LOGGER.error("Error while getting data from customerByuserWebSimplifyCustomerwithPagination CustomerService.java"+ex.getMessage());
	        }
	    	Constant.LOGGER.info("**************************************************************************");
	    	Constant.LOGGER.info("successfully fetched data from customerByuserWebSimplifyCustomerwithPagination CustomerService.java");
	    	return customers;
		}
		
		public List<com.fidz.entr.app.reportcustomerswebPlan.Customer> customerTextSearchBasedOnDepartmentandCity(@Valid String text,
				String schemaName,String city,String department,int pageNumber, int pageSize) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			List<com.fidz.entr.app.reportcustomerswebPlan.Customer>  cus = null;
//			TextIndexDefinition textIndex = null;
//			TextCriteria criteria = null;
//			Query query = null;
           Constant.LOGGER.info("Inside customerTextSearch CustomerService.java");
	    	try {
	    		try {
				    Query q = new Query();
				    q.addCriteria(Criteria.where("department.id").in(department).andOperator(Criteria.where("city.id").is(city).andOperator(Criteria.where("name").regex(text,"i"))));
				    //q.addCriteria(Criteria.where("name").regex(text,"i").andOperator("department.id").is(department).and("city.id").is(city));
				    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
		    	    q.with(pageableRequest);
				    cus = this.mongoTemplate.find(q, com.fidz.entr.app.reportcustomerswebPlan.Customer.class);
					Constant.LOGGER.info("Query "+q);

				}catch(Exception ex) {
					Constant.LOGGER.info("Error while fetching customer byname"+ex.getMessage());
				}
	    	}catch(Exception ex) {
	    	Constant.LOGGER.error("Inside customerTextSearch CustomerService.java"+ ex.getMessage());

		}
			return cus;
		}

}
