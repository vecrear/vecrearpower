package com.fidz.entr.app.awsconfig.service;

public class ApiCountLimitNotFoundException extends Exception{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3254501454484109855L;

	public ApiCountLimitNotFoundException(String name) {
		super(" not found with " + name);
	}

}
