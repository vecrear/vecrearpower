package com.fidz.entr.app.controller;

import java.io.IOException;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.app.service.ValidateService;
/**
 *  This controller is user to validate the tagged company, department, state, city, region, etc...
 *  and these API is used during while editing and deleting a row from admin page.
 * @author Ashok
 *
 */

@CrossOrigin(maxAge = 3600)
@RestController("FEBValidateController")
@RequestMapping(value="/validate")
public class ValidateController {
	
	@Autowired
	private ValidateService validateService;
	
	@PostMapping("/companytagged/id/{id}")
    public JSONObject validateCompanyTagged(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value = "id") String id){
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return validateService.validateCompanyTagged(schemaName,id);
    }
	
	@PostMapping("/devicetagged/id/{id}")
    public JSONObject validateDeviceTagged(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value = "id") String id){
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return validateService.validateDeviceTagged(schemaName,id);
    }
	
	@PostMapping("/departmenttagged/id/{id}")
    public JSONObject validateDepartmentTagged(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value = "id") String id){
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return validateService.validateDepartmentTagged(schemaName,id);
			}
	
	
	@PostMapping("/userroletagged/id/{id}")
    public JSONObject validateUserRoleTagged(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value = "id") String id){
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return validateService.validateUserRoleTagged(schemaName,id);
			}
	
	@PostMapping("/customercategorytagged/id/{id}")
    public JSONObject validateCustomerCategoryTagged(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value = "id") String id){
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return validateService.validateCustomerCategoryTagged(schemaName,id);
			}
	
}
