package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.app.reportactivities.Company;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class PlannedUsers {
	
	private List<String> userId;
	
	private String customerCategoryId;
	
	private String customerType;

	public PlannedUsers(List<String> userId, String customerCategoryId, String customerType) {
		super();
		this.userId = userId;
		this.customerCategoryId = customerCategoryId;
		this.customerType = customerType;
	}

	@Override
	public String toString() {
		return "PlannedUsers [userId=" + userId + ", customerCategoryId=" + customerCategoryId + ", customerType="
				+ customerType + "]";
	}

	public PlannedUsers() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	

}
