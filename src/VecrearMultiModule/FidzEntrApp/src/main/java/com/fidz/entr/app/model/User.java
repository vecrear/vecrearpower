package com.fidz.entr.app.model;
import java.util.Date;

import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Contact;

import com.fidz.base.model.PersonName;
import com.fidz.base.model.Role;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.TaggedCities;
import com.fidz.entr.base.model.TaggedDevices;
import com.fidz.entr.base.model.UserHierarchyLevel;

import lombok.Data;

@Data
@Document(collection = "user")
@JsonIgnoreProperties(value = { "target" })
public class User extends com.fidz.entr.base.model.User {
	
	@NotNull
	@DBRef(lazy = true)
	protected UserRole userRole;
	
	@DBRef(lazy = true)
	protected List<User> reportsTo;
	
	@DBRef(lazy = true)
	protected List<Department> additionalDepartments;
	
	@DBRef(lazy = true)
	protected Device device;
	
	@DBRef(lazy = true)
	protected Country country;
	
	@DBRef(lazy = true)
	protected Region region;
	
	@DBRef(lazy = true)
	protected State state;
	
	@DBRef(lazy = true)
	protected City city;
	
	@DBRef(lazy = true)
	protected List<City> citylist;
	//@JsonIgnore
	protected Map<String, Object> additionalFields;
	
	@NotNull
	protected int userRoleId;

	@NotNull
	protected String managerName;
	
	@NotNull
	protected String managerMobile;
	
	@NotNull
	protected String managerEmail;
	
	protected List<TaggedDevices> taggedDevices;
	

	@DBRef(lazy = true)
	protected com.fidz.entr.app.usermanagers.User userManager;
	protected boolean geofenceTagged = false;
	

	

	public User() {
		// TODO Auto-generated constructor stub
	}








	
	
	
	
	









	

	
	
}
