package com.fidz.entr.app.model;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.base.model.TaggedDevices;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class TaggedUsers {
	@Id
	protected String id;
	
	protected boolean userTagged ;

	@Override
	public String toString() {
		return "TaggedGeofences [id=" + id + ", userTagged=" + userTagged + "]";
	}

	public TaggedUsers(String id, boolean userTagged) {
		super();
		this.id = id;
		this.userTagged = userTagged;
	}

}
