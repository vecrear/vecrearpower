package com.fidz.entr.app.reportattendance;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.DeviceType;
import com.fidz.base.model.Status;
import com.fidz.base.model.Stream;
import com.fidz.entr.app.reportusers.DeviceOsType;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
@Document(collection = "device")
public class Device {

	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	
	@Id
	protected String id;
	@Indexed(unique = true)
	protected String deviceId;
	//@JsonIgnore
	//@DBRef(lazy = true)
	//protected DeviceType deviceType;
	protected DeviceOsType deviceType;
	@JsonIgnore
	protected Object schema;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Stream  stream;
	@JsonIgnore
	protected Object currentData;
	
	protected String imei;
	@JsonIgnore
	protected Object deviceHardwareDetails;
	@JsonIgnore
	protected Object company;
	
	protected boolean deviceTagged;
	
	protected String taggedUserName;
		
	
}
