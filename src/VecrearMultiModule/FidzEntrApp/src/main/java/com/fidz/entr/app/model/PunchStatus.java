package com.fidz.entr.app.model;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
@Document(collection = "PunchStatus")
public class PunchStatus {
	@Id
	protected String id;
	protected boolean ispunchIn;
	protected Date punchStartDateTime;
	protected String deptId;
	protected String username;
	protected String schemaName;

	public PunchStatus() {
		super();
		// TODO Auto-generated constructor stub
	}


	

	
	
	
	
	
}
