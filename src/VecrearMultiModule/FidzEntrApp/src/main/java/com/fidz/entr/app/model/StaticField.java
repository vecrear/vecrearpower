package com.fidz.entr.app.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.base.model.Visibility;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class StaticField {
	@NotNull
	protected String fieldName;
	protected int sortOrder;
	protected Visibility visibility;
	
	public StaticField(@NotNull String fieldName, int sortOrder, Visibility visibility) {
		super();
		this.fieldName = fieldName;
		this.sortOrder = sortOrder;
		this.visibility = visibility;
	}

	@Override
	public String toString() {
		return "StaticField [fieldName=" + fieldName + ", sortOrder=" + sortOrder + ", visibility=" + visibility + "]";
	}

}
