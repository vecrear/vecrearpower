package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.naming.NameNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Topic;
import com.fidz.entr.app.config.Constant;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.service.DeviceService;
import com.fidz.entr.base.model.Device;

@CrossOrigin(maxAge = 3600)
@RestController("FEADeviceController")
@Configuration
@PropertySource(value = "classpath:vecrearconfig.properties", ignoreResourceNotFound = true)
@RequestMapping(value = "/devices")
public class DeviceController {

	@Autowired
	private DeviceService deviceService;

	@Value("${deviceservices.enabled}")
	private Boolean property;

	@PostMapping("/get/all/status/{status}")
	public List<Device> getAllDeviceData(@RequestHeader(value = "schemaName") String schemaName,
			@PathVariable(value = "status") String status) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getAllDeviceData(schemaName, status);
	}

	// to get the mapped and unmapped Devices

	@PostMapping("/get/all/companyid/{companyid}/userbase/{tagged}/status/{status}")
	public List<Device> getAllTagUnTaggedDevices(@PathVariable(value = "companyid") String companyid,
			@PathVariable(value = "tagged") String variableMap, @RequestHeader(value = "schemaName") String schemaName,
			@PathVariable(value = "status") String status) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getAllTagUnTaggedDevices(companyid, variableMap, schemaName, status);
	}

	@PostMapping("/get/all/byusername/{username}/status/{status}")
	public List<Device> getAllDevicesByUserName(@PathVariable(value = "username") String username,
			@RequestHeader(value = "schemaName") String schemaName, @PathVariable(value = "status") String status) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getAllDevicesByUserName(username, schemaName, status);
	}

	@PostMapping("/get/all/{companyid}/status/{status}")
	public List<Device> getAllDeviceDataByComapany(@PathVariable(value = "companyid") String companyId,
			@RequestHeader(value = "schemaName") String schemaName, @PathVariable(value = "status") String status) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getAllDeviceDataByComapany(companyId, schemaName, status);
	}

	// Post request to get all devices paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}/status/{status}")
	public List<Device> getAllDeviceDataPaginated(@PathVariable(value = "pageNumber") int pageNumber,
			@PathVariable(value = "pageSize") int pageSize, @PathVariable(value = "status") String status,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getAllDeviceDataPaginated(pageNumber, pageSize, status, schemaName);
	}

	@PostMapping
	public Device createDevice(@Valid @RequestBody Device device) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.createDevice(device);
	}

	@PostMapping("/creates")
	public void createDevices(@Valid @RequestBody Device device) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		deviceService.createDevices(device);
	}

	@PostMapping("/get/id/{id}")
	public Device getDeviceDataById(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getDeviceDataById(id, schemaName);
	}

	@PostMapping("/update/id/{id}")
	public Device updateDeviceData(@PathVariable(value = "id") String id, @Valid @RequestBody Device device) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.updateDeviceData(id, device);

	}

	@PostMapping("/update/makeactive/id/{id}")
	public Device updateUserMakeActive(@PathVariable(value = "id") String id, @Valid @RequestBody Device device)
			throws NameNotFoundException {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.updateDeviceMakeActive(id, device);
	}

	@PostMapping("/get/id/deviceid/{deviceid}")
	public Device getDeviceDataByDeviceId(@PathVariable(value = "deviceid") final String deviceid,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getDeviceDataByDeviceId(deviceid, schemaName);
	}

	// get topic name
	@PostMapping("/get/id/{id}/topic")
	public Topic getTopicById(@PathVariable(value = "id") final String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getTopicById(id, schemaName);
	}

	// get topic name based on device id
	@PostMapping("/get/id/deviceid/{deviceid}/topic")
	public Topic getTopicByDeviceId(@PathVariable(value = "deviceid") final String deviceid,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getTopicByDeviceId(deviceid, schemaName);
	}

	// get devices by deviceTypeId
//  	@PostMapping("/get/type/devicetype/{id}")
//      public Stream<Device> getDevicesByDeviceTypeId(@PathVariable(value = "id") final String id, @RequestHeader(value="schemaName") String schemaName) {
//  		System.out.println("property value******"+property);
//		String message="Deviceservices are disabled";
//		if(!property) 
//		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//  		return deviceService.getDevicesByDeviceTypeId(id,schemaName);
//      }

	// get devices by deviceType
	@PostMapping("/devicetype/name/{name}")
	public Stream<Device> getDevicesByDeviceType(@PathVariable(value = "name") final String name,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getDevicesByDeviceType(name, schemaName);
	}

	// update the current device data
	@PatchMapping("/update/{id}/currentdata")
	public Device updateCurrentData(@PathVariable(value = "id") String id, @Valid @RequestBody Object currentData,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.updateCurrentData(id, currentData, schemaName);

	}

	// update the current device data
	@PatchMapping("/patch/{deviceid}/devicehardwaredata")
	public Device updateCurrentDataUsingDeviceId(@PathVariable(value = "deviceid") String deviceid,
			@Valid @RequestBody Object deviceHardwareDetails, @RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.updateCurrentDataDeviceId(deviceid, deviceHardwareDetails, schemaName);

	}

	@PostMapping("/get/id/{id}/currentdata")
	public Object getCurrentDataById(@PathVariable(value = "id") final String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.getCurrentDataById(id, schemaName);
	}

	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteDevice(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.deleteDevice(id, schemaName);
	}

	@PostMapping("/delete/soft/id/{id}")
	public Map<String, String> softDeleteDevice(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.deleteSoftCompany(id, schemaName, timeUpdate);
	}

	@PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public List<Device> streamAllDevices() {
		System.out.println("property value******" + property);
		String message = "Deviceservices are disabled";
		if (!property)
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.streamAllDevices();
	}

	@PostMapping("/search/text/{text}/{pageNumber}/{pageSize}/status/{status}")
	public List<Device> deviceTextSearch(@Valid @PathVariable(value = "text") String text,
			@PathVariable(value = "pageNumber") int pageNumber, @PathVariable(value = "pageSize") int pageSize,
			@RequestHeader(value = "schemaName") String schemaName, @PathVariable(value = "status") String status) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return deviceService.deviceTextSearch(text, schemaName, pageNumber, pageSize, status);
	}

	// Exception Handling Examples
	/*
	 * @ExceptionHandler public ResponseEntity<ErrorResponse>
	 * handleDuplicateKeyException(DuplicateKeyException ex) { return
	 * ResponseEntity.status(HttpStatus.CONFLICT).body(new
	 * ErrorResponse("A Device with the same details already exists")); }
	 * 
	 * @ExceptionHandler public ResponseEntity<?>
	 * handleNotFoundException(DeviceNotFoundException ex) { return
	 * ResponseEntity.notFound().build(); }
	 */
}
