package com.fidz.entr.app.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
@Data
@Document(collection = "feedback")
@JsonIgnoreProperties(value = { "target" })
public class FeedBack {
	@Id
	protected String id;
	
	protected String userName;
	
	protected String schemaName;
	
	protected Date feedbackDateTime;
	
	protected String message;

	public FeedBack(String id, String userName, String schemaName, Date feedbackDateTime, String message) {
		super();
		this.id = id;
		this.userName = userName;
		this.schemaName = schemaName;
		this.feedbackDateTime = feedbackDateTime;
		this.message = message;
	}

	@Override
	public String toString() {
		return "FeedBack [id=" + id + ", userName=" + userName + ", schemaName=" + schemaName + ", feedbackDateTime="
				+ feedbackDateTime + ", message=" + message + "]";
	}
	
	
	

}
