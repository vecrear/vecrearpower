package com.fidz.entr.app.controller;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.service.ActivityFieldService;
import com.fidz.entr.base.model.UserRole;

@CrossOrigin(maxAge = 3600)
@RestController("FEAActivityFieldController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/activityfields")
public class ActivityFieldController {
	@Autowired
	private ActivityFieldService  activityFieldService;
	
	@Value("${activityservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all")
	public List<ActivityField> getAllActivityFields(@RequestHeader(value="schemaName") String schemaName) throws UnknownHostException{
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.getAllActivityFields(schemaName);
	}
	
	//Post request to get all ActivityFields  paginated
	@PostMapping("/get/all/web/{pageNumber}/{pageSize}")
	public List<ActivityField> getAllActivityFieldsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.getAllActivityFieldsPaginated(pageNumber, pageSize, schemaName);
	}
	
	@PostMapping("search/{text}/get/all/web/{pageNumber}/{pageSize}")
	public List<ActivityField> getAllSearchActivityFieldsPaginated(@PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.getAllSearchActivityFieldsPaginated(text,pageNumber, pageSize, schemaName);
	}
	
	
	
	@PostMapping("/status/activityfieldid/{activityfieldid}")
	public Map<String, String>  activityfieldidMappedOrNot(@PathVariable(value="activityfieldid") String activityfieldid,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="activityfield are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.activityfieldidMappedOrNot(activityfieldid,schemaName);
	}
	
	@PostMapping
	public ActivityField createActivityField(@Valid @RequestBody ActivityField activity){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.createActivityField(activity);
	}
	@PostMapping("/creates")
	public void createActivityFields(@Valid @RequestBody ActivityField activity){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		activityFieldService.createActivityFields(activity);
	}
	@PostMapping("/get/id/{id}")
	public ResponseEntity<ActivityField> getActivityFieldById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.getActivityFieldById(id, schemaName);
		
	}
	/*@GetMapping("/activityname/{name}")
    public Mono<Activity> getActivityByName(@PathVariable(value = "name") String name) {
		return activityService.getActivityByName(name);
    }
	*/
	@PostMapping("/update/id/{id}")
    public ActivityField updateActivityField(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody ActivityField activity) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.updateActivityField(id, activity);
    }


	@PostMapping("/delete/id/{id}")
    public Map<String, String> deleteActivityField(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.deleteActivityField(id, schemaName);
    }
	
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteActivityField(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.deleteSoftActivityField(id, schemaName, timeUpdate);
    }
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<ActivityField> streamAllActivities() {
    	System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.streamAllActivityFields();
    }
    
    @PostMapping("get/all/deptid/{deptid}")
    public List<ActivityField> getAllActivityFieldsByDepartment(@PathVariable(value = "deptid") String deptid, @RequestHeader(value="schemaName") String schemaName) {
	
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFieldService.getAllActivityFieldsByDepartment(deptid, schemaName);
	}	

    // Exception Handling Examples
   /* @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Activity Field with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ActivityFieldNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/
	
   
	
}
