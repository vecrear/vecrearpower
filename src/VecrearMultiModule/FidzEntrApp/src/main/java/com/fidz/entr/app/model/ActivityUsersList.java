package com.fidz.entr.app.model;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
@Data
@JsonIgnoreProperties(value = { "target" })
public class ActivityUsersList {
	@Id
	protected String id;
	
	protected String activityName;

}
