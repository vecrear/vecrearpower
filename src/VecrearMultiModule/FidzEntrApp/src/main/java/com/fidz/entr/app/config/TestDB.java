package com.fidz.entr.app.config;

import java.util.List;

import javax.validation.constraints.NotBlank;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fidz.base.model.Base;
import com.fidz.base.model.Group;


import lombok.Data;

@Data
@Document(collection = "mydb")
public class TestDB extends Base{

	@Id
	protected String id;
	
	@NotBlank
	protected String name;
	
	@DBRef(lazy = true)
	protected List<Group> groups;



	
	
	
}
