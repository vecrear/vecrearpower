package com.fidz.entr.app.model;



import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "devicedetails")
@JsonIgnoreProperties(value = { "target" })
public class DeviceDetails extends Base{

	@Id
	protected String id;
	
	@NotBlank
	
	protected String deviceId;
	private String myDB;
	protected Object deviceData;
	public DeviceDetails(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, String deviceId, String myDB, Object deviceData) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.deviceId = deviceId;
		this.myDB = myDB;
		this.deviceData = deviceData;
	}
	@Override
	public String toString() {
		return "DeviceDetails [id=" + id + ", deviceId=" + deviceId + ", myDB=" + myDB + ", deviceData=" + deviceData
				+ ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp="
				+ updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp
				+ ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}
	
		
	
}
