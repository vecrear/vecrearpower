package com.fidz.entr.app.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.app.service.UserRoleService;
import com.fidz.entr.base.exception.UserRoleNotFoundException;


@CrossOrigin(maxAge = 3600)
@RestController("FEBUserRoleController")
@RequestMapping(value="/userroles")
public class UserRoleController {
	@Autowired
	private UserRoleService userRoleService;
	
	@PostMapping("/get/all")
    public List<UserRole> getAllUserRoles(@RequestHeader(value="schemaName") String schemaName) throws IOException {
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.getAllUserRoles(schemaName);
    }
	
	//Post request to get all UserRoles  paginated
	@PostMapping("/get/all/web/{pageNumber}/{pageSize}")
	public List<UserRole> getAllUserRolesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.getAllUserRolesPaginated(pageNumber, pageSize, schemaName);
	}
	
	@PostMapping("/search/text/{text}/{pageNumber}/{pageSize}")
	public List<UserRole> getAllUserRolesPaginatedSearch(@Valid @PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.getAllUserRolesPaginatedSearch(pageNumber, pageSize, schemaName,text);
	}
	
	
	
	
	@PostMapping("get/all/deptid/{deptid}")
    public List<UserRole> getAllUserRolesByDepartment(@PathVariable(value = "deptid") String deptid, @RequestHeader(value="schemaName") String schemaName) {
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.getAllUserRolesByDepartment(deptid, schemaName);
	}	
	
    @PostMapping
    public UserRole createUserRole(@Valid @RequestBody UserRole userRole) {
    	 final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return userRoleService.createUserRole(userRole);
    }
    
    @PostMapping("/creates")
    public void createUserRoles(@Valid @RequestBody UserRole userRole) {
    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			userRoleService.createUserRoles(userRole);
    }
    @PostMapping("/get/id/{id}")
    public UserRole getUserRoleById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.getUserRoleById(id, schemaName);
    }
	
    @PostMapping("/get/name/userrole/{name}")
    public UserRole getUserRoleByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.getUserRoleByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public UserRole updateUserRole(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody UserRole userRole) {
    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.updateUserRole(id, userRole);
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteUserRole(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.deleteUserRole(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteUserRole(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.deleteSoftUserRole(id, schemaName, timeUpdate);
    }
    
    
    
    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<UserRole> streamAllUserRoles() {
    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userRoleService.streamAllUserRoles();
    }
   

    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A UserRole with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(UserRoleNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
