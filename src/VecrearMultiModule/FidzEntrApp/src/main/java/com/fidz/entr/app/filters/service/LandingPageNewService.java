package com.fidz.entr.app.filters.service;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.controller.UserNameAppender;
import com.fidz.entr.app.filters.model.ActivityMap;
import com.fidz.entr.app.filters.model.LandingPageMap;
import com.fidz.entr.app.filters.model.LandingPageNew;
import com.fidz.entr.app.filters.model.RequestType;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.service.UserService;

@Service
public class LandingPageNewService {

    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    UserService userService;
    @Autowired
    PercentageCalculatorService percentage;
   
    @SuppressWarnings("unchecked")
    public JSONObject getlandingPage(String schemaName, LandingPageNew pagenew) throws ParseException {
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        Constant.LOGGER.info("Inside  getlandingPage Search LandinPageNewService.java");
        String userid =pagenew.getUserId().getId();
   
       
       
        Constant.LOGGER.info("Userssss"+userid);
        StopWatch watch = new StopWatch();
       
        watch.start();
      
        Collection<String> type = new ArrayList<>();
        type.add("OPEN");
        type.add("COMPLETED");
        type.add("POSTPONED");
        List<com.fidz.entr.app.reportusers.User> obj =null;
        User use =null;
        JSONObject jsons = new JSONObject();
       
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Constant.LOGGER.info("startdate"+pagenew.getStartDate());
        Date startDate = format.parse(pagenew.getStartDate()+" "+"00:00");
        Date endDate =format.parse(pagenew.getEndDate()+" "+"23:59");
    
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusHours(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    
     
        use = this.userService.getUserById(userid, schemaName);
        if(use.getUserRoleId()>=3 && use.getUserRoleId()<=6) {
        obj = this.userService.getAllUsersRoles(schemaName, userid,"ACTIVE");
       
        Constant.LOGGER.info("######startdate"+startDate);
        Constant.LOGGER.info("######enddate"+currentDatePlusOneDay);
       
        String id = use.getId();
        List<String> userids = obj.stream().map(user-> user.getId()).collect(Collectors.toList());
        int noOfElements =userids.size();
        userids.add(id);
        if(pagenew.getActivityType().equals(RequestType.ALL) && (pagenew.getHierarchy().equals(RequestType.ALL))) {
            Constant.LOGGER.info("inside if condition where request type activityType ALL and Hierarchy is ALL");
            List<Map<String,Object>> jsonObjects = new ArrayList<Map<String,Object>>();
                Map<String,Object> chart1 = new HashMap<String,Object>();  

// Chart 1 Query             Criteria.where("activityStatus").in(type)
            Aggregation agg1 = newAggregation(
                     match((Criteria.where("activityStatus").in(type).andOperator(
                             Criteria.where("resource.id").in(userids),
                                  Criteria.where("actualStartDateTime").gte(startDate),
                                  Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                 )),
                         group("activityStatus").count().as("totalcount"),
                         project("totalcount").and("activityStatus").previousOperation(),
                         sort(Sort.Direction.DESC,"totalcount")
                         );
             
              AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
              List<ActivityMap> result = groupResults.getMappedResults();
             
              Constant.LOGGER.info("Aggregate result"+result);
              watch.stop();
         Stream<String> nos =result.stream().map(p->p.getTotalcount());
         int s =nos.collect(Collectors.summingInt(t->Integer.parseInt(t)));
         chart1.put("activity", result);
         chart1.put("Total value", s);
         chart1.put("NoOfUsers",noOfElements);
         jsons.put("chart1", chart1);
       

//Chart 2         
         obj = this.userService.getAllUsersbaseRoles(schemaName, userid,"ACTIVE");
         obj.forEach(user-> {
             Map<String,Object> chart2 = new HashMap<String,Object>();  
          List<com.fidz.entr.app.reportusers.User> obj2 = this.userService.getAllUsersRoles(schemaName, user.getId(),"ACTIVE");
          obj2.add(user);
          List<String> userids2 = obj2.stream().map(users-> users.getId()).collect(Collectors.toList());
         Constant.LOGGER.info(""+userids2);
          Aggregation agg2 = newAggregation(
                     match((Criteria.where("resource.id").in(userids2).andOperator(
                             Criteria.where("activityStatus").in(type),
                                  Criteria.where("actualStartDateTime").gte(startDate),
                                  Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                 )),
                         group("activityStatus").count().as("totalcount"),
                         project("totalcount").and("activityStatus").previousOperation(),
                         sort(Sort.Direction.DESC,"totalcount")
                         );
             
              AggregationResults<LandingPageMap> groupResults2 = mongoTemplate.aggregate(agg2,Activity.class,LandingPageMap.class);
              List<LandingPageMap> result2 = groupResults2.getMappedResults();
              double d =0;
              double d2=0;
              double d3=0;
           if(result2.size()<=1) {
            d3 = 0;
           }else if(result2.size()>1) {
               d  = Double.valueOf(result2.get(1).getTotalcount());
               d2 = Double.valueOf(result2.get(0).getTotalcount());
             
               d3 = this.percentage.calculatePercentage(d, d2);
           }
     
           String username = UserNameAppender.userNameAppend(user.getName().getFirstName(), user.getName().getMiddleName(), user.getName().getLastName());
          chart2.put("userName", username);
          Constant.LOGGER.info("USerName"+username);

          chart2.put("UserRole", user.getUserRole().getName());
          chart2.put("hierarchyWise", result2);
          chart2.put("percentage", d3);
          jsonObjects.add(chart2);

         
          });

         jsons.put("chart2",jsonObjects);
        Constant.LOGGER.info("jsonObjects"+jsonObjects);
    //Chart 3
        List<Map<String,Object>> jsonObjects1 = new ArrayList<Map<String,Object>>();
         Collection<String> cl = new ArrayList<String>();
         cl.add("OPEN");
         cl.add("COMPLETED");
         cl.add("CANCELLED");
         cl.add("POSTPONED");
        
        
         obj = this.userService.getAllUsersbaseRoles(schemaName, userid,"ACTIVE");

         obj.forEach(u-> {
             Map<String,Object> chart3 = new HashMap<String,Object>();

              List<com.fidz.entr.app.reportusers.User> obj3 = this.userService.getAllUsersRoles(schemaName, u.getId(),"ACTIVE");
               int elements = obj3.size();
              obj3.add(u);
              List<String> userids3 = obj3.stream().map(users-> users.getId()).collect(Collectors.toList());
          
              Aggregation     agg3 = newAggregation(
                         match((Criteria.where("resource.id").in(userids3).andOperator(
                                 Criteria.where("activityStatus").in(cl),
                                      Criteria.where("actualStartDateTime").gte(startDate),
                                      Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                     )),
                             group("activityStatus").count().as("totalcount"),
                             project("totalcount").and("activityStatus").previousOperation(),
                             sort(Sort.Direction.DESC,"totalcount")
                             );
              AggregationResults<ActivityMap> groupResults4 = mongoTemplate.aggregate(agg3,Activity.class,ActivityMap.class);
              List<ActivityMap> result4 = groupResults4.getMappedResults();
               String username = UserNameAppender.userNameAppend(u.getName().getFirstName(), u.getName().getMiddleName(), u.getName().getLastName());
 
                 chart3.put("activity", result4);
                 chart3.put("userName",username);
                 chart3.put("userRole",u.getUserRole().getName());
                 chart3.put("noOfUsers", elements);
                 jsonObjects1.add(chart3);

         });
         jsons.put("chart3",jsonObjects1);

        }
       
        if(pagenew.getActivityType().equals(RequestType.BYID) && (pagenew.getHierarchy().equals(RequestType.ALL))) {
            Constant.LOGGER.info("inside if condition where request type activityType BYID and Hierarchy is ALL");
            List<Map<String,Object>> jsonObjects = new ArrayList<Map<String,Object>>();
               Map<String,Object> chart1 = new HashMap<String,Object>();  

            String activityid= pagenew.getActId().getId();
             
             
            Aggregation agg1 = newAggregation(
                     match((Criteria.where("activityStatus").in(type).andOperator(
                             Criteria.where("resource.id").in(userids),
                             Criteria.where("activityType.id").is(activityid),
                                  Criteria.where("actualStartDateTime").gte(startDate),
                                  Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                 )),
                         group("activityStatus").count().as("totalcount"),
                         project("totalcount").and("activityStatus").previousOperation(),
                         sort(Sort.Direction.DESC,"totalcount")
                         );
             
              AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
              List<ActivityMap> result = groupResults.getMappedResults();
             
              Constant.LOGGER.info("Aggregate result"+result);
              Stream<String> nos =result.stream().map(p->p.getTotalcount());
                 int s =nos.collect(Collectors.summingInt(t->Integer.parseInt(t)));
                 chart1.put("activity", result);
                 chart1.put("Total value", s);
                 chart1.put("NoOfUsers",noOfElements);
                 jsons.put("chart1", chart1);
       
                //Chart 2         
                 obj = this.userService.getAllUsersbaseRoles(schemaName, userid,"ACTIVE");
                 obj.forEach(user-> {
                  
                     Map<String,Object> chart2 = new HashMap<String,Object>();  
 
                  List<com.fidz.entr.app.reportusers.User> obj2 = this.userService.getAllUsersRoles(schemaName, user.getId(),"ACTIVE");
                  obj2.add(user);
                  List<String> userids2 = obj2.stream().map(users-> users.getId()).collect(Collectors.toList());

                  Aggregation agg2 = newAggregation(
                             match((Criteria.where("resource.id").in(userids2).andOperator(
                                     Criteria.where("activityStatus").in(type),
                                     Criteria.where("activityType.id").is(activityid),
                                          Criteria.where("actualStartDateTime").gte(startDate),
                                          Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                         )),
                                 group("activityStatus").count().as("totalcount"),
                                 project("totalcount").and("activityStatus").previousOperation(),
                                 sort(Sort.Direction.DESC,"totalcount")
                                 );
                     
                      AggregationResults<LandingPageMap> groupResults2 = mongoTemplate.aggregate(agg2,Activity.class,LandingPageMap.class);
                      List<LandingPageMap> result2 = groupResults2.getMappedResults();
                     
                      double d =0;
                      double d2=0;
                      double d3=0;
                   if(result2.size()<=1) {
                    d3 = 0;
                   }else if(result2.size()>1) {
                       d  = Double.valueOf(result2.get(1).getTotalcount());
                       d2 = Double.valueOf(result2.get(0).getTotalcount());
                     
                       d3 = this.percentage.calculatePercentage(d, d2);
                   }
                     
                  String username = UserNameAppender.userNameAppend(user.getName().getFirstName(), user.getName().getMiddleName(), user.getName().getLastName());
                  chart2.put("userName", username); 
                  chart2.put("UserRole", user.getUserRole().getName());
                  chart2.put("hierarchyWise", result2);
                  chart2.put("percentage", d3);
                  jsonObjects.add(chart2);

                  });
                
                  jsons.put("chart2", jsonObjects);

               //Chart 3
                 Collection<String> cl = new ArrayList<String>();
                 cl.add("OPEN");
                 cl.add("COMPLETED");
                 cl.add("CANCELLED");
                 cl.add("POSTPONED");
                
                 List<Map<String,Object>> jsonObjects1 = new ArrayList<Map<String,Object>>();

                 obj = this.userService.getAllUsersbaseRoles(schemaName, userid,"ACTIVE");
                 obj.forEach(u-> {
                     Map<String,Object> chart3 = new HashMap<String,Object>();

                      List<com.fidz.entr.app.reportusers.User> obj3 = this.userService.getAllUsersRoles(schemaName, u.getId(),"ACTIVE");
                      int elements = obj3.size();
                      obj3.add(u);
                      List<String> userids3 = obj3.stream().map(users-> users.getId()).collect(Collectors.toList());
                    
                      Aggregation     agg3 = newAggregation(
                                 match((Criteria.where("resource.id").in(userids3).andOperator(
                                         Criteria.where("activityStatus").in(cl),
                                         Criteria.where("activityType.id").is(activityid),
                                         Criteria.where("actualStartDateTime").gte(startDate),
                                              Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                             )),
                                     group("activityStatus").count().as("totalcount"),
                                     project("totalcount").and("activityStatus").previousOperation(),
                                     sort(Sort.Direction.DESC,"totalcount")
                                     );
                      AggregationResults<ActivityMap> groupResults4 = mongoTemplate.aggregate(agg3,Activity.class,ActivityMap.class);
                      List<ActivityMap> result4 = groupResults4.getMappedResults();
                       String username = UserNameAppender.userNameAppend(u.getName().getFirstName(), u.getName().getMiddleName(), u.getName().getLastName());

                         chart3.put("activity", result4);
                         chart3.put("userName",username);
                         chart3.put("userRole",u.getUserRole().getName());
                         chart3.put("noOfUsers", elements);
                          jsonObjects1.add(chart3);

                 });
                 jsons.put("chart3",jsonObjects1);

        }      
                 if(pagenew.getActivityType().equals(RequestType.ALL) && (pagenew.getHierarchy().equals(RequestType.BYID))) {
                        Constant.LOGGER.info("inside if condition where request type activityType ALL and Hierarchy is BYID");
                        List<Map<String,Object>> jsonObjects = new ArrayList<Map<String,Object>>();
                           Map<String,Object> chart1 = new HashMap<String,Object>();  

                     String hierarchyid =pagenew.getHierarchyId().getId();
                     Constant.LOGGER.info("HierarchyId"+hierarchyid);
                     obj = this.userService.getAllUsersRoles(schemaName, hierarchyid,"ACTIVE");
                     List<String> hierarchyids = obj.stream().map(user-> user.getId()).collect(Collectors.toList());
                     Constant.LOGGER.info("List HierarchyIds"+hierarchyids);  
                     int noOfElements2 =hierarchyids.size();
                     Constant.LOGGER.info("List noOfElements2"+noOfElements2);  

                       
                        Aggregation agg1 = newAggregation(
                                 match((Criteria.where("activityStatus").in(type).andOperator(
                                         Criteria.where("resource.id").in(hierarchyids),
                                              Criteria.where("actualStartDateTime").gte(startDate),
                                              Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                             )),
                                     group("activityStatus").count().as("totalcount"),
                                     project("totalcount").and("activityStatus").previousOperation(),
                                     sort(Sort.Direction.DESC,"totalcount")
                                     );
                       
                        AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
                          List<ActivityMap> result = groupResults.getMappedResults();
                        Constant.LOGGER.info("Aggregate result"+result);
                          Stream<String> nos =result.stream().map(p->p.getTotalcount());
                             int s =nos.collect(Collectors.summingInt(t->Integer.parseInt(t)));
                             
                             chart1.put("activity", result);
                             chart1.put("Total value", s);
                             chart1.put("NoOfUsers",noOfElements2);
                             jsons.put("chart1", chart1);
       
                                //Chart 2         
                             obj = this.userService.getAllUsersbaseRoles(schemaName, hierarchyid,"ACTIVE");
                             obj.forEach(user-> {
                              
                                 Map<String,Object> chart2 = new HashMap<String,Object>();  

                              List<com.fidz.entr.app.reportusers.User> obj2 = this.userService.getAllUsersRoles(schemaName, user.getId(),"ACTIVE");
                              obj2.add(user);
                              List<String> userids2 = obj2.stream().map(users-> users.getId()).collect(Collectors.toList());

                              Aggregation agg2 = newAggregation(
                                         match((Criteria.where("resource.id").in(userids2).andOperator(
                                                 Criteria.where("activityStatus").in(type),
                                                 Criteria.where("actualStartDateTime").gte(startDate),
                                                      Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                                     )),
                                             group("activityStatus").count().as("totalcount"),
                                             project("totalcount").and("activityStatus").previousOperation(),
                                             sort(Sort.Direction.DESC,"totalcount")
                                             );
                                 
                                  AggregationResults<LandingPageMap> groupResults2 = mongoTemplate.aggregate(agg2,Activity.class,LandingPageMap.class);
                                  List<LandingPageMap> result2 = groupResults2.getMappedResults();
                                 
                                  double d =0;
                                  double d2=0;
                                  double d3=0;
                               if(result2.size()<=1) {
                                d3 = 0;
                               }else if(result2.size()>1) {
                                   d  = Double.valueOf(result2.get(1).getTotalcount());
                                   d2 = Double.valueOf(result2.get(0).getTotalcount());
                                 
                                   d3 = this.percentage.calculatePercentage(d, d2);
                               }
                                 
                                  String username = UserNameAppender.userNameAppend(user.getName().getFirstName(), user.getName().getMiddleName(), user.getName().getLastName());
 
                              chart2.put("userName", username);
                              chart2.put("UserRole", user.getUserRole().getName());
                              chart2.put("hierarchyWise", result2);
                              chart2.put("percentage", d3);
                              jsonObjects.add(chart2);

                              });
                            
                             jsons.put("chart2", jsonObjects);     
                             
                             
                             //Chart 3
                             Collection<String> cl = new ArrayList<String>();
                             cl.add("OPEN");
                             cl.add("COMPLETED");
                             cl.add("CANCELLED");
                             cl.add("POSTPONED");
                             List<Map<String,Object>> jsonObjects1 = new ArrayList<Map<String,Object>>();
 
                            
                             obj = this.userService.getAllUsersbaseRoles(schemaName, hierarchyid,"ACTIVE");
                             obj.forEach(u-> {
                                 Map<String,Object> chart3 = new HashMap<String,Object>();

                                  List<com.fidz.entr.app.reportusers.User> obj3 = this.userService.getAllUsersRoles(schemaName, u.getId(),"ACTIVE");
                                  int elements = obj3.size();
                                  obj3.add(u);
                                  List<String> userids3 = obj3.stream().map(users-> users.getId()).collect(Collectors.toList());
                                 
                                  Aggregation     agg3 = newAggregation(
                                             match((Criteria.where("resource.id").in(userids3).andOperator(
                                                     Criteria.where("activityStatus").in(cl),
                                                     Criteria.where("actualStartDateTime").gte(startDate),
                                                          Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                                         )),
                                                 group("activityStatus").count().as("totalcount"),
                                                 project("totalcount").and("activityStatus").previousOperation(),
                                                 sort(Sort.Direction.DESC,"totalcount")
                                                 );
                                  AggregationResults<ActivityMap> groupResults4 = mongoTemplate.aggregate(agg3,Activity.class,ActivityMap.class);
                                  List<ActivityMap> result4 = groupResults4.getMappedResults();
                                   String username = UserNameAppender.userNameAppend(u.getName().getFirstName(), u.getName().getMiddleName(), u.getName().getLastName());
 
                                     chart3.put("activity", result4);
                                     chart3.put("userName",username);
                                     chart3.put("userRole",u.getUserRole().getName());
                                     chart3.put("noOfUsers", elements);
                                      jsonObjects1.add(chart3);

                             });
                             jsons.put("chart3",jsonObjects1);
                       
                 }
                    if(pagenew.getActivityType().equals(RequestType.BYID) && (pagenew.getHierarchy().equals(RequestType.BYID))) {
                        Constant.LOGGER.info("inside if condition where request type activityType BYID and Hierarchy is BYID");
                        String activityid= pagenew.getActId().getId();
                        String hierarchyid =pagenew.getHierarchyId().getId();
                        List<Map<String,Object>> jsonObjects = new ArrayList<Map<String,Object>>();
                           Map<String,Object> chart1 = new HashMap<String,Object>();  
                         obj = this.userService.getAllUsersRoles(schemaName, hierarchyid,"ACTIVE");
                         List<String> hierarchyids = obj.stream().map(user-> user.getId()).collect(Collectors.toList());
                         int noOfElements3 =hierarchyids.size();
                           
                        Aggregation agg1 = newAggregation(
                                 match((Criteria.where("activityStatus").in(type).andOperator(
                                         Criteria.where("resource.id").in(hierarchyids),
                                         Criteria.where("activityType.id").is(activityid),
                                              Criteria.where("actualStartDateTime").gte(startDate),
                                              Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                             )),
                                     group("activityStatus").count().as("totalcount"),
                                     project("totalcount").and("activityStatus").previousOperation(),
                                     sort(Sort.Direction.DESC,"totalcount")
                                     );
                         
                          AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
                          List<ActivityMap> result = groupResults.getMappedResults();
                         
                          Constant.LOGGER.info("Aggregate result"+result);
                          Stream<String> nos =result.stream().map(p->p.getTotalcount());
                             int s =nos.collect(Collectors.summingInt(t->Integer.parseInt(t)));
                             chart1.put("activity", result);
                             chart1.put("Total value", s);
                             chart1.put("NoOfUsers",noOfElements3);
                             jsons.put("chart1", chart1);
                             
                             
                             
                            //Chart 2         
                             obj = this.userService.getAllUsersbaseRoles(schemaName, hierarchyid,"ACTIVE");
                             obj.forEach(user-> {
                              
                                 Map<String,Object> chart2 = new HashMap<String,Object>();  

                              List<com.fidz.entr.app.reportusers.User> obj2 = this.userService.getAllUsersRoles(schemaName, user.getId(),"ACTIVE");
                              obj2.add(user);
                              List<String> userids2 = obj2.stream().map(users-> users.getId()).collect(Collectors.toList());

                              Aggregation agg2 = newAggregation(
                                         match((Criteria.where("resource.id").in(userids2).andOperator(
                                                 Criteria.where("activityStatus").in(type),
                                                 Criteria.where("activityType.id").is(activityid),
                                                      Criteria.where("actualStartDateTime").gte(startDate),
                                                      Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                                     )),
                                             group("activityStatus").count().as("totalcount"),
                                             project("totalcount").and("activityStatus").previousOperation(),
                                             sort(Sort.Direction.DESC,"totalcount")
                                             );
                                 
                                  AggregationResults<LandingPageMap> groupResults2 = mongoTemplate.aggregate(agg2,Activity.class,LandingPageMap.class);
                                  List<LandingPageMap> result2 = groupResults2.getMappedResults();
                                 
                                  double d =0;
                                  double d2=0;
                                  double d3=0;
                               if(result2.size()<=1) {
                                d3 = 0;
                               }else if(result2.size()>1) {
                                   d  = Double.valueOf(result2.get(1).getTotalcount());
                                   d2 = Double.valueOf(result2.get(0).getTotalcount());
                                 
                                   d3 = this.percentage.calculatePercentage(d, d2);
                               }
                               String username = UserNameAppender.userNameAppend(user.getName().getFirstName(), user.getName().getMiddleName(), user.getName().getLastName());
 
                              chart2.put("userName", username); 
                              chart2.put("UserRole", user.getUserRole().getName());
                              chart2.put("hierarchyWise", result2);
                              chart2.put("percentage", d3);
                              jsonObjects.add(chart2);

                              });
                            
                              jsons.put("chart2", jsonObjects);
                              //Chart 3
                                 Collection<String> cl = new ArrayList<String>();
                                 cl.add("OPEN");
                                 cl.add("COMPLETED");
                                 cl.add("CANCELLED");
                                 cl.add("POSTPONED");
                                
                                 List<Map<String,Object>> jsonObjects1 = new ArrayList<Map<String,Object>>();

                                 obj = this.userService.getAllUsersbaseRoles(schemaName, hierarchyid,"ACTIVE");
                                 obj.forEach(u-> {
                                     Map<String,Object> chart3 = new HashMap<String,Object>();

                                      List<com.fidz.entr.app.reportusers.User> obj3 = this.userService.getAllUsersRoles(schemaName, u.getId(),"ACTIVE");
                                      int elements = obj3.size();
                                      obj3.add(u);
                                      List<String> userids3 = obj3.stream().map(users-> users.getId()).collect(Collectors.toList());
                                    
                                      Aggregation     agg3 = newAggregation(
                                                 match((Criteria.where("resource.id").in(userids3).andOperator(
                                                         Criteria.where("activityStatus").in(cl),
                                                         Criteria.where("activityType.id").is(activityid),
                                                         Criteria.where("actualStartDateTime").gte(startDate),
                                                              Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
                                                             )),
                                                     group("activityStatus").count().as("totalcount"),
                                                     project("totalcount").and("activityStatus").previousOperation(),
                                                     sort(Sort.Direction.DESC,"totalcount")
                                                     );
                                      AggregationResults<ActivityMap> groupResults4 = mongoTemplate.aggregate(agg3,Activity.class,ActivityMap.class);
                                      List<ActivityMap> result4 = groupResults4.getMappedResults();
                                       String username = UserNameAppender.userNameAppend(u.getName().getFirstName(), u.getName().getMiddleName(), u.getName().getLastName());

                                         chart3.put("activity", result4);
                                         chart3.put("userName",username);
                                         chart3.put("userRole",u.getUserRole().getName());
                                         chart3.put("noOfUsers", elements);
                                         jsonObjects1.add(chart3);

                                 });
                                 jsons.put("chart3",jsonObjects1);

                             
                             
                             
                    }
       
       
        }
       
        return jsons;
    }

}
