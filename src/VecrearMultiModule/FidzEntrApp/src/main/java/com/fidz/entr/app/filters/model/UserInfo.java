package com.fidz.entr.app.filters.model;


import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class UserInfo {
	
	private String userid;
	
	private String notificationId;
	
	private String phoneNo;
	
	private String email;

}
