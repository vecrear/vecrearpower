package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Boundary {

	private double north = 0.0;
	private double south = 0.0;
	private double east = 0.0;
	private double west = 0.0;
	
	public Boundary(double north, double south, double east, double west) {
		super();
		this.north = north;
		this.south = south;
		this.east = east;
		this.west = west;
	}
	
	@Override
	public String toString() {
		return "Boundary [north=" + north + ", south=" + south + ", east=" + east + ", west=" + west + "]";
	}
	
	
}
