package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.ActivityLogNotFoundException;
import com.fidz.entr.app.model.ActivityLog;
import com.fidz.entr.app.service.ActivityLogService;


@CrossOrigin(maxAge = 3600)
@RestController("FEAActivityLogController")
@RequestMapping(value="/activitylogs")
public class ActivityLogController {
	
	@Autowired
	private ActivityLogService  activityLogService;

	@PostMapping("get/all")
	public List<ActivityLog> getAllActivityLog(@RequestHeader(value="schemaName") String schemaName){
		return activityLogService.getAllActivityLog(schemaName);
	}
	/*@PostMapping
	public Mono<ActivityLog> craeteActivityLog(@Valid @RequestBody ActivityLog activityLog){
		return activityLogService.createActivityLog(activityLog);
	}*/
	/*@PostMapping
	public Flux<ActivityLog> createActivityLog(@Valid @RequestBody List<ActivityLog> activityLog){
		return activityLogService.createActivityLogForList(activityLog);
	}*/
	
	@PostMapping
	public List<ActivityLog> createActivityLog(@Valid @RequestBody List<ActivityLog> activityLog){
		return activityLogService.createActivityLogForList(activityLog);
	}
	
	@PostMapping("/creates")
	public void createActivityLogs(@Valid @RequestBody List<ActivityLog> activityLog){
		activityLogService.createActivityLogForLists(activityLog);
	}
	
	@PostMapping("/get/id/{id}")
	public ActivityLog getActivityLogById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		return activityLogService.getActivityLogById(id, schemaName);
		
	}
	
	@PostMapping("/update/id/{id}")
    public ActivityLog updateActivityLog(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody ActivityLog activityLog) {
        return activityLogService.updateActivityLog(id, activityLog);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteActivityLog(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return activityLogService.deleteActivityLog(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteActivityLog(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return activityLogService.deleteSoftActivityLog(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<ActivityLog> streamAllActivityLogs() {
        return activityLogService.streamAllActivityLogs();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A ActivityLog with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ActivityLogNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
