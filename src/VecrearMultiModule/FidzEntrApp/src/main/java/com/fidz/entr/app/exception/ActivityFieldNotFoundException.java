package com.fidz.entr.app.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ActivityFieldNotFoundException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4268614524084884773L;
	public ActivityFieldNotFoundException(String activityField) {
		super("Activity Field not found with id/name " + activityField);
	}
}
