package com.fidz.entr.app.filters.model;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.google.firebase.database.annotations.NotNull;
import lombok.Data;

@Data
@Document(collection = "broadcasttemplate")
@JsonIgnoreProperties(value = { "target" })
public class BroadCastTemplate extends Base{

@Id
private String id;	

private String userId;

private Modes mode;

private String templateName;

private String subject;

private String message;


}
