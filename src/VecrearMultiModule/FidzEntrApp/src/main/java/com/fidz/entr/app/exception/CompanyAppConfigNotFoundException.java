package com.fidz.entr.app.exception;

public class CompanyAppConfigNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5903396951983079291L;
	
	public CompanyAppConfigNotFoundException(String companyAppConfig) {
		super("CompanyAppConfig not found with id/name " + companyAppConfig);
	}

}
