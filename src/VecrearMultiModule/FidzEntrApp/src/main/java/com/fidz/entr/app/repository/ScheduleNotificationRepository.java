package com.fidz.entr.app.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.ScheduleNotification;

@Repository
public interface ScheduleNotificationRepository extends MongoRepository<ScheduleNotification, String>{

	List<ScheduleNotification> findByBeforeTime(Date localDateTime);
}
