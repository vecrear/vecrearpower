package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.superadmin.model.SuperAdmin;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class SuperAdminAuthenticateResponse {

	protected SuperAdmin superadmin;
	protected boolean isSuperAdmin;
	public SuperAdminAuthenticateResponse(SuperAdmin superadmin, boolean isSuperAdmin) {
		super();
		this.superadmin = superadmin;
		this.isSuperAdmin = isSuperAdmin;
	}
	@Override
	public String toString() {
		return "SuperAdminAuthenticateResponse [superadmin=" + superadmin + ", isSuperAdmin=" + isSuperAdmin + "]";
	}
	

}
