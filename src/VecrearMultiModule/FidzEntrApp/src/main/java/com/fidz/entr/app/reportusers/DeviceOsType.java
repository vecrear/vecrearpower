package com.fidz.entr.app.reportusers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum DeviceOsType {
	ANDROID,IOS,WEB,WINDOWS,WATCH,OTHER
}
