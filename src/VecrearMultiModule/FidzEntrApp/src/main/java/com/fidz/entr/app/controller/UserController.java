package com.fidz.entr.app.controller;

import java.util.List;

import java.util.Map;

import javax.naming.NameNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.service.UserService;
import com.fidz.services.authentication.model.Login;


@CrossOrigin(maxAge = 3600)
@RestController("FEAUserController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/users")
public class UserController {

	@Autowired
	private UserService  userService;
	
	@Value("${userservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all/status/{status}")
	//@Cacheable(value = "users")
	public List<User> getAllUsers(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsers(schemaName,status);
	}
	
	@PostMapping("/get/all/web/status/{status}")
	//@Cacheable(value = "users")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersWeb(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersWeb(schemaName,status);
	}
	

	   @PostMapping("/get/all/web/department/{deptid}/company/{comid}/status/{status}")
       public List<com.fidz.entr.app.reportusers.User> getAllUsersWebBasedOnDepartmentandCompany(@PathVariable(value="deptid") String did,@PathVariable(value="comid") String cid,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersWebBasedOnDepartmentandCompany(did,cid,schemaName,status);
	}
	
	   
	   @PostMapping("/get/all/web/department/{deptid}/company/{comid}/city/{cityid}/status/{status}")
       public List<com.fidz.entr.app.reportusers.User> getAllUsersWebBasedOnDepartmentandCompanyandCity(@PathVariable(value="deptid") String did,@PathVariable(value="comid") String cid,@PathVariable(value="cityid") String cityid,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersWebBasedOnDepartmentandCompanyandCity(did,cid,cityid,schemaName,status);
	}
	   
	   
	@PostMapping("/get/onelevel/id/{id}/status/{status}")
	//@Cacheable(value = "users")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRoles(@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersbaseRoles(schemaName,id,status);
	}
	
	
	@PostMapping("/get/onelevel/forgeofenceunmapped/id/{id}/status/{status}")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesForGeofences(@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersbaseRolesForGeofences(schemaName,id,status);
	}
	
	
	@PostMapping("/get/onelevel/forgeofencemapped/id/{id}/status/{status}")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesForGeofencesMapped(@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersbaseRolesForGeofencesMapped(schemaName,id,status);
	}
	
	
	@PostMapping("/aboveonelevel/roleid/{roleid}/status/{status}")
	//@Cacheable(value = "users")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesAboveOnelevel(@PathVariable(value="roleid") int roleid,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersbaseRolesAboveOnelevel(schemaName,roleid,status);
	}
	
	
	
	@PostMapping("/aboveonelevel/deptid/{deptid}/roleid/{roleid}/status/{status}")
	//@Cacheable(value = "users")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersbaseRolesAboveOnelevel(@PathVariable(value="deptid") String deptid,@PathVariable(value="roleid") int roleid,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersbaseRolesAboveOnelevelbyDeptandRoleId(deptid,schemaName,roleid,status);
	}
	
	
	@PostMapping("/status/type/{type}/id/{id}/status/{status}")
	//@Cacheable(value = "users")
	public String getStatusofObjectMappedOrNot(@PathVariable(value="type") String type,@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getStatusofObjectMappedOrNot(type,schemaName,id,status);
	}
	
	
	
	
	@PostMapping("/get/alllevels/id/{id}/status/{status}")
	//@Cacheable(value = "users")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersRoles(@PathVariable(value="id") String id,@RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersRoles(schemaName,id,status);
	}
	
	

	
	//Post request to get all Users web  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}/status/{status}")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersWebPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status) {
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersWebPaginated(pageNumber, pageSize, schemaName, status);
	}
	
	@PostMapping("get/all/web/search/{text}/{pageNumber}/{pageSize}/status/{status}")
	public List<com.fidz.entr.app.reportcustomerswebPlan.User> getAllUsersWebPaginatedTextSearch(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName,@PathVariable(value="status") String status,@PathVariable(value="text") String text) {
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUsersWebPaginatedTextSearch(pageNumber, pageSize, schemaName, status, text);
	}
	
	
	@PostMapping
	public User createUser(@Valid @RequestBody User user){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		System.out.println("user creation has been started------------------");
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.createUser(user);
	}
	
	@PostMapping("/creates")
	public void createUsers(@Valid @RequestBody User user){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		userService.createUsers(user);
	}
	
	@PostMapping("/get/id/{id}")
	public User getUserById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getUserById(id, schemaName);
		
	}
	
	@PostMapping("/update/id/{id}")
    public User updateUser(@PathVariable(value = "id") String id,  @Valid @RequestBody User user) {
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.updateUser(id, user);
    }
	
	
	@PostMapping("/update/makeactive/id/{id}")
    public User updateUserMakeActive(@PathVariable(value = "id") String id , @Valid @RequestBody User user) throws NameNotFoundException {
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.updateUserMakeActive(id, user);
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteUser(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.deleteUser(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteUser(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.deleteSoftUser(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<User> streamAllUsers() {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.streamAllUsers();
    }
    @PatchMapping("/update/id/{id}/patch")
    public User updateUserPatch(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody String schemaName) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.updateUserPatch(id, schemaName);
    }
    @PostMapping("/get/password/decrypt/password")
	public String getDeycryptPassword(@Valid @RequestBody Login login, @RequestHeader(value="schemaName") String schemaName){
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getDeycryptPassword(login, schemaName);
		
	}
    
    @PatchMapping("/update/{id}")
    public User updatePunchStatus(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return userService.updatePunchStatus(id,schemaName);
       
    }
    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A User with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(UserNotFoundException ex) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return ResponseEntity.notFound().build();
    }
    
    @PostMapping("/get/username/{userName}")
    public User getAllUserByuserName(@PathVariable(value = "userName") String username,@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return userService.getAllUserByuserName(username,schemaName);


	}
    
    
    
	
}
