package com.fidz.entr.app.controller;

import java.util.List;

import lombok.Data;

@Data
public class CustomerReq {
	
	private List<String> customers;
	
	private String schemaName;

}
