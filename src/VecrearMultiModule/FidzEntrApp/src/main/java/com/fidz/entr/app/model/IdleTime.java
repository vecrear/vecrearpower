package com.fidz.entr.app.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "idletime")
@JsonIgnoreProperties(value = { "target" })
public class IdleTime extends Base{
	
	@Id
	protected String id;
	
	protected long fromDateTimeStamp;
	
	protected long toDateTimeStamp;
	
	protected String duration;
	
	protected String address;

	public IdleTime(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, long fromDateTimeStamp, long toDateTimeStamp, String duration,
			String address) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.fromDateTimeStamp = fromDateTimeStamp;
		this.toDateTimeStamp = toDateTimeStamp;
		this.duration = duration;
		this.address = address;
	}

	public IdleTime() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "IdleTime [id=" + id + ", fromDateTimeStamp=" + fromDateTimeStamp + ", toDateTimeStamp="
				+ toDateTimeStamp + ", duration=" + duration + ", address=" + address + ", createdTimestamp="
				+ createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp
				+ ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser="
				+ deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	
	
	

}
