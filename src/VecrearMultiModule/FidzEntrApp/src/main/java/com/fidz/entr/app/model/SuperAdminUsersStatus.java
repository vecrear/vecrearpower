package com.fidz.entr.app.model;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.TaggedDevices;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class SuperAdminUsersStatus {
	@Id
	protected String id;
	protected String userName;
	protected Status userStatus;
	public SuperAdminUsersStatus(String id, String userName, Status userStatus) {
		super();
		this.id = id;
		this.userName = userName;
		this.userStatus = userStatus;
	}
	public SuperAdminUsersStatus() {
		// TODO Auto-generated constructor stub
	}
	
	
}
