package com.fidz.entr.app.filters.controller;

import java.text.ParseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fidz.entr.app.filters.model.LastpunchinDate;
import com.fidz.entr.app.filters.model.PunchedInCount;
import com.fidz.entr.app.filters.service.AttendanceFilterService;


@CrossOrigin(maxAge = 3600)
@RestController("FEAAttendanceFilterController")
@RequestMapping(value="/filter")
public class AttendanceFilterController {
	
	@Autowired
	private AttendanceFilterService attendanceFilterService;
	
	
	
	@PostMapping("/attendance/id/{userid}")
	    public List<PunchedInCount> getpunchIncount(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="userid")String userid) throws ParseException {
	    return attendanceFilterService.getpunchIncount(schemaName, userid);
	    }
	    
	    @PostMapping("/attendance/lastpunchindate/id/{userid}")
	    	 public List<LastpunchinDate> getlastpunch(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="userid")String userid) throws ParseException {
	    		    return attendanceFilterService.getlastpunch(schemaName,userid);
	    }
	    
}

