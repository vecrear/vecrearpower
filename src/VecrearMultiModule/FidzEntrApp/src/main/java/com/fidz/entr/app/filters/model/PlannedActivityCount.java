package com.fidz.entr.app.filters.model;

import lombok.Data;
@Data
public class PlannedActivityCount {
    
	
	private String activityConfigurationType;
	
	private String totalPlannedActivity;

	public PlannedActivityCount(String activityConfigurationType, String totalPlannedActivity) {
		super();
		this.activityConfigurationType = activityConfigurationType;
		this.totalPlannedActivity = totalPlannedActivity;
	}

	@Override
	public String toString() {
		return "PlannedActivityCount [activityConfigurationType=" + activityConfigurationType
				+ ", totalPlannedActivity=" + totalPlannedActivity + "]";
	}
	
	
	
	
}
