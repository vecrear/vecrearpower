package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.CompanyAppConfig;

@Repository("FEACompanyAppConfigRepository")
public interface CompanyAppConfigRepository extends MongoRepository<CompanyAppConfig, String>{

}
