package com.fidz.entr.app.awsconfig.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.entr.app.awsconfig.model.AwsInfo;
import com.fidz.entr.app.awsconfig.model.Userpwdencrypt;
import com.fidz.entr.app.awsconfig.service.AwsInfoNotFoundException;
import com.fidz.entr.app.awsconfig.service.AwsInfoService;
import com.fidz.entr.app.awsconfig.service.UserPwdEncryptService;



@CrossOrigin(maxAge = 3600)
@RestController("FEAUserPwdController")
@RequestMapping(value="/userpwdencrypt")
public class UserPwdEncryptController {
	
	@Autowired
	private UserPwdEncryptService userPwdEncryptService;
	
	@PostMapping("/get/all")
	//@Cacheable(value = "users")
	public List<Userpwdencrypt> getAllInfo(@RequestHeader(value="schemaName") String schemaName){
		return userPwdEncryptService.getAllInfo(schemaName);
	}
	
	@PostMapping
	public String createInfo(@Valid @RequestBody Userpwdencrypt info){
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return userPwdEncryptService.createInfo(info);
	}
	
	@PostMapping("/decrypt")
	public String decryptInfo(@Valid @RequestBody Userpwdencrypt info){
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return userPwdEncryptService.decryptInfo(info);
	}
	

}
