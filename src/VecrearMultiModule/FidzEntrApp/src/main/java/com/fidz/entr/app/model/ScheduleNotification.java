package com.fidz.entr.app.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "scheduler")
@JsonIgnoreProperties(value = { "target" })
public class ScheduleNotification{
	
	
	public ScheduleNotification(String id, @NotNull String schemaName, String userid, String activityId,
			String notificationid, String notifyMessage, String objectType, String time, String beforeTime) {
		super();
		this.id = id;
		this.schemaName = schemaName;
		this.userid = userid;
		this.activityId = activityId;
		this.notificationid = notificationid;
		this.notifyMessage = notifyMessage;
		this.objectType = objectType;
		this.time = time;
		this.beforeTime = beforeTime;
	}

	public ScheduleNotification() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	protected String id;
	
	@NotNull
	protected String schemaName;
	
	protected String userid;
	
	protected String activityId;
	
	protected String notificationid;
	
	protected String notifyMessage;
	
	protected String objectType;
	
	protected String time;
	
	protected String beforeTime;

	




	

	

	
	

	
	

}
