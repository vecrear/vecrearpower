package com.fidz.entr.app.reportattendance;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.superadmin.model.RoleFeatureConfig;

import lombok.Data;


@Data
@Document(collection = "userrole")
@JsonIgnoreProperties(value = { "target" })
public class UserRole extends com.fidz.entr.base.model.UserRole{

	@NotNull
	protected int userRoleId;

	
	protected RoleFeatureConfig roleFeatureConfig;





	
	
	


	
		
	
}
