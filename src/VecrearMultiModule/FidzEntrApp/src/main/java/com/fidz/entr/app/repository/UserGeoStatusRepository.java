package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.UserGeoStatus;

@Repository("UserGeoStatusRepository")
public interface UserGeoStatusRepository extends MongoRepository<UserGeoStatus, String> {
	UserGeoStatus findByUserId(String userId);
}
