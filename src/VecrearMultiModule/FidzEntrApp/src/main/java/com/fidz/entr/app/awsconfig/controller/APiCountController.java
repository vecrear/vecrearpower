package com.fidz.entr.app.awsconfig.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.awsconfig.model.APiCountInfo;
import com.fidz.entr.app.awsconfig.model.APiCountLimit;
import com.fidz.entr.app.awsconfig.service.APiCountInfoService;
import com.fidz.entr.app.awsconfig.service.APiCountMaxLimitService;
import com.fidz.entr.app.awsconfig.service.ApiCountInfoNotFoundException;
import com.fidz.entr.app.awsconfig.service.ApiCountLimitNotFoundException;
import com.fidz.entr.app.model.Customer;

@CrossOrigin(maxAge = 3600)
@RestController("FEAAPiCountController")
@RequestMapping(value = "/apicount")
public class APiCountController {

	@Autowired
	private APiCountInfoService aPiCountInfoService;
	

	@Autowired
	private APiCountMaxLimitService countMaxLimitService;
	

	@PostMapping("/get/all")
	// @Cacheable(value = "users")
	public List<APiCountInfo> getAllInfo(@RequestHeader(value = "schemaName") String schemaName) {
		return aPiCountInfoService.getAllInfo(schemaName);
	}

	@PostMapping
	public APiCountInfo createInfo(@Valid @RequestBody APiCountInfo info,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return aPiCountInfoService.createInfo(info, schemaName);
	}
	
	
	@PostMapping("/limitinfo")
	public APiCountLimit maxLimitInfo(@Valid @RequestBody APiCountLimit maxLimitInfo,
			@RequestHeader(value = "schemaName") String schemaName) throws ApiCountLimitNotFoundException {
		System.out.println("maxLimitInfo ----------------"+maxLimitInfo.toString());
		return countMaxLimitService.maxLimitInfo(maxLimitInfo, schemaName);
	}
	
	
	@PostMapping("/limitinfo/get/all")
	// @Cacheable(value = "users")
	public List<APiCountLimit> getAllInfolimitinfo(@RequestHeader(value = "schemaName") String schemaName) {
		return countMaxLimitService.getAllInfolimitinfo(schemaName);
	}
	
	
	
	@PostMapping("/limitinfo/get/id/{id}")
	public ResponseEntity<APiCountLimit> getAPiCountLimitId(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName) throws ApiCountLimitNotFoundException{
		
	
		return countMaxLimitService.getAPiCountLimitId(id, schemaName);
		
		
	}
	
	@PostMapping("/limitinfo/update/id/{id}")
	public APiCountLimit updateAPiCountLimitId(@PathVariable(value="id") String id,@Valid @RequestBody APiCountLimit aPiCountLimitinfo) throws ApiCountLimitNotFoundException{
	
		return countMaxLimitService.updateAPiCountLimitId(id,aPiCountLimitinfo);
		
	}

	@PostMapping("/limitinfo/get/companyid/{companyid}")
	public APiCountLimit getbycomapnyId(@PathVariable(value="companyid") String companyId,@RequestHeader(value="schemaName") String schemaName) {
		
		return countMaxLimitService.getAPiCountLimitbycompanyId(companyId, schemaName);

	}
	
	@PostMapping("/limitinfo/delete/id/{id}")
	public Map<String, String> deleteAPiCountLimitId(@PathVariable(value="id") String id,@RequestHeader(value="schemaName")String schemaName) throws ApiCountLimitNotFoundException{
		
	
		return countMaxLimitService.deleteAPiCountLimitId(id,schemaName);
		
		
	}
	
	@PostMapping("/get/byurltype")
	public Map<String, String> getAPiCountInfo(@Valid @RequestBody APiCountInfo info,
			@RequestHeader(value = "urltype") String urltype) throws ApiCountInfoNotFoundException {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return aPiCountInfoService.getAPiCountInfo(info, urltype);
	}
	
	
	
	@PostMapping("/get/bycomanyid/{comanyid}")
	public Map<String, String> getAPiCountInfoByCompany(@PathVariable(value="comanyid") String comanyid,
			@RequestHeader(value="schemaName")String schemaName) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return aPiCountInfoService.getAPiCountInfoByCompany(comanyid, schemaName);
	}
	
	
	
	@PostMapping("/get/bydeptid/{deptid}")
	public Map<String, String> getAPiCountInfoByDept(@PathVariable(value="deptid") String deptid,
			@RequestHeader(value="schemaName")String schemaName) {
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return aPiCountInfoService.getAPiCountInfoByDept(deptid, schemaName);
	}
	
	
	
	
	
	
	
	
	
	


}
