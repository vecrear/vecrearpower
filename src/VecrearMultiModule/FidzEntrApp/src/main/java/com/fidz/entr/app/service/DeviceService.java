package com.fidz.entr.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.naming.NameNotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.model.Topic;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.User;
import com.fidz.entr.base.exception.DeviceNotFoundException;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.repository.DeviceRepository;
import com.fidz.entr.superadmin.model.SuperAdmin;
import com.fidz.entr.superadmin.service.SuperAdminService;

@Service("FEADeviceService")
public class DeviceService {

	@Autowired
	private DeviceRepository deviceRepository;

	private GenericAppINterface<Device> genericINterface;

	@Autowired
	public DeviceService(GenericAppINterface<Device> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private SuperAdminService superAdminService;

	public List<Device> getAllDeviceData(String schemaName, String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllDeviceData DeviceService.java FEADEVICESERVICE");
		List<Device> devices = null;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("status").is(status));
			devices = this.genericINterface.find(query, Device.class);
		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error while getting data from getAllDeviceData DeviceService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully fetched data from getAllDeviceData DeviceService.java");
		return devices;

	}

	public Device createDevice(Device device) {
		Constant.LOGGER.info("Inside createDevice DeviceService.java");
		String schemaName = device.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// String DeviceId=device.getDeviceId();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		Device dev = getDeviceDataByDeviceId(device.getDeviceId(), schemaName);

		Query query3 = new Query();
		query3.addCriteria(Criteria.where("userName").is(device.getDeviceId()));
		List<SuperAdmin> superAdmins = this.mongoTemplate.find(query3, SuperAdmin.class);
		Constant.LOGGER.info("after getting super admin");

		List<String> names = superAdmins.stream().map(p -> p.getUserName()).collect(Collectors.toList());
		Constant.LOGGER.info("names" + names);

		if (superAdmins.isEmpty() && dev != null) {
			if (dev.getDeviceId().equals(device.getDeviceId())) {
				Constant.LOGGER.info("Inside if statement--------------------------------------------------------");
				throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + device.getDeviceId());
			}
		}

		if (superAdmins.size() > 0 && names.stream().anyMatch(s -> s.equals(device.getDeviceId()) == true)) {

			System.out.println("superAdmins------------size" + superAdmins.size());
			System.out.println("names------------------size" + names.size());
			// -------------------------------------------------userCheck-------------------------------------------------------------------------------------
			Constant.LOGGER.info("Inside if statement--------------------------------------------------------");
			throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + device.getDeviceId());

		} else if (superAdmins.size() > 0
				&& names.stream().anyMatch(s -> s.equalsIgnoreCase(device.getDeviceId()) == true)) {
			Constant.LOGGER.info("Inside else if statement");
			throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + device.getDeviceId());

		} else {
			System.out.println("inside elseeeee");
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

			return this.deviceRepository.save(device);
		}

	}

	public void createDevices(@Valid @RequestBody Device device) {
		String schemaName = device.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		String DeviceId = device.getDeviceId();
		Device dev = getDeviceDataByDeviceId(DeviceId, schemaName);

		if (dev != null) {
			throw new NameFoundException(ExceptionConstant.SAME_DEVICE + ": " + device.getDeviceId());
		} else {
			this.deviceRepository.save(device);
		}
		// this.genericINterface.saveName(device);
	}

	public Device getDeviceDataById(String id, String schemaName) {
		Constant.LOGGER.info("Inside getDeviceDataById DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Device.class);
		// return deviceRepository.findById(id);
	}

	public Object getCurrentDataById(String id, String schemaName) {
		Constant.LOGGER.info("Inside getCurrentDataById DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device deviceMono = this.genericINterface.findById(id, Device.class);
		Device device = null;
		if (deviceMono != null) {
			device = deviceMono;
		}
		return device.getCurrentData();

	}

	// update the current device data
	public Device updateCurrentData(String id, Object currentData, String schemaName) {
		Constant.LOGGER.info("Inside updateCurrentData DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

		Device deviceMono = this.genericINterface.findById(id, Device.class);
		Device device = null;
		if (deviceMono != null) {
			device = deviceMono;
		}
		device.setCurrentData(currentData);
		return this.deviceRepository.save(device);

	}
	/// patch request for device

	public Device updateCurrentDataDeviceId(String deviceid, Object deviceHardwareDetails, String schemaName) {
		Constant.LOGGER.info("Inside updateCurrentDataDeviceId DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device deviceMono = this.deviceRepository.findByDeviceId(deviceid);
		Device device = null;
		if (deviceMono != null) {
			device = deviceMono;
		}
		device.setDeviceHardwareDetails(deviceHardwareDetails);
		return this.deviceRepository.save(device);

	}

	public Device updateDeviceData(String id, Device device) {
		Constant.LOGGER.info("Inside updateDeviceData DeviceService.java");
		String schemaName = device.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device dev = null;
		Device d = null;
		SuperAdmin superAdmin = null;

		try {
			dev = getDeviceDataByDeviceId(device.getDeviceId(), schemaName);
			superAdmin = this.superAdminService.getSuperAdminByUserName(device.getDeviceId(), "VTrackSuperAdmin");

		} catch (Exception ex) {
			Constant.LOGGER.info("Error inside try block of updateDeviceData DeviceService.java");
		}

		Query query3 = new Query();
		query3.addCriteria(Criteria.where("userName").is(device.getDeviceId()));
		List<SuperAdmin> superAdmins = this.mongoTemplate.find(query3, SuperAdmin.class);
		System.out.println("inside after same id validation" + superAdmins);

		List<String> names = superAdmins.stream().map(p -> p.getUserName()).collect(Collectors.toList());
		if (dev != null) {
			if (dev.getId().equalsIgnoreCase(id)) {
				names.remove(dev.getDeviceId());
			}
		}
		if (superAdmins.isEmpty()) {

			Constant.LOGGER.info("Inside if statement--------------------------------------------------------");
			throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + device.getDeviceId());

		}
		if (superAdmins.size() > 0 && names.stream().anyMatch(s -> s.equals(device.getDeviceId()) == true)) {

			System.out.println("superAdmins------------size" + superAdmins.size());
			System.out.println("names------------------size" + names.size());
			// -------------------------------------------------userCheck-------------------------------------------------------------------------------------
			Constant.LOGGER.info("Inside if statement--------------------------------------------------------");
			throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + device.getDeviceId());

		} else if (superAdmins.size() > 0
				&& names.stream().anyMatch(s -> s.equalsIgnoreCase(device.getDeviceId()) == true)) {
			Constant.LOGGER.info("Inside else if statement");
			throw new NameFoundException(ExceptionConstant.SAME_NUMBER + ": " + device.getDeviceId());

		} else {
			Constant.LOGGER.info("Inside else statement--------------------------------------------------------");

			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

			device.setId(id);
			d = this.deviceRepository.save(device);
		}

		return d;

	}

	// hard delete
	public Map<String, String> deleteDevice(String id, String schemaName) {

		Constant.LOGGER.info("Inside deleteDevice DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		TimeUpdate timeUpdate = new TimeUpdate();

		Device dev = getDeviceDataById(id, schemaName);
		if (dev != null) {

			dev.setStatus(Status.INACTIVE);
			dev.setDeletedByUser(dev.getCreatedByUser());
			dev.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());

			dev = this.deviceRepository.save(dev);

			// this.deviceRepository.deleteById(id);
		} else {
			Constant.LOGGER.info("Device is null inside DeviceService.java deleteDevice()");
		}

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Device deleted successfully");
		Constant.LOGGER.info("Device deleted successfully made INACTIVE DeviceService.java deleteDevice()");

		return response;
	}

	// soft delete
	public Map<String, String> deleteSoftCompany(String id, String schemaName, TimeUpdate timeUpdate) {
		Constant.LOGGER.info("Inside deleteSoftCompany DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device device = this.genericINterface.findById(id, Device.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		device.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		device.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		device.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(device);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Device deleted successfully");
		return response;

	}

	public Device getDeviceDataByDeviceId(String deviceid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device d = null;
		try {
			Constant.LOGGER.info("Inside getDeviceDataByDeviceId DeviceService.java");
			d = this.deviceRepository.findByDeviceId(deviceid);
			System.out.println("@@@@@@@@@@@@@@@this.deviceRepository.findByDeviceId(deviceid)"
					+ this.deviceRepository.findByDeviceId(deviceid));
		} catch (Exception e) {
			// TODO: handle exception
		} // return this.deviceRepository.findByDeviceId(deviceid);
		return d;
	}
	// get topic

	public Topic getTopicById(String id, String schemaName) {
		Constant.LOGGER.info("Inside getTopicById DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// Device device = this.deviceRepository.findById(id);
		Device device = this.genericINterface.findById(id, Device.class);
		Topic monotopic = null;
		if (device.getStream() != null) {

			Topic topic = new Topic(device.getStream().getTopic());
			topic.setName(device.getStream().getTopic());
			monotopic = topic;
		}

		return monotopic;
	}

	public Topic getTopicByDeviceId(String deviceid, String schemaName) {
		Constant.LOGGER.info("Inside getTopicByDeviceId DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device device = this.deviceRepository.findByDeviceId(deviceid);

		Topic monotopic = null;
		if (device.getStream() != null) {
			Topic topic = new Topic(device.getStream().getTopic());
			System.out.println("topic name:" + topic.getName());
			topic.setName(device.getStream().getTopic());
			monotopic = topic;
		}

		return monotopic;

	}

	public Stream<Device> getDevicesByDeviceType(String name, String schemaName) {
		Constant.LOGGER.info("Inside getDevicesByDeviceType DeviceService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// List<Device> dev= this.genericINterface.findAll(Device.class);
		return this.genericINterface.findAll(Device.class).stream().filter(data -> data.getDeviceType().equals(name));
		// filter(dev -> dev.getDeviceType().getName().contains(name));
		// .filter(dev -> dev.getDeviceType().getName().contains(name));
	}

//  	public Stream<Device> getDevicesByDeviceTypeId(String id, String schemaName) {
//  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//        return this.deviceRepository.findAll().stream().filter(data -> data.getDeviceType().getId().contains(id));
//        		//.filter(devices -> devices.getDeviceType().getId().contains(id));
//  	}

	public List<Device> streamAllDevices() {
		return deviceRepository.findAll();
	}

	// To get all devices paginated
	// page number starts from zero
	public List<Device> getAllDeviceDataPaginated(int pageNumber, int pageSize, String status, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllDeviceDataPaginated DeviceService.java");
		Query query = new Query();
		List<Device> devices = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.addCriteria(Criteria.where("status").is(status));
			query.with(pageableRequest);
			devices = this.genericINterface.find(query, Device.class);

		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error occurred inside getAllDeviceDataPaginated DeviceService.java " + ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllDeviceDataPaginated " + query);
		return devices;

	}

	public List<Device> getAllDeviceDataByComapany(String companyId, String schemaName, String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllDeviceDataByComapany DeviceService.java");
		Query query = new Query();
		List<Device> devices = null;

		try {
			query.addCriteria(
					Criteria.where("company.id").in(companyId).andOperator(Criteria.where("status").is(status)));
			devices = this.genericINterface.find(query, Device.class);

		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error occurred inside getAllDeviceDataByComapany DeviceService.java " + ex.getMessage());
		}

		return devices;
	}

	public List<Device> getAllTagUnTaggedDevices(String companyid, String variableMap, String schemaName,
			String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside get all devices  Service.java");
		Query query = new Query();
		List<Device> devices = null;

		try {
			if (variableMap.equalsIgnoreCase("tagged")) {
				query.addCriteria(Criteria.where("deviceTagged").is(true).andOperator(
						Criteria.where("company.id").is(companyid).andOperator(Criteria.where("status").is(status))));
			} else if (variableMap.equalsIgnoreCase("untagged")) {
				query.addCriteria(Criteria.where("deviceTagged").is(false).andOperator(
						Criteria.where("company.id").is(companyid).andOperator(Criteria.where("status").is(status))));
			}

			devices = this.genericINterface.find(query, Device.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error inside Device. Device.java " + ex.getMessage());
		}
		return devices;
	}

	public List<Device> getAllDevicesByUserName(String username, String schemaName, String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside all devicees devicedeviceService.java");
		Query query1 = new Query();
		Query query2 = new Query();
		List<Device> device = new ArrayList<Device>();
		List<Device> deviceList = null;

		try {
			query1.addCriteria(
					Criteria.where("taggedUserName").is(username).andOperator(Criteria.where("status").is(status)));
			Constant.LOGGER.info("query-------------" + query1.toString());

			deviceList = this.genericINterface.find(query1, Device.class);
			device.addAll(deviceList);
			query2.addCriteria(
					Criteria.where("deviceTagged").is(false).andOperator(Criteria.where("status").is(status)));
			Constant.LOGGER.info("query-------------" + query2.toString());

			deviceList = this.genericINterface.find(query2, Device.class);
			device.addAll(deviceList);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error inside device. device.java " + ex.getMessage());
		}
		return device;
	}

	public List<Device> getDeviceDataByImei(String imei, String schemaName) {
		List<Device> deviceList = null;
		Constant.LOGGER.info("imei-------------" + imei + "--" + schemaName);
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query1 = new Query();
		query1.addCriteria(Criteria.where("imei").is(imei));
		Constant.LOGGER.info("query-------------" + query1.toString());

		deviceList = this.genericINterface.find(query1, Device.class);
		return deviceList;
	}

	public Device updateDeviceMakeActive(String id, @Valid Device device) throws NameNotFoundException {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(device.getSchemaName());
		Device updatedToActivedevice = null;
		List<Device> deviceList = null;
		Constant.LOGGER.info("Inside updateDeviceMakeActive DeviceService.java");

		if (device.getCompany().getId() != null) {
			Constant.LOGGER.info("Inside updateDeviceMakeActive DeviceService.java when is not null");
			String comId = device.getCompany().getId();
			String schemaName = device.getSchemaName();
			String status = "ACTIVE";
			deviceList = getAllDeviceDataByComapany(comId, schemaName, status);
			if (deviceList.size() > 0) {
				Constant.LOGGER.info("Company is fine ");
				Device existingDevice = getDeviceDataById(id, schemaName);

				if (existingDevice != null) {
					Constant.LOGGER.info("device is fine ");
					updatedToActivedevice = updateDeviceData(id, device);

				} else {
					throw new NameNotFoundException("Your Device does Not Exist please create a Device and Map newly");
				}
			} else {
				throw new NameNotFoundException("Your Company does Not Exist please create a Company and Map newly");
			}

		}

		return updatedToActivedevice;

	}

	public List<Device> deviceTextSearch(@Valid String text, String schemaName, int pageNumber, int pageSize,
			String status) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<Device> dev = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = new Query();
		Constant.LOGGER.info("Inside deviceTextSearch deviceService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(Device.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query.addCriteria(Criteria.where("status").is(status));
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			dev = mongoTemplate.find(query, Device.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside deviceTextSearch deviceService.java" + ex.getMessage());

		}
		return dev;
	}

}
