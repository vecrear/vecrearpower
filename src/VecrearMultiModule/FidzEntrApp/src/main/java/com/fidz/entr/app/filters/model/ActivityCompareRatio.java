package com.fidz.entr.app.filters.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class ActivityCompareRatio {

	
	private Object actualStartDateTime;
    
	private int totalcount;

	@Override
	public String toString() {
		return "ActivityCompareRatio [actualStartDateTime=" + actualStartDateTime + ", totalcount=" + totalcount + "]";
	}

	public ActivityCompareRatio(Object actualStartDateTime, int totalcount) {
		super();
		this.actualStartDateTime = actualStartDateTime;
		this.totalcount = totalcount;
	}

	public ActivityCompareRatio() {
		super();
		// TODO Auto-generated constructor stub
	}
	
    

	
	
	
	
	
	
	
}
