package com.fidz.entr.app.configs;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class FCMInitializ {

    @Value("${app.firebase-configuration-file}")
    private String firebaseConfigPath;

    Logger logger = LoggerFactory.getLogger(FCMInitializ.class);
    
    @PostConstruct
    public void initialize() {
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource(firebaseConfigPath).getInputStream())).build();
            logger.info(options.toString());
            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                logger.info("Firebase application has been initialized");
            }
            
          
            
			/*
			 * try { options = new FirebaseOptions.Builder()
			 * .setCredentials(GoogleCredentials.fromStream(new
			 * ClassPathResource("/serviceAccountKey.json").getInputStream()))
			 * .setDatabaseUrl(FB_BASE_URL) .build(); if(FirebaseApp.getApps().isEmpty()) {
			 * //<--- check with this line FirebaseApp.initializeApp(options); } } catch
			 * (IOException e) { e.printStackTrace(); }
			 */
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

}
