package com.fidz.entr.app.reportcustomersweb;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "region")
@JsonIgnoreProperties(value = { "target" })
public class Region extends SimplifiedBase {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	@JsonIgnore
	protected boolean regionTagged;
	@JsonIgnore
	protected String taggedCountryName;
	@JsonIgnore
	protected String description;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<State> states;
	@JsonIgnore
	protected List<Object> taggedStates;


	
	

	

	



	

}
