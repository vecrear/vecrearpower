package com.fidz.entr.app.filters.service;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.DateOperators;
import org.springframework.data.mongodb.core.aggregation.StringOperators;
import org.springframework.data.mongodb.core.aggregation.StringOperators.Concat;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.ActivityCompare;
import com.fidz.entr.app.filters.model.ActivityCompareRatio;
import com.fidz.entr.app.filters.model.ActivityMap;
import com.fidz.entr.app.filters.model.CompareActivity;
import com.fidz.entr.app.filters.model.CompareActivityRatio;
import com.fidz.entr.app.filters.model.DashboardFilter;
import com.fidz.entr.app.filters.model.PlannedActivityCount;
import com.fidz.entr.app.filters.model.UnPlannedActivityCount;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.repository.ActivityTypeRepository;

@Service("FEAActivityFilterService")
public class ActivityFilterService{

	@Autowired
	MongoTemplate mongoTemplate;
	
	private GenericAppINterface<ActivityType> genericINterface;
	
	@Autowired
	public ActivityFilterService(GenericAppINterface<ActivityType> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	@Autowired
	ActivityTypeRepository activityTypeRepository;
	
	public List<UnPlannedActivityCount> getplannedActivitycount(String schemaName, String userid) throws ParseException {
		     
		   	 Constant.LOGGER.info("Inside get UnPlannedActivityCount() ActivityFilterService.java");
		     StopWatch watch = new StopWatch();
		   	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		     watch.start();
		   	 Instant instant = Instant.ofEpochMilli(new Date().getTime());
		  	 ZonedDateTime TimeinIST = instant.atZone(ZoneId.of("Asia/Kolkata"));
		 	 Constant.LOGGER.info("Date_time"+TimeinIST);
		  	 Date registerTimeBeginDate = Date.from(TimeinIST.toInstant());
		  	 Instant instant1 = Instant.ofEpochMilli(new Date().getTime()+86400000);
		  	 Constant.LOGGER.info("instant 1"+instant1);
		  	 ZonedDateTime TimeinIST1 = instant.atZone(ZoneId.of("Asia/Kolkata"));
		  	 ZonedDateTime zdtNextDay = TimeinIST1.plusDays( 1 );
		 	 Constant.LOGGER.info("Date_time"+zdtNextDay);
		  	 Date registerTimeBeginDate1 = Date.from(zdtNextDay.toInstant());
		  	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			 String strDate = format.format(registerTimeBeginDate+" "+"00:00");
		     String endDate1 = format.format(registerTimeBeginDate1+" "+"23:59");
		     Date endDate =format.parse(endDate1);

		     Constant.LOGGER.info("strDate"+strDate);
		     Date startDate =format.parse(strDate);
		     Constant.LOGGER.info("time"+startDate);
		 
		 	 Aggregation agg1 = newAggregation( 
		     			match(Criteria.where("plannedStartDateTime").gte(startDate).lte(endDate).andOperator
		     					(Criteria.where("activityConfigurationType").is("UNPLANNED")
		     							.orOperator(Criteria.where("activityConfigurationType").is("PLANNED"))
		     							.orOperator(Criteria.where("activityConfigurationType").is("SELFPLANNED"))
		     							.andOperator(Criteria.where("resource.id").is(userid)))),
		                group("activityConfigurationType").count().as("totalUnPlannedActivity"),
		                project("totalUnPlannedActivity").and("activityConfigurationType").previousOperation(),
		                sort(Sort.Direction.DESC,"totalUnPlannedActivity") 
		               );
		      
		 	  AggregationResults<UnPlannedActivityCount> groupResults = mongoTemplate.aggregate(agg1,Activity.class,UnPlannedActivityCount.class );
		      List<UnPlannedActivityCount> result = groupResults.getMappedResults();
		      Constant.LOGGER.info("Aggregate result"+agg1);
		      watch.stop();
		     
		      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
		      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
		     
		      return result;
		
	}

	

	
	public List<PlannedActivityCount> getunplannedActivitycount(String schemaName, String userid) throws ParseException {
	     
	   	 Constant.LOGGER.info("Inside get PlannedActivityCoun() ActivityFilterService.java");
	     StopWatch watch = new StopWatch();
	   	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     watch.start();
	     Instant instant = Instant.ofEpochMilli(new Date().getTime());
	  	 ZonedDateTime TimeinIST = instant.atZone(ZoneId.of("Asia/Kolkata"));
	 	 Constant.LOGGER.info("Date_time"+TimeinIST);
	  	 Date registerTimeBeginDate = Date.from(TimeinIST.toInstant());
	  	 Instant instant1 = Instant.ofEpochMilli(new Date().getTime()+86400000);
	  	 Constant.LOGGER.info("instant 1"+instant1);
	  	 ZonedDateTime TimeinIST1 = instant.atZone(ZoneId.of("Asia/Kolkata"));
	  	 ZonedDateTime zdtNextDay = TimeinIST1.plusDays( 1 );
	 	 Constant.LOGGER.info("Date_time"+zdtNextDay);
	  	 Date registerTimeBeginDate1 = Date.from(zdtNextDay.toInstant());
	  	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
		 String strDate = format.format(registerTimeBeginDate+" "+"00:00");
	     String endDate1 = format.format(registerTimeBeginDate1+" "+"23:59");
	     Date endDate =format.parse(endDate1);

	     Constant.LOGGER.info("strDate"+strDate);
	     Date startDate =format.parse(strDate);
	     Constant.LOGGER.info("time"+startDate);
	 
	 	 Aggregation agg1 = newAggregation( 
	     			match(Criteria.where("plannedStartDateTime").gte(startDate).lte(endDate).andOperator(Criteria.where("activityConfigurationType").is("UNPLANNED").andOperator(Criteria.where("resource.id").is(userid)))),
	                group("activityConfigurationType").count().as("totalUnPlannedActivity"),
	                project("totalUnPlannedActivity").and("activityConfigurationType").previousOperation(),
	                sort(Sort.Direction.DESC,"totalUnPlannedActivity") 
	               );
	      
	 	  AggregationResults<PlannedActivityCount> groupResults = mongoTemplate.aggregate(agg1,Activity.class,PlannedActivityCount.class );
	      List<PlannedActivityCount> result = groupResults.getMappedResults();
	      Constant.LOGGER.info("Aggregate result"+agg1);
	      watch.stop();
	     
	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
	      return result;
	
	      
	}

    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ActivityMap> getactivitymap(String schemaName,String userid,String activitytype,String startdate, String enddate) throws ParseException{
		 Constant.LOGGER.info("Inside get getactivitymap() ActivityFilterService.java");
	 	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     StopWatch watch = new StopWatch();
	     watch.start();
	     Collection type = new ArrayList();
	     type.add("POSTPONED");
	     type.add("OPEN");
	     type.add("CANCELLED");
	     type.add("COMPLETED");
	   
	     DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
         Constant.LOGGER.info("startdate"+startdate);
         Date startDate = format.parse(startdate+" "+"00:00");
         Date endDate = format.parse(enddate+" "+"23:59");
         
	     Aggregation agg1 = newAggregation( 
	     			match(Criteria.where("activityStatus").in(type).andOperator(
	     					Criteria.where("resource.id").is(userid),
	     					Criteria.where("activityType.id").is(activitytype),
	     					Criteria.where("actualStartDateTime").gte(startDate).lte(endDate))
	     					),
	     			group("actualStartDateTime","activityStatus").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 10)).as("actualStartDateTime"),
	     			group("activityStatus","actualStartDateTime").count().as("totalcount"),
	     			project("totalcount").and("activityStatus").previousOperation(),
	     		    sort(Sort.Direction.DESC,"totalcount"), 
	     		    sort(Sort.Direction.ASC,"activityStatus.actualStartDateTime")
	     			);
	      AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
	      List<ActivityMap> result = groupResults.getMappedResults();
	      Constant.LOGGER.info("Aggregate result"+agg1);
	      watch.stop();
	     
	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
	      return result;
	      
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ActivityMap> getactivitymap1(String schemaName, DashboardFilter dash) throws ParseException{
	     
		 Constant.LOGGER.info("Inside get getactivitymap1() ActivityFilterService.java");
	 	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     StopWatch watch = new StopWatch();
	     watch.start();
	     Collection type = new ArrayList();
	     type.add("POSTPONED");
	     type.add("OPEN");
	     type.add("CANCELLED");
	     type.add("COMPLETED");
         List<String> userid = dash.getUsers().stream().map(user-> user.getId()).collect(Collectors.toList());
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
         Constant.LOGGER.info("startdate"+dash.getStartDate());
         Date startDate = format.parse(dash.getStartDate()+" "+"00:00");
         Date endDate =format.parse(dash.getEndDate()+" "+"23:59");
      
         LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
         localDateTime=localDateTime.plusHours(1);
         Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
      
         Constant.LOGGER.info("######startdate"+startDate);
         Constant.LOGGER.info("######enddate"+currentDatePlusOneDay);
  

	     Aggregation agg1 = newAggregation( 
	    		 match(Criteria.where("activityStatus").in(type).andOperator(
	     					(Criteria.where("resource.id").in(userid)),
	     				     Criteria.where("activityType.id").is(dash.getActivityId()),
	     					 Criteria.where("actualStartDateTime").gte(startDate),
	     					 Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
	     					),
	     			group("actualStartDateTime","activityStatus").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 10)).as("actualStartDateTime"),
	     			group("activityStatus","actualStartDateTime").count().as("totalcount"),
	     			project("totalcount").and("activityStatus").previousOperation(),
	     		    sort(Sort.Direction.DESC,"totalcount"), 
	     		    sort(Sort.Direction.ASC,"activityStatus.actualStartDateTime")
	     			);
	      
	      AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
	      List<ActivityMap> result = groupResults.getMappedResults();
	      
	      Constant.LOGGER.info("Aggregate result"+agg1);
	      watch.stop();
	     
	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
	      return result;
	}
	

	public List<ActivityCompare> compared(String schemaName,CompareActivity act) throws ParseException{
		 Constant.LOGGER.info("Inside get compared() ActivityFilterService.java");
	 	 
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     StopWatch watch = new StopWatch();
	     watch.start();
         List<ActivityCompare> result =null;
         List<String> userid = act.getUsers().stream().map(user-> user.getId()).collect(Collectors.toList());
         String b=act.getActivityId();
         List<String> customers = act.getCustomers().stream().map(customer-> customer.getId()).collect(Collectors.toList());
        // Date s=act.getStartDate();
       //  Date e=act.getEndDate();
         //Constant.LOGGER.info(""+s); 
         DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
   		 Date s = format.parse(act.getStartDate()+" "+"00:00");
   		 Date e = format.parse(act.getEndDate()+" "+"23:59");
   		 
         LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
         localDateTime=localDateTime.plusHours(1);
         Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
         Constant.LOGGER.info("StartDate"+s);
         Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
         Aggregation agg1 = newAggregation( 
	    		 match((Criteria.where("resource.id").in(userid)).andOperator(
	    				 Criteria.where("customer.id").in(customers),
	    				 Criteria.where("activityType.id").is(b),
	     				 Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
	     			group("actualStartDateTime","customer.id").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 10)).as("actualStartDateTime"),
	     			group("actualStartDateTime").count().as("totalcount"),
	     			project("totalcount").and("dates").previousOperation(),
	     		    sort(Sort.Direction.DESC,"totalcount"),
	     		    sort(Sort.Direction.ASC,"dates")
	     			);
	           
	            
	      AggregationResults<ActivityCompare> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompare.class);
	      result = groupResults.getMappedResults();
	      Constant.LOGGER.info("Aggregate result"+agg1);
	      watch.stop();
	     
	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
	     return result;
	     
	}
	
	
	public List<ActivityCompareRatio> comparedRatio(String schemaName,CompareActivityRatio act) throws ParseException{
		
		Constant.LOGGER.info("Inside get comparedRatio() ActivityFilterService.java");
	 	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    StopWatch watch = new StopWatch();
	    watch.start();
        List<ActivityCompareRatio> result =null;
        List<String> userid = act.getUserId().stream().map(user-> user.getId()).collect(Collectors.toList());
        String acta=act.getActivityId();
      
        //Date s=act.getStartDate();
       // Date e=act.getEndDate();
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
  		 Date s = format.parse(act.getStartDate()+" "+"00:00");
  		 Date e = format.parse(act.getEndDate()+" "+"23:59");
  		
        
        LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusHours(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        Constant.LOGGER.info("StartDate"+s);
        Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
             Aggregation agg1 = newAggregation( 
	    	 match((Criteria.where("resource.id").in(userid)).andOperator(
	    			Criteria.where("activityType.id").is(acta),
	    		    Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
		     group("actualStartDateTime").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 10)).as("actualStartDateTime"),
	         group("actualStartDateTime").count().as("totalcount"),
	         project("totalcount").and("actualStartDateTime").previousOperation(),
	         sort(Sort.Direction.DESC,"totalcount"),
	         sort(Sort.Direction.ASC,"actualStartDateTime")
       );
	     
	      AggregationResults<ActivityCompareRatio> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompareRatio.class);
	      result = groupResults.getMappedResults();
	      Constant.LOGGER.info("Aggregate result"+agg1);
	      watch.stop();
	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
	      return result;
	     
	}



	// Annually Activities count based on Financial Year
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ActivityMap> getactivitymapyearly(String schemaName, DashboardFilter dash) throws ParseException {
				 
	Constant.LOGGER.info("Inside get getactivitymapyearly() ActivityFilterService.java Aggregates based on Financial year");
    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    StopWatch watch = new StopWatch();
		Collection type = new ArrayList();
	    type.add("POSTPONED");
	    type.add("OPEN");
	    type.add("CANCELLED");
	    type.add("COMPLETED");
        List<String> userid = dash.getUsers().stream().map(user-> user.getId()).collect(Collectors.toList());

			  final List<ActivityMap> annually = new ArrayList<ActivityMap>();

			     
			   List<ActivityMap> result=null;
		  
			   DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
		      Constant.LOGGER.info("startdate"+dash.getStartDate()+" "+"00:00");
		      Date startDate = format.parse(dash.getStartDate()+" "+"00:00");
		      Date endDate =format.parse(dash.getEndDate()+" "+"23:59");
		      Constant.LOGGER.info("before plus 1 day"+endDate);
		      LocalDateTime edate = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		      edate=edate.plusHours(1);
		      LocalDateTime localDateTimesd = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		    
		      for (LocalDateTime date = localDateTimesd; date.isBefore(edate); date =  date.plusYears(1)) {
		      watch.start();
		    
		      Constant.LOGGER.info("######startdate"+date);
		      Constant.LOGGER.info("######enddate"+date.plusYears(1));

		   	     Aggregation agg1 = newAggregation( 
		   	    		 match(Criteria.where("activityStatus").in(type).andOperator(
		   	     					(Criteria.where("resource.id").in(userid)),
		   	     				     Criteria.where("activityType.id").is(dash.getActivityId()),
		   	     					 Criteria.where("actualStartDateTime").gte(date),
		   	     					 Criteria.where("actualStartDateTime").lte(date.plusYears(1)))
		   	     					),
		   	     			group("actualStartDateTime","activityStatus").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 4)).as("actualStartDateTime"),
		   	     			group("activityStatus","actualStartDateTime").count().as("totalcount"),
		   	     			project("totalcount").and("activityStatus").previousOperation(),
		   	     		    sort(Sort.Direction.DESC,"totalcount"), 
		   	     		    sort(Sort.Direction.ASC,"activityStatus.actualStartDateTime")
		   	     			);
		   	      
		   	      AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
		   	      result = groupResults.getMappedResults();
		   	      
		   	      Constant.LOGGER.info("Aggregate result"+agg1);
		   	     
		   	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
		   	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
		   	   
		   	      annually.addAll(result);
			      watch.stop();


		   	     }
				

		      return annually;
			}
			
			
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ActivityMap> getactivitymapmonthly(String schemaName, DashboardFilter dash) throws ParseException {
		 
		Constant.LOGGER.info("Inside get getactivitymapmonthly() ActivityFilterService.java Aggregates based on year");
	 	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     StopWatch watch = new StopWatch();
	     watch.start();
	     Collection type = new ArrayList();
	     type.add("POSTPONED");
	     type.add("OPEN");
	     type.add("CANCELLED");
	     type.add("COMPLETED");
	    List<String> userid = dash.getUsers().stream().map(user-> user.getId()).collect(Collectors.toList());

	    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
         Constant.LOGGER.info("startdate"+dash.getStartDate()+" "+"00:00");
         Date startDate = format.parse(dash.getStartDate()+" "+"00:00");
         Date endDate =format.parse(dash.getEndDate()+" "+"23:59");
         
         LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
         localDateTime=localDateTime.plusHours(1);
         Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
      
         Constant.LOGGER.info("######startdate"+startDate);
         Constant.LOGGER.info("######enddate"+currentDatePlusOneDay);
  

	     Aggregation agg1 = newAggregation( 
	    		 match(Criteria.where("activityStatus").in(type).andOperator(
	     					(Criteria.where("resource.id").in(userid)),
	     				     Criteria.where("activityType.id").is(dash.getActivityId()),
	     					 Criteria.where("actualStartDateTime").gte(startDate),
	     					 Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
	     					),
	     			group("actualStartDateTime","activityStatus").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 7)).as("actualStartDateTime"),
	     			group("activityStatus","actualStartDateTime").count().as("totalcount"),
	     			project("totalcount").and("activityStatus").previousOperation(),
	     		    sort(Sort.Direction.DESC,"totalcount"), 
	     		    sort(Sort.Direction.ASC,"activityStatus.actualStartDateTime")
	     			);
	      
	      AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
	      List<ActivityMap> result = groupResults.getMappedResults();
	      
	      Constant.LOGGER.info("Aggregate result"+agg1);
	      watch.stop();
	     
	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
	      return result;
	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<ActivityMap> getactivitymapquarterly(String schemaName, DashboardFilter dash) throws ParseException {
		 
		Constant.LOGGER.info("Inside get getactivitymapquarterly() ActivityFilterService.java Aggregates based on year");
	 	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     StopWatch watch = new StopWatch();
	     watch.start();
	     Collection type = new ArrayList();
	     type.add("POSTPONED");
	     type.add("OPEN");
	     type.add("CANCELLED");
	     type.add("COMPLETED");
		  List<String> userid = dash.getUsers().stream().map(user-> user.getId()).collect(Collectors.toList());

		  DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
         Constant.LOGGER.info("startdate"+dash.getStartDate()+" "+"00:00");
         Date startDate = format.parse(dash.getStartDate()+" "+"00:00");
         Date endDate =format.parse(dash.getEndDate()+" "+"23:59");
         LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
         localDateTime=localDateTime.plusHours(1);
         Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
      
         Constant.LOGGER.info("######startdate"+startDate);
         Constant.LOGGER.info("######enddate"+currentDatePlusOneDay);
  

	     Aggregation agg1 = newAggregation( 
	    		 match(Criteria.where("activityStatus").in(type).andOperator(
	     					(Criteria.where("resource.id").in(userid)),
	     				     Criteria.where("activityType.id").is(dash.getActivityId()),
	     					 Criteria.where("actualStartDateTime").gte(startDate),
	     					 Criteria.where("actualStartDateTime").lte(currentDatePlusOneDay))
	     					),
	     			group("actualStartDateTime","activityStatus").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 7)).as("actualStartDateTime"),
	     			group("activityStatus","actualStartDateTime").count().as("totalcount"),
	     			project("totalcount").and("activityStatus").previousOperation(),
	     		    sort(Sort.Direction.DESC,"totalcount"), 
	     		    sort(Sort.Direction.ASC,"activityStatus.actualStartDateTime")
	     			);
	      
	      AggregationResults<ActivityMap> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityMap.class);
	      List<ActivityMap> result = groupResults.getMappedResults();
	      
	      Constant.LOGGER.info("Aggregate result"+agg1);
	      watch.stop();
	     
	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
	      return result;
	
	}



//	public List<ActivityCompare> comparedyearly(String schemaName, CompareActivity act) {	
//   Constant.LOGGER.info("Inside get comparedyearly() ActivityFilterService.java");
//	 	 
//		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//	     StopWatch watch = new StopWatch();
//	     watch.start();
//         List<ActivityCompare> result =null;
//         String a=act.getUserId();
//         String b=act.getActivityId();
//         List<String> c =act.getCustomerId();
//         Date s=act.getStartDate();
//         Date e=act.getEndDate();
//         Constant.LOGGER.info(""+s); 
//         LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//         localDateTime=localDateTime.plusDays(1);
//         Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//         Constant.LOGGER.info("StartDate"+s);
//         Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
//         Aggregation agg1 = newAggregation( 
//	    		 match((Criteria.where("resource.id").is(a)).andOperator(
//	    				 Criteria.where("customer.id").in(c),
//	    				 Criteria.where("activityType.id").is(b),
//	     				 Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
//	     			group("actualStartDateTime","customer.id").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 4)).as("actualStartDateTime"),
//	     			group("actualStartDateTime").count().as("totalcount"),
//	     			project("totalcount").and("dates").previousOperation(),
//	     		    sort(Sort.Direction.DESC,"totalcount"),
//	     		    sort(Sort.Direction.ASC,"dates")
//	     			);
//	           
//	            
//	      AggregationResults<ActivityCompare> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompare.class);
//	      result = groupResults.getMappedResults();
//	      Constant.LOGGER.info("Aggregate result"+agg1);
//	      watch.stop();
//	     
//	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
//	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
//	     return result;
//	}
//
//


//	public List<ActivityCompare> comparedmonthly(String schemaName, CompareActivity act) {
//		  Constant.LOGGER.info("Inside get comparedmonthly() ActivityFilterService.java");
//		 	 
//			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		     StopWatch watch = new StopWatch();
//		     watch.start();
//	         List<ActivityCompare> result =null;
//	         String a=act.getUserId();
//	         String b=act.getActivityId();
//	         List<String> c =act.getCustomerId();
//	         Date s=act.getStartDate();
//	         Date e=act.getEndDate();
//	         Constant.LOGGER.info(""+s); 
//	         LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//	         localDateTime=localDateTime.plusDays(1);
//	         Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//	         Constant.LOGGER.info("StartDate"+s);
//	         Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
//	         Aggregation agg1 = newAggregation( 
//		    		 match((Criteria.where("resource.id").is(a)).andOperator(
//		    				 Criteria.where("customer.id").in(c),
//		    				 Criteria.where("activityType.id").is(b),
//		     				 Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
//		     			group("actualStartDateTime","customer.id").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 7)).as("actualStartDateTime"),
//		     			group("actualStartDateTime").count().as("totalcount"),
//		     			project("totalcount").and("dates").previousOperation(),
//		     		    sort(Sort.Direction.DESC,"totalcount"),
//		     		    sort(Sort.Direction.ASC,"dates")
//		     			);
//		           
//		            
//		      AggregationResults<ActivityCompare> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompare.class);
//		      result = groupResults.getMappedResults();
//		      Constant.LOGGER.info("Aggregate result"+agg1);
//		      watch.stop();
//		     
//		      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
//8		      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
//		     return result;
//	
//	}
//	
//	public List<ActivityCompare> comparedquarterly(String schemaName, CompareActivity act) {
//		  Constant.LOGGER.info("Inside get comparedquarterly() ActivityFilterService.java");
//		 	 
//			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		     StopWatch watch = new StopWatch();
//		     watch.start();
//	         List<ActivityCompare> result =null;
//	         String a=act.getUserId();
//	         String b=act.getActivityId();
//	         List<String> c =act.getCustomerId();
//	         Date s=act.getStartDate();
//	         Date e=act.getEndDate();
//	         Constant.LOGGER.info(""+s); 
//	         LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//	         localDateTime=localDateTime.plusDays(1);
//	         Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//	         Constant.LOGGER.info("StartDate"+s);
//	         Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
//	         Aggregation agg1 = newAggregation( 
//		    		 match((Criteria.where("resource.id").is(a)).andOperator(
//		    				 Criteria.where("customer.id").in(c),
//		    				 Criteria.where("activityType.id").is(b),
//		     				 Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
//		     			group("actualStartDateTime","customer.id").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 7)).as("actualStartDateTime"),
//		     			group("actualStartDateTime").count().as("totalcount"),
//		     			project("totalcount").and("dates").previousOperation(),
//		     		    sort(Sort.Direction.DESC,"totalcount"),
//		     		    sort(Sort.Direction.ASC,"dates")
//		     			);
//		           
//		            
//		      AggregationResults<ActivityCompare> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompare.class);
//		      result = groupResults.getMappedResults();
//		      Constant.LOGGER.info("Aggregate result"+agg1);
//		      watch.stop();
//		     
//		      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
//		      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
//		     return result;
//	
//	}
//
//
//
//
//	public List<ActivityCompareRatio> comparedRatioYearly(String schemaName, CompareActivityRatio act) {
//		Constant.LOGGER.info("Inside get comparedRatioYearly() ActivityFilterService.java");
//	 	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//	    StopWatch watch = new StopWatch();
//	    watch.start();
//        List<ActivityCompareRatio> result =null;
//        String a=act.getUserId();
//        String acta=act.getActivityId();
//      
//        Date s=act.getStartDate();
//        Date e=act.getEndDate();
//        LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//        localDateTime=localDateTime.plusDays(1);
//        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//        Constant.LOGGER.info("StartDate"+s);
//        Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
//             Aggregation agg1 = newAggregation( 
//	    	 match((Criteria.where("resource.id").is(a)).andOperator(
//	    			Criteria.where("activityType.id").is(acta),
//	    		    Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
//		     group("actualStartDateTime").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 4)).as("actualStartDateTime"),
//	         group("actualStartDateTime").count().as("totalcount"),
//	         project("totalcount").and("actualStartDateTime").previousOperation(),
//	         sort(Sort.Direction.DESC,"totalcount"),
//	         sort(Sort.Direction.ASC,"actualStartDateTime")
//       );
//	     
//	      AggregationResults<ActivityCompareRatio> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompareRatio.class);
//	      result = groupResults.getMappedResults();
//	      Constant.LOGGER.info("Aggregate result"+agg1);
//	      watch.stop();
//	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
//	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
//	      return result;
//	     
//	}
//	
//	public List<ActivityCompareRatio> comparedRatioMonthly(String schemaName, CompareActivityRatio act) {
//		Constant.LOGGER.info("Inside get comparedRatioMonthly() ActivityFilterService.java");
//	 	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//	    StopWatch watch = new StopWatch();
//	    watch.start();
//        List<ActivityCompareRatio> result =null;
//        String a=act.getUserId();
//        String acta=act.getActivityId();
//      
//        Date s=act.getStartDate();
//        Date e=act.getEndDate();
//        LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//        localDateTime=localDateTime.plusDays(1);
//        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//        Constant.LOGGER.info("StartDate"+s);
//        Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
//             Aggregation agg1 = newAggregation( 
//	    	 match((Criteria.where("resource.id").is(a)).andOperator(
//	    			Criteria.where("activityType.id").is(acta),
//	    		    Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
//		     group("actualStartDateTime").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 7)).as("actualStartDateTime"),
//	         group("actualStartDateTime").count().as("totalcount"),
//	         project("totalcount").and("actualStartDateTime").previousOperation(),
//	         sort(Sort.Direction.DESC,"totalcount"),
//	         sort(Sort.Direction.ASC,"actualStartDateTime")
//       );
//	     
//	      AggregationResults<ActivityCompareRatio> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompareRatio.class);
//	      result = groupResults.getMappedResults();
//	      Constant.LOGGER.info("Aggregate result"+agg1);
//	      watch.stop();
//	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
//	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
//	      return result;
//	     
//	}
//
//	public List<ActivityCompareRatio> comparedRatioQuarterly(String schemaName, CompareActivityRatio act) {
//		Constant.LOGGER.info("Inside get comparedRatioQuarterly() ActivityFilterService.java");
//	 	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//	    StopWatch watch = new StopWatch();
//	    watch.start();
//        List<ActivityCompareRatio> result =null;
//        String a=act.getUserId();
//        String acta=act.getActivityId();
//      
//        Date s=act.getStartDate();
//        Date e=act.getEndDate();
//        LocalDateTime localDateTime = e.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//        localDateTime=localDateTime.plusDays(1);
//        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
//        Constant.LOGGER.info("StartDate"+s);
//        Constant.LOGGER.info("Endate"+currentDatePlusOneDay);
//             Aggregation agg1 = newAggregation( 
//	    	 match((Criteria.where("resource.id").is(a)).andOperator(
//	    			Criteria.where("activityType.id").is(acta),
//	    		    Criteria.where("actualStartDateTime").gte(s).lte(currentDatePlusOneDay))),
//		     group("actualStartDateTime").first(StringOperators.Substr.valueOf("actualStartDateTime").substring(0, 7)).as("actualStartDateTime"),
//	         group("actualStartDateTime").count().as("totalcount"),
//	         project("totalcount").and("actualStartDateTime").previousOperation(),
//	         sort(Sort.Direction.DESC,"totalcount"),
//	         sort(Sort.Direction.ASC,"actualStartDateTime")
//       );
//	     
//	      AggregationResults<ActivityCompareRatio> groupResults = mongoTemplate.aggregate(agg1,Activity.class,ActivityCompareRatio.class);
//	      result = groupResults.getMappedResults();
//	      Constant.LOGGER.info("Aggregate result"+agg1);
//	      watch.stop();
//	      Constant.LOGGER.info("Time Elapsed TimeMillis(): " + watch.getTotalTimeMillis());
//	      Constant.LOGGER.info("Time Elapsed TimeSeconds(): " + watch.getTotalTimeSeconds());
//	      return result;
//	     
//	}
//
	
}
		
