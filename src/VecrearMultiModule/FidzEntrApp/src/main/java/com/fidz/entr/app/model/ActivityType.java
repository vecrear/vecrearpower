package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.UserRole;

import lombok.Data;

@Data
@Document(collection = "activitytype")
@JsonIgnoreProperties(value = { "target" })
public class ActivityType extends Base {
	@Id
	protected String id;
	
	@NotNull
	@NotBlank
	protected String name;
	

	@DBRef(lazy = true)
	protected Company company;
	
	@NotNull
	@DBRef(lazy = true)
	protected Department department;
	
    protected int sortOrder; //sort order
    
    protected boolean editable; //Will the fields be editable?

    @NotNull
	@DBRef(lazy = true)
	protected List<ActivityField> activityFields;
    
    @DBRef(lazy = true)
	protected List<UserRole> mappedUserRoles; //mapped to specific user roles
	
	protected boolean mapToAllUserRoles; //mapped to all user roles
	
	protected ActivityPerformedFrequency activityPerformedFrequency;
	
	protected TimeZone timeZone; //Time given below is specific to the time zone specified.
	
	protected String performBeforeThisTime; //Time as a String
	
	protected String performAfterThisTime; //Time as a String
	
	protected WeekDay performOnThisWeekDay;
	
	protected int performOnThisDateOfMonth; //1 - 31
	
	protected List<ActivityTarget> activityTargets;
	
	@DBRef(lazy = true)
	protected List<Product> products;
	
	/**
	 * enableMultipleEntry - boolean
	 * Single Entry or Multiple Entry for Activity Log
	 * true means Multiple Entry
	 * false means Single Entry (Default)
	 */
	
	protected boolean enableMultipleEntry = false;

	
	
		
	

	

	

	



	

	
	

	
}
