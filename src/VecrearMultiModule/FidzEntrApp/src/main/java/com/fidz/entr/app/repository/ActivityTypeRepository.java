package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.ActivityType;
@Repository("feaactivitytypesrepository")
public interface ActivityTypeRepository extends MongoRepository<ActivityType, String>{
	//Mono<ActivityType> findByName(String name);
	ActivityType findByName(String name);
}
