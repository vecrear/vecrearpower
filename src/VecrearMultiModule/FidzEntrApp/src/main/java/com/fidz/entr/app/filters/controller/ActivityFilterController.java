package com.fidz.entr.app.filters.controller;
import java.text.ParseException;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.ActivityCompare;
import com.fidz.entr.app.filters.model.ActivityCompareRatio;
import com.fidz.entr.app.filters.model.ActivityMap;
import com.fidz.entr.app.filters.model.CompareActivity;
import com.fidz.entr.app.filters.model.CompareActivityRatio;
import com.fidz.entr.app.filters.model.DashboardFilter;
import com.fidz.entr.app.filters.model.PlannedActivityCount;
import com.fidz.entr.app.filters.model.UnPlannedActivityCount;
import com.fidz.entr.app.filters.service.ActivityFilterService;



@CrossOrigin(maxAge = 3600)
@RestController("FEAActivityFilterController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/filter")
public class ActivityFilterController{
	
	@Autowired
	ActivityFilterService activityFilterService;
	
	@Value("${dashboardservices.enabled}")
	private Boolean property;
	
	@PostMapping("/activity/unplanned/id/{id}")
    public List<UnPlannedActivityCount> getplannedActivityIncount(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="id")String userid) throws ParseException {
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.getplannedActivitycount(schemaName, userid);
    }
  
	
	@PostMapping("/activity/planned/id/{id}")
    public List<PlannedActivityCount> getunplannedActivityIncount(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="id")String userid) throws ParseException {
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.getunplannedActivitycount(schemaName, userid);
	}
	

	@PostMapping("/activity/map/id/{id}/type/{activitytype}/sdate={startdate}|edate={enddate}")
    public List<ActivityMap> getactivitymap(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="id")String userid,@PathVariable(value="activitytype")String activitytype,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate) throws ParseException{
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.getactivitymap(schemaName,userid,activitytype,startdate,enddate);
    }
	
	@PostMapping("/activity/daily")
    public List<ActivityMap> getactivitymap1(@RequestHeader(value="schemaName")String schemaName, @RequestBody DashboardFilter dash) throws ParseException{
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.getactivitymap1(schemaName,dash);
    }
	
	@PostMapping("/activity/compare")
     public List<ActivityCompare> compared(@RequestBody CompareActivity act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.compared(schemaName,act);
	}
	
	@PostMapping("/activity/compare/ratio")
    public List<ActivityCompareRatio> comparedRatio(@RequestBody CompareActivityRatio act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.comparedRatio(schemaName,act);
	}
	
	
	// Annually Activities count based on Financial Year
	@PostMapping("/activity/annually")
    public List<ActivityMap> getactivitymapyearly(@RequestHeader(value="schemaName")String schemaName, @RequestBody DashboardFilter dash) throws ParseException{
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.getactivitymapyearly(schemaName,dash);
	 }

	@PostMapping("/activity/monthly")
    public List<ActivityMap> getactivitymapmonthly(@RequestHeader(value="schemaName")String schemaName,@RequestBody DashboardFilter dash) throws ParseException{
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.getactivitymapmonthly(schemaName,dash);
    }
	
	@PostMapping("activity/quarterly")
    public List<ActivityMap> getactivitymapquarterly(@RequestHeader(value="schemaName")String schemaName,@RequestBody DashboardFilter dash) throws ParseException{
		System.out.println("property value******"+property);
		String message="Dashboardservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityFilterService.getactivitymapquarterly(schemaName,dash);
    }
//	@PostMapping("/activity/compare/annually")
//    public List<ActivityCompare> comparedyearly(@RequestBody CompareActivity act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
//		System.out.println("property value******"+property);
//		String message="Dashboardservices are disabled";
//		if(!property) 
//		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//		return activityFilterService.comparedyearly(schemaName,act);
//	}
//	
//	@PostMapping("/activity/compare/monthly")
//    public List<ActivityCompare> comparedmonthly(@RequestBody CompareActivity act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
//		System.out.println("property value******"+property);
//		String message="Dashboardservices are disabled";
//		if(!property) 
//		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//		return activityFilterService.comparedmonthly(schemaName,act);
//	}
//	
//	@PostMapping("/activity/compare/quarterly")
//    public List<ActivityCompare> comparedquarterly(@RequestBody CompareActivity act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
//		System.out.println("property value******"+property);
//		String message="Dashboardservices are disabled";
//		if(!property) 
//		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//		return activityFilterService.comparedquarterly(schemaName,act);
//	}
//	
//	@PostMapping("/activity/compare/ratio/annually")
//    public List<ActivityCompareRatio> comparedRatioYearly(@RequestBody CompareActivityRatio act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
//		System.out.println("property value******"+property);
//		String message="Dashboardservices are disabled";
//		if(!property) 
//		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//		return activityFilterService.comparedRatioYearly(schemaName,act);
//	}
//	
//	@PostMapping("/activity/compare/ratio/monthly")
//    public List<ActivityCompareRatio> comparedRatioMonthly(@RequestBody CompareActivityRatio act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
//		System.out.println("property value******"+property);
//		String message="Dashboardservices are disabled";
//		if(!property) 
//		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//		return activityFilterService.comparedRatioMonthly(schemaName,act);
//	}
//	
//	@PostMapping("/activity/compare/ratio/quarterly")
//    public List<ActivityCompareRatio> comparedRatioQuarterly(@RequestBody CompareActivityRatio act,@RequestHeader(value="schemaName")String schemaName) throws ParseException{
//		System.out.println("property value******"+property);
//		String message="Dashboardservices are disabled";
//		if(!property) 
//		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//		return activityFilterService.comparedRatioQuarterly(schemaName,act);
//	}
}
