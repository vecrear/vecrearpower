package com.fidz.entr.app.reportattendance;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class PhoneNumber {
	protected String primary;
	protected String primaryPhExtn;
	
	protected String secondary;
	protected String secondaryPhExtn;
	
	public PhoneNumber(String primary, String primaryPhExtn, String secondary, String secondaryPhExtn) {
		super();
		this.primary = primary;
		this.primaryPhExtn = primaryPhExtn;
		this.secondary = secondary;
		this.secondaryPhExtn = secondaryPhExtn;
	}

	@Override
	public String toString() {
		return "PhoneNumber [primary=" + primary + ", primaryPhExtn=" + primaryPhExtn + ", secondary=" + secondary
				+ ", secondaryPhExtn=" + secondaryPhExtn + "]";
	}
	
}
