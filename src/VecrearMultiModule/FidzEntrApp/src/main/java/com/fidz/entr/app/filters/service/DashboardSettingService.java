package com.fidz.entr.app.filters.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.DashboardSetting;
import com.fidz.entr.app.filters.model.DisplayType;
import com.fidz.entr.app.filters.model.Fperiod;
import com.fidz.entr.app.filters.model.WeekName;
import com.fidz.entr.app.filters.repository.DasboardSettingRepository;


@Service("FEADashboardSettingService")
public class DashboardSettingService {
	
    private GenericAppINterface<DashboardSettingService> genericINterface;
	
    @Autowired
	public DashboardSettingService(GenericAppINterface<DashboardSettingService> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	
    @Autowired
	DasboardSettingRepository dasboardSettingRepository;
	
	
    public DashboardSetting create(String schemaName,DashboardSetting dboard) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside create() DashboardSettingService.java");
		DashboardSetting board =null;
		try {
			board= this.dasboardSettingRepository.save(dboard);
			
		}catch(Exception ex) {
		Constant.LOGGER.error("Error while creating in create() DashboardSettingService.java"+ex.getMessage());	
		}
		Constant.LOGGER.info("successfull creation of DashboardSetting create() DashboardSettingService.java");
		return board;
	 }

	
    
    
    
   public DashboardSetting updateDashboardSetting(String schemaName,String id, @Valid DashboardSetting dboard) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside updateDashboardSetting() DashboardSettingService.java");
		dboard.setId(id);
	    Constant.LOGGER.info("successfull updation of DashboardSetting DashboardSettingService.java");
    	return this.dasboardSettingRepository.save(dboard);

     }


	
   public List<DashboardSetting> getall(String schemaName) {
	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	Constant.LOGGER.info("Inside getall() DashboardSettingService.java");
    List<DashboardSetting> dboard = null;
    List<DashboardSetting> dbs =null;
     DashboardSetting dboards = new DashboardSetting();
     DisplayType dType = new DisplayType();
    try {
    	 dboard=this.genericINterface.findAll(DashboardSetting.class);
    	 if(dboard.isEmpty() == true) {
    		 Constant.LOGGER.info("inside if condition");
    		 dboards.setId(dboards.getId());
             dboards.setFinperiod(Fperiod.A);
             dboards.setStartofweek(WeekName.Mon);
             dType.setAnnually(true);
             dType.setDaily(true);
             dType.setForenight(true);
             dType.setMonthly(true);
             dType.setQuarterly(true);
             dType.setWeekly(true);
             dboards.setSelection(dType);
             dboards.setGrouping(true);
             dboards.setLegend(true);
             this.dasboardSettingRepository.save(dboards);
              
             
    	 }
    	 
    	 dbs = this.genericINterface.findAll(DashboardSetting.class);	 
    }catch(Exception ex) {
    	 Constant.LOGGER.error("Error while fetching getall() DashboardSettingService.java "+ex.getMessage());
    }
    
    Constant.LOGGER.info("after successfull fetching of data getall() DashboardSettingService.java");
	return dbs;
	}





public DashboardSetting getById(String schemaName, String id) {
MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
Constant.LOGGER.info("Inside getById()  DashboardSettingService.java");
DashboardSetting dboard = null;
try {
	dboard=this.genericINterface.findById(id, DashboardSetting.class);
}catch(Exception ex) {
	Constant.LOGGER.error("Error while fetching getById() DashboardSettingService.java"+ex.getMessage()); 
}
Constant.LOGGER.info("after successfull fetching of data getById() DashboardSettingService.java");
	return dboard;
}
	
	
	
	
}
