package com.fidz.entr.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.model.TimeZoneInfo;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.repository.DepartmentRepository;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.repository.CompanyRepository;
import com.fidz.entr.base.service.CompanyService;



@Service("FEADepartmentService")
public class DepartmentService {
	private GenericAppINterface<Department> genericINterface;
	@Autowired
	public DepartmentService(GenericAppINterface<Department> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private DepartmentRepository departmentRepository;
	
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private static String name = "Department";
	
    public List<Department> getAllDepartments(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDepartments DepartmentService.java");
    	List<Department> departments=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	departments=this.genericINterface.findAll(Department.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllDepartments DepartmentService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllDepartments DepartmentService.java");
    	return departments;
    }
	
    public Department createDepartment(Department department) {
    	Department departments=null;
    	TimeZoneInfo  timeZoneInfo =  new TimeZoneInfo();
    		Constant.LOGGER.info(" Inside DepartmentService.java Value for insert Department record: createDepartment  :: " + department);
    		String schemaName=department.getSchemaName();
    	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	    
    	    //--------------------TO CHECK THE SAME IN SAME DEPT--------IN SAME COMPANY
    	    
    	    Query query = new Query();
        	List<Department> departmentsList = null;

        
        		query.addCriteria(Criteria.where("company.id").is(department.getCompany().getId()));
        	
        		departmentsList= this.genericINterface.find(query, Department.class);
        		List<String> names = null;
        		if(departmentsList.size() > 0) {
        			names = departmentsList.stream().map(p->p.getName()).collect(Collectors.toList());
            		Constant.LOGGER.info("Country List data"+names);
           	   	 
        	   	if (names.stream().anyMatch(s -> s.equals(department.getName())==true)) {
        	   			Constant.LOGGER.info("Inside if statement");

        				throw new NameFoundException(name+ExceptionConstant.SAME_NAME_SAME_DEPARTMENT+": "+department.getName());
        	   	 }else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(department.getName())==true)) {
        	   				Constant.LOGGER.info("Inside else if statement");
            			throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+department.getName());
        	   	}else{
        	   		 departments=this.departmentRepository.save(department);
        	    	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
        	    	 if(departments != null) {
        	    		 Constant.LOGGER.info("inside saving the timezoneInfo to superadmin");
                     try {
        	    	 timeZoneInfo.setId(departments.getId());
                     timeZoneInfo.setSchemaName(departments.getSchemaName());
                     timeZoneInfo.setDeptTimeZone(departments.getDeptTimeZone());
                     this.mongoTemplate.save(timeZoneInfo, "timezoneinfo");
        	    	 Constant.LOGGER.info("timezoneInfo saved to superadmin successfully");
                    }catch(Exception ex) {
            	    	 Constant.LOGGER.info("error while saving timezoneInfo  to superadmin" +ex.getMessage());

                     }
        	    	 }
        	    	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	    	 return departments;
        	    	    
        	   		}
        			
        		}else {
        			
        			departments=this.departmentRepository.save(department);
        			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
        	    	 if(departments != null) {
        	    		 Constant.LOGGER.info("inside saving the timezoneInfo to superadmin");
                     try {
        	    	 timeZoneInfo.setId(departments.getId());
                     timeZoneInfo.setSchemaName(departments.getSchemaName());
                     timeZoneInfo.setDeptTimeZone(departments.getDeptTimeZone());
                     this.mongoTemplate.save(timeZoneInfo, "timezoneinfo");
        	    	 Constant.LOGGER.info("timezoneInfo saved to superadmin successfully");
                    }catch(Exception ex) {
            	    	 Constant.LOGGER.info("error while saving timezoneInfo  to superadmin" +ex.getMessage());

                     }
        	    	 }
        	    	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        			return departments;
        		}
        		
        	//------------------------------------------------------------       

    }
    
    public void createDepartments(Department department) {
        String schemaName=department.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        this.genericINterface.saveName(department);
    }
    
    public Department getDepartmentById(String id, String schemaName) {
    	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	Constant.LOGGER.info("Inside getDepartmentById DepartmentService.java");

    	 return this.genericINterface.findById(id, Department.class);
		
    }
	
    public Department getDepartmentByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	Constant.LOGGER.info("Inside getDepartmentByName DepartmentService.java");

    	return this.departmentRepository.findByName(name);
		
    }
	
    public Department updateDepartment(String id, Department department) {
        String schemaName=department.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    String departId=department.getId();
	    Query query = new Query();
    	List<Department> departmentsList = null;
    	Department departments=null;
    	TimeZoneInfo  timeZoneInfo =  new TimeZoneInfo();


    
    		query.addCriteria(Criteria.where("company.id").is(department.getCompany().getId()));
    	
    		departmentsList= this.genericINterface.find(query, Department.class);
    		Department ctr = this.genericINterface.findById(id, Department.class);
    		departmentsList.remove(ctr);
    		List<String> names = null;
    		if(departmentsList.size() > 0) {
    			names = departmentsList.stream().map(p->p.getName()).collect(Collectors.toList());
        		Constant.LOGGER.info("Country List data"+names);
       	   	 
    	   	if (names.stream().anyMatch(s -> s.equals(department.getName())==true)) {
    	   			Constant.LOGGER.info("Inside if statement");

    				throw new NameFoundException(name+ExceptionConstant.SAME_NAME_SAME_DEPARTMENT+": "+department.getName());
    	   	 }else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(department.getName())==true)) {
    	   				Constant.LOGGER.info("Inside else if statement");
        			throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+department.getName());
    	   	}else{
    			
	    department.setId(id);
	    departments = this.departmentRepository.save(department);
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
   	 if(departments != null) {
   		 Constant.LOGGER.info("inside saving the timezoneInfo to superadmin");
        try {
   	    timeZoneInfo.setId(departments.getId());
        timeZoneInfo.setSchemaName(departments.getSchemaName());
        timeZoneInfo.setDeptTimeZone(departments.getDeptTimeZone());
        this.mongoTemplate.save(timeZoneInfo, "timezoneinfo");
   	 Constant.LOGGER.info("timezoneInfo saved to superadmin successfully");
       }catch(Exception ex) {
	    	 Constant.LOGGER.info("error while saving timezoneInfo  to superadmin" +ex.getMessage());

        }
   	 }
   	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	 return departments;

    		}
    		}
    		department.setId(id);
    	    departments = this.departmentRepository.save(department);
    	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
    	   	 if(departments != null) {
    	   		 Constant.LOGGER.info("inside saving the timezoneInfo to superadmin");
    	        try {
    	   	    timeZoneInfo.setId(departments.getId());
    	        timeZoneInfo.setSchemaName(departments.getSchemaName());
    	        timeZoneInfo.setDeptTimeZone(departments.getDeptTimeZone());
    	        this.mongoTemplate.save(timeZoneInfo, "timezoneinfo");
    	   	 Constant.LOGGER.info("timezoneInfo saved to superadmin successfully");
    	       }catch(Exception ex) {
    		    	 Constant.LOGGER.info("error while saving timezoneInfo  to superadmin" +ex.getMessage());

    	        }
    	   	 }
    	   	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	    return departments;
    	
    }

    public Stream<User> getAllUsersByDepartId(String departId, String schemaName) {
 		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	Constant.LOGGER.info("Inside getAllUsersByDepartId DepartmentService.java");

 		//List<User> users=(List<User>) this.userRepository.findAll().stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
 		Stream<User> users=this.genericINterface.findAll(User.class).stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
 		return users;
 		/*return this.genericINterface.findAll(Activity.class).filter(activity ->activity.getResource().getId().contains(userId) && activity.getActivityStatus().equals(ActivityStatus.valueOf("OPEN"))
 				&&  activity.getActivityConfigurationType().equals(ActivityConfigurationType.valueOf("PLANNED")));*/

 	}
  //hard delete
  	public Map<String, String> deleteDepartment(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	Constant.LOGGER.info("Inside deleteDepartment DepartmentService.java");

  		this.departmentRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Department deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftDepartment(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	Constant.LOGGER.info("Inside deleteSoftDepartment DepartmentService.java");

  		Department department = this.genericINterface.findById(id, Department.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		department.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		department.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		department.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(department);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Department deleted successfully");
  		return response;

  	}
    public List<Department> streamAllDepartments() {
        return departmentRepository.findAll();
    }
//TO get all department paginated
	public List<Department> getAllDepartmentsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllDepartmentsPaginated DepartmentService.java");
    	Query query = new Query();
    	List<Department> departments =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	departments= this.genericINterface.find(query, Department.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllDepartmentsPaginated DeviceDetailsService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllDepartmentsPaginated "+query);
        return departments;
	}

	public List<Department> getAllDepartmentsByCompany(String companyid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllDepartmentsByCompany DepartmentService.java");
    	Query query = new Query();
    	List<Department> departments =null;

    	try {
    		query.addCriteria(Criteria.where("company.id").in(companyid));
    	departments= this.genericINterface.find(query, Department.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllDepartmentsByCompany DeviceDetailsService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllDepartmentsByCompany "+query);
        return departments;
	}
	
	public List<TimeZoneInfo> getAllTimeZoneInfos(String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		Query query = new Query();
    	List<TimeZoneInfo> tInfos =null;
    	
    	try {
    		
    		query.addCriteria(Criteria.where("schemaName").in(schemaName));
    		tInfos = mongoTemplate.find(query, TimeZoneInfo.class);
    		
    	}catch(Exception ex) {
    	    Constant.LOGGER.error("Error occurred inside  getAllTimeZoneInfos"+ex.getMessage());

    	}
    	return tInfos;
	}

	public List<Department> departmentTextSearch(@Valid String text, String schemaName, int pageNumber, int pageSize) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<Department>  dep = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
       Constant.LOGGER.info("Inside departmentTextSearch CompanyService.java");
    	try {
    		
    		textIndex = new TextIndexDefinitionBuilder()
    				.onAllFields()
    				.build();
    		mongoTemplate.indexOps(Department.class).ensureIndex(textIndex);
    	    criteria = TextCriteria.forDefaultLanguage()
    	    		.matchingAny(text).caseSensitive(false).diacriticSensitive(false);
              query = TextQuery.queryText(criteria)
    	    		  .sortByScore()
    	    		  .with(PageRequest.of(pageNumber, pageSize));
              dep = mongoTemplate.find(query, Department.class);
    		
    					
	}catch(Exception ex) {
    	Constant.LOGGER.error("Inside departmentTextSearch CompanyService.java"+ ex.getMessage());

	}
		return dep;
	}

	public List<Department> getAllSearchDepartmentsByCompany(String companyid, String schemaName, @Valid String text) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllSearchDepartmentsByCompany DepartmentService.java");
    	Query query = new Query();
    	List<Department> departments =null;

    	try {
    		query.addCriteria(Criteria.where("company.id").in(companyid).andOperator(Criteria.where("name").regex(text,"i")));
    	departments= this.genericINterface.find(query, Department.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllSearchDepartmentsByCompany DeviceDetailsService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllSearchDepartmentsByCompany "+query);
        return departments;
	}

}
