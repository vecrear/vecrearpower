package com.fidz.entr.app;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ScheduledFuture;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.superadmin.model.SuperAdmin;
import com.fidz.entr.superadmin.service.SuperAdminService;
import com.fidz.services.authentication.security.EncriptionUtil;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(  basePackages = {"com.fidz.entr.app",  
		"com.fidz.entr.base", 
		"com.fidz.base", 
		"com.fidz.entr.superadmin", 
		"com.fidz.services.location", 
		"com.fidz.services.authentication"  }


		)
@EnableReactiveMongoRepositories
//@EnableCaching
@EnableScheduling
public class FidzEntrApplication{

	private static final String urlPrefix ="https://maps.googleapis.com/maps/api/geocode/json?";
	
	
	
//    @Value("${aws.access_key_id}")
//    private String accessKeyId = "AKIAW2CTFB4M3FG7VQIK";
//    // Secret access key will be read from the application.properties file during the application intialization.
//    @Value("${aws.secret_access_key}")
//    private String secretAccessKey = "DtYaab0D3bZOAID0AnrEdiAD3dDkUWSlkyuKSbdV";
//    // Region will be read from the application.properties file  during the application intialization.
//    @Value("${aws.s3.region}")
//    private String region = "ap-south-1";;
// 
//    @Bean
//    public AmazonS3 getAmazonS3Cient() {
//        final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
//        // Get AmazonS3 client and return the s3Client object.
//        return AmazonS3ClientBuilder
//                .standard()
//                .withRegion(Regions.fromName(region))
//                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
//                .build();
//    }

	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(FidzEntrApplication.class, args);
		System.out.println("------------------------------------------------This is vecrear Bangalore-------------------------------");
		
		System.out.println("yyyyyyyyyyyyyyyyyyyyyyyEncryption :: "+new EncriptionUtil().encrypt("1234", new EncriptionUtil().getSecurityKey("8088597794")));
		System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxdecyption  :: "+new EncriptionUtil().decrypt("vBOwepSWWbtbUzigUXVNGA==", new EncriptionUtil().getSecurityKey("8088597794")));
		System.out.println(java.time.Clock.systemUTC().instant());
//		String d = String.valueOf(java.time.Clock.systemUTC().instant());
//		   
//   	    DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH);
//   	    Date startDate = (Date)format.parse(d);
//   	   LocalDateTime localDateTime = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
//   	       
//   	   LocalDateTime minusminutes = localDateTime.minusMinutes(1);
//   	   String diff = String.valueOf(minusminutes);
//	        Date match =format.parse(diff);
//
//   	     Constant.LOGGER.info("minusminutes----"+match);
			String ss = reverseGeoCoding(12.91035611,77.59965067);
		
		
			String inputValue = "2021-02-17T12:07:18.445Z";
		    Instant timestamp = Instant.parse(inputValue);
		    System.out.println("---------timestamp------>"+timestamp);
		    ZonedDateTime losAngelesTime = timestamp.atZone(ZoneId.of("Europe/Luxembourg"));
		    System.out.println("--------Europe/Luxembourg------->"+losAngelesTime);
		
		    System.out.println("----------------------------------------------------------------------->");
		    
		    String inputValue1 = "2021-03-06T22:04:27.622Z";
		    Instant timestamp1 = Instant.parse(inputValue1);
		    System.out.println("---------timestamp1------>"+timestamp1);
		    ZonedDateTime losAngelesTime1 = timestamp1.atZone(ZoneId.of("Asia/Kolkata"));
		    System.out.println("--------Asia/Kolkata ------->"+losAngelesTime1);
		    
		    System.out.println("----------------------------------------------------------------------->");
		
		

}
	
	private static String reverseGeoCoding(double latitude, double longitude) {
		System.out.println("\nLatitude: " + latitude +"\nLongitude: " + longitude);
		String address = "";
		String status = "";
		String url = urlPrefix+"latlng="+latitude+","+longitude+"&key=AIzaSyDNMKPzkkKtjipjb3YIyXEEXh-Piq7qbbU";
		System.out.println("\nurl: " + url);
		try {
			URL uriAddress = new URL(url);
			URLConnection res = uriAddress.openConnection();
			BufferedReader bufferedReader  =  new BufferedReader(new InputStreamReader(res.getInputStream()));
		    StringBuilder builder = new StringBuilder();
		    String line;
		    while ((line = bufferedReader.readLine()) != null) {
		        builder.append(line + "\n");
		    }	
		    String builderArray = builder.toString();
		    //System.out.println("\nbuilderArray: \n" + builderArray);
		    JSONObject root = (JSONObject)JSONValue.parseWithException(builderArray);
		    //System.out.println("\nresult: \n" + root);
		    JSONArray resultArray = (JSONArray) root.get("results");
		    JSONObject obj0 = (JSONObject) resultArray.get(0);
		    //System.out.println("\nobj0: \n" + obj0);
		    String formattedAddress = (String) obj0.get("formatted_address");
		    System.out.println("\nFormattedAddress: \n" + formattedAddress);
		    address = formattedAddress;
		    
		    // Accessing status
		    String stat = (String) root.get("status");
		    status = stat;
		    System.out.println("\nStatus: " + status);
		}
		catch(Exception e) {
			System.out.println("\nException: " + e);
		}
		if(status.contentEquals("ZERO_RESULTS")) {
			address = "No address";
			return address;
		}
		if(status.contentEquals("OVER_QUERY_LIMIT")) {
			address = "Crossed your quota of api calls";
			return address;
		}
		if(status.contentEquals("REQUEST_DENIED")) {
			address = "Request denied";
			return address;
		}
		if(status.contentEquals("INVALID_REQUEST")) {
			address = "Invalid query";
			return address;
		}		
		if(status.contentEquals("UNKNOWN_ERROR")) {
			address = "Server Error";
			return address;
		}		
		if(address.contentEquals("")) {
			address = "Blank Address";
			return address;
		}
		else {
			return address;
		}
	}



}
