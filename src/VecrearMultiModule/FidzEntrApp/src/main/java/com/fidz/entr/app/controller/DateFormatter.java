package com.fidz.entr.app.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;



public class DateFormatter {

	
	public static Date getMinusOneDate(String date) throws ParseException {
	
	 DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	 Date fdate = format.parse(date);
   
     LocalDateTime localDateTime = fdate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
     //end date as minus 1
     localDateTime=localDateTime.minusDays(1);
     Date currentDateMinusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
     return currentDateMinusOneDay;
	}
}
