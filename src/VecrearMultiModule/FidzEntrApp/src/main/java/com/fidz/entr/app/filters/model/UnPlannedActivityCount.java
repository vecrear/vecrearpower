package com.fidz.entr.app.filters.model;

import lombok.Data;

@Data
public class UnPlannedActivityCount {

	
	private String activityConfigurationType;
	
	private String totalUnPlannedActivity;

	public UnPlannedActivityCount(String activityConfigurationType, String totalUnPlannedActivity) {
		super();
		this.activityConfigurationType = activityConfigurationType;
		this.totalUnPlannedActivity = totalUnPlannedActivity;
	}

	@Override
	public String toString() {
		return "PlannedActivityCount [activityConfigurationType=" + activityConfigurationType
				+ ", totalUnPlannedActivity=" + totalUnPlannedActivity + "]";
	}







}
