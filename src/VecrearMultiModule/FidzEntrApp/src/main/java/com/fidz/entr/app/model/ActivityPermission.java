package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum ActivityPermission {
	VIEW_ACTIVITY, ASSIGN_ACTIVITY, PERFORM_ACTIVITY, EDIT_ACTIVITY, DELETE_ACTIVITY
}
