package com.fidz.entr.app.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
@Data
@Document(collection = "androidversion")
@JsonIgnoreProperties(value = { "target" })
public class VersionControll {
	
	@Id
	protected String id;
	
	protected String schemaName;
	
	protected int versionCode;
	
	protected String versionNumber;

}
