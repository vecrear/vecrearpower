package com.fidz.entr.app.filters.model;



import lombok.Data;

@Data
public class PunchInInfo {

	
private String punchInCount;

public PunchInInfo(String punchInCount) {
	super();
	this.punchInCount = punchInCount;
}

@Override
public String toString() {
	return "PunchInfo [punchInCount=" + punchInCount + "]";
}





}
