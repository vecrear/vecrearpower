package com.fidz.entr.app.configs;

import java.util.List;



import lombok.Data;
@Data
public class ErrorResponse1 {
	public ErrorResponse1(String message, List<String> details) {
        super();
        this.message = message;
        this.details = details;
    }
 
    //General error message about nature of error
    private String message;
 
    //Specific errors in API request processing
    private List<String> details;
    //private String details;
}
