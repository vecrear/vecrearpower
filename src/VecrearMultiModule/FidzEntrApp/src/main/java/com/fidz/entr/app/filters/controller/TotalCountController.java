package com.fidz.entr.app.filters.controller;

import java.text.ParseException;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.MultiUserReportAttendance;
import com.fidz.entr.app.filters.service.TotalCountService;
import com.google.gson.JsonObject;

@CrossOrigin(maxAge = 3600)			
@RestController("TotalCountController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/totalcount")
public class TotalCountController {
	
	@Autowired
	TotalCountService totalCountService;

	@PostMapping("/get/all")
	public JSONObject getCount(@RequestHeader(value="schemaName")String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return totalCountService.getCount(schemaName);
	}
	
	@PostMapping("/datewise/sdate={startdate}|edate={enddate}")
	public JSONObject getByDateCount(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate) throws ParseException {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return totalCountService.getByDateCount(schemaName,startdate,enddate);
	}
	
	
	@PostMapping("user/{id}/datewise/sdate={startdate}|edate={enddate}")
	public JSONObject getByUserDateCount(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="id") String id,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate) throws ParseException {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return totalCountService.getByUserDateCount(schemaName,id,startdate,enddate);
	}
	
	@PostMapping("/multiuser")
	public JSONObject getMultiUserReportAttendance(@RequestHeader(value="schemaName")String schemaName,@RequestBody MultiUserReportAttendance mult) throws ParseException {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return totalCountService.getMultiUserReportAttendance(schemaName,mult);
	}
	
	@PostMapping("/phone/{phone}/message/{message}")
	public String sendSMS(@RequestHeader(value="schemaName")String schemaName,@PathVariable(value="phone") String phone,@PathVariable(value="message") String message) throws ParseException {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return totalCountService.sendSms(schemaName, phone, message);
	}
}
