package com.fidz.entr.app.reportactivities;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;

import lombok.Data;

@JsonIgnoreProperties(value = { "target" })
public enum ActivityStatus {
	OPEN, POSTPONED, COMPLETED, CANCELLED
}
