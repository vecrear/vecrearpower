package com.fidz.entr.app.exception;

public class CustomerCategoryNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3984419706377765612L;
	public CustomerCategoryNotFoundException(String customerCategory) {
		super("CustomerCategory not found with id/name " + customerCategory);
	}
}
