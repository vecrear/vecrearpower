package com.fidz.entr.app.filters.model;

import java.util.Date;
import java.util.List;

import com.fidz.entr.app.model.User;

import lombok.Data;

@Data
public class CompareActivityRatio {

	private List<User> userId;
	
	private String activityId;
	
    private String startDate;
	
	private String endDate;

}
