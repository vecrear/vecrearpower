package com.fidz.entr.app.model;

import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.base.model.LoginMode;
import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Login extends com.fidz.entr.superadmin.model.Login{
	
	
public Login(@NotNull String userName, @NotNull String password, String imei, String notificationId,
			LoginMode loginMode){
		super(userName, password, imei, notificationId, loginMode);
		// TODO Auto-generated constructor stub
	}

	
@Override
public String toString() {
	return "Login [imei=" + imei + ", notificationId=" + notificationId + ", loginMode=" + loginMode + ", userName="
			+ userName + ", password=" + password + "]";
}

		

}
