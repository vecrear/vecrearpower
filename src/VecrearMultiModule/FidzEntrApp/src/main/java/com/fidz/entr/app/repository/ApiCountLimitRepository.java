package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.awsconfig.model.APiCountInfo;
import com.fidz.entr.app.awsconfig.model.APiCountLimit;
import com.fidz.entr.app.awsconfig.model.AwsInfo;


import reactor.core.publisher.Mono;

@Repository
public interface ApiCountLimitRepository extends MongoRepository<APiCountLimit, String>{
	//Mono<Department> findByName(String name);
	APiCountLimit findByCompanyId(String name);
	
}