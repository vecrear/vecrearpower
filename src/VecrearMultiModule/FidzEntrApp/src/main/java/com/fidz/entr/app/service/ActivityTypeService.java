package com.fidz.entr.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.exception.ActivityTypeNotFoundException;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.Notification;
import com.fidz.entr.app.model.PushNotificationRequest;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserActivityNotify;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.app.reportcustomers.Customer;
import com.fidz.entr.app.repository.ActivityTypeRepository;
import com.fidz.entr.app.repository.CustomerRepository;
import com.fidz.entr.app.repository.UserRepository;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;

@Service("FEAActivityTypeService")
public class ActivityTypeService {
	private GenericAppINterface<ActivityType> genericINterface;

	@Autowired
	public ActivityTypeService(GenericAppINterface<ActivityType> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private ActivityTypeRepository activityTypeRepository;
	@Autowired
	UserRepository userRepository;

	@Autowired
	private FCMServic fcmServic;

	@Autowired
	private NotificationSendingService notificationSendingService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	private static String name = "ActivityType";

	// get All Activity Type(Post Request)
	public List<ActivityType> getAllActivityType(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllActivityType ActivityStatusChangeLog.java");
		List<ActivityType> activityTypes = null;
		try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
			activityTypes = this.genericINterface.findAll(ActivityType.class);
		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error while getting data from getAllActivityType ActivityTypeService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully fetched data from getAllActivityType ActivityTypeService.java");
		return activityTypes;
	}

	public ActivityType createActivityType(ActivityType activityType) {
		ActivityType activityTypes = null;

		Constant.LOGGER
				.info(" Inside ActivityTypeService.java Value for insert ActivityType record:createActivityType :: "
						+ activityType);
		String schemaName = activityType.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

		Notification noty = new Notification();

		// Constant.LOGGER.info(" List data"+names);

		Query query = new Query();
		List<ActivityType> activityTypeList = null;

		query.addCriteria(Criteria.where("department.id").is(activityType.getDepartment().getId()));

		activityTypeList = this.genericINterface.find(query, ActivityType.class);

		if (activityTypeList.size() > 0) {
			List<String> names = activityTypeList.stream().map(p -> p.getName()).collect(Collectors.toList());
			if (names.stream().anyMatch(s -> s.equals(activityType.getName()) == true)) {
				Constant.LOGGER.info("Inside if statement");
				throw new NameFoundException(
						name + ExceptionConstant.SAME_NAME_SAME_ACTIVITY_TYPE + ": " + activityType.getName());

			} else if (names.stream().anyMatch(s -> s.equalsIgnoreCase(activityType.getName()) == true)) {
				Constant.LOGGER.info("Inside else if statement");
				throw new NameFoundException(
						name + ExceptionConstant.SAME_NAME_DIFFERENT_CASE + ": " + activityType.getName());
			} else {
				Constant.LOGGER.info("Inside else statement with same department different activity type");

				activityTypes = this.activityTypeRepository.save(activityType);

				if (activityType != null) {
					Constant.LOGGER.error("user.getNotificationId()----" + activityType.toString());
					String departId = activityType.getDepartment().getId();
					// Stream<User> notifiedUsers = getAllUsersByDepartId(departId,schemaName);
					List<User> notifiedUsers = getAllUsersByDepartId(departId, schemaName);
					Constant.LOGGER.error("notifiedUsers----" + notifiedUsers.toString());
					UserActivityNotify userActivityNoty = new UserActivityNotify();

					notifiedUsers.forEach(user -> {

						if (user.getNotificationId() != null) {
							try {
								Constant.LOGGER.error("user.getNotificationId()----" + user.getNotificationId());
								userActivityNoty.setNotificationId(user.getNotificationId());
								userActivityNoty.setUserActivityStatus(
										"New Activity Type has been mapped to you! Please perform config sync");
								Constant.LOGGER.error("userActivityNoty----" + userActivityNoty.toString());
								noty.setMessage(userActivityNoty.getUserActivityStatus());
								noty.setToken(userActivityNoty.getNotificationId());
								String res = this.notificationSendingService.sendNotification(noty);
								if (res != null) {
									Constant.LOGGER.info("Notification Sending Response" + " " + res);
								}

							} catch (Exception e) {
								// TODO: handle exception
							}

						} else {
							Constant.LOGGER.error("User Device not found");
						}

					});

				}

			}
			return activityTypes;
		} else {
			Constant.LOGGER.info("Inside else ------------statement with different dept and diff  activity type");

			activityTypes = this.activityTypeRepository.save(activityType);

			if (activityType != null) {
				Constant.LOGGER.error("user.getNotificationId()----" + activityTypes.toString());
				String departId = activityType.getDepartment().getId();
				List<User> notifiedUsers = getAllUsersByDepartId(departId, schemaName);
				Constant.LOGGER.error("notifiedUsers----" + notifiedUsers.toString());
				UserActivityNotify userActivityNoty = new UserActivityNotify();

				notifiedUsers.forEach(user -> {

					if (user.getNotificationId() != null) {
						try {
							Constant.LOGGER.error("user.getNotificationId()----" + user.getNotificationId());
							userActivityNoty.setNotificationId(user.getNotificationId());
							userActivityNoty.setUserActivityStatus(
									"New Activity Type has been mapped to you! Please perform config sync");
							Constant.LOGGER.error("userActivityNoty----" + userActivityNoty.toString());
							noty.setMessage(userActivityNoty.getUserActivityStatus());
							noty.setToken(userActivityNoty.getNotificationId());
							String res = this.notificationSendingService.sendNotification(noty);
							if (res != null) {
								Constant.LOGGER.info("Notification Sending Response" + " " + res);
							}

						} catch (Exception e) {
							Constant.LOGGER.info("Exception----------------" + " " + e.toString());
						}
					} else {
						Constant.LOGGER.error("User Device not found");
					}

				});

			}

			return activityTypes;
		}

		// return activityTypes;

	}

	public void createActivityTypes(ActivityType activityType) {
		String schemaName = activityType.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info(" Inside ActivityTypeService.java createActivityTypes  ");

		// return activityTypeRepository.save(activityType);
		this.genericINterface.saveName(activityType);
	}

	/*
	 * //get All Activity Type by id(Post Request) public ActivityType
	 * getActivityTypeById(String id,String schemaName){
	 * MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName); return
	 * this.genericINterface.findById(id, ActivityType.class);
	 * 
	 * }
	 */
	// get All Activity Type by id(Post Request)
	public ResponseEntity<ActivityType> getActivityTypeById(String id, String schemaName) {
		ActivityType activityType = null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info(" Inside ActivityTypeService.java getActivityTypeById  ");

		activityType = this.genericINterface.findById(id, ActivityType.class);
		if (activityType == null) {
			throw new ActivityTypeNotFoundException(id);
		} else {
			return ResponseEntity.ok(activityType);
		}

	}

	// get All Activity Type by name(Post Request)
	public ActivityType getActivityTypeByName(String name, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info(" Inside ActivityTypeService.java getActivityTypeByName");

		return this.activityTypeRepository.findByName(name);
	}

	/*
	 * public Flux<ActivityType> getAllActivityType1(){ return
	 * activityTypeRepository.findAll(); }
	 */

	/*
	 * public Mono<ActivityType> getActivityTypeById1(String id) { return
	 * activityTypeRepository.findById(id).switchIfEmpty(Mono.error(new
	 * ActivityTypeNotFoundException(id)));
	 * 
	 * }
	 */

	/*
	 * public Mono<ActivityType> getActivityTypeByName1(String name) { return
	 * activityTypeRepository.findByName(name).switchIfEmpty(Mono.error( new
	 * ActivityTypeNotFoundException(name))); }
	 */

	public ActivityType updateActivityType(String id, ActivityType activityType) {
		String schemaName = activityType.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info(" Inside ActivityTypeService.java updateActivityType  ");
		Notification noty = new Notification();
		Query query = new Query();
		List<ActivityType> activityTypeList = null;
		ActivityType acttype = null;

		query.addCriteria(Criteria.where("department.id").is(activityType.getDepartment().getId()));

		activityTypeList = this.genericINterface.find(query, ActivityType.class);
		ActivityType ctr = this.genericINterface.findById(id, ActivityType.class);
		if (activityTypeList.size() > 0) {
			List<String> names = activityTypeList.stream().map(p -> p.getName()).collect(Collectors.toList());
			names.remove(ctr.getName());
			if (names.stream().anyMatch(s -> s.equals(activityType.getName()) == true)) {
				Constant.LOGGER.info("Inside if statement");
				throw new NameFoundException(
						name + ExceptionConstant.SAME_NAME_SAME_ACTIVITY_TYPE + ": " + activityType.getName());

			} else if (names.stream().anyMatch(s -> s.equalsIgnoreCase(activityType.getName()) == true)) {
				Constant.LOGGER.info("Inside else if statement");
				throw new NameFoundException(
						name + ExceptionConstant.SAME_NAME_DIFFERENT_CASE + ": " + activityType.getName());
			} else {
				Constant.LOGGER.info("Inside else statement with same department different activity type");

				// String departId=activityType.getDepartment().getId();
				// Stream<User> notifiedUsers=getAllUsersByDepartId(departId,schemaName);
				// Constant.LOGGER.error("activityType----"+activityType.toString());
				if (activityType != null) {
					Constant.LOGGER.error("user.getNotificationId()----" + activityType.toString());
					String departId = activityType.getDepartment().getId();
					List<User> notifiedUsers = getAllUsersByDepartId(departId, schemaName);
					Constant.LOGGER.error("notifiedUsers----" + notifiedUsers.toString());
					UserActivityNotify userActivityNoty = new UserActivityNotify();
					notifiedUsers.forEach(user -> {

						if (user.getNotificationId() != null) {
							Constant.LOGGER.error("user.getNotificationId()----" + user.getNotificationId());
							userActivityNoty.setNotificationId(user.getNotificationId());
							userActivityNoty.setUserActivityStatus(
									"Activity Type has been updated! Please perform config sync");
							Constant.LOGGER.error("userActivityNoty----" + userActivityNoty.toString());
							noty.setMessage(userActivityNoty.getUserActivityStatus());
							noty.setToken(userActivityNoty.getNotificationId());
							String res = this.notificationSendingService.sendNotification(noty);
							Constant.LOGGER.info("Notification Sending Response" + " " + res);

						} else {
							Constant.LOGGER.error("User Device not found");
						}

					});

				}

//		List<Notification> nds=new ArrayList<Notification>() ;
//		notifiedUsers.forEach(user -> {
//			if (user.getNotificationId()!=null) {
//				Notification notification = new Notification();
//				notification.setMessage("Activity Record Updated");
//				notification.setUniqueNotificationId(user.getNotificationId());
//				notification.setOsType("AndroidOS");
//				nds.add(notification);
//			}
//		  });
//		  
//		
//		System.out.println("nds details"+nds);
//		NotificationForDeviceService.getInstance().sendNotification(nds);
				activityType.setId(id);
				acttype = activityTypeRepository.save(activityType);

			}

		}
		activityType.setId(id);
		acttype = activityTypeRepository.save(activityType);

		return acttype;

	}

//    public Stream<User> getAllUsersByDepartId(String departId, String schemaName) {
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
// 		Constant.LOGGER.info(" Inside ActivityTypeService.java getAllUsersByDepartId");
//
//		//List<User> users=(List<User>) this.userRepository.findAll().stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
//		Stream<User> users=this.genericINterface.findAll(User.class).stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
//		return users;
//		/*return this.genericINterface.findAll(Activity.class).filter(activity ->activity.getResource().getId().contains(userId) && activity.getActivityStatus().equals(ActivityStatus.valueOf("OPEN"))
//				&&  activity.getActivityConfigurationType().equals(ActivityConfigurationType.valueOf("PLANNED")));*/
//
//	}

	public List<User> getAllUsersByDepartId(String departId, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info(" Inside ActivityTypeService.java getAllUsersByDepartId");
		Query query = new Query();
		query.addCriteria(Criteria.where("department.id").is(departId));
		// List<User> users=(List<User>)
		// this.userRepository.findAll().stream().filter(userlist ->
		// userlist.getDepartment().getId().equals(departId));
		List<User> users = this.genericINterface.find(query, User.class);
		return users;
		/*
		 * return this.genericINterface.findAll(Activity.class).filter(activity
		 * ->activity.getResource().getId().contains(userId) &&
		 * activity.getActivityStatus().equals(ActivityStatus.valueOf("OPEN")) &&
		 * activity.getActivityConfigurationType().equals(ActivityConfigurationType.
		 * valueOf("PLANNED")));
		 */

	}

	// hard delete
	public Map<String, String> deleteActivityType(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		ActivityType activityType = null;
		Notification noty = new Notification();

		Constant.LOGGER.info(" Inside ActivityTypeService.java deleteActivityType");
		activityType = this.genericINterface.findById(id, ActivityType.class);

		if (activityType != null) {
			String departId = activityType.getDepartment().getId();
			List<User> notifiedUsers = getAllUsersByDepartId(departId, schemaName);
			UserActivityNotify userActivityNoty = new UserActivityNotify();

			notifiedUsers.forEach(user -> {
				if (user.getNotificationId() != null) {
					Constant.LOGGER.error("user.getNotificationId()----" + user.getNotificationId());
					userActivityNoty.setNotificationId(user.getNotificationId());
					userActivityNoty
							.setUserActivityStatus("Activity Type has been deleted! Please perform config sync");
					Constant.LOGGER.error("userActivityNoty----" + userActivityNoty.toString());
					noty.setMessage(userActivityNoty.getUserActivityStatus());
					noty.setToken(userActivityNoty.getNotificationId());
					String res = this.notificationSendingService.sendNotification(noty);
					Constant.LOGGER.info("Notification Sending Response" + " " + res);

				} else {
					Constant.LOGGER.error("User Device not found");
				}
			});
			this.activityTypeRepository.deleteById(id);

		}
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "ActivityType deleted successfully");
		return response;

	}

	// soft delete
	public Map<String, String> deleteSoftActivityType(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		ActivityType activityType = this.genericINterface.findById(id, ActivityType.class);
		Constant.LOGGER.info(" Inside ActivityTypeService.java deleteSoftActivityType  ");

		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		activityType.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		activityType.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		activityType.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(activityType);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "ActivityType deleted successfully");
		return response;

	}

	public List<ActivityType> streamAllActivityTypes() {
		return activityTypeRepository.findAll();
	}

	// To get all ActivityTypes paginated
	// page number starts from zero
	public List<ActivityType> getAllActivityTypesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllActivityTypesPaginated ActivityTypeService.java");
		Query query = new Query();
		List<ActivityType> activityTypes = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest);
			activityTypes = this.genericINterface.find(query, ActivityType.class);

		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error occurred inside getAllActivityTypesPaginated ActivityTypeService.java " + ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllActivityTypesPaginated " + query);
		return activityTypes;

	}

	public List<ActivityType> getAllActyvitytypesByDepartment(String deptid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllActyvitytypesByDepartment ActivityTypeService.java");
		Query query = new Query();
		List<ActivityType> listofActivityTypes = null;

		try {
			query.addCriteria(Criteria.where("department.id").is(deptid));
			listofActivityTypes = this.genericINterface.find(query, ActivityType.class);

		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error occurred getAllActyvitytypesByDepartment ActivityTypeService.java " + ex.getMessage());
		}

		return listofActivityTypes;
	}

	public String getStatusofObjectMappedOrNot(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getStatusofObjectMappedOrNot ActivityTypeService.java");

		Query q1 = new Query();
		ActivityType activityType = null;
		String message = "";

		try {
			activityType = this.genericINterface.findById(id, ActivityType.class);
			if (activityType.getDepartment() != null) {
				message = "activity is mapped with department : " + activityType.getDepartment().getName();
				Constant.LOGGER.error("activity is mapped with department  " + activityType.getDepartment().getName());
			} else {
				message = "activity is not mapped";
			}
		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error occurred getStatusofObjectMappedOrNot ActivityTypeService.java" + ex.getMessage());
		}
		return message;
	}

	public String custcategoryidtMappedOrNot(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside custcategoryidtMappedOrNot ActivityTypeService.java");

		Query query = new Query();
		// ActivityType activityType =null;
		String message = "";
		List<ActivityType> listofActivityTypes = null;
		List<Customer> customerslist = null;
		try {

			query.addCriteria(Criteria.where("activityTargets.customerCategory.id").is(id));
			listofActivityTypes = this.genericINterface.find(query, ActivityType.class);
			Constant.LOGGER.error("listofActivityTypes size " + listofActivityTypes.size());
			customerslist = customerService.getAllCustomerswebByCutomerCategotyID(id, schemaName);
			Constant.LOGGER.error("customerslist size based on the customercategory id  " + customerslist.size());
			Constant.LOGGER.info("ActivityTypes List size based on customerCategoryId" + listofActivityTypes.size());
			if ((listofActivityTypes.size() > 0 || (customerslist.size() > 0))) {

				message = "Customer Category is mapped";

			} else {
				message = "Customer Category is not mapped";
			}

		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error occurred custcategoryidtMappedOrNot ActivityTypeService.java " + ex.getMessage());
		}

		return message;
	}

	public List<ActivityType> getAllActyvitytypesByCompany(String companyid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllActyvitytypesByCompany ActivityTypeService.java");
		Query query = new Query();
		List<ActivityType> listofActivityTypes = null;

		try {
			query.addCriteria(Criteria.where("company.id").is(companyid));
			listofActivityTypes = this.genericINterface.find(query, ActivityType.class);

		} catch (Exception ex) {
			Constant.LOGGER
					.error("Error occurred getAllActyvitytypesByCompany ActivityTypeService.java  " + ex.getMessage());
		}

		return listofActivityTypes;
	}

	public List<ActivityType> getAllSearchActivityTypesPaginated(String text, int pageNumber, int pageSize,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<ActivityType> activityTypes = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside getAllSearchActivityTypesPaginated CustomerService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(ActivityType.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			activityTypes = mongoTemplate.find(query, ActivityType.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside getAllSearchActivityTypesPaginated CustomerService.java" + ex.getMessage());

		}
		return activityTypes;
	}

	public List<ActivityType> getAllSearchActivityTypesByNamePaginated(String text, int pageNumber, int pageSize,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<ActivityType> activityTypes = null;
		Query q = new Query();

		try {
			q.addCriteria(Criteria.where("name").regex(text, "i"));
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			q.with(pageableRequest);
			activityTypes = this.genericINterface.find(q, ActivityType.class);

		} catch (Exception ex) {
			Constant.LOGGER
					.info("Error occured inside getAllSearchActivityTypesByNamePaginated" + ex.getLocalizedMessage());
		}
		return activityTypes;
	}

}
