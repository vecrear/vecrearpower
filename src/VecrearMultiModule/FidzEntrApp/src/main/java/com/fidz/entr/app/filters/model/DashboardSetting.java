package com.fidz.entr.app.filters.model;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Document(collection = "dashboardsetting")
@JsonIgnoreProperties(value = { "target" })
@Data
public class DashboardSetting{
    
	@Id
	private String id;
   
	private Fperiod finperiod;

	private WeekName startofweek;
	
    private DisplayType selection;

    private boolean legend;
    
    private boolean grouping;

	

	public DashboardSetting(){
		super();
		// TODO Auto-generated constructor stub
	}



	public DashboardSetting(String id, Fperiod finperiod, WeekName startofweek, DisplayType selection, boolean legend,
			boolean grouping) {
		super();
		this.id = id;
		this.finperiod = finperiod;
		this.startofweek = startofweek;
		this.selection = selection;
		this.legend = legend;
		this.grouping = grouping;
	}



	@Override
	public String toString() {
		return "DashboardSetting [id=" + id + ", finperiod=" + finperiod + ", startofweek=" + startofweek
				+ ", selection=" + selection + ", legend=" + legend + ", grouping=" + grouping + "]";
	}

	



}
