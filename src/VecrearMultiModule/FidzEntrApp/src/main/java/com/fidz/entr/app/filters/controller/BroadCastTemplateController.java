package com.fidz.entr.app.filters.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.BroadCastTemplate;
import com.fidz.entr.app.filters.service.BroadCastTemplateService;

@CrossOrigin(maxAge = 3600)
@RestController("BroadCastTemplateController")
@Configuration
@RequestMapping(value = "/broadcasttemplates")
public class BroadCastTemplateController {

	@Autowired
	BroadCastTemplateService broadcastTemplateService;

	@PostMapping("/get/all")
	public List<BroadCastTemplate> getAllBroadCastTemplate(@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return broadcastTemplateService.getAllBroadCastTemplate(schemaName);
	}

	@PostMapping
	public BroadCastTemplate createBroadCastTemplate(@Valid @RequestBody BroadCastTemplate broadCastTemplate) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return broadcastTemplateService.createBroadCastTemplate(broadCastTemplate);
	}

	@PostMapping("/get/id/{id}")
	public BroadCastTemplate getBroadCastTemplateById(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return broadcastTemplateService.getBroadCastTemplateById(id, schemaName);
	}

	@PostMapping("/update/id/{id}")
	public BroadCastTemplate updateBroadCastTemplate(@PathVariable(value = "id") String id,
			@Valid @RequestBody BroadCastTemplate broadCastTemplate) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:" + baseUrl);
		return broadcastTemplateService.updateBroadCastTemplate(id, broadCastTemplate);

	}

	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteBroadCastTemplate(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		return broadcastTemplateService.deleteBroadCastTemplate(id, schemaName);
	}

	@PostMapping("/delete/soft/id/{id}")
	public Map<String, String> softDeletebroadcastTemplateService(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {

		return broadcastTemplateService.softDeletebroadcastTemplateService(id, schemaName, timeUpdate);
	}

}
