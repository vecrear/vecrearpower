package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.PunchStatus;
import com.fidz.entr.app.model.UserRole;


@Repository
public interface PunchInStatusRepository extends MongoRepository<PunchStatus, String>{
	//Mono<UserRole> findByName(String name);
	//PunchStatus findByUsername(String name);
}
