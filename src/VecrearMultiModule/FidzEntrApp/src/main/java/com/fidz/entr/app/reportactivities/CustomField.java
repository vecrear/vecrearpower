package com.fidz.entr.app.reportactivities;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.app.model.UIComponentType;
import com.fidz.entr.base.model.DataType;
import com.fidz.entr.base.model.Validation;
import com.fidz.entr.base.model.Visibility;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class CustomField {
	
	
	protected UIComponentType uiComponentType; //TEXT BOX, RADIO BUTTON, ETC
    protected List<String> options; //value options yesorno
	protected int sortOrder; //sort order
	protected String range; //data range
	protected String hint; //UI Hint
	@NotNull
	protected String fieldName;
    protected DataType dataType;
    protected boolean mandatory;
    protected String pattern;
    protected Validation validation;
    protected List<Validation> dependencyValidations;                 
    protected Visibility visibility;

}
