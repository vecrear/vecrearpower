package com.fidz.entr.app.filters.service;

import org.springframework.stereotype.Service;

@Service
public class PercentageCalculatorService {

	public double calculatePercentage(double obtained, double total) {
        return obtained * 100 / total;
    }
	
}
