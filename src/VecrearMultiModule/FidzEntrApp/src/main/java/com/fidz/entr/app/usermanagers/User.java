package com.fidz.entr.app.usermanagers;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.app.model.PunchStatus;


import com.fidz.entr.base.model.TaggedDevices;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class User  {
	
	@NotNull
	@CreatedDate
	protected Date createdTimestamp;
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@LastModifiedBy
	protected String updatedByUser;
	@LastModifiedDate
	protected Date deletedTimestamp;
	@LastModifiedBy
	protected String deletedByUser;
	@NotNull
	protected Status status;
	@NotNull
	protected String schemaName;
	@Id
	protected String id;
	protected String userName;
	protected String password; //should be encrypted password
	protected PersonName name;
	protected Address address;
	protected Contact contact;
	@JsonIgnore
	protected Object emergencyContactName;
	@JsonIgnore
	protected Object emergencyContact;
	@JsonIgnore
	protected List<Object> appRoles;
	@JsonIgnore
	protected List<Object> reportsTo;
	@JsonIgnore
	protected List<Object> additionalDepartments;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object device;
	@DBRef(lazy = true)
	@JsonIgnore
	protected Object country;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object region;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object state;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object city;
	@JsonIgnore
	protected Map<String, Object> additionalFields;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object userRole;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object company;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object department;
	@JsonIgnore
	protected String notificationId;
	@JsonIgnore
	protected int userRoleId;
	@JsonIgnore
	protected String managerName;
	@JsonIgnore
	protected String managerMobile;
	@JsonIgnore
	protected String managerEmail;
	@JsonIgnore
	protected List<Object> taggedDevices;
	protected boolean geofenceTagged = false;


	
	
	
}
