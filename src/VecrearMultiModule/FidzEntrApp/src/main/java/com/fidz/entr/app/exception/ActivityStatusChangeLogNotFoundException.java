package com.fidz.entr.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ActivityStatusChangeLogNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3947065372896992537L;

	public ActivityStatusChangeLogNotFoundException(String activityStatusChangeLog) {
		super("ActivityStatusChangeLog not found with id/name " + activityStatusChangeLog);
	}
}
