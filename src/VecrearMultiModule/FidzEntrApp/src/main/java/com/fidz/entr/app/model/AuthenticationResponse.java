package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class AuthenticationResponse {
	
	protected Object schemaName;
	protected User user;
	protected boolean isSuperAdmin;
	protected String loginStatus;
	
	  public AuthenticationResponse() {
	        super();
	        // TODO Auto-generated constructor stub
	    }
	
	public AuthenticationResponse(Object schemaName, User user, boolean isSuperAdmin) {
		super();
		this.schemaName = schemaName;
		this.user = user;
		this.isSuperAdmin = isSuperAdmin;
	}
    public AuthenticationResponse(Object schemaName, User user, boolean isSuperAdmin, String loginStatus) {
        super();
        this.schemaName = schemaName;
        this.user = user;
        this.isSuperAdmin = isSuperAdmin;
        this.loginStatus = loginStatus;
    }
	@Override
	public String toString() {
		return "AuthenticationResponse [schemaName=" + schemaName + ", user=" + user + ", isSuperAdmin=" + isSuperAdmin
				+ "]";
	}
	
	

	
	
	
	

}
