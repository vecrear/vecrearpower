package com.fidz.entr.app.awsconfig.service;

import java.awt.image.BufferedImage;
import java.io.File;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
@Service("FEAAwsAWSS3Service")
public interface AWSS3Service {
	String uploadFile(MultipartFile multipartFile);
	
	String uploadFiles(BufferedImage bufferedImage,String path);

	byte[]  getTheConvertedImageFromByteCode(String imageConvertionFile);

	String uploadFileforS3(File files);
}
