package com.fidz.entr.app.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomerTypeFieldNotFoundException extends RuntimeException{

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerTypeFieldNotFoundException(String id) {
		super(" Field not found with id/name " + id);
	}
}
