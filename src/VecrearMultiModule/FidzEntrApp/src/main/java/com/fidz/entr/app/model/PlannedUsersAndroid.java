package com.fidz.entr.app.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class PlannedUsersAndroid {

	
	private List<String> userId;
	
	private List<String> cityIds;
	
	private String deptId;

	public PlannedUsersAndroid(List<String> userId) {
		super();
		this.userId = userId;
	}

	

	public PlannedUsersAndroid() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PlannedUsersAndroid(List<String> userId, List<String> cityIds ,String deptId) {
		super();
		this.userId = userId;
		this.cityIds = cityIds;
		this.deptId = deptId;
	}

	
	
	
}
