package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.DepartmentNotFoundException;
import com.fidz.entr.app.model.CustomerTypeField;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.service.DeviceDetailsService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEBDeviceDetailsController")
@RequestMapping(value="/devicedetails")
public class DeviceDetailsController {
	@Autowired
	private DeviceDetailsService deviceDetailsService;
	
	@PostMapping("/get/all")
    public List<DeviceDetails> getAllDeviceDetails(@RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return deviceDetailsService.getAllDeviceDetails(schemaName);
    }
	
	//Post request to get all DeviceDetails  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<DeviceDetails> getAllDeviceDetailsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return deviceDetailsService.getAllDeviceDetailsPaginated(pageNumber, pageSize, schemaName);
	}
	
    @PostMapping
    public DeviceDetails createDeviceDetail(@Valid @RequestBody DeviceDetails deviceDetails) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return deviceDetailsService.createDeviceDetail(deviceDetails);
    }
    @PostMapping("/creates")
    public void createDeviceDetails(@Valid @RequestBody DeviceDetails deviceDetails) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	deviceDetailsService.createDeviceDetails(deviceDetails);
    }
    
    @PostMapping("/get/id/{id}")
    public DeviceDetails getDeviceDetailsById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return deviceDetailsService.getDeviceDetailsById(id, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public DeviceDetails updateDeviceDetails(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody DeviceDetails deviceDetails) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return deviceDetailsService.updateDeviceDetails(id, deviceDetails);    
        
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteDeviceDetails(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return deviceDetailsService.deleteDeviceDetails(id, schemaName);
    }
   
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteDeviceDetails(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return deviceDetailsService.deleteSoftDeviceDetails(id, schemaName, timeUpdate);
    }
    
    
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A DeviceDetails with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(DepartmentNotFoundException ex) {
       
    	return ResponseEntity.notFound().build();
    }
    
}
