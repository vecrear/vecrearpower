package com.fidz.entr.app.awsconfig.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserPwdEncryptNotFoundException extends Exception {

	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserPwdEncryptNotFoundException(String name) {
		super("region not found with " + name);
	}


}
