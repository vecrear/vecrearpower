package com.fidz.entr.app.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;

import lombok.Data;

@Data
@Document(collection = "activityfield")
@JsonIgnoreProperties(value = { "target" })
public class ActivityField extends Base{
	
	@Id
	protected String id;
	
	protected CustomField customField;
	
	
	@DBRef(lazy = true)
	protected Company company;
	
	
	@DBRef(lazy = true)
	protected Department department;

	
}
