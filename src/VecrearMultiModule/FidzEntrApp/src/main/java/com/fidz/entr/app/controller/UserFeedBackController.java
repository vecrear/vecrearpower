package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.UserFeedBackNoteFoundException;
import com.fidz.entr.app.model.UserFeedBack;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.app.service.UserFeedBackService;
import com.fidz.entr.base.exception.UserRoleNotFoundException;

@CrossOrigin(maxAge = 3600)
@RestController("FEBUserFeedBackController")
@RequestMapping(value="/userfeedback")
public class UserFeedBackController {
	
	@Autowired
	
	private UserFeedBackService userFeedBackService;
	

	@PostMapping("/get/all")
    public List<UserFeedBack> getAllUserFeedBacks(@RequestHeader(value="schemaName") String schemaName) {
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userFeedBackService.getAllUserFeedBacks(schemaName);
    }
	
	@PostMapping("/get/all/mode/{mode}")
    public List<UserFeedBack> getAllUserFeedBacksByMode(@RequestHeader(value="schemaName") String schemaName,@PathVariable(value = "mode") String mode) {
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userFeedBackService.getAllUserFeedBacksByMode(mode,schemaName);
    }
	
	//Post request to get all UserRoles  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<UserFeedBack> getAllUserFeedBackPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userFeedBackService.getAllUserFeedBackPaginated(pageNumber, pageSize, schemaName);
	}
	
	
	 @PostMapping
	 public UserFeedBack createUserFeedBack(@Valid @RequestBody UserFeedBack userFeedBack) {
	    	 final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
	    	return userFeedBackService.createUserFeedBack(userFeedBack);
	    }
	 
	   @PostMapping("/get/userName/{userName}")
	    public UserFeedBack getUserFeedBack(@PathVariable(value = "userName") String userName, @RequestHeader(value="schemaName") String schemaName) {
	    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
				Constant.LOGGER.info("URL:"+baseUrl);
				return userFeedBackService.getUserFeedBack(userName, schemaName);
	    }
	   
	   @PostMapping("/update/id/{id}")
	    public UserFeedBack updateUserFeedBack(@PathVariable(value = "id") String id,
	                                                   @Valid @RequestBody UserFeedBack userFeedBack) {
	    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
				Constant.LOGGER.info("URL:"+baseUrl);
				return userFeedBackService.updateUserFeedBack(id, userFeedBack);
	    }
	   
	   @PostMapping("/get/id/{id}")
	    public UserFeedBack getUserFeedBackById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
	    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
				Constant.LOGGER.info("URL:"+baseUrl);
				return userFeedBackService.getUserFeedBackById(id, schemaName);
	    }
	   
	   @PostMapping("/delete/id/{id}")
	    public Map<String, String> deleteUserFeedBackById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
	    	  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
				Constant.LOGGER.info("URL:"+baseUrl);
				return userFeedBackService.deleteUserFeedBackById(id, schemaName);
	    }
	   
	
	    // Exception Handling Examples
	    @ExceptionHandler
	    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
	        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A UserFeedBack with the same details already exists"));
	    }

	    @ExceptionHandler
	    public ResponseEntity<?> handleNotFoundException(UserFeedBackNoteFoundException ex) {
	        return ResponseEntity.notFound().build();
	    }
	   
	   
	   
	   

}
