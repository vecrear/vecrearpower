package com.fidz.entr.app.service;

import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.model.PushNotificationRequest;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserDepartmentConfig;
import com.fidz.entr.app.model.UserGeoStatus;
import com.fidz.entr.app.repository.UserGeoStatusRepository;

@Service("UserGeoStatusService")
public class UserGeoStatusService {
	
	  @Autowired
	    UserGeoStatusRepository userGeoStatusRepository;
	  
	  @Autowired
	  UserService UserService;
	  
	  @Autowired
	  FCMServic fcmServic;
 
	  
	  
	 public List<UserGeoStatus> getAll(String schemaName){
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);	
		 Constant.LOGGER.info("Inside getAll() UserGeoStatusService().java");
	    	List<UserGeoStatus> userGeostatus=null;
	    	try {
	    		userGeostatus = userGeoStatusRepository.findAll();
	    	}catch(Exception ex) {
	    	Constant.LOGGER.error("Error while getting userstatus() UserStatusService().java");	
	    	}
	    	return userGeostatus;
	    }

//		public UserGeoStatus updateUserGeoStatus(String userId, @Valid UserGeoStatus userGeoStatusReq,String schemaName) {
//	    	Constant.LOGGER.info("Inside updateUserGeoStatus() UserGeoStatusService().java");
//		    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);	
//	    	PushNotificationRequest ppp = PushNotificationRequest.getInstance(); 
//			// UserGeoStatus userGeoStatus = null;
//			try {
//  		 		 String notifyId= null; 
//  	             
//  		 		 	List<UserGeoStatus> geoStatusList = userGeoStatusRepository.findAll();
//  		 		
//  	        		 Constant.LOGGER.info("userGeoStatus-----"+userGeoStatusReq.toString());
//  	         		if(userId !=null) { 
//  	         		
//  	         			//userGeoStatus.setUserId(userId);
//  	         			this.userGeoStatusRepository.save(userGeoStatusReq);      		  	
//  	        		    ppp.setMessage(userGeoStatusReq.getGeoStatus());
//  	         		  	ppp.setTitle("Vecrear-Trac");
//  	         		  	ppp.setToken(userGeoStatusReq.getNotificationId());
//  	        		  	ppp.setTopic("");
//  	         		  
//  	        		  Thread geoThread = new Thread() {
//  	    			    public void run() {
//  	    			    	
//  	    			    	   try {
//  	    			    			
//  	    			    		   for (UserGeoStatus userGeoStatusList : geoStatusList) {
//  	    	  	        		  		
//  	    	  	        		  		if(userGeoStatusReq.getGeoStatus().trim().equalsIgnoreCase(userGeoStatusList.getGeoStatus().trim())) {	
//  	    	  		        		  	Constant.LOGGER.error("same data"+userGeoStatusList.getGeoStatus().trim());//todo logs
//  	    	  		        		  	 	
//  	    	  		             		}else
//  	    	  		             		{
//  	    	  		             			//new FCMServic().sendMessageToToken(ppp);//todo logs to change the IOC
//  	    	  		             			Constant.LOGGER.error("changed the userStatus data"+userGeoStatusList.getGeoStatus().trim());//todo logs
//  	    	  		             			
//  	    	  		             			String sendTouser= sendNotificationToUser(ppp);
//  	    	  		             			String status = sendNotificationToManger(userGeoStatusReq,schemaName);
//											
//											
//										
//  	    	  		             		}
//  	    	  						}
//  	    			    		   
//  	    						Thread.sleep(2);
//  	    					} catch (InterruptedException e) {
//  	    						e.printStackTrace();
//  	    					}	
//  	    			    }
//
//					
//  	    			};
//
//  	    			geoThread.start();
//  	    			
//  	         		}else {
//  	         			Constant.LOGGER.error("User Device not found");
//  	         		}
//  	             	
//  	             
//  	              }catch(Exception ex) {
//  	            Constant.LOGGER.error("Failure while Getting data Status() "+"\t"+ex.getMessage());
//  	         }
			
//			return userGeoStatusReq;
			
			
		
	//	}


		public UserGeoStatus updateUserGeoStatus(String userId, @Valid UserGeoStatus userGeoStatusReq,String schemaName) {
            Constant.LOGGER.info("Inside updateUserGeoStatus() UserGeoStatusService().java");
            MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);   
            PushNotificationRequest ppp = PushNotificationRequest.getInstance();
            // UserGeoStatus userGeoStatus = null;
            try {
                    //String notifyId= null;
                  
                        //UserGeoStatus geoStatusList = userGeoStatusRepository.findByUserId("userId");
                       
                       Constant.LOGGER.info("userGeoStatus-----"+userGeoStatusReq.toString());
                       if(userId !=null) {
//                       Query q = new Query();
//                       q.addCriteria(Criteria.where("userId").is(userId));
                           //userGeoStatus.setUserId(userId);
                           //UserGeoStatus geostatus = mongoTemplate.findOne(q, UserGeoStatus.class);
                           this.userGeoStatusRepository.save(userGeoStatusReq);                   
                          ppp.setMessage(userGeoStatusReq.getGeoStatus());
                          ppp.setTitle("Vecrear-Trac");
                          ppp.setToken(userGeoStatusReq.getNotificationId());
                          ppp.setTopic("");
                        
                        Thread geoThread = new Thread() {
                          public void run() {
                             
                                 Constant.LOGGER.error("userGeoStatusReq"+userGeoStatusReq.getGeoStatus().trim());//todo logs
								
								 String sendTouser= sendNotificationToUser(ppp);
								 String status = sendNotificationToManger(userGeoStatusReq,schemaName);   
                          }

                   
                      };

                      geoThread.start();
                    
                     
                       }else {
                           Constant.LOGGER.error("User Device not found");
                       }
                      
                  
                    }catch(Exception ex) {
                  Constant.LOGGER.error("Failure while Getting data Status() "+"\t"+ex.getMessage());
               }
           
            return userGeoStatusReq;
           
           
       
        }
		
		protected String sendNotificationToUser(PushNotificationRequest ppp) {
			Constant.LOGGER.info("Inside sendNotificationToUSER------------------------------status------"+ppp.toString());
			try {
				fcmServic.sendMessageToToken(ppp);
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "success";
		}

		protected String sendNotificationToManger(@Valid UserGeoStatus userGeoStatusReq,String schemaName) {
			Constant.LOGGER.info("Inside sendNotificationToManger------------------------------------");
		    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);	
	    	PushNotificationRequest pppManager = PushNotificationRequest.getInstance(); 
			// UserGeoStatus userGeoStatus = null;
			try {
  		 		 String notifyId= null; 
  		 		Constant.LOGGER.info("Inside sendNotificationToManger------userManger------------------------------");
  		 		 	User userManger = UserService.getUserById(userGeoStatusReq.getUserMangermongoId(), schemaName);
  		 		
  	        		 Constant.LOGGER.info("userGeoStatus-----"+userManger.toString());
  	         		if(userManger.getNotificationId() !=null) { 
  	         		
  	         			     		  	
  	         			pppManager.setMessage("your user"+" " +userGeoStatusReq.getGeoStatus());
  	         			pppManager.setTitle("Vecrear-Trac");
  	         			pppManager.setToken(userManger.getNotificationId());
  	         			pppManager.setTopic("");
  	         		  
  	        		  Thread geoThread = new Thread() {
  	    			    public void run() {
  	    			    	
  	    			    	   try {
  	    			    		 Constant.LOGGER.info("userGeoStatus- updation to manager----");
  	    			    		try {
									fcmServic.sendMessageToToken(pppManager);
								} catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
  	    						Thread.sleep(3);
  	    					} catch (InterruptedException e) {
  	    						e.printStackTrace();
  	    					}	
  	    			    }  
  	    			};

  	    			geoThread.start();
  	    			return "success";
  	    			
  	         		}else {
  	         			Constant.LOGGER.error("User Device not found");
  	         		}
  	             	
  	         		return "success";
  	              }catch(Exception ex) {
  	            
  	            Constant.LOGGER.error("Failure while Getting data Status() "+"\t"+ex.getMessage());
  	          return "failure";
  	         }
		
			
		}

		public Map<String, String> deleteUserGeoStatus(String userId,String schemaName) {
		    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);	
		    Constant.LOGGER.info("inside deleteUserStatus  deleteUserGeoStatus.java");
			 this.userGeoStatusRepository.deleteById(userId);
	    	 Map<String, String> response = new HashMap<String,String>();
	    	 response.put("message", "UserGeoStatus deleted successfully");
	    	 return response;
		}
}
