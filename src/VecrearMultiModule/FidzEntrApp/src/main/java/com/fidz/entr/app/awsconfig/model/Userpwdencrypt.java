package com.fidz.entr.app.awsconfig.model;

import java.util.List;


import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Group;
import com.fidz.entr.app.config.UserData;

import lombok.Data;
@Data
@Document(collection = "Userpwdencrypt")
@JsonIgnoreProperties(value = { "target" })
public class Userpwdencrypt {
	
	@NotNull
	protected String userName;
	@NotNull
	protected String password ;
	protected String encryptedPwd ;
	
	public Userpwdencrypt(String id, @NotNull String userName, @NotNull String password, String encryptedPwd) {
		super();
		
		this.userName = userName;
		this.password = password;
		this.encryptedPwd = encryptedPwd;
	}

	public Userpwdencrypt() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Userpwdencrypt [userName=" + userName + ", password=" + password + ", encryptedPwd="
				+ encryptedPwd + "]";
	}

	

	

	
	
	
	
	
	

}
