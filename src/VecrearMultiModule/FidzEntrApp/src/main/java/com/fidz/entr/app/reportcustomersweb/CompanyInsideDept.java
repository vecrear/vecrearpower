package com.fidz.entr.app.reportcustomersweb;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Base;
import com.fidz.base.model.Contact;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "company")
@JsonIgnoreProperties(value = { "target" })
public class CompanyInsideDept extends SimplifiedBase {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	//Company Type: https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country
	@JsonIgnore
	@NotNull
	protected String type;
	@JsonIgnore
	protected String description;
	
	//International Securities Identification Number (ISIN) 
	@JsonIgnore
	protected String isin;
	@JsonIgnore
	protected Address headquarterAddress;
	@JsonIgnore
	protected String website;
	@JsonIgnore
	protected Contact contact;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object parent;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<Object> subsidiaries;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<Object> facilities;
	@JsonIgnore
	@DBRef(lazy = true)
	protected List<Department> departments;

	


	
}
