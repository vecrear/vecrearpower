package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.Department;

import reactor.core.publisher.Mono;

@Repository
public interface DepartmentRepository extends MongoRepository<Department, String>{
	//Mono<Department> findByName(String name);
	Department findByName(String name);
}
