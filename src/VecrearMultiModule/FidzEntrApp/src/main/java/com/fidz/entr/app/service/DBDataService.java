package com.fidz.entr.app.service;

import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.repository.GenericBaseINterface;
import com.fidz.entr.app.config.ClientConnectionManager;
import com.fidz.entr.app.config.DBDataNotFoundException;
import com.fidz.entr.app.model.DBData;
import com.fidz.entr.app.repository.DBDataRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FEADBDataService")
@Component
public class DBDataService {
	private GenericBaseINterface<DBData> genericINterface;
	@Autowired
	public DBDataService(GenericBaseINterface<DBData> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private DBDataRepository  dBDataRepository;
	
	//private UserDataInterface userDataInterface;
	@Autowired
	private MongoTemplate mongoTemplate;
	/*@Autowired
	public DBDataService(UserDataInterface userDataInterface){
		this.userDataInterface=userDataInterface;
	}*/
	public Flux<DBData> getAllDBDatas(){
		
		return dBDataRepository.findAll();
	}
	public Mono<DBData> createDBData(DBData dBData){
		 String schemaName=dBData.getSchemaName();
	    	
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//return dBDataRepository.save(dBData);
		 return this.genericINterface.saveName(dBData);
		
	}
	
	public Mono<DBData> getDBDataById(String id){
		return dBDataRepository.findById(id).switchIfEmpty(Mono.error(new DBDataNotFoundException(id)));
		
	}
	 public Mono<DBData> getDBDataByName(String clientId)  {
		 
		 return dBDataRepository.findByClientId(clientId).switchIfEmpty(Mono.error(
					new DBDataNotFoundException(clientId))); 
	 }
	 
	 
	
    public Mono<ResponseEntity<DBData>> updateDBData(String id, DBData dBData) {
        return dBDataRepository.findById(id)
                .flatMap(existingDBData -> {
                    return dBDataRepository.save(dBData);
                })
                .map(updateDBData -> new ResponseEntity<>(updateDBData, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public Mono<ResponseEntity<Void>> deleteDBData(String id) {
        return dBDataRepository.findById(id)
                .flatMap(existingDBData ->
                dBDataRepository.delete(existingDBData)
                            .then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)))
                )
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    
    public Flux<DBData> streamAllDBDatas() {
        return dBDataRepository.findAll();
    }
    
    
    public Mono<ResponseEntity<Void>> getMongoDBConnections(String clientId) throws UnknownHostException, UnsupportedOperationException  {
		 
    	ClientConnectionManager conn=new ClientConnectionManager();
    	System.out.println("connection establish"+conn.getMyDB(clientId,dBDataRepository));
    	//System.out.println("connection establish"+conn.getMyDB(clientId));
    	//System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiii");
    	//MongoTest.testMong();
    	//MongoTest tt=new MongoTest();
		//tt.useMongoTemplateForCrud();
    	//MongoTest ts=new MongoTest();
    	//ts.useMongoTemplateForCrud();
    	//System.out.println("endddd");
    	//UserData us1=new UserData("satya");
    	//System.out.println("completed AAAAAAAAAAAAAAAAAAAAAAAAAAAAA    ::"+userDataInterface.saveName(us1));
		 //System.out.println("completed AAAAAAAAAAAAAAAAAAAAAAAAAAAAA    :: "+reactiveMongoTemplate.save(us1, "UserData"));
    	return Mono.just(new ResponseEntity<Void>(HttpStatus.OK));
	 }
    

}
