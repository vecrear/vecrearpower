package com.fidz.entr.app.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.DBRef;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
@Data
@JsonIgnoreProperties(value = { "target" })
public class ActivityTarget {
	@DBRef(lazy = true)
	protected CustomerCategory customerCategory;//list
	
	protected CustomerType customerType;


	
	
	
	
}
