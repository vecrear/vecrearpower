package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class PushNotificationRequest {

    private String title="Fidzeal-vTrack";
    private String message;//Web User message from userstatus controller
    private String topic;
    private String token;//Notification Id

    private static PushNotificationRequest instance =  new PushNotificationRequest();
    
    public PushNotificationRequest() {
    }

    public PushNotificationRequest(String title, String messageBody, String topicName) {
        this.title = title;
        this.message = messageBody;
        this.topic = topicName;
    }
    
    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public PushNotificationRequest(String title, String message, String topic, String token) {
		super();
		this.title = title;
		this.message = message;
		this.topic = topic;
		this.token = token;
	}

	public static PushNotificationRequest getInstance() {
		return instance;
	}

	public static void setInstance(PushNotificationRequest instance) {
		PushNotificationRequest.instance = instance;
	}


}
