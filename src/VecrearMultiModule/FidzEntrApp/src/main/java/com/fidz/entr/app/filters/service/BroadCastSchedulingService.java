package com.fidz.entr.app.filters.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.Schedules;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.filters.model.BroadCastScheduler;
import com.fidz.entr.app.filters.model.BroadCastTemplate;
import com.fidz.entr.app.filters.model.Modes;
import com.fidz.entr.app.filters.model.SchedulePeriod;
import com.fidz.entr.app.filters.model.UserInfo;
import com.fidz.entr.app.filters.repository.BroadCastSchedulerRepository;
import com.fidz.entr.app.model.PushNotificationRequest;
import com.fidz.entr.app.repository.ScheduleNotificationRepository;

@Service
public class BroadCastSchedulingService {

    private GenericAppINterface<BroadCastScheduler> genericINterface;
    @Autowired
    public BroadCastSchedulingService(GenericAppINterface<BroadCastScheduler> genericINterface){
        this.genericINterface=genericINterface;   
        }
    @Autowired
    BroadCastSchedulerService broadcast;
   
    @Autowired
    ScheduleNotificationRepository scheduleNotificationRepository;

     @Autowired
     private  FCMServic fcmServic;
     
     @Autowired
    private MailService mailService;
     
     @Autowired
      private MongoTemplate mongoTemplate;
     
     @Autowired
     private TotalCountService totalcountService;
   
     @Autowired
     BroadCastSchedulerRepository broadcasting;
     
     @Scheduled(fixedRate = 30000)
     public String broadCastScheduling() throws ParseException, IOException {
           PushNotificationRequest ppp = PushNotificationRequest.getInstance();
           DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"); 
           DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
           Query q = new Query();
           List<BroadCastScheduler> broadCastSchedulers= null;
         //departmentService.getAllTimeZoneInfos(schemaName)
           //format.setTimeZone(TimeZone.getTimeZone("GMT"));
          // LocalDateTime now = LocalDateTime.now(ZoneId.of("GMT+05:30"));
           LocalDateTime now = LocalDateTime.now();
         //  Constant.LOGGER.info("LocalDateTime getDayOfMonth-------"+now.getDayOfMonth());
         //  Constant.LOGGER.info("LocalDateTime getDayOfYear-------"+now.getDayOfYear());
         //  Constant.LOGGER.info("LocalDateTime getYear-------"+now.getYear());
          // Constant.LOGGER.info("LocalDateTime getHour-------"+now.getHour());
          // Constant.LOGGER.info("LocalDateTime getMinute-------"+now.getMinute());
         //  Constant.LOGGER.info("LocalDateTime getSecond-------"+now.getSecond());
          // Constant.LOGGER.info("LocalDateTime getMonthValue-------"+now.getMonthValue());
           String d = dtf.format(now);
           Date endDate = format.parse(d);
           long epoch = endDate.getTime();
           Calendar c = Calendar.getInstance();
           
            
          Date de = c.getTime();
          String dates = String.valueOf(de);
         // Constant.LOGGER.info("dates------dates-----"+dates);
          String epochTime = String.valueOf(epoch);
         // Constant.LOGGER.info("epochTime------epochTime-----"+epochTime);
          
          MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin"); 
          
         // q.addCriteria(Criteria.where("scheduleDateTime").in(epochTime));
          q.addCriteria(Criteria.where("scheduleDateTime").is(epochTime));
          Constant.LOGGER.info("Query------------------"+q);
          
          broadCastSchedulers = this.mongoTemplate.find(q, BroadCastScheduler.class);
          
          Constant.LOGGER.info(" broadCastSchedulers.size()----;"+ broadCastSchedulers.size());
         
          Constant.LOGGER.info("*********Time sceduler running Calendar*********"+dates);
          Constant.LOGGER.info("*********Time sceduler running*********"+String.valueOf(epoch));
          Constant.LOGGER.info("schedlerList*****"+broadCastSchedulers);
        if(broadCastSchedulers.size()>0) {
                Constant.LOGGER.info("Found matching Date");
            for(int i=0;i<broadCastSchedulers.size();i++) {
            	if(broadCastSchedulers.get(i).getMode().equals(Modes.email)) {
                     if(broadCastSchedulers.get(i).getEmailSent().equals("false")) {
                     for(int j=0;j<broadCastSchedulers.get(i).getUsersList().size();j++) {
                     Constant.LOGGER.info("send email"); 
                     String email = broadCastSchedulers.get(i).getUsersList().get(j).getEmail();
                     String message = broadCastSchedulers.get(i).getMessage();
                     String subject = broadCastSchedulers.get(i).getSubject();
                     Thread geoThread = new Thread() {
                        public void run() {
                               try {     
                                mailService.sendEmail(email, subject, message);
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                            e.printStackTrace();
                            }   
                        } 
                    };
                    geoThread.start();
                 }
                        if(broadCastSchedulers.get(i) != null) {
                            BroadCastScheduler broadCastScheduler = new BroadCastScheduler();
                             String dsr  = null;
                              String dateconfg = null;
                             switch (broadCastSchedulers.get(i).getSchedules()) {
                           
                             case daily:
                    
                                 Constant.LOGGER.info("inside DAILY for Mode Email");

                                 broadCastScheduler.setId(broadCastSchedulers.get(i).getId());
                                 broadCastScheduler.setCreatedUserId(broadCastSchedulers.get(i).getCreatedUserId());
                                 broadCastScheduler.setCreatedUserName(broadCastSchedulers.get(i).getCreatedUserName());
                                 broadCastScheduler.setEmailSent(broadCastSchedulers.get(i).getEmailSent());
                                 broadCastScheduler.setMessage(broadCastSchedulers.get(i).getMessage());
                                 broadCastScheduler.setMode(broadCastSchedulers.get(i).getMode());
                                 dsr  = broadCastSchedulers.get(i).getScheduleDateTime();
                                 dateconfg = dateConfigurator(dsr, SchedulePeriod.daily);
                                 broadCastScheduler.setScheduleDateTime(dateconfg);
                                 broadCastScheduler.setSchedules(SchedulePeriod.daily);
                                 broadCastScheduler.setSchemaName("VTrackSuperadmin");
                                 broadCastScheduler.setEmailSent("false");
                                 broadCastScheduler.setSubject(broadCastSchedulers.get(i).getSubject());
                                 broadCastScheduler.setTemplate(broadCastSchedulers.get(i).getTemplate());
                                 broadCastScheduler.setTemplateName(broadCastSchedulers.get(i).getTemplateName());
                                 broadCastScheduler.setUsersList(broadCastSchedulers.get(i).getUsersList());
                                 broadcasting.save(broadCastScheduler);

                                break;
                                
                             case monthly:
                                 Constant.LOGGER.info("inside MONTHLY for Mode Email");

                                 broadCastScheduler.setId(broadCastSchedulers.get(i).getId());
                                 broadCastScheduler.setCreatedUserId(broadCastSchedulers.get(i).getCreatedUserId());
                                     broadCastScheduler.setCreatedUserName(broadCastSchedulers.get(i).getCreatedUserName());
                                     broadCastScheduler.setEmailSent(broadCastSchedulers.get(i).getEmailSent());
                                     broadCastScheduler.setMessage(broadCastSchedulers.get(i).getMessage());
                                     broadCastScheduler.setMode(broadCastSchedulers.get(i).getMode());
                                     dsr  = broadCastSchedulers.get(i).getScheduleDateTime();
                                     dateconfg = dateConfigurator(dsr, SchedulePeriod.monthly);
                                     broadCastScheduler.setScheduleDateTime(dateconfg);
                                     broadCastScheduler.setSchedules(SchedulePeriod.monthly);
                                     broadCastScheduler.setSchemaName("VTrackSuperadmin");
                                     broadCastScheduler.setEmailSent("false");
                                     broadCastScheduler.setSubject(broadCastSchedulers.get(i).getSubject());
                                     broadCastScheduler.setTemplate(broadCastSchedulers.get(i).getTemplate());
                                     broadCastScheduler.setTemplateName(broadCastSchedulers.get(i).getTemplateName());
                                     broadCastScheduler.setUsersList(broadCastSchedulers.get(i).getUsersList());
                                     broadcasting.save(broadCastScheduler);
                                
                                    
                                     break;
                                
                             case weekly:
                                 Constant.LOGGER.info("inside WEEKLY for Mode Email");

                                 broadCastScheduler.setId(broadCastSchedulers.get(i).getId());
                                 broadCastScheduler.setCreatedUserId(broadCastSchedulers.get(i).getCreatedUserId());
                                     broadCastScheduler.setCreatedUserName(broadCastSchedulers.get(i).getCreatedUserName());
                                     broadCastScheduler.setEmailSent(broadCastSchedulers.get(i).getEmailSent());
                                     broadCastScheduler.setMessage(broadCastSchedulers.get(i).getMessage());
                                     broadCastScheduler.setMode(broadCastSchedulers.get(i).getMode());
                                     dsr  = broadCastSchedulers.get(i).getScheduleDateTime();
                                     dateconfg = dateConfigurator(dsr, SchedulePeriod.weekly);
                                     broadCastScheduler.setScheduleDateTime(dateconfg);
                                     broadCastScheduler.setSchedules(SchedulePeriod.weekly);
                                     broadCastScheduler.setSchemaName("VTrackSuperadmin");
                                     broadCastScheduler.setEmailSent("false");
                                     broadCastScheduler.setSubject(broadCastSchedulers.get(i).getSubject());
                                     broadCastScheduler.setTemplate(broadCastSchedulers.get(i).getTemplate());
                                     broadCastScheduler.setTemplateName(broadCastSchedulers.get(i).getTemplateName());
                                     broadCastScheduler.setUsersList(broadCastSchedulers.get(i).getUsersList());
                                     broadcasting.save(broadCastScheduler);
                                 break;
                                
                             case none:
                                     Constant.LOGGER.info("inside NONE for Mode Email");
                                     broadcast.deleteBroadCastSchedulerById(broadCastSchedulers.get(i).getId(),"VTrackSuperadmin");
                                 
                                 break;

                               default:
                                
                                 break;
                                
                             }    
                        }else {
                            Constant.LOGGER.info("EMail is null");
                        }
                       
                }
                    
                       
               }else if(broadCastSchedulers.get(i).getMode().equals(Modes.sms)) {
                if(broadCastSchedulers.get(i).getSmsSent().equals("false")) {
                    for(int k=0;k<broadCastSchedulers.get(i).getUsersList().size();k++) {
                        
   
                         Constant.LOGGER.info("send sms");
                         String phone = broadCastSchedulers.get(i).getUsersList().get(k).getPhoneNo();
                         String message = broadCastSchedulers.get(i).getMessage();
                          Thread geoThread = new Thread() {
                            public void run() {
                                   try {     
                                    String str =totalcountService.sendSms("VTrackSuperAdmin", phone, message);
                                    Constant.LOGGER.info("SMS Response"+" "+str);
                                    Thread.sleep(10);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }   
                            } 
                        };
                        geoThread.start();   

                      }
                    if(broadCastSchedulers.get(i) != null) {
                        BroadCastScheduler broadCastScheduler = new BroadCastScheduler();
                         String dsr  = null;
                          String dateconfg = null;
                         switch (broadCastSchedulers.get(i).getSchedules()) {
                       
                         case daily:
                
                             Constant.LOGGER.info("inside DAILY for Mode SMS");

                             broadCastScheduler.setId(broadCastSchedulers.get(i).getId());
                             broadCastScheduler.setCreatedUserId(broadCastSchedulers.get(i).getCreatedUserId());
                             broadCastScheduler.setCreatedUserName(broadCastSchedulers.get(i).getCreatedUserName());
                             broadCastScheduler.setEmailSent(broadCastSchedulers.get(i).getSmsSent());
                             broadCastScheduler.setMessage(broadCastSchedulers.get(i).getMessage());
                             broadCastScheduler.setMode(broadCastSchedulers.get(i).getMode());
                             dsr  = broadCastSchedulers.get(i).getScheduleDateTime();
                             dateconfg = dateConfigurator(dsr, SchedulePeriod.daily);
                             broadCastScheduler.setScheduleDateTime(dateconfg);
                             broadCastScheduler.setSchedules(SchedulePeriod.daily);
                             broadCastScheduler.setSchemaName("VTrackSuperadmin");
                             broadCastScheduler.setEmailSent("false");
                             broadCastScheduler.setSubject(broadCastSchedulers.get(i).getSubject());
                             broadCastScheduler.setTemplate(broadCastSchedulers.get(i).getTemplate());
                             broadCastScheduler.setTemplateName(broadCastSchedulers.get(i).getTemplateName());
                             broadCastScheduler.setUsersList(broadCastSchedulers.get(i).getUsersList());
                             broadcasting.save(broadCastScheduler);

                            break;
                            
                         case monthly:
                             Constant.LOGGER.info("inside MONTHLY for Mode SMS");

                             broadCastScheduler.setId(broadCastSchedulers.get(i).getId());
                             broadCastScheduler.setCreatedUserId(broadCastSchedulers.get(i).getCreatedUserId());
                                 broadCastScheduler.setCreatedUserName(broadCastSchedulers.get(i).getCreatedUserName());
                                 broadCastScheduler.setSmsSent(broadCastSchedulers.get(i).getSmsSent());
                                 broadCastScheduler.setMessage(broadCastSchedulers.get(i).getMessage());
                                 broadCastScheduler.setMode(broadCastSchedulers.get(i).getMode());
                                 dsr  = broadCastSchedulers.get(i).getScheduleDateTime();
                                 dateconfg = dateConfigurator(dsr, SchedulePeriod.monthly);
                                 broadCastScheduler.setScheduleDateTime(dateconfg);
                                 broadCastScheduler.setSchedules(SchedulePeriod.monthly);
                                 broadCastScheduler.setSchemaName("VTrackSuperadmin");
                                 broadCastScheduler.setSmsSent("false");
                                 broadCastScheduler.setSubject(broadCastSchedulers.get(i).getSubject());
                                 broadCastScheduler.setTemplate(broadCastSchedulers.get(i).getTemplate());
                                 broadCastScheduler.setTemplateName(broadCastSchedulers.get(i).getTemplateName());
                                 broadCastScheduler.setUsersList(broadCastSchedulers.get(i).getUsersList());
                                 broadcasting.save(broadCastScheduler);
                            
                                
                                 break;
                            
                         case weekly:
                             Constant.LOGGER.info("inside WEEKLY for Mode SMS");

                             broadCastScheduler.setId(broadCastSchedulers.get(i).getId());
                             broadCastScheduler.setCreatedUserId(broadCastSchedulers.get(i).getCreatedUserId());
                                 broadCastScheduler.setCreatedUserName(broadCastSchedulers.get(i).getCreatedUserName());
                                 broadCastScheduler.setSmsSent(broadCastSchedulers.get(i).getSmsSent());
                                 broadCastScheduler.setMessage(broadCastSchedulers.get(i).getMessage());
                                 broadCastScheduler.setMode(broadCastSchedulers.get(i).getMode());
                                 dsr  = broadCastSchedulers.get(i).getScheduleDateTime();
                                 dateconfg = dateConfigurator(dsr, SchedulePeriod.weekly);
                                 broadCastScheduler.setScheduleDateTime(dateconfg);
                                 broadCastScheduler.setSchedules(SchedulePeriod.weekly);
                                 broadCastScheduler.setSchemaName("VTrackSuperadmin");
                                 broadCastScheduler.setSmsSent("false");
                                 broadCastScheduler.setSubject(broadCastSchedulers.get(i).getSubject());
                                 broadCastScheduler.setTemplate(broadCastSchedulers.get(i).getTemplate());
                                 broadCastScheduler.setTemplateName(broadCastSchedulers.get(i).getTemplateName());
                                 broadCastScheduler.setUsersList(broadCastSchedulers.get(i).getUsersList());
                                 broadcasting.save(broadCastScheduler);
                             break;
                            
                         case none:
                                 Constant.LOGGER.info("inside NONE for Mode SMS");
                             
                            broadcast.deleteBroadCastSchedulerById(broadCastSchedulers.get(i).getId(),"VTrackSuperadmin");
                             
                             break;

                           default:
                            
                             break;
                            
                         }    
                       
                    }else {
                        Constant.LOGGER.info("SMS is null");
                    }
                    

                }

            }
            }
                }else {
                           Constant.LOGGER.error("its not of any type");
                           }
      
return "success";
   
       
       
    }

    private static String dateConfigurator(String dsr, SchedulePeriod sc) {
        long timeInMillis = 0;
        LocalDateTime date =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.valueOf(dsr)), ZoneId.systemDefault());
        if(sc.equals(SchedulePeriod.daily)) {
            Constant.LOGGER.info("inside date configurator DAILY");
            LocalDateTime minusminutes = date.plusDays(1);
            Instant instant = minusminutes.atZone(ZoneId.systemDefault()).toInstant();   
         timeInMillis = instant.toEpochMilli();
        }
       
        if(sc.equals(SchedulePeriod.monthly)) {
            Constant.LOGGER.info("inside date configurator MONTHLY");
              LocalDateTime minusminutes = date.plusMonths(1);
              Instant instant = minusminutes.atZone(ZoneId.systemDefault()).toInstant();   
             timeInMillis = instant.toEpochMilli();
        }
        if(sc.equals(SchedulePeriod.weekly)) {
            Constant.LOGGER.info("inside date configurator WEEKLY");

              LocalDateTime minusminutes = date.plusWeeks(1);
              Instant instant = minusminutes.atZone(ZoneId.systemDefault()).toInstant();   
             timeInMillis = instant.toEpochMilli();
        }
        return String.valueOf(timeInMillis);
       
    }


    }