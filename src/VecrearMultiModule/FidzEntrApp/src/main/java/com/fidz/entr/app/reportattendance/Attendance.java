package com.fidz.entr.app.reportattendance;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Attendance {
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	
	@LastModifiedBy
	protected String updatedByUser;
	
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	
	@LastModifiedBy
	protected String deletedByUser;
	
	@NotNull
	protected Status status;
	@NotNull
	protected String schemaName;
	
	@Id
	protected String id;
	
	@DBRef(lazy = true)
	protected User user;
	
	//protected long calenderDatestamp;
	
	//protected long punchInDateTimeStamp;
    protected Date calenderDatestamp;
	
	protected Date punchInDateTimeStamp;
	
	@DBRef(lazy = true)
	protected Location punchInLocation;
	
	//protected long punchOutDateTimeStamp;
	protected Date punchOutDateTimeStamp;
	
	@DBRef(lazy = true)
	protected Location punchOutLocation;
	
	protected String workingDuration;
	
	protected List<LogOffTime> logOffTime;
	@DBRef(lazy = true)
	protected List<IdleTime> idleTime;
	protected boolean punchInPerTheDay;


	


	
}
