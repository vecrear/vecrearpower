package com.fidz.entr.app.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class ResetPassword extends com.fidz.services.authentication.model.ResetPassword{public ResetPassword(String id, @NotNull String userName, @NotNull String password) {
		super(id, userName, password);
		// TODO Auto-generated constructor stub
	}

@Override
public String toString() {
	return "ResetPassword [id=" + id + ", userName=" + userName + ", password=" + password + "]";
}




}
