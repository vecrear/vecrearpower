package com.fidz.entr.app.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "customerTypefield")
@JsonIgnoreProperties(value = { "target" })
public class CustomerTypeField extends Base{
	
	@Id
	protected String id;
	
	@NotNull
	protected String name;
	
	protected CustomerTypeCustomField customerTypeCustomField;

	public CustomerTypeField(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name,
			CustomerTypeCustomField customerTypeCustomField) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.customerTypeCustomField = customerTypeCustomField;
	}

	@Override
	public String toString() {
		return "CustomerTypeField [id=" + id + ", name=" + name + ", customerTypeCustomField=" + customerTypeCustomField
				+ ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp="
				+ updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp
				+ ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}



	

	
	
}
