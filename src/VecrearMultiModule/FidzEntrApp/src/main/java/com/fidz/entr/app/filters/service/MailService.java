package com.fidz.entr.app.filters.service;


import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import com.fidz.base.validator.Constant;


@Service("MService")
public class MailService {

	
	
	private JavaMailSender javaMailSender;

	@Autowired
	public MailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;

	}
	
	public void sendEmail(String email, String subject, String message) throws MailException {
       
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(email);
		mail.setSubject(subject);
		mail.setText(message);
        javaMailSender.send(mail);
    }

	public void sendEmailWithAttachment(String email, String subject, String message, String userid,String userMobile,String schemaNames) throws MailException, MessagingException {
		  Constant.LOGGER.info("Inside sendEmailWithAttachment"+email);
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
	        helper.setTo(email);
			helper.setSubject(subject);
			helper.setText(message);
			String s = "alpha_reports_";
	        String r = s.concat(userMobile);
			//FileSystemResource file = new FileSystemResource("C:\\alphareports.xlsx");
	        String filename = "/home/ubuntu/apps/test-mail/"+schemaNames+"/";
			FileSystemResource file = new FileSystemResource(filename+r+".xlsx");
			helper.addAttachment(file.getFilename(), file);
			javaMailSender.send(mimeMessage);
	
	}
	
	

}
