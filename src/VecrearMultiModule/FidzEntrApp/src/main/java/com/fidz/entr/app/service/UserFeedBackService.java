package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.UserFeedBack;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.app.repository.UserFeedBackRepository;
import com.fidz.entr.base.exception.NameFoundException;


@Service("UserFeedBackService")
public class UserFeedBackService {
	
	private GenericAppINterface<UserFeedBack> genericINterface;
	@Autowired
	public UserFeedBackService(GenericAppINterface<UserFeedBack> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private UserFeedBackRepository userFeedBackRepository;
	public List<UserFeedBack> getAllUserFeedBacks(String schemaName) {
	  	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	Constant.LOGGER.info("Inside getAllUserRoles UserRoleService.java");
	   	List<UserFeedBack> userFeedBack=null;
	   	try {
	   	userFeedBack=this.genericINterface.findAll(UserFeedBack.class);
	   	}catch(Exception ex) {
	   	Constant.LOGGER.error("Error while getting data from  UserFeedBack.java"+ex.getMessage());
	       }
	   	Constant.LOGGER.info("**************************************************************************");
	   	Constant.LOGGER.info("successfully fetched data from UserFeedBack.java");
	   	return userFeedBack;
	}
	public List<UserFeedBack> getAllUserFeedBackPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserRolesPaginated UserRoleService.java");
    	Query query = new Query();
    	List<UserFeedBack> userFeedBack =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	userFeedBack= this.genericINterface.find(query, UserFeedBack.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserRolesPaginated userFeedBack.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success userFeedBack "+query);
        return userFeedBack;
	}
	public UserFeedBack createUserFeedBack(@Valid UserFeedBack userFeedBack) {
		UserFeedBack usFeedBack=null;
      	
		Constant.LOGGER.info(" Inside UserFeedBack.java Value for insert UserRole record: UserFeedBack  :: " + usFeedBack);
		
		String schemaName=userFeedBack.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    usFeedBack=this.userFeedBackRepository.save(userFeedBack);
	    
	    //--------------------TO CHECK THE SAME IN SAME UserRole--------IN SAME Department
	    
//	    Query query = new Query();
//    	List<UserFeedBack> usList = null;
//    	
//		query.addCriteria(Criteria.where("userName").is(userFeedBack.getUserName()));
//    	
//		usList= this.genericINterface.find(query, UserFeedBack.class);
//	
//    		if(usList.size()>0) {
//    			List<String> names = usList.stream().map(p->p.getName()).collect(Collectors.toList());
//        		Constant.LOGGER.info("data"+names);
//       	   	 
//    	   		if (names.stream().anyMatch(s -> s.equals(userFeedBack.getName())==true)) {
//    	   			Constant.LOGGER.info("Inside if statement");
//    	   			throw new NameFoundException(userFeedBack.getName()+" "+"cannot use same name in same department");
//    	   	 }
//    	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(userFeedBack.getName())==true)) {
//    	   				Constant.LOGGER.info("Inside else if statement");
//    	   	     throw new NameFoundException(userFeedBack.getName()+"  "+"with different case");
//    	   		}else{
//    	   			usFeedBack = this.userFeedBackRepository.save(userFeedBack);
//    	    	    
//    	   		}
//    			
//    		}else {
//    			
//    			usFeedBack=this.userFeedBackRepository.save(userFeedBack);
//    		}

   return usFeedBack;
	}
	public UserFeedBack getUserFeedBack(String userName, String schemaName) {
	  	Constant.LOGGER.info("Inside getUserRoleById UserRoleService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(userName, UserFeedBack.class);
	}
	public UserFeedBack updateUserFeedBack(String id, @Valid UserFeedBack userFeedBack) {
	      String schemaName=userFeedBack.getSchemaName();
		   	Constant.LOGGER.info("Inside userFeedBack userFeedBack.java");
		
		    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		    userFeedBack.setId(id);
		    return this.userFeedBackRepository.save(userFeedBack);
	}
	public UserFeedBack getUserFeedBackById(String id, String schemaName) {
		Constant.LOGGER.info("Inside getUserFeedBackById UserFeedBack.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, UserFeedBack.class);
	}

	public Map<String, String> deleteUserFeedBackById(String id, String schemaName) {
		
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	Constant.LOGGER.info("Inside deleteUserFeedBackById deleteUserFeedBack.java");
        this.userFeedBackRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "deleted successfully");
		return response;
	}
	public List<UserFeedBack> getAllUserFeedBacksByMode(String mode, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserRolesPaginated UserRoleService.java");
    	Query query = new Query();
    	Query query1 = new Query();
    	List<UserFeedBack> userFeedBack =null;
    	try {
    	if(mode.equals("android")) {
    		query.addCriteria(Criteria.where("mode").is(mode));
    		userFeedBack= this.genericINterface.find(query, UserFeedBack.class);
    	}else if(mode.equals("web")) {
    		query1.addCriteria(Criteria.where("mode").is(mode));
    		userFeedBack= this.genericINterface.find(query1, UserFeedBack.class);
    	}
    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserRolesPaginated userFeedBack.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success userFeedBack "+query);
        return userFeedBack;
	}

	
	
	
	
	

}
