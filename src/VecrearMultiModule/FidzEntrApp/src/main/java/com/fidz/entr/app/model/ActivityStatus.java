package com.fidz.entr.app.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum ActivityStatus {
	OPEN, POSTPONED, COMPLETED, CANCELLED
}
