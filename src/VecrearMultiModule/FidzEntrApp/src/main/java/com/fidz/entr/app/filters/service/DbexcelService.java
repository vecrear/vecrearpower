package com.fidz.entr.app.filters.service;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.Location;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.service.AttendanceService;
import com.fidz.entr.app.service.DeviceService;
import com.fidz.entr.app.service.UserService;
import com.fidz.entr.base.model.Device;
import com.fidz.services.location.model.GpsData;
import com.fidz.services.location.model.TravelType;
import com.fidz.services.location.service.GpsDataService;

@Service
public class DbexcelService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private AttendanceService attendanceService;
	
	@Autowired
	private GpsDataService gpsDataService;
	
	public  Attendance createAttendance(String schemaName) throws IOException{
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Attendance attend = null;
		try {
		Constant.LOGGER.info("inside createAttendance ExceltoDbService.java");
			FileInputStream inp = new FileInputStream("E:\\Data Migration\\NewDataMig\\attendancedemo.xlsx");
		Workbook wb = new XSSFWorkbook(inp); 
		Sheet sheet = wb.getSheet("Sheet1");
		Iterator<Row> rows = sheet.iterator();
	   List<Attendance> attendances2 = new ArrayList<Attendance>();
		 int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();

	        // skip header
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }

	        Iterator<Cell> cellsInRow = currentRow.iterator();
		
	        Attendance attendance = new Attendance();
	        Attendance attendance1 = new Attendance();
	        
	        
	       Location lc = new Location();
	       Location lc1 = new Location();
	        
	        int cellIdx = 0;
	        while (cellsInRow.hasNext()) {
	        Cell currentCell = cellsInRow.next();
	        
	        switch (cellIdx) {
			case 2:
				Constant.LOGGER.info("case 2"+currentCell.getDateCellValue() );
				Date date = currentCell.getDateCellValue();
				
//				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
//				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
//				LocalDateTime localDateTime1 = date.toInstant().atZone(ZoneId.of("GMT+05:30")).toLocalDateTime();
//		        format.setTimeZone(TimeZone.getTimeZone("GMT"));
//		        String dss = dtf.format(localDateTime1);
//		        Date endDate = format.parse(dss);
//		        Constant.LOGGER.info("Format"+format);
		       // Date dss = format.parse(source)
		          
				//Instant inst = getDate(currentCell.getDateCellValue());
				//Date date1 = Date.from(inst);
				//LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.of("IST")).toLocalDateTime();
               //  Date de = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()); 
				attendance.setCreatedTimestamp(date);
			     	attendance.setCalenderDatestamp(date);
			     	attendance.setPunchInDateTimeStamp(date);
				break;

			case 7:
				Constant.LOGGER.info("case 7"+currentCell.getNumericCellValue() );

		     	lc.setLatitude(currentCell.getNumericCellValue());
			break;
			
			case 8:
				Constant.LOGGER.info("case 8"+currentCell.getNumericCellValue() );

		     	lc.setLongitude(currentCell.getNumericCellValue());
			break;
			
			case 9:
				Constant.LOGGER.info("case 9"+currentCell.getStringCellValue());
		     	lc.setAddress(currentCell.getStringCellValue());
			break;
	

			case 3:
				
				Constant.LOGGER.info("case 3"+currentCell.getDateCellValue() );
				Date dates = currentCell.getDateCellValue();
			    attendance1.setPunchOutDateTimeStamp(dates);
			     
				break;

			case 11:
				Constant.LOGGER.info("case 11"+currentCell.getNumericCellValue() );

		     	lc1.setLatitude(currentCell.getNumericCellValue());
			break;
			
			case 12:
				Constant.LOGGER.info("case 12"+currentCell.getNumericCellValue() );

		     	lc1.setLongitude(currentCell.getNumericCellValue());
			break;
			
			case 13:
				Constant.LOGGER.info("case 13"+currentCell.getStringCellValue());

		     	lc1.setAddress(currentCell.getStringCellValue());
			break;	
			
			case 18:
				currentCell.setCellType(Cell.CELL_TYPE_STRING);
				String d =currentCell.getStringCellValue() ;
				//d.replaceAll("\\.", "");
				
				Constant.LOGGER.info("case 18"+" "+ d);
				Device devID = deviceService.getDeviceDataByDeviceId(d, schemaName);
				lc.setDeviceId(devID);
				User us = userService.getAllUserByuserName(d, schemaName);
				attendance.setSchemaName(schemaName);
				attendance.setCreatedByUser(devID.getCreatedByUser());
				attendance.setUser(us);
				attendance.setPunchInLocation(lc);
				attendance.setPunchInPerTheDay(true);
				
				lc1.setDeviceId(devID);

				attendance1.setPunchOutLocation(lc1);
				attendance1.setPunchInPerTheDay(false);
				break;
			
			
			default:
				break;
			}
	        
	       
	        
		 	      cellIdx++;
	      }
	        if(attendance!=null) 
	        
		 	       attend = attendanceService.createAttendances(attendance,attendance1);
	           
	        //updateAttendance(attend.getId(), schemaName,attend);
	        Constant.LOGGER.info(" created Attendance Id"+ attend.getId());
		 	       
	        
		        }
//	      for (Attendance attendance2 : attendances2) {
//	    	  int i = 1;
//			attend = attendanceService.createAttendance(attendance2);
//			updateAttendance(attend.getId(), schemaName,attend,i);
//			i++;
//		
//	    
//	      }
	      wb.close();
		}catch(Exception ex) {
			Constant.LOGGER.info("Error while creating Attendance"+ ex.getMessage());
		}
		return attend;
	}







@SuppressWarnings("unchecked")
public String createGps(String schemaName) {
	GpsData gpsdata = null;
	try {
		Constant.LOGGER.info("inside createGps ExceltoDbService.java");
			FileInputStream inp = new FileInputStream("E:\\Data Migration\\NewDataMig\\gpsdata.xlsx");
		Workbook wb = new XSSFWorkbook(inp); 
		Sheet sheet = wb.getSheet("Sheet1");
		Iterator<Row> rows = sheet.iterator();
		 int rowNumber = 0;
	      while (rows.hasNext()) {
	        Row currentRow = rows.next();
	        

	        // skip header
	        if (rowNumber == 0) {
	          rowNumber++;
	          continue;
	        }

	        Iterator<Cell> cellsInRow = currentRow.iterator();
		
	        GpsData gps  =  new GpsData();
	       JSONObject jsonObject = new JSONObject();
	       JSONObject jsonObject1 = new JSONObject();
	        
	        
	       
	        int cellIdx = 0;
	        while (cellsInRow.hasNext()) {
	        Cell currentCell = cellsInRow.next();
	        
	        switch (cellIdx) {
			case 2:
				Constant.LOGGER.info("case 2"+currentCell.getNumericCellValue() );
				jsonObject.put("latitude",currentCell.getNumericCellValue());
				

				break;

			case 3:
				Constant.LOGGER.info("case 3"+currentCell.getNumericCellValue() );
				jsonObject.put("longitude",currentCell.getNumericCellValue());
		     	
			break;
			
			case 4:
				Constant.LOGGER.info("case 4"+currentCell.getDateCellValue());
				jsonObject.put("dateTime", currentCell.getDateCellValue());
				gps.setDateTime(currentCell.getDateCellValue());
                
		     	
			break;
			
			case 5:
				Constant.LOGGER.info("case 5"+currentCell.getNumericCellValue());
				jsonObject.put("speed",currentCell.getNumericCellValue());

			break;
			
			case 6:
				Constant.LOGGER.info("case 6"+currentCell.getNumericCellValue());
				jsonObject.put("bearing",currentCell.getNumericCellValue());
            break;
			
			case 7:
				Constant.LOGGER.info("case 7"+currentCell.getNumericCellValue());
				jsonObject.put("accuracy",currentCell.getNumericCellValue());
            break;
	         

			case 8:
				currentCell.setCellType(Cell.CELL_TYPE_STRING);
				Constant.LOGGER.info("case 8"+currentCell.getStringCellValue());
				jsonObject.put("provider",currentCell.getStringCellValue());
				break;

             case 9:
            
				Constant.LOGGER.info("case 8"+currentCell.getStringCellValue());
				jsonObject.put("address",currentCell.getStringCellValue());
				break;
				
             case 10:
 				
            	 Constant.LOGGER.info("case 10"+currentCell.getNumericCellValue() );
                 gps.setDistance(currentCell.getNumericCellValue());
		     
 				break;
 				
             case 11:
 				
            	 Constant.LOGGER.info("case 11"+currentCell.getStringCellValue() );
 				jsonObject.put("gpsFor", currentCell.getStringCellValue());
 				break;

			
			case 14:
				currentCell.setCellType(Cell.CELL_TYPE_STRING);
				String d =currentCell.getStringCellValue() ;
				Constant.LOGGER.info("case 14"+" "+ d);
				Constant.LOGGER.info("case 14"+currentCell.getStringCellValue());
				gps.setSchemaName(schemaName);
				gps.setDeviceID(d);
				gps.setTravelType(TravelType.MOVEMENT);
				User us = userService.getAllUserByuserName(d, schemaName);
				jsonObject.put("userName", d);
				jsonObject.put("userId",us.getId());
				jsonObject.put("firstName",us.getName().getFirstName());
				jsonObject.put("middleName",us.getName().getMiddleName());
				jsonObject.put("lastName",us.getName().getLastName());
				jsonObject1.put("map", jsonObject);
				gps.setGpsData(jsonObject1);
			break;
			
			
			
			default:
				break;
			}
	        cellIdx++;
	      }
if(gps!=null)
	gpsDataService.createGpsData(gps);

	      wb.close();
	      }
		}catch(Exception ex) {
			Constant.LOGGER.info("Error while creating Gpsdata"+ ex.getMessage());
		}
		
	
	
	return "success";
}





@SuppressWarnings("unchecked")
public String addmatcheddata(String schemaName) {
	try {
		Constant.LOGGER.info("inside createActivity ExceltoDbService.java");
		FileInputStream inp = new FileInputStream("E:\\Data Migration\\Activity Reports\\question.xlsx");
	    Workbook wb = new XSSFWorkbook(inp); 
	    FileInputStream inps = new FileInputStream("E:\\Data Migration\\Activity Reports\\jobdata.xlsx");
	    Workbook wbs = new XSSFWorkbook(inps); 
	    Sheet sheet = wb.getSheet("Sheet1");
	    Iterator<Row> rows = sheet.iterator();
	    Sheet sheets = wbs.getSheet("Sheet1");
	    String cusname = "";
	    String jobdname = "";
	    int rowNumber = 0;
        while (rows.hasNext()) {
        Row currentRow = rows.next();
        

        // skip header
        if (rowNumber == 0) {
          rowNumber++;
          continue;
        }

        Iterator<Cell> cellsInRow = currentRow.iterator();
	 
        
        
       int cellIdx = 0;
        while (cellsInRow.hasNext()) {
        Cell currentCell = cellsInRow.next();
        
        switch (cellIdx) {
        
      case 0:
    	  currentCell.setCellType(Cell.CELL_TYPE_STRING);
      	  Constant.LOGGER.info("case 0"+currentCell.getStringCellValue());
      	  cusname = currentCell.getStringCellValue();
      	  
      	  break;
      case 2:
    	  currentCell.setCellType(Cell.CELL_TYPE_STRING);
          Constant.LOGGER.info("case 2"+currentCell.getStringCellValue());
          jobdname = currentCell.getStringCellValue();
    	  break;
		default:
			break;
		}
        cellIdx++;
      }

	    List<Integer> matchedRows = new ArrayList<>();
	    for (Row row : sheets) {
	    	 for (Cell cell : row) {
	              cell.setCellType(Cell.CELL_TYPE_STRING); 
	            
	                if(cell.getRichStringCellValue().getString().trim().contains(cusname)) {
	                    // return row.getRowNum(); Instead of this...
	               
	                    matchedRows.add(row.getRowNum()); // Add the row to the list of matches
	                }
	            
	    	 }
	    }            
	    
	if(matchedRows.size()>=0) {
	 for(int i : matchedRows) {
		 Constant.LOGGER.info("Matched Rows"+ matchedRows);
		 Constant.LOGGER.info("Row number"+ i);
		// Row row = sheet.getRow(i);
		
		Cell cell = sheets.getRow(i).getCell(3);
		cell.setCellValue(jobdname);	
	 
	
	 }
	 FileOutputStream outputStream = new FileOutputStream(new File("E:\\Data Migration\\Activity Reports\\jobdatass.xlsx"));
     wbs.write(outputStream);
     outputStream.close();
	}
        }
        wbs.close();
        wb.close();
	}catch(Exception ex) {
		Constant.LOGGER.info("error while creating Activities"+ex.getMessage());
	}
	
	return "success";
}


@SuppressWarnings("unchecked")
public String addCities(String schemaName) {
	try {
		Constant.LOGGER.info("inside createActivity ExceltoDbService.java");
		FileInputStream inp = new FileInputStream("E:\\Data Migration\\Activity Reports\\New folder\\user.xlsx");
	    Workbook wb = new XSSFWorkbook(inp); 
	    FileInputStream inps = new FileInputStream("E:\\Data Migration\\Activity Reports\\New folder\\userc.xlsx");
	    Workbook wbs = new XSSFWorkbook(inps); 
	    Sheet sheet = wb.getSheet("Sheet1");
	    Iterator<Row> rows = sheet.iterator();
	    Sheet sheets = wbs.getSheet("Sheet1");
	    String cusname = "";
	    String jobdname = "";
	    int rowNumber = 0;
        while (rows.hasNext()) {
        Row currentRow = rows.next();
        

        // skip header
        if (rowNumber == 0) {
          rowNumber++;
          continue;
        }

        Iterator<Cell> cellsInRow = currentRow.iterator();
	 
        
        
       int cellIdx = 0;
        while (cellsInRow.hasNext()) {
        Cell currentCell = cellsInRow.next();
        
        switch (cellIdx) {
        
      case 0:
    	  currentCell.setCellType(Cell.CELL_TYPE_STRING);
      	  Constant.LOGGER.info("case 0"+currentCell.getStringCellValue());
      	  cusname = currentCell.getStringCellValue();
      	  
      	  break;
      case 5:
    	  currentCell.setCellType(Cell.CELL_TYPE_STRING);
          Constant.LOGGER.info("case 5"+currentCell.getStringCellValue());
          jobdname = currentCell.getStringCellValue();
    	  break;
		default:
			break;
		}
        cellIdx++;
      }

	    List<Integer> matchedRows = new ArrayList<>();
	    for (Row row : sheets) {
	    	 for (Cell cell : row) {
	              cell.setCellType(Cell.CELL_TYPE_STRING); 
	            
	                if(cell.getRichStringCellValue().getString().trim().contains(cusname)) {
	                    // return row.getRowNum(); Instead of this...
	               
	                    matchedRows.add(row.getRowNum()); // Add the row to the list of matches
	                }
	            
	    	 }
	    }            
	    
	
	 for(int i : matchedRows) {
		 Constant.LOGGER.info("Matched Rows"+ matchedRows);
		 Constant.LOGGER.info("Row number"+ i);
		// Row row = sheet.getRow(i);
		
		Cell cell = sheets.getRow(i).getCell(0);
		cell.setCellValue(jobdname);	
	 
	
	 }
	 FileOutputStream outputStream = new FileOutputStream(new File("E:\\Data Migration\\Activity Reports\\New folder\\usersss.xlsx"));
     wbs.write(outputStream);
     outputStream.close();
        }
        wbs.close();
        wb.close();
	}catch(Exception ex) {
		Constant.LOGGER.info("error while creating Activities"+ex.getMessage());
	}
	
	return "success";
}











}
