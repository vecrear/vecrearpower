package com.fidz.entr.app.awsconfig.service;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fidz.base.validator.Constant;

@CrossOrigin(maxAge = 3600)
@RestController("FEAAwsAWSS3Ctrl")
@Configuration
@RequestMapping(value= "/s3")
public class AWSS3Ctrl {
	

	@Autowired
	private AWSS3Service awss3Service;

	@PostMapping(value= "/uploadfipponmedia")
	public ResponseEntity<String> uploadFile() throws IOException {

		String path = "UniqueKey1/vecAromas/9964919566/activity/activityid/images";
		String username = "9964919566";
		String activityid = null;
		String data = null;
		try {
			data = readFileAsString("C:\\Users\\madhu\\Music\\fippon_users\\sample.txt");
			Constant.LOGGER.info("data-------------------:"+data);
		} catch (Exception e1) {
		
			e1.printStackTrace();
		} 
				 byte[] urlImage = getTheConvertedImageFromByteCode(data);
				 Constant.LOGGER.info("base64Content-------------------:"+urlImage.toString());  
				 String url = writeByteToImageFile(urlImage, "image.png",path);
				 Constant.LOGGER.info("url--------url------url-----:"+url);
				 final String response = url+"  "+ "uploaded successfully";
				 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	public static String readFileAsString(String fileName)throws Exception 
	  { 
	    String data = ""; 
	    data = new String(Files.readAllBytes(Paths.get(fileName))); 
	    return data; 
	  } 

	private byte[] getTheConvertedImageFromByteCode(String base64Content) {
		Constant.LOGGER.info("base64Content length-------------------:"+base64Content.length());
		//System.out.println(Base64.decodeBase64(base64Content));  
		return Base64.decodeBase64(base64Content);  
	}
	
//	
//    public static byte[] convertToImg(String base64) throws IOException  
//    {  
//         return Base64.decodeBase64(base64);  
//    }  
    public String writeByteToImageFile(byte[] imgBytes, String imgFileName,String path) throws IOException  
    {  
         File imgFile = new File(imgFileName);  
         BufferedImage img = ImageIO.read(new ByteArrayInputStream(imgBytes));  
         ImageIO.write(img, "png", imgFile); 
         Constant.LOGGER.info("imgFile--------------------------------:"+imgFile.toString());
         FileInputStream input = new FileInputStream(imgFile);
         Constant.LOGGER.info("input----input-----input--input---------input--------:"+input.toString());
         
         //String url = awss3Service.uploadFiles(img);
         
         String url = awss3Service.uploadFiles(img,path);
         Constant.LOGGER.info("urlfile--------------------------------:"+url);
         return url;

    }  }
    
    
    
