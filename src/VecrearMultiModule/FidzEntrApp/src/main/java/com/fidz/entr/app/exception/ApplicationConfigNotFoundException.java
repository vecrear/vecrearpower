package com.fidz.entr.app.exception;

public class ApplicationConfigNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6298946919921170560L;

	public ApplicationConfigNotFoundException(String applicationConfig) {
		super("ApplicationConfig not found with id/name " + applicationConfig);
	}
}
