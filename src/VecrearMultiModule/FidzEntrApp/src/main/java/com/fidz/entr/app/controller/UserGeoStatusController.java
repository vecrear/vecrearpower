package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.UserGeoStatus;
import com.fidz.entr.app.service.UserGeoStatusService;

@RestController("UserGeoStatusController")
@CrossOrigin
@RequestMapping("/usergeostatus")
public class UserGeoStatusController {
	
	@Autowired
	UserGeoStatusService userGeoStatusService;
	
	  @PostMapping("/get/all") 
      public List<UserGeoStatus> getAll(@RequestHeader(value = "schemaName") String schemaName){
		  final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
		  return userGeoStatusService.getAll(schemaName);
     }
   
     
	    @PostMapping("/update/id/{userid}")
	    public UserGeoStatus updateUserGeoStatus(@PathVariable(value = "userid") String userId, @Valid @RequestBody UserGeoStatus userGeoStatus,@RequestHeader(value = "schemaName") String schemaName) {
	       final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userGeoStatusService.updateUserGeoStatus(userId, userGeoStatus,schemaName); 
		
	    }
	 
	    @PostMapping("/delete/id/{userid}")
		public Map<String, String> deleteUserGeoStatus(@PathVariable(value = "userid") String userId,@RequestHeader(value = "schemaName") String schemaName) {
	    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return userGeoStatusService.deleteUserGeoStatus(userId,schemaName);
		}
	

}
