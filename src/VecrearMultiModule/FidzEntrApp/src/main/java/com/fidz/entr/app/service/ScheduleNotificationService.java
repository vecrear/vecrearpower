package com.fidz.entr.app.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.model.Activity;
import com.fidz.entr.app.model.PushNotificationRequest;
import com.fidz.entr.app.model.ScheduleNotification;
import com.fidz.entr.app.repository.ScheduleNotificationRepository;

@Service

public class ScheduleNotificationService {
	
	
	
	private GenericAppINterface<ScheduleNotification> genericINterface;
	@Autowired
	public ScheduleNotificationService(GenericAppINterface<ScheduleNotification> genericINterface){
		this.genericINterface=genericINterface;	
		}
	
	@Autowired
	ScheduleNotificationRepository scheduleNotificationRepository;

	 @Autowired
	 private  FCMServic fcmServic;
	 
	 @Autowired
	  private MongoTemplate mongoTemplate;
	
	@Scheduled(fixedRate = 30000)
	public String activityscheduledNotification() throws ParseException  {
	       PushNotificationRequest ppp = PushNotificationRequest.getInstance(); 
	       DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");  
           DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
           Query q = new Query();
           List<ScheduleNotification> listNotify= null;
         //departmentService.getAllTimeZoneInfos(schemaName)
           //format.setTimeZone(TimeZone.getTimeZone("GMT"));
          // LocalDateTime now = LocalDateTime.now(ZoneId.of("GMT+05:30"));
           LocalDateTime now = LocalDateTime.now();
             String d = dtf.format(now);
             Date endDate = format.parse(d);
             long epoch = endDate.getTime();
             String epochTime = String.valueOf(epoch);
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");     
         q.addCriteria(Criteria.where("beforeTime").is(epochTime));
         Constant.LOGGER.info("Query"+q);
       listNotify = this.mongoTemplate.find(q, ScheduleNotification.class);
	        Constant.LOGGER.info("*********Time sceduler running*********"+String.valueOf(epoch));
	        
	        Constant.LOGGER.info("schedlerList*****"+listNotify);
        if(listNotify.size()>0) {
	        	Constant.LOGGER.info("Found matching Date");
	        for(int i=0;i<listNotify.size();i++) {
		     	if(listNotify.get(i).getObjectType().equals("Activity")) {  
			     	Constant.LOGGER.info("send notification");  
		    	    ppp.setMessage("Perform an activity before this time :"+listNotify.get(i).getNotifyMessage());
					ppp.setTitle("Vecrear-Trac");
				  	ppp.setToken(listNotify.get(i).getNotificationid());
				  	ppp.setTopic("");
				  	Constant.LOGGER.error("user.getNotificationId()----"+ppp.toString());
				  	Thread geoThread = new Thread() {
				    public void run() {
				    	   try {  	
				    		 fcmServic.sendMessageToToken(ppp);
							Thread.sleep(3);
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (Exception e) {
												e.printStackTrace();
						}	
				    }  
				};
		     	
				geoThread.start();
		     	}
	        }
	        	}else {
			     		  Constant.LOGGER.error("its not of any type");
			     		  }
       

	  
       
	      
     
      
	       
	       
	    
			return "success";
	}  
}


