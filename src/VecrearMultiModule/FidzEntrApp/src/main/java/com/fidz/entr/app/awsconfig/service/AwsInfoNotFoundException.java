package com.fidz.entr.app.awsconfig.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AwsInfoNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1757726792093303678L;
	
	public AwsInfoNotFoundException(String name) {
		super("region not found with " + name);
	}


}
