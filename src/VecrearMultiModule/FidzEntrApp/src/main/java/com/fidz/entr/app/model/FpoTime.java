package com.fidz.entr.app.model;

import java.util.Date;
import java.util.TimeZone;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.app.reportusers.Company;
import com.fidz.entr.app.reportusers.Department;
import com.fidz.entr.base.model.DistanceCalculationMethod;
import com.fidz.entr.base.model.PunchInFrequency;

import lombok.Data;

@JsonIgnoreProperties(value = { "target" })
public enum FpoTime {

	DAILY,NONE  //if it is daily 23:59:00

}
