package com.fidz.entr.app.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.service.DbexcelService;
import com.fidz.entr.app.filters.service.ExceltoDbService;
import com.fidz.entr.app.model.Attendance;

@CrossOrigin(maxAge = 3600)			
@RestController
@RequestMapping(value="/exceltodb")
public class ExcelTodbController {
	@Autowired
	private ExceltoDbService exceltoDbService;
	
	@Autowired
	private DbexcelService dbservice;
	
	@PostMapping("/attendance")
    public Attendance excelToDb(@RequestHeader(value="schemaName") String schemaName) throws IOException {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return dbservice.createAttendance(schemaName);
    }
	
	@PostMapping("/gpsdata")
    public String excelToDbgpsdata(@RequestHeader(value="schemaName") String schemaName) throws IOException {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return dbservice.createGps(schemaName);
    }

}
