package com.fidz.entr.app.reportactivities;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.app.reportattendance.Contact;
import com.fidz.entr.app.reportattendance.PersonName;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Customer {
	
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	
	@Id
	protected String id;
	
	@NotNull
	protected String name;
	
	protected String customerId;
	
	protected PersonName contactName;
	
	protected Contact contact;
	
	protected Address address;
	@JsonIgnore
	protected Object customerType;
	
	protected String customerDepartment;
	

	@DBRef(lazy = true)
	protected Company company;
	

	@DBRef(lazy = true)
	protected Department department;
	
	@DBRef(lazy = true)
	protected Country country;
	
	@DBRef(lazy = true)
	protected Region region;
	
	@DBRef(lazy = true)
	protected State state;
	
	@DBRef(lazy = true)
	protected City city;
	
	@DBRef(lazy = true)
	protected CustomerCategory customerCategory;
	
	//optional mapping based on customer type - for regular customer this field is mandatory
	@DBRef(lazy = true)
	protected List<User> users;
	
	protected Map<String, Object> additionalFields;

	
	



	
}
