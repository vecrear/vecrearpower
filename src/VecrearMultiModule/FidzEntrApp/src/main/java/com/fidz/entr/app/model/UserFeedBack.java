package com.fidz.entr.app.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "UserFeedBack")
@JsonIgnoreProperties(value = { "target" })
public class UserFeedBack {	
	
	@Id
	protected String Id;
	
	@NotBlank
	@NotNull
	protected String userName;
	
	@NotBlank
	@NotNull
	protected String name;

	@NotNull
	protected String schemaName;
	
	@DateTimeFormat
	private Date createdDate;
	
	protected String userFeedback;
	
	@NotNull
	protected String mode;
	
	

}
