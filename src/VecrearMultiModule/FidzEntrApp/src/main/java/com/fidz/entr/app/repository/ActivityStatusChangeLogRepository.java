package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.ActivityStatusChangeLog;
@Repository
public interface ActivityStatusChangeLogRepository extends MongoRepository<ActivityStatusChangeLog, String>{

}
