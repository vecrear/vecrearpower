package com.fidz.entr.app.reportactivities;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Base;
import com.fidz.base.model.Contact;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Facility extends Base {
	@Id
	protected String id;
	
	@NotNull
	protected String name;
	
	protected String description;
	
	protected Address address;
	
	protected Contact contact;
	
	@DBRef(lazy = true)
	protected Company company;

	public Facility(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name, String description, Address address,
			Contact contact, Company company) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.description = description;
		this.address = address;
		this.contact = contact;
		this.company = company;
	}

	@Override
	public String toString() {
		return "Facility [id=" + id + ", name=" + name + ", description=" + description + ", address=" + address
				+ ", contact=" + contact + ", company=" + company + ", createdTimestamp=" + createdTimestamp
				+ ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser="
				+ updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser
				+ ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	
	
}
