package com.fidz.entr.app.model;


import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class TimeTamper {
	
	@NotNull
	protected String schemaName;
	
	protected String imei;
	//protected long deviceTime;
	protected String deviceTime;
	
	
	protected String mobile;
	
	protected String userId;
	
	//timeZone
	protected String timeZone;
	
	
	

	
	
	
	
}
