package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.ActivityStatusChangeLogNotFoundException;
import com.fidz.entr.app.model.ActivityStatusChangeLog;
import com.fidz.entr.app.service.ActivityStatusChangeLogService;


@CrossOrigin(maxAge = 3600)
@RestController("FEAActivityStatusChangeLogController")
@RequestMapping(value="/activitystatuschangelogs")
public class ActivityStatusChangeLogController {

	@Autowired
	private ActivityStatusChangeLogService activityStatusChangeLogService;
	
	
	@PostMapping("/get/all")
    public List<ActivityStatusChangeLog> getAllActivityStatusChangeLogs(@RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return activityStatusChangeLogService.getAllActivityStatusChangeLogs(schemaName);
    }
	
    @PostMapping
    public ActivityStatusChangeLog createActivityStatusChangeLog(@Valid @RequestBody ActivityStatusChangeLog activityStatusChangeLog) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return activityStatusChangeLogService.createActivityStatusChangeLog(activityStatusChangeLog);
    }
    
    @PostMapping("/creates")
    public void createActivityStatusChangeLogs(@Valid @RequestBody ActivityStatusChangeLog activityStatusChangeLog) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	activityStatusChangeLogService.createActivityStatusChangeLogs(activityStatusChangeLog);
    }
    
    @PostMapping("/get/id/{id}")
    public ActivityStatusChangeLog getActivityStatusChangeLogById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return activityStatusChangeLogService.getActivityStatusChangeLogById(id, schemaName);
    }
	
	
    @PostMapping("/update/id/{id}")
    public ActivityStatusChangeLog updateActivityStatusChangeLog(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody ActivityStatusChangeLog activityStatusChangeLog) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return activityStatusChangeLogService.updateActivityStatusChangeLog(id, activityStatusChangeLog);
               
    }

   
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteActivityStatusChangeLog(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return activityStatusChangeLogService.deleteActivityStatusChangeLog(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteActivityStatusChangeLog(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return activityStatusChangeLogService.deleteSoftActivityStatusChangeLog(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<ActivityStatusChangeLog> streamAllActivityStatusChangeLogs() {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
    	return activityStatusChangeLogService.streamAllActivityStatusChangeLogs();
    }


    // Exception Handling Examples
   /* @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A ActivityStatusChangeLog with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ActivityStatusChangeLogNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/
	
}
