package com.fidz.entr.app.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.TimeTamper;
import com.fidz.entr.app.model.User;

@Service("FEATimeTamperService")
public class TimeTamperService {
	//@Autowired
	//private TimeTamperRepository  timeTamperRepository;
	
	@Autowired
	private UserService userService;
	
	  public Map<String, Object> checkTimeTamper(TimeTamper timeTamper) {
		  String schemaName = (timeTamper.getSchemaName());
		  Constant.LOGGER.info("Inside checkTimeTamper TimeTamperService.java");
		  Constant.LOGGER.info("Inside checkTimeTamper timeTamper------------"+timeTamper.toString());
		  
       	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//Date strDeviceTime = timeTamper.getDeviceTime();
		String imei = timeTamper.getImei();

		String timeZone = timeTamper.getTimeZone();

		Date date = new Date();
		Long millis = System.currentTimeMillis();


		
		//User userdept = this.userService.getUserById(timeTamper.getUserId(), schemaName);
		DateTime deviceTime = new DateTime(timeTamper.getDeviceTime());
		//DateTime servertime =  new DateTime();
		DateTime now = new DateTime();
		
		DateTime servertime = now.toDateTime(DateTimeZone.forID(timeZone));
		Constant.LOGGER.info("servertime  servertime"+servertime.toString());
		//Constant.LOGGER.info("timeZone"+timeZone.equalsIgnoreCase(userdept.getDepartment().getDeptTimeZone()));

		DateTime added = servertime.plusMinutes(3);
		DateTime subtracted = servertime.minusMinutes(3);

		
		
		Map<String, Object> response = new HashMap<String, Object>();
		if (added.isAfter(deviceTime)) {
			if (deviceTime.isAfter(subtracted)) {
				response.put("message", "DEVICESUCCESS");
				response.put("timetamper", timeTamper);
				return response;
			}else {
				response.put("message", "DEVICEFAILURE");
				response.put("timetamper", timeTamper);
				return response;
			}
		}else {
			response.put("message", "DEVICEFAILURE");
			response.put("timetamper", timeTamper);
			return response;
		}
		  
	    }
	  
	  
//	  String schemaName = (timeTamper.getSchemaName());
//	   	Constant.LOGGER.info("Inside checkTimeTamper TimeTamperService.java");
//		Constant.LOGGER.info("Inside checkTimeTamper timeTamper---"+timeTamper.toString());
// 	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//	
////	Date strDeviceTime = timeTamper.getDeviceTime();
//	
//	 DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
//	String imei = timeTamper.getImei();
//
//	String timeZone = timeTamper.getTimeZone();
//
//	Date date = new Date();
//	Long millis = System.currentTimeMillis();
//
//	DateTime now = new DateTime();
//	
//	 SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
//	 
//	 DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
//	 
//	DateTime captured_dateTime = now.toDateTime(DateTimeZone.forID(timeZone));
//	
//	//Constant.LOGGER.info("servertime  captured_dateTime"+dateFormatter.pacaptured_dateTime.toString());
//	
//
//	DateTime deviceTime = new DateTime(timeTamper.getDeviceTime());
//	//DateTime servertime =  new DateTime(ZoneId.systemDefault());
//	
//	Constant.LOGGER.info("servertime  "+captured_dateTime.toString());
//	Constant.LOGGER.info("servertime  servertime"+captured_dateTime.toString());
//	DateTime added = captured_dateTime.plusMinutes(3);
//	DateTime subtracted = captured_dateTime.minusMinutes(3);
//	
//	
//	Map<String, Object> response = new HashMap<String, Object>();
//	
//
//	
//	
//	if (added.isAfter(deviceTime)) {
//		if (deviceTime.isAfter(subtracted)) {
//			response.put("message", "DEVICESUCCESS");
//			response.put("timetamper", timeTamper);
//			return response;
//		}else {
//			response.put("message", "DEVICEFAILURE");
//			response.put("timetamper", timeTamper);
//			return response;
//		}
//	}else {
//		response.put("message", "DEVICEFAILURE");
//		response.put("timetamper", timeTamper);
//		return response;
//	}
//	  
//  }
	  
	
}
