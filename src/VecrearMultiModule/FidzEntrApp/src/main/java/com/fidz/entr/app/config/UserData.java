package com.fidz.entr.app.config;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fidz.base.model.Group;

import lombok.Data;

@Data
@Document(collection = "userdata")
public class UserData {
	@Id
	protected String id;
	private String name;
	@NotNull
	protected String schemaName;
	
	@DBRef(lazy = true)
	protected List<Group> groups;

	public UserData(String id, String name, @NotNull String schemaName, List<Group> groups) {
		super();
		this.id = id;
		this.name = name;
		this.schemaName = schemaName;
		this.groups = groups;
	}

	@Override
	public String toString() {
		return "UserData [id=" + id + ", name=" + name + ", schemaName=" + schemaName + ", groups=" + groups + "]";
	}
	
	
	

	
	
	
	
	/*public UserData() {
		
	}
	public UserData(String name) {
		super();
		this.name = name;
	}*/
	

}
