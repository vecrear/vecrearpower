package com.fidz.entr.app.filters.service;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.LastpunchinDate;
import com.fidz.entr.app.filters.model.PunchedInCount;
import com.fidz.entr.app.model.Attendance;


@Service("FEAAttendanceFilterService")
public class AttendanceFilterService {

	@Autowired
	MongoTemplate mongoTemplate;
	
	  public List<PunchedInCount> getpunchIncount(String schemaName,String userid) throws ParseException{
		   	 Constant.LOGGER.info("Inside get punchincount()");
		   	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		   	 Instant instant = Instant.ofEpochMilli(new Date().getTime());
		  	 ZonedDateTime TimeinIST = instant.atZone(ZoneId.of("Asia/Kolkata"));
		 	 Constant.LOGGER.info("Date_time"+TimeinIST);
		  	 Date registerTimeBeginDate = Date.from(TimeinIST.toInstant());
		  	 Instant instant1 = Instant.ofEpochMilli(new Date().getTime()+86400000);
		  	 Constant.LOGGER.info("instant 1"+instant1);
		  	 ZonedDateTime TimeinIST1 = instant.atZone(ZoneId.of("Asia/Kolkata"));
		  	 ZonedDateTime zdtNextDay = TimeinIST1.plusDays( 1 );
		 	 Constant.LOGGER.info("Date_time"+zdtNextDay);
		  	 Date registerTimeBeginDate1 = Date.from(zdtNextDay.toInstant());
		  	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			 String strDate = format.format(registerTimeBeginDate);
		     String endDate1 = format.format(registerTimeBeginDate1);
		     Date endDate =format.parse(endDate1+" "+"23:59");

		     Constant.LOGGER.info("strDate"+strDate);
		     Date startDate =format.parse(strDate+" "+"00:00");
		     Constant.LOGGER.info("time"+startDate);
		  	
		 	 Aggregation agg1 = newAggregation( 
		     			match(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(endDate).andOperator(Criteria.where("user.id").is(userid))),
		   		        group("punchInDateTimeStamp","user.id").count().as("punchedIn"),
		                group("punchedIn").count().as("totaltimespunchedInToday"),
		                project("totaltimespunchedInToday").and("punchedIn").previousOperation(),
		                sort(Sort.Direction.DESC,"totaltimespunchedInToday")
		               );
		      
		 	  AggregationResults<PunchedInCount> groupResults = mongoTemplate.aggregate(agg1,Attendance.class,PunchedInCount.class );
		      List<PunchedInCount> result = groupResults.getMappedResults();
		      Constant.LOGGER.info("Aggregate result"+agg1);
		      return result;
		    }
		    
		 
	  
		    public List<LastpunchinDate> getlastpunch(String schemaName,String userid) throws ParseException{
		    	 Constant.LOGGER.info("Inside get getlastpunch()");
		       	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		       	 Instant instant = Instant.ofEpochMilli(new Date().getTime());
		      	 ZonedDateTime TimeinIST = instant.atZone(ZoneId.of("Asia/Kolkata"));
		     	 Constant.LOGGER.info("Date_time"+TimeinIST);
		      	 Date registerTimeBeginDate = Date.from(TimeinIST.toInstant());
		      	 Instant instant1 = Instant.ofEpochMilli(new Date().getTime()+86400000);
		      	 Constant.LOGGER.info("instant 1"+instant1);
		      	 ZonedDateTime TimeinIST1 = instant.atZone(ZoneId.of("Asia/Kolkata"));
		      	 ZonedDateTime zdtNextDay = TimeinIST1.plusDays( 1 );
		     	 Constant.LOGGER.info("Date_time"+zdtNextDay);
		      	 Date registerTimeBeginDate1 = Date.from(zdtNextDay.toInstant());
		      	DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
		    	 String strDate = format.format(registerTimeBeginDate);
		         String endDate1 = format.format(registerTimeBeginDate1);
		         Date endDate =format.parse(endDate1+" "+"23:59");

		         Constant.LOGGER.info("strDate"+strDate);
		         Date startDate =format.parse(strDate+" "+"00:00");
		         Constant.LOGGER.info("time"+startDate);
		         
		       	 Aggregation agg1 = newAggregation(
		       			  match(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(endDate)
		       					  .orOperator(Criteria.where("user.id")).is(userid)),
		       			  group("punchInDateTimeStamp").last("punchInDateTimeStamp").as("lastpunchinDate"),
		       			  project("lastpunchinDate"),
		       			  sort(Sort.Direction.DESC,"lastpunchinDate")
		       			 );
		  
		       	AggregationResults<LastpunchinDate> groupResults = mongoTemplate.aggregate(agg1,Attendance.class,LastpunchinDate.class );
		        List<LastpunchinDate> result = groupResults.getMappedResults().subList(0, 1);
		        Constant.LOGGER.info("Aggregate result"+agg1);
		        Constant.LOGGER.info("Aggregation Results"+result);
		        return result;
		    
		    }

}
