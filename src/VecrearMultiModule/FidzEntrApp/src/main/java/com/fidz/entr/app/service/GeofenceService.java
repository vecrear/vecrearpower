package com.fidz.entr.app.service;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.DeviceNotFoundException;
import com.fidz.base.model.Device;
import com.fidz.base.model.Status;
import com.fidz.base.repository.DeviceRepository;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.GeoFence;
import com.fidz.entr.app.model.Notification;
import com.fidz.entr.app.model.TaggedUsers;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserActivityNotify;
import com.fidz.entr.app.repository.GeofenceRepository;
import com.fidz.entr.app.repository.UserRepository;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.TaggedDevices;
import com.fidz.services.location.exception.GeofenceNotFoundException;


@Service
public class GeofenceService {
	private GenericAppINterface<GeoFence> genericINterface;
	@Autowired
	public GeofenceService(GenericAppINterface<GeoFence> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private GeofenceRepository geofenceRepository;
	@Autowired
	private DeviceRepository deviceRepository;
	@Autowired
	private UserService userService;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private NotificationSendingService notificationSendingService;
	
	private static String name = "GeoFence";
	
    public List<GeoFence> getAllGeofence(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllGeofence GeofenceService.java");
    	List<GeoFence> geoFences=null;
    	try {
    	
    	geoFences=this.genericINterface.findAll(GeoFence.class);
    	}catch(Exception ex){
    	Constant.LOGGER.error("Error while getting data from getAllGeofence GeofenceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllGeofence GeofenceService.java");
    	return geoFences;
    }
	
    
    public GeoFence createGeoFence(GeoFence geofence) {
    	GeoFence geofences=null;
    	Notification noty = new Notification();
    	try {
    		Constant.LOGGER.info(" Inside GeoFenceService.java Value for insert GeoFence record:createFacility :: " + geofence);
			String schemaName = geofence.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			//geofences = this.geofenceRepository.save(geofence);
			
			  //List<GeoFence> applcations = this.genericINterface.findAll(GeoFence.class);
		    	//List<GeoFence> applcations = this.geofenceRepository.findByName(geofence.getName());
		    	 List<GeoFence> applcations =null;
		       	Query q = new Query();
		       	q.addCriteria(Criteria.where("name").regex(geofence.getName(),"i"));
		       	applcations = this.mongoTemplate.find(q, GeoFence.class);
			  List<String> names = applcations.stream().map(p->p.getName()).collect(Collectors.toList());
				Constant.LOGGER.info("Application List data"+names);
			 
				if (names.stream().anyMatch(s -> s.equals(geofence.getName())==true)) {
					Constant.LOGGER.info("Inside if statement");
					throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+geofence.getName());			 
					}
				else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(geofence.getName())==true)) {
						Constant.LOGGER.info("Inside else if statement");
					throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+geofence.getName());			 
				}else{
					Constant.LOGGER.info("Inside else statement");
					
					geofences = this.geofenceRepository.save(geofence);
					
					
			GeoFence geo = this.genericINterface.findById(geofences.getId(), GeoFence.class);		
					User userObject  = userService.getUserById(geofences.getGeofenceOwnerId(), schemaName);
						if(userObject.getUserRoleId()>=2 && userObject.getUserRoleId()<=6) {
					if(geo.getGeofenceOwnerId()!=null) {
						UserActivityNotify userActivityNoty =  new UserActivityNotify();
						String type = geo.getType();
						
						geo.getUsersDeviceList().forEach(user->{
				            userActivityNoty.setNotificationId(user.getNotificationId());
							userActivityNoty.setUserActivityStatus(type+" "+"Geofence has been created to you by"+" "+userObject.getName().getFirstName());
							noty.setMessage(userActivityNoty.getUserActivityStatus());
							noty.setToken(userActivityNoty.getNotificationId());
							String res =this.notificationSendingService.sendNotification(noty);
							Constant.LOGGER.info("Notification Sending Response"+" "+res);
							
							Constant.LOGGER.info("Inside else statement"+user.getId());
							
							User userGeofence = userService.getUserById(user.getId(), schemaName);
							Constant.LOGGER.info("Inside else statement"+userGeofence.isGeofenceTagged());
							userGeofence.setGeofenceTagged(true);
							this.userRepository.save(userGeofence);
							Constant.LOGGER.info("Inside else statement"+user.getId());
								
							
						});
					}
					
			 }
				}
			
    	}catch(Exception e) {
    		Constant.LOGGER.error(" Inside GeoFenceService.java Value for insert GeoFence record:createFacility :: " + e.getMessage());
    	}
       return geofences;
    }
    
    public void createGeoFences(GeoFence geofence) {
        String schemaName=geofence.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside createGeoFences GeofenceService.java");
        this.genericINterface.saveName(geofence);
    }
   
    public GeoFence getGeoFenceById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getGeoFenceById GeofenceService.java");
        return this.genericINterface.findById(id, GeoFence.class);
		
    }
	
    public List<GeoFence> getGeoFenceByDeviceIds(final String deviceId, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getGeoFenceByDeviceIds GeofenceService.java");
       //Device device = this.deviceRepository.findById(deviceId).block();
    	Device device = this.deviceRepository.findByDeviceId(deviceId);
    	//Device device = this.genericINterface.findById(deviceId, GeoFence.class);
    	Query query = new Query();
    	List<GeoFence> geofenceList =null;
    	
    	if (device == null) {
    		return (List<GeoFence>) new DeviceNotFoundException(deviceId);
    	}else {
    		
    		query.addCriteria(Criteria.where("usersDeviceList.device.id").in(deviceId));
    		geofenceList= this.genericINterface.find(query, GeoFence.class);
     
    	}

    	return geofenceList;
		
    }
    
	public List<GeoFence> getGeoFenceByDeviceId(final String deviceId, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getGeoFenceByDeviceId GeofenceService.java");
       //Device device = this.deviceRepository.findByDeviceId(deviceId).block();
    	Query query = new Query();
    	List<GeoFence> geofenceList =null;
    	
    	
    	Device device = this.deviceRepository.findByDeviceId(deviceId);
    	if (device == null) {
    		return (List<GeoFence>) new DeviceNotFoundException(deviceId);
    	}else {
    		
    		query.addCriteria(Criteria.where("usersDeviceList.device.id").in(deviceId));
    		geofenceList= this.genericINterface.find(query, GeoFence.class);
     
    	}
    	
     	return geofenceList;
			

    }
	public List<GeoFence> getGeoFenceByUserId(final String deviceId, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getGeoFenceByDeviceId GeofenceService.java");
       //Device device = this.deviceRepository.findByDeviceId(deviceId).block();
    	Query query = new Query();
    	List<GeoFence> geofenceList =null;
    	
    	    query.addCriteria(Criteria.where("usersDeviceList.id").in(deviceId));
    		geofenceList= this.genericINterface.find(query, GeoFence.class);
     
    	
     	return geofenceList;
			

    }
    
 /*public List<Device> getDeviceListNoGeofence() {
    	List<GeoFence> geofence=geofenceRepository.findAll();
    	HashSet<Device> deviceSet=new HashSet<Device>();
    	((Mono<Device>) geofence).subscribe(geo -> {
    		deviceSet.addAll(geo.getDeviceList());
    	});
    	return deviceRepository.findAll().filter(
    			dev -> !deviceSet.contains(dev));
    }*/
    public Stream<Device> getDeviceListNoGeofence(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getDeviceListNoGeofence GeofenceService.java");
       List<GeoFence> geofence=this.geofenceRepository.findAll();
    	HashSet<Device> deviceSet=new HashSet<Device>();
    	/*((Mono<Device>) geofence).subscribe(geo -> {
    		deviceSet.addAll(geo.getDeviceList());
    	});*/
    	return  this.deviceRepository.findAll().stream().filter(
    			dev -> !deviceSet.contains(dev));
    }
	
    public GeoFence updateGeoFenceData(String id,GeoFence geoFence) {
    	String schemaName=geoFence.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside updateGeoFenceData GeofenceService.java");
    	GeoFence geo = new GeoFence();
    	Notification noty = new Notification();
    	geoFence.setId(id);
    	geo = this.geofenceRepository.save(geoFence);
    	GeoFence geos = this.genericINterface.findById(geoFence.getId(), GeoFence.class);		
		User userObject  = userService.getUserById(geos.getGeofenceOwnerId(), schemaName);
	 List<TaggedUsers> taggedUsers = new ArrayList<TaggedUsers>();
         if(userObject.getUserRoleId()>=2 && userObject.getUserRoleId()<=6) {
		if(geos.getGeofenceOwnerId()!=null) {
			UserActivityNotify userActivityNoty =  new UserActivityNotify();
			String type = geos.getType();
			taggedUsers = geoFence.getTaggedUsers();
			for (int i=0;i<taggedUsers.size();i++) {
				User userGeofence = userService.getUserById(taggedUsers.get(i).getId(), schemaName);
				Constant.LOGGER.info("Inside else statement"+userGeofence.isGeofenceTagged());
				if(taggedUsers.get(i).isUserTagged()) {
					userGeofence.setGeofenceTagged(true);
				}else {
					userGeofence.setGeofenceTagged(false);
				}
				this.userRepository.save(userGeofence);
			}
			
	
			 //taggedUsers =geos.getTaggedUsers();
			geos.getUsersDeviceList().forEach(user->{
	            userActivityNoty.setNotificationId(user.getNotificationId());
				userActivityNoty.setUserActivityStatus(type+" "+"Geofence has been updated by"+" "+userObject.getName().getFirstName());
				noty.setMessage(userActivityNoty.getUserActivityStatus());
				noty.setToken(userActivityNoty.getNotificationId());
				String res =this.notificationSendingService.sendNotification(noty);
				Constant.LOGGER.info("Notification Sending Response"+" "+res);
				

				
				
				Constant.LOGGER.info("Inside else statement"+user.getId());
			});
		}
		
 }
    	
    	
    	
    	
    	
    	return geo;
    }
   
    //hard delete
   	public Map<String, String> deleteGeoFenceData(String id, String schemaName) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside deleteGeoFenceData GeofenceService.java");
    	
    	
    	
//    	GeoFence geos = this.genericINterface.findById(id, GeoFence.class);		
//		//User userObject  = userService.getUserById(geos.getGeofenceOwnerId(), schemaName);
//    	List<TaggedUsers> taggedUsers = new ArrayList<TaggedUsers>();
//		taggedUsers =geos.getTaggedUsers();
//		
//		geos.getUsersDeviceList().forEach(user->{
//			User userGeofence = userService.getUserById(user.getId(), schemaName);
//			Constant.LOGGER.info("Inside else statement"+userGeofence.isGeofenceTagged());
//			
//			
//			for (int i=0;i<geos.getTaggedUsers().size();i++) {
//				if(geos.getTaggedUsers().get(i).isUserTagged()) {
//					userGeofence.setGeofenceTagged(true);
//				}else {
//					userGeofence.setGeofenceTagged(false);
//				}
//				
//			}
//			
//			//userGeofence.setGeofenceTagged(false);
//			this.userRepository.save(userGeofence);
//			Constant.LOGGER.info("Inside else statement"+user.getId());
//		});
//	
  
    	
    	
		this.geofenceRepository.deleteById(id);
   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "Geofence deleted successfully");
   		return response;

   	}
        //soft delete
   	public Map<String, String> deleteSoftGeoFenceData(String id, String schemaName, TimeUpdate timeUpdate) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside deleteSoftGeoFenceData GeofenceService.java");
        GeoFence geoFence = this.genericINterface.findById(id, GeoFence.class);
   		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
   		geoFence.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
   		geoFence.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
   		geoFence.setStatus(Status.INACTIVE);
   		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.genericINterface.saveName(geoFence);

   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "Company deleted successfully");
   		return response;

   	}
 
    public List<GeoFence> streamAllGeoFenceData() {
        return geofenceRepository.findAll();
    }


	public List<GeoFence> getAllGeofenceByOwner(String geofenceOwnerId, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<GeoFence> selectedGeofences = null;
		Constant.LOGGER.info("Inside  Customer.java");
        Query q = new Query();
        try {
        	q.addCriteria(Criteria.where("geofenceOwnerId").in(geofenceOwnerId));
        	selectedGeofences = this.genericINterface.find(q,GeoFence.class );
        	Constant.LOGGER.info("selectedGeofences-------------"+selectedGeofences);
        }catch(Exception ex) {
			Constant.LOGGER.error("selectedGeofences-------------------");

        }
		
		return selectedGeofences;
	}


	public List<GeoFence> getAllGeofenceByNameSearch(String text, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<GeoFence> selectedGeofences = null;
		Constant.LOGGER.info("Inside Geofence By name Search");
        Query q = new Query();
        try {
        	q.addCriteria(Criteria.where("name").regex(text,"i"));
        	selectedGeofences = this.genericINterface.find(q,GeoFence.class );
        	Constant.LOGGER.info("getAllGeofenceByNameSearch selectedGeofences-------------"+selectedGeofences);
        }catch(Exception ex) {
			Constant.LOGGER.error("getAllGeofenceByNameSearch selectedGeofences-------------------");

        }
		
		return selectedGeofences;
	}
    
}
