package com.fidz.entr.app.config;

import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Repository
public interface UserDataInterface {
	
	Mono<UserData> saveName(UserData userData);
	
	/*List<UserData> getAllUserData();*/
	Flux<UserData> findAll();
}
