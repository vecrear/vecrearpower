package com.fidz.entr.app.reportcustomersweb;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import lombok.Data;

@Data
@Document(collection = "customercategory")
@JsonIgnoreProperties(value = { "target" })
public class CustomerCategory extends SimplifiedBase {
	@Id
	protected String id;
	
	@NotBlank
	protected String name;

	@JsonIgnore
	@DBRef(lazy = true)
	protected Company company;

	
	
	
}
