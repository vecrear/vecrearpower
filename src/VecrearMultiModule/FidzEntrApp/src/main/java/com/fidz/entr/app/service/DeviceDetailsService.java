package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.CustomerTypeField;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.repository.DeviceDetailsRepository;


@Service
public class DeviceDetailsService {
	private GenericAppINterface<DeviceDetails> genericINterface;
	@Autowired
	public DeviceDetailsService(GenericAppINterface<DeviceDetails> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private DeviceDetailsRepository deviceDetailsRepository;
	
   public List<DeviceDetails> getAllDeviceDetails(String schemaName) {
	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   	Constant.LOGGER.info("Inside getAllDeviceDetails DeviceDetailsService.java");
   	List<DeviceDetails> deviceDetails=null;
   	try {
//   	Query q = new Query();
//   	q.addCriteria(Criteria.where("status").is("ACTIVE"));
   	deviceDetails=this.genericINterface.findAll(DeviceDetails.class);
   	}catch(Exception ex) {
   	Constant.LOGGER.error("Error while getting data from getAllDeviceDetails DeviceDetailsService.java"+ex.getMessage());
       }
   	Constant.LOGGER.info("**************************************************************************");
   	Constant.LOGGER.info("successfully fetched data from getAllDeviceDetails DeviceDetailsService.java");
   	return deviceDetails;
    	
    }
	
   public DeviceDetails createDeviceDetail(DeviceDetails deviceDetail) {
	   DeviceDetails deviceDetails=null;
   	try {
   		Constant.LOGGER.info(" Inside DeviceDetailsService.java Value for insert DeviceDetails record:createDeviceDetails :: " + deviceDetails);
   		String schemaName=deviceDetail.getSchemaName();
   	   	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       //return deviceDetailsRepository.save(deviceDetails);
	    deviceDetails=this.deviceDetailsRepository.save(deviceDetails);
   	}catch(Exception e) {
   		Constant.LOGGER.error(" Inside DeviceDetailsService.java Value for insert DeviceDetails record:createDeviceDetails :: " + deviceDetails);
   	}
      return deviceDetails; 
   }
   
    public void createDeviceDetails(DeviceDetails deviceDetails) {
    	
        String schemaName=deviceDetails.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        //return deviceDetailsRepository.save(deviceDetails);
        this.genericINterface.saveName(deviceDetails);
    }
    
    
    public DeviceDetails getDeviceDetailsById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       	Constant.LOGGER.info("Inside getDeviceDetailsById DeviceDetailsService.java");
        return this.genericINterface.findById(id, DeviceDetails.class);
		
    }
   
	
    public DeviceDetails updateDeviceDetails(String id, DeviceDetails deviceDetails) {
    	String schemaName=deviceDetails.getSchemaName();
       	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       	Constant.LOGGER.info("Inside updateDeviceDetails DeviceDetailsService.java");
        deviceDetails.setId(id);
	    return this.deviceDetailsRepository.save(deviceDetails);
      
    }

  //hard delete
  	public Map<String, String> deleteDeviceDetails(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       	Constant.LOGGER.info("Inside deleteDeviceDetails DeviceDetailsService.java");
        this.deviceDetailsRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "DeviceDetails deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftDeviceDetails(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       	Constant.LOGGER.info("Inside deleteSoftDeviceDetails DeviceDetailsService.java");
        DeviceDetails deviceDetails = this.genericINterface.findById(id, DeviceDetails.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		deviceDetails.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		deviceDetails.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		deviceDetails.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(deviceDetails);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "DeviceDetails deleted successfully");
  		return response;

  	}
//To get all DeviceDetails Paginated
	public List<DeviceDetails> getAllDeviceDetailsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllcustomertypeFieldsPaginated DeviceDetailsService.java");
    	Query query = new Query();
    	List<DeviceDetails> deviceDetails =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	deviceDetails= this.genericINterface.find(query, DeviceDetails.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllcustomertypeFieldsPaginated DeviceDetailsService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllcustomertypeFieldsPaginated "+query);
        return deviceDetails;
	}
}
