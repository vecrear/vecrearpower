package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.CustomerCategory;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Device;

/**
 * @author vecrear
 *
 */

@Service
public class ValidateService {

	@Autowired
	private MongoTemplate mongoTemplate;

	/**
	 * 
	 * @param schemaName: will be the company schemaName.
	 * @param id:         will be company mongoDB Id.
	 * @return JSONObject
	 */

	@SuppressWarnings("unchecked")
	public JSONObject validateCompanyTagged(String schemaName, String id) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside validateCompanyTagged ValidateService.java");
		JSONObject jsonObject = new JSONObject();
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("company.id").is(id));
			Device dev = this.mongoTemplate.findOne(q, Device.class);
			Map<String, String> devdata = new HashMap<>();
			if (dev != null) {
				devdata.put("taggedtoDevice", "true");
				devdata.put("message", "This company is tagged to DeviceId: " + dev.getDeviceId());
			} else {
				devdata.put("taggedtoDevice", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("company.id").is(id));
			Department dep = this.mongoTemplate.findOne(q, Department.class);
			Map<String, String> depdata = new HashMap<>();
			if (dep != null) {
				depdata.put("taggedtoDepartment", "true");
				depdata.put("message", "This company is tagged to Department: " + dep.getName());
			} else {
				depdata.put("taggedtoDepartment", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("company.id").is(id));
			UserRole userRole = this.mongoTemplate.findOne(q, UserRole.class);
			Map<String, String> userRoleData = new HashMap<>();

			if (userRole != null) {
				userRoleData.put("taggedtoUserRole", "true");
				userRoleData.put("message", "This company is tagged to UserRole: " + userRole.getName());
			} else {
				userRoleData.put("taggedtoUserRole", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("company.id").is(id));
			User users = this.mongoTemplate.findOne(q, User.class);
			Map<String, String> usersData = new HashMap<>();

			if (users != null) {
				usersData.put("taggedtoUser", "true");
				usersData.put("message", "This company is tagged to User: " + users.getUserName());
			} else {
				usersData.put("taggedtoUser", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("company.id").is(id));
			CustomerCategory cusCategory = this.mongoTemplate.findOne(q, CustomerCategory.class);
			Map<String, String> cusCategoryData = new HashMap<>();
			if (cusCategory != null) {
				cusCategoryData.put("taggedtoCustomerCategory", "true");
				cusCategoryData.put("message", "This company is tagged to CustomerCategory: " + cusCategory.getName());
			} else {
				cusCategoryData.put("taggedtoCustomerCategory", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("company.id").is(id));
			ActivityType activityType = this.mongoTemplate.findOne(q, ActivityType.class);
			Map<String, String> activityTypeData = new HashMap<>();
			if (activityType != null) {
				activityTypeData.put("taggedtoactivityType", "true");
				activityTypeData.put("message", "This company is tagged to activityType: " + activityType.getName());
			} else {
				activityTypeData.put("taggedtoactivityType", "false");
			}

			jsonObject.put("deviceDetails", devdata);
			jsonObject.put("departmentDetails", depdata);
			jsonObject.put("userRoleDetails", userRoleData);
			jsonObject.put("userDetails", usersData);
			jsonObject.put("customerCategoryDetails", cusCategoryData);
			jsonObject.put("activityTypeDetails", activityTypeData);

		} catch (Exception ex) {
			Constant.LOGGER.info("Error inside validateCompanyTagged ValidateService.java" + ex.getLocalizedMessage());
		}
		return jsonObject;
	}

	/**
	 * 
	 * @param schemaName: will be the company schemaName
	 * @param id:         will be device mongoDB Id
	 * @return JSONObject
	 */

	@SuppressWarnings("unchecked")
	public JSONObject validateDeviceTagged(String schemaName, String id) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside validateDeviceTagged ValidateService.java");
		JSONObject jsonObject = new JSONObject();
		Query q = new Query();
		try {

			q.addCriteria(Criteria.where("device.id").is(id));
			User users = this.mongoTemplate.findOne(q, User.class);
			Map<String, String> usersData = new HashMap<>();

			if (users != null) {
				usersData.put("taggedtoUser", "true");
				usersData.put("message", "This device is tagged to User: " + users.getUserName());
			} else {
				usersData.put("taggedtoUser", "false");
			}

			jsonObject.put("userDetails", usersData);
		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error occurred inside validateDeviceTagged ValidateService.java" + ex.getLocalizedMessage());

		}
		return jsonObject;
	}

	/**
	 * 
	 * @param schemaName : company name
	 * @param id         : need to send department object id
	 * @return JSONOject
	 */

	@SuppressWarnings("unchecked")
	public JSONObject validateDepartmentTagged(String schemaName, String id) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside validateDepartmentTagged ValidateService.java");
		JSONObject jsonObject = new JSONObject();
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("department.id").is(id));
			Company comp = this.mongoTemplate.findOne(q, Company.class);
			Map<String, String> compData = new HashMap<>();
			if (comp != null) {
				compData.put("taggedtoCompany", "true");
				compData.put("message", "This department is tagged to Company: " + comp.getName());
			} else {
				compData.put("taggedtoCompany", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("department.id").is(id));
			ActivityType activityType = this.mongoTemplate.findOne(q, ActivityType.class);
			Map<String, String> activityTypeData = new HashMap<>();
			if (activityType != null) {
				activityTypeData.put("taggedtoactivityType", "true");
				activityTypeData.put("message", "This department is tagged to activityType: " + activityType.getName());
			} else {
				activityTypeData.put("taggedtoactivityType", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("department.id").is(id));
			User users = this.mongoTemplate.findOne(q, User.class);
			Map<String, String> usersData = new HashMap<>();

			if (users != null) {
				usersData.put("taggedtoUser", "true");
				usersData.put("message", "This department is tagged to User: " + users.getUserName());
			} else {
				usersData.put("taggedtoUser", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("department.id").is(id));
			Customer customer = this.mongoTemplate.findOne(q, Customer.class);
			Map<String, String> customersData = new HashMap<>();

			if (customer != null) {
				customersData.put("taggedtoCustomer", "true");
				customersData.put("message", "This department is tagged to Customer: " + customer.getName());
			} else {
				customersData.put("taggedtoCustomer", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("department.id").is(id));
			UserRole userRole = this.mongoTemplate.findOne(q, UserRole.class);
			Map<String, String> userRoleData = new HashMap<>();

			if (userRole != null) {
				userRoleData.put("taggedtoUserRole", "true");
				userRoleData.put("message", "This department is tagged to UserRole: " + userRole.getName());
			} else {
				userRoleData.put("taggedtoUserRole", "false");
			}
			

			jsonObject.put("companyDetails", compData);
			jsonObject.put("activityTypeDetails", activityTypeData);
			jsonObject.put("userDetails", usersData);
			jsonObject.put("customerDetails", customersData);
			jsonObject.put("userRoleDetails", userRoleData);

		} catch (Exception ex) {
			Constant.LOGGER.error("error inside Department tagged" + ex.getLocalizedMessage());
		}
		return jsonObject;
	}

	/**
	 * 
	 * @param schemaName : Company Name
	 * @param id : UserRole Object Id
	 * @return JSONObject
	 */

	@SuppressWarnings("unchecked")
	public JSONObject validateUserRoleTagged(String schemaName, String id) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside validateUserRoleTagged ValidateService.java");
		JSONObject jsonObject = new JSONObject();
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("userrole.id").is(id));
			User users = this.mongoTemplate.findOne(q, User.class);
			Map<String, String> usersData = new HashMap<>();

			if (users != null) {
				usersData.put("taggedtoUser", "true");
				usersData.put("message", "This userrole is tagged to User: " + users.getUserName());
			} else {
				usersData.put("taggedtoUser", "false");
			}

			jsonObject.put("userDetails", usersData);

		} catch (Exception ex) {
			Constant.LOGGER.error("inside validateUserRoleTagged ValidateService.java" + ex.getLocalizedMessage());
		}

		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject validateUsersTagged(String schemaName, String id) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside validateUserRoleTagged ValidateService.java");
		JSONObject jsonObject = new JSONObject();
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("user.id").is(id));
			User users = this.mongoTemplate.findOne(q, User.class);
			Map<String, String> usersData = new HashMap<>();

			if (users != null) {
				usersData.put("taggedtoUser", "true");
				usersData.put("message", "This userrole is tagged to User: " + users.getUserName());
			} else {
				usersData.put("taggedtoUser", "false");
			}

			jsonObject.put("userDetails", usersData);

		} catch (Exception ex) {
			Constant.LOGGER.error("inside validateUserRoleTagged ValidateService.java" + ex.getLocalizedMessage());
		}

		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject validateCustomerCategoryTagged(String schemaName, String id) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside validateCustomerCategoryTagged ValidateService.java");
		JSONObject jsonObject = new JSONObject();
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("customerCategory.id").is(id));
			Customer customers = this.mongoTemplate.findOne(q, Customer.class);
			Map<String, String> customersData = new HashMap<>();

			if (customers != null) {
				customersData.put("taggedtoCustomer", "true");
				customersData.put("message", "This customerCategory is tagged to Customer: " + customers.getName());
			} else {
				customersData.put("taggedtoCustomer", "false");
			}

			q = new Query();
			q.addCriteria(Criteria.where("activityTargets.customerCategory.id").in(id));
			ActivityType activityTypes = this.mongoTemplate.findOne(q, ActivityType.class);
			Map<String, String> activityTypesData = new HashMap<>();

			if (activityTypes != null) {
				activityTypesData.put("taggedtoactivityType", "true");
				activityTypesData.put("message",
						"This customerCategory is tagged to activityType: " + activityTypes.getName());
			} else {
				activityTypesData.put("taggedtoactivityType", "false");
			}

			jsonObject.put("CustomerDetails", customersData);
			jsonObject.put("ActivityTypesDetails", activityTypesData);

		} catch (Exception ex) {
			Constant.LOGGER
					.error("inside validateCustomerCategoryTagged ValidateService.java" + ex.getLocalizedMessage());
		}

		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public JSONObject validateUsersTagghed(String schemaName, String id) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside validateUserRoleTagged ValidateService.java");
		JSONObject jsonObject = new JSONObject();
		Query q = new Query();
		try {
			q.addCriteria(Criteria.where("user.id").is(id));
			User users = this.mongoTemplate.findOne(q, User.class);
			Map<String, String> usersData = new HashMap<>();

			if (users != null) {
				usersData.put("taggedtoUser", "true");
				usersData.put("message", "This userrole is tagged to User: " + users.getUserName());
			} else {
				usersData.put("taggedtoUser", "false");
			}

			jsonObject.put("userDetails", usersData);
		} catch (Exception ex) {
			Constant.LOGGER.error("inside validateUserRoleTagged ValidateService.java" + ex.getLocalizedMessage());
		}

		return jsonObject;
	}

}
