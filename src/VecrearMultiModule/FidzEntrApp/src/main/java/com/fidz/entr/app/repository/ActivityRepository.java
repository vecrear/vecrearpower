package com.fidz.entr.app.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.Activity;
import com.mongodb.BasicDBObject;

@Repository
public interface ActivityRepository extends MongoRepository<Activity, String>{
	//Mono<Activity> findByName(String name);
	List<Activity> findByDepartment(String id);
	List<Activity> findByResource(String id);
	
	@Query("{'arrived': {$gte: ?0, $lte:?1 }}")               
	public List<Activity> findByCreatedTimestamp(String from, String to);
	
	List<Activity> findByPlannedStartDateTimeBetween(String startdate, String enddate);
	Activity findByCustomer(String id);
	
}
