package com.fidz.entr.app.controller;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.ApplicationConfigNotFoundException;
import com.fidz.entr.app.model.ApplicationConfig;
import com.fidz.entr.app.service.ApplicationConfigService;
import com.fidz.entr.base.model.Country;


@CrossOrigin(maxAge = 3600)
@RestController("FEAApplicationConfigController")
@RequestMapping(value="/applicationconfigs")
public class ApplicationConfigController {

	@Autowired
	private ApplicationConfigService applicationConfigService;
	
	
	@PostMapping("/get/all")
    public List<ApplicationConfig> getAllApplicationConfig(@RequestHeader(value="schemaName") String schemaName) {
    	return applicationConfigService.getAllApplicationConfig(schemaName);
    }
	
	//Post request to get all ApplicationConfigs  paginated
		@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	    public List<ApplicationConfig> getAllApplicationConfigPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	    return applicationConfigService.getAllApplicationConfigPaginated(pageNumber, pageSize, schemaName);
	    }
		
	
    @PostMapping
    public ApplicationConfig createApplicationConfig(@Valid @RequestBody ApplicationConfig applicationConfig) {
        return applicationConfigService.createApplicationConfig(applicationConfig);
    }
    
    @PostMapping("/creates")
    public void createApplicationConfigs(@Valid @RequestBody ApplicationConfig applicationConfig) {
       applicationConfigService.createApplicationConfigs(applicationConfig);
    }
    
    @PostMapping("/get/id/{id}")
    public ApplicationConfig getApplicationConfigById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return applicationConfigService.getApplicationConfigById(id, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public ApplicationConfig updateApplicationConfig(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody ApplicationConfig applicationConfig) {
        return applicationConfigService.updateApplicationConfig(id, applicationConfig);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteApplicationConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return applicationConfigService.deleteApplicationConfig(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteApplicationConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return applicationConfigService.deleteSoftApplicationConfig(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<ApplicationConfig> streamAllApplicationConfigs() {
        return applicationConfigService.streamAllApplicationConfigs();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A ApplicationConfig with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ApplicationConfigNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
