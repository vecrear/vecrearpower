package com.fidz.entr.app.service;

import java.net.UnknownHostException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.model.User;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.ActivityFieldNotFoundException;
import com.fidz.entr.app.exception.CustomerTypeFieldNotFoundException;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.CustomerTypeField;
import com.fidz.entr.app.repository.ActivityFieldRepository;
import com.fidz.entr.app.repository.CustomerTypeFieldRepository;

@Service("FEACustomerTypeFieldService")
public class CustomerTypeFieldService {
	private GenericAppINterface<CustomerTypeField> genericINterface;
	@Autowired
	public CustomerTypeFieldService(GenericAppINterface<CustomerTypeField> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	@Autowired
	private CustomerTypeFieldRepository  customerTypeFieldRepository;

	public List<CustomerTypeField> getAllCustomertypeFields(String schemaName) throws UnknownHostException{
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllCustomertypeFields CustomerTypeFieldService.java");
    	List<CustomerTypeField> acFields=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	acFields=this.genericINterface.findAll(CustomerTypeField.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllCustomertypeFields CustomerTypeFieldService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCustomertypeFields CustomerTypeFieldService.java");
    	return acFields;
	}
	
	public CustomerTypeField createCustomertypeField(CustomerTypeField customerTypeFields) {
		CustomerTypeField customerTypeField=null;
		try {
			Constant.LOGGER.info(" Inside customerTypeFields.java Value for insert customerTypeFields  :: " + customerTypeFields);
			String schemaName = customerTypeFields.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			customerTypeField=this.customerTypeFieldRepository.save(customerTypeFields);
		}catch(Exception e) {
			Constant.LOGGER.error(" Inside customerTypeFields.java Value for insert customerTypeFields:: " + customerTypeFields);
		}
		return customerTypeField;
	}
	
	public void createCustomertypeFields(CustomerTypeField customerTypeField){	
        String schemaName=customerTypeField.getSchemaName();
		Constant.LOGGER.info("Inside createCustomertypeFields CustomerTypeFieldService.java");

	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//return activityFieldRepository.save(activity);
	    this.genericINterface.saveName(customerTypeField);
	}
	public ResponseEntity<CustomerTypeField> getCustomertypeFieldById(String id, String schemaName){
		CustomerTypeField customerTypeField=null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getCustomertypeFieldById CustomerTypeFieldService.java");

		customerTypeField=this.genericINterface.findById(id, CustomerTypeField.class);
		if (customerTypeField == null) {
			throw new ActivityFieldNotFoundException(id);
            //return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(customerTypeField);
        }
		/*try {
			activityField=this.genericINterface.findById(id, ActivityField.class);
			if (activityField == null) {
	            return ResponseEntity.notFound().build();
	        } else {
	            return ResponseEntity.ok(activityField);
	        }
			
		}catch(ActivityFieldNotFoundException e) {
			//throw new ActivityFieldNotFoundException("bean: "+id+ " not Found");
			System.out.println("inside Catch");
			return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
		}
		//return activityField;
		return new ResponseEntity<>(activityField, HttpStatus.OK);
		//return this.genericINterface.findById(id, ActivityField.class);
*/		
		
	}
	
    /*public Mono<Activity> getActivityByName(String name) {
		return activityRepository.findByName(name).switchIfEmpty(Mono.error(
				new ActivityNotFoundException(name)));
    }*/
	
	public CustomerTypeField updateCustomertypeField(String id, CustomerTypeField customerTypeFields) {
		String schemaName = customerTypeFields.getSchemaName();
     MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside updateCustomertypeField CustomerTypeFieldService.java");
       CustomerTypeField customerTypeField = null;
		customerTypeField = this.genericINterface.findById(id, CustomerTypeField.class);
		if (customerTypeField == null) {
			throw new CustomerTypeFieldNotFoundException(id);
		} else {
			customerTypeField.setId(id);
			
			return this.customerTypeFieldRepository.save(customerTypeField);
		}

	}
	
	
  //hard delete
  	public Map<String, String> deleteCustomertypeField(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside deleteCustomertypeField CustomerTypeFieldService.java");
         this.customerTypeFieldRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "ActivityField deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftCustomertypeField(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside deleteSoftCustomertypeField CustomerTypeFieldService.java");

  		CustomerTypeField customerTypeField = this.genericINterface.findById(id, CustomerTypeField.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		customerTypeField.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		customerTypeField.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		customerTypeField.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(customerTypeField);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "deleted successfully");
  		return response;

  	}
    public List<CustomerTypeField> streamAllCustomertypeFields() {
        return customerTypeFieldRepository.findAll();
    }
//TO get all CustomerTypeField paginated
	public List<CustomerTypeField> getAllcustomertypeFieldsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllcustomertypeFieldsPaginated CustomerTypeFieldService.java");
    	Query query = new Query();
    	List<CustomerTypeField> customerTypeFields =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	customerTypeFields= this.genericINterface.find(query, CustomerTypeField.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllcustomertypeFieldsPaginated CustomerTypeFieldService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllcustomertypeFieldsPaginated "+query);
        return customerTypeFields;
	}
    


}
