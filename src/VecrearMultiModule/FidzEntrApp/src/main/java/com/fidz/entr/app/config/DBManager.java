package com.fidz.entr.app.config;

import java.net.UnknownHostException;

import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

import lombok.Data;

@Component
@Data
public class DBManager {

	private String host;
	private int port;
	private String dbName;
	private String mongoDbUserName;
	private String pwd;
	
	public DBManager() throws UnknownHostException, UnsupportedOperationException {
	}

	public DBManager(String host, int port, String dbName, String mongoDbUserName, String pwd)
			throws UnknownHostException, UnsupportedOperationException {

		this.setHost(host);
		this.setPort(port);
		this.setDbName(dbName);
		this.setMongoDbUserName(mongoDbUserName);
		this.setPwd(pwd);
	}
	 
	public boolean connect() throws NullPointerException{
		Mongo mongo = new Mongo(host, port);
		DB db = mongo.getDB(dbName);
		//DBCollection collection = db.getCollection("user");
		//System.out.println(collection.toString());

		return true;

	}

	public boolean reConnect() {

		return false;

	}

	public boolean disConnect() {

		return false;

	}

	public boolean isConnected() {

		return false;

	}

}
