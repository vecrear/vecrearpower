package com.fidz.entr.app.reportactivities;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.app.model.ActivityPerformedFrequency;
import com.fidz.entr.app.model.WeekDay;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.UserRole;

import lombok.Data;
@Data
@JsonIgnoreProperties(value = { "target" })
public class ActivityType {
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	
	@Id
	protected String id;

	@NotNull
	protected String name;
	@JsonIgnore
	protected Object company;
	@JsonIgnore
	protected Object department;

    protected int sortOrder; //sort order
    
    protected boolean editable; //Will the fields be editable?

	//protected List<CustomField> activityFields;
  
    @NotNull
	@DBRef(lazy = true)
	protected List<ActivityField> activityFields;
    @JsonIgnore
	protected List<Object> mappedUserRoles; //mapped to specific user roles
	
	protected boolean mapToAllUserRoles; //mapped to all user roles
	
	protected ActivityPerformedFrequency activityPerformedFrequency;
	
	protected TimeZone timeZone; //Time given below is specific to the time zone specified.
	
	protected String performBeforeThisTime; //Time as a String
	
	protected String performAfterThisTime; //Time as a String
	
	protected WeekDay performOnThisWeekDay;
	
	protected int performOnThisDateOfMonth; //1 - 31
	
	@DBRef(lazy = true)
	protected List<ActivityTarget> activityTargets;
	
	@DBRef(lazy = true)
	protected List<Product> products;
	
	/**
	 * enableMultipleEntry - boolean
	 * Single Entry or Multiple Entry for Activity Log
	 * true means Multiple Entry
	 * false means Single Entry (Default)
	 */
	protected boolean enableMultipleEntry = false;

}