package com.fidz.entr.app.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "UserActivityStatus")
@JsonIgnoreProperties(value = { "target" })
public class UserActivityStatus {

}
