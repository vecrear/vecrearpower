package com.fidz.entr.app.exception;

public class DepartmentNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4063414270194638636L;
	public DepartmentNotFoundException(String department) {
		super("Department not found with id/name " + department);
	}

}
