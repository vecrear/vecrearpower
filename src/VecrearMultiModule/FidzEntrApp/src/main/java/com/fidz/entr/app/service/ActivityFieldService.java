package com.fidz.entr.app.service;

import java.net.UnknownHostException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.model.User;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.ActivityFieldNotFoundException;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.repository.ActivityFieldRepository;

import com.fidz.entr.base.exception.CityNameFoundException;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.exception.NameFoundException;

import com.fidz.entr.base.model.UserRole;

@Service("FEAActivityFieldService")
public class ActivityFieldService {
	private GenericAppINterface<ActivityField> genericINterface;
	@Autowired
	public ActivityFieldService(GenericAppINterface<ActivityField> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	@Autowired
	private ActivityFieldRepository  activityFieldRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private static String name = "ActivityField";

	public List<ActivityField> getAllActivityFields(String schemaName) throws UnknownHostException{
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllActivityFields ActivityFieldService.java");
    	List<ActivityField> acFields=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	acFields=this.genericINterface.findAll(ActivityField.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllActivityFields ActivityFieldService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllActivityFields ActivityFieldService.java");
    	return acFields;
	}
	
	public ActivityField createActivityField(ActivityField activity) {
		ActivityField activityfields=null;
		
			Constant.LOGGER.info(" Inside ActivityFieldService.java Value for insert ActivityField record:createActivityField :: " + activity);
			String schemaName = activity.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  //--------------------TO CHECK THE SAME IN SAME DEPT--------IN SAME ACTIVITYFIELD
    	    
    	    Query query = new Query();
        	List<ActivityField> acFields = null;
          
        
        		query.addCriteria(Criteria.where("department.id").is(activity.getDepartment().getId()));
        	    acFields= this.genericINterface.find(query, ActivityField.class);
     
	
	    	 if(acFields.size()>0) {
		   	  List<String> names = acFields.stream().map(p->p.getCustomField().getFieldName()).collect(Collectors.toList());
		   		Constant.LOGGER.info("ActivityField List data"+names);
		   	 
		   		if (names.stream().anyMatch(s -> s.equals(activity.getCustomField().getFieldName())==true)) {
		   			Constant.LOGGER.info("Inside if statement");
					throw new NameFoundException(ExceptionConstant.SAME_ACTIVITYFIELD_SAME_DEPARTMENT+": "+activity.getCustomField().getFieldName());
		   	 }
		   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(activity.getCustomField().getFieldName())==true)) {
		   				Constant.LOGGER.info("Inside else if statement");
					throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+activity.getCustomField().getFieldName());
		   		}else{
		   			Constant.LOGGER.info("Inside else statement");
		   		Constant.LOGGER.info("Successfull ActivityField creation"+activity.toString());	
			activityfields=this.activityFieldRepository.save(activity);
	        return activityfields;

		   		}
	    	 }
		activityfields=this.activityFieldRepository.save(activity);
        return activityfields;
	}
	
	public void createActivityFields(ActivityField activity){	
        String schemaName=activity.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//return activityFieldRepository.save(activity);
	    this.genericINterface.saveName(activity);
	}
	public ResponseEntity<ActivityField> getActivityFieldById(String id, String schemaName){
		ActivityField activityField=null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		Constant.LOGGER.info("inside getActivityFieldById ActivityFieldService.java");	

		activityField=this.genericINterface.findById(id, ActivityField.class);
		if (activityField == null) {
			throw new ActivityFieldNotFoundException(id);
            //return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(activityField);
        }
		/*try {
			activityField=this.genericINterface.findById(id, ActivityField.class);
			if (activityField == null) {
	            return ResponseEntity.notFound().build();
	        } else {
	            return ResponseEntity.ok(activityField);
	        }
			
		}catch(ActivityFieldNotFoundException e) {
			//throw new ActivityFieldNotFoundException("bean: "+id+ " not Found");
			System.out.println("inside Catch");
			return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
		}
		//return activityField;
		return new ResponseEntity<>(activityField, HttpStatus.OK);
		//return this.genericINterface.findById(id, ActivityField.class);
*/		
		
	}
	
    /*public Mono<Activity> getActivityByName(String name) {
		return activityRepository.findByName(name).switchIfEmpty(Mono.error(
				new ActivityNotFoundException(name)));
    }*/
	
	public ActivityField updateActivityField(String id, ActivityField activity) {
		String schemaName = activity.getSchemaName();
   		Constant.LOGGER.info("inside updateActivityField ActivityFieldService.java");	

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		ActivityField activitys = null;
		ActivityField activityss = null;
		activitys = this.genericINterface.findById(id, ActivityField.class);
		 Query query = new Query();
     	List<ActivityField> acFields = null;
       
     
     		query.addCriteria(Criteria.where("department.id").is(activity.getDepartment().getId()));
     	    acFields= this.genericINterface.find(query, ActivityField.class);
  
	if(acFields.size()>0) {
		ActivityField ctr = this.genericINterface.findById(id, ActivityField.class);
	   	  List<String> names = acFields.stream().map(p->p.getCustomField().getFieldName()).collect(Collectors.toList());
	   		Constant.LOGGER.info("ActivityField List data"+names);
	   	 names.remove(ctr.getCustomField().getFieldName());
	   		if (names.stream().anyMatch(s -> s.equals(activity.getCustomField().getFieldName())==true)) {
	   			Constant.LOGGER.info("Inside if statement");
				throw new NameFoundException(ExceptionConstant.SAME_ACTIVITYFIELD_SAME_DEPARTMENT+": "+activity.getCustomField().getFieldName());
	   	 }
	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(activity.getCustomField().getFieldName())==true)) {
	   				Constant.LOGGER.info("Inside else if statement");
				throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+activity.getCustomField().getFieldName());
	   		}else{
		if (activitys == null) {
			throw new ActivityFieldNotFoundException(id);
		} else {
			activity.setId(id);
			
			 activityss =  this.activityFieldRepository.save(activity);
			 return activityss;
		}
	   		}
             
	}
	activity.setId(id);
	
	activityss =  this.activityFieldRepository.save(activity);
	return activityss;

	}
	
	
  //hard delete
  	public Map<String, String> deleteActivityField(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		Constant.LOGGER.info("inside deleteActivityField ActivityFieldService.java");	

  		this.activityFieldRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "ActivityField deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftActivityField(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		Constant.LOGGER.info("inside deleteSoftActivityField ActivityFieldService.java");	

  		ActivityField activityField = this.genericINterface.findById(id, ActivityField.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		activityField.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		activityField.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		activityField.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(activityField);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "ActivityField deleted successfully");
  		return response;

  	}
    public List<ActivityField> streamAllActivityFields() {
        return activityFieldRepository.findAll();
    }

    //To get all ActivityFields paginated
    //page number starts from zero
	public List<ActivityField> getAllActivityFieldsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

		Constant.LOGGER.info("inside getAllActivityFieldsPaginated ActivityFieldService.java");
    	Query query = new Query();
    	List<ActivityField> activityFields =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	activityFields= this.genericINterface.find(query, ActivityField.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllActivityFieldsPaginated ActivityFieldService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllActivityFieldsPaginated "+query);
        return activityFields;	
	
	}

	public Map<String, String>  activityfieldidMappedOrNot(String activityfieldid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Map<String, String> response = new HashMap<String, String>();
  		//response.put("message", "User deleted successfully");
	 	Constant.LOGGER.info("inside getStatusofObjectMappedOrNot ActivityTypeService.java");
		
		 	Query q1 = new  Query();
		 	ActivityField activityfield =null;
			String message = "";
			List<ActivityType> listofActivityTypes =null;
			try {
				q1.addCriteria(Criteria.where("activityFields.id").is(activityfieldid));
		    	listofActivityTypes = this.genericINterface.find(q1, ActivityType.class);
		    	if (listofActivityTypes.size() > 0 ) {
				
					response.put("message", "activityfield is mapped with activitytype");
				    Constant.LOGGER.error("activity is mapped with department  ");
				    return response;
				} else {
				
					response.put("message", "activityfield is not mapped");
					return response;
				}
		    	}catch(Exception ex) {
			    Constant.LOGGER.error("Error occurred activityfieldidMappedOrNot activityFields.java"+ex.getMessage());
		        }
			
			return response;
			
	}

	
	public List<ActivityField> getAllActivityFieldsByDepartment(String deptid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllActivityFieldDepartment ActivityFieldService.java");
    	Query query = new Query();
    	List<ActivityField> listofActivityFields =null;

    	try {
    	query.addCriteria(Criteria.where("department.id").is(deptid));
    	listofActivityFields= this.genericINterface.find(query, ActivityField.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred listofActivityFields ActivityField.java "+ex.getMessage());
        }
    	
        return listofActivityFields;
	}

	public List<ActivityField> getAllSearchActivityFieldsPaginated(String text,int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<ActivityField>  activityFields = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
       Constant.LOGGER.info("Inside getAllSearchActivityFieldsPaginated CustomerService.java");
    	try {
    		
    		textIndex = new TextIndexDefinitionBuilder()
    				.onAllFields()
    				.build();
    		mongoTemplate.indexOps(ActivityField.class).ensureIndex(textIndex);
    	    criteria = TextCriteria.forDefaultLanguage()
    	    		.matchingAny(text).caseSensitive(false).diacriticSensitive(false);
              query = TextQuery.queryText(criteria)
    	    		  .sortByScore()
    	    		  .with(PageRequest.of(pageNumber, pageSize));
              activityFields = mongoTemplate.find(query, ActivityField.class);
    		
    					
	}catch(Exception ex) {
    	Constant.LOGGER.error("Inside getAllSearchActivityFieldsPaginated CustomerService.java"+ ex.getMessage());

	}
		return activityFields;
	}
    


}
