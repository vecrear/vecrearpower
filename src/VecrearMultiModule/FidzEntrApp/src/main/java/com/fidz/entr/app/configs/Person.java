package com.fidz.entr.app.configs;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "person")
@Data
public class Person {

	    @Id
	    private String id;

	    private String name;

	    private String surname;

	    private long   age;
	    protected String schemaName;

		

		public Person() {
			// TODO Auto-generated constructor stub
		}



		public Person(String id, String name, String surname, long age, String schemaName) {
			super();
			this.id = id;
			this.name = name;
			this.surname = surname;
			this.age = age;
			this.schemaName = schemaName;
		}



		@Override
		public String toString() {
			return "Person [id=" + id + ", name=" + name + ", surname=" + surname + ", age=" + age + ", schemaName="
					+ schemaName + "]";
		}

		
	    
	    
	
}
