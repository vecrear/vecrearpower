package com.fidz.entr.app.reportcustomersweb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class PersonName {
	protected String firstName;
	protected String middleName;
	protected String lastName;
	
	@JsonIgnore
	protected String prefix;
	@JsonIgnore
	protected String suffix;
	
	public PersonName(String firstName, String middleName, String lastName, String prefix, String suffix) {
		super();
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.prefix = prefix;
		this.suffix = suffix;
	}

	@Override
	public String toString() {
		return "Name [firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", prefix="
				+ prefix + ", suffix=" + suffix + "]";
	}

}
