package com.fidz.entr.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AttendanceNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 683290246618434374L;

	public AttendanceNotFoundException(String city) {
		super("Attendance not found with id/name " + city);
	}
}
