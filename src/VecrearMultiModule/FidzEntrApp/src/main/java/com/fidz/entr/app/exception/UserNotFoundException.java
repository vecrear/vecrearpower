package com.fidz.entr.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1104393811350706384L;
	public UserNotFoundException(String user) {
		super("User not found with id/name " + user);
	}
	public UserNotFoundException(String user, String superadmin) {
		super(superadmin);
	}

}
