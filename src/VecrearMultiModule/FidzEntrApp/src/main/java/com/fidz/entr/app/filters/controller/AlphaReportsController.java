package com.fidz.entr.app.filters.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.AlphaReports;
import com.fidz.entr.app.filters.service.AlphaReportsService;
import com.fidz.entr.app.model.Activity;

@CrossOrigin(maxAge = 3600)
@RestController("AlphaReportsController")
@Configuration
@RequestMapping(value="/alphareports")
public class AlphaReportsController {
	
	@Autowired
	AlphaReportsService alphaReportService;
	
	@PostMapping("/get/all")
	public List<AlphaReports> getAllAlphaReports(@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.getAllAlphaReports(schemaName);
	}
	
	@PostMapping("/search/{text}/get/all/{pageNumber}/{pageSize}")
	public List<AlphaReports> getAllAlphaReportsSearchPaginated(@PathVariable(value = "text") String text,@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.getAllAlphaReportsSearchPaginated(text,pageNumber,pageSize,schemaName);
	}
	
	@PostMapping("/get/all/{pageNumber}/{pageSize}")
	public List<AlphaReports> getAllAlphaReportsPaginated(@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.getAllAlphaReportsPaginated(pageNumber,pageSize,schemaName);
	}
	
	
	@PostMapping("/get/all/byuserid/{userid}/{pageNumber}/{pageSize}")
	public List<AlphaReports> getAllAlphaReportsByUserIDPaginated(@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@PathVariable(value = "userid") String userid,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.getAllAlphaReportsByUserIDPaginated(pageNumber,pageSize,userid,schemaName);
	}
	
	@PostMapping("/search/{text}/get/all/byuserid/{userid}/{pageNumber}/{pageSize}")
	public List<AlphaReports> getAllAlphaReportsByUserIDSearchPaginated(@PathVariable(value = "text") String text,@PathVariable(value = "pageNumber") int pageNumber,@PathVariable(value = "pageSize") int pageSize,@PathVariable(value = "userid") String userid,@RequestHeader(value = "schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.getAllAlphaReportsByUserIDSearchPaginated(text,pageNumber,pageSize,userid,schemaName);
	}

	@PostMapping
	public AlphaReports createAlphaReports(@Valid @RequestBody AlphaReports alphaReports){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.createAlphaReports(alphaReports);
	}
	
	@PostMapping("/get/id/{id}")
    public AlphaReports getAlphaReportsById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.getAlphaReportsById(id, schemaName);
    }
	
	@PostMapping("/update/id/{id}")
    public AlphaReports updateAlphaReportsById(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody AlphaReports alphaReports) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return alphaReportService.updateAlphaReportsById(id, alphaReports);   
        
    }
	
	
	
	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteAlphaReportsById(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		
		return alphaReportService.deleteAlphaReportsById(id, schemaName);
	}

	@PostMapping("/filters")
	public List<Activity> filterAlphaReports(@Valid @RequestBody AlphaReports alphaReports ) throws ParseException{
		return alphaReportService.filterAlphaReports(alphaReports);
	}
	
	
	
}
