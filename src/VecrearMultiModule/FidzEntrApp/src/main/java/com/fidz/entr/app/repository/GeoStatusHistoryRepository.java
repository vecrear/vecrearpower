package com.fidz.entr.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.app.model.GeoStatusHistory;

@Repository("GeoStatusHistoryRepository")
public interface GeoStatusHistoryRepository extends MongoRepository<GeoStatusHistory, String>{

}
