package com.fidz.entr.app.exception;

public class ActivityLogNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3253226869671901045L;

	public ActivityLogNotFoundException(String activitylog) {
		super("ActivityLog not found with id/name " + activitylog);
	}
	
}
