package com.fidz.entr.app.config;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.entr.app.model.Activity;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@CrossOrigin(maxAge = 3600)
@RestController("FEAUserDataController")
@RequestMapping(value="/userdatas")
public class UserDataController {
	@Autowired
	private UserDataService  userDataService;
	
	@GetMapping
	public Flux<UserData> getAllUserDatas(){
		return userDataService.getAllUserDatas();
	}
	/*@GetMapping
    public Flux<UserData> getAllUserDatas(@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("@@@@@@@@@@@@@@@@@########userdata222222"+schemaName);
		return userDataService.getAllUserDatas(schemaName);
    }*/
	@PostMapping
	public Mono<UserData> createUserData(@Valid @RequestBody UserData userData){
		return userDataService.createUserData(userData);
	}
	
	
}
