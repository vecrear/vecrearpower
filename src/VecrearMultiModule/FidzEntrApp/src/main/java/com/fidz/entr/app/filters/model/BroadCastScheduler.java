package com.fidz.entr.app.filters.model;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import lombok.Data;

@Data
@Document(collection = "broadcastscheduler")
@JsonIgnoreProperties(value = { "target" })
public class BroadCastScheduler {

	@Id
	private String id;
	
	private String schemaName;
	
	private String createdUserName;
	
	private String createdUserId;
	
	private SchedulePeriod schedules;
	
	private String scheduleDateTime;
	
	private List<UserInfo> usersList;
	
	@DBRef(lazy = true)
	private BroadCastTemplate template;
	
	private Modes mode;
	
	
	private String templateName;

	private String subject;

	private String message;
	
	private String emailSent;
	
	private String smsSent = "false";

	public BroadCastScheduler(String id, String schemaName, String createdUserName, String createdUserId,
			SchedulePeriod schedules, String scheduleDateTime, List<UserInfo> usersList, BroadCastTemplate template,
			Modes mode, String templateName, String subject, String message, String emailSent, String smsSent) {
		super();
		this.id = id;
		this.schemaName = schemaName;
		this.createdUserName = createdUserName;
		this.createdUserId = createdUserId;
		this.schedules = schedules;
		this.scheduleDateTime = scheduleDateTime;
		this.usersList = usersList;
		this.template = template;
		this.mode = mode;
		this.templateName = templateName;
		this.subject = subject;
		this.message = message;
		this.emailSent = emailSent;
		this.smsSent = smsSent;
	}

	public BroadCastScheduler() {
		// TODO Auto-generated constructor stub
	}

	
	
	
}


