package com.fidz.entr.app.filters.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.filters.model.AlphaReports;
import com.fidz.entr.app.filters.model.BroadCastScheduler;
import com.fidz.entr.app.filters.model.Modes;
import com.fidz.entr.app.filters.model.SchedulePeriod;
import com.fidz.entr.app.filters.repository.BroadCastSchedulerRepository;

@Service
public class BroadCastSchedulerService {
	private GenericAppINterface<BroadCastScheduler> genericINterface;
	@Autowired
	public BroadCastSchedulerService(GenericAppINterface<BroadCastScheduler> genericINterface){
		this.genericINterface=genericINterface;
	}

	@Autowired
	BroadCastSchedulerRepository broadCastSchedulerRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	public List<BroadCastScheduler> getAllBroadCastScheduler(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
    	Constant.LOGGER.info("Inside getAllBroadCastSchedulers BroadCastTemplateService.java");
    	List<BroadCastScheduler> broadCastSchedulers=null;
    	Query q = new Query();
    	
    	try {
    		q.addCriteria(Criteria.where("schemaName").is(schemaName));
    		broadCastSchedulers=this.genericINterface.find(q,BroadCastScheduler.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllbroadCastSchedulers BroadCastSchedulerService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllbroadCastSchedulers BroadCastSchedulerService.java");
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);

    	return broadCastSchedulers;
	}

	public BroadCastScheduler createBroadCastScheduler(@Valid BroadCastScheduler broadCastScheduler) throws ParseException {
		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");

		BroadCastScheduler bCastScheduler= null;
		long milis = Long.valueOf(broadCastScheduler.getScheduleDateTime());
		   Date d = new Date(milis);
		   DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		   String formatted = format.format(d);
		    Date de =format.parse(formatted);
		    LocalDateTime localDateTimesd = de.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		    LocalDateTime locals = null;
		    Constant.LOGGER.info(localDateTimesd+"eeeeeee");
		   	try {
		   		Constant.LOGGER.info(" Inside BroadCastSchedulerService.java Value for insert createBroadCastScheduler record:createBroadCastTemplate :: " + broadCastScheduler);
		   		String schemaName=broadCastScheduler.getSchemaName();
		
			    bCastScheduler=this.broadCastSchedulerRepository.save(broadCastScheduler);

		
		   	}catch(Exception e) {
		   		Constant.LOGGER.error(" Inside BroadCastSchedulerService.java Value for insert createBroadCastScheduler record:createBroadCastTemplate :: " + e.getMessage());
		   	}
		   	Constant.LOGGER.info("**************************************************************************");
	    	Constant.LOGGER.info("successfully created data from BroadCastScheduler createBroadCastScheduler.java");
		      return bCastScheduler; 
	}
	
	public BroadCastScheduler getBroadCastSchedulerById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
        Constant.LOGGER.info("Inside getAllBroadCastSchedulers BroadCastTemplateService.java");
    	BroadCastScheduler broadCastSchedulers=null;
    	try {
    		broadCastSchedulers=this.genericINterface.findById(id, BroadCastScheduler.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getbroadCastSchedulersById BroadCastSchedulerService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getbroadCastSchedulersById BroadCastSchedulerService.java");
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return broadCastSchedulers;
	}
	public BroadCastScheduler updateBroadCastScheduler(String id, @Valid BroadCastScheduler broadCastScheduler) {
    	Constant.LOGGER.info("Inside updateBroadCastScheduler BroadCastTemplateService.java");
         String schemaName=broadCastScheduler.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
	    String broadCastTemplateId=broadCastScheduler.getId();
	    broadCastScheduler.setId(id);
	    return this.broadCastSchedulerRepository.save(broadCastScheduler);
	}

	public Map<String, String> deleteBroadCastSchedulerById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside deleteBroadCastSchedulerById BroadCastTemplateService.java");
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		this.broadCastSchedulerRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "BroadCastTemplate deleted successfully");
		return response;
	}

	public BroadCastScheduler updateBroadCastSchedulerTrue(@Valid BroadCastScheduler broadCastScheduler) {
    	Constant.LOGGER.info("Inside updateBroadCastSchedulerTrue BroadCastTemplateService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
        String ids=broadCastScheduler.getId();
	     broadCastScheduler.setEmailSent("true");
	     broadCastScheduler.setId(ids);
	     
	    return this.broadCastSchedulerRepository.save(broadCastScheduler);
	}

	public BroadCastScheduler updateBroadCastSchedulerSmsTrue(@Valid BroadCastScheduler broadCastScheduler) {
    	Constant.LOGGER.info("Inside updateBroadCastSchedulerTrue BroadCastTemplateService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
	     String ids=broadCastScheduler.getId();
	     broadCastScheduler.setSmsSent("true");
	     broadCastScheduler.setId(ids);
	    return this.broadCastSchedulerRepository.save(broadCastScheduler);
	}
	public List<BroadCastScheduler> getAllBroadCastSchedulerByUserID(String userid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
    	Constant.LOGGER.info("Inside broadCastSchedulers broadCastSchedulers.java");
    	List<BroadCastScheduler> broadCastSchedulers=null;
    	Query q = new Query();
    	
    	try {
    		q.addCriteria(Criteria.where("createdUserId").is(userid));
    		broadCastSchedulers=this.genericINterface.find(q,BroadCastScheduler.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from broadCastSchedulers broadCastSchedulers.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from broadCastSchedulers broadCastSchedulers.java");
    	return broadCastSchedulers;
	}

	public List<BroadCastScheduler> getAllBroadCastSchedulerPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllBroadCastSchedulerPaginated BroadCastSchedulerService.java");
		Query query = new Query();
		List<BroadCastScheduler> broadCastSchedulers = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest);
			broadCastSchedulers = this.genericINterface.find(query, BroadCastScheduler.class);

		} catch (Exception ex) {
			Constant.LOGGER.error(
					"Error occurred inside getAllBroadCastSchedulerPaginated BroadCastSchedulerService.java " + ex.getMessage());
		}
		Constant.LOGGER.info("After success getAllBroadCastSchedulerPaginated " + query);
		return broadCastSchedulers;
	}

	public List<BroadCastScheduler> getAllBroadCastSchedulerSearchPaginated(String text, int pageNumber, int pageSize,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<BroadCastScheduler> broadCastSchedulers = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside getAllBroadCastSchedulerSearchPaginated BroadCastSchedulerService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(BroadCastScheduler.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			broadCastSchedulers = mongoTemplate.find(query, BroadCastScheduler.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside getAllBroadCastSchedulerSearchPaginated BroadCastSchedulerService.java" + ex.getMessage());

		}
		return broadCastSchedulers;
	}

	public List<BroadCastScheduler> getAllBroadCastSchedulerUserIdPaginated(String userid, int pageNumber, int pageSize,
			String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
    	Constant.LOGGER.info("Inside getAllBroadCastSchedulerUserIdPaginated broadCastSchedulers.java");
    	List<BroadCastScheduler> broadCastSchedulers=null;
    	Query q = new Query();
    	
    	try {
    		q.addCriteria(Criteria.where("createdUserId").is(userid));
    		final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			q.with(pageableRequest);
    		broadCastSchedulers=this.genericINterface.find(q,BroadCastScheduler.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllBroadCastSchedulerUserIdPaginated broadCastSchedulers.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllBroadCastSchedulerUserIdPaginated broadCastSchedulers.java");
    	return broadCastSchedulers;
	}

	public List<BroadCastScheduler> getAllBroadCastSchedulerUserIdSearchPaginated(String text, String userid, int pageNumber,
			int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<BroadCastScheduler> broadCastSchedulers = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = new Query();
		Constant.LOGGER.info("Inside getAllBroadCastSchedulerUserIdSearchPaginated BroadCastSchedulerService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(BroadCastScheduler.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query.addCriteria(Criteria.where("createdUserId").is(userid));
            query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
            broadCastSchedulers = mongoTemplate.find(query, BroadCastScheduler.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside getAllBroadCastSchedulerUserIdSearchPaginated BroadCastSchedulerService.java" + ex.getMessage());

		}
	
return broadCastSchedulers;
	}
}
