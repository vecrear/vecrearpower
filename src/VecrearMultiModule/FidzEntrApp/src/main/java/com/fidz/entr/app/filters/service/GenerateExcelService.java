package com.fidz.entr.app.filters.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Activity;

@Service
public class GenerateExcelService {

	public String generateExcel(List<Activity> activities, String userid, String userMobile,String schemaNames) throws FileNotFoundException,IOException {
		
	//	private static final String SUFFIX = "vtrack";
		
//		 //AWSCredentials credentials = new ProfileCredentialsProvider().getCredentials();
//		 AWSCredentials credentials = new BasicAWSCredentials("AKIAW2CTFB4M3FG7VQIK", "DtYaab0D3bZOAID0AnrEdiAD3dDkUWSlkyuKSbdV");
//		 AmazonS3 s3client = new AmazonS3Client(credentials);
//		 
//		 String bucketName = "vtrack_alpa";
//		 s3client.createBucket(bucketName);
//		 
		// createFolder(bucketName,"reports",s3client);
		
		
		 // Creating Workbook instances 
		Workbook wb = new XSSFWorkbook();
           
       
          
        // Creating Sheets using sheet object 
        Sheet sheet = wb.createSheet("Reports Sheet");
        System.out.println("Sheets Has been Created successfully"); 

        
        String[] COLUMNs = {"S. No.","CUSTOMER", "USER", "COMPANY", "DEPARTMENT","ACTIVITYTYPE","PLANNEDSTARTDATETIME","ACTUALSTARTDATETIME","PERFORMEDLOCATION","PLANNEDLOCATION","ACTIVITYSTATUS","ACTIVITYCONFIGURATIONTYPE"};

        Font headerFont = wb.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.RED.getIndex());
        
        CellStyle headerCellStyle = wb.createCellStyle();
        headerCellStyle.setFont(headerFont);
        
        Row headerRow = sheet.createRow(0);
        
     // Header
        for (int col = 0; col < COLUMNs.length; col++) {
        Cell cell = headerRow.createCell(col);
        cell.setCellValue(COLUMNs[col]);
        cell.setCellStyle(headerCellStyle);
        }
        
        int rowIdx = 1;
        for (Activity activity : activities) {
        Row row = sheet.createRow(rowIdx++);
        row.createCell(0).setCellValue(rowIdx-1);
        row.createCell(1).setCellValue(activity.getCustomer().getName());
        row.createCell(2).setCellValue(activity.getResource().getUserName());
        row.createCell(3).setCellValue(activity.getCompany().getName());
        row.createCell(4).setCellValue(activity.getDepartment().getName());
        row.createCell(5).setCellValue(activity.getActivityType().getName());
        row.createCell(6).setCellValue(activity.getPlannedStartDateTime().toGMTString());
        row.createCell(7).setCellValue(activity.getActualStartDateTime().toGMTString());
        row.createCell(8).setCellValue(activity.getPerformedLocation().getAddressloc());
        row.createCell(9).setCellValue(activity.getPlannedLocation().getAddressloc());
        row.createCell(10).setCellValue(activity.getActivityStatus().name());
        row.createCell(11).setCellValue(activity.getActivityConfigurationType().name());

        }
       
        // An output stream accepts output bytes and sends them to sink. 
       // FileOutputStream fileOut = new FileOutputStream("C:\\alphareports.xlsx"); 
       // /home/ubuntu/apps/test-mail
        
        String current = new java.io.File( "." ).getCanonicalPath();
        System.out.println("Current dir:"+current);
    	Constant.LOGGER.info("send email"+current);
        String currentDir = System.getProperty("user.dir");
        System.out.println("Current dir using System:" +currentDir);
    	Constant.LOGGER.info("send email"+currentDir);
    	String s = "alpha_reports_";
        String r = s.concat(userMobile);
        String filename = "/home/ubuntu/apps/test-mail/"+schemaNames+"/";
        Path path = Paths.get(filename);
        Constant.LOGGER.info("path"+path);
         if (!Files.exists(path)) {
            
            Files.createDirectory(path);
            System.out.println("Directory created");
        } else {
            
            System.out.println("Directory already exists");
        }
        FileOutputStream fileOut = new FileOutputStream(filename+r+".xlsx"); 
        
        //  String fileName = "vtrack_alpa" + SUFFIX + "alphareports.xlsx";
        // s3client.putObject(new PutObjectRequest("vtrack_alpa", fileName, 
        //       new File("C:\\Users\\user\\Desktop\\testvideo.mp4")));
        
        //-----------------
        wb.write(fileOut); 
        wb.close();
		
		return "success";
	}
	
//	 public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
//		    // create meta-data for your folder and set content-length to 0
//		    ObjectMetadata metadata = new ObjectMetadata();
//		    metadata.setContentLength(0);
//		    // create empty content
//		    InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
//		    // create a PutObjectRequest passing the folder name suffixed by /
//		    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
//		                folderName + SUFFIX, emptyContent, metadata);
//		    // send request to S3 to create folder
//		    client.putObject(putObjectRequest);
//		}
	
}
