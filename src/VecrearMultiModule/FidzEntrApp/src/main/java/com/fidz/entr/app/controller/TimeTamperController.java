package com.fidz.entr.app.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.TimeTamper;

import com.fidz.entr.app.service.TimeTamperService;



@CrossOrigin(maxAge = 3600)
@RestController("FEATimeTamperController")
@RequestMapping(value="/timetamper")
public class TimeTamperController {
	
	@Autowired
	private TimeTamperService  timeTamperService;
	@PostMapping("/checktamper")
	public Map<String, Object> checkTimeTamper(@Valid @RequestBody TimeTamper timeTamper){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return timeTamperService.checkTimeTamper(timeTamper);
		
	}

    @GetMapping("/s1")
    public @ResponseBody byte[] getImage() throws IOException {
        final InputStream in = getClass().getResourceAsStream("E:/Broadcast/sun.jpg");
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
        return IOUtils.toByteArray(in);
    }

    @GetMapping(value = "/s2", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] getImageWithMediaType() throws IOException {
        final InputStream in = getClass().getResourceAsStream("E:/Broadcast/sun.jpg");
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
        return IOUtils.toByteArray(in);
    }
    @GetMapping(value = "/s3", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getFile() throws IOException {
        final InputStream in = getClass().getResourceAsStream("E:/Broadcast/sun.jpg");
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
        return IOUtils.toByteArray(in);
    }
   
}
