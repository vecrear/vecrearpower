package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Application;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;

import lombok.Data;

@Data
@Document(collection = "companyappconfig")
@JsonIgnoreProperties(value = { "target" })
public class CompanyAppConfig extends com.fidz.entr.base.model.CompanyAppConfig {

	@NotNull
	protected boolean additionalUserFieldsExist = false;
	
	@NotNull
	protected boolean additionalCustomerFieldsExist = false;
	
	protected List<CustomField> additionalUserFields;
	
	protected List<CustomField> additionalCustomerFields;
	
	@NotNull
	protected boolean staticUserFieldsConfigChanged = false;
	
	@NotNull
	protected boolean staticCustomerFieldsConfigChanged = false;
	
	protected List<StaticField> staticUserFields;
	
	protected List<StaticField> staticCustomerFields;
	
	/**
	 * features - List.
	 * The features should be a subset of features of the associated application
	 */
	@DBRef(lazy = true)
	protected List<Feature> features;

	public CompanyAppConfig(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull Company company, @NotNull Application application,
			@NotNull String defaultCountryCode, @NotNull String defaultLanguageCode, @NotNull String defaultTimeZone,
			@NotNull boolean additionalUserFieldsExist, @NotNull boolean additionalCustomerFieldsExist,
			List<CustomField> additionalUserFields, List<CustomField> additionalCustomerFields,
			@NotNull boolean staticUserFieldsConfigChanged, @NotNull boolean staticCustomerFieldsConfigChanged,
			List<StaticField> staticUserFields, List<StaticField> staticCustomerFields, List<Feature> features) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName, id, company, application, defaultCountryCode, defaultLanguageCode, defaultTimeZone);
		this.additionalUserFieldsExist = additionalUserFieldsExist;
		this.additionalCustomerFieldsExist = additionalCustomerFieldsExist;
		this.additionalUserFields = additionalUserFields;
		this.additionalCustomerFields = additionalCustomerFields;
		this.staticUserFieldsConfigChanged = staticUserFieldsConfigChanged;
		this.staticCustomerFieldsConfigChanged = staticCustomerFieldsConfigChanged;
		this.staticUserFields = staticUserFields;
		this.staticCustomerFields = staticCustomerFields;
		this.features = features;
	}

	@Override
	public String toString() {
		return "CompanyAppConfig [additionalUserFieldsExist=" + additionalUserFieldsExist
				+ ", additionalCustomerFieldsExist=" + additionalCustomerFieldsExist + ", additionalUserFields="
				+ additionalUserFields + ", additionalCustomerFields=" + additionalCustomerFields
				+ ", staticUserFieldsConfigChanged=" + staticUserFieldsConfigChanged
				+ ", staticCustomerFieldsConfigChanged=" + staticCustomerFieldsConfigChanged + ", staticUserFields="
				+ staticUserFields + ", staticCustomerFields=" + staticCustomerFields + ", features=" + features
				+ ", id=" + id + ", company=" + company + ", application=" + application + ", defaultCountryCode="
				+ defaultCountryCode + ", defaultLanguageCode=" + defaultLanguageCode + ", defaultTimeZone="
				+ defaultTimeZone + ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + "]";
	}

	

	


	

	

}
