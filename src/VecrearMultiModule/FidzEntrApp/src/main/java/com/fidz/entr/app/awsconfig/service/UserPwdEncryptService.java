package com.fidz.entr.app.awsconfig.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;

import com.fidz.entr.app.awsconfig.model.Userpwdencrypt;
import com.fidz.entr.app.repository.UserPwdEncryptRepository;
import com.fidz.services.authentication.security.EncriptionUtil;


@Service("FEAUserPwdEncryptService")
public class UserPwdEncryptService {
	
	String AES = "AES";
	private GenericAppINterface<Userpwdencrypt> genericINterface;
	@Autowired
	public UserPwdEncryptService(GenericAppINterface<Userpwdencrypt> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	@Autowired
	private UserPwdEncryptRepository userPwdEncryptRepository;

	public List<Userpwdencrypt> getAllInfo(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findAll(Userpwdencrypt.class);
	}

	public String createInfo(@Valid Userpwdencrypt info) {
		//Userpwdencrypt userpwdencrypt = new Userpwdencrypt() ;	
		//String SchemaName = null;
		String encryptedPwd = null;
		if(info.getUserName()!= null) {
			try {
				String userName = info.getUserName();
				String userPwd = info.getPassword();
				
				System.out.println("yyyyyyyyyyyyyyyyyyyyyyyEncryption :: "+new EncriptionUtil().encrypt(info.getPassword(), new EncriptionUtil().getSecurityKey(info.getUserName())));
				System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxdecyption  :: "+new EncriptionUtil().decrypt("vBOwepSWWbtbUzigUXVNGA==", new EncriptionUtil().getSecurityKey("8088597794")));
			
				 encryptedPwd = new EncriptionUtil().encrypt(info.getPassword(), new EncriptionUtil().getSecurityKey(info.getUserName()));
				//userpwdencrypt.setUserName(userName);
				//userpwdencrypt.setPassword(userPwd);
				//userpwdencrypt.setEncryptedPwd(encryptedPwd);
				//System.out.println("userpwdencrypt--------  :: "+userpwdencrypt.toString());
				
				if(encryptedPwd != null) {
					
					try {
						encryptedPwd = "{\"userName\": " +"\""+userName+"\""+","+
				                    " \"userPwd\": " +"\""+userPwd+"\""+","+
				                    " \"encryptedPwd\": " +"\""+encryptedPwd+"\""+"}";
					}catch (Exception err){
					    
					}
					return encryptedPwd;
				}else {
					String encryptedPwdmsg = "please verify your credentials once";
					return 		encryptedPwd = "{\"userName\": " +"\""+userName+"\""+","+
		                    " \"userPwd\": " +"\""+userPwd+"\""+","+
		                    " \"encryptedPwd\": " +"\""+encryptedPwdmsg+"\""+"}";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return e.getMessage();
			} 
		}else {
			String encryptedPwdmsg = "please verify your credentials once";
			return 		encryptedPwd = "{\"userName\": " +"\""+info.getUserName()+"\""+","+
                    " \"userPwd\": " +"\""+info.getPassword()+"\""+","+
                    " \"encryptedPwd\": " +"\""+encryptedPwdmsg+"\""+"}";
		}
		

		
	}
	
	public String decryptInfo(@Valid Userpwdencrypt info) {
		String decryptpwd = null;
		String username = info.getUserName();
		String password = info.getPassword();
		decryptpwd = new EncriptionUtil().decrypt(password, new EncriptionUtil().getSecurityKey(username));

		return decryptpwd;
	}
	
	
	
}
