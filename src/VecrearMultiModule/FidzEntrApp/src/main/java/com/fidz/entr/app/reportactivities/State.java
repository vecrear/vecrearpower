package com.fidz.entr.app.reportactivities;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.TaggedCities;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class State  {
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	@Id
	protected String id;
	
	@NotNull
	protected String name;
	
	protected String code;
	@JsonIgnore
	protected boolean isStateTagged;
	@JsonIgnore
	protected String taggedRegionName;
	
	protected String description;
	
	@JsonIgnore
	protected List<Object> cities;
	@JsonIgnore
	protected List<TaggedCities> taggedCities;






	

}
