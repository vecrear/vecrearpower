package com.fidz.base.controller;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.DeviceNotFoundException;
import com.fidz.base.model.Device;
import com.fidz.base.model.Topic;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.service.DeviceService;


@CrossOrigin(maxAge = 3600)
@RestController("FBaseDeviceController")
@RequestMapping(value="/base/devices")
public class DeviceController {

	
	@Autowired
	private DeviceService deviceService;
	
	
	@PostMapping("/get/all")
    public List<Device> getAllDeviceData(@RequestHeader(value="schemaName") String schemaName) {
    	return deviceService.getAllDeviceData(schemaName);
    }
	
    @PostMapping
    public Device createDevice(@Valid @RequestBody Device device) {
        return deviceService.createDevice(device);
    }
    
    @PostMapping("/creates")
    public void createDevices(@Valid @RequestBody Device device) {
        deviceService.createDevices(device);
    }
    
    @PostMapping("/get/id/{id}")
    public Device getDeviceDataById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return deviceService.getDeviceDataById(id, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public Device updateDeviceData(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Device device) {
        return deviceService.updateDeviceData(id, device);    
        
    }
    
    @PostMapping("/get/id/deviceid/{deviceid}")
    public Device getDeviceDataByDeviceId(@PathVariable(value = "deviceid") final String deviceid, @RequestHeader(value="schemaName") String schemaName) {
		return deviceService.getDeviceDataByDeviceId(deviceid, schemaName);
    }
    
    //get topic name
    @PostMapping("/get/id/{id}/topic")
      public Topic getTopicById(@PathVariable(value = "id") final String id, @RequestHeader(value="schemaName") String schemaName) {
  		return deviceService.getTopicById(id, schemaName);
      }
  	//get topic name based on device id 
  	@PostMapping("/get/id/deviceid/{deviceid}/topic")
      public Topic getTopicByDeviceId(@PathVariable(value = "deviceid") final String deviceid, @RequestHeader(value="schemaName") String schemaName) {
  		return deviceService.getTopicByDeviceId(deviceid, schemaName);
      }
  	
  	//get devices by deviceTypeId
//  	@PostMapping("/get/type/devicetype/{id}")
//      public Stream<Device> getDevicesByDeviceTypeId(@PathVariable(value = "id") final String id, @RequestHeader(value="schemaName") String schemaName) {
//  		return deviceService.getDevicesByDeviceTypeId(id,schemaName);
//      }
  	
  	//get devices by deviceType
  	@PostMapping("/devicetype/name/{name}")
      public Stream<Device> getDevicesByDeviceType(@PathVariable(value = "name") final String name, @RequestHeader(value="schemaName") String schemaName) {
  		return deviceService.getDevicesByDeviceType(name, schemaName);
      }
  	
  	//update the current device data
  	@PatchMapping("/update/{id}/currentdata")
      public Device updateCurrentData(@PathVariable(value = "id") String id,
                                                     @Valid @RequestBody Object currentData, @RequestHeader(value="schemaName") String schemaName) {
  		return deviceService.updateCurrentData(id, currentData,schemaName);
         
      }
  	
  //update the current device data
  	@PatchMapping("/patch/{deviceid}/devicehardwaredata")
      public Device updateCurrentDataUsingDeviceId(@PathVariable(value = "deviceid") String deviceid,
                                                     @Valid @RequestBody Object deviceHardwareDetails, @RequestHeader(value="schemaName") String schemaName) {
  		return deviceService.updateCurrentDataDeviceId(deviceid, deviceHardwareDetails, schemaName);
         
      }
  	
	@PostMapping("/get/id/{id}/currentdata")
	public Object getCurrentDataById(@PathVariable(value = "id") final String id, @RequestHeader(value="schemaName") String schemaName) {
		return deviceService.getCurrentDataById(id, schemaName);
	}

	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteDevice(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		return deviceService.deleteDevice(id, schemaName);
	}

	@PostMapping("/delete/soft/id/{id}")
	public Map<String, String> softDeleteDevice(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
		return deviceService.deleteSoftCompany(id, schemaName, timeUpdate);
	}

	@PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public List<Device> streamAllDevices() {
		return deviceService.streamAllDevices();
	}


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Device with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(DeviceNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
