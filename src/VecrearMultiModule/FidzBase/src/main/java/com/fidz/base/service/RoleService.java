package com.fidz.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.GroupNameFoundException;
import com.fidz.base.exception.RoleNameFoundException;
import com.fidz.base.exception.RoleNotFoundException;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Group;
import com.fidz.base.model.Role;
import com.fidz.base.model.Status;
import com.fidz.base.repository.RoleRepository;
import com.fidz.base.validator.Constant;

@Service("FidzBaseRoleService")
public class RoleService {

	private GenericAppINterface<Role> genericINterface;
	@Autowired
	public RoleService(GenericAppINterface<Role> genericINterface){
	this.genericINterface=genericINterface;
	}
	@Autowired
	private RoleRepository roleRepository;
	
	
    public List<Role> getAllRoles(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	
    	Constant.LOGGER.info("Inside getAllRoles RoleService.java");
    	List<Role> roles=null;
    	try {
    	roles=this.genericINterface.findAll(Role.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllRoles RoleService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllRoles RoleService.java");
    
    	//return this.genericINterface.findAll(Role.class);
    	return roles;
    			
    	//return this.roleRepository.findAll();
    }
    
    public void createRole(Role role) {
    	String schemaName=role.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 List<Role> roles = this.genericINterface.findAll(Role.class);
    	 
	   	  List<String> names = roles.stream().map(p->p.getName()).collect(Collectors.toList());
	   		Constant.LOGGER.info("Group List data"+names);
	   	 
	   		if (names.stream().anyMatch(s -> s.equals(role.getName())==true)) {
	   			Constant.LOGGER.info("Inside if statement");
	   			throw new RoleNameFoundException(role.getName()+" "+"");
	   	 }
	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(role.getName())==true)) {
	   				Constant.LOGGER.info("Inside else if statement");
	   	      throw new RoleNameFoundException(role.getName()+"  "+"with different case");
	   		}else{
	   			Constant.LOGGER.info("Inside else statement");
	   			//return  this.featureRepository.save(feature);	
	    	this.genericINterface.saveName(role);
	        //return this.groupRepository.save(group);
	   		}
    	
    	
    }
    
    public Role createRoles(@Valid @RequestBody Role role) {
    	String schemaName=role.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 List<Role> roles = this.genericINterface.findAll(Role.class);
    	 
	   	  List<String> names = roles.stream().map(p->p.getName()).collect(Collectors.toList());
	   		Constant.LOGGER.info("Group List data"+names);
	   	 
	   		if (names.stream().anyMatch(s -> s.equals(role.getName())==true)) {
	   			Constant.LOGGER.info("Inside if statement");
	   			throw new RoleNameFoundException(role.getName()+" "+"");
	   	 }
	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(role.getName())==true)) {
	   				Constant.LOGGER.info("Inside else if statement");
	   	      throw new RoleNameFoundException(role.getName()+"  "+"with different case");
	   		}else{
	   			Constant.LOGGER.info("Inside else statement");
	   			//return  this.featureRepository.save(feature);	
	    	return this.roleRepository.save(role);
	   		}
	}
	
    
    /*public Optional<Role> getRoleById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.roleRepository.findById(id);
    }*/
    
    public ResponseEntity<Role> getRoleById(String id,String schemaName) {
    	Role role=null;
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	role=this.genericINterface.findById(id, Role.class);
    	if(role==null) {
    		throw new RoleNotFoundException(id);
    	}else {
    		return ResponseEntity.ok(role);
    	}
    	//return this.genericINterface.findById(id, Role.class);
    }
	
   
	public Role getRoleByName(String name, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.roleRepository.findByName(name);
	}
   
	
    public Role updateRole(String id, Role role) {
    	String schemaName=role.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	role.setId(id);
		return this.roleRepository.save(role);
    }
    
    public Map<String, String> deleteRole(String  id, String schemaName) {
  	  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
      	 this.roleRepository.deleteById(id);
      	 Map<String, String> response = new HashMap<String, String>();
      	    response.put("message", "Role deleted successfully");
      	    return response;
         
      }
      //soft delete
     public Map<String, String> deleteSoftRole(String  id, String schemaName, TimeUpdate timeUpdate) {
  		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		  Role role=this.genericINterface.findById(id, Role.class);
  		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		  role.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		  role.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		  role.setStatus(Status.INACTIVE);
  		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		  this.genericINterface.saveName(role);
  	    	 
  	    	 Map<String, String> response = new HashMap<String, String>();
  	    	    response.put("message", "Role deleted successfully");
  	    	    return response;
  	       
  	    }
 

    //Roles are Sent to the client as Server Sent Events
   
    public List<Role> streamAllRoles() {
        return this.roleRepository.findAll();
    }
  
    //To get all roles paginated
    //page number starts from zero
	public List<Role> getAllRolesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllRolesPaginated RoleService.java");
    	Query query = new Query();
    	List<Role> roles =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	roles= this.genericINterface.find(query, Role.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllRolesPaginated RoleService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllRolesPaginated "+query);
        return roles;
	
	}
}
