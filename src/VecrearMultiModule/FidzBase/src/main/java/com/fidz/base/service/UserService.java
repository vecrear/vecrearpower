package com.fidz.base.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.model.Stream;
import com.fidz.base.model.User;
import com.fidz.base.repository.BaseUserRepository;
import com.fidz.base.repository.GenericBaseINterface;
import com.fidz.base.validator.Constant;
/*
import com.fidz.services.notification.model.Notification;
import com.fidz.services.notification.service.NotificationForDeviceService;*/

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FBaseUserService")
public class UserService {
	private GenericAppINterface<User> genericINterface;
	@Autowired
	public UserService(GenericAppINterface<User> genericINterface){
		this.genericINterface=genericINterface;
	}
	@Autowired
	private BaseUserRepository  baseUserRepository;
	
	public List<User> getAllUsers(String schemaName){
		Constant.LOGGER.info("Inside getAllUsers UserService.java");
    	List<User> users=null;
    	try {
        users=this.genericINterface.findAll(User.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllUsers UserService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllUsers UserService.java");
    
    	//return this.genericINterface.findAll(Role.class);
    	return users;
		
	}
	
	public User createUser(User user){
		System.out.println("inside createUser userService.java");
		User users=null;
		try {
			Constant.LOGGER.debug(" Inside UserService.java Value for insert User record: createUser  :: " + user);
			 String schemaName=user.getSchemaName();
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	users=this.baseUserRepository.save(user);
			
		}catch(Exception e) {
			Constant.LOGGER.debug(" Inside UserService.java Value for insert User record: createUser  :: " + user);
		}
		
		return users;
	}
	
	
	public void createUsers(User user){
		 String schemaName=user.getSchemaName();
	    	
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		
   	    this.genericINterface.saveName(user);
	}
	public User getUserById(String id, String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, User.class);
		
	}
	
    public User updateUser(String id, User user) {
    	String schemaName=user.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    
    	user.setId(id);
		
    	return this.baseUserRepository.save(user);
        
    }

   
  //hard delete
  	public Map<String, String> deleteUser(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.baseUserRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "User deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftUser(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		User user = this.genericINterface.findById(id, User.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		user.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		user.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		user.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(user);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "User deleted successfully");
  		return response;

  	}
    public List<User> streamAllUsers() {
        return baseUserRepository.findAll();
    }
    
    public User updateUserPatch(String id,String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	User userMono=this.genericINterface.findById(id, User.class);
    	User user = null;
  		if (userMono != null) {
  			user = userMono;
  		}
  		user.setSchemaName(schemaName);
  		
     return this.baseUserRepository.save(user);
}
    
   

}
