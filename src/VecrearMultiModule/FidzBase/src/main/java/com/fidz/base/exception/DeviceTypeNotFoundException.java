package com.fidz.base.exception;

public class DeviceTypeNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4148156008678472062L;

	public DeviceTypeNotFoundException(String deviceId) {
		super("DeviceType data not found with id " + deviceId);
		// TODO Auto-generated constructor stub
	}
	
}
