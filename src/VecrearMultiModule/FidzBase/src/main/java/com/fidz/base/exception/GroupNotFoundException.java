package com.fidz.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class GroupNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6287956391030601431L;

	public GroupNotFoundException(String groupId) {
        super("Group not found with id " + groupId);
    }
}
