package com.fidz.base.exception;

public class GroupNameFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6133078221187302060L;
	
	public GroupNameFoundException(String group) {
		super("Group Name Already Exists " + group);
	}
	

}
