package com.fidz.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Base {
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	
	@LastModifiedBy
	protected String updatedByUser;
	
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	
	@LastModifiedBy
	protected String deletedByUser;
	
	@NotNull
	protected Status status;
	@NotNull
	protected String schemaName;
	
	

	public Base() {
		super();
	}



	public Base(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName) {
		super();
		this.createdTimestamp = createdTimestamp;
		this.createdByUser = createdByUser;
		this.updatedTimestamp = updatedTimestamp;
		this.updatedByUser = updatedByUser;
		this.deletedTimestamp = deletedTimestamp;
		this.deletedByUser = deletedByUser;
		this.status = status;
		this.schemaName = schemaName;
	}



	@Override
	public String toString() {
		return "Base [createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp="
				+ updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp
				+ ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}


	

	
	

}
