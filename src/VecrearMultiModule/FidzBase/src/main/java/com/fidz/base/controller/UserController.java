package com.fidz.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.StreamNotFoundException;
import com.fidz.base.model.User;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.service.UserService;


@CrossOrigin(maxAge = 3600)
@RestController("FBaseUserController")
@RequestMapping(value="/base/users")
public class UserController {

	@Autowired
	private UserService  userService;
	
	@PostMapping("/get/all")
	public List<User> getAllUsers(@RequestHeader(value="schemaName") String schemaName){
		return userService.getAllUsers(schemaName);
	}
	@PostMapping
	public User createUser(@Valid @RequestBody User user){
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		return userService.createUser(user);
	}
	
	@PostMapping("/creates")
	public void createUsers(@Valid @RequestBody User user){
		userService.createUsers(user);
	}
	
	@PostMapping("/get/id/{id}")
	public User getUserById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		return userService.getUserById(id, schemaName);
		
	}
	
	@PostMapping("/update/id/{id}")
    public User updateUser(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody User user) {
        return userService.updateUser(id, user);
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteUser(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return userService.deleteUser(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteUser(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return userService.deleteSoftUser(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<User> streamAllUsers() {
        return userService.streamAllUsers();
    }
    @PatchMapping("/update/id/{id}/patch")
    public User updateUserPatch(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody String schemaName) {
        return userService.updateUserPatch(id, schemaName);
    }
    
 // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A User with the same name already exists"));
    }

//    @ExceptionHandler
//    public ResponseEntity<?> handleNotFoundException(UserNotFoundException ex) {
//        return ResponseEntity.notFound().build();
//    }
   
}
