package com.fidz.base.config;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
@Configuration
@ComponentScan(basePackages = { "com.fidz.*" })
@ConfigurationProperties(prefix = "spring.data.mongodb")
//@PropertySource("classpath:application.properies")

@PropertySource(value="classpath:application.properties",ignoreResourceNotFound=true)
@EnableMongoRepositories(basePackages = "com.fidz")
public class MyConfig {
	
	private static String TAG = MyConfig.class.getSimpleName();
	//@Autowired
    //MongoTemplate mongoTemplate;
	@Value("${spring.data.mongodb.host}")
	private String mongoHost;
	
	@Value("${spring.data.mongodb.port}")
	private String mongoPort;
	
	@Value("${spring.data.mongodb.database}")
	private String mongoDB;
	
	 @Bean
	    public MongoClient mongo() throws Exception {
			System.out.println("mongoHost ::::"+mongoHost);
	        //return new MongoClient("192.168.1.12");
			MongoClientOptions.Builder options = MongoClientOptions.builder();
			options.socketKeepAlive(true);
			
			MongoClient mongoClient = new MongoClient(mongoHost, options.build());
		//return new MongoClient(mongoHost);
			System.out.println("mongoDB ::::"+mongoClient.toString());
			System.out.println("mongoDB ::::"+options.toString());
			return new MongoClient(mongoHost, options.build());
	    }
	@Bean
	  protected String getDatabaseName() {
		System.out.println("mongoDB ::::"+mongoDB);
		System.out.println("000################inside reactiveMongoTemplate ");
	    return mongoDB;
		//return "fidzentrapp1";
	  }
	 @Bean
	    public MongoTemplate mongoTemplate(final Mongo mongo) throws Exception {
	    	System.out.println("---------Entered into Vecrear MONGO DB database--------");

	        return new MongoTemplate(mongoDbFactory(mongo));
	    }
	   
	    public MultiTenantMongoDbFactory mongoDbFactory(final Mongo mongo) throws Exception {
	    	System.out.println("#####################################");
	        return new MultiTenantMongoDbFactory(mongo, getDatabaseName());
	    }
}
