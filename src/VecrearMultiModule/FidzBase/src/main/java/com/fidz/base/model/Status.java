package com.fidz.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum Status {
	DRAFT, ACTIVE, INACTIVE, DELETED
}
