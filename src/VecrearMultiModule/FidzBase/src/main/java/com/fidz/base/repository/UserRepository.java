package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.base.model.User;

import reactor.core.publisher.Mono;

//@Repository("fidzbaserepository")
public interface UserRepository extends MongoRepository<User, String>{
	//public Mono<User> findByUserName(String userName);
	public User findByUserName(String userName);
}
