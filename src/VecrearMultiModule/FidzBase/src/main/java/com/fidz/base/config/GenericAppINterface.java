package com.fidz.base.config;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;


@Repository
public interface GenericAppINterface<T> {

	void saveName(Object objectToSave);
	
	Optional<Object> saveNames(Object objectToSave);
	
	<T> List<T> findAll(Class<T> entityClass);

	<T> T findById(Object id, Class<T> entityClass);
	
	/*<T> T findByName(Object name, Class<T> ty);*/

	<T> T findByName(Object name, Class<T> entityClass);

	<T> List<T> findOne(Query query, Class<T> entityClass);
	<T> List<T> find(Query query, Class<T> entityClass);

	
	
	
}
