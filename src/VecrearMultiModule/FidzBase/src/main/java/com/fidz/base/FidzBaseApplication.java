package com.fidz.base;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class FidzBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(FidzBaseApplication.class, args);
	}
}
