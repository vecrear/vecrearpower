package com.fidz.base.exception;

public class StreamNameFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6379045781289169301L;
	
	public StreamNameFoundException(String stream) {
		super("Stream Name Already Exists " + stream);

	}

}
