package com.fidz.base.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.mongodb.client.result.DeleteResult;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Repository
public class GenericBaseInterfaceImpl<TY, T> implements GenericBaseINterface<TY>{
private final ReactiveMongoTemplate reactiveMongoTemplate;
	
    @Autowired
	public GenericBaseInterfaceImpl(ReactiveMongoTemplate reactiveMongoTemplate) {
	
	this.reactiveMongoTemplate = reactiveMongoTemplate;
   }

	@Override
	public Mono<TY> saveName(TY model1) {
		
		return reactiveMongoTemplate.save(model1);
	}

	@Override
	public Flux<TY> findAll(Class<TY> ty) {
		
		return reactiveMongoTemplate.findAll(ty);
	}

	@Override
	public Mono<TY> findById(Object id, Class<TY> ty ) {
		
		return reactiveMongoTemplate.findById(id, ty);
	}
	
	/*public <T> Mono<T> findAndRemove(String id, String schemaName) {
		return findAndRemove(id, schemaName);
	}*/
	/*@Override
	public Mono<DeleteResult> remove(Object object) {

		return reactiveMongoTemplate.remove(object);
	}*/
	@Override
	public Mono<DeleteResult> remove(Object object) {

		return reactiveMongoTemplate.remove(object);

		
	}
	@Override
	public  Mono<TY> findAndRemove(Query query, Class<TY> entityClass, String collectionName) {
		return reactiveMongoTemplate.findAndRemove(query, entityClass, collectionName);
	}
	
	@Override
	public Mono<TY> findByUserName(Object name, Class<TY> ty) {
		// TODO Auto-generated method stub
		return reactiveMongoTemplate.findById(name, ty);
	}
	
	@Override
	public Mono<TY> findByUserNames(Object id, Class<TY> ty, String name) {
		// TODO Auto-generated method stub
		return reactiveMongoTemplate.findById(id, ty, name);
	}
/*
	@Override
	public Mono<TY> findById(Object id, Class<TY> ty ) {
		
		return reactiveMongoTemplate.findById(id, ty);
	}*/
	/*public <T> Mono<T> findById(Object id, Class<T> entityClass) {
		return findById(id, entityClass, determineCollectionName(entityClass));
	}*/

	
	
	/*public <T> Mono<T> findById(Object id, Class<T> entityClass, String collectionName) {

		MongoPersistentEntity<?> persistentEntity = mappingContext.getPersistentEntity(entityClass);
		MongoPersistentProperty idProperty = persistentEntity != null ? persistentEntity.getIdProperty() : null;

		String idKey = idProperty == null ? ID_FIELD : idProperty.getName();

		return doFindOne(collectionName, new Document(idKey, id), null, entityClass, null);
	}*/
}
