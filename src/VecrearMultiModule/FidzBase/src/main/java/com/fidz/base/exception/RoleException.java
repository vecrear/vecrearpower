package com.fidz.base.exception;

public class RoleException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8668598053597275610L;

	public RoleException(String message) {
        super(message);
    }
}
