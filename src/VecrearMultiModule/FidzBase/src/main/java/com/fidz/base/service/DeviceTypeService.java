package com.fidz.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.ApplicationNameFoundException;
import com.fidz.base.exception.DeviceTypeNameFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Device;
import com.fidz.base.model.DeviceType;
import com.fidz.base.model.Status;
import com.fidz.base.repository.BaseDeviceTypeRepository;
import com.fidz.base.validator.Constant;

@Service("FBaseDeviceTypeService")
public class DeviceTypeService {
	@Autowired
	private BaseDeviceTypeRepository basedeviceTypeRepository;
	
	private GenericAppINterface<DeviceType> genericINterface;
	@Autowired
	public DeviceTypeService(GenericAppINterface<DeviceType> genericINterface){
		this.genericINterface=genericINterface;
	}
	
    public List<DeviceType> getAllDeviceTypes(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDeviceTypes getAllDeviceTypes.java");
    	List<DeviceType> devType=null;
    	try {
    	devType=this.genericINterface.findAll(DeviceType.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllDeviceTypes getAllDeviceTypes.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllDeviceTypes getAllDeviceTypes.java");
    	return devType;
    }
	
    public DeviceType createDeviceType(DeviceType deviceType) {
    	
    	String schemaName=deviceType.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 List<DeviceType> deviceTypes = this.genericINterface.findAll(DeviceType.class);
    	 
    	  List<String> names = deviceTypes.stream().map(p->p.getName()).collect(Collectors.toList());
    		Constant.LOGGER.info("DeviceType List data"+names);
    	 
    		if (names.stream().anyMatch(s -> s.equals(deviceType.getName())==true)) {
    			Constant.LOGGER.info("Inside if statement");
    			throw new DeviceTypeNameFoundException(deviceType.getName()+" "+"");
    	 }
    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(deviceType.getName())==true)) {
    				Constant.LOGGER.info("Inside else if statement");
    	  throw new DeviceTypeNameFoundException(deviceType.getName()+"  "+"with different case");
    		}else{
    			Constant.LOGGER.info("Inside else statement");
    			return this.basedeviceTypeRepository.save(deviceType);    	 }
    	}
    
    
    public void createDeviceTypes(DeviceType deviceType) {
    	String schemaName=deviceType.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	this.genericINterface.saveName(deviceType);
    }
    
    public DeviceType getDeviceTypeById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, DeviceType.class);
		
    }
	
    public DeviceType getDeviceTypeByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.basedeviceTypeRepository.findByName(name);
		
    }
	
    public DeviceType updateDeviceType(String id, DeviceType deviceType) {
    	String schemaName=deviceType.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	deviceType.setId(id);
    	return this.basedeviceTypeRepository.save(deviceType);
       
    }
    //hard delete
	public Map<String, String> deleteDeviceType(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.basedeviceTypeRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Device Type deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftDeviceType(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		DeviceType deviceType = this.genericINterface.findById(id, DeviceType.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		deviceType.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		deviceType.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		deviceType.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(deviceType);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Device Type deleted successfully");
		return response;

	}
   

    public List<DeviceType> streamAllDeviceTypes() {
        return basedeviceTypeRepository.findAll();
    }
}
