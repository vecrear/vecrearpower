package com.fidz.base.controller;

import java.util.List;

import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.exception.GroupNotFoundException;
import com.fidz.base.model.Group;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.service.GroupService;
import com.mongodb.DuplicateKeyException;


@CrossOrigin(maxAge = 3600)
@RestController("BaseGroupController")
@RequestMapping(value="/groups")
public class GroupController {
	
	@Autowired
	private GroupService groupService;

	@PostMapping("/get/all")
    public List<Group> getAllGroups(@RequestHeader(value="schemaName") String schemaName) {
    return groupService.getAllGroups(schemaName);
	}
	
	//Post request to get all groups  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Group> getAllGroupsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
    return groupService.getAllGroupsPaginated(pageNumber, pageSize, schemaName);
    }
    
    @PostMapping("/creates")
    public void createGroup(@Valid @RequestBody Group group) {
        groupService.createGroup(group);
    }
    @PostMapping
    public Group createGroups(@Valid @RequestBody Group group) {
        return groupService.createGroups(group);
    }
   
	/*@GetMapping("/get/{id}")
    public Optional<Group> getGroupById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return groupService.getGroupById(id, schemaName);
    }*/
    @PostMapping("/get/id/{id}")
    public ResponseEntity<Group> getGroupById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return groupService.getGroupById(id, schemaName);
    }
    
    @PostMapping("/get/name/{name}")
	public Group getGroupByName(@PathVariable(value = "name") String name,
			@RequestHeader(value = "schemaName") String schemaName) {
		return groupService.getGroupByName(name, schemaName);
	}
   
	@PostMapping("/update/id/{id}")
    public Group updateGroup(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Group group) {
        return groupService.updateGroup(id, group);
    }
    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteGroup(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return groupService.deleteGroup(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteGroup(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return groupService.deleteSoftGroup(id, schemaName,timeUpdate);
    }
    //Groups are Sent to the client as Server Sent Events
    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Group> streamAllGroups() {
        return groupService.streamAllGroups();
    }

    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Group with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(GroupNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }	
  /*  @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("Server Error", details);
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
 
    @ExceptionHandler(GroupNotFoundException.class)
    public final ResponseEntity<Object> handleUserNotFoundException(GroupNotFoundException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse1 error = new ErrorResponse1("Record Not Found", details);
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }*/
    /*protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ErrorResponse1 error = new ErrorResponse1("Validation Failed", details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }*/
 
}
