package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.base.model.Device;

//@Repository("fidzbaserepository")
public interface DeviceRepository extends MongoRepository<Device, String>{
	//public Mono<Device> findByDeviceId(String deviceId);
	public Device findByDeviceId(String deviceId);
	
}
