package com.fidz.base.exception;

public class UserNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2381588076340650490L;

	public UserNotFoundException(String userId) {
        //super("User not found with id " + userId);
		super(userId);
    }
}
