package com.fidz.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RoleNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4128803771587525711L;

	public RoleNotFoundException(String roleId) {
        super("Role not found with id " + roleId);
    }
}
