  package com.fidz.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.exception.ApplicationNotFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Stream;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.service.ApplicationService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/applications")
public class ApplicationController {
	
	@Autowired
	private ApplicationService applicationService;
	
	@PostMapping("/get/all")
    public List<Application> getAllApplications(@RequestHeader(value="schemaName") String schemaName) {
    	return applicationService.getAllApplications(schemaName);
    }
	

	//Post request to get all applications  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<Application> getAllApplicationsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return applicationService.getAllApplicationsPaginated(pageNumber, pageSize, schemaName);
    }

	@PostMapping
	public Application createApplication(@Valid @RequestBody Application application) {
		return applicationService.createApplication(application);
	}
	
    @PostMapping("/creates")
    public Application createApplications(@Valid @RequestBody Application application) {
        return applicationService.createApplications(application);
    }
    
    @PostMapping("/get/id/{id}")
    public Application getApplicationById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return applicationService.getApplicationById(id, schemaName);
    }
    
    @PostMapping("/get/name/appname/{name}")
    public Application getApplicationByAppName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return applicationService.getApplicationByAppName(name, schemaName);
    }
    
    @PostMapping("/update/id/{id}")
    public Application updateApplication(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Application application, @RequestHeader(value="schemaName") String schemaName) {
        return applicationService.updateApplication(id, application, schemaName);
    }
    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteApplication(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return applicationService.deleteApplication(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteApplication(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return applicationService.deleteSoftApplication(id, schemaName, timeUpdate);
    }
    
    //Applications are Sent to the client as Server Sent Events
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Application> streamAllApplications() {
        return applicationService.streamAllApplications();
    }
	

    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Application with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ApplicationNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }	
}
