package com.fidz.base.exception;

public class StreamNotFoundException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2545367721862142993L;

	public StreamNotFoundException(String deviceId) {
		super("Device data not found with id " + deviceId);
		// TODO Auto-generated constructor stub
	}
}
