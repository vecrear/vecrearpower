package com.fidz.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import lombok.Data;


@Data
@Document(collection = "device")
@JsonIgnoreProperties(value = { "target" })
public class Device extends Base {
	@Id
	protected String id;
	
	
	@NotBlank
	@Indexed(unique = true)
	@NotNull
	protected String deviceId;
	
//	@DBRef(lazy = true)
//	protected DeviceType deviceType;
	
	protected DeviceOsType deviceType;
	
	protected Object schema;
	
	@DBRef(lazy = true)
	protected Stream  stream;
	
	protected Object currentData;
	
	protected String imei;
	
	protected Object deviceHardwareDetails;

	public Device() {
		// TODO Auto-generated constructor stub
	}

	
	
	
	

	

	

	

}
