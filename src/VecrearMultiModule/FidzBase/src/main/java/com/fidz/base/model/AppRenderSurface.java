package com.fidz.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum AppRenderSurface {
	WEB, ANDROID, IOS, WINDOWS, BLACKBERRY, HYBRID, NOTSPECIFIED
}
