package com.fidz.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PutMapping;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.DeviceTypeNameFoundException;
import com.fidz.base.exception.FeatureNameFoundException;
import com.fidz.base.model.DeviceType;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Group;
import com.fidz.base.model.Status;
import com.fidz.base.repository.FeatureRepository;
import com.fidz.base.validator.Constant;

@Service
public class FeatureService {
	@Autowired
	private FeatureRepository featureRepository;
	
	private GenericAppINterface<Feature> genericINterface;
	@Autowired
	public FeatureService(GenericAppINterface<Feature> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	//get all
    public List<Feature> getAllFeatures(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllFeatures FeatureService.java");
    	List<Feature> features=null;
    	try {
    	features=this.genericINterface.findAll(Feature.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllFeatures FeatureService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllFeatures FeatureService.java");
    	return features;
    }
    //save
    public Feature createFeature(Feature feature) {
    	String schemaName=feature.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 List<Feature> features = this.genericINterface.findAll(Feature.class);
    	 
   	  List<String> names = features.stream().map(p->p.getName()).collect(Collectors.toList());
   		Constant.LOGGER.info("DeviceType List data"+names);
   	 
   		if (names.stream().anyMatch(s -> s.equals(feature.getName())==true)) {
   			Constant.LOGGER.info("Inside if statement");
   			throw new FeatureNameFoundException(feature.getName()+" "+"");
   	 }
   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(feature.getName())==true)) {
   				Constant.LOGGER.info("Inside else if statement");
   	  throw new FeatureNameFoundException(feature.getName()+"  "+"with different case");
   		}else{
   			Constant.LOGGER.info("Inside else statement");
   			return  this.featureRepository.save(feature);	
   			}
   	}
    	
	 
    
    //save
	public Feature createFeatures(Feature feature) {
		String schemaName = feature.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.featureRepository.save(feature);

	}
    //get by id
    public Feature getFeatureById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Feature.class);
    }
    
	//get by name
    public Feature getFeatureByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.featureRepository.findByName(name);
    }
	
    public Feature updateFeature(String id, Feature feature, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	feature.setId(id);
    	return this.featureRepository.save(feature);
    }

    //hard delete
    public Map<String, String> deleteFeature(String  id, String schemaName) {
 	  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	 this.featureRepository.deleteById(id);
     	 Map<String, String> response = new HashMap<String, String>();
     	    response.put("message", "Group deleted successfully");
     	    return response;
        
     }
     //soft delete
	public Map<String, String> deleteSoftFeature(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Feature feature = this.genericINterface.findById(id, Feature.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		feature.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		feature.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		feature.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(feature);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Feature deleted successfully");
		return response;

	}

    
    public List<Feature> streamAllFeatures() {
        return this.featureRepository.findAll();
    }

    //To get all features paginated
    //page number starts from zero
	public List<Feature> getAllFeaturesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllFeaturesPaginated FeatureService.java");
    	Query query = new Query();
    	List<Feature> features =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	features= this.genericINterface.find(query, Feature.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllFeaturesPaginated FeatureService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllFeaturesPaginated "+query);
        return features;	
	}
	
	
	

}
