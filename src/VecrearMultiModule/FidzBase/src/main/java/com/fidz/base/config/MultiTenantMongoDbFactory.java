package com.fidz.base.config;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MultiTenantMongoDbFactory extends SimpleMongoDbFactory{
	
	
	private MongoTemplate mongoTemplate;
	private final String defaultName;
	private static final ThreadLocal<String> dbName = new ThreadLocal<String>();
	private static final HashMap<String, Object> databaseIndexMap = new HashMap<String, Object>();
	private static final Logger logger = LoggerFactory.getLogger(MultiTenantMongoDbFactory.class);
	
	public MultiTenantMongoDbFactory(Mongo mongo, String databaseName) {
		super((MongoClient) mongo, databaseName);
		// TODO Auto-generated constructor stub
		 logger.debug("Instantiating " + MultiTenantMongoDbFactory.class.getName() + " with default database name: " + databaseName);
		this.defaultName = databaseName;
	}
	
	 public void setMongoTemplate(final MongoTemplate mongoTemplate) {
	        
	        this.mongoTemplate = mongoTemplate;
	    }
	 
	 public static void setDatabaseNameForCurrentThread(final String databaseName) {
	       System.out.println("inside setDatabaseNameForCurrentThread########");
	        dbName.set(databaseName);
	    }
	    
	 
	    public static void clearDatabaseNameForCurrentThread() {
	       
	        dbName.remove();
	    }
	    @Override
	    public MongoDatabase getDb() {
	        final String tlName = dbName.get();
	        final String dbToUse = (tlName != null ? tlName : this.defaultName);
	        logger.debug("Acquiring database: " + dbToUse);
	        createIndexIfNecessaryFor(dbToUse);
	        return super.getDb(dbToUse);
	    }

	 
	    
	    private void createIndexIfNecessaryFor(final String database) {
	        if (this.mongoTemplate == null) {
	            
	            return;
	        }
//	      sync and init once
	        boolean needsToBeCreated = false;
	        synchronized (MultiTenantMongoDbFactory.class) {
	            final Object syncObj = databaseIndexMap.get(database);
	            if (syncObj == null) {
	                databaseIndexMap.put(database, new Object());
	                needsToBeCreated = true;
	            }
	        }
//	      
	    }
	 
	
}
