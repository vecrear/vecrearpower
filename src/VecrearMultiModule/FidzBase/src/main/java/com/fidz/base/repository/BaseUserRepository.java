package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.base.model.User;
@Repository("fidzbaseuserrepository")
public interface BaseUserRepository extends MongoRepository<User, String>{
	//public Mono<User> findByUserName(String userName);
	public User findByUserName(String userName);
}



