package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.base.model.Feature;
@Repository("fidzbaseFeaturerepository")
public interface FeatureRepository extends MongoRepository<Feature, String>{
	//public Mono<Feature> findByName(String name);
	public Feature findByName(String name);
}
