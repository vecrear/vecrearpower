package com.fidz.base.exception;

public class ServerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8792943829299892399L;
	
	public ServerException(String messsage) {
		super("server exception " + messsage);
	}

}
