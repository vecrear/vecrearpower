package com.fidz.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.exception.RoleNotFoundException;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Role;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.service.RoleService;
import com.mongodb.DuplicateKeyException;

@CrossOrigin(maxAge = 3600)
@RestController("BaseRoleController")
@RequestMapping(value="/roles")
public class RoleController {
	@Autowired
	private RoleService roleService;
	
	
	@PostMapping("/get/all")
    public List<Role> getAllRoles(@RequestHeader(value="schemaName") String schemaName) {
    	return roleService.getAllRoles(schemaName);
    }
  
	//Post request to get all roles  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Role> getAllRolesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return roleService.getAllRolesPaginated(pageNumber, pageSize, schemaName);
	 }
	
	@PostMapping("/creates")
    public void createRole(@Valid @RequestBody Role role) {
        roleService.createRole(role);
    }

	@PostMapping
	public Role createRoles(@Valid @RequestBody Role role) {
		return roleService.createRoles(role);
	}
	
	/*@GetMapping("/get/id/{id}")
    public Optional<Role> getRoleById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return roleService.getRoleById(id, schemaName);
    }*/
	
	@PostMapping("/get/id/{id}")
    public ResponseEntity<Role> getRoleById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return roleService.getRoleById(id, schemaName);
    }
	
	    @PostMapping("/get/name/{name}")
		public Role getRoleByName(@PathVariable(value = "name") String name,
				@RequestHeader(value = "schemaName") String schemaName) {
			return roleService.getRoleByName(name, schemaName);
		}
	   
		@PostMapping("/update/id/{id}")
	    public Role updateRole(@PathVariable(value = "id") String id,
	                                                   @Valid @RequestBody Role role) {
	        return roleService.updateRole(id, role);
	    }
	    
	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteRole(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		return roleService.deleteRole(id, schemaName);
	}
	    @PostMapping("/delete/soft/id/{id}")
	    public Map<String, String> softDeleteRole(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
	        return roleService.deleteSoftRole(id, schemaName, timeUpdate);
	    }
	  
	// Roles are Sent to the client as Server Sent Events
	@GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public List<Role> streamAllRoles() {
		return roleService.streamAllRoles();
	}

   //  Exception Handling
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Role with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(RoleNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }	
    
   /* @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleRoleException(RoleException ex) {
    	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex.getMessage()));
    }
    
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleIllegalArgumentException(IllegalArgumentException ex) {
    	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorResponse(ex.getMessage()));
    }*/   
	
	   /*@ExceptionHandler(Exception.class)
	    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
	        List<String> details = new ArrayList<>();
	        details.add(ex.getLocalizedMessage());
	        ErrorResponse1 error = new ErrorResponse1("Server Error", details);
	        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	 
	    @ExceptionHandler(RoleNotFoundException.class)
	    public final ResponseEntity<Object> handleUserNotFoundException(RoleNotFoundException ex, WebRequest request) {
	        List<String> details = new ArrayList<>();
	        details.add(ex.getLocalizedMessage());
	        ErrorResponse1 error = new ErrorResponse1("Record Not Found", details);
	        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
	    }*/
}
