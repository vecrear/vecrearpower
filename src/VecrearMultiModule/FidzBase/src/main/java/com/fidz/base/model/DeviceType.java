package com.fidz.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;


@Data
@Document(collection = "devicetype")
@JsonIgnoreProperties(value = { "target" })
public class DeviceType extends Base {

	@Id
	protected String id;
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	protected String description;

	protected Object schema;

	public DeviceType(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotBlank String name, String description, Object schema) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.description = description;
		this.schema = schema;
	}

	@Override
	public String toString() {
		return "DeviceType [id=" + id + ", name=" + name + ", description=" + description + ", schema=" + schema
				+ ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp="
				+ updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp
				+ ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}



	

	
	
}
