package com.fidz.base.exception;

public class FeatureNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8876817717343422962L;

	public FeatureNotFoundException(String feature) {
        super("Feature not found with id/name " + feature);
    }
}
