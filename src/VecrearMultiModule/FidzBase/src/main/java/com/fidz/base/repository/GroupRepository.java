package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.fidz.base.model.Group;

//@Repository
public interface GroupRepository extends MongoRepository<Group, String>{
	
	public Group findByName(String name);

}
