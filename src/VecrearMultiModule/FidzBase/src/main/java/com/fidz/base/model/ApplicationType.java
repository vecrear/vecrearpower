package com.fidz.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//External application or internal application (Fidz application)
@JsonIgnoreProperties(value = { "target" })
public enum ApplicationType {
	EXTERNAL, INTERNAL, NOTSPECIFIED
}
