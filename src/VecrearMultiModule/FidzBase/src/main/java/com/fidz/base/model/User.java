package com.fidz.base.model;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "user")
@JsonIgnoreProperties(value = { "target" })
public class User extends Base {
	@Id
	protected String id;
	
	@NotBlank
	@NotNull
	@Indexed(unique = true)
	protected String userName;
	
	protected String password; //should be encrypted password
	
	protected PersonName name;
	
	protected Address address;
	
	protected Contact contact;
	
	protected PersonName emergencyContactName;
	
	protected Contact emergencyContact;
	
	@DBRef(lazy = true)
	protected List<Role> appRoles;
	public User() {
		
	}
	

	public User(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, String userName, String password, PersonName name, Address address,
			Contact contact, PersonName emergencyContactName, Contact emergencyContact, List<Role> appRoles) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.name = name;
		this.address = address;
		this.contact = contact;
		this.emergencyContactName = emergencyContactName;
		this.emergencyContact = emergencyContact;
		this.appRoles = appRoles;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", name=" + name + ", address=" + address + ", contact="
				+ contact + ", emergencyContactName=" + emergencyContactName + ", emergencyContact=" + emergencyContact
				+ ", appRoles=" + appRoles + ", createdTimestamp=" + createdTimestamp + ", createdByUser="
				+ createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser
				+ ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status
				+ ", schemaName=" + schemaName + "]";
	}

	

	

	
	
}
