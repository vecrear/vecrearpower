package com.fidz.base.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.ApplicationNameFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Base;
import com.fidz.base.model.Device;
import com.fidz.base.model.Status;
import com.fidz.base.model.Topic;
import com.fidz.base.repository.BaseDeviceRepository;
import com.fidz.base.repository.DeviceRepository;
import com.fidz.base.validator.Constant;

@Service("FBaseDeviceService")
public class DeviceService {

	@Autowired
	private BaseDeviceRepository baseDeviceRepository;
	
	private GenericAppINterface<Device> genericINterface;
	@Autowired
	public DeviceService(GenericAppINterface<Device> genericINterface){
		this.genericINterface=genericINterface;
	}
	
    public List<Device> getAllDeviceData(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDeviceData DeviceService.java");
    	List<Device> dev=null;
    	try {
    	dev=this.genericINterface.findAll(Device.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllDeviceData DeviceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllDeviceData DeviceService.java");
    	return dev;
    }
	
    public Device createDevice(Device device) {
    	String schemaName=device.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	// List<Device> devices = this.genericINterface.findAll(Device.class);
    	return this.baseDeviceRepository.save(device);
        //return deviceRepository.save(device);
    }
    public void createDevices(@Valid @RequestBody Device device) {
    	String schemaName=device.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        this.genericINterface.saveName(device);
    }
    public Device getDeviceDataById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, Device.class);
		//return deviceRepository.findById(id);
    }
    public Object getCurrentDataById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Device deviceMono=this.genericINterface.findById(id, Device.class);
	    Device device = null;
		if (deviceMono != null) {
			device = deviceMono;
		}
		return device.getCurrentData();
		
	}
    
  //update the current device data
  	public  Device updateCurrentData(String id, Object currentData, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		
  		Device deviceMono=this.genericINterface.findById(id, Device.class);
  	    Device device = null;
  		if (deviceMono != null) {
  			device = deviceMono;
  		}
  		device.setCurrentData(currentData);
  		return this.baseDeviceRepository.save(device);
  		
  	}
   /// patch request for device
  
  	
	public  Device updateCurrentDataDeviceId(String deviceid, Object deviceHardwareDetails, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device deviceMono=this.baseDeviceRepository.findByDeviceId(deviceid);
  	    Device device = null;
  		if (deviceMono != null) {
  			device = deviceMono;
  		}
  		device.setDeviceHardwareDetails(deviceHardwareDetails);
  		return this.baseDeviceRepository.save(device);
  		
  	}
	
    public Device updateDeviceData(String id, Device device) {
    	String schemaName=device.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	device.setId(id);
    	return this.baseDeviceRepository.save(device);
    }
    
    
    //hard delete
    public Map<String, String> deleteDevice(String  id, String schemaName) {
 	  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	 this.baseDeviceRepository.deleteById(id);
     	 Map<String, String> response = new HashMap<String, String>();
     	    response.put("message", "Device deleted successfully");
     	    return response;
        
     }
     //soft delete
    public Map<String, String> deleteSoftCompany(String  id, String schemaName, TimeUpdate timeUpdate) {
 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		 Device device=this.genericINterface.findById(id, Device.class);
 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
 		device.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
 		device.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
 		device.setStatus(Status.INACTIVE);
 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		  this.genericINterface.saveName(device);
 	    	 
 	    	 Map<String, String> response = new HashMap<String, String>();
 	    	    response.put("message", "Device deleted successfully");
 	    	    return response;
 	       
 	    }
    
    public Device getDeviceDataByDeviceId(String deviceid, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    //	System.out.println("@@@@@@@@@@@@@@@this.deviceRepository.findByDeviceId(deviceid)"+this.baseDeviceRepository.findByDeviceId(deviceid));
		//return this.deviceRepository.findByDeviceId(deviceid);
    	return this.baseDeviceRepository.findByDeviceId(deviceid);
	}
    //get topic
    public Topic getTopicById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//Device device = this.deviceRepository.findById(id);
    	Device device = this.genericINterface.findById(id, Device.class);
    	Topic monotopic = null;
		if (device.getStream() != null) {

			Topic topic = new Topic(device.getStream().getTopic());
			topic.setName(device.getStream().getTopic());
			monotopic = topic;
		}

		return monotopic;
	}
    
    
    public Topic getTopicByDeviceId(String deviceid, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device device = this.baseDeviceRepository.findByDeviceId(deviceid);

		Topic monotopic = null;
		if (device.getStream() != null) {
			Topic topic = new Topic(device.getStream().getTopic());
			System.out.println("topic name:" + topic.getName());
			topic.setName(device.getStream().getTopic());
			monotopic = topic;
		}

		return monotopic;

	}
    
    public Stream<Device> getDevicesByDeviceType(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	//List<Device> dev= this.genericINterface.findAll(Device.class);
    	return this.genericINterface.findAll(Device.class).stream().filter(data -> data.getDeviceType().equals(name));
    	//filter(dev -> dev.getDeviceType().getName().contains(name));
        //.filter(dev -> dev.getDeviceType().getName().contains(name));
  	}
  	
//  	public Stream<Device> getDevicesByDeviceTypeId(String id, String schemaName) {
//  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//        return this.baseDeviceRepository.findAll().stream().filter(data -> data.getDeviceType().contains(id));
//        		//.filter(devices -> devices.getDeviceType().getId().contains(id));
//  	}
    public List<Device> streamAllDevices() {
        return baseDeviceRepository.findAll();
    }
}
