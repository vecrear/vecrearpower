package com.fidz.base.exception;

public class DeviceTypeNameFoundException  extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5390016888467658671L;

	public DeviceTypeNameFoundException(String deviceType) {
		super("DeviceType Name already Exits " + deviceType);
	}

}
