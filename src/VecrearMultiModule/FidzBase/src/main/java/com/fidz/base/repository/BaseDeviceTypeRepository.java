package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.base.model.DeviceType;
@Repository("fidzdevicetypebaserepository")
public interface BaseDeviceTypeRepository extends MongoRepository<DeviceType, String>{
	public DeviceType findByName(String name);
}
