package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.base.model.Stream;

@Repository("fidzbaseStreamrepository")
public interface BaseStreamRepository extends MongoRepository<Stream, String>{
	public Stream findByName(String name);
	
}
