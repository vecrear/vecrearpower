package com.fidz.base.controller;

import java.util.Date;

import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;
@Data
public class TimeUpdate {
	
	@LastModifiedBy
	protected String deletedByFidzUser;
	

	@LastModifiedDate
	protected Date deletedFidzTimestamp;
	
	public TimeUpdate() {
		
	}


	public TimeUpdate(String deletedByFidzUser, Date deletedFidzTimestamp) {
		super();
		this.deletedByFidzUser = deletedByFidzUser;
		this.deletedFidzTimestamp = deletedFidzTimestamp;
	}


	@Override
	public String toString() {
		return "TimeUpdate [deletedByFidzUser=" + deletedByFidzUser + ", deletedFidzTimestamp=" + deletedFidzTimestamp
				+ "]";
	}


	

	
	
	
	
	
}
