package com.fidz.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.exception.FeatureNotFoundException;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Group;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.service.FeatureService;
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/features")
public class FeatureController {
	@Autowired
	private FeatureService featureService;
	
	@PostMapping("/get/all")
    public List<Feature> getAllFeatures(@RequestHeader(value="schemaName") String schemaName) {
    	return featureService.getAllFeatures(schemaName);
    }
	
	//Post request to get all features  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Feature> getAllFeaturesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return featureService.getAllFeaturesPaginated(pageNumber, pageSize, schemaName);
	 }

	@PostMapping("/creates")
	public void createFeature(@Valid @RequestBody Feature feature) {
		featureService.createFeature(feature);
	}
	
	@PostMapping
	public Feature createFeatures(@Valid @RequestBody Feature feature) {
		return featureService.createFeatures(feature);
	}
	
	@PostMapping("/get/id/{id}")
    public Feature getFeatureById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return featureService.getFeatureById(id, schemaName);
    }
	
	@PostMapping("/get/name/featurename/{name}")
    public Feature getFeatureByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return featureService.getFeatureByName(name, schemaName);
    }
	
	
	@PostMapping("/update/id/{id}")
	public Feature updateFeature(@PathVariable(value = "id") String id,
			@Valid @RequestBody Feature feature, @RequestHeader(value="schemaName") String schemaName) {
		return featureService.updateFeature(id, feature, schemaName);
	}

	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteFeature(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		return featureService.deleteFeature(id, schemaName);
	}

	@PostMapping("/delete/soft/id/{id}")
	public Map<String, String> softDeleteFeature(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
		return featureService.deleteSoftFeature(id, schemaName, timeUpdate);
	}
	
   
    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Feature> streamAllFeatures() {
        return featureService.streamAllFeatures();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Feature with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(FeatureNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }	
}
