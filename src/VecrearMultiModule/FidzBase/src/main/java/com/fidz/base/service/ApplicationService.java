package com.fidz.base.service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.ApplicationNameFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Status;
import com.fidz.base.model.Stream;
import com.fidz.base.repository.ApplicationRepository;
import com.fidz.base.validator.Constant;

@Service
public class ApplicationService {
	
	@Autowired
	private ApplicationRepository applicationRepository;
	
	private GenericAppINterface<Application> genericINterface;
	@Autowired
	public ApplicationService(GenericAppINterface<Application> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	
    public List<Application> getAllApplications(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllApllication ApplicationService.java");
    	List<Application> app=null;
    	try {
        app=this.genericINterface.findAll(Application.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllApllication ApplicationService.java"+ex.getMessage());
         }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllApllication ApplicationService.java");
    	return app;
    }

	
 public Application createApplication(Application application) {
  Constant.LOGGER.info("+++++++++++++++++++++++++++++");
  String schemaName=application.getSchemaName();
  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  List<Application> applcations = this.genericINterface.findAll(Application.class);
 
  List<String> names = applcations.stream().map(p->p.getName()).collect(Collectors.toList());
	Constant.LOGGER.info("Application List data"+names);
 
  if (names.stream().anyMatch(s -> s.equals(application.getName())==true)) {
  Constant.LOGGER.info("Inside if statement");
  throw new ApplicationNameFoundException(application.getName()+" "+"");
  }else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(application.getName())==true)) {
  Constant.LOGGER.info("Inside else if statement");
  throw new ApplicationNameFoundException(application.getName()+"  "+"with different case");
  }else{
  Constant.LOGGER.info("Inside else statement");
  return this.applicationRepository.save(application);
  }
  }
	
	
    
    public Application createApplications(Application application) {
    	String schemaName=application.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        return this.applicationRepository.save(application);
    }
    
    
    public Application getApplicationById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Application.class);
    }
	
   
    public Application getApplicationByAppName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.applicationRepository.findByName(name);
    }
	
   
    public Application updateApplication(String id, Application application, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	application.setId(id);
    	return this.applicationRepository.save(application);
    	
    }

    //hard delete
    public Map<String, String> deleteApplication(String  id, String schemaName) {
 	  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	 this.applicationRepository.deleteById(id);
     	 Map<String, String> response = new HashMap<String, String>();
     	    response.put("message", "Application deleted successfully");
     	    return response;
        
     }
     //soft delete
    public Map<String, String> deleteSoftApplication(String  id, String schemaName, TimeUpdate timeUpdate) {
 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		 Application application=this.genericINterface.findById(id, Application.class);
 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
 		application.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
 		application.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
 		application.setStatus(Status.INACTIVE);
 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		  this.genericINterface.saveName(application);
 	    	 
 	    	 Map<String, String> response = new HashMap<String, String>();
 	    	    response.put("message", "Application deleted successfully");
 	    	    return response;
 	       
 	    }
    

    //Applications are Sent to the client as Server Sent Events
   
    public List<Application> streamAllApplications() {
        return this.applicationRepository.findAll();
    }

  //To get all applications paginated
    //page number starts from zero
	public List<Application> getAllApplicationsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllApplicationsPaginated ApplicationService.java");
    	Query query = new Query();
    	List<Application> applications =null;
    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	applications= this.genericINterface.find(query, Application.class);	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllApplicationsPaginated ApplicationService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllApplicationsPaginated "+query);
        return applications;	
	}

}
