package com.fidz.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum DeviceOsType {
	ANDROID,IOS,WEB,WINDOWS,WATCH,OTHER
}
