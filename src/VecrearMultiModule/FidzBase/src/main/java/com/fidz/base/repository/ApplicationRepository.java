package com.fidz.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.fidz.base.model.Application;

//@Repository
public interface ApplicationRepository extends MongoRepository<Application, String>{
	//Mono<Application> findByName(String name);
	public Application findByName(String name);
}
