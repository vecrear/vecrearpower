package com.fidz.base.exception;

public class ApplicationNameFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2907590101100692501L;
	
	public ApplicationNameFoundException(String Application) {
		super("Application name already exits " + Application);
	}
 
}
