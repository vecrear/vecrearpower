package com.fidz.base.config;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
@Repository
public class GenericAppInterfaceImpl<TY,T> implements GenericAppINterface<TY>{
	 private final MongoTemplate mongoTemplate;
	@Autowired
		public GenericAppInterfaceImpl(MongoTemplate mongoTemplate) {
		
		this.mongoTemplate = mongoTemplate;
	   }
	
	@Override
	public void saveName(Object objectToSave) {
		
    mongoTemplate.save(objectToSave);
		
	}

	@Override
	public <T> List<T> findAll(Class<T> entityClass) {
		// TODO Auto-generated method stub
		return mongoTemplate.findAll(entityClass);
	}

	

	@Override
	public <T> T findById(Object id, Class<T> entityClass) {
		// TODO Auto-generated method stub
		return mongoTemplate.findById(id, entityClass);
	}

	@Override
	public <T> T findByName(Object name, Class<T> entityClass) {
		// TODO Auto-generated method stub
		return mongoTemplate.findById(name, entityClass);
	}

	@Override
	public Optional<Object> saveNames(Object objectToSave) {
		 mongoTemplate.save(objectToSave);
		return null;
	}

	
	
	public <T> List<T> findOne(Query query, Class<T> entityClass) {
		return (List<T>) mongoTemplate.findOne(query, entityClass);
		
	}

	@Override
	public <T> List<T> find(Query query, Class<T> entityClass) {
		// TODO Auto-generated method stub
		return (List<T>) mongoTemplate.find(query, entityClass);
	}




	

	

	

	
	

}
