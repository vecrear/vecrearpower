package com.fidz.base.exception;

public class RoleNameFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7000692405041870119L;
	
	public RoleNameFoundException(String role) {
		super("Role Name Already Exists " + role);

	}

}
