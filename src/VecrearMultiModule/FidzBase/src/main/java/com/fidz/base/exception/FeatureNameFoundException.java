package com.fidz.base.exception;

public class FeatureNameFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7272750464899441876L;
	
	public FeatureNameFoundException(String feature) {
		super("Feature Name Already exits " + feature);
	}

}
