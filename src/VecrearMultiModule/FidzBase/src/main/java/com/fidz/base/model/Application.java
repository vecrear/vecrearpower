package com.fidz.base.model;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "application")
@JsonIgnoreProperties(value = { "target" })
public class Application extends Base {
	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	protected String description;
	
	//External Application or Internal Application (FiDz Application)
	@NotNull
	protected ApplicationType type;
	
	//WEB, ANDROID, IOS, WINDOWS, BLACKBERRY, HYBRID
	@NotNull
	protected AppRenderSurface appRenderSurface;
	
	@DBRef(lazy = true)
	protected List<Role> roles;
	
	@DBRef(lazy = true)
	protected List<Feature> features;

	public Application(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotBlank String name, String description,
			@NotNull ApplicationType type, @NotNull AppRenderSurface appRenderSurface, List<Role> roles,
			List<Feature> features) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.description = description;
		this.type = type;
		this.appRenderSurface = appRenderSurface;
		this.roles = roles;
		this.features = features;
	}

	@Override
	public String toString() {
		return "Application [id=" + id + ", name=" + name + ", description=" + description + ", type=" + type
				+ ", appRenderSurface=" + appRenderSurface + ", roles=" + roles + ", features=" + features
				+ ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp="
				+ updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp
				+ ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	
}
