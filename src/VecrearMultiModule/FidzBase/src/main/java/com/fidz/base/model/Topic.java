package com.fidz.base.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Topic {
	protected String name;

	public Topic(String name) {
		super();
		this.name = name;
	}

	@Override
	public String toString() {
		return "Topic [name=" + name + "]";
	}

}
