package com.fidz.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.FeatureNameFoundException;
import com.fidz.base.exception.RoleNameFoundException;
import com.fidz.base.model.Role;
import com.fidz.base.model.Status;
import com.fidz.base.model.Stream;
import com.fidz.base.repository.BaseStreamRepository;
import com.fidz.base.validator.Constant;


@Service("FBaseStreamService")
public class StreamService {

	@Autowired
	private BaseStreamRepository baseStreamRepository;
	
	private GenericAppINterface<Stream> genericINterface;

	@Autowired
	public StreamService(GenericAppINterface<Stream> genericINterface) {
		this.genericINterface = genericINterface;
	}

	public List<Stream> getAllStreams(String schemaName) {
MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	
    	Constant.LOGGER.info("Inside getAllStreams StreamService.java");
    	List<Stream> streams=null;
    	try {
    	streams=this.genericINterface.findAll(Stream.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllStreams StreamService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllStreams StreamService.java");
    
    	//return this.genericINterface.findAll(Role.class);
    	return streams;
	}
		
	public Stream createStream(Stream stream) {
		String schemaName = stream.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 List<Stream> streams = this.genericINterface.findAll(Stream.class);
    	 
	   	  List<String> names = streams.stream().map(p->p.getName()).collect(Collectors.toList());
	   		Constant.LOGGER.info("Group List data"+names);
	   	 
	   		if (names.stream().anyMatch(s -> s.equals(stream.getName())==true)) {
	   			Constant.LOGGER.info("Inside if statement");
	   			throw new FeatureNameFoundException(stream.getName()+" "+"");
	   	 }
	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(stream.getName())==true)) {
	   				Constant.LOGGER.info("Inside else if statement");
	   	     throw new FeatureNameFoundException(stream.getName()+"  "+"with different case");
	   		}else{
	   			Constant.LOGGER.info("Inside else statement");
	   		Constant.LOGGER.info("Successfull Stream creation"+stream.toString());	
		return this.baseStreamRepository.save(stream);
	   		}
	}
	public void createStreams(Stream stream) {
		Constant.LOGGER.info("Inside CreateStreams StreamService.java");
		String schemaName = stream.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		
	    	this.genericINterface.saveName(stream);
	        //return this.groupRepository.save(group);
	   		
		
		
	}
	    
	public Stream getStreamById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Stream.class);

	}
		
	public Stream getStreamByName(String name, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.baseStreamRepository.findByName(name);

	}
		
	    public Stream updateStream(String id, Stream stream) {
	    	String schemaName = stream.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			stream.setId(id);
			return this.baseStreamRepository.save(stream);
	        
	    }
	    //hard delete
	    public Map<String, String> deleteStream(String  id, String schemaName) {
	 	  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     	 this.baseStreamRepository.deleteById(id);
	     	 Map<String, String> response = new HashMap<String, String>();
	     	    response.put("message", "Stream deleted successfully");
	     	    return response;
	        
	     }
	     //soft delete
	    public Map<String, String> deleteSoftStream(String  id, String schemaName, TimeUpdate timeUpdate) {
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    Stream stream=this.genericINterface.findById(id, Stream.class);
	    //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
	 	stream.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
	 	stream.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
	 	stream.setStatus(Status.INACTIVE);
	 	// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
	 	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	 	 this.genericINterface.saveName(stream);
	 	    	 
	 	 Map<String, String> response = new HashMap<String, String>();
	     response.put("message", "Stream deleted successfully");
	 	 return response;
	 	       
	 	    }

	   

	    public List<Stream> streamAllStreams() {
	        return baseStreamRepository.findAll();
	    }

	 
}
