package com.fidz.base.exception;

public class ApplicationNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1757163888278906300L;

	public ApplicationNotFoundException(String app) {
        super("Application not found with id/name " + app);
    }
}
