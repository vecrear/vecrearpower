package com.fidz.base.exception;

public class DeviceNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3335236889217995409L;
	
	public DeviceNotFoundException(String deviceId) {
		super("Device data not found with id " + deviceId);
		// TODO Auto-generated constructor stub
	}

}
