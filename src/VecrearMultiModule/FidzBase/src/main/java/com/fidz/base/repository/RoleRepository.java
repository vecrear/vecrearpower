package com.fidz.base.repository;


import org.springframework.data.mongodb.repository.MongoRepository;

import com.fidz.base.model.Role;

//@Repository
public interface RoleRepository extends MongoRepository<Role, String>{

	Role findByName(String name);
}
