package com.fidz.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.FeatureNameFoundException;
import com.fidz.base.exception.GroupNameFoundException;
import com.fidz.base.exception.GroupNotFoundException;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Group;
import com.fidz.base.model.Status;
import com.fidz.base.repository.GroupRepository;
import com.fidz.base.validator.Constant;
@Service("baseservice")
public class GroupService {
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	private GroupRepository groupRepository;
	private GenericAppINterface<Group> genericINterface;
	@Autowired
	public GroupService(GenericAppINterface<Group> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	public List<Group> getAllGroups(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllGroups GroupService.java");
    	List<Group> groups=null;
    	try {
    	groups=this.genericINterface.findAll(Group.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllGroups GroupService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllGroups GroupService.java");
    	return groups;
    	//return this.groupRepository.findAll();
    }
 
	/* public void createGroup(Group group) {
		 try {
			Constant.LOGGER.debug(" Inside TestDBMT.java Value for insert TestDBMT record :: "+group);
	    	String schemaName=group.getSchemaName();
	    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	this.genericINterface.saveName(group);
	        //return this.groupRepository.save(group);
		 }catch(Exception e) {
				Constant.LOGGER.error("Exception inside TestDBMT.java createTestDBMT::"+e.getMessage());
			}
	    }*/
	public void createGroup(Group group) {
		 
	    	String schemaName=group.getSchemaName();
	    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	 List<Group> groups = this.genericINterface.findAll(Group.class);
    	 
	   	  List<String> names = groups.stream().map(p->p.getName()).collect(Collectors.toList());
	   		Constant.LOGGER.info("Group List data"+names);
	   	 
	   		if (names.stream().anyMatch(s -> s.equals(group.getName())==true)) {
	   			Constant.LOGGER.info("Inside if statement");
	   			throw new GroupNameFoundException(group.getName()+" "+"");
	   	 }
	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(group.getName())==true)) {
	   				Constant.LOGGER.info("Inside else if statement");
	   	      throw new GroupNameFoundException(group.getName()+"  "+"with different case");
	   		}else{
	   			Constant.LOGGER.info("Inside else statement");
	   			//return  this.featureRepository.save(feature);	
	    	this.genericINterface.saveName(group);
	        //return this.groupRepository.save(group);
	   		}
	    }
	 public Group createGroups(Group group) {
		 String schemaName=group.getSchemaName();
	    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	 List<Group> groups = this.genericINterface.findAll(Group.class);
 	 
	   	  List<String> names = groups.stream().map(p->p.getName()).collect(Collectors.toList());
	   		Constant.LOGGER.info("Group List data"+names);
	   	 
	   		if (names.stream().anyMatch(s -> s.equals(group.getName())==true)) {
	   			Constant.LOGGER.info("Inside if statement");
	   			throw new GroupNameFoundException(group.getName()+" "+"");
	   	 }
	   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(group.getName())==true)) {
	   				Constant.LOGGER.info("Inside else if statement");
	   	      throw new GroupNameFoundException(group.getName()+"  "+"with different case");
	   		}else{
	   			Constant.LOGGER.info("Inside else statement");
	   			//return  this.featureRepository.save(feature);	
	    	return this.groupRepository.save(group);
	        //return this.groupRepository.save(group);
	   		}
	    	
	    }
	
	 
   /* public Optional<Group> getGroupById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	//return this.genericINterface.findById(id, Group.class);
		return this.groupRepository.findById(id);
    }*/
	 public ResponseEntity<Group> getGroupById(String id, String schemaName) {
		    Group groups=null;
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			groups=this.genericINterface.findById(id, Group.class);
			if (groups == null) {
				throw new GroupNotFoundException(id);
	           
	        } else {
	            return ResponseEntity.ok(groups);
	        }
	    	//MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			//return this.genericINterface.findById(id, Group.class);
	    }
	
	 public Group getGroupByName(String name,String schemaName) {
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 return this.groupRepository.findByName(name);
			//return this.genericINterface.findByName(name, Group.class);
		}
    
    public Group updateGroup(String id, Group group) {
    	 String schemaName=group.getSchemaName();
    	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	group.setId(id);
		return this.groupRepository.save(group);
    	
    }
    //hard delete
   public Map<String, String> deleteGroup(String  id, String schemaName) {
	  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 this.groupRepository.deleteById(id);
    	 Map<String, String> response = new HashMap<String, String>();
    	    response.put("message", "Group deleted successfully");
    	    return response;
       
    }
    //soft delete
   public Map<String, String> deleteSoftGroup(String  id, String schemaName, TimeUpdate timeUpdate) {
		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		  Group group=this.genericINterface.findById(id, Group.class);
		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		  group.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		  group.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		  group.setStatus(Status.INACTIVE);
		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		  this.genericINterface.saveName(group);
	    	 
	    	 Map<String, String> response = new HashMap<String, String>();
	    	    response.put("message", "Group deleted successfully");
	    	    return response;
	       
	    }

    //Groups are Sent to the client as Server Sent Events
    
    public List<Group> streamAllGroups() {
        return groupRepository.findAll();
    }
    
    //To get all countries paginated
    //page number starts from zero
	public List<Group> getAllGroupsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllGroupsPaginated GroupService.java");
    	Query query = new Query();
    	List<Group> groups =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	groups= this.genericINterface.find(query, Group.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllGroupsPaginated GroupService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllGroupsPaginated "+query);
        return groups;	
	}
	
	

}
