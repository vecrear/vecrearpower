package com.fidz.base.model;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

//Privileges or Permissions are grouped together as Roles
@Data
@Document(collection = "group")
@JsonIgnoreProperties(value = { "target" })
public class Group extends Base {
	
	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	
	protected String description;

	public Group(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotBlank String name, String description) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.description = description;
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + ", description=" + description + ", createdTimestamp="
				+ createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp
				+ ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser="
				+ deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	

	

	
	
	
}
