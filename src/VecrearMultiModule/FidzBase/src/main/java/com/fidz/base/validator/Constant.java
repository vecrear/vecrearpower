package com.fidz.base.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Constant {
	//public static final Logger LOGGER = LogManager.getLogger(Constant.class);
	public static final Logger LOGGER = LoggerFactory.getLogger(Constant.class);
	public static String activityTypeCreated = "New Activity has been mapped to you";
	public static String activityTypeDetails = "New Activity details has been mapped to you";
	public static String customerCreated = "New customer has been mapped to you";
	public static String taskCreatedfromWeb = "New task has been assigned to you from web";
	public static String taskCreatedfromdevice = "New task has been assigned to you from device";
	
}
