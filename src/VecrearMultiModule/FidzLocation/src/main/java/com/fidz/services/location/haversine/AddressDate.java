package com.fidz.services.location.haversine;

import lombok.Data;

@Data
public class AddressDate {
	private double latitude;
	private String address;
	private String dateTime;
	
	
	public static AddressDate addressDate = new AddressDate();
	
	// Constructors
	public AddressDate() {
		super();
	}
	
	public AddressDate(double latitude,String address, String dateTime) {
		super();
		this.latitude = latitude;
		this.address = address;
		this.dateTime = dateTime;
	}
	
	public static AddressDate getAddressDate() {
		return addressDate;
	}

	public static void setAddressDate(AddressDate addressDate) {
		AddressDate.addressDate = addressDate;
	}
	@Override
	public String toString() {
		return "AddressDate [address=" + address + ", dateTime=" + dateTime + ", latitude=" + latitude + "]";
	}
}