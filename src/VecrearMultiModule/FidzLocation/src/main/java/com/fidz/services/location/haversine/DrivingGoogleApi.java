package com.fidz.services.location.haversine;

import java.util.List;

import lombok.Data;

@Data
public class DrivingGoogleApi {
	protected double distance;
	protected List <Double> latitude;
	protected List <Double> longitude;
	
	public static DrivingGoogleApi drivingGoogleApi = new DrivingGoogleApi();
	
	public DrivingGoogleApi() {
		super();
	}

	public DrivingGoogleApi(double distance, List<Double> latitude, List<Double> longitude) {
		super();
		this.distance = distance;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public static DrivingGoogleApi getDrivingGoogleApi() {
		return drivingGoogleApi;
	}
	public static void setDrivingGoogleApi(DrivingGoogleApi drivingGoogleApi) {
		DrivingGoogleApi.drivingGoogleApi =  drivingGoogleApi;
	}
	
	@Override
	public String toString() {
		return "DrivingGoogleApi [distance=" + distance + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}	
}
