package com.fidz.services.location.haversine;
import lombok.Data;

@Data
public class NoiseAdjDistanceAlgorithm {
	private double noiseAdjTotalDistance;
	private double noiseAdjMedian; 
	private double noiseAdjStdDev; 
 	private int noiseAdjTotalCount;
	private double percentileThresholdValue;
	private double distanceLost;
	private int noOfPointsDropped;
	private String category;
	
	public static NoiseAdjDistanceAlgorithm naAlgorithm = new NoiseAdjDistanceAlgorithm();
	
	// Constructors
	public NoiseAdjDistanceAlgorithm() {
		super();
	}
	
	public NoiseAdjDistanceAlgorithm(double noiseAdjTotalDistance, double noiseAdjMedian, double noiseAdjStdDev,
			int noiseAdjTotalCount, double percentileThresholdValue, double distanceLost, int noOfPointsDropped,
			String category) {
		super();
		this.noiseAdjTotalDistance = noiseAdjTotalDistance;
		this.noiseAdjMedian = noiseAdjMedian;
		this.noiseAdjStdDev = noiseAdjStdDev;
		this.noiseAdjTotalCount = noiseAdjTotalCount;
		this.percentileThresholdValue = percentileThresholdValue;
		this.distanceLost = distanceLost;
		this.noOfPointsDropped = noOfPointsDropped;
		this.category = category;
	}
	
	// Getter and setter methods
	
	public double getNoiseAdjTotalDistance() {
		return noiseAdjTotalDistance;
	}

	public void setNoiseAdjTotalDistance(double noiseAdjTotalDistance) {
		this.noiseAdjTotalDistance = noiseAdjTotalDistance;
	}

	public double getNoiseAdjMedian() {
		return noiseAdjMedian;
	}

	public void setNoiseAdjMedian(double noiseAdjMedian) {
		this.noiseAdjMedian = noiseAdjMedian;
	}

	public double getNoiseAdjStdDev() {
		return noiseAdjStdDev;
	}

	public void setNoiseAdjStdDev(double noiseAdjStdDev) {
		this.noiseAdjStdDev = noiseAdjStdDev;
	}

	public int getNoiseAdjTotalCount() {
		return noiseAdjTotalCount;
	}

	public void setNoiseAdjTotalCount(int noiseAdjTotalCount) {
		this.noiseAdjTotalCount = noiseAdjTotalCount;
	}

	public double getPercentileThresholdValue() {
		return percentileThresholdValue;
	}

	public void setPercentileThresholdValue(double percentileThresholdValue) {
		this.percentileThresholdValue = percentileThresholdValue;
	}

	public double getDistanceLost() {
		return distanceLost;
	}

	public void setDistanceLost(double distanceLost) {
		this.distanceLost = distanceLost;
	}

	public int getNoOfPointsDropped() {
		return noOfPointsDropped;
	}

	public void setNoOfPointsDropped(int noOfPointsDropped) {
		this.noOfPointsDropped = noOfPointsDropped;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public static NoiseAdjDistanceAlgorithm getNaAlgorithm() {
		return naAlgorithm;
	}

	public static void setNaAlgorithm(NoiseAdjDistanceAlgorithm naAlgorithm) {
		NoiseAdjDistanceAlgorithm.naAlgorithm = naAlgorithm;
	}

	@Override
	public String toString() {
		return "NoiseAdjDistanceAlgorithm [noiseAdjTotalDistance=" + noiseAdjTotalDistance + ", noiseAdjMedian="
				+ noiseAdjMedian + ", noiseAdjStdDev=" + noiseAdjStdDev + ", noiseAdjTotalCount=" + noiseAdjTotalCount
				+ ", percentileThresholdValue=" + percentileThresholdValue + ", distanceLost=" + distanceLost
				+ ", noOfPointsDropped=" + noOfPointsDropped + ", category=" + category + "]";
	}
}
