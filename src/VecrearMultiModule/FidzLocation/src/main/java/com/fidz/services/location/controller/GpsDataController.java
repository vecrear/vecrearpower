package com.fidz.services.location.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.validator.Constant;
import com.fidz.services.location.model.GpsData;
import com.fidz.services.location.model.GpsDataDistnaceCal;
import com.fidz.services.location.model.GpsDataMultiUser;
import com.fidz.services.location.service.GpsDataService;

@CrossOrigin(maxAge = 3600)
@RestController
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/gpsdatas")
public class GpsDataController {
	
	@Autowired
	private GpsDataService gpsDataService;
	
	@Value("${gpsdataservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all")
    public List<GpsData> getAllGpsData(@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.getAllGpsData(schemaName);
    	
    }
	
	@PostMapping("/reports/{userId}/sdate={startdate}|edate={enddate}")
    public List<GpsData> getGpsDataBasedOnData(@PathVariable(value="userId") String userId,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.getGpsDataBasedOnData(schemaName,userId,startdate,enddate);
    	
    }
	@PostMapping("/multiuser")
    public List<GpsData> getGpsDataBasedOnMultiUser(@RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody GpsDataMultiUser gpsDataMultiUser) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.getGpsDataBasedOnMultiUser(schemaName,gpsDataMultiUser);
    	
    }
	
	
	@PostMapping("/multiusernew")
    public List<GpsDataDistnaceCal> getGpsDataBasedOnMultiUserNew(@RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody GpsDataMultiUser gpsDataMultiUser) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.getGpsDataBasedOnMultiUserNew(schemaName,gpsDataMultiUser);
    	
    }
	
	
	
	
	
	@PostMapping("/multiuser/distancecount")
    public Double getGpsDataBasedOnMultiUseDistance(@RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody GpsDataMultiUser gpsDataMultiUser) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.getGpsDataBasedOnMultiUseDistance(schemaName,gpsDataMultiUser);
    	
    }

	@PostMapping("/get/id/deviceid/{deviceid}")
    public List<GpsData> getGpsDataByDeviceId(@PathVariable(value = "deviceid") final String deviceid,@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.getGpsDataByDeviceId(deviceid, schemaName);
    	
    }

	@PostMapping
	public GpsData createGpsData(@Valid @RequestBody GpsData gpsData) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.createGpsData(gpsData);
		// return geofenceRepository.save(geofence);
	}

	@PostMapping("/creates")
	public void createGpsDatas(@Valid @RequestBody GpsData gpsData) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		gpsDataService.createGpsData(gpsData);
	}

	@PostMapping("/update/id/{id}")
	public GpsData updateGpsData(@PathVariable(value = "id") String id, @Valid @RequestBody GpsData gpsData) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.updateGpsData(id, gpsData);
	}

	@PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public List<GpsData> streamAllGpsData() {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.streamAllGpsData();
	}

	@PostMapping("/delete/id/{id}")
	public Map<String, String> deleteGpsData(@PathVariable(value = "id") String id,
			@RequestHeader(value = "schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Gpsdataservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return gpsDataService.deleteGpsData(id, schemaName);
	}
	
	//getting the data from the users
	 @PostMapping("/deviceid/{deviceid}/sdate={startdate}|edate={enddate}")
     public List<GpsData> getGps_dataUsersByDate(@PathVariable(value="deviceid") String deviceid,@PathVariable(value="startdate") String startdate,@PathVariable(value="enddate") String enddate, @RequestHeader(value="schemaName") String schemaName) throws ParseException{
		 System.out.println("property value******"+property);
			String message="Gpsdataservices are disabled";
			if(!property) 
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message); 	   
			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
			Constant.LOGGER.info("URL:"+baseUrl);
			return gpsDataService.getGps_dataUsersByDate(deviceid,startdate,enddate,schemaName);
     }
	 
	 //getting the data of dynamic keys
	 @PostMapping("/awsinfo/byschemaname")
		public List<com.fidz.services.location.model.AwsInfo> getAWSnfo(
				@RequestHeader(value = "schemaName") String schemaName) {
			
			return gpsDataService.getAWSnfo(schemaName);
		}

	 
//	 @PostMapping("/latitude={latitude}|longitude={longitude}")
//		public String reverseGeoCoding(@PathVariable(value = "latitude") double latitude, @PathVariable(value = "longitude") double longitude) {
//			String message="Gpsdataservices are disabled";
//			if(!property) 
//				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
//			final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
//			Constant.LOGGER.info("URL:"+baseUrl);
//			return gpsDataService.reverseGeoCoding(latitude, longitude);
//		}
	
}
