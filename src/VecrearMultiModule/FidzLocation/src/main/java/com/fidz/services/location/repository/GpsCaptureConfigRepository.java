package com.fidz.services.location.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.fidz.services.location.model.GPSCaptureConfig;



public interface GpsCaptureConfigRepository extends MongoRepository<GPSCaptureConfig, String> {

}
