// marker util activated
// google api removed (in HaversineService)
// api to return list [distance,size of array,  array of lat longs]
package com.fidz.services.location.haversine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
//import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fidz.services.location.model.GpsData;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import org.springframework.data.mongodb.core.MongoTemplate;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.base.config.GenericAppINterface;
import com.google.gson.Gson;

//import com.distance.calculation.service.HaversineUtil;

@Service 
public class HaversineReductionService {	
	
	@Autowired
	MongoTemplate mongoTemplate;

    //static Haversine algorithmCalculation = Haversine.getHaversine();
    static DistanceAlgorithm algorithmCalculation = DistanceAlgorithm.getAlgorithm();
    static NoiseAdjDistanceAlgorithm naAlgorithmCalculation = NoiseAdjDistanceAlgorithm.getNaAlgorithm();
    //static NoiseAdjHaversine naAlgorithmCalculation = NoiseAdjHaversine.getNaHaversine();
    static AddressDate addressDate = AddressDate.getAddressDate();
    //****** DrivingGoogleApi
    static DrivingGoogleApi drivingGoogleApi = DrivingGoogleApi.getDrivingGoogleApi();
    // Stores the key: value; latitude: {address, dateTime}
    static Map<Double, AddressDate> map = new HashMap<Double, AddressDate>();
    
    //@Autowired
    //private HaversineUtil haversineUtil; //used to call the utility service with additional methods 
    @Autowired
    private DistanceAlgorithmUtil_Copy1 algoUtil;
    
    //******************************************* 
    @Autowired
    private MarkerReductionUtil markerUtil;
    //******************************************* 


    //to get the gps users from the Database
    public List<GpsData> getGpsInfo_dataUsersByDate(String deviceid,String startdate, String enddate, String schemaName) throws ParseException {
		 //System.out.println("------------------Entering getGpsInfo_dataUsersByDate() in Haversine Service!!--------------------"); 
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 Query query = null;
    	 List<GpsData> gps = null;
    	 try { 
    		 String date_string = "2019-10-17";
    		 DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    		 Date startDate = format.parse(startdate);
    		 Date endDate = format.parse(enddate);
    		 
    		 Query query1 = new Query().addCriteria(Criteria.where("dateTime").gte(startDate).lte(endDate).and("deviceID").is(deviceid));
    		 System.out.println("Query "+ query1);
    		 gps = this.mongoTemplate.find(query1, GpsData.class);  
    	 }
    	 catch(Exception ex) {
    		 Constant.LOGGER.error("Error occurred" + ex);
   		}
         return gps;
	}    
     

	/**
	 * Populating the array using GPS API
	 */
	public double[][] createLatLongArray(String rawJson) throws ParseException {
		JSONArray root = (JSONArray) JSONValue.parseWithException(rawJson);
		JSONObject rootObj, gpsData,mapData;
		int size = root.size();
		double userPath[][] = new double [size][2];		
		
		for (int i = 0; i < size; i++){
			// Parsing through the java object
			rootObj = (JSONObject) root.get(i);
			gpsData = (JSONObject) rootObj.get("gpsData");
			mapData = (JSONObject) gpsData.get("map");
			double latitudeData = (double) mapData.get("latitude");
			double longitudeData = (double) mapData.get("longitude");
			String addressData = (String) mapData.get("address");
			String dateTimeData = (String) mapData.get("dateTime");
			//System.out.println("Latitude: " + latitudeData + "  "+"Longitude: " + longitudeData +" "+ "Adress: "+ addressData +" "+"dateTime: "+ dateTimeData);
			userPath[i][0] = latitudeData;
			userPath[i][1] = longitudeData;
			
			// Storing values into HashMap
			AddressDate data = new AddressDate(latitudeData, addressData, dateTimeData);
			map.put(data.getLatitude(), data);	
		}
		return userPath;
	}
   
	//end createLatLongArray()
		
	/** Distance Calculation related methods */
	
	// Map( dist: [lat,long])  // lat long of coordinate wrt start coordinate entered //-------------
	static Map<Double, List<Double>> calcDistances = new HashMap<Double, List<Double>>(); //-------------
	static int finalSize = 0; 					// updated in sortGeoCoordinates and used in main function //-------------
	static int newSize = 0; 					// updated in removeNoisePoints() //-------------
	//static double noiseSubDistanceWeight = 0; 	// updated in weightPercentage() //-------------
	static double noiseSubDistancePercScore = 0; 	// updated in z_scoreCalculation() //-------------	
	JSONObject algo = new JSONObject();        // formated JSON object that is returned
	
	// Filter out  0.0 values
	public static double[][] filterArray(int size, double [][] tempArray){
		//double[][] tempArray = new double[size][2];
		int count = 0;
		
		// Count the occurrences of 0.0, store lat/ long values in a list
		ArrayList<Double> list = new ArrayList<Double>();
		for(int i = 0; i < size; i++) {
		    for(int j = 0; j < 2; j++) {
		    	if (tempArray[i][j] == 0.0)
		    		count += 1;
		        list.add(tempArray[i][j]);
		    }    
		}
		if(count == 0) {
			count = 0;
			System.out.println("Count of 0.0 values: " + count);
		}
		
		// Remove all the occurrences of 0.0 
		else {
			System.out.println("Count of 0.0 values: " + count / 2);
			for(int s = 0; s < count; s++) {
				list.remove(new Double(0.0));
			}
		}
		// Store lat/ long values in an array
		int arrSize1 = 0;
		if (list.size() != 0) {arrSize1 = list.size() / 2; } 
		System.out.println("\nList size: " + arrSize1);
		double[][] tempLatLongArray = new double[arrSize1][2];
		int k = 0;
		for(int i = 0 ; i < arrSize1; i++) {
	         for(int j = 0; j < 2; j++) {
	        	 tempLatLongArray[i][j] = list.get(k);
	                 k++;
	                 if(k > list.size()) {
	                     break;
	                 }
	         }
	     }	
		return tempLatLongArray;
	}
	// end filterArray()
	
	// Function to round decimal places 
	public static double roundDecimal (int scale, double value) {
		BigDecimal roundValue = new BigDecimal(value);
	    roundValue = roundValue.setScale(scale, BigDecimal.ROUND_HALF_UP); 
	    double rounded = roundValue.doubleValue();
	    return rounded;
	}
	
	// Calculate Distance between two given points
	// Using Haversine formula 
	public static double CrowDistance(double lat1, double long1, double lat2, double long2) {
		double R = 6371;
		double diffLat = Math.toRadians(lat2 - lat1);
	    double diffLon = Math.toRadians(long2 - long1);
	    lat1 = Math.toRadians(lat1);
	    lat2 = Math.toRadians(lat2);
	 
	    double a = Math.pow(Math.sin(diffLat / 2),2) + Math.pow(Math.sin(diffLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
	    double c = 2 * Math.asin(Math.sqrt(a));
	    double distance =  R * c;
		return distance;		
	} 
	// end CrowDistance()
	
	
	// Sorting distances 
	public static Double[][] SortGeoCoordinates(double[][] LatLongArray, int arrSize, double startLat,double startLong) {		
		System.out.println("\narrSize: "+ arrSize);
		for(int i = 0; i < arrSize; i++) {
			double dist1 = CrowDistance(startLat, startLong, LatLongArray[i][0],LatLongArray[i][1]);
			double dist = dist1;			
			// Storing into a HashMap
			List<Double> valList = new ArrayList<Double>();
			// Latitude
			valList.add(LatLongArray[i][0]);
			// Longitude
			valList.add(LatLongArray[i][1]); 		
			calcDistances.put(dist, valList);
		}		
		// Sorting by Keys
        Double[][] returnedArray = new Double[arrSize][];
        int index = 0;
        
        ArrayList<Double> sortedKeys = new ArrayList<Double>(calcDistances.keySet()); 
        Collections.sort(sortedKeys);  
	    // Display the TreeMap
        System.out.println("Key count: " + sortedKeys.size());
        
	    for (Double x : sortedKeys) { 
	        //Storing values into returnedArray
		    List<Double> row = calcDistances.get(x);
		    returnedArray[index] = row.toArray(new Double[row.size()]);
		    index++; 
	    }
	    finalSize = sortedKeys.size();
	    return returnedArray;
	} 
	// end SortGeoCoordinates()

	// Cumulative calculation of sub distances 
	public static double PiecewiseLinearDistance(Double[][] finalArray, int arrSize) {
		double totalDistance = 0;
		double subDistance = 0;
		
		for (int i = 1; i <= arrSize; i++) {
			if(i == arrSize) {break;}
			else {
				subDistance = CrowDistance(finalArray[i-1][0],finalArray[i-1][1],finalArray[i][0],finalArray[i][1]);
				//System.out.println("\nSub Distance: " + subDistance);
				totalDistance += subDistance;
			}
		}
		return totalDistance;
	}
	// end PiecewiseLinearDistance()
	
	// Calculating and adding crow distance to the matrix
	public static Double[][] smallDataSetCrow(Double[][] finalArray, int finalSize){
		Double[][] matrix = new Double[finalSize][3];
		double subDistance = 0.0;
		// First row
		matrix[0][0] = finalArray[0][0];
		matrix[0][1] = finalArray[0][1];
		matrix[0][2] = subDistance;
		
		for(int i = 1; i < finalSize; i++) {
			subDistance = CrowDistance(finalArray[i-1][0],finalArray[i-1][1],finalArray[i][0],finalArray[i][1]);
			matrix[i][0] = finalArray[i][0];
			matrix[i][1] = finalArray[i][1];
			matrix[i][2] = subDistance;
		}
		return matrix;
	} 
	// end smallDataSetCrow()	
	
	
	public static void printMatrix(Double ipMatrix[][], int Size) throws IOException {			
			System.out.println("Latitude" +"		" + "Longitude" +"				 "+"CrowDistance\n");
		    for(int i = 0; i < Size; i++) {
		    	// Printing Matrix values
				System.out.println(ipMatrix[i][0] +"		" + ipMatrix[i][1] +"				 "+ipMatrix[i][2]);
			}
		    // Calling setter methods
		    naAlgorithmCalculation.setCategory(" <= 7, no points removed");
	}
	// end printMatrix() (dataset <= 10)	
	
	// Printing smooth matrix
	public static void printMatrixSmooth(double ipMatrix[][], int Size) throws IOException {			
			System.out.println("Latitude" +"		" + "Longitude" +"				 "+"CrowDistance\n");
		    for(int i = 0; i < Size; i++) {
		    	// Printing Matrix values
				System.out.println(ipMatrix[i][0] +"		" + ipMatrix[i][1] +"				 "+ipMatrix[i][2]);
			}
	}
	// end printMatrixSmooth()

	// Printing matrix (dataset > 7)
	public static void printMatrix(double ipMatrix[][], int Size, String placeholder) throws IOException{
		System.out.println("Latitude" +"		" + "Longitude" +"				 "+"CrowDistance" +"		 		" + "        "+placeholder +"		 		 	   "
				+ "checkedNoise\n");
		for(int i = 0; i < Size; i++) {
			System.out.println(ipMatrix[i][0] +"		" + ipMatrix[i][1] +"				 "+ipMatrix[i][2] +"		 		" + ipMatrix[i][3] +" 		 	   "
					+ ipMatrix[i][4]);	
		}
		// Calling setter method
		naAlgorithmCalculation.setCategory("points > 7");
	}
	// end printMatrix() (dataset > 7)	
	
	// Printing observed data points
	//noise placeholder = distance dropped
	public static void printThresholdObservations(int finalSize, int newSize, double totalDistance, double noisePlaceholder) {
		double kmDropped = 0;
	    int pointsDropped = finalSize - newSize;
	    double noiseAdjustedDistance = totalDistance - noisePlaceholder;
	    double roundedNoisePlaceholder = roundDecimal(3, noisePlaceholder);
	    double roundedNoiseAdjustedDistance = roundDecimal(3, noiseAdjustedDistance);
	    // Calling setter methods
	    naAlgorithmCalculation.setNoOfPointsDropped(pointsDropped);
	    naAlgorithmCalculation.setDistanceLost(roundedNoisePlaceholder);
	    naAlgorithmCalculation.setNoiseAdjTotalDistance(roundedNoiseAdjustedDistance);
	    
	    System.out.println("\nPoints dropped: " + pointsDropped);
	    if (pointsDropped == 0) {
	    	kmDropped = 0;
	    }
	    else {
	    	kmDropped = totalDistance / pointsDropped;
	    }

	    System.out.println("\n(1) kilometers dropped after smoothening the Matrix: " + kmDropped);
	    System.out.println("\ntotalDist, finalSize, newSize: " + totalDistance + " " + finalSize + " "+ newSize);
	    
	    // Calling setter methods
	    //naAlgorithmCalculation.setTotalDistance(totalDistance);
	    double roundedTotalDistance = roundDecimal(3, totalDistance);
	    algorithmCalculation.setTotalDistance(roundedTotalDistance);
	    algorithmCalculation.setTotalCount(finalSize);
	    //naAlgorithmCalculation.setHaversineTotalCount(finalSize);
	    naAlgorithmCalculation.setNoiseAdjTotalCount(newSize);
	    
	    double distPercentageLost = (noisePlaceholder * 100 ) / totalDistance;
	    System.out.println("\n(2) percentage distance lost: "+ distPercentageLost);
	    System.out.println("\ndistance dropped: "+ noisePlaceholder);
	    System.out.println("\n-------------------------------------------------------------------------------------------------------");
	    System.out.println("\n-------------------------------------------------------------------------------------------------------");
	}
	// end printThresholdObservations()	
	
	// Median Calculation
	public static double findMedian(List<Double> list, int size) {
		double result = 0;
		if (size % 2 != 0) {
			result = (double)list.get(size/ 2);
		}
		else {
			double value1 = list.get((size - 1) /2 );
			double value2 = list.get(size/ 2);
			result = (double) (value1 + value2) / 2.0;
		}	
		return result;
	} 
	// end findMedian();
	
	// Quartile Calculation
	public static double findQuartile(List<Double> list, double median, int finalSize) {
		List<Double> tempList = new ArrayList<Double>();
		double value = 0;
		// Copying values lesser than median
		for (int i = 0; i < finalSize; i++) {
			if(list.get(i) < median) {
				tempList.add(list.get(i));
			}
		}
		value = findMedian(tempList, tempList.size());
		return value;
	} 
	// end findQuartile()
	
	// Percentile calculation using Interpolation of rank
	public static double findPercentile(List<Double>list, double percentile, int finalSize) {
		double percentileValue = 0;
		int length = list.size();
		double rank = percentile *(length + 1);
		int integerPart = (int) rank;
		double fractionPart = rank - integerPart;
		
		if(fractionPart == 0.0) {
			percentileValue = list.get(integerPart);
		}
		else {
			double subValue = (list.get(integerPart + 1) - list.get(integerPart)) * fractionPart;
			double value = list.get(integerPart) + subValue;
			percentileValue = value;
		}		
		return percentileValue;		
	} 
	// end findPercentile()	
	
	// Standard Deviation
	public static double calculateStandardDeviation(List<Double> list, double median){
		double std = 0;
		int size = list.size();
		//double mean = 0; //----------
		//double sum = 0;  //----------
		double numerator = 0;
		double variance = 0;
		/*    //----------
		for (int i = 0; i < size; i++) {
			sum += list.get(i);
		}	
		*/
		// summation [( subDist - median )square]
		for (int i = 0; i < size; i++) {
			numerator += Math.pow((list.get(i) - median), 2); 
		}
		variance = numerator / (double)(size - 1);
		System.out.println("\n(1)variance: " + variance);
		std = Math.sqrt(variance);
		return std;
	}
	// end calculateStandardDeviation()	

	// removeNoisePoints
	public static double[][] removeNoisePoints(double [][] ipMatrix, int finalSize){
		List<Double> tempLatList  = new ArrayList<Double>();
		List<Double> tempLongList = new ArrayList<Double>();
		double subDistance = 0;
			
		for(int i = 0; i < finalSize; i++) {
			if(ipMatrix[i][4]  == 0.0) {
				tempLatList.add(ipMatrix[i][0]);
				tempLongList.add(ipMatrix[i][1]);
			}
		}
		newSize = tempLatList.size();
		// New size of matrix
		double opMatrix [][] = new double[newSize][3];  
			
		for(int index = 0;  index < newSize; index++) {
			opMatrix[index][0] = tempLatList.get(index);
			opMatrix[index][1] = tempLongList.get(index);
		}			
		for(int j = 1; j <= newSize; j++) {
			if(j == newSize) { break;}
			else {
				subDistance = CrowDistance(opMatrix[j-1][0],opMatrix[j-1][1],opMatrix[j][0],opMatrix[j][1]);
				opMatrix[j][2] = subDistance;	
			}
		}			
		return opMatrix;
	}
	// end removeNoisePoints()	
	
	// Percentile Calculation and Z score Calculation
	public static double[][] percentileCalculation(Double[][] finalArray, int finalSize){
		
		double[][] percentileMatrix = new double[finalSize][5];
		List<Double> subDistList = new ArrayList<Double>(); 
		List<Double> listCopy = new ArrayList<Double>();			
		List<Double> minMaxMedianDiffList = new ArrayList<Double>(); // used in zscore calculations
		
		double distMedianDiff = 0;
		double subDistance = 0;
		double threshold = 0;
		double median = 0;
	
		// For the first reading
		//subDistance = CrowDistance(finalArray[0][0],finalArray[0][1],finalArray[finalSize-1][0],finalArray[finalSize-1][1]);
		subDistance = 0.0;
		subDistList.add(subDistance);
		listCopy.add(subDistance);
		percentileMatrix[0][0] = finalArray[0][0];
		percentileMatrix[0][1] = finalArray[0][1];
		percentileMatrix[0][2] = subDistance;
		
		for (int i = 1; i <= finalSize; i++) {
			if(i == finalSize) { break;}
			else {
				subDistance = CrowDistance(finalArray[i-1][0],finalArray[i-1][1],finalArray[i][0],finalArray[i][1]);
				subDistList.add(subDistance);
				listCopy.add(subDistance);
				percentileMatrix[i][0] = finalArray[i][0];
				percentileMatrix[i][1] = finalArray[i][1];
				percentileMatrix[i][2] = subDistance;
			}
		} 
		// end for loop
		
		Collections.sort(listCopy);		
		double standardDeviation = 0;
		double zscore = 0;
		
		// Calculating Median
		median = findMedian(listCopy, finalSize);
		System.out.println("\nMedian: " + median);
		
		// Calling setter method
		double roundedMedian = roundDecimal(3, median);
		algorithmCalculation.setMedian(roundedMedian);
		//naAlgorithmCalculation.setMedian(median);
		
		// Calculating 1st Quartile
		double quartile = findQuartile(listCopy, median, finalSize);
		System.out.println("\n1st Quartile: " + quartile);
		
		// Calculating Percentiles:
		// set to 40%
		double percentile = 0.40; 
		double value = findPercentile(listCopy, percentile, finalSize);
		System.out.println("\nThreshold value for percentile "+ percentile + " is: " + value);
		
		//threshold = quartile;
		threshold = value;
		System.out.println("\nPercentile threshold: " + threshold);
		double roundedThreshold = roundDecimal(3, threshold);
		// Calling setter method
		naAlgorithmCalculation.setPercentileThresholdValue(roundedThreshold);
		
		
		// Calculating numerator for zscore
		for(int j = 0; j < finalSize; j++) {
			double dist = subDistList.get(j);
			// Numerator  = Di - Median
			distMedianDiff = dist - median; 
			// Used in z score
			minMaxMedianDiffList.add(distMedianDiff);	 
		}
		
		// Calculate standard deviation 
		standardDeviation = calculateStandardDeviation(subDistList, median);		
		System.out.println("\n(2)Standard deviation: " + standardDeviation);
		double roundedStandardDeviation = roundDecimal(3, standardDeviation);
		// Calling setter method
		algorithmCalculation.setStdDev(roundedStandardDeviation);
		//naAlgorithmCalculation.setStdDev(standardDeviation);
		
		// Finding z score
		for(int k = 0; k < finalSize; k++) {
			if(standardDeviation == 0) {
				zscore = 0;
			}
			zscore = minMaxMedianDiffList.get(k) /standardDeviation;
			percentileMatrix[k][3] = zscore;
			/*
			if(zscore < threshold) {
				zscoreMatrix[k][4] = 1.0;
				noiseSubDistancescore += subDistance;
			}
			else {
				zscoreMatrix[k][4] = 0.0;
			}
			*/
			// Accessing the Subdistance for Percentile threshold check
			if(percentileMatrix[k][2] < threshold) {
				percentileMatrix[k][4] = 1.0;
				noiseSubDistancePercScore += percentileMatrix[k][2];
			}
			else {
				percentileMatrix[k][4] = 0.0;
			}
		}		
		return percentileMatrix;		
	}
	//end percentileCalculation()

	
	public static void statisticsOfMatrix (double[][] percentileMatrix, int newSize){
		List<Double> subDistList = new ArrayList<Double>(); 
		List<Double> listCopy = new ArrayList<Double>();
		
		System.out.println("\nstatisticsOfMatrix newSize: " + newSize);
		double subDistance = 0;
		double median = 0;
		
		subDistList.add(0.0);
		for (int i = 1; i <= newSize; i++) {
			if(i == newSize) { break;}
			else {
				subDistance = CrowDistance(percentileMatrix[i-1][0],percentileMatrix[i-1][1],percentileMatrix[i][0],percentileMatrix[i][1]);
				subDistList.add(subDistance);
				listCopy.add(subDistance);
			}
		} 
		// end for loop
		
		Collections.sort(listCopy);		
		double standardDeviation = 0;
		
		// Calculating Median
		median = findMedian(listCopy, newSize);
		System.out.println("\nstatisticsOfMatrix Median: " + median);
		double roundedMedian = roundDecimal (3, median);
		naAlgorithmCalculation.setNoiseAdjMedian(roundedMedian);
		
		// Calculate standard deviation 
		standardDeviation = calculateStandardDeviation(subDistList, median);		
		System.out.println("\nstatisticsOfMatrix Standard deviation: " + standardDeviation);
		double roundedStdDev = roundDecimal(3, standardDeviation);
		// Calling setter method
		naAlgorithmCalculation.setNoiseAdjStdDev(roundedStdDev);	
	} // end statisticsOfMatrix()
	
	/**	
	 *  Haversine distance calculation
	 */
	//public double[][] distanceHaversine(String rawJson)throws ParseException, IOException{	
	//public Haversine distanceHaversine(String rawJson)throws ParseException, IOException{
	@SuppressWarnings("unchecked")
	public JSONObject distanceReductionHaversine(String deviceid,String startdate, String enddate, String schemaName)throws ParseException, IOException{
		
		List<GpsData> gps = getGpsInfo_dataUsersByDate(deviceid,startdate,enddate,schemaName);
		String rawJson = (String) new Gson().toJson(gps);
		System.out.println("----------------------Json data-----from gps------------stored--------------------");
		

		// Array of latitudes and longitudes
		double userPath[][] = createLatLongArray(rawJson);
		System.out.println("\nReduction of markers.....\n");
		//*******************************************************************************
		//radius  = 0.005km, 0.010km
		double reducedPath[][] = markerUtil.removePoints(0.005, userPath); // 5m, 10m
		
		//*******************************************************************************
		int size = userPath.length;
		System.out.println("\nLength of the userPath: " + size);
		
		int reducedSize = reducedPath.length;
		System.out.println("\nLength of reducedPath: " + reducedSize);
		
		//if(size == 0) {
		if(reducedSize == 0) {
			//set zero and return;
			algorithmCalculation.setTotalCount(0);
		    algorithmCalculation.setTotalDistance(0.0);
		    algorithmCalculation.setMedian(0.0);
		    algorithmCalculation.setStdDev(0.0);
		    algorithmCalculation.setPointToPoint(0.0);
		    
		    naAlgorithmCalculation.setNoiseAdjTotalDistance(0.0);
		    naAlgorithmCalculation.setNoiseAdjMedian(0.0);
		    naAlgorithmCalculation.setNoiseAdjStdDev(0.0);
		    naAlgorithmCalculation.setNoiseAdjTotalCount(0);
		    naAlgorithmCalculation.setPercentileThresholdValue(0.0);
		    naAlgorithmCalculation.setDistanceLost(0.0);
		    naAlgorithmCalculation.setNoOfPointsDropped(0);
		    naAlgorithmCalculation.setCategory("arrSize <= 1");
		    
		    // Populating the JSON Object
			algo.put("haversine", algorithmCalculation);
			algo.put("noiseAdjustedHaversine", naAlgorithmCalculation);
			
			JSONArray latLong = new JSONArray();
			latLong = null;
			algo.put("latLongArray", latLong);
			algo.put("googleMapsAPI", 0.0);
		}
		else {
			// With 0.0 values removed
			double LatLongArray[][] = filterArray(reducedSize, reducedPath); //size, userPath
			//double LatLongArray[][] = filterArray(size, userPath);
			// Size of the filtered array
			int arrSize = LatLongArray.length;
			
			// Updated after removeNoisePoints()
			double pointsDropped = 0;
			double distPercentageLost = 0;
			double kmDropped = 0;	        
			
			// The starting location coordinates
			double startLat = LatLongArray[0][0];
			double startLong = LatLongArray[0][1];
			// The ending location coordinates
			double endLat = LatLongArray[arrSize-1][0];
			double endLong = LatLongArray[arrSize-1][1];
			System.out.println("\nstartLat: " + startLat +"  "+"startLong: "+ startLong);
			System.out.println("\nendLat: " + endLat +"  "+"endLong: "+ endLong);
			
			//Point to point
			double pointToPoint = CrowDistance(startLat, startLong, endLat, endLong);
			System.out.println("\npointToPoint: " + pointToPoint);
			double roundedPointToPoint = roundDecimal(3, pointToPoint);
			// Calling setter method 
			algorithmCalculation.setPointToPoint(roundedPointToPoint);
			
			Double finalArray[][] = SortGeoCoordinates(LatLongArray, arrSize, startLat, startLong);
			
			if (arrSize <= 1) {
				//set zero and return;
					algorithmCalculation.setTotalCount(0);
				    algorithmCalculation.setTotalDistance(0.0);
				    algorithmCalculation.setMedian(0.0);
				    algorithmCalculation.setStdDev(0.0);
				    algorithmCalculation.setPointToPoint(0.0);
				    
				    naAlgorithmCalculation.setNoiseAdjTotalDistance(0.0);
				    naAlgorithmCalculation.setNoiseAdjMedian(0.0);
				    naAlgorithmCalculation.setNoiseAdjStdDev(0.0);
				    naAlgorithmCalculation.setNoiseAdjTotalCount(0);
				    naAlgorithmCalculation.setPercentileThresholdValue(0.0);
				    naAlgorithmCalculation.setDistanceLost(0.0);
				    naAlgorithmCalculation.setNoOfPointsDropped(0);
				    naAlgorithmCalculation.setCategory("arrSize <= 1");
				    
				    // Populating the JSON Object
					algo.put("haversine", algorithmCalculation);
					algo.put("noiseAdjustedHaversine", naAlgorithmCalculation);
					
					JSONArray latLong = new JSONArray();
					latLong = null;
					algo.put("latLongArray", latLong);
					algo.put("googleMapsAPI", 0.0);
			}
			else if (arrSize <= 7 && arrSize > 1){ //greater than 7 points send to main calculation   (arrSize <= 50 ) // before setting
					System.out.println("\n In <=7 && > 1");
					double totalDistance = PiecewiseLinearDistance(finalArray, finalSize);
				    System.out.println("\nTotal Distance: " + totalDistance);		
				    System.out.println("\n-------------------------------------------------------------------------------------------------------");
				    System.out.println("\n-------------------------------------------------------------------------------------------------------");	    
				    Double smallSet[][] = smallDataSetCrow(finalArray, finalSize);
					List <Double> listLat = new ArrayList <Double>();  // For google map api 
					List <Double> listLong = new ArrayList <Double>(); // For google map api
				    printMatrix(smallSet, finalSize);
			    
				    algorithmCalculation.setTotalCount(finalSize);
					double roundedTotalDistance = roundDecimal(3, totalDistance);
					System.out.println("\npointToPoint: " + totalDistance + "\nroundedPointToPoint: " + roundedTotalDistance);
					algorithmCalculation.setTotalDistance(roundedTotalDistance);
				    //algorithmCalculation.setTotalDistance(totalDistance);
					algorithmCalculation.setMedian(0.0);
				    algorithmCalculation.setStdDev(0.0);
				    algorithmCalculation.setPointToPoint(0.0);
				    
				    naAlgorithmCalculation.setNoiseAdjTotalDistance(0.0);
				    naAlgorithmCalculation.setNoiseAdjMedian(0.0);
				    naAlgorithmCalculation.setNoiseAdjStdDev(0.0);
				    naAlgorithmCalculation.setNoiseAdjTotalCount(0);
				    naAlgorithmCalculation.setPercentileThresholdValue(0.0);
				    naAlgorithmCalculation.setDistanceLost(0.0);
				    naAlgorithmCalculation.setNoOfPointsDropped(0);
				    naAlgorithmCalculation.setCategory("arrSize <= 7 and arrSize > 1");
				    
				    // Populating the JSON Object
					algo.put("haversine", algorithmCalculation);
					algo.put("noiseAdjustedHaversine", naAlgorithmCalculation);
					Set<Double> keySet = map.keySet(); // keySet returns keys from the hashMap
					
					// Sending lat, long, address, dateTime, checkedNoise
					JSONArray latLong = new JSONArray();
					for(int l = 0; l < finalSize; l++) {
						JSONObject row = new JSONObject();
						row.put("latitude", finalArray[l][0]);
						row.put("longitude", finalArray[l][1]);
						//row.put("checkedNoise", finalArray[l][4]);
						row.put("checkedNoise", 0);
						
						double key = finalArray[l][0]; // latitude key
						AddressDate object = (AddressDate) map.get(key);
						row.put("address", object.getAddress());
						row.put("dateTime", object.getDateTime());				
						latLong.add(l,row);
						
						listLat.add(finalArray[l][0]);
						listLong.add(finalArray[l][1]);
					}
					algo.put("latLongArray", latLong);
					/*
					// calling Google Map API  
					System.out.println("\nlistLat: " + listLat + "\nlistLong" + listLong);
					//double googleDistance = algoUtil.googleMapsApi(listLat, listLong, "DRIVING");	
					algoUtil.googleMapsApi(listLat, listLong, "DRIVING");
					System.out.println("\nGoogleApi results: " + drivingGoogleApi.getDistance() + "\nLat: " + drivingGoogleApi.getLatitude() + "\nLong: " + drivingGoogleApi.getLongitude());
					double googleDistance = drivingGoogleApi.getDistance();
					double roundedGoogleDistance = roundDecimal(3, googleDistance);
					//System.out.println("\ngoogleDistance: " + googleDistance + "\nroundedGoogleDistance: " + roundedGoogleDistance);
					
					//haver.put("googleMapsAPI", googleDistance);
					algo.put("googleMapsAPI", roundedGoogleDistance);
					// Sending Google lat long array 
					JSONArray googleLatLong = new JSONArray();
					for(int index = 0; index < drivingGoogleApi.getLatitude().size(); index++) {
						JSONObject newRow = new JSONObject();
						newRow.put("latitude",drivingGoogleApi.getLatitude().get(index));
						newRow.put("longitude",drivingGoogleApi.getLongitude().get(index));
						
						googleLatLong.add(index, newRow);
					}
					//Adding the destination Lat Long  // will make the list of lat longs + 1
					JSONObject lastRow = new JSONObject();
					lastRow.put("latitude", endLat); 
					lastRow.put("longitude", endLong); 
					googleLatLong.add(lastRow);
					
					algo.put("googleMap_LatLong",googleLatLong);
					*/					
			}
			else {
			    double totalDistance = PiecewiseLinearDistance(finalArray, finalSize);
			    System.out.println("\nTotal Distance: " + totalDistance);		
			    System.out.println("\n-------------------------------------------------------------------------------------------------------");
			    System.out.println("\n-------------------------------------------------------------------------------------------------------");	    
			    // Percentile calculation and Z score matrix
			    double percentileMatrix[][] = percentileCalculation(finalArray, finalSize);
				List <Double> listLat = new ArrayList <Double>();  // For google map api 
				List <Double> listLong = new ArrayList <Double>(); // For google map api 		    
			    String placeholder2 = "Zscore";
			    printMatrix(percentileMatrix, finalSize, placeholder2);	    
		
				double smoothPercentileMatrix[][] = removeNoisePoints(percentileMatrix, finalSize);
				System.out.println("\nSize: " + newSize);
				System.out.println("Smoothened matrix:");
				printMatrixSmooth(smoothPercentileMatrix, newSize);
			
			    printThresholdObservations(finalSize, newSize, totalDistance, noiseSubDistancePercScore);
			    
			    // to get median and standard deviation of noise adjusted matrix (with noise points removed) 
			    statisticsOfMatrix(smoothPercentileMatrix, newSize);
	//******************************************************************************************************************		    
			    // Finding the shortest path 
			    //double[][] shortPath = haversineUtil.shortestPath(smoothPercentileMatrix, newSize);
			    //System.out.println("\nShortest Path: " + Arrays.deepToString(shortPath));
			    // TODO add to json object, get address and dateTime 
	//******************************************************************************************************************		    
			    algo.put("haversine", algorithmCalculation);
			    algo.put("noiseAdjustedHaversine", naAlgorithmCalculation);
				
				//Accessing HashMap values
				Set<Double> keySet = map.keySet(); // keySet returns keys from the hashMap
				// Sending lat, long, address, dateTime, checkedNoise
				JSONArray latLong = new JSONArray();
				for(int l = 0; l < finalSize; l++) {
					JSONObject row = new JSONObject();
					row.put("latitude", percentileMatrix[l][0]);
					row.put("longitude", percentileMatrix[l][1]);
					row.put("checkedNoise", percentileMatrix[l][4]);
					
					double key = percentileMatrix[l][0]; // latitude key
					AddressDate object = (AddressDate) map.get(key);
					row.put("address", object.getAddress());
					row.put("dateTime", object.getDateTime());
	
					latLong.add(l,row);
					
					listLat.add(percentileMatrix[l][0]);
					listLong.add(percentileMatrix[l][1]);				
				}
				algo.put("latLongArray", latLong);
    			/*
				// calling Google Map API  
				System.out.println("\nlistLat: " + listLat + "\nlistLong" + listLong);
				//double googleDistance = algoUtil.googleMapsApi(listLat, listLong, "DRIVING");
				algoUtil.googleMapsApi(listLat, listLong, "DRIVING");//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				System.out.println("\nGoogleApi results: " + drivingGoogleApi.getDistance() + "\nLat: " + drivingGoogleApi.getLatitude() + "\nLong: " + drivingGoogleApi.getLongitude());
				double googleDistance = drivingGoogleApi.getDistance();				
				double roundedGoogleDistance = roundDecimal(3, googleDistance);
				//System.out.println("\ngoogleDistance: " + googleDistance + "\nroundedGoogleDistance: " + roundedGoogleDistance);
				//haver.put("googleMapsAPI", googleDistance);
				algo.put("googleMap_Distance", roundedGoogleDistance);//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>	
				
				// Sending Google lat long array 
				JSONArray googleLatLong = new JSONArray();
				for(int index = 0; index < drivingGoogleApi.getLatitude().size(); index++) {
					JSONObject newRow = new JSONObject();
					newRow.put("latitude",drivingGoogleApi.getLatitude().get(index));
					newRow.put("longitude",drivingGoogleApi.getLongitude().get(index));
					
					googleLatLong.add(index, newRow);
				}
				//Adding the destination Lat Long  // will make the list of lat longs + 1
				JSONObject lastRow = new JSONObject();
				lastRow.put("latitude", endLat); 
				lastRow.put("longitude", endLong); 
				googleLatLong.add(lastRow);
				
				algo.put("googleMap_LatLong",googleLatLong);
				*/
			}
		}
		newSize = 0;
		finalSize = 0;
		calcDistances.clear();
		noiseSubDistancePercScore = 0;
		return algo;
	}
}