package com.fidz.services.location.haversine;

import java.util.ArrayList;
import java.util.List;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service 
public class MarkerReductionUtil {
	//@Autowired 
	//private HaversineService_Copy haversineService;

	public double [][] removePoints(double radius, double[][]userPath){	
		try {
			if (userPath.length == 0) {
				return userPath;
			}
			List<Double> listLat = new ArrayList<Double>();
			List<Double> listLong = new ArrayList<Double>();
			int size = userPath.length;
			System.out.println("\nIn MarkerReductionUtil.....");
			System.out.println("\nsize: " + size);
	
			double subDistance = 0;
			
			for(int i = 1; i < size; i++) {
				subDistance = HaversineService.CrowDistance(userPath[i-1][0], userPath[i-1][1], userPath[i][0], userPath[i][1]);
				if(subDistance > radius) {
					listLat.add(userPath[i][0]);
					listLong.add(userPath[i][1]);
				}
			}
			int listSize = listLat.size(); // for the first point
			int reducedSize = listSize + 1;
			System.out.println("\nreduced size: " + reducedSize);
			
			double reducedPath[][] = new double [reducedSize][2];
			reducedPath[0][0] = userPath[0][0];
			reducedPath[0][1] = userPath[0][1];		
			
			int index = 1;
			for(int k = 0; k < listSize; k++) {
				reducedPath[index][0] = listLat.get(k);
				reducedPath[index][1] = listLong.get(k);
				index++;
			}
			
			System.out.println("\nReduced Array: " + "\nreducedPath.length: " + reducedPath.length + "\nreducedSize: " + reducedSize);
			for(int j = 0; j < reducedPath.length; j++) {
				System.out.println("Latitude: " + reducedPath[j][0] + "    " +"Longitude: "+ reducedPath[j][1]);
			}
			return reducedPath;
		}
		catch(Exception e) {
			System.out.println("Exception thrown: " + e + "\nreturning userPath");
			return userPath;
		}
	}
}
