package com.fidz.services.location.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.repository.GenericBaseINterface;
import com.fidz.base.validator.Constant;
import com.fidz.services.location.exception.GpsCaptureConfigNotFoundException;
import com.fidz.services.location.model.GPSCaptureConfig;
import com.fidz.services.location.repository.GpsCaptureConfigRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class GpsCaptureConfigService {
	private GenericAppINterface<GPSCaptureConfig> genericINterface;
	@Autowired
	public GpsCaptureConfigService(GenericAppINterface<GPSCaptureConfig> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private GpsCaptureConfigRepository gpsCaptureConfigRepository;
	
	public List<GPSCaptureConfig> getAllGpsData(String schemaName) {
		Constant.LOGGER.info("getAllGpsData GpsCaptureConfigService.java" );
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        return this.genericINterface.findAll(GPSCaptureConfig.class);
    }
	
	
	public GPSCaptureConfig saveGpsConfigData(GPSCaptureConfig gpsCaptureConfig) {
		String schemaName = gpsCaptureConfig.getSchemaName();

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.gpsCaptureConfigRepository.save(gpsCaptureConfig);
	}
	
	public void saveGpsConfigDatas(GPSCaptureConfig gpsCaptureConfig) {
		String schemaName = gpsCaptureConfig.getSchemaName();

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// return gpsCaptureConfigRepository.save(gpsCaptureConfig);
		this.genericINterface.saveName(gpsCaptureConfig);
	}

	 public GPSCaptureConfig getGpsConfigDataById(String id, String schemaName) {
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 return this.genericINterface.findById(id, GPSCaptureConfig.class);
			
	    }
	 
	 
	public GPSCaptureConfig updateGPSCaptureConfig(String id,GPSCaptureConfig gpsCaptureConfig) {
		String schemaName = gpsCaptureConfig.getSchemaName();

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		gpsCaptureConfig.setId(id);
		return this.gpsCaptureConfigRepository.save(gpsCaptureConfig);
		
	}
	
	public List<GPSCaptureConfig> streamAllGpsData() {
        return gpsCaptureConfigRepository.findAll();
    }
	
}
