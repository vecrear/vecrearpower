package com.fidz.services.location.service;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;


import org.springframework.stereotype.Repository;

import com.fidz.services.location.model.LatestGpsLatLong;





@Repository("LatestLatLngRepository")
public interface LatestLatLngRepository extends MongoRepository<LatestGpsLatLong, String> {


	//public List<LatestGpsLatLong> findByUserid(String userid);
}
