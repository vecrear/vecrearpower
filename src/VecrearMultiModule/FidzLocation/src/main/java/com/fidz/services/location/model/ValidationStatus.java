package com.fidz.services.location.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class ValidationStatus {

	
	boolean validationStatus;

	@Override
	public String toString() {
		return "ValidationStatus [validationStatus=" + validationStatus + "]";
	}

	public ValidationStatus(boolean validationStatus) {
		super();
		this.validationStatus = validationStatus;
	}

	public ValidationStatus() {
		
	}

}
