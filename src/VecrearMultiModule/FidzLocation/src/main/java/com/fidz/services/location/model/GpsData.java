package com.fidz.services.location.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "gpsdata")
@JsonIgnoreProperties(value = { "target" })
public class GpsData {
	@Id
	private String id;
	protected Object gpsData;
	
	protected String deviceID;
	protected Date dateTime;
	protected String schemaName;
	protected double distance;
	protected TravelType travelType;
	protected String address;
	
	public static GpsData gData = new GpsData();
	
	public GpsData() {
		super();
	}

	


	public static GpsData getgData() {
		return gData;
	}

	public static void setgData(GpsData gData) {
		GpsData.gData = gData;
	}

	@Override
	public String toString() {
		return "GpsData [id=" + id + ", gpsData=" + gpsData + ", deviceID=" + deviceID + ", dateTime=" + dateTime
				+ ", schemaName=" + schemaName + ", distance=" + distance + ", travelType=" + travelType + ", address="
				+ address + "]";
	}




	public GpsData(String id, Object gpsData, String deviceID, Date dateTime, String schemaName, double distance,
			TravelType travelType, String address) {
		super();
		this.id = id;
		this.gpsData = gpsData;
		this.deviceID = deviceID;
		this.dateTime = dateTime;
		this.schemaName = schemaName;
		this.distance = distance;
		this.travelType = travelType;
		this.address = address;
	}




	public GpsData( Object gpsData, String deviceID, Date dateTime, String schemaName, double distance,
			TravelType travelType, String address) {
		super();
	
		this.gpsData = gpsData;
		this.deviceID = deviceID;
		this.dateTime = dateTime;
		this.schemaName = schemaName;
		this.distance = distance;
		this.travelType = travelType;
		this.address = address;
	}
	
	
	
	




	
}
