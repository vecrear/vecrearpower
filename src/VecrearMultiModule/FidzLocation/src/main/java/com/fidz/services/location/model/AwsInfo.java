package com.fidz.services.location.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



import lombok.Data;
@Data
@Document(collection = "awsInfo")
@JsonIgnoreProperties(value = { "target" })
public class AwsInfo {
	
	protected String accessKey;
	protected String secretKey ;
	protected String bucketName ;
	protected String region ;
	protected String schemaName ;
	protected String geoAddressKey ;
	protected String geoDistKey;
	protected String smsUrl ;
	protected String geoKey;
	
	
	



	@Override
	public String toString() {
		return "AwsInfo [accessKey=" + accessKey + ", secretKey=" + secretKey + ", bucketName=" + bucketName
				+ ", region=" + region + ", schemaName=" + schemaName + ", geoAddressKey=" + geoAddressKey
				+ ", geoDistKey=" + geoDistKey + ", smsUrl=" + smsUrl + ", geoKey=" + geoKey + "]";
	}

	
	
	
	
	

}
