package com.fidz.services.location.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "gpsdata")
@JsonIgnoreProperties(value = { "target" })
public class GpsDataDistnaceCal {
	@Id
	private String id;
	
	protected GpsDataConvertion gpsData;
	protected String deviceID;
	protected Date dateTime;
	protected String schemaName;
	protected double distance;
	protected TravelType travelType;
	protected String address;
	
	public static GpsDataDistnaceCal gData1 = new GpsDataDistnaceCal();
	
	public GpsDataDistnaceCal() {
		super();
	}

	public static GpsDataDistnaceCal getgData() {
		return gData1;
	}

	public static void setgData(GpsDataDistnaceCal gData1) {
		GpsDataDistnaceCal.gData1 = gData1;
	}



	
	
	
	




	
}
