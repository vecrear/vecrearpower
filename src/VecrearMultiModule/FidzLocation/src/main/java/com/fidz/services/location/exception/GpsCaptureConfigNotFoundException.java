package com.fidz.services.location.exception;

public class GpsCaptureConfigNotFoundException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 985372509531244252L;

	public GpsCaptureConfigNotFoundException(String gpsId) {
		super("GPS Config not found with id " + gpsId);
		// TODO Auto-generated constructor stub
	}

}
