package com.fidz.services.location.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.services.location.model.DateTime;
import com.fidz.services.location.model.ValidationStatus;
import com.fidz.services.location.util.DateTimeUtil;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/datetime")
public class DateTimeController {
@Autowired
DateTimeUtil dateTimeUtil;

	@PostMapping("/getcurrentdatetime")
    public DateTime getCurrentDateTime() {
		return dateTimeUtil.getCurrentDateTime();
	}
	
	@PostMapping("/validatedatetime/{datetime}")
	public ValidationStatus getValidateDateTime(@PathVariable(value = "datetime") final long datetime) {
		
		return dateTimeUtil.validateDateTime(datetime);
	}
}
