package com.fidz.services.location.util;


import java.time.Instant;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Service;

import com.fidz.services.location.model.DateTime;
import com.fidz.services.location.model.ValidationStatus;


@Service
public class DateTimeUtil {

	
	public DateTime getCurrentDateTime() {
		ZoneId zoneId = ZoneId.of("UTC");
		ZonedDateTime zonedDateTime = ZonedDateTime.now(zoneId);
		return new DateTime(zonedDateTime.toInstant().toEpochMilli());
	}
	
	public ValidationStatus validateDateTime(long datetime) {
		ZoneId zoneId = ZoneId.of("UTC");
		ZonedDateTime passedDatetime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(datetime), zoneId);
		ZonedDateTime currentDateTime = ZonedDateTime.now(zoneId);
		long minutes = ChronoUnit.MINUTES.between(currentDateTime, passedDatetime);
		System.out.println("diff:" + minutes);
		boolean valid = !(minutes < -10 || minutes > 10);
		return new ValidationStatus(valid);
	}
	
	
	
	public static void main(String args[]) {
		//System.out.println(ZoneId.getAvailableZoneIds());
		ZoneId zoneId = ZoneId.of("UTC");
		ZonedDateTime zonedDateTime = ZonedDateTime.now(zoneId);
		System.out.println(zonedDateTime);
		long datatimePassed = zonedDateTime.toInstant().toEpochMilli(); 
		System.out.println(datatimePassed);	
		ZonedDateTime passedDatetime = ZonedDateTime.ofInstant(Instant.ofEpochMilli(datatimePassed), zoneId);
		System.out.println(passedDatetime);
		ZonedDateTime currentDateTime = ZonedDateTime.now(zoneId);
		System.out.println(currentDateTime);

		long minutes = ChronoUnit.MINUTES.between(currentDateTime, passedDatetime);
		System.out.println("diff:" + minutes);
	}
	
}
