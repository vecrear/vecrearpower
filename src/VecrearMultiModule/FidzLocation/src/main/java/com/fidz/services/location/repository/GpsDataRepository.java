package com.fidz.services.location.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.services.location.model.GpsData;

@Repository
public interface GpsDataRepository extends MongoRepository<GpsData, String>{
	public List<GpsData> findByDeviceID(String deviceId);
}
