package com.fidz.services.location;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;


/*import com.fidz.services.location.client.LocationClient;
import com.fidz.services.location.model.Location;*/


@ComponentScan(basePackages = {"com.fidz.services.location", "com.fidz.base","com.fidz.entr.app"})
@SpringBootApplication
@EnableReactiveMongoRepositories
@EnableConfigurationProperties
public class FidzLocationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FidzLocationServiceApplication.class, args);
		
 
		//RgcService rgcService = new RgcService(); ;
	//	 rgcService.reverseGeoCoding(14.519780, 76.750488);
/*    	Location location = new Location("KA-01-AM-1902","GPS",12.9060673,
    			77.593689,829.8242640751287,0,0);
    	
    	//location.set
    	LocationClient lc = new LocationClient();
    	
    	lc.saveLocation(location).subscribe(
    			value -> System.out.println("FidzLocationServiceApplication.main : " + value));*/
	}
}


/*	@Bean
	public CommandLineRunner locations(LocationRepository locationrepository) {
		return args->{
			locationrepository.deleteAll().subscribe(null,null, ()->{
				
				Stream.of(new Location(UUID.randomUUID().toString(),123.456,345.5555),
						new Location(UUID.randomUUID().toString(),123.456,345.5555),
						new Location(UUID.randomUUID().toString(),123.456,345.5555),
						new Location(UUID.randomUUID().toString(),123.456,345.5555)
						
						).forEach(location -> {
							locationrepository.save(location)
							//.subscribe(loc ->System.out.println(loc));
							.subscribe(System.out::println);
						});
			});
		};
	}*/

