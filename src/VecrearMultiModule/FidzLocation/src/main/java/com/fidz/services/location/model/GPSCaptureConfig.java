package com.fidz.services.location.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "gpscaptureconfig")
@JsonIgnoreProperties(value = { "target" })
public class GPSCaptureConfig extends Base {
	
	@Id
	private String id;
	
	//private  
	/*
	 * isGpsEnabled - Whether location needs to be retrieved from GPS provider
	 */
	private boolean gpsEnabled = true;
	/*
	 * isNetworkEnabled - Whether location needs to be retrieved from Network provider
	 */
    private boolean networkEnabled = true;
	/*
	 * isFusedEnabled - Whether location needs to be retrieved from Fused provider
	 */
    private boolean fusedEnabled = true;
    
    /*
     * gpsOrder - In which order the location retrieval through GPS would happen. Value should be between 1 to 3.
     */
    private int gpsOrder = 1;
    /*
     * networkOrder - In which order the location retrieval through Network would happen. Value should be between 1 to 3.
     */
    private int networkOrder = 2;
    /*
     * fusedOrder - In which order the location retrieval through Fused would happen. Value should be between 1 to 3.
     */
    private int fusedOrder = 3;
    
    /*
     * interval - The interval in which the location has to be retrieved
     */
    private Interval interval;
    
    //Retry fields
    /*
     * enableRetry - Is retry of location is enabled if the location capture failed
     */
    private boolean enableRetry = true;
    /*
     * retryCount - how many times retry should be done if the location capture failed
     */    
    private int retryCount = 2;
    /*
     * retryInterval - the interval after which the retry has to be done
     */       
    private Interval retryInterval;
	public GPSCaptureConfig(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, boolean gpsEnabled, boolean networkEnabled, boolean fusedEnabled,
			int gpsOrder, int networkOrder, int fusedOrder, Interval interval, boolean enableRetry, int retryCount,
			Interval retryInterval) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.gpsEnabled = gpsEnabled;
		this.networkEnabled = networkEnabled;
		this.fusedEnabled = fusedEnabled;
		this.gpsOrder = gpsOrder;
		this.networkOrder = networkOrder;
		this.fusedOrder = fusedOrder;
		this.interval = interval;
		this.enableRetry = enableRetry;
		this.retryCount = retryCount;
		this.retryInterval = retryInterval;
	}
	@Override
	public String toString() {
		return "GPSCaptureConfig [id=" + id + ", gpsEnabled=" + gpsEnabled + ", networkEnabled=" + networkEnabled
				+ ", fusedEnabled=" + fusedEnabled + ", gpsOrder=" + gpsOrder + ", networkOrder=" + networkOrder
				+ ", fusedOrder=" + fusedOrder + ", interval=" + interval + ", enableRetry=" + enableRetry
				+ ", retryCount=" + retryCount + ", retryInterval=" + retryInterval + ", createdTimestamp="
				+ createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp
				+ ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser="
				+ deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}
    
	
	
    
    

    
    
	

}
