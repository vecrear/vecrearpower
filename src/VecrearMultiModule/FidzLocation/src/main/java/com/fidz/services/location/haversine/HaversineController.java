package com.fidz.services.location.haversine;

import java.io.IOException;

//import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.services.location.haversine.HaversineService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/haversine")
public class HaversineController {
	
	@Autowired
	private HaversineService haversineService;
	@Autowired 
	private HaversineReductionService haversineReductionService;
	
	//public String IP = "http://13.234.130.40:8088";
	//http://13.234.130.40:8088/gpsdatas/deviceid/8088597794/sdate=2020-02-19|edate=2020-02-20
	//IP+'/gpsdatas/deviceid/'+username+'/sdate='+sdate+'|edate='+edate

	@RequestMapping(method = RequestMethod.POST, value = {"/", "/distance"})
	public String getHomePage() {
		return "			                    Enter valid URL";
	}
	// Return JSONObject
	// With google api + no marker reduction
	@SuppressWarnings("unchecked")
	@PostMapping("/deviceid/{deviceid}/sdate={sdate}|edate={edate}")
	public JSONObject distanceHaversine(@PathVariable (value = "deviceid") String deviceid, @PathVariable(value="sdate") String startdate, @PathVariable(value="edate") String enddate, @RequestHeader(value="schemaName") String schemaName) throws ParseException, IOException{

			//System.out.println("-------------------------enter---------------------");
			if(deviceid == "Select Users") {
				JSONObject result = new JSONObject();
				result.put("invalidParameter", deviceid);
				result.put("deviceidEntered", "Select Users");
				return result;
			}
			return haversineService.distanceHaversine(deviceid,startdate,enddate,schemaName);
	}
	
	// Return JSONObject
	// marker reduce added, google api removed
	@SuppressWarnings("unchecked")
	@PostMapping("/reduce/deviceid/{deviceid}/sdate={sdate}|edate={edate}")
	public JSONObject distanceReductionHaversine(@PathVariable (value = "deviceid") String deviceid, @PathVariable(value="sdate") String startdate, @PathVariable(value="edate") String enddate, @RequestHeader(value="schemaName") String schemaName) throws ParseException, IOException{

			//System.out.println("-------------------------enter---------------------");
			if(deviceid == "Select Users") {
				JSONObject result = new JSONObject();
				result.put("invalidParameter", deviceid);
				result.put("deviceidEntered", "Select Users");
				return result;
			}
			return haversineReductionService.distanceReductionHaversine(deviceid,startdate,enddate,schemaName);
	}
	
}
