package com.fidz.services.location.haversine;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/rhumb")
public class RhumbController {
	
	@Autowired
	private RhumbService rhumbService;
	
	// Return JSONObject
	@SuppressWarnings("unchecked")
	@PostMapping("/deviceid/{deviceid}/sdate={sdate}|edate={edate}")
	//@RequestMapping(method = RequestMethod.POST, value = "/rhumb/gpsdatas/deviceid/{username}/sdate={sdate}|edate={edate}")
	public JSONObject distanceRhumb(@PathVariable (value = "deviceid") String deviceid, @PathVariable(value="sdate") String startdate, @PathVariable(value="edate") String enddate, @RequestHeader(value="schemaName") String schemaName) throws ParseException, IOException{

			//System.out.println("-------------------------enter---------------------");
			if(deviceid == "Select Users") {
				JSONObject result = new JSONObject();
				result.put("invalidParameter", deviceid);
				result.put("deviceidEntered", "Select Users");
				return result;
			}
			return rhumbService.distanceRhumb(deviceid,startdate,enddate,schemaName);
	}

}
