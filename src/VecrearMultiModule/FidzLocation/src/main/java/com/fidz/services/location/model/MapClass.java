package com.fidz.services.location.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class MapClass {
	protected String dateTime;	// "dateTime": "2021-01-04T04:13:39.331Z",
	protected double altitude;	//"altitude": 798.3514404296875,
	protected String lastName;	// "lastName": "s",
	protected String address;	//"address": "Kadirenalli Cross, Outer Ring Rd, Kadarenahalli Park, Kadarenahalli, Banashankari, Bengaluru, Karnataka 560070, India",
	protected String distance;	//"distance": "0.0",
	protected double bearing;	//"bearing": 167.1,
	protected double latitude;	//"latitude": 12.91765556,
	protected float accuracy;	//"accuracy": 6.8919916,
	protected String timeZone ;	//"timeZone": "4 Jan 2021 04:13:39 GMT",
	protected String userName;	//"userName": "8088597794",
	protected String userId;	//"userId": "5fa12acb0274390001d2c09d",
	protected String deviceId;	 //"deviceId": "8088597794",
	protected float speed;	// "speed": 6.5,
	protected String token;	//"token": "abc",
	protected String firstName; 	//"firstName": "Deepthi ",
	protected String provider;	 //"provider": "gps",
	protected String middleName;	 //"middleName": "s",
	protected String notificationId;	//"notificationId": "chD8seUKj9Y:APA91bHrmHNu91fLHf4sNNPUAlIHveH9Jp3g22KSZcHjKebnKpdjHozbNyUQoAM-sr3rVDx4KVL6Qc0q56BliBwLMOQFdxEOw030CrhPfFhcG39XKM5NKO0lzeZkAUMfwjBzjt8BXc3n",
	protected String gpsFor;//"gpsFor": "punchIn",
	protected String activityStatus;//"activityStatus": "COMPLETED",
	protected double longitude;		//"longitude": 77.56454765
}
