package com.fidz.services.location.haversine;
import lombok.Data;

@Data
public class NoiseAdjHaversine {
	
	private double noiseAdjTotalDistance;
	private double noiseAdjMedian; 
	private double noiseAdjStdDev; 
 	private int noiseAdjHaversineTotalCount;
	private double percentileThresholdValue;
	private double distanceLost;
	private int noOfPointsDropped;
	private String category; // "Sparse" "Dense"
	
	public static NoiseAdjHaversine naHaversine = new NoiseAdjHaversine();

	// Constructors
	public NoiseAdjHaversine() {
		super();
	}
	
	public NoiseAdjHaversine(double noiseAdjTotalDistance, double noiseAdjMedian,double noiseAdjStdDev, int noiseAdjHaversineTotalCount,
			double percentileThresholdValue, double distanceLost, int noOfPointsDropped, String category) {
		
		super();		
		this.noiseAdjTotalDistance = noiseAdjTotalDistance;
		this.noiseAdjMedian = noiseAdjMedian;
		this.noiseAdjStdDev = noiseAdjStdDev;		
		this.noiseAdjHaversineTotalCount = noiseAdjHaversineTotalCount;
		this.percentileThresholdValue = percentileThresholdValue;
		this.distanceLost = distanceLost;
		this.noOfPointsDropped = noOfPointsDropped;
		this.category = category;
	}
	
	public static NoiseAdjHaversine getNaHaversine() {
		return naHaversine;
	}

	public static void setNaHaversine(NoiseAdjHaversine naHaversine) {
		NoiseAdjHaversine.naHaversine = naHaversine;
	}
	
	@Override
	public String toString() {
		return "NoiseAdjHaversine [noiseAdjTotalDistance=" + noiseAdjTotalDistance + ", noiseAdjMedian="
				+ noiseAdjMedian + ", noiseAdjStdDev=" + noiseAdjStdDev + ", noiseAdjHaversineTotalCount="
				+ noiseAdjHaversineTotalCount + ", percentileThresholdValue=" + percentileThresholdValue
				+ ", distanceLost=" + distanceLost + ", noOfPointsDropped=" + noOfPointsDropped + ", category="
				+ category + "]";
	}

}
