package com.fidz.services.location.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.apache.logging.log4j.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.services.location.model.LatestGpsLatLong;


@Service("LatestLatLongService")
public class LatestLatLongService {
	


private GenericAppINterface<LatestGpsLatLong> genericINterface;
@Autowired
public LatestLatLongService(GenericAppINterface<LatestGpsLatLong> genericINterface){
	this.genericINterface=genericINterface;
}
      


public LatestLatLongService() {
	// TODO Auto-generated constructor stub
}

@Autowired
LatestLatLngRepository latestLatLngRepository;


@Autowired
GpsDataService gpsDataService;


@Autowired
public LatestLatLongService  latestLatLongService;

Thread oneMain = null;
Thread secondMain = null;


Object objectResponse = null;

private static final String urlPrefix ="https://maps.googleapis.com/maps/api/geocode/json?";

public LatestGpsLatLong createLatestLatLong(LatestGpsLatLong latestGpsLatLong) {
	String schemaName = latestGpsLatLong.getSchemaName();
	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	Constant.LOGGER.info("inside createLatestLatLong LatestLatLongService.java");
	LatestGpsLatLong latLong=null;
	try {
		latLong= this.latestLatLngRepository.save(latestGpsLatLong);
		
	}catch(Exception ex) {
		Constant.LOGGER.info("error inside createLatestLatLong LatestLatLongService"+ex.getMessage());
	}
	
	return latLong;
	
}


public LatestGpsLatLong getLatestLatLongByuserId(String userid , String schemaName) {
	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	LatestGpsLatLong latLong=null;
	try {
	latLong= this.genericINterface.findById(userid, LatestGpsLatLong.class);
		
	//	latLong = latestLatLngRepository.findByUserId(userid);
		
	}catch(Exception ex) {
		Constant.LOGGER.info("error inside createLatestLatLong LatestLatLongService"+ex.getMessage());
	}
	
	return latLong;
	
}



public double getGoogleDistance(double lat1, double lng1, double lat2, double lng2,String geoDistKey) {
	//System.out.println("\nEntering google API.....\n");
			//geoDistKey = "AIzaSyAlCECFt-Y2fubjPbyabfu0wNqWbyzdNw4";
			Constant.LOGGER.info("google distnce Apis-geoDistKey--"+geoDistKey);
			final String uriprefix = "https://maps.googleapis.com/maps/api/directions/json?units=metric";
			double gDist = 0;
			String uri = null;
			//System.out.println(lat1 + " " + lng1 + " "+ lat2 + " "+ lng2);
			try {
				uri = uriprefix + "&origin=" + lat1 + "," + lng1 + "&destination=" + lat2 + "," + lng2 + "&key="+geoDistKey+"";
				URL uriAddress = new URL(uri);
				Constant.LOGGER.info("google distnce Apis---"+uri);
				URLConnection res = uriAddress.openConnection();
				BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(res.getInputStream())); 
				StringBuilder builder = new StringBuilder();
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					builder.append(line + "\n");
				}
				String builderArray = builder.toString();
				//System.out.println("\n\nbuilderArray: \n" + builderArray);
			    JSONObject directions = (JSONObject) JSONValue.parseWithException(builderArray);
			    JSONArray routes = (JSONArray) directions.get("routes");
			    JSONObject routeObj = (JSONObject) routes.get(0);
			    JSONArray legs = (JSONArray) routeObj.get("legs");
				JSONObject legsObj = (JSONObject) legs.get(0);
				JSONObject distanceObj = (JSONObject) legsObj.get("distance");
				long value = (long) distanceObj.get("value");
				//System.out.println("\nDistance value: " + value + "m");
				// Converting to km
				gDist = (double) value / 1000;			
			}
			catch(Exception e) {
				System.out.println("Exception: " + e);
			}
			return gDist;
}



	
}