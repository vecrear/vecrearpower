package com.fidz.services.location.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class DateTime {

	private long dateTime;

	public DateTime(long dateTime) {
		super();
		this.dateTime = dateTime;
	}

	@Override
	public String toString() {
		return "DateTime [dateTime=" + dateTime + "]";
	}

}
