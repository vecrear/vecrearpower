package com.fidz.services.location.haversine;

import lombok.Data;

@Data
public class Haversine{
	
	protected double totalDistance;
	protected double median;
	protected double stdDev;
	protected int haversineTotalCount;
	protected double pointToPoint;
	
	public static Haversine haversine = new Haversine();
	
	// Constructors
	public Haversine() {
		super();
	}

	public Haversine(double totalDistance, double median, double stdDev, int haversineTotalCount, double pointToPoint) {
		super();
		this.totalDistance = totalDistance;
		this.median = median;
		this.stdDev = stdDev;
		this.haversineTotalCount = haversineTotalCount;
		this.pointToPoint = pointToPoint;
	}

	public static Haversine getHaversine() {
		return haversine;
	}

	public static void setHaversine(Haversine haversine) {
		Haversine.haversine = haversine;
	}
	
	// To string method
	@Override
	public String toString() {
		return "Haversine [totalDistance=" + totalDistance + ", median=" + median + ", stdDev=" + stdDev
				+ ", haversineTotalCount=" + haversineTotalCount + ", pointToPoint=" + pointToPoint + "]";
	}
	
}
