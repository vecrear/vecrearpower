package com.fidz.services.location.haversine;
import lombok.Data;

@Data
public class DistanceAlgorithm {
	protected double totalDistance;
	protected double median;
	protected double stdDev;
	protected int totalCount;
	protected double pointToPoint;
	
	
	public static DistanceAlgorithm algorithm = new DistanceAlgorithm();
	
	public DistanceAlgorithm () {
		super();
	}

	public DistanceAlgorithm(double totalDistance, double median, double stdDev, int totalCount,
			double pointToPoint) {
		super();
		this.totalDistance = totalDistance;
		this.median = median;
		this.stdDev = stdDev;
		this.totalCount = totalCount;
		this.pointToPoint = pointToPoint;
	}
		
	public double getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	public double getMedian() {
		return median;
	}

	public void setMedian(double median) {
		this.median = median;
	}

	public double getStdDev() {
		return stdDev;
	}

	public void setStdDev(double stdDev) {
		this.stdDev = stdDev;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public double getPointToPoint() {
		return pointToPoint;
	}

	public void setPointToPoint(double pointToPoint) {
		this.pointToPoint = pointToPoint;
	}

	public static DistanceAlgorithm getAlgorithm() {
		return algorithm;
	}

	public static void setAlgorithm(DistanceAlgorithm algorithm) {
		DistanceAlgorithm.algorithm = algorithm;
	}

	@Override
	public String toString() {
		return "DistanceAlgorithm [totalDistance=" + totalDistance + ", median=" + median + ", stdDev=" + stdDev
				+ ", totalCount=" + totalCount + ", pointToPoint=" + pointToPoint
				+ "]";
	}
	
	
	
}
