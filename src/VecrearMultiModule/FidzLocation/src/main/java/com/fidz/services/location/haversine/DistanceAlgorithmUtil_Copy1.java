// Returning object with distance and list of lat longs from the google api

package com.fidz.services.location.haversine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Service;

@Service
public class DistanceAlgorithmUtil_Copy1 {
	private static final String uriprefix = "https://maps.googleapis.com/maps/api/directions/json?units=metric";
	
	//public Double googleMapsApi(List<Double> listLat, List<Double> listLong, String travelMode){
	public void googleMapsApi(List<Double> listLat, List<Double> listLong, String travelMode){	
		System.out.println("\n\nIn googleMapsAPI\n" + "\nSize: " + listLat.size());
		System.out.println("\nListLat: " + listLat + "\nListLong: " + listLong);	
		String uri = null;
		boolean gotDistance = false;
		double subDistance = 0;
		double distance = 0; 	
		int size = listLat.size();
		
		List <Double> lats = new ArrayList<Double>();
		List <Double> longs = new ArrayList<Double>(); 
		
		
		for(int i = 1; i < size; i++) {     
			uri = uriprefix + "&origin=" + listLat.get(i - 1) + "," + listLong.get(i - 1) + "&destination=" + listLat.get(i) + "," + listLong.get(i) + 
					  "&key=AIzaSyDNMKPzkkKtjipjb3YIyXEEXh-Piq7qbbU";
			//key=AIzaSyDNMKPzkkKtjipjb3YIyXEEXh-Piq7qbbU
			//TODO "if conditions" to check, lat longs are ->received correctly  -> if points are being dropped
			//System.out.println("\nThe uri used: " + uri);
			try {
				URL uriAddress = new URL(uri);
				URLConnection res = uriAddress.openConnection();
				BufferedReader bufferedReader  =  new BufferedReader(new InputStreamReader(res.getInputStream()));
			    StringBuilder builder = new StringBuilder();
			    String line;
			    while ((line = bufferedReader.readLine()) != null) {
			        builder.append(line + "\n");
			    }
			    String builderArray = builder.toString();
			    // Accessing routes -> legs -> distance -> value (given in meters)
			    // Storing the route: routes -> legs -> steps -> start_location -> end_location
			    JSONObject directions = (JSONObject) JSONValue.parseWithException(builderArray);
			    JSONArray routes = (JSONArray) directions.get("routes");
			    JSONObject routeObj = (JSONObject) routes.get(0);
			    JSONArray legs = (JSONArray) routeObj.get("legs");
				JSONObject legsObj = (JSONObject) legs.get(0);
				JSONObject distanceObj = (JSONObject) legsObj.get("distance");
				long value = (long) distanceObj.get("value");
				//System.out.println("\nDistance value: " + value + "m");
				// Converting to km
				subDistance = (double) value / 1000;
				distance += subDistance;
				//System.out.println("\nSubDistance: " +subDistance + "\nUpdatedDistance: " + distance);				
			    gotDistance = true;
			    
			    // Accessing and storing lat longs
			    JSONArray steps = (JSONArray) legsObj.get("steps");
			    //System.out.println("\nAccesing and storing start_location from Google api.....");
			    int j = 0;
			    for(j = 0  ; j < steps.size() ; j++) {
			    	JSONObject stepsObj = (JSONObject) steps.get(j);
			    	// storing start_location
			    	JSONObject startLocation = (JSONObject) stepsObj.get("start_location");
			    	double lat = (double) startLocation.get("lat");
			    	double lng = (double) startLocation.get("lng");
			    	//System.out.println("\nLat: " + lat +"   "+ "Long: " + lng);
			    	lats.add(lat);
			    	longs.add(lng);	    	
			    }
			    // Getting the last step's end_location // <TO DO>
//			    System.out.println("\nj: " + j);
//			    JSONObject stepsObj = (JSONObject) steps.get(j);
//		    	JSONObject startLocation = (JSONObject) stepsObj.get("end_location");
//		    	double lat = (double) startLocation.get("lat");
//		    	double lng = (double) startLocation.get("lng");
//		    	lats.add(lat);
//		    	longs.add(lng);			    
			    
			    //System.out.println("\nStored lats: " + lats + "\nStored longs: " + longs);
				if (!gotDistance) {
					System.out.println("\nDid not receive distance ");	
				}
				if(lats.size() == 0) {
					lats = null;
					longs = null;
				}
				DrivingGoogleApi.drivingGoogleApi.setDistance(distance);
				DrivingGoogleApi.drivingGoogleApi.setLatitude(lats);
				DrivingGoogleApi.drivingGoogleApi.setLongitude(longs);
			}
			catch(Exception e) {
				System.out.println("\nError received: " + e);
				distance = 0;
				lats = null;
				longs = null;
				DrivingGoogleApi.drivingGoogleApi.setDistance(distance);
				DrivingGoogleApi.drivingGoogleApi.setLatitude(lats);
				DrivingGoogleApi.drivingGoogleApi.setLongitude(longs);
				//return distance;
			}
		}
		System.out.println("\nIn googleMapsApi => Calculated Distance: " + distance + "\nSize: " + lats.size());
		//System.out.println("\nIn googleMapsApi => Lats: " + lats);
		//System.out.println("\nIn googleMapsApi => Longs: " + longs);
		
		//return distance;	
	}

}
