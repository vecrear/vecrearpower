package com.fidz.services.location.exception;

public class GeofenceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7901269888605370396L;

	public GeofenceNotFoundException(String geofenceId) {
		super("Geofence not found with id " + geofenceId);
		// TODO Auto-generated constructor stub
	}
}
