package com.fidz.services.location.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Interval {
	private int hours = 0;
    private int minutes = 1;
    private int seconds = 0;
    private int milliseconds = 0;
    
	public Interval(int hours, int minutes, int seconds, int milliseconds) {
		super();
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.milliseconds = milliseconds;
	}

	@Override
	public String toString() {
		return "Interval [hours=" + hours + ", minutes=" + minutes + ", seconds=" + seconds + ", milliseconds="
				+ milliseconds + "]";
	}

}
