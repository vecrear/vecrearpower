package com.fidz.services.location.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.services.location.model.GpsData;

import lombok.Data;

@Data
@Document(collection = "latestlatlng")
@JsonIgnoreProperties(value = { "target" })
public class LatestGpsLatLong {

	@Id
	private String id;
	
	private String schemaName;
	
	private double lat;
	
	private double lng;
	
	private String gpsfor;
	
	private String latestDate;
	
	public static LatestGpsLatLong latestGpsLatLong = new LatestGpsLatLong();
	
	public LatestGpsLatLong() {
		super();
	}

	public static LatestGpsLatLong getLatestGpsLatLong() {
		return latestGpsLatLong;
	}

	public static void setLatestGpsLatLong(LatestGpsLatLong latestGpsLatLong) {
		LatestGpsLatLong.latestGpsLatLong = latestGpsLatLong;
	}
	
	
	 	
}
