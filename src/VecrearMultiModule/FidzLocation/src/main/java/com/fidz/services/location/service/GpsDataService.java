package com.fidz.services.location.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.validator.Constant;
import com.fidz.services.location.model.AwsInfo;
import com.fidz.services.location.model.GpsData;
import com.fidz.services.location.model.GpsDataConvertion;
import com.fidz.services.location.model.GpsDataDistnaceCal;
import com.fidz.services.location.model.GpsDataMultiUser;
import com.fidz.services.location.model.LatestGpsLatLong;
import com.fidz.services.location.repository.GpsDataRepository;

@Service
public class GpsDataService {
	private GenericAppINterface<GpsData> genericINterface;

	@Autowired
	public GpsDataService(GenericAppINterface<GpsData> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private GpsDataRepository gpsDataRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	public LatestLatLongService latestLatLongService;

	public static final double ROUND_MULTIPLIER = 100000.0;

	public List<GpsData> getAllGpsData(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllGpsData GpsDataService.java ");
		List<GpsData> gpsDatas = null;
		try {
			gpsDatas = this.genericINterface.findAll(GpsData.class);
		} catch (Exception ex) {
			Constant.LOGGER.error("Error while fetching data from getAllGpsData GpsDataService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("Successfully fetched data from getAllGpsData GpsDataService.java");
		return gpsDatas;
	}

	public List<GpsData> getGpsDataByDeviceId(String deviceId, String schemaName) {
		Constant.LOGGER.info("inside getGpsDataByDeviceId GpsDataService.java ");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.gpsDataRepository.findByDeviceID(deviceId);

	}

	public GpsData createGpsData(GpsData gpsData) {
		GpsData gpsDatas = null;
		try {
			Constant.LOGGER
					.info(" Inside GpsDataService.java Value for insert GpsData record:createGpsData :: " + gpsData);
			String schemaName = gpsData.getSchemaName();
			// String schemaName ="fidzentrapp1";

			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			gpsDatas = this.gpsDataRepository.save(gpsData);
		} catch (Exception e) {
			Constant.LOGGER.error(
					" Error Inside GpsDataService.java Value for insert GpsData record:createGpsData :: " + gpsData);
		}
		return gpsDatas;
	}

	public void createGpsDatas(GpsData gpsData) {

		String schemaName = gpsData.getSchemaName();
		// String schemaName ="fidzentrapp1";
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(gpsData);
	}

	public GpsData getGpsDataById(String id, String schemaName) {
		Constant.LOGGER.info("inside getGpsDataById GpsDataService.java ");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, GpsData.class);

	}

	public GpsData updateGpsData(String id, GpsData gpsData) {
		String schemaName = gpsData.getSchemaName();
		// String schemaName="fidzentrapp1";
		Constant.LOGGER.info("inside updateGpsData GpsDataService.java ");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		gpsData.setId(id);
		return this.gpsDataRepository.save(gpsData);

	}

	// hard delete
	public Map<String, String> deleteGpsData(String id, String schemaName) {
		Constant.LOGGER.info("inside deleteGpsData GpsDataService.java ");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.gpsDataRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "GpsData deleted successfully");
		return response;

	}

	public List<GpsData> streamAllGpsData() {

		return gpsDataRepository.findAll();
	}

	// to get the gps users from the Database
	public List<GpsData> getGps_dataUsersByDate(String deviceid, String startdate, String enddate, String schemaName)
			throws ParseException {
		Constant.LOGGER.info("Entering getGps_dataUsersByDate() Gps_dataUsersService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = null;
		List<GpsData> gps = null;

		try {
			// String date_string = "2019-10-17";
			// DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
			// Date startDate = format.parse(startdate);
			// Date endDate = format.parse(enddate);
			String date_string = "2019-10-17";
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			Date startDate = format.parse(startdate + " " + "00:00");
			Date endDate = format.parse(enddate + " " + "23:59");
			LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			localDateTime = localDateTime.plusMinutes(1);
			Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			// Query query1 = new
			// Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
			Query query1 = new Query().addCriteria(
					Criteria.where("dateTime").gte(startDate).lte(currentDatePlusOneDay).and("deviceID").is(deviceid));
			// query1.with(new Sort(Sort.Direction.DESC,"date_time"));
			Constant.LOGGER.info("Query" + query1);
			gps = this.mongoTemplate.find(query1, GpsData.class);
		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred" + ex);
		}
		return gps;
	}

	public List<GpsData> getGpsDataBasedOnData(String schemaName, String userId, String startdate, String enddate) {
		Constant.LOGGER.info("Entering getGps_dataUsersByDate() Gps_dataUsersService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = null;
		List<GpsData> gps = null;

		try {
			String date_string = "2019-10-17";
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			Date startDate = format.parse(startdate + " " + "00:00");
			Date endDate = format.parse(enddate + " " + "23:59");
			LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			localDateTime = localDateTime.plusMinutes(1);

			Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			// Query query1 = new
			// Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
			Query query1 = new Query().addCriteria(Criteria.where("dateTime").gte(startDate).lte(currentDatePlusOneDay)
					.and("gpsData.map.userId").is(userId));

			// query1.with(new Sort(Sort.Direction.DESC,"date_time"));
			Constant.LOGGER.info("Query" + query1);
			gps = this.mongoTemplate.find(query1, GpsData.class);
		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred" + ex);
		}
		return gps;
	}

//	public String reverseGeoCoding(double latitude, double longitude) {
//		 Constant.LOGGER.info("Entering reverseGeoCoding() Gps_dataUsersService.java"); 
//         String urlPrefix ="https://maps.googleapis.com/maps/api/geocode/json?";
//		System.out.println("\nLatitude: " + latitude +"\nLongitude: " + longitude);
//		String address = "";
//		String status = "";
//		String url = urlPrefix+"latlng="+latitude+","+longitude+"&key=AIzaSyDNMKPzkkKtjipjb3YIyXEEXh-Piq7qbbU";
//		System.out.println("\nurl: " + url);
//		try {
//			URL uriAddress = new URL(url);
//			URLConnection res = uriAddress.openConnection();
//			BufferedReader bufferedReader  =  new BufferedReader(new InputStreamReader(res.getInputStream()));
//		    StringBuilder builder = new StringBuilder();
//		    String line;
//		    while ((line = bufferedReader.readLine()) != null) {
//		        builder.append(line + "\n");
//		    }	
//		    String builderArray = builder.toString();
//		    //System.out.println("\nbuilderArray: \n" + builderArray);
//		    JSONObject root = (JSONObject)JSONValue.parseWithException(builderArray);
//		    //System.out.println("\nresult: \n" + root);
//		    JSONArray resultArray = (JSONArray) root.get("results");
//		    JSONObject obj0 = (JSONObject) resultArray.get(0);
//		    //System.out.println("\nobj0: \n" + obj0);
//		    String formattedAddress = (String) obj0.get("formatted_address");
//		    System.out.println("\nFormattedAddress: \n" + formattedAddress);
//		    address = formattedAddress;
//		    
//		    // Accessing status
//		    String stat = (String) root.get("status");
//		    status = stat;
//		    System.out.println("\nStatus: " + status);
//		}
//		catch(Exception e) {
//			System.out.println("\nException: " + e);
//		}
//		if(status.contentEquals("ZERO_RESULTS")) {
//			address = "No address";
//			return address;
//		}
//		if(status.contentEquals("OVER_QUERY_LIMIT")) {
//			address = "Crossed your quota of api calls";
//			return address;
//		}
//		if(status.contentEquals("REQUEST_DENIED")) {
//			address = "Request denied";
//			return address;
//		}
//		if(status.contentEquals("INVALID_REQUEST")) {
//			address = "Invalid query";
//			return address;
//		}		
//		if(status.contentEquals("UNKNOWN_ERROR")) {
//			address = "Server Error";
//			return address;
//		}		
//		if(address.contentEquals("")) {
//			address = "Blank Address";
//			return address;
//		}
//		else {
//			return address;
//		}
//	}
//	
	public List<GpsData> getGpsDataBasedOnMultiUser(String schemaName, @Valid GpsDataMultiUser gpsDataMultiUser) {
		Constant.LOGGER.info("Entering getGpsDataBasedOnMultiUser() Gps_dataUsersService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = null;
		List<GpsData> gps = null;

		try {
			String date_string = "2019-10-17";

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			Date startDate = format.parse(gpsDataMultiUser.getStartDate() + " " + "00:00");
			Date endDate = format.parse(gpsDataMultiUser.getEndDate() + " " + "23:59");
			LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			// localDateTime=localDateTime.plusDays(1);
			localDateTime = localDateTime.plusMinutes(1);
			Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			List<String> userid = gpsDataMultiUser.getUsers().stream().map(user -> user.getId())
					.collect(Collectors.toList());
			int page = gpsDataMultiUser.getPageNum();
			int size = gpsDataMultiUser.getPageSiz();
			final Pageable pageableRequest = PageRequest.of(page, size);

			// Query query1 = new
			// Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
			query = new Query().addCriteria(Criteria.where("dateTime").gte(startDate).lte(currentDatePlusOneDay)
					.and("gpsData.map.userId").in(userid));
			query.with(Sort.by(Sort.Direction.ASC, "dateTime"));
			query.with(pageableRequest);
			// query1.with(new Sort(Sort.Direction.DESC,"date_time"));
			Constant.LOGGER.info("Query" + query);
			gps = this.mongoTemplate.find(query, GpsData.class);
		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred" + ex);
		}
		return gps;
	}

	public Double getGpsDataBasedOnMultiUseDistance(String schemaName, @Valid GpsDataMultiUser gpsDataMultiUser) {
		Constant.LOGGER.info("Entering getGpsDataBasedOnMultiUser() Gps_dataUsersService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = null;
		List<GpsDataDistnaceCal> gps = null;
		double s = 0.0;

		try {
			String date_string = "2019-10-17";
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			Date startDate = format.parse(gpsDataMultiUser.getStartDate() + " " + "00:00");
			Date endDate = format.parse(gpsDataMultiUser.getEndDate() + " " + "23:59");
			LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			// localDateTime=localDateTime.plusDays(1);
			localDateTime = localDateTime.plusMinutes(1);
			Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			List<String> userid = gpsDataMultiUser.getUsers().stream().map(user -> user.getId())
					.collect(Collectors.toList());
			query = new Query().addCriteria(Criteria.where("dateTime").gte(startDate).lte(currentDatePlusOneDay)
					.and("gpsData.map.userId").in(userid));

			Constant.LOGGER.info("Query" + query);
			gps = this.mongoTemplate.find(query, GpsDataDistnaceCal.class);
			Stream<Double> nos = gps.stream().map(p -> p.getDistance());
			s = nos.collect(Collectors.summingDouble(t -> t.doubleValue()));
			s = Math.round(s * 100.0) / 100.0;

		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred" + ex);
		}
		return s;
	}

	List<GpsDataDistnaceCal> gps = null;
	double gDist = 0.0;
	Double lat = 0.0;
	Double lng = 0.0;
	String geoaddressKey = null;
	String geoDistKey = null;
	double previousLat = 0.0;
	double previousLng = 0.0;

	public List<GpsDataDistnaceCal> getGpsDataBasedOnMultiUserNew(String schemaName,
			@Valid GpsDataMultiUser gpsDataMultiUser) {
		Constant.LOGGER.info("Entering getGpsDataBasedOnMultiUser() Gps_dataUsersService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = null;

		try {
			List<AwsInfo> awsInfoKeyList = null;
			awsInfoKeyList = getAWSnfo(schemaName);
			if (awsInfoKeyList.get(0).getGeoDistKey() != null) {
				geoDistKey = awsInfoKeyList.get(0).getGeoDistKey();
				Constant.LOGGER.info("geoDistKey----------------------" + geoDistKey);
			}
			String date_string = "2019-10-17";
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
			Date startDate = format.parse(gpsDataMultiUser.getStartDate() + " " + "00:00");
			Date endDate = format.parse(gpsDataMultiUser.getEndDate() + " " + "23:59");
			LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			// localDateTime=localDateTime.plusDays(1);
			localDateTime = localDateTime.plusMinutes(1);
			Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			List<String> userid = gpsDataMultiUser.getUsers().stream().map(user -> user.getId())
					.collect(Collectors.toList());
			int page = gpsDataMultiUser.getPageNum();
			int size = gpsDataMultiUser.getPageSiz();
			final Pageable pageableRequest = PageRequest.of(page, size);

			// Query query1 = new
			// Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
			query = new Query().addCriteria(Criteria.where("dateTime").gte(startDate).lte(currentDatePlusOneDay)
					.and("gpsData.map.userId").in(userid));
			query.with(Sort.by(Sort.Direction.ASC, "dateTime"));
			query.with(pageableRequest);
			// query1.with(new Sort(Sort.Direction.DESC,"date_time"));
			Constant.LOGGER.info("Query" + query);

			gps = this.mongoTemplate.find(query, GpsDataDistnaceCal.class);

			// gps = calculatetheDistance(gps,schemaName,gpsDataMultiUser);

			List<GpsDataDistnaceCal> MainGps = gps;
			LatestGpsLatLong latLng = null;
			ObjectMapper objectMapper = new ObjectMapper();
			if (MainGps.size() > 0) {
				for (int i = 0; i < MainGps.size(); i++) {

					schemaName = MainGps.get(i).getSchemaName();
					String address = null;
					String deviceId = MainGps.get(i).getDeviceID();
					lat = MainGps.get(i).getGpsData().getMap().getLatitude();
					lng = MainGps.get(i).getGpsData().getMap().getLongitude();
					String gpsfor = MainGps.get(i).getGpsData().getMap().getGpsFor();

					if (gpsfor.equals("punchIn")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if (i == 0 && gpsfor.equals("movement")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if (i == 0 && gpsfor.equals("punchOut")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if (i == 0 && gpsfor.equals("activity")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if ((MainGps.get(i).getGpsData().getMap().getLatitude() == 0.0)
							&& gpsfor.equals("movement")) {
						previousLat = previousLat;
						previousLng = previousLng;
						gDist = 0.0;
					} else {

						Constant.LOGGER.info("lat1----lat2-----------" + previousLat + previousLng);
						Constant.LOGGER.info("lat2----lng2-----------" + lat + lng);

						boolean isSameLoc = isSameLocation(previousLat, previousLng, lat, lng);
						if (!isSameLoc) {

							Constant.LOGGER.info("isSameLoc---------------------------------" + isSameLoc);
							gDist = latestLatLongService.getGoogleDistance(previousLat, previousLng, lat, lng,
									geoDistKey);
							previousLat = lat;
							previousLng = lng;
							// gDist = 5.0;

						} else {

							Constant.LOGGER.info("isSameLoc---------------------------------" + isSameLoc);
							previousLat = lat;
							previousLng = lng;
							gDist = 0.0;

						}
					}

					GpsDataConvertion gpsclassPayload = MainGps.get(i).getGpsData();
					MainGps.get(i).setDistance(gDist);
					MainGps.get(i).setGpsData(gpsclassPayload);
					MainGps.set(i, MainGps.get(i));
				}
			}

			gps = MainGps;

		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred" + ex);
		}
		return gps;
	}

	public List<GpsDataDistnaceCal> getIdleTimeMultiUserNew(String schemaName,
			@Valid GpsDataMultiUser gpsDataMultiUser) {
		Constant.LOGGER.info("Entering getGpsDataBasedOnMultiUser() Gps_dataUsersService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query = null;

		try {
			List<AwsInfo> awsInfoKeyList = null;
			awsInfoKeyList = getAWSnfo(schemaName);
			if (awsInfoKeyList.get(0).getGeoDistKey() != null) {
				geoDistKey = awsInfoKeyList.get(0).getGeoDistKey();
				Constant.LOGGER.info("geoDistKey----------------------" + geoDistKey);
			}
			String date_string = "2019-10-17";
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);

			Date startDate = format.parse(gpsDataMultiUser.getStartDate());
			Date endDate = format.parse(gpsDataMultiUser.getEndDate());
			LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			localDateTime = localDateTime.plusMinutes(1);
			Date currentDatePlusOneSecond = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
			List<String> userid = gpsDataMultiUser.getUsers().stream().map(user -> user.getId())
					.collect(Collectors.toList());
			int page = gpsDataMultiUser.getPageNum();
			int size = gpsDataMultiUser.getPageSiz();
			final Pageable pageableRequest = PageRequest.of(page, size);

			// Query query1 = new
			// Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
			query = new Query().addCriteria(Criteria.where("dateTime").gte(startDate).lte(currentDatePlusOneSecond)
					.and("gpsData.map.userId").in(userid));
			query.with(Sort.by(Sort.Direction.ASC, "dateTime"));
			query.with(pageableRequest);
			// query1.with(new Sort(Sort.Direction.DESC,"date_time"));
			Constant.LOGGER.info("Query" + query);

			gps = this.mongoTemplate.find(query, GpsDataDistnaceCal.class);

			// gps = calculatetheDistance(gps,schemaName,gpsDataMultiUser);

			List<GpsDataDistnaceCal> MainGps = gps;
			LatestGpsLatLong latLng = null;
			ObjectMapper objectMapper = new ObjectMapper();
			if (MainGps.size() > 0) {
				for (int i = 0; i < MainGps.size(); i++) {

					schemaName = MainGps.get(i).getSchemaName();
					String address = null;
					String deviceId = MainGps.get(i).getDeviceID();
					lat = MainGps.get(i).getGpsData().getMap().getLatitude();
					lng = MainGps.get(i).getGpsData().getMap().getLongitude();
					String gpsfor = MainGps.get(i).getGpsData().getMap().getGpsFor();

					if (gpsfor.equals("punchIn")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if (i == 0 && gpsfor.equals("movement")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if (i == 0 && gpsfor.equals("punchOut")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if (i == 0 && gpsfor.equals("activity")) {
						previousLat = lat;
						previousLng = lng;
						gDist = 0.0;
					} else if ((MainGps.get(i).getGpsData().getMap().getLatitude() == 0.0)
							&& gpsfor.equals("movement")) {
						previousLat = previousLat;
						previousLng = previousLng;
						gDist = 0.0;
					} else {

						Constant.LOGGER.info("lat1----lat2-----------" + previousLat + previousLng);
						Constant.LOGGER.info("lat2----lng2-----------" + lat + lng);

						boolean isSameLoc = isSameLocation(previousLat, previousLng, lat, lng);
						if (!isSameLoc) {

							Constant.LOGGER.info("isSameLoc---------------------------------" + isSameLoc);
							// gDist = latestLatLongService.getGoogleDistance(previousLat, previousLng,
							// lat,lng,geoDistKey);
							previousLat = lat;
							previousLng = lng;
							gDist = 5.0;

						} else {

							Constant.LOGGER.info("isSameLoc---------------------------------" + isSameLoc);
							previousLat = lat;
							previousLng = lng;
							gDist = 0.0;

						}
					}

					GpsDataConvertion gpsclassPayload = MainGps.get(i).getGpsData();
					MainGps.get(i).setDistance(gDist);
					MainGps.get(i).setGpsData(gpsclassPayload);
					MainGps.set(i, MainGps.get(i));
				}
			}

			gps = MainGps;

		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred" + ex);
		}
		return gps;
	}

	public static boolean isSameLocation(Double firstLat, Double firstLng, Double secondLat, Double secondLng) {
		return (isNearBy(firstLat, secondLat) && isNearBy(firstLng, secondLng));
	}

	private static boolean isNearBy(Double first, Double second) {
		foobar(first, second);
		double diff;
		if (first >= second) {
			diff = first - second;
		} else {
			diff = second - first;
		}

//return !(diff > 0.0009); //todo
		return (diff < 0.0009);
	}

	public static void foobar(double first, double second) {
		BigDecimal opA = BigDecimal.valueOf(first);
		BigDecimal opB = BigDecimal.valueOf(second);
		BigDecimal result;
		if (first > second) {
			result = opA.subtract(opB);
		} else {
			result = opB.subtract(opA);
		}

		result = result.multiply(BigDecimal.valueOf(ROUND_MULTIPLIER));

		int cutResult = result.intValue();

		result = BigDecimal.valueOf(cutResult / ROUND_MULTIPLIER);

	}

	public List<AwsInfo> getAWSnfo(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<AwsInfo> awsInfo = null;
		Query query = new Query();
		query.addCriteria(Criteria.where("schemaName").is(schemaName));
		awsInfo = this.genericINterface.find(query, AwsInfo.class);
		return awsInfo;
	}

}
