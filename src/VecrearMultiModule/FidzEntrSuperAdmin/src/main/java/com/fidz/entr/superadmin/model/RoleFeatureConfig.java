package com.fidz.entr.superadmin.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Feature;
import com.fidz.base.model.Role;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "rolefeatureconfig")
@JsonIgnoreProperties(value = { "target" })
public class RoleFeatureConfig extends Base{
	
	@Id
	protected String id;
	
	@NotNull
	protected String permissionName;
	

	@DBRef(lazy = true)
	protected List<Feature> features;
	

	@DBRef(lazy = true)
	protected Role roles;

	public RoleFeatureConfig(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String permissionName, @NotNull List<Feature> features,
			@NotNull Role roles) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.permissionName = permissionName;
		this.features = features;
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "RoleFeatureConfig [id=" + id + ", permissionName=" + permissionName + ", features=" + features
				+ ", roles=" + roles + ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + "]";
	}

	

	
	
}
