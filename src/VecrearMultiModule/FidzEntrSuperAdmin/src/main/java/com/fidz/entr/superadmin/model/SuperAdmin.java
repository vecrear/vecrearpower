package com.fidz.entr.superadmin.model;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import lombok.Data;

@Data
@Document(collection = "superadmin")
@JsonIgnoreProperties(value = { "target" })
public class SuperAdmin extends Base{
	
	@Id
	protected String id;
	
	@NotNull
	protected String schemaNames;
	
/*	@NotNull
	@DBRef(lazy = true)
	protected Company company;*/
	protected Object company;
	@NotNull
	@Indexed(unique = true)
	@NotBlank
	protected String userName;
	
	protected String password;
	
	protected String userMongoId ;
	
	
	@NotNull
	@Indexed(unique = true)
	@NotBlank
	protected String phoneNumber;
	
	@NotNull
	protected String imei;
	
	@NotNull
	//protected long dateTime;
	protected Date dateTime;
	@NotNull
	protected boolean isSuperAdmin;
	
	public SuperAdmin() {
		// TODO Auto-generated constructor stub
	}


	


	


	

	
	
}
