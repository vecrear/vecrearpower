package com.fidz.entr.superadmin.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.superadmin.model.RoleFeatureConfig;


@Repository
public interface RoleFeatureConfigRepository extends MongoRepository<RoleFeatureConfig, String>{

}
