package com.fidz.entr.superadmin.exception;

public class RoleFeatureConfigNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9092904866453689160L;
	public RoleFeatureConfigNotFoundException(String rolefeatureConfig) {
		super("Role Feature Config not found with id/name " + rolefeatureConfig);
	}
}
