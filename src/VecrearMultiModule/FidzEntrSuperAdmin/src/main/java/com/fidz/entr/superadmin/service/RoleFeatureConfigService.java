package com.fidz.entr.superadmin.service;

import java.util.HashMap;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.User;
import com.fidz.entr.superadmin.exception.RoleFeatureConfigNotFoundException;
import com.fidz.entr.superadmin.model.RoleFeatureConfig;
import com.fidz.entr.superadmin.repository.RoleFeatureConfigRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RoleFeatureConfigService {
	private GenericAppINterface<RoleFeatureConfig> genericINterface;
	@Autowired
	public RoleFeatureConfigService(GenericAppINterface<RoleFeatureConfig> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private RoleFeatureConfigRepository roleFeatureConfigRepository;
	
	
    public List<RoleFeatureConfig> getAllRoleFeatureConfigs(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllRoleFeatureConfigs RoleFeatureConfigService.java");
    	List<RoleFeatureConfig> roleFeatureConfigs=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	roleFeatureConfigs=this.genericINterface.findAll(RoleFeatureConfig.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllRoleFeatureConfigs RoleFeatureConfigService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllRoleFeatureConfigs RoleFeatureConfigService.java");
    	return roleFeatureConfigs;
    	
    }
	
    
	public RoleFeatureConfig createRoleFeatureConfig(RoleFeatureConfig roleFeatureConfig) {
		RoleFeatureConfig roleFeatureConfigs=null;
		try {
			Constant.LOGGER.debug(" Inside RoleFeatureConfigService.java Value for insert RoleFeatureConfig record:createRoleFeatureConfig :: " + roleFeatureConfig);
			String schemaName = roleFeatureConfig.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			roleFeatureConfigs=this.roleFeatureConfigRepository.save(roleFeatureConfig);
		}catch(Exception e) {
			Constant.LOGGER.debug(" Inside RoleFeatureConfigService.java Value for insert RoleFeatureConfig record:createRoleFeatureConfig :: " + roleFeatureConfig);
		}
		return roleFeatureConfigs;
	}
    
	public void createRoleFeatureConfigs(RoleFeatureConfig roleFeatureConfig) {
		String schemaName = roleFeatureConfig.getSchemaName();

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(roleFeatureConfig);
	}
	
    public RoleFeatureConfig getRoleFeatureConfigById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, RoleFeatureConfig.class);
		
    }
	
	/*
    public Mono<SuperAdmin> getApplicationByAppName(String name) {
		return superAdminRepository.findByName(name).switchIfEmpty(Mono.error(
				new SuperAdminNotFoundException(name)));
    }*/
	
   
    public RoleFeatureConfig updateRoleFeatureConfig(String id, RoleFeatureConfig roleFeatureConfig) {
    	String schemaName = roleFeatureConfig.getSchemaName();

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		roleFeatureConfig.setId(id);
		return this.roleFeatureConfigRepository.save(roleFeatureConfig);
		
    }

    
  //hard delete
  	public Map<String, String> deleteRoleFeatureConfig(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.roleFeatureConfigRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "RoleFeatureConfig deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftRoleFeatureConfig(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		RoleFeatureConfig roleFeatureConfig = this.genericINterface.findById(id, RoleFeatureConfig.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		roleFeatureConfig.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		roleFeatureConfig.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		roleFeatureConfig.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(roleFeatureConfig);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "RoleFeatureConfig deleted successfully");
  		return response;

  	}
     
    //SuperAdmin are Sent to the client as Server Sent Events
    public List<RoleFeatureConfig> streamAllRoleFeatureConfigs() {
        return roleFeatureConfigRepository.findAll();
    }

//To get all RoleFeatureConfig Paginated
	public List<RoleFeatureConfig> getAllRoleFeatureConfigsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllRoleFeatureConfigsPaginated RoleFeatureConfigService.java");
    	Query query = new Query();
    	List<RoleFeatureConfig> roleFeatureConfigs =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	roleFeatureConfigs= this.genericINterface.find(query, RoleFeatureConfig.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllRoleFeatureConfigsPaginated RoleFeatureConfigService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllRoleFeatureConfigsPaginated "+query);
        return roleFeatureConfigs;
	}

}
