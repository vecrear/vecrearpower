package com.fidz.entr.superadmin.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.superadmin.model.SuperAdmin;
@Repository
public interface SuperAdminRepository extends MongoRepository<SuperAdmin, String>{
	//public Mono<SuperAdmin> findByPhoneNumber(String userName);
	public SuperAdmin findByPhoneNumber(String userName);
	public SuperAdmin findByUserName(String userName);
	public SuperAdmin findByUserMongoId(String userMongoId);
}
