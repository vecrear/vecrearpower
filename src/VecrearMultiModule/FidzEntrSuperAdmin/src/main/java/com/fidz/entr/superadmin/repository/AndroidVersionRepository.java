package com.fidz.entr.superadmin.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.fidz.entr.superadmin.model.AndroidVersion;

@Repository
public interface AndroidVersionRepository extends MongoRepository<AndroidVersion,String>{

}
