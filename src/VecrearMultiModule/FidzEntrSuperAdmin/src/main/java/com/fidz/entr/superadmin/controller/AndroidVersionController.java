package com.fidz.entr.superadmin.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.superadmin.model.AndroidVersion;
import com.fidz.entr.superadmin.service.AndroidVersionService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/androidversions")
public class AndroidVersionController {
	
	@Autowired
	private AndroidVersionService  androidVersionService;
	
	@PostMapping("/get/all")
	public List<AndroidVersion> getAllAndroidVersions(@RequestHeader(value="schemaName") String schemaName){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return androidVersionService.getAllAndroidVersions(schemaName);
	}
	
	//Post request to get all AndroidVersions  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<AndroidVersion> getAllAndroidVersionsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return androidVersionService.getAllAndroidVersionsPaginated(pageNumber, pageSize, schemaName);
    }
	
	@PostMapping
	public AndroidVersion createAndroidVersion(@Valid @RequestBody AndroidVersion androidVersion){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return androidVersionService.createAndroidVersion(androidVersion);
	}
	@PostMapping("/creates")
	public void createAndroidVersions(@Valid @RequestBody AndroidVersion androidVersion){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		androidVersionService.createAndroidVersions(androidVersion);
	}
	@PostMapping("/get/id/{id}")
	public AndroidVersion getAndroidVersionById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return androidVersionService.getAndroidVersionById(id, schemaName);
		
	}
	
	@PostMapping("/update/id/{id}")
    public AndroidVersion updateAndroidVersion(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody AndroidVersion androidVersion) {
        return androidVersionService.updateAndroidVersion(id, androidVersion);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteAndroidVersion(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return androidVersionService.deleteAndroidVersion(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteAndroidVersion(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return androidVersionService.softDeleteAndroidVersion(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<AndroidVersion> streamAllAndroidVersion() {
        return androidVersionService.streamAllAndroidVersion();
    }


}
