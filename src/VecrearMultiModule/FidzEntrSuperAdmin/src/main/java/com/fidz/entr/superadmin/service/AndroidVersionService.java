package com.fidz.entr.superadmin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.superadmin.model.AndroidVersion;
import com.fidz.entr.superadmin.repository.AndroidVersionRepository;

@Service
public class AndroidVersionService {

	private GenericAppINterface<AndroidVersion> genericINterface;
	@Autowired
	public AndroidVersionService(GenericAppINterface<AndroidVersion> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private AndroidVersionRepository  androidVersionRepository;
	
	
	public List<AndroidVersion> getAllAndroidVersions(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllAndroidVersions AndroidVersionService.java");
    	List<AndroidVersion> androidVersions=null;
    	try {
    	androidVersions	=	this.genericINterface.findAll(AndroidVersion.class);
    	Constant.LOGGER.error("androidVersions--------------"+androidVersions.toString());
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllAndroidVersions AndroidVersionService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllAndroidVersions AndroidVersionService.java");
    	return androidVersions;		
	}

	public List<AndroidVersion> getAllAndroidVersionsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllAndroidVersionsPaginated AndroidVersionService.java");
    	Query query = new Query();
    	List<AndroidVersion> androidVersions =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	androidVersions= this.genericINterface.find(query, AndroidVersion.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllAndroidVersionsPaginated AndroidVersionService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllAndroidVersionsPaginated "+query);
        return androidVersions;	
	}		
	

	public AndroidVersion createAndroidVersion(@Valid AndroidVersion androidVersion) {
		AndroidVersion androidVersions=null;
		try {
			Constant.LOGGER.info(" Inside AndroidVersionService.java Value for insert AndroidVersion record:createAndroidVersiony :: " + androidVersion);
			String schemaName=androidVersion.getSchemaName();
	    	
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
			 androidVersions=this.androidVersionRepository.save(androidVersion);
		}catch(Exception e) {
			Constant.LOGGER.info(" Inside AndroidVersionService.java Value for insert AndroidVersion record:createAndroidVersiony :: " + androidVersion);
		}
		 return androidVersions;
	}

	public void createAndroidVersions(@Valid AndroidVersion androidVersion) {
		String schemaName=androidVersion.getSchemaName();
    	Constant.LOGGER.info("Inside createAndroidVersions AndroidVersionService.java");

		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		//return countryRepository.save(country);
		this.genericINterface.saveName(androidVersion);	
	}

	public AndroidVersion getAndroidVersionById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
    	Constant.LOGGER.info("Inside getAndroidVersionById AndroidVersionService.java");
         return this.genericINterface.findById(id, AndroidVersion.class);
	}

	public AndroidVersion updateAndroidVersion(String id, @Valid AndroidVersion androidVersion) {
		String schemaName=androidVersion.getSchemaName();
    	
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		 androidVersion.setId(id);
	    	Constant.LOGGER.info("Inside updateAndroidVersion AndroidVersionService.java");

		 return this.androidVersionRepository.save(androidVersion);
	}

	public Map<String, String> deleteAndroidVersion(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
    	Constant.LOGGER.info("Inside deleteAndroidVersion AndroidVersionService.java");
        this.androidVersionRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "AndroidVersion deleted successfully");
          return response;		
	}

	public Map<String, String> softDeleteAndroidVersion(String id, String schemaName, @Valid TimeUpdate timeUpdate) {
    	Constant.LOGGER.info("Inside softDeleteAndroidVersion AndroidVersionService.java");

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("VTrackSuperAdmin");
		AndroidVersion androidVersion = this.genericINterface.findById(id, AndroidVersion.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		androidVersion.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		androidVersion.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		androidVersion.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(androidVersion);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "androidVersion deleted successfully");
		return response;	}

	public List<AndroidVersion> streamAllAndroidVersion() {
		return androidVersionRepository.findAll();
	}
	

}
