package com.fidz.entr.superadmin.controller;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.validator.Constant;
import com.fidz.entr.superadmin.model.RoleFeatureConfig;
import com.fidz.entr.superadmin.model.SuperAdmin;
import com.fidz.entr.superadmin.model.SuperAdminAuthenticateResponse;
import com.fidz.entr.superadmin.service.SuperAdminService;
import com.fidz.services.authentication.model.Login;


@CrossOrigin(maxAge = 3600)
@RestController
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/superadmin")
public class SuperAdminController {

	@Autowired
	private SuperAdminService superAdminService;
	
	@Value("${haversineservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all")
    public List<SuperAdmin> getAllSuperAdmins(@RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.getAllSuperAdmins(schemaName);
    }
	
	//Post request to get all RoleFeatureConfig  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<SuperAdmin> getAllSuperAdminsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.getAllSuperAdminsPaginated(pageNumber, pageSize, schemaName);
	}
	
    @PostMapping
    public SuperAdmin createSuperAdmin(@Valid @RequestBody SuperAdmin superAdmin) {
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.createSuperAdmin(superAdmin);
    }
    @PostMapping("/creates")
    public void createSuperAdmins(@Valid @RequestBody SuperAdmin superAdmin) {
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		superAdminService.createSuperAdmin(superAdmin);
    }
    
    @PostMapping("/get/id/{id}")
    public SuperAdmin getSuperAdminById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.getSuperAdminById(id, schemaName);
    }
	
	/*@GetMapping("/appname/{name}")
    public Mono<SuperAdmin> getApplicationByAppName(@PathVariable(value = "name") String name) {
		return superAdminRepository.findByName(name).switchIfEmpty(Mono.error(
				new SuperAdminNotFoundException(name)));
    }*/
	
    @PostMapping("/update/id/{id}")
    public SuperAdmin updateSuperAdmin(@PathVariable(value = "id") String id, @Valid @RequestBody SuperAdmin superAdmin) {
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.updateSuperAdmin(id, superAdmin);
    }

   
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteSuperAdmin(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.deleteSuperAdmin(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteSuperAdmin(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.deleteSoftSuperAdmin(id, schemaName, timeUpdate);
    }
    
    //SuperAdmin are Sent to the client as Server Sent Events
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<SuperAdmin> streamAllSuperAdmins() {
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.streamAllSuperAdmins();
    }

    @PostMapping("/login/authenticate")
	 public ResponseEntity<SuperAdminAuthenticateResponse> authenticateUser(@Valid @RequestBody Login login) {
	     
    	System.out.println("property value******"+property);
		String message="Superadminservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return superAdminService.authenticateUser(login);
	 }
    
    // Exception Handling Examples
 /*   @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A SuperAdmin with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(SuperAdminNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }	*/
}
