package com.fidz.entr.superadmin.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;


@Data
@Document(collection = "androidversion")
@JsonIgnoreProperties(value = { "target" })
public class AndroidVersion extends Base {

	@Id
	protected String id;
	
	protected String schemaNames;
	
	protected String versionCode;
	
	protected String versionNumber;

	public AndroidVersion(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, String schemaNames, String versionCode, String versionNumber) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.schemaNames = schemaNames;
		this.versionCode = versionCode;
		this.versionNumber = versionNumber;
	}

	public AndroidVersion() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "AndroidVersion [id=" + id + ", schemaNames=" + schemaNames + ", versionCode=" + versionCode
				+ ", versionNumber=" + versionNumber + ", createdTimestamp=" + createdTimestamp + ", createdByUser="
				+ createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser
				+ ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status
				+ ", schemaName=" + schemaName + "]";
	}

	
	
	
	
	

}
