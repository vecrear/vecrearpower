package com.fidz.entr.superadmin.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.entr.base.model.LoginMode;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Login extends com.fidz.services.authentication.model.Login{	
	
	protected String imei;
	
	protected String notificationId;
	
	protected LoginMode loginMode;

	/*public Login(@NotNull String userName, @NotNull String password, String imei, String notificationId,
			LoginMode loginMode) {
		super(userName, password);
		this.imei = imei;
		this.notificationId = notificationId;
		this.loginMode = loginMode;
	}
*/
	@Override
	public String toString() {
		return "Login [imei=" + imei + ", notificationId=" + notificationId + ", loginMode=" + loginMode + ", userName="
				+ userName + ", password=" + password + "]";
	}

	public Login(@NotNull String userName, @NotNull String password, String imei, String notificationId,
			LoginMode loginMode) {
		super(userName, password);
		this.imei = imei;
		this.notificationId = notificationId;
		this.loginMode = loginMode;
	}

	public Login() {
		super();
	}

	

	
	
	
}
