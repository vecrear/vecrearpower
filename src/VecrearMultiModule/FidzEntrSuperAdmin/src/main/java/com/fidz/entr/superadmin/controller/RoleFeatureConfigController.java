package com.fidz.entr.superadmin.controller;

import java.util.List;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.entr.superadmin.exception.RoleFeatureConfigNotFoundException;
import com.fidz.entr.superadmin.model.RoleFeatureConfig;
import com.fidz.entr.superadmin.payload.ErrorResponse;
import com.fidz.entr.superadmin.service.RoleFeatureConfigService;


@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/rolefeatureconfigs")
public class RoleFeatureConfigController {

	@Autowired
	private RoleFeatureConfigService roleFeatureConfigService;
	
	@PostMapping("/get/all")
    public List<RoleFeatureConfig> getAllRoleFeatureConfigs(@RequestHeader(value="schemaName") String schemaName) {
    	return roleFeatureConfigService.getAllRoleFeatureConfigs(schemaName);
    }
	
	//Post request to get all RoleFeatureConfig  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<RoleFeatureConfig> getAllRoleFeatureConfigsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return roleFeatureConfigService.getAllRoleFeatureConfigsPaginated(pageNumber, pageSize, schemaName);
	}
	
    @PostMapping
    public RoleFeatureConfig createRoleFeatureConfig(@Valid @RequestBody RoleFeatureConfig roleFeatureConfig) {
        return roleFeatureConfigService.createRoleFeatureConfig(roleFeatureConfig);
    }
    @PostMapping("/creates")
    public void createRoleFeatureConfigs(@Valid @RequestBody RoleFeatureConfig roleFeatureConfig) {
       roleFeatureConfigService.createRoleFeatureConfigs(roleFeatureConfig);
    }
    @PostMapping("/get/id/{id}")
    public RoleFeatureConfig getRoleFeatureConfigById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return roleFeatureConfigService.getRoleFeatureConfigById(id, schemaName);
    }
	
	/*@GetMapping("/appname/{name}")
    public Mono<SuperAdmin> getApplicationByAppName(@PathVariable(value = "name") String name) {
		return superAdminRepository.findByName(name).switchIfEmpty(Mono.error(
				new SuperAdminNotFoundException(name)));
    }*/
	
    @PostMapping("/update/id/{id}")
    public RoleFeatureConfig updateRoleFeatureConfig(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody RoleFeatureConfig roleFeatureConfig) {
        return roleFeatureConfigService.updateRoleFeatureConfig(id, roleFeatureConfig);
    }

  
    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteRoleFeatureConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return roleFeatureConfigService.deleteRoleFeatureConfig(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteRoleFeatureConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return roleFeatureConfigService.deleteSoftRoleFeatureConfig(id, schemaName, timeUpdate);
    }
    
    //SuperAdmin are Sent to the client as Server Sent Events
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<RoleFeatureConfig> streamAllRoleFeatureConfigs() {
        return roleFeatureConfigService.streamAllRoleFeatureConfigs();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A SuperAdmin with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(RoleFeatureConfigNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }	
}
