package com.vecrear.multimodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VecrearMultiModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(VecrearMultiModuleApplication.class, args);
	}

}
