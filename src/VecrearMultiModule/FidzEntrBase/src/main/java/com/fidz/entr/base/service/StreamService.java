package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.Stream;
import com.fidz.entr.base.repository.StreamRepository;

@Service("FEBStreamService")
public class StreamService {

	@Autowired
	private StreamRepository streamRepository;
	
	private GenericAppINterface<Stream> genericINterface;

	@Autowired
	public StreamService(GenericAppINterface<Stream> genericINterface) {
		this.genericINterface = genericINterface;
	}

	public List<Stream> getAllStreams(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllStreams StreamService.java");
    	List<Stream> streams=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	streams=this.genericINterface.findAll(Stream.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllStreams StreamService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllStreams StreamService.java");
    	return streams;

	}
		
	public Stream createStream(Stream stream) {
		String schemaName = stream.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.streamRepository.save(stream);
	}
	public void createStreams(Stream stream) {
		String schemaName = stream.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(stream);
	}
	    
	public Stream getStreamById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Stream.class);

	}
		
	public Stream getStreamByName(String name, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.streamRepository.findByName(name);

	}
		
	    public Stream updateStream(String id, Stream stream) {
	    	String schemaName = stream.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			stream.setId(id);
			return this.streamRepository.save(stream);
	        
	    }
	    //hard delete
	    public Map<String, String> deleteStream(String  id, String schemaName) {
	 	  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     	 this.streamRepository.deleteById(id);
	     	 Map<String, String> response = new HashMap<String, String>();
	     	    response.put("message", "Stream deleted successfully");
	     	    return response;
	        
	     }
	     //soft delete
	    public Map<String, String> deleteSoftStream(String  id, String schemaName, TimeUpdate timeUpdate) {
	 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	 		 Stream stream=this.genericINterface.findById(id, Stream.class);
	 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
	 		stream.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
	 		stream.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
	 		stream.setStatus(Status.INACTIVE);
	 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
	 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	 		  this.genericINterface.saveName(stream);
	 	    	 
	 	    	 Map<String, String> response = new HashMap<String, String>();
	 	    	    response.put("message", "Stream deleted successfully");
	 	    	    return response;
	 	       
	 	    }

	   

	    public List<Stream> streamAllStreams() {
	        return streamRepository.findAll();
	    }
	  //To get all streams paginated
	    //page number starts from zero
		public List<Stream> getAllStreamsPaginated(int pageNumber, int pageSize, String schemaName) {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	Constant.LOGGER.info("inside getAllStreamsPaginated StreamService.java");
	    	Query query = new Query();
	    	List<Stream> streams =null;

	    	try {
		    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
	    	query.with(pageableRequest);
	    	streams= this.genericINterface.find(query, Stream.class);	
	    	

	    	}catch(Exception ex) {
		    Constant.LOGGER.error("Error occurred inside getAllStreamsPaginated StreamService.java "+ex.getMessage());
	        }
	    	Constant.LOGGER.info("After success getAllStreamsPaginated "+query);
	        return streams;	
		
		}
}
