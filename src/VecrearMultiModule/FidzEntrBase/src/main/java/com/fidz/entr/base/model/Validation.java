package com.fidz.entr.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Validation {
	protected String name;
	protected String pattern;
	protected String jsCodeSnippet;
	protected String javaCodeSnippet;
	
	public Validation(String name, String pattern, String jsCodeSnippet, String javaCodeSnippet) {
		super();
		this.name = name;
		this.pattern = pattern;
		this.jsCodeSnippet = jsCodeSnippet;
		this.javaCodeSnippet = javaCodeSnippet;
	}
	
	@Override
	public String toString() {
		return "Validation [name=" + name + ", pattern=" + pattern + ", jsCodeSnippet=" + jsCodeSnippet
				+ ", javaCodeSnippet=" + javaCodeSnippet + "]";
	}
}
