package com.fidz.entr.base.repository;

import org.springframework.stereotype.Repository;

import reactor.core.publisher.Mono;
@Repository
public interface GenericINterface<T> {
	Mono<T> saveName(T model1);
}
