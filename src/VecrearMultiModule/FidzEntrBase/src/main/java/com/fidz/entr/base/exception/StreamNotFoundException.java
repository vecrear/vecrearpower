package com.fidz.entr.base.exception;

public class StreamNotFoundException extends RuntimeException{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2280589551226115742L;

	public StreamNotFoundException(String stream) {
		super("Stream not found with id/name " + stream);
	}
}
