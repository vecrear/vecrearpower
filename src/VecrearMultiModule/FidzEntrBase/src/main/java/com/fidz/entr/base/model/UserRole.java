package com.fidz.entr.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "userrole")
@JsonIgnoreProperties(value = { "target" })
public class UserRole extends Base {

	@Id
	protected String id;
	
	@NotBlank
	protected String name;
	
	protected String description;
	
	@DBRef(lazy = true)
	protected Company company;
	
	@DBRef(lazy = true)
	protected Department department;

	
}
