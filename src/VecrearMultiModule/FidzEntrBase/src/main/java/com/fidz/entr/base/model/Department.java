package com.fidz.entr.base.model;

import java.util.Date;
import java.util.TimeZone;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Department extends Base {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	protected String description;
	
	@DBRef(lazy = true)
	protected Company company;


	@NotNull
	protected PunchInFrequency punchInFrequency;
	
	/**
	 * gpsInterval - Should be in mins
	 */
	@NotNull
	protected int gpsInterval;
	
	protected String mqttTopicName;
	
	protected DistanceCalculationMethod distanceCalculationMethod;

	protected String forcePunchOutTime;
	
	protected TimeZone forcePunchOutTimeZone;
	
	protected String deptTimeZone;
	
	protected String country;
	
	
	
	

	
	


	


}
