package com.fidz.entr.base.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.TaggedCities;
import com.fidz.entr.base.repository.CityRepository;
import com.fidz.entr.base.repository.StateRepository;

@Service("FEBStateService")
public class StateService {
	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	private GenericAppINterface<State> genericINterface;

	@Autowired
	public StateService(GenericAppINterface<State> genericINterface) {
		this.genericINterface = genericINterface;
	}

	@Autowired
	private CityService cityService;

	private static String name = "State";

	public List<State> getAllStates(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllStates StateService.java");
		List<State> states = null;
		try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
			states = this.genericINterface.findAll(State.class);
		} catch (Exception ex) {
			Constant.LOGGER.error("Error while getting data from getAllStates StateService.java" + ex.getMessage());
		}
		Constant.LOGGER.info("**************************************************************************");
		Constant.LOGGER.info("successfully fetched data from getAllStates StateService.java");
		return states;
	}

	public State createState(State state) {

		Constant.LOGGER.info(" Inside StateService.java Value for insert State record:createState :: " + state);
		String schemaName = state.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// List<State> states = this.genericINterface.findAll(State.class);
		// List<State> states = this.stateRepository.findByName(state.getName());
		List<State> states = null;
		Query q = new Query();
		q.addCriteria(Criteria.where("name").regex(state.getName(), "i"));
		states = this.mongoTemplate.find(q, State.class);
		List<TaggedCities> taggedCities = new ArrayList<TaggedCities>();

		List<String> names = states.stream().map(p -> p.getName()).collect(Collectors.toList());
		Constant.LOGGER.info("Facility List data" + names);

		if (names.stream().anyMatch(s -> s.equals(state.getName()) == true)) {
			Constant.LOGGER.info("Inside if statement");
			throw new NameFoundException(name + ExceptionConstant.SAME_NAME + ": " + state.getName());
		} else if (names.stream().anyMatch(s -> s.equalsIgnoreCase(state.getName()) == true)) {
			Constant.LOGGER.info("Inside else if statement");
			throw new NameFoundException(name + ExceptionConstant.SAME_NAME_DIFFERENT_CASE + ": " + state.getName());
		} else {
			Constant.LOGGER.info("Successfull creation of State StateService.java");
			Constant.LOGGER.info("Inside else statement");
			// to update the tagged cities from state
			try {
				taggedCities = state.getTaggedCities();
				Constant.LOGGER.info("taggedCities---size-" + taggedCities.size());
				City city = null;
				for (int i = 0; i < taggedCities.size(); i++) {
					city = cityService.getCityById(taggedCities.get(i).getId(), schemaName);
					city.setId(taggedCities.get(i).getId());
					city.setCityTagged(taggedCities.get(i).isCityTagged());
					city.setTaggedStateName(taggedCities.get(i).getTaggedStateName());
					this.cityRepository.save(city);

				}

			} catch (Exception e) {
				// return null;
			}

			// return this.genericINterface.saveName(state); }

			return this.stateRepository.save(state);

			// return this.stateRepository.save(state);
			//
		}

	}

	public void createStates(State state) {
		String schemaName = state.getSchemaName();
		Constant.LOGGER.info("Inside createStates StateService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(state);
	}

	public State getStateById(String id, String schemaName) {
		Constant.LOGGER.info("Inside getStateById StateService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, State.class);

	}

	public State getStateByName(String name, String schemaName) {
		Constant.LOGGER.info("Inside getStateByName StateService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.stateRepository.findByName(name);
	}

	public State updateState(String id, State state) {
		Constant.LOGGER.info("Inside updateState StateService.java");

		String schemaName = state.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		City city = null;
		List<TaggedCities> taggedCities = state.getTaggedCities();
		// List<State> states = this.genericINterface.findAll(State.class);
		// List<State> states = this.stateRepository.findByName(state.getName());
		List<State> states = null;
		Query q = new Query();
		q.addCriteria(Criteria.where("name").regex(state.getName(), "i"));
		states = this.mongoTemplate.find(q, State.class);
		State ctr = getStateById(id, schemaName);
		List<String> names = states.stream().map(p -> p.getName()).collect(Collectors.toList());
		names.remove(ctr.getName());
		Constant.LOGGER.info("Facility List data" + names);

		if (names.stream().anyMatch(s -> s.equals(state.getName()) == true)) {
			Constant.LOGGER.info("Inside if statement");
			throw new NameFoundException(name + ExceptionConstant.SAME_NAME + ": " + state.getName());
		} else if (names.stream().anyMatch(s -> s.equalsIgnoreCase(state.getName()) == true)) {
			Constant.LOGGER.info("Inside else if statement");
			throw new NameFoundException(name + ExceptionConstant.SAME_NAME_DIFFERENT_CASE + ": " + state.getName());
		} else {
			for (int i = 0; i < taggedCities.size(); i++) {
				city = cityService.getCityById(taggedCities.get(i).getId(), schemaName);
				city.setId(taggedCities.get(i).getId());
				city.setCityTagged(taggedCities.get(i).isCityTagged());
				city.setTaggedStateName(taggedCities.get(i).getTaggedStateName());
				this.cityRepository.save(city);

			}

			state.setId(id);

			return this.stateRepository.save(state);
		}

	}

	public Optional<Object> updateStates(String id, State state) {
		String schemaName = state.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.stateRepository.findById(id).flatMap(existingApplication -> {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			state.setId(id);
			return this.genericINterface.saveNames(state);
		});

	}

	// hard delete
	public Map<String, String> deleteState(String id, String schemaName) {
		Constant.LOGGER.info("Inside deleteState StateService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.stateRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "State deleted successfully");
		return response;

	}

	// soft delete
	public Map<String, String> deleteSoftState(String id, String schemaName, TimeUpdate timeUpdate) {
		Constant.LOGGER.info("Inside deleteSoftState StateService.java");
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		State state = this.genericINterface.findById(id, State.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		state.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		state.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		state.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(state);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "State deleted successfully");
		return response;

	}

	public List<State> streamAllStates() {
		return stateRepository.findAll();
	}

	// To get all states paginated
	// page number starts from zero
	public List<State> getAllStatesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllStatesPaginated StateService.java");
		Query query = new Query();
		List<State> states = null;

		try {
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			query.with(pageableRequest);
			states = this.genericINterface.find(query, State.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error occurred inside getAllStatesPaginated StateService.java " + ex.getMessage());
		}
		return states;
	}

	public List<State> getAllTagUnTaggedStates(String variableMap, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllTagUnTaggedStates StateService.java");
		Query query = new Query();
		List<State> states = null;

		try {
			if (variableMap.equalsIgnoreCase("tagged")) {
				query.addCriteria(Criteria.where("stateTagged").in(true));
			} else if (variableMap.equalsIgnoreCase("untagged")) {
				query.addCriteria(Criteria.where("stateTagged").in(false));
			}

			states = this.genericINterface.find(query, State.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error inside getAllTagUnTaggedStates StateService.java " + ex.getMessage());
		}
		return states;
	}

	public List<State> getAllstatesByRegionName(String regionname, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllstatesByRegionName StateService.java");
		Query query1 = new Query();
		Query query2 = new Query();
		List<State> state = new ArrayList<State>();
		List<State> states = null;

		try {
			query1.addCriteria(Criteria.where("taggedRegionName").in(regionname));
			states = this.genericINterface.find(query1, State.class);
			state.addAll(states);

			query2.addCriteria(Criteria.where("stateTagged").in(false));
			states = this.genericINterface.find(query2, State.class);
			state.addAll(states);

		} catch (Exception ex) {
			Constant.LOGGER.error("Error inside getAllstatesByRegionName StateService.java " + ex.getMessage());
		}
		return state;
	}

	public List<State> stateTextSearch(@Valid String text, String schemaName, int pageNumber, int pageSize) {
		List<State> state = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
		Constant.LOGGER.info("Inside stateTextSearch StateService.java");
		try {

			textIndex = new TextIndexDefinitionBuilder().onAllFields().build();
			mongoTemplate.indexOps(State.class).ensureIndex(textIndex);
			criteria = TextCriteria.forDefaultLanguage().matchingAny(text).caseSensitive(false)
					.diacriticSensitive(false);
			query = TextQuery.queryText(criteria).sortByScore().with(PageRequest.of(pageNumber, pageSize));
			state = mongoTemplate.find(query, State.class);

		} catch (Exception ex) {
			Constant.LOGGER.error("Inside stateTextSearch StateService.java" + ex.getMessage());

		}
		return state;
	}

	public List<State> stateTextByNameSearch(@Valid String text, String schemaName, int pageNumber, int pageSize) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<State> states = null;
		Query q = new Query();

		try {
			q.addCriteria(Criteria.where("name").regex(text, "i"));
			final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
			q.with(pageableRequest);
			states = this.genericINterface.find(q, State.class);

		} catch (Exception ex) {
			Constant.LOGGER.info("Error occured inside getAllSearchCitiesPaginated" + ex.getLocalizedMessage());
		}
		return states;
	}
}
