package com.fidz.entr.base.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Base;
import com.fidz.base.model.Contact;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "company")
@JsonIgnoreProperties(value = { "target" })
public class Company extends Base {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	//Company Type: https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country
	@NotNull
	protected String type;
	
	protected String description;
	
	//International Securities Identification Number (ISIN) 
	protected String isin;
	
	protected Address headquarterAddress;
	
	protected String website;
	
	protected Contact contact;
	
	@DBRef(lazy = true)
	protected Company parent;
	
	@DBRef(lazy = true)
	protected List<Company> subsidiaries;
	
	@DBRef(lazy = true)
	protected List<Facility> facilities;
	
	@DBRef(lazy = true)
	protected List<Department> departments;

	


	
}
