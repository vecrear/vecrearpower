package com.fidz.entr.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "city")
@JsonIgnoreProperties(value = { "target" })
public class City extends Base {

	@Id
	protected String id;
	
	//@NotNull(message = "City name is Empty")
	//@NotBlank(message = "CityName.required")
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	protected boolean cityTagged;
	
	protected String taggedStateName;
	
	protected String description;

	
	
	


	

	
	





	

	


	

}
