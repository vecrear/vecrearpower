package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.entr.base.model.DeviceType;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.ProductCategory;
import com.fidz.entr.base.service.ProductService;

@CrossOrigin(maxAge = 3600)
@RestController("FEBProductController")
@RequestMapping(value="/products")
public class ProductController {
	
	@Autowired
	private ProductService productService; 
	
	
	@PostMapping("/get/all")
	//@Cacheable(value = "products")
    public List<Product> getAllProducts(@RequestHeader(value="schemaName") String schemaName) {
    	return productService.getAllProducts(schemaName);
    }
	

	//Post request to get all products paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Product> getAllProductsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
    return productService.getAllProductsPaginated(pageNumber, pageSize, schemaName);
	}
		
	@PostMapping
	public Product createProduct(@Valid @RequestBody Product product) {
		return productService.createProduct(product);
	}
	
	@PostMapping("/creates")
    public void createProducts(@Valid @RequestBody Product product) {
		productService.createProduct(product);
    }

	// all filtering Apis
	
	// get all the categories by id
	  @PostMapping("/get/id/{id}")
	    public Product getProductById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
			return productService.productById(id, schemaName);
	    }

	  
	  //get by name
	  @PostMapping("/get/name/productname/{name}")
	    public Product getProductByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
			return productService.getProductByName(name, schemaName);
	    }
	  
	  // updating the products 
	  
	  @PostMapping("/update/id/{id}")
	    public Product updateProduct(@PathVariable(value = "id") String id,
	                                                   @Valid @RequestBody Product product) {
	        return productService.updateProduct(id, product);
	    }
	  
	  @PostMapping("/delete/id/{id}")
	    public Map<String, String> deleteProduct(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
	        return productService.deleteProduct(id, schemaName);
	    }
	    @PostMapping("/delete/soft/id/{id}")
	    public Map<String, String> softDeleteProduct(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
	        return productService.softDeleteProduct(id, schemaName, timeUpdate);
	    }
	    
	    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	    public List<Product> streamAllProducts() {
	        return productService.streamAllProducts();
	    }
	
	
}
