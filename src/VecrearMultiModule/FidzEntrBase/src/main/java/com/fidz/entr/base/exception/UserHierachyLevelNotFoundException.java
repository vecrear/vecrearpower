package com.fidz.entr.base.exception;

public class UserHierachyLevelNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4439154753766799088L;
	public UserHierachyLevelNotFoundException(String userHierachy) {
		super("UserHierachy not found with id/name " + userHierachy);
	}

}
