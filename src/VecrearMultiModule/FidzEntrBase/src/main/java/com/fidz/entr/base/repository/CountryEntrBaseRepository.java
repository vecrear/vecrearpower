package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.Country;


@Repository("FEntraBaseRepository")
public interface CountryEntrBaseRepository extends MongoRepository<Country, String>{

}
