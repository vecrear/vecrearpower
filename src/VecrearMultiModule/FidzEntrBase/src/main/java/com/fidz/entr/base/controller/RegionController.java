package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.RegionNotFoundException;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.service.RegionService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEBRegionController")
@RequestMapping(value="/regions")
public class RegionController {

	@Autowired
	private RegionService regionService;
	
	
	@PostMapping("/get/all")
    public List<Region> getAllRegions(@RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.getAllRegions(schemaName);
    }
	

	@PostMapping("/get/all/{tagged}")
    public List<Region> getAllTagUnTaggedRegions(@PathVariable(value = "tagged") String variableMap, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.getAllTagUnTaggedRegions(variableMap,schemaName);
    }
	
	
	@PostMapping("/get/all/byname/{countryname}")
    public List<Region> getAllRegionsByCountryName(@PathVariable(value = "countryname") String countryname, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.getAllRegionsByCountryName(countryname,schemaName);
    }
	
	//Post request to get all regions  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Region> getAllRegionsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.getAllRegionsPaginated(pageNumber, pageSize, schemaName);
    }
	
	
    @PostMapping
    public Region createRegion(@Valid @RequestBody Region region) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
        return regionService.createRegion(region);
    }
    
    @PostMapping("/creates")
    public void createRegions(@Valid @RequestBody Region region) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		regionService.createRegions(region);
    }
    @PostMapping("/get/id/{id}")
    public Region getRegionById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.getRegionById(id, schemaName);
    }
	
    @PostMapping("/get/name/regionname/{name}")
    public Region getRegionByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.getRegionByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public Region updateRegion(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Region region) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.updateRegion(id, region);
    }
    @PostMapping("/updates/id//{id}")
    public Optional<Object> updateRegions(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Region region) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.updateRegions(id, region);
    }
  
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteRegion(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.deleteRegion(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteRegion(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.deleteSoftRegion(id, schemaName, timeUpdate);
    }
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Region> streamAllRegions() {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return regionService.streamAllRegions();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Region with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(RegionNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
    
    @PostMapping("/search/text/{text}/{pageNumber}/{pageSize}")
 	public List<Region> regionTextSearch(@Valid @PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize,@RequestHeader(value="schemaName") String schemaName){
 		
 		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
   		Constant.LOGGER.info("URL:"+baseUrl);
 		return regionService.regionTextSearch(text,schemaName,pageNumber,pageSize);
 	}
}
