package com.fidz.entr.base.repository;


import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.City;

import reactor.core.publisher.Mono;

@Repository
public interface CityInterface {
	Mono<City> saveName(City city);
}


