package com.fidz.entr.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum LoginMode {
 WEB_LOGIN, ANDROID_LOGIN, NON_ANDROID_LOGIN, MINIREPORT_LOGIN, ADMIN
}
