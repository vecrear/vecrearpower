package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.UserAppConfig;
@Repository
public interface UserAppConfigRepository extends MongoRepository<UserAppConfig, String>{

}
