package com.fidz.entr.base.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.TaggedRegions;
import com.fidz.entr.base.model.TaggedStates;
import com.fidz.entr.base.repository.CountryEntrBaseRepository;
import com.fidz.entr.base.repository.RegionRepository;



@Service("FEntrBaseCountryService")
public class CountryService {
	private GenericAppINterface<Country> genericINterface;
	@Autowired
	public CountryService(GenericAppINterface<Country> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private RegionService regionService;
	
	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private CountryEntrBaseRepository  countryRepository;
	
    private static String name = "Country";

	public List<Country> getAllCountries(String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCountries CountryService.java");
    	List<Country> countries=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	countries=this.genericINterface.findAll(Country.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCountries CountryService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCountries CountryService.java");
    	return countries;
		
	}
	public Country createCountry(Country country){
		Constant.LOGGER.info("Inside createCountry CountryService.java");
			String schemaName=country.getSchemaName();
	       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			 List<Country> countries = this.genericINterface.findAll(Country.class);
		    // List<Country> countries = this.countryRepository.findByName(country.getName());

	    	  List<String> names = countries.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("Application List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(country.getName())==true)){
	    			Constant.LOGGER.info("Inside if statement");
					throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+country.getName());
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(country.getName())==true)){
	    				Constant.LOGGER.info("Inside else if statement");
						throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+country.getName());
	    		}else{
	    			Constant.LOGGER.info("Successfull creation of Country CountryService.java");
	    			Constant.LOGGER.info("Inside else statement");
	    			
	    			 List<TaggedRegions> taggedregions = new ArrayList<TaggedRegions>();
	    		    	
	    		    	try {
	    		    		taggedregions = country.getTaggedRegions();
	    					Constant.LOGGER.info("taggedCities---size-"+taggedregions.size());
	    					Region  region =null;	
	    		    		for(int i = 0;i<taggedregions.size();i++)
	    		    		{
	    		    			region = regionService.getRegionById(taggedregions.get(i).getId(), schemaName);
	    		    			region.setId(taggedregions.get(i).getId());
	    		    			region.setRegionTagged(taggedregions.get(i).isRegionTagged());
	    		    			region.setTaggedCountryName(taggedregions.get(i).getTaggedCountryName());
	    		    			this.regionRepository.save(region);
	    		    			
	    		    		}
	    				
	    				}catch(Exception e) {
	    					//return null;
	    				}
	    			 
			 return  this.countryRepository.save(country);
	    		}
		 
	}
	public void createCountrys(Country country){
		 String schemaName=country.getSchemaName();
	 Constant.LOGGER.info("Inside createCountrys CountryService.java");
    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//return countryRepository.save(country);
		this.genericINterface.saveName(country);
	}
	
	public Country getCountryById(String id, String schemaName){
		 Constant.LOGGER.info("Inside getCountryById CountryService.java");
     MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Country.class);
		
		
	}
	
    public  Country updateCountry(String id, Country country) {
    	 String schemaName=country.getSchemaName();
		 Constant.LOGGER.info("Inside updateCountry CountryService.java");
	   MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 
		 List<TaggedRegions> taggedregions = new ArrayList<TaggedRegions>();
		 List<Country> countries = this.genericINterface.findAll(Country.class);
		 Country ctr = this.genericINterface.findById(id, Country.class);
		 countries.remove(ctr);
   		 Constant.LOGGER.info("Country List data"+countries);

   	     List<String> names = countries.stream().map(p->p.getName()).collect(Collectors.toList());
   		 Constant.LOGGER.info("Application List data"+names);
   	 
   		if (names.stream().anyMatch(s -> s.equals(country.getName())==true)){
   			Constant.LOGGER.info("Inside if statement");
				throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+country.getName());
   	 }
   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(country.getName())==true)){
   				Constant.LOGGER.info("Inside else if statement");
					throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+country.getName());
   		}else{
	    	try {
	    		taggedregions = country.getTaggedRegions();
				Constant.LOGGER.info("taggedCities---size-"+taggedregions.size());
				Region  region =null;	
	    		for(int i = 0;i<taggedregions.size();i++)
	    		{
	    			region = regionService.getRegionById(taggedregions.get(i).getId(), schemaName);
	    			region.setId(taggedregions.get(i).getId());
	    			region.setRegionTagged(taggedregions.get(i).isRegionTagged());
	    			region.setTaggedCountryName(taggedregions.get(i).getTaggedCountryName());
	    			this.regionRepository.save(region);
	    			
	    		}
			
			}catch(Exception e) {
				//return null;
			}
			 country.setId(id);

			 return this.countryRepository.save(country);

   		}
    	
    }

  
    
    //hard delete
	public Map<String, String> deleteCountry(String id, String schemaName) {
		 Constant.LOGGER.info("Inside deleteCountry CountryService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.countryRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Country deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftCountry(String id, String schemaName, TimeUpdate timeUpdate) {
		 Constant.LOGGER.info("Inside deleteSoftCountry CountryService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Country country = this.genericINterface.findById(id, Country.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		country.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		country.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		country.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(country);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Country deleted successfully");
		return response;

	}
    public List<Country> streamAllCountries() {
        return countryRepository.findAll();
    }
    
    //To get all countries paginated
    //page number starts from zero
	public List<Country> getAllCountriesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllCountriesPaginated CountryService.java");
    	Query query = new Query();
    	List<Country> countries =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	countries= this.genericINterface.find(query, Country.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllCountriesPaginated CountryService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllCountriesPaginated "+query);
        return countries;	
	}
	public List<Country> getAllTagUnTaggedCountry(String variableMap, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllTagUnTaggedCountry CountryService.java");
    	Query query = new Query();
    	List<Country> countries =null;

    	try {
    		if(variableMap.equalsIgnoreCase("tagged")) {
    			query.addCriteria(Criteria.where("countryTagged").in(true));
    		}else if(variableMap.equalsIgnoreCase("untagged"))
    		{
    			query.addCriteria(Criteria.where("countryTagged").in(false));
    		}
    	
    		countries= this.genericINterface.find(query, Country.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("inside getAllTagUnTaggedCountry CountryService.java "+ex.getMessage());
        }
        return countries;
	}
	
}
