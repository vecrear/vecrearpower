package com.fidz.entr.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.DeviceType;
import com.fidz.base.model.Status;
import com.fidz.base.model.Stream;

import lombok.Data;

@Data
@Document(collection = "device")
@JsonIgnoreProperties(value = { "target" })
public class Device extends com.fidz.base.model.Device {

	
	@DBRef(lazy = true)
	protected Company company;
	
	protected boolean deviceTagged;
	
	protected String taggedUserName;

}
