package com.fidz.entr.base.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.exception.RegionNotFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.ProductCategory;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.TaggedCities;
import com.fidz.entr.base.model.TaggedRegions;
import com.fidz.entr.base.model.TaggedStates;
import com.fidz.entr.base.repository.RegionRepository;
import com.fidz.entr.base.repository.StateRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FEBRegionService")
public class RegionService {

	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private GenericAppINterface<Region> genericINterface;
	
	@Autowired
	public RegionService(GenericAppINterface<Region> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	private static String name = "Region";

    public List<Region> getAllRegions(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllRegions RegionService.java");
    	List<Region> regions=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	regions=this.genericINterface.findAll(Region.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllRegions RegionService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllRegions RegionService.java");
    	return regions;
    }
	
    public Region createRegion(Region region) {
    
    		Constant.LOGGER.info(" Inside RegionService.java Value for insert Region record:createRegion :: " + region);
    		String schemaName=region.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	//List<Region> regions = this.genericINterface.findAll(Region.class);
        	//List<Region> regions = this.regionRepository.findByName(region.getName());
        	List<Region> regions =null;
	    	Query q = new Query();
	    	q.addCriteria(Criteria.where("name").regex(region.getName(),"i"));
	    	regions = this.mongoTemplate.find(q, Region.class);
        	List<TaggedStates> taggedStates = new ArrayList<TaggedStates>();
	    	  List<String> names = regions.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("Region List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(region.getName())==true)){
	    			Constant.LOGGER.info("Inside if statement");
	    			throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+region.getName());
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(region.getName())==true)){
	    				Constant.LOGGER.info("Inside else if statement");
		    		throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+region.getName());
	    		}else{
	    			Constant.LOGGER.info("Successfull creation of Region RegionService.java");
	    			Constant.LOGGER.info("Inside else statement");
	    			//this.genericINterface.saveName(region);	    		}
	    			
	    			
	    			try {
	    				taggedStates = region.getTaggedStates();
	    				Constant.LOGGER.info("taggedCities---size-"+taggedStates.size());
	    				State  state = null;	
	    	    		for(int i = 0;i<taggedStates.size();i++)
	    	    		{
	    	    			state = stateService.getStateById(taggedStates.get(i).getId(), schemaName);
	    	    			state.setId(taggedStates.get(i).getId());
	    	    			state.setStateTagged(taggedStates.get(i).isStateTagged());
	    	    			state.setTaggedRegionName(taggedStates.get(i).getTaggedRegionName());
	    	    			this.stateRepository.save(state);
	    	    			
	    	    		}
		    		
	    			}catch(Exception e) {
	    				//return null;
	    			}
                   return this.regionRepository.save(region);
	    		}
    	 
    }
    public void createRegions(Region region) {
    	try {
    		Constant.LOGGER.info(" Inside RegionService.java Value for insert Region record:createRegions :: " + region);
    		String schemaName=region.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	this.genericINterface.saveName(region);
    	}
    	catch (Exception e) {
			Constant.LOGGER.error("Exception inside RegionService.java create Regions::" + e.getMessage());
		}
    }
    
    public Region getRegionById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside getRegionById RegionService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, Region.class);
    }
	
    public Region getRegionByName(String name, String schemaName) {
    	Constant.LOGGER.info("Inside getRegionByName RegionService.java");
     MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.regionRepository.findByName(name);
		
    }
	
    public Region updateRegion(String id, Region region) {
    	Constant.LOGGER.info("Inside updateRegion RegionService.java");
     String schemaName=region.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	List<TaggedStates> taggedStates = new ArrayList<TaggedStates>();
    	//List<Region> regions = this.genericINterface.findAll(Region.class);
    	//List<Region> regions = this.regionRepository.findByName(region.getName());
    	List<Region> regions =null;
    	Query q = new Query();
    	q.addCriteria(Criteria.where("name").regex(region.getName(),"i"));
    	regions = this.mongoTemplate.find(q, Region.class);
    	Region ctr = getRegionById(id, schemaName);
    	  List<String> names = regions.stream().map(p->p.getName()).collect(Collectors.toList());
    		names.remove(ctr.getName());
    	  Constant.LOGGER.info("Region List data"+names);
    	 
    		if (names.stream().anyMatch(s -> s.equals(region.getName())==true)){
    			Constant.LOGGER.info("Inside if statement");
    			throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+region.getName());
    	 }
    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(region.getName())==true)){
    				Constant.LOGGER.info("Inside else if statement");
	    		throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+region.getName());
    		}else{
    			Constant.LOGGER.info("Successfull creation of Region RegionService.java");
    			Constant.LOGGER.info("Inside else statement");
    			//this.genericINterface.saveName(region);	    		}
    	
    	try {
			taggedStates = region.getTaggedStates();
			Constant.LOGGER.info("taggedCities---size-"+taggedStates.size());
			State  state =null;	
    		for(int i = 0;i<taggedStates.size();i++)
    		{
    			state = stateService.getStateById(taggedStates.get(i).getId(), schemaName);
    			state.setId(taggedStates.get(i).getId());
    			state.setStateTagged(taggedStates.get(i).isStateTagged());
    			state.setTaggedRegionName(taggedStates.get(i).getTaggedRegionName());
    			this.stateRepository.save(state);
    			
    		}
		
		}catch(Exception e) {
			//return null;
		}
    	region.setId(id);

    	return this.regionRepository.save(region);
    		}
    }

	public Optional<Object> updateRegions(String id, Region region) {
    	Constant.LOGGER.info("Inside updateRegions RegionService.java");
      String schemaName = region.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// /facility.setId(id);
		return this.regionRepository.findById(id).flatMap(existingApplication -> {
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			region.setId(id);
			return this.genericINterface.saveNames(region);
		});
    	
    }
	
	 //hard delete
		public Map<String, String> deleteRegion(String id, String schemaName) {
	    	Constant.LOGGER.info("Inside deleteRegion RegionService.java");
         MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			this.regionRepository.deleteById(id);
			Map<String, String> response = new HashMap<String, String>();
			response.put("message", "Region deleted successfully");
			return response;

		}
	     //soft delete
		public Map<String, String> deleteSoftRegion(String id, String schemaName, TimeUpdate timeUpdate) {
	    	Constant.LOGGER.info("Inside deleteSoftRegion RegionService.java");
         MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			Region region = this.genericINterface.findById(id, Region.class);
			// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
			region.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
			region.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
			region.setStatus(Status.INACTIVE);
			// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			this.genericINterface.saveName(region);

			Map<String, String> response = new HashMap<String, String>();
			response.put("message", "Region deleted successfully");
			return response;

		}

    public List<Region> streamAllRegions() {
        return regionRepository.findAll();
    }
    
    //To get all regions paginated
    //page number starts from zero
	public List<Region> getAllRegionsPaginated(int pageNumber, int pageSize, String schemaName) {
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllRegionsPaginated RegionService.java");
    	Query query = new Query();
    	List<Region> regions =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	regions= this.genericINterface.find(query, Region.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllRegionsPaginated RegionService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllRegionsPaginated ");
        return regions;	
	}
	public List<Region> getAllTagUnTaggedRegions(String variableMap, String schemaName) {
    	Constant.LOGGER.info("inside getAllTagUnTaggedRegions RegionService.java");
     MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Query query = new Query();
    	List<Region> regions =null;

    	try {
    		if(variableMap.equalsIgnoreCase("tagged")) {
    			query.addCriteria(Criteria.where("regionTagged").in(true));
    		}else if(variableMap.equalsIgnoreCase("untagged"))
    		{
    			query.addCriteria(Criteria.where("regionTagged").in(false));
    		}
    	
    		regions= this.genericINterface.find(query, Region.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error inside getAllTagUnTaggedRegions RegionService.java "+ex.getMessage());
        }
        return regions;	
	}
	public List<Region> getAllRegionsByCountryName(String countryname, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllRegionsByCountryName RegionService.java");
       
    	Query query1 = new Query();
    	Query query2 = new Query();
    	List<Region> regions =null;
    	List<Region> region =new ArrayList<Region>();

    	try {
    		
    		query1.addCriteria(Criteria.where("taggedCountryName").in(countryname));
    		regions= this.genericINterface.find(query1, Region.class);
    		region.addAll(regions);
    		
    		query2.addCriteria(Criteria.where("regionTagged").in(false));
    		regions= this.genericINterface.find(query2, Region.class);
    		region.addAll(regions);

    	}catch(Exception ex) {
	    Constant.LOGGER.error("inside getAllRegionsByCountryName RegionService.java "+ex.getMessage());
        }
        return region;
	}

	
	public List<Region> regionTextSearch(@Valid String text, String schemaName, int pageNumber, int pageSize) {
		List<Region>  region = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
       Constant.LOGGER.info("Inside regionTextSearch RegionService.java");
    	try {
    		
    		textIndex = new TextIndexDefinitionBuilder()
    				.onAllFields()
    				.build();
    		mongoTemplate.indexOps(Region.class).ensureIndex(textIndex);
    	    criteria = TextCriteria.forDefaultLanguage()
    	    		.matchingAny(text).caseSensitive(false).diacriticSensitive(false);
              query = TextQuery.queryText(criteria)
    	    		  .sortByScore()
    	    		  .with(PageRequest.of(pageNumber, pageSize));
              region = mongoTemplate.find(query, Region.class);
    		
    					
	}catch(Exception ex) {
    	Constant.LOGGER.error("Inside regionTextSearch RegionService.java"+ ex.getMessage());

	}
		return region;
	}
	
}
