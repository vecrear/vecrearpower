package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.ProductCategory;
import com.fidz.entr.base.service.ProductCategoryService;

@CrossOrigin(maxAge = 3600)
@RestController("FEBProductsCategoryController")
@RequestMapping(value="/productCategories")
public class ProductsCategoryController {

	
	@Autowired
	private ProductCategoryService productCategoryService; 
	
	
	@PostMapping("/get/all")
    public List<ProductCategory> getAllProductCategories(@RequestHeader(value="schemaName") String schemaName) {
    	return productCategoryService.getAllProductCategories(schemaName);
    }
	
	//Post request to get all product categories paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<ProductCategory> getAllProductCategoriesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return productCategoryService.getAllProductCategoriesPaginated(pageNumber, pageSize, schemaName);
	}
	
	@PostMapping
	public ProductCategory createProductCategory(@Valid @RequestBody ProductCategory productCategory) {
		return productCategoryService.createProductCategories(productCategory);
	}
	
	@PostMapping("/creates")
    public void createProductCategories(@Valid @RequestBody ProductCategory productCategory) {
		productCategoryService.createProductCategories(productCategory);
    }
	
	// get all the categories by id
	  @PostMapping("/get/id/{id}")
	    public ProductCategory getProductCategoriesById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
			return productCategoryService.productCategoriesById(id, schemaName);
	    }

	  
	  //get by name
	  @PostMapping("/get/name/productcategoryname/{name}")
	    public ProductCategory getProductCategoriesByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
			return productCategoryService.getProductCategoriesByName(name, schemaName);
	    }
	  
	  // updating the products 
	  
	  @PostMapping("/update/id/{id}")
	    public ProductCategory updateProductCategory(@PathVariable(value = "id") String id,
	                                                   @Valid @RequestBody ProductCategory productCategory) {
	        return productCategoryService.productCategories(id, productCategory);
	    }
	  
	  @PostMapping("/delete/id/{id}")
	    public Map<String, String> deleteProductCategory(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
	        return productCategoryService.deleteProductCategory(id, schemaName);
	    }
	    @PostMapping("/delete/soft/id/{id}")
	    public Map<String, String> softDeleteProductCategory(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
	        return productCategoryService.softDeleteProductCategory(id, schemaName, timeUpdate);
	    }
	    
	    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	    public List<ProductCategory> streamAllProductCategories() {
	        return productCategoryService.streamAllProductCategories();
	    }
	  
}
