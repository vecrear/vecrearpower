package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.UserHierarchyLevel;

@Repository
public interface UserHierarchyLevelRepository extends MongoRepository<UserHierarchyLevel, String>{
	//Mono<UserHierarchyLevel> findByName(String name);
	UserHierarchyLevel findByName(String name);
}
