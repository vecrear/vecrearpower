package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.Stream;

@Repository("FEBStreamRepository")
public interface StreamRepository extends MongoRepository<Stream, String>{
	//public Mono<Stream> findByName(String name);
	public Stream findByName(String name);
}
