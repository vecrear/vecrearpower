package com.fidz.entr.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DeviceNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 575100697457680893L;
	public DeviceNotFoundException(String device) {
		super("Device not found with id/name " + device);
	}
	public DeviceNotFoundException(String device, String data) {
		super(data);
	}
}
