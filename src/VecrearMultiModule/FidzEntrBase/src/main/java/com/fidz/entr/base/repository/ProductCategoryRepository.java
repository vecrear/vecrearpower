package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.ProductCategory;


public interface ProductCategoryRepository extends MongoRepository<ProductCategory, String> {
	
	public ProductCategory findByCategoryName(String categoryName);

}
