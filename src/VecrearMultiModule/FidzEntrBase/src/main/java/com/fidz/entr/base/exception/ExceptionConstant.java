package com.fidz.entr.base.exception;

public class ExceptionConstant {

	 public static final String SAME_NAME = " Name Already exists";
	 
	 public static final String SAME_NAME_DIFFERENT_CASE = " Name Already exists with different case";

	 public static final String SAME_NAME_SAME_DEPARTMENT = " Cannot use same Department in same Company";

	 public static final String SAME_NAME_SAME_USER_ROLE = " Cannot use same UserRole in same Department";

	 public static final String SAME_NAME_SAME_CUSTOMER_CATEGORY = " Cannot use same CustomerCategory in same Comapny";

	 public static final String SAME_NUMBER = " Phone Number already exists";

	 public static final String SAME_NAME_SAME_ACTIVITY_TYPE = " Cannot use same ActivityType in same Department";
	 
	 public static final String SAME_DEVICE = "Device Already exists";
	 
	 public static final String SAME_ACTIVITYFIELD_SAME_DEPARTMENT = " Cannot use same ActivityField in same Department";



}
