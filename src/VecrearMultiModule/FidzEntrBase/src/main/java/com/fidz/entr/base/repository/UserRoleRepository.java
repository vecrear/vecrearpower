package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.UserRole;



@Repository("FEntrBaseRepository")
public interface UserRoleRepository extends MongoRepository<UserRole, String>{
	//Mono<UserRole> findByName(String name);
	UserRole findByName(String name);
}
