package com.fidz.entr.base.validator;

import org.springframework.stereotype.Component;

import com.fidz.base.exception.ValidationException;
import com.fidz.base.validator.Validator;
import com.fidz.entr.base.model.City;
@Component
public class CityValidator implements Validator<City>{

	@Override
	public boolean doValidate(boolean mandatory, City value) throws ValidationException {
		// TODO Auto-generated method stub
		if((value.getName().isEmpty()) && (value.getName().contains(""))) {
			return false;
		}
		return true;
	}

	@Override
	public boolean doMandatoryCheck(City value) throws ValidationException {
		// TODO Auto-generated method stub
		return false;
	}

}
