package com.fidz.entr.base.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.index.Indexed;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class CustomField {
	@NotNull
	@Indexed(unique = true)
	protected String fieldName;
    protected DataType dataType;
    protected boolean mandatory;
    protected String pattern;
    protected Validation validation;
    protected List<Validation> dependencyValidations;                 
    protected Visibility visibility;
}
