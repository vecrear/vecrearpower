package com.fidz.entr.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum DataType {
	STRING, INTEGER, DOUBLE, LONG, PHONENUMBER, NAME, ADDRESS
}
