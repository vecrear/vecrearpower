package com.fidz.entr.base.exception;

public class UserAppConfigNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4859685532056747239L;
	
	public UserAppConfigNotFoundException(String userAppConfig) {
		super("UserAppConfig not found with id/name " + userAppConfig);
	}

}
