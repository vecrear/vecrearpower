package com.fidz.entr.base.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.City;

import reactor.core.publisher.Mono;

@Repository
public class CityInterfaceImpl implements CityInterface{
//public class GenericInterfaceImpl<TY> implements GenericINterface<TY>{
	private final ReactiveMongoTemplate reactiveMongoTemplate;
	
	
	@Autowired
	public CityInterfaceImpl(ReactiveMongoTemplate reactiveMongoTemplate) {
		
		this.reactiveMongoTemplate = reactiveMongoTemplate;
	}



	@Override
	public Mono<City> saveName(City city) {
		// TODO Auto-generated method stub
		return reactiveMongoTemplate.save(city);
	}
	
	/*@Autowired
    public GenericInterfaceImpl<TY>(ReactiveMongoTemplate reactiveMongoTemplate) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
    }*/
	
	/*@Override
	public Mono<TY> saveName(TY city) {
		// TODO Auto-generated method stub
		return reactiveMongoTemplate.save(city);

	}*/

}
