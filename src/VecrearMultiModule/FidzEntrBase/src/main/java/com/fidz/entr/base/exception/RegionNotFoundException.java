package com.fidz.entr.base.exception;

public class RegionNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5103846091102166521L;
	public RegionNotFoundException(String region) {
		super("Region not found with id/name " + region);
	}
}
