package com.fidz.entr.base.exception;

public class ProductCategoryNotFoundException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;
	
	public ProductCategoryNotFoundException(String product) {
		super("ProductCategory not found with id/name " + product);
	}
	public ProductCategoryNotFoundException(String product, String data) {
		super(data);
	}

}
