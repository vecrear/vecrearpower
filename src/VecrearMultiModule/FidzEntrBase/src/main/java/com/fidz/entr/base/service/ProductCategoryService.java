package com.fidz.entr.base.service;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.ProductCategory;
import com.fidz.entr.base.repository.ProductCategoryRepository;
import com.fidz.entr.base.repository.ProductRepository;

@Service("/FEBProductCategoryService")
public class ProductCategoryService {
	@Autowired
	private ProductCategoryRepository productCategoryRepository;
	
	
	private GenericAppINterface<ProductCategory> genericINterface;
	
	@Autowired
	public ProductCategoryService(GenericAppINterface<ProductCategory> genericINterface) {
		this.genericINterface=genericINterface;
	}
	
	

		public void createProductCategory(ProductCategory productCategory) {
			try {
				Constant.LOGGER.debug(" Inside ProductCategories.java Value for insert Product record :: " + productCategory);
				String schemaName = productCategory.getSchemaName();
				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
				// return companyRepository.save(company);
				this.genericINterface.saveName(productCategory);
				
			} catch (Exception e) {
				Constant.LOGGER.error("Exception inside ProductCategories.java create Product::" + e.getMessage());
			}
		}

	
	

	public ProductCategory createProductCategories(@Valid ProductCategory productCategory) {
		ProductCategory pCategory=null;
	    	try {
			Constant.LOGGER.debug(" Inside ProductCategories.java Value for insert ProductCategor record :: " + productCategory);
	    	String schemaName=productCategory.getSchemaName();
	    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	pCategory=this.productCategoryRepository.save(productCategory);
	    	}catch (Exception e) {
				Constant.LOGGER.error("Exception inside ProductCategories.java create ProductCategor::" + e.getMessage());
			}
	    	return pCategory;
	}


	public List<ProductCategory> getAllProductCategories(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllProductCategories ProductCategoryService.java");
    	List<ProductCategory> productCategories=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	productCategories=this.genericINterface.findAll(ProductCategory.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllProductCategories ProductCategoryService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllProductCategories ProductCategoryService.java");
    	return productCategories;
	}



	public ProductCategory productCategoriesById(String id, String schemaName) {
		// TODO Auto-generated method stub
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, ProductCategory.class);
	}



	public ProductCategory getProductCategoriesByName(String name, String schemaName) {
		// TODO Auto-generated method stub
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.productCategoryRepository.findByCategoryName(name);
	}



	public ProductCategory productCategories(String id, @Valid ProductCategory productCategory) {
		String schemaName = productCategory.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		productCategory.setId(id);
		return this.productCategoryRepository.save(productCategory);
	}



	public Map<String, String> deleteProductCategory(String id, String schemaName) {
		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     	 this.productCategoryRepository.deleteById(id);
	     	 Map<String, String> response = new HashMap<String, String>();
	     	    response.put("message", "productCategory deleted successfully");
	     	    return response;
	}



	public Map<String, String> softDeleteProductCategory(String id, String schemaName, @Valid TimeUpdate timeUpdate) {
		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		  ProductCategory productCategory=this.genericINterface.findById(id, ProductCategory.class);
	 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		  productCategory.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		  productCategory.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		  productCategory.setStatus(Status.INACTIVE);
	 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
	 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	 		  this.genericINterface.saveName(productCategory);
	 	    	 
	 	    	 Map<String, String> response = new HashMap<String, String>();
	 	    	    response.put("message", "ProductCategory deleted successfully");
	 	    	    return response;
	}



	public List<ProductCategory> streamAllProductCategories() {
		// TODO Auto-generated method stub
		 return productCategoryRepository.findAll();
	}


	 //To get all ProductCategory paginated
    //page number starts from zero
	public List<ProductCategory> getAllProductCategoriesPaginated(int pageNumber, int pageSize, String schemaName) {

		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllProductCategoriesPaginated ProductCategoryService.java");
    	Query query = new Query();
    	List<ProductCategory> productCategories =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	productCategories= this.genericINterface.find(query, ProductCategory.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllProductCategoriesPaginated ProductCategoryService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllProductCategoriesPaginated "+query);
        return productCategories;
	
	}
	

}
