package com.fidz.entr.base.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "country")
@JsonIgnoreProperties(value = { "target" })
public class Country extends Base {

	@Id
	protected String id;

	@NotBlank
	@Indexed(unique = true)
	protected String name;

	protected boolean countryTagged;
	
	protected String taggedcontinentName;

	@NotNull
	protected String code;

	protected String description;

	@DBRef(lazy = true)
	protected List<State> states;

	@DBRef(lazy = true)
	protected List<Region> regions;
	
	protected List<TaggedRegions> taggedRegions;


	



	
}
