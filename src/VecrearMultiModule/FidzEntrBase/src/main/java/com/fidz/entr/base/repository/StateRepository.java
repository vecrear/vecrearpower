package com.fidz.entr.base.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.State;

@Repository("FEBStateRepository")
public interface StateRepository extends MongoRepository<State, String>{

	//public Mono<State> findByName(String name);
	public State findByName(String name);

	//public State findByyName(String name);

	//public State findOne(String name);
}
