package com.fidz.entr.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "userhierarchylevel")
@JsonIgnoreProperties(value = { "target" })
public class UserHierarchyLevel extends Base {
	
	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	protected String description;
	
	protected int levelNo;
	
	@NotNull
	@DBRef(lazy = true)
	protected Company company;
	
	/**
	 * Department is required, if the user hierarchy is a department level hierarchy.
	 */
	@DBRef(lazy = true)
	protected Department department;
	
	/**
	 * Set to true if it is Department level hierarchy. By Default it is company level hierarchy.
	 * False means Company level hierarchy.
	 */
	protected boolean isDepartmentHierarchy = false;

	public UserHierarchyLevel(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name, String description, int levelNo,
			@NotNull Company company, Department department, boolean isDepartmentHierarchy) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.description = description;
		this.levelNo = levelNo;
		this.company = company;
		this.department = department;
		this.isDepartmentHierarchy = isDepartmentHierarchy;
	}

	@Override
	public String toString() {
		return "UserHierarchyLevel [id=" + id + ", name=" + name + ", description=" + description + ", levelNo="
				+ levelNo + ", company=" + company + ", department=" + department + ", isDepartmentHierarchy="
				+ isDepartmentHierarchy + ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + "]";
	}

	





	
}
