package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.entr.base.exception.StreamNotFoundException;
import com.fidz.entr.base.model.Stream;
import com.fidz.entr.base.payload.ErrorResponse;
import com.fidz.entr.base.service.StreamService;

@CrossOrigin(maxAge = 3600)
@RestController("FEBStreamController")
@RequestMapping(value="/streams")
public class StreamController {
	@Autowired
	private StreamService streamService;
	
	@PostMapping("/get/all")
    public List<Stream> getAllStreams(@RequestHeader(value="schemaName") String schemaName) {
    	return streamService.getAllStreams(schemaName);
    }

	//Post request to get all streams  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<Stream> getAllStreamsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return streamService.getAllStreamsPaginated(pageNumber, pageSize, schemaName);
    }
	
    @PostMapping
    public Stream createCompany(@Valid @RequestBody Stream stream) {
        return streamService.createStream(stream);
    }
    
    @PostMapping("/creates")
    public void createCompanys(@Valid @RequestBody Stream stream) {
        streamService.createStreams(stream);
    }
    
    @PostMapping("/get/id/{id}")
    public Stream getCompanyById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return streamService.getStreamById(id, schemaName);
    }
	
	@PostMapping("/get/name/streamname/{name}")
    public Stream getStreamByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return streamService.getStreamByName(name, schemaName);
    }
	
	@PostMapping("/update/id/{id}")
    public Stream updateStream(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Stream stream) {
        return streamService.updateStream(id, stream);
    }
	
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteStream(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return streamService.deleteStream(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteStream(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return streamService.deleteSoftStream(id, schemaName, timeUpdate);
    }
   
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Stream> streamAllStreams() {
        return streamService.streamAllStreams();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Stream with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(StreamNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
	
}
