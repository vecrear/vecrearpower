package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.Device;

@Repository("FEBDeviceRepository")
public interface DeviceRepository extends MongoRepository<Device, String>{
	
	//public Mono<Device> findByDeviceId(String deviceId);
	public Device findByDeviceId(String deviceId);
	public Device findByImei(String imei);
	
}
