package com.fidz.entr.base.repository;


import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.Company;

@Repository
public interface CompanyRepository extends MongoRepository<Company, String>{
	//public Mono<Company> findByName(String name);
	public Company findByName(String name);

	//public Company findByyName(String name);
}
