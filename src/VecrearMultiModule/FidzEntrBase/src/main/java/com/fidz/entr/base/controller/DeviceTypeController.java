package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.base.exception.DeviceTypeNotFoundException;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.DeviceType;
import com.fidz.entr.base.service.DeviceTypeService;



@CrossOrigin(maxAge = 3600)
@RestController("FEBDeviceTypeController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/devicetypes")
public class DeviceTypeController {

	@Autowired
	private DeviceTypeService deviceTypeService;
	
	@Value("${deviceservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all")
    public List<DeviceType> getAllDeviceTypes(@RequestHeader(value="schemaName") String schemaName) {
    	return deviceTypeService.getAllDeviceTypes(schemaName);
    }
	
	
	//----------getting the device types based on the company--------------------------//
	@PostMapping("/get/all/{companyid}")
    public List<DeviceType> getAllDeviceTypesByCompany(@PathVariable(value="companyid") String companyid,@RequestHeader(value="schemaName") String schemaName) {
    	return deviceTypeService.getAllDeviceTypesByCompany(companyid,schemaName);
    }
	//Post request to get all device types paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<DeviceType> getAllDeviceTypesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return deviceTypeService.getAllDeviceTypesPaginated(pageNumber, pageSize, schemaName);
	}
		
	
    @PostMapping
    public DeviceType createDeviceType(@Valid @RequestBody DeviceType deviceType) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return deviceTypeService.createDeviceType(deviceType);
    }
    
    @PostMapping("/creates")
    public void createDeviceTypes(@Valid @RequestBody DeviceType deviceType) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	deviceTypeService.createDeviceTypes(deviceType);
    }
    
    @PostMapping("/get/id/{id}")
    public DeviceType getDeviceTypeById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return deviceTypeService.getDeviceTypeById(id, schemaName);
    }
	
    @PostMapping("/get/name/devicetypename/{name}")
    public DeviceType getDeviceTypeByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return deviceTypeService.getDeviceTypeByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public DeviceType updateDeviceType(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody DeviceType deviceType) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return deviceTypeService.updateDeviceType(id, deviceType);
    }
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteDeviceType(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return deviceTypeService.deleteDeviceType(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteDeviceType(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return deviceTypeService.deleteSoftDeviceType(id, schemaName, timeUpdate);
    }
  
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<DeviceType> streamAllDeviceTypes() {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return deviceTypeService.streamAllDeviceTypes();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A DeviceType with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(DeviceTypeNotFoundException ex) {
    	System.out.println("property value******"+property);
		String message="Deviceservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.notFound().build();
    }
}
