package com.fidz.entr.base.exception;

public class CityNameFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3953944324844781157L;

	
	public CityNameFoundException(String city) {
		super("City name already exits: " + city);
	}
	
}
