package com.fidz.entr.base.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "target" })
public enum Visibility {
	WEB, MOBILE, BOTH, NONE
}
