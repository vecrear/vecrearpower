package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.CompanyNotFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.service.CompanyService;

@CrossOrigin(maxAge = 3600)
@RestController("FEBCompanyController")
@RequestMapping(value="/companies")
public class CompanyController {
	@Autowired
	private CompanyService companyService;
	
	
	@PostMapping("/get/all")
	//@Cacheable(value = "companies")
    public List<Company> getAllCompanies(@RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.getAllCompanies(schemaName);
    }
	
	//Post request to get all companies  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Company> getAllCompaniesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.getAllCompaniesPaginated(pageNumber, pageSize, schemaName);
    }
	
	
	
	@PostMapping
	public Company createCompany(@Valid @RequestBody Company company) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.createCompany(company);
	}
	
	@PostMapping("/creates")
    public void createCompanys(@Valid @RequestBody Company company) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		companyService.createCompanys(company);
    }
    
    @PostMapping("/get/id/{id}")
    public Company getCompanyById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.getCompanyById(id, schemaName);
    }
	
	@PostMapping("/get/name/companyname/{name}")
    public Company getCompanyByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.getCompanyByName(name, schemaName);
    }
	
	@PostMapping("/update/id/{id}")
    public Company updateCompany(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Company company) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.updateCompany(id, company);
    }
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteCompany(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return companyService.deleteCompany(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteCompany(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.deleteSoftCompany(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Company> streamAllCompanies() {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.streamAllCompanies();
    }
    
    @PostMapping("/search/text/{text}/{pageNumber}/{pageSize}")
	public List<Company> companyTextSearch(@Valid @PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize,@RequestHeader(value="schemaName") String schemaName){
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return companyService.companyTextSearch(text,schemaName,pageNumber,pageSize);
	}


    // Exception Handling Examples
   /* @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Company with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(CompanyNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/
}
