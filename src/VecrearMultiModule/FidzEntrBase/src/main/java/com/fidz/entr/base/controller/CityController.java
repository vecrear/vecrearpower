package com.fidz.entr.base.controller;

import java.util.List;

import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.CityNotFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.service.CityService;

@CrossOrigin(maxAge = 3600)
@RestController("FEBCityController")
@RequestMapping(value="/cities")
public class CityController {
	@Autowired
	private CityService cityService;
	
	@PostMapping("/get/all")
    public List<City> getAllCities(@RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.getAllCities(schemaName);
	}
	
	@PostMapping("/get/all/{tagged}")
    public List<City> getAllTagUnTaggedCities(@PathVariable(value = "tagged") String variableMap, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.getAllTagUnTaggedCities(variableMap,schemaName);
    }
	
	@PostMapping("/get/all/byname/{statename}")
    public List<City> getAllCitiesByStateName(@PathVariable(value = "statename") String statename, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.getAllCitiesByStateName(statename,schemaName);
    }
	
	
	
	//Post request to get all cities  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<City> getAllCitiesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.getAllCitiesPaginated(pageNumber, pageSize, schemaName);
	}
	
	@PostMapping("search/{text}/byname/{pageNumber}/{pageSize}")
	public List<City> getAllSearchCitiesPaginated(@PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.getAllSearchCitiesPaginated(pageNumber, pageSize, schemaName,text);
	}
		
		
	
    @PostMapping
    public City createCity(@Valid @RequestBody City city) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
        return cityService.createCity(city);
    }
    
    @PostMapping("/creates")
    public void createCitys(@Valid @RequestBody City city) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
       cityService.createCitys(city);
    }
    
    @PostMapping("/get/id/{id}")
    public City getCityById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.getCityById(id, schemaName);
    }
	
    @PostMapping("/get/name/cityname/{name}")
    public List<City> getCityByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.getCityByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public City updateCity(@PathVariable(value = "id") String id,@Valid @RequestBody City city) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.updateCity(id, city);
    }
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteCity(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.deleteCity(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteCity(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
		Constant.LOGGER.info("URL:"+baseUrl);
		return cityService.deleteSoftCity(id, schemaName, timeUpdate);
    }
   
    
     @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
     public List<City> streamAllCities() {
    	 final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
 		Constant.LOGGER.info("URL:"+baseUrl);
 		return cityService.streamAllCities();
    }

     @PostMapping("/search/text/{text}/{pageNumber}/{pageSize}")
 	public List<City> cityTextSearch(@Valid @PathVariable(value="text") String text,@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize,@RequestHeader(value="schemaName") String schemaName){
 		
 		final String baseUrl = ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString();
   		Constant.LOGGER.info("URL:"+baseUrl);
 		return cityService.cityTextSearch(text,schemaName,pageNumber,pageSize);
 	}

    // Exception Handling Examples
   @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A City with the same details already exists"));
    }
    
    

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(CityNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
