package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.fidz.entr.base.model.Product;


public interface ProductRepository extends MongoRepository<Product, String> {
	
	public Product findByName(String name);

}
