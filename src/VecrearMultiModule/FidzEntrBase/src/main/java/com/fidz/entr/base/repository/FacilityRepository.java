package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.Facility;

@Repository
public interface FacilityRepository extends MongoRepository<Facility, String>{
	//Mono<Facility> findByName(String name);
	Facility findByName(String name);
}
