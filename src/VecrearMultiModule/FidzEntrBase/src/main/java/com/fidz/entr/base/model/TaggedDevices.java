package com.fidz.entr.base.model;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class TaggedDevices {
	
	@Id
	protected String id;
	
	protected boolean deviceTagged;
	
	protected String taggedUserName;

	@Override
	public String toString() {
		return "TaggedDevices [id=" + id + ", deviceTagged=" + deviceTagged + ", taggedUserName=" + taggedUserName
				+ "]";
	}

	public TaggedDevices(String id, boolean deviceTagged, String taggedUserName) {
		super();
		this.id = id;
		this.deviceTagged = deviceTagged;
		this.taggedUserName = taggedUserName;
	}
	

}
