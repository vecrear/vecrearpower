package com.fidz.entr.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Application;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "companyappconfig")
@JsonIgnoreProperties(value = { "target" })
public class CompanyAppConfig extends Base {

	@Id
	protected String id;
	
	@NotNull
	@DBRef(lazy = true)
	protected Company company;
	

	@DBRef(lazy = true)
	protected Application application;

	@NotNull
	protected String defaultCountryCode;
	
	@NotNull
	protected String defaultLanguageCode;
	
	@NotNull
	protected String defaultTimeZone;

	public CompanyAppConfig(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull Company company, @NotNull Application application,
			@NotNull String defaultCountryCode, @NotNull String defaultLanguageCode, @NotNull String defaultTimeZone) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.company = company;
		this.application = application;
		this.defaultCountryCode = defaultCountryCode;
		this.defaultLanguageCode = defaultLanguageCode;
		this.defaultTimeZone = defaultTimeZone;
	}

	@Override
	public String toString() {
		return "CompanyAppConfig [id=" + id + ", company=" + company + ", application=" + application
				+ ", defaultCountryCode=" + defaultCountryCode + ", defaultLanguageCode=" + defaultLanguageCode
				+ ", defaultTimeZone=" + defaultTimeZone + ", createdTimestamp=" + createdTimestamp + ", createdByUser="
				+ createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser
				+ ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status
				+ ", schemaName=" + schemaName + "]";
	}


	

}
