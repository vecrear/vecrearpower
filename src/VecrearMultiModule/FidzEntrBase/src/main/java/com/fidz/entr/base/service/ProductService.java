package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.ProductCategory;
import com.fidz.entr.base.repository.ProductRepository;

@Service("/FEBProductService")
public class ProductService {
	@Autowired
	private ProductRepository productRepository;
	
	
	private GenericAppINterface<Product> genericINterface;
	
	@Autowired
	public ProductService(GenericAppINterface<Product> genericINterface) {
		this.genericINterface=genericINterface;
	}
	
	

		public void createProducts(Product product) {
			try {
				Constant.LOGGER.debug(" Inside ProductService.java Value for insert Product record :: " + product);
				String schemaName = product.getSchemaName();
				MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
				// return companyRepository.save(company);
				this.genericINterface.saveName(product);
				
			} catch (Exception e) {
				Constant.LOGGER.error("Exception inside ProductService.java create Product::" + e.getMessage());
			}
		}

	
	

	public Product createProduct(@Valid Product product) {
		 
			Constant.LOGGER.info(" Inside ProductService.java Value for insert product record :: " + product);
	    	String schemaName=product.getSchemaName();
	    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    	List<Product> products = this.genericINterface.findAll(Product.class);
	    	 
	    	  List<String> names = products.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("Facility List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(product.getName())==true)){
	    			Constant.LOGGER.info("Inside if statement");
	    			throw new NameFoundException(product.getName()+"Product Name Already exists"+"");
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(product.getName())==true)){
	    				Constant.LOGGER.info("Inside else if statement");
	    	  throw new NameFoundException(product.getName()+" Product Name Already exists "+"with different case");
	    		}else{
	    			Constant.LOGGER.info("Successfull creation of Product ProductService.java");
	    			Constant.LOGGER.info("Inside else statement");
	    			this.genericINterface.saveName(product);	    		}
  
  	
	    	return this.productRepository.save(product);
	    
	    	 
	}


	public List<Product> getAllProducts(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findAll(Product.class);
	}



	public Product productById(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Product.class);
	}



	public Product getProductByName(String name, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.productRepository.findByName(name);
	}



	public Product updateProduct(String id, @Valid Product product) {
		String schemaName = product.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		product.setId(id);
		return this.productRepository.save(product);
	}



	public Map<String, String> deleteProduct(String id, String schemaName) {
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	 this.productRepository.deleteById(id);
     	 Map<String, String> response = new HashMap<String, String>();
     	    response.put("message", "productCategory deleted successfully");
     	    return response;
	}



	public Map<String, String> softDeleteProduct(String id, String schemaName, @Valid TimeUpdate timeUpdate) {
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 Product product=this.genericINterface.findById(id, Product.class);
	 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		 product.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		 product.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		 product.setStatus(Status.INACTIVE);
	 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
	 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	 		  this.genericINterface.saveName(product);
	 	    	 
	 	    	 Map<String, String> response = new HashMap<String, String>();
	 	    	    response.put("message", "product deleted successfully");
	 	    	    return response;
	}



	public List<Product> streamAllProducts() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

    //To get all facilities paginated
    //page number starts from zero
	public List<Product> getAllProductsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllProductsPaginated ProductService.java");
    	Query query = new Query();
    	List<Product> products =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	products= this.genericINterface.find(query, Product.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllProductsPaginated ProductService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllProductsPaginated "+query);
        return products;
	
	}

}
