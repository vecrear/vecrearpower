package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.UserAppConfig;
import com.fidz.entr.base.model.UserHierarchyLevel;
import com.fidz.entr.base.repository.UserHierarchyLevelRepository;


@Service("FEBUserHierarchyLevelService")
public class UserHierarchyLevelService {

	@Autowired
	private UserHierarchyLevelRepository userHierarchyLevelRepository;
	
	private GenericAppINterface<UserHierarchyLevel> genericINterface;

	@Autowired
	public UserHierarchyLevelService(GenericAppINterface<UserHierarchyLevel> genericINterface) {
		this.genericINterface = genericINterface;
	}
	
    public List<UserHierarchyLevel> getAllUserHierachy(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllUserHierachy UserHierarchyLevelService.java");
    	List<UserHierarchyLevel> userHierarchyLevels=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	userHierarchyLevels=this.genericINterface.findAll(UserHierarchyLevel.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllUserHierachy UserHierarchyLevelService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllUserHierachy UserHierarchyLevelService.java");
    	return userHierarchyLevels;
    }
	
    public UserHierarchyLevel createUserHierachy(UserHierarchyLevel userHierarchyLevel) {
    	
    		Constant.LOGGER.debug(" Inside UserHierarchyLevelService.java Value for insert userHierarchyLevel record:createuserHierarchyLevel :: " + userHierarchyLevel);
    		String schemaName=userHierarchyLevel.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       
        	List<UserHierarchyLevel> userHierarchyLevels = this.genericINterface.findAll(UserHierarchyLevel.class);
	    	 
	    	  List<String> names = userHierarchyLevels.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("UserHierarchyLevel List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(userHierarchyLevel.getName())==true)){
	    			Constant.LOGGER.info("Inside if statement");
	    			throw new NameFoundException(userHierarchyLevel.getName()+"UserHierarchyLevel Name Already exists"+"");
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(userHierarchyLevel.getName())==true)){
	    				Constant.LOGGER.info("Inside else if statement");
	    	  throw new NameFoundException(userHierarchyLevel.getName()+" UserHierarchyLevel Name Already exists "+"with different case");
	    		}else{
	    			Constant.LOGGER.info("Successfull creation of UserHierarchyLevel UserHierarchyLevelService.java");
	    			Constant.LOGGER.info("Inside else statement");
	    		//	return this.genericINterface.saveName(state);	    		}

	
	    //	return this.stateRepository.save(state);
	    
        	//return this.stateRepository.save(state);
    	//	
	    		}    	
       return  this.userHierarchyLevelRepository.save(userHierarchyLevel);
    	
    	
    }
    
    public void createUserHierachys(UserHierarchyLevel userHierarchyLevel) {
    	String schemaName=userHierarchyLevel.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	this.genericINterface.saveName(userHierarchyLevel);
    }
    
    public UserHierarchyLevel getUserHierachyById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, UserHierarchyLevel.class);
		
    }
	
    public UserHierarchyLevel getUserHierachyByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.userHierarchyLevelRepository.findByName(name);
		
    }
	
   
    public UserHierarchyLevel updateUserHierachy(String id, UserHierarchyLevel userHierarchyLevel) {
    	String schemaName=userHierarchyLevel.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	userHierarchyLevel.setId(id);
    	return this.userHierarchyLevelRepository.save(userHierarchyLevel);
    }

 
    //hard delete
	public Map<String, String> deleteUserHierachy(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.userHierarchyLevelRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserHierarchyLevel deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftUserHierachy(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		UserHierarchyLevel userHierarchyLevel = this.genericINterface.findById(id, UserHierarchyLevel.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		userHierarchyLevel.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		userHierarchyLevel.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		userHierarchyLevel.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(userHierarchyLevel);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserHierarchyLevel deleted successfully");
		return response;

	}
    
    public List<UserHierarchyLevel> streamAllUserHierarchies() {
        return userHierarchyLevelRepository.findAll();
    }

    //To get all UserHierarchyLevel paginated
    //page number starts from zero
	public List<UserHierarchyLevel> getAllUserHierachyPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserHierachyPaginated UserHierarchyLevelService.java");
    	Query query = new Query();
    	List<UserHierarchyLevel> uHierarchyLevels =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	uHierarchyLevels= this.genericINterface.find(query, UserHierarchyLevel.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserHierachyPaginated UserHierarchyLevelService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllUserHierachyPaginated "+query);
        return uHierarchyLevels;	
	
	
	
	}

}
