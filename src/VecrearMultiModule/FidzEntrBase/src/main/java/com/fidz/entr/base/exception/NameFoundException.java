package com.fidz.entr.base.exception;

public class NameFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6239501510559592352L;
    public NameFoundException(String name) {
    	super(""+name);
    }
}
