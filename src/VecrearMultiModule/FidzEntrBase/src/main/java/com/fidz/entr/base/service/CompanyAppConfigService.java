package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.CompanyAppConfig;
import com.fidz.entr.base.repository.CompanyAppConfigRepository;


@Service("FEntrbaseCompanyAppConfigService")
public class CompanyAppConfigService {
	private GenericAppINterface<CompanyAppConfig> genericINterface;
	@Autowired
	public CompanyAppConfigService(GenericAppINterface<CompanyAppConfig> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private CompanyAppConfigRepository companyAppConfigRepository;
	
    public List<CompanyAppConfig> getAllCompanyAppConfigs(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCompanyAppConfigs CompanyAppConfigService.java");
    	List<CompanyAppConfig> companyAppConfigs=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	companyAppConfigs=this.genericINterface.findAll(CompanyAppConfig.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCompanyAppConfigs CompanyAppConfigService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCompanyAppConfigs CompanyAppConfigService.java");
    	return companyAppConfigs;
    	
    }
	
    public CompanyAppConfig createCompanyAppConfig(CompanyAppConfig companyAppConfig) {
    	CompanyAppConfig companyAppConfigs=null;
    	try {
    		Constant.LOGGER.debug(" Inside CompanyAppConfigService.java Value for insert companyAppConfig record:createCompanyAppConfig :: " + companyAppConfig);
    		String schemaName=companyAppConfig.getSchemaName();
        	
    	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	    companyAppConfigs=this.companyAppConfigRepository.save(companyAppConfig);
    	}catch(Exception e) {
    		Constant.LOGGER.debug(" Inside CompanyAppConfigService.java Value for insert companyAppConfig record:createCompanyAppConfig :: " + companyAppConfig);
    	}
       return companyAppConfigs;
    }
    
    public void createCompanyAppConfigs(CompanyAppConfig companyAppConfig) {
        String schemaName=companyAppConfig.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        //return companyAppConfigRepository.save(companyAppConfig);
	    this.genericINterface.saveName(companyAppConfig);
    }
    
    public CompanyAppConfig getCompanyAppConfigById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, CompanyAppConfig.class);
		
    }
	
    public CompanyAppConfig updateCompanyAppConfig(String id, CompanyAppConfig companyAppConfig) {
    	
        String schemaName=companyAppConfig.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    companyAppConfig.setId(id);
	    return this.companyAppConfigRepository.save(companyAppConfig);
       
    }

  
    //hard delete
   	public Map<String, String> deleteCompanyAppConfig(String id, String schemaName) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.companyAppConfigRepository.deleteById(id);
   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "CompanyAppConfig deleted successfully");
   		return response;

   	}
        //soft delete
   	public Map<String, String> deleteSoftCompanyAppConfig(String id, String schemaName, TimeUpdate timeUpdate) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		CompanyAppConfig companyAppConfig = this.genericINterface.findById(id, CompanyAppConfig.class);
   		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
   		companyAppConfig.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
   		companyAppConfig.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
   		companyAppConfig.setStatus(Status.INACTIVE);
   		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.genericINterface.saveName(companyAppConfig);

   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "CompanyAppConfig deleted successfully");
   		return response;

   	}
    
    public List<CompanyAppConfig> streamAllCompanyAppConfigs() {
        return companyAppConfigRepository.findAll();
    }

}
