package com.fidz.entr.base.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "state")
@JsonIgnoreProperties(value = { "target" })
public class State extends Base {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	protected boolean stateTagged;
	protected String taggedRegionName;
	
	protected String code;
	
	protected String description;
	
	@DBRef(lazy = true)
	protected List<City> cities;
	
	protected List<TaggedCities> taggedCities;

	
	

	

}
