package com.fidz.entr.base.model;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class TaggedCities {
	
	@Id
	protected String id;
	
	protected boolean cityTagged;
	
	protected String taggedStateName;

	@Override
	public String toString() {
		return "TaggedCities [id=" + id + ", cityTagged=" + cityTagged + ", taggedStateName=" + taggedStateName + "]";
	}

	public TaggedCities(String id, boolean cityTagged, String taggedStateName) {
		super();
		this.id = id;
		this.cityTagged = cityTagged;
		this.taggedStateName = taggedStateName;
	}
	
	
	

}