package com.fidz.entr.base.service;
import java.util.ArrayList;



import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.CityNameFoundException;
import com.fidz.entr.base.exception.CityNotFoundException;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.repository.CityRepository;

@Service("FEBCityService")
public class CityService {

	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private MongoTemplate mongoTemplate;
	private GenericAppINterface<City> genericINterface;
	@Autowired
	public CityService(GenericAppINterface<City> genericINterface){
		this.genericINterface=genericINterface;
	}
	
  public static String name = "City";
  
	public List<City> getAllCities(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCities CityService.java");
    	List<City> cities=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	cities=this.genericINterface.findAll(City.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCities CityService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCities CityService.java");
    	return cities;
	    	
	    	
	    }

	
	
	public City createCity(City city) {
		    Constant.LOGGER.info(" Inside CityService.java Value for insert City record :: " + city);
			String schemaName = city.getSchemaName();
		    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			List<City> cities = null;
	        //List<City> cities = this.cityRepository.findByName(city.getName());
	        Query q = new Query();
	    	q.addCriteria(Criteria.where("name").regex(city.getName(),"i"));
	    	cities = this.mongoTemplate.find(q, City.class);
			List<String> names = cities.stream().map(p->p.getName()).collect(Collectors.toList());
			Constant.LOGGER.info("Cities List data"+names);
            
			if (names.stream().anyMatch(s -> s.equals(city.getName())==true)) {
				Constant.LOGGER.info("Inside if statement");
				throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+city.getName());
            }
			else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(city.getName())==true)) {
					Constant.LOGGER.info("Inside else if statement");
		     throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+city.getName());
			}else{
				Constant.LOGGER.info("Inside else statement");
				return this.cityRepository.save(city);
            }
            }
	
    
	public void createCitys(City city) {
		try {
			Constant.LOGGER.info(" Inside CityService.java Value for insert City record :: " + city);
			String schemaName = city.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			this.cityRepository.save(city);
		 }  catch (Exception e) {
			Constant.LOGGER.error("Exception inside CityService.java createCity::" + e.getMessage());
		}
	}
	
    public City getCityById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside getCityById CityService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, City.class);
    }
	
    public List<City> getCityByName(String name, String schemaName) {
    	Constant.LOGGER.info("Inside getCityByName CityService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
       List<City> cities = null;
   	Query q = new Query();
   	q.addCriteria(Criteria.where("name").regex(name,"i"));
   	
   	
       cities = this.mongoTemplate.find(q, City.class);
//    	if((this.cityRepository.findByName(name).equals(null))) {
//    		throw new CityNotFoundException(name);
//    	}
//    	else {
//    		return this.cityRepository.findByName(name);
//    	}
       return cities;
		
    }
	
    public City updateCity(String id, City city) {
    	Constant.LOGGER.info("Inside updateCity CityService.java");
        String schemaName=city.getSchemaName(); 
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	//List<City> cities = this.genericINterface.findAll(City.class);
    	List<City> cities = null;
    	Query q = new Query();
    	q.addCriteria(Criteria.where("name").regex(city.getName(),"i"));
    	
        cities = this.mongoTemplate.find(q, City.class);
        City ctr = getCityById(id, schemaName);
		List<String> names = cities.stream().map(p->p.getName()).collect(Collectors.toList());
		names.remove(ctr.getName());
		Constant.LOGGER.info("Cities List data"+names);
        
		if (names.stream().anyMatch(s -> s.equals(city.getName())==true)) {
			Constant.LOGGER.info("Inside if statement");
			throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+city.getName());
        }
		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(city.getName())==true)) {
				Constant.LOGGER.info("Inside else if statement");
	     throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+city.getName());
		}else{
			Constant.LOGGER.info("Inside else statement");
    	city.setId(id);
    	return this.cityRepository.save(city);

		}
		
    	
    }
    //hard delete
    public Map<String, String> deleteCity(String  id, String schemaName) {
    	Constant.LOGGER.info("Inside deleteCity CityService.java");
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	 this.cityRepository.deleteById(id);
     	 Map<String, String> response = new HashMap<String, String>();
     	    response.put("message", "City deleted successfully");
     	    return response;
        
     }
     //soft delete
    public Map<String, String> deleteSoftCity(String  id, String schemaName, TimeUpdate timeUpdate) {
    	Constant.LOGGER.info("Inside deleteSoftCity CityService.java");
         MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		 City city=this.genericINterface.findById(id, City.class);
 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
 		 city.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
 		 city.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
 		 city.setStatus(Status.INACTIVE);
 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		  this.genericINterface.saveName(city);
 	    	 
 	    	 Map<String, String> response = new HashMap<String, String>();
 	    	    response.put("message", "City deleted successfully");
 	    	    return response;
 	       
 	    }
  
    
    public List<City> streamAllCities() {
        return cityRepository.findAll();
    }

//To get all cities paginated
	public List<City> getAllCitiesPaginated(int pageNumber, int pageSize, String schemaName) {
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllCitiesPaginated CityService.java");
    	Query query = new Query();
    	List<City> city =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	city= this.genericINterface.find(query, City.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllCitiesPaginated CityService.java "+ex.getMessage());
        }
        return city;
}


	public List<City> getAllTagUnTaggedCities(String variableMap, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllTagUnTaggedCities  CityService.java");
    	Query query = new Query();
    	List<City> cities =null;

    	try {
    		if(variableMap.equalsIgnoreCase("tagged")) {
    			query.addCriteria(Criteria.where("cityTagged").in(true));
    		}else if(variableMap.equalsIgnoreCase("untagged"))
    		{
    			query.addCriteria(Criteria.where("cityTagged").in(false));
    		}
    	
    		cities= this.genericINterface.find(query, City.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error inside City. City.java "+ex.getMessage());
        }
        return cities;	
	}


	public List<City> getAllCitiesByStateName(String statename, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside all getAllCitiesByStateName CityService.java");
    	Query query1 = new Query();
    	Query query2 = new Query();
    	List<City> city =new ArrayList<City>();
    	List<City> cities =null;

    	try {
    		query1.addCriteria(Criteria.where("taggedStateName").is(statename));
    		Constant.LOGGER.info("query-------------"+query1.toString());
    		
    		cities= this.genericINterface.find(query1, City.class);	
    		city.addAll(cities);
    		query2.addCriteria(Criteria.where("cityTagged").is(false));
    		Constant.LOGGER.info("query-------------"+query2.toString());
    		
    		cities= this.genericINterface.find(query2, City.class);	
    		city.addAll(cities);

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error inside City. City.java "+ex.getMessage());
        }
        return city;	
	}



	public List<City> cityTextSearch(@Valid String text, String schemaName, int pageNumber, int pageSize) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<City>  city = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
       Constant.LOGGER.info("Inside cityTextSearch CityService.java");
    	try {
    		
    		textIndex = new TextIndexDefinitionBuilder()
    				.onAllFields()
    				.build();
    		mongoTemplate.indexOps(City.class).ensureIndex(textIndex);
    	    criteria = TextCriteria.forDefaultLanguage()
    	    		.matchingAny(text).caseSensitive(false).diacriticSensitive(false);
              query = TextQuery.queryText(criteria)
    	    		  .sortByScore()
    	    		  .with(PageRequest.of(pageNumber, pageSize));
              city = mongoTemplate.find(query, City.class);
    		
    					
	}catch(Exception ex) {
    	Constant.LOGGER.error("Inside cityTextSearch CountryService.java"+ ex.getMessage());

	}
		return city;
	}



	public List<City> getAllSearchCitiesPaginated(int pageNumber, int pageSize, String schemaName, String text) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<City> cities = null;
		Query q = new Query();
				
try {
	q.addCriteria(Criteria.where("name").regex(text,"i"));
	final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
	q.with(pageableRequest);
	cities = this.genericINterface.find(q, City.class);
	
}catch(Exception ex) {
	Constant.LOGGER.info("Error occured inside getAllSearchCitiesPaginated" + ex.getLocalizedMessage());
}
		return cities;
	}
	

}
