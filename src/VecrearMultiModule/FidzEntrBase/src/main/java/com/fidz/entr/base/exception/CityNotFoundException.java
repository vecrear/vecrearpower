package com.fidz.entr.base.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="City Not Found")
public class CityNotFoundException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 683290246618434374L;

	public CityNotFoundException(String city) {
		super("City not found with id/name " + city);
	}
	
	
}
