package com.fidz.entr.base.model;
import java.util.Date;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Contact;
import com.fidz.base.model.PersonName;
import com.fidz.base.model.Role;
import com.fidz.base.model.Status;


import lombok.Data;

@Data
@Document(collection = "user")
@JsonIgnoreProperties(value = { "target" })
public class User extends com.fidz.base.model.User {
	
	@NotNull
	@DBRef(lazy = true)
	protected Company company;
	@NotNull
	@DBRef(lazy = true)
	protected Department department;
	
	@DBRef(lazy = true)
	protected UserHierarchyLevel userHierarchyLevel;
	
	protected String notificationId;

	public User() {
		// TODO Auto-generated constructor stub
	}


	@Override
	public String toString() {
		return "User [company=" + company + ", department=" + department + ", userHierarchyLevel=" + userHierarchyLevel
				+ ", notificationId=" + notificationId + ", id=" + id + ", userName=" + userName + ", password="
				+ password + ", name=" + name + ", address=" + address + ", contact=" + contact
				+ ", emergencyContactName=" + emergencyContactName + ", emergencyContact=" + emergencyContact
				+ ", appRoles=" + appRoles + ", createdTimestamp=" + createdTimestamp + ", createdByUser="
				+ createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser
				+ ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status
				+ ", schemaName=" + schemaName + "]";
	}






	public User(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, String userName, String password, PersonName name, Address address,
			Contact contact, PersonName emergencyContactName, Contact emergencyContact, List<Role> appRoles,
			@NotNull Company company, @NotNull Department department, UserHierarchyLevel userHierarchyLevel,
			String notificationId) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName, id, userName, password, name, address, contact, emergencyContactName, emergencyContact,
				appRoles);
		this.company = company;
		this.department = department;
		this.userHierarchyLevel = userHierarchyLevel;
		this.notificationId = notificationId;
	}












	
	

	



	

	
	
}
