package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.DeviceType;
import com.fidz.entr.base.repository.DeviceTypeRepository;


@Service("FEBDeviceTypeService")
public class DeviceTypeService {
	@Autowired
	private DeviceTypeRepository deviceTypeRepository;
	
	private GenericAppINterface<DeviceType> genericINterface;
	@Autowired
	public DeviceTypeService(GenericAppINterface<DeviceType> genericINterface){
		this.genericINterface=genericINterface;
	}
	
    public List<DeviceType> getAllDeviceTypes(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDeviceTypes DeviceTypeService.java");
    	List<DeviceType> deviceTypes=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	deviceTypes=this.genericINterface.findAll(DeviceType.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllDeviceTypes DeviceTypeService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllDeviceTypes DeviceTypeService.java");
    	return deviceTypes;
    }
	
    public DeviceType createDeviceType(DeviceType deviceType) {
    	String schemaName=deviceType.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.deviceTypeRepository.save(deviceType);
        
    }
    public void createDeviceTypes(DeviceType deviceType) {
    	String schemaName=deviceType.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	this.genericINterface.saveName(deviceType);
    }
    
    public DeviceType getDeviceTypeById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, DeviceType.class);
		
    }
	
    public DeviceType getDeviceTypeByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.deviceTypeRepository.findByName(name);
		
    }
	
    public DeviceType updateDeviceType(String id, DeviceType deviceType) {
    	String schemaName=deviceType.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	deviceType.setId(id);
    	return this.deviceTypeRepository.save(deviceType);
       
    }
    //hard delete
	public Map<String, String> deleteDeviceType(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.deviceTypeRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Device Type deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftDeviceType(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		DeviceType deviceType = this.genericINterface.findById(id, DeviceType.class);
		deviceType.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		deviceType.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		deviceType.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(deviceType);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Device Type deleted successfully");
		return response;

	}
   

    public List<DeviceType> streamAllDeviceTypes() {
        return deviceTypeRepository.findAll();
    }
    
    //To get all device types paginated
    //page number starts from zero
	public List<DeviceType> getAllDeviceTypesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllDeviceTypesPaginated DeviceTypeService.java");
    	Query query = new Query();
    	List<DeviceType> deviceTypes =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	deviceTypes= this.genericINterface.find(query, DeviceType.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllDeviceTypesPaginated DeviceTypeService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllDeviceTypesPaginated "+query);
        return deviceTypes;
	
	}

	public List<DeviceType> getAllDeviceTypesByCompany(String companyid, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDeviceTypes DeviceTypeService.java");
    	List<DeviceType> deviceTypes=null;
    	try {
    		Query q = new Query();
    		q.addCriteria(Criteria.where("company.id").in(companyid));
    		deviceTypes= this.genericINterface.find(q, DeviceType.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllDeviceTypes DeviceTypeService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllDeviceTypes DeviceTypeService.java");
    	return deviceTypes;
	}
}
