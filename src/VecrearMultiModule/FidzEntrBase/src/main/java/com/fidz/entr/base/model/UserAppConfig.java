package com.fidz.entr.base.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Application;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;

import lombok.Data;

@Data
@Document(collection = "userappconfig")
@JsonIgnoreProperties(value = { "target" })
public class UserAppConfig extends Base {
	
	@Id
	protected String id;

	
	@DBRef(lazy = true)
	protected User user;
	
	
	@DBRef(lazy = true)
	protected Application application;
	
	
	@DBRef(lazy = true)
	protected Company company;
	
	@NotNull
	protected String defaultLanguageCode;
	
	@NotNull
	protected String defaultTimeZone;

	public UserAppConfig(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull User user, @NotNull Application application,
			@NotNull Company company, @NotNull String defaultLanguageCode, @NotNull String defaultTimeZone) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.user = user;
		this.application = application;
		this.company = company;
		this.defaultLanguageCode = defaultLanguageCode;
		this.defaultTimeZone = defaultTimeZone;
	}

	@Override
	public String toString() {
		return "UserAppConfig [id=" + id + ", user=" + user + ", application=" + application + ", company=" + company
				+ ", defaultLanguageCode=" + defaultLanguageCode + ", defaultTimeZone=" + defaultTimeZone
				+ ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp="
				+ updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp
				+ ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	


	
}
