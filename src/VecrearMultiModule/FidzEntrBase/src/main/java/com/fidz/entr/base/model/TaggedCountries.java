package com.fidz.entr.base.model;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
@Data
@JsonIgnoreProperties(value = { "target" })
public class TaggedCountries {
	@Id
	protected String id;
	
	protected boolean countryTagged;
	
	protected String taggedcontinentName;
}
