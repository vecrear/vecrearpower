package com.fidz.entr.base.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.City;


//@Repository
public interface CityRepository extends MongoRepository<City, String>{
	//Mono<City> findByName(String name);
	List<City> findByName(String name);

	//City findByyName(String name);
	
	
}
