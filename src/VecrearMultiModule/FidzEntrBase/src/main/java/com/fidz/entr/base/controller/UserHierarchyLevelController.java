package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.base.exception.UserHierachyLevelNotFoundException;
import com.fidz.entr.base.model.Stream;
import com.fidz.entr.base.model.UserHierarchyLevel;
import com.fidz.entr.base.repository.UserHierarchyLevelRepository;
import com.fidz.entr.base.service.UserHierarchyLevelService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEBUserHierarchyLevelController")
@RequestMapping(value="/userhierachylevels")
public class UserHierarchyLevelController {
	@Autowired
	private UserHierarchyLevelService userHierarchyLevelService;
	
	@PostMapping("/get/all")
    public List<UserHierarchyLevel> getAllUserHierachy(@RequestHeader(value="schemaName") String schemaName) {
    	return userHierarchyLevelService.getAllUserHierachy(schemaName);
    }
	
	
	//Post request to get all UserHierarchyLevel  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<UserHierarchyLevel> getAllUserHierachyPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return userHierarchyLevelService.getAllUserHierachyPaginated(pageNumber, pageSize, schemaName);
    }
	
    @PostMapping
    public UserHierarchyLevel createUserHierachy(@Valid @RequestBody UserHierarchyLevel userHierarchyLevel) {
        return userHierarchyLevelService.createUserHierachy(userHierarchyLevel);
    }
    @PostMapping("/creates")
    public void createUserHierachys(@Valid @RequestBody UserHierarchyLevel userHierarchyLevel) {
        userHierarchyLevelService.createUserHierachy(userHierarchyLevel);
    }
    
    @PostMapping("/get/id/{id}")
    public UserHierarchyLevel getUserHierachyById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return userHierarchyLevelService.getUserHierachyById(id, schemaName);
    }
	
    @PostMapping("/get/name/userHierachy/{name}")
    public UserHierarchyLevel getUserHierachyByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return userHierarchyLevelService.getUserHierachyByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public UserHierarchyLevel updateUserHierachy(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody UserHierarchyLevel userHierarchyLevel) {
        return userHierarchyLevelService.updateUserHierachy(id, userHierarchyLevel);
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteUserHierarchy(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return userHierarchyLevelService.deleteUserHierachy(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteUserHierarchy(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return userHierarchyLevelService.deleteSoftUserHierachy(id, schemaName, timeUpdate);
    }
   
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<UserHierarchyLevel> streamAllUserHierarchies() {
        return userHierarchyLevelService.streamAllUserHierarchies();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Company with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(UserHierachyLevelNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
