package com.fidz.entr.base.model;

import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "ProductCategory")
@JsonIgnoreProperties(value = { "target" })
public class ProductCategory extends Base {
	
	@Id
	protected String id;
	
	@NotNull
	protected String categoryName;
	
	@NotNull
	protected String type;
	
	protected String details;

	public ProductCategory(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName,String id, @NotNull String categoryName, @NotNull String type, String details) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		// TODO Auto-generated constructor stub
		this.id = id;
		this.categoryName = categoryName;
		this.type = type;
		this.details = details;
	}

	@Override
	public String toString() {
		return "ProductCategory [id=" + id + ", categoryName=" + categoryName + ", type=" + type + ", details="
				+ details + ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + "]";
	}

	
	
	
	
	
	
	

}
