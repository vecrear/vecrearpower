package com.fidz.entr.base.service;
import java.util.HashMap;


import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition.TextIndexDefinitionBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.ApplicationNameFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.CompanyAppConfig;
import com.fidz.entr.base.repository.CompanyRepository;

@Service("FEBCompanyService")
public class CompanyService {
	@Autowired
	private CompanyRepository companyRepository;
	
	private GenericAppINterface<Company> genericINterface;
	@Autowired
	
	public CompanyService(GenericAppINterface<Company> genericINterface){
		this.genericINterface=genericINterface;
	}
	@Autowired
	private MongoTemplate mongoTemplate;
    
	private static String name = "Company";
	  
    public List<Company> getAllCompanies(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCompanies CompanyService.java");
    	List<Company> companies=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	companies=this.genericINterface.findAll(Company.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCompanies CompanyService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCompanies CompanyService.java");
    	return companies;
    }
	
    public Company createCompany(Company company) {
    	Constant.LOGGER.info("Inside createCompany CompanyService.java ");
    	String schemaName=company.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 //List<Company> companies = this.genericINterface.findAll(Company.class);
	     //List<Company> companies = this.companyRepository.findByName(company.getName());
	     List<Company> companies =null;
	    	Query q = new Query();
	    	q.addCriteria(Criteria.where("name").regex(company.getName(),"i"));
	    	companies = this.mongoTemplate.find(q, Company.class);
    	  List<String> names = companies.stream().map(p->p.getName()).collect(Collectors.toList());
    		Constant.LOGGER.info("Application List data"+names);
    	 
    		if (names.stream().anyMatch(s -> s.equals(company.getName())==true)) {
    			Constant.LOGGER.info("Inside if statement");
    			throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+company.getName());    	
    			}
    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(company.getName())==true)) {
    				Constant.LOGGER.info("Inside else if statement");
        		throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+company.getName());    	
    		}else{
    			Constant.LOGGER.info("Successfull creation of Company CompanyService.java");
    			Constant.LOGGER.info("Inside else statement");
    			return this.companyRepository.save(company);
    	 }
     }

	public void createCompanys(Company company) {
		try {
			Constant.LOGGER.info(" Inside CompanyService.java Value for insert Company record :: " + company);
			String schemaName = company.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			// return companyRepository.save(company);
			this.genericINterface.saveName(company);
		} catch (Exception e) {
			Constant.LOGGER.error("Exception inside CompanyService.java create Company::" + e.getMessage());
		}
	}

	public Company getCompanyById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside getCompanyById CompanyService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, Company.class);
	}

    public Company getCompanyByName(String name, String schemaName) {
    	Constant.LOGGER.info("Inside getCompanyByName CompanyService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.companyRepository.findByName(name);
    }
	
    public Company updateCompany(String id, Company company) {
    	Constant.LOGGER.info("Inside updateCompany CompanyService.java");
       String schemaName = company.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 //List<Company> companies = this.genericINterface.findAll(Company.class);
	     List<Company> companies =null;
	    	Query q = new Query();
	    	q.addCriteria(Criteria.where("name").regex(company.getName(),"i"));
	    	companies = this.mongoTemplate.find(q, Company.class);
		 Company ctr = getCompanyById(id, schemaName);
   	  List<String> names = companies.stream().map(p->p.getName()).collect(Collectors.toList());
   		names.remove(ctr.getName());
   	  Constant.LOGGER.info("Application List data"+names);
   	 
   		if (names.stream().anyMatch(s -> s.equals(company.getName())==true)) {
   			Constant.LOGGER.info("Inside if statement");
   			throw new NameFoundException(name+ExceptionConstant.SAME_NAME+": "+company.getName());    	
   			}
   		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(company.getName())==true)) {
   				Constant.LOGGER.info("Inside else if statement");
       		throw new NameFoundException(name+ExceptionConstant.SAME_NAME_DIFFERENT_CASE+": "+company.getName());    	
   		}else{
		company.setId(id);
		return this.companyRepository.save(company);
   		}
    }

    //hard delete
    public Map<String, String> deleteCompany(String  id, String schemaName) {
    	Constant.LOGGER.info("Inside deleteCompany CompanyService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	 this.companyRepository.deleteById(id);
     	 Map<String, String> response = new HashMap<String, String>();
     	    response.put("message", "Company deleted successfully");
     	    return response;
        
     }
     //soft delete
    public Map<String, String> deleteSoftCompany(String  id, String schemaName, TimeUpdate timeUpdate) {
    	Constant.LOGGER.info("Inside deleteSoftCompany CompanyService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		 Company company=this.genericINterface.findById(id, Company.class);
 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
 		company.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
 		company.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
 		company.setStatus(Status.INACTIVE);
 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		  this.genericINterface.saveName(company);
 	    	 
 	    	 Map<String, String> response = new HashMap<String, String>();
 	    	    response.put("message", "Company deleted successfully");
 	    	    return response;
 	       
 	    }


    public List<Company> streamAllCompanies() {
        return companyRepository.findAll();
    }
    
    //To get all companies paginated
	public List<Company> getAllCompaniesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllCompaniesPaginated CompanyService.java");
    	Query query = new Query();
    	List<Company> companies =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	companies= this.genericINterface.find(query, Company.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllCompaniesPaginated CompanyService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllCompaniesPaginated "+query);

        return companies;	}
	
	/**
	 * 
	 * @param text : String value to be searched in company list
	 * @param schemaName : company name/client name
	 * @param pageNumber : starts with 0
	 * @param pageSize : no. of objects/rows to be sent
	 * @return : list of companies matching the text string
	 */
	public List<Company> companyTextSearch(@Valid String text,
			String schemaName,int pageNumber,int pageSize) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		List<Company>  com = null;
		TextIndexDefinition textIndex = null;
		TextCriteria criteria = null;
		Query query = null;
       Constant.LOGGER.info("Inside companyTextSearch CompanyService.java");
    	try {
    		
    		textIndex = new TextIndexDefinitionBuilder()
    				.onAllFields()
    				.build();
    		mongoTemplate.indexOps(Company.class).ensureIndex(textIndex);
    	    criteria = TextCriteria.forDefaultLanguage()
    	    		.matchingAny(text).caseSensitive(false).diacriticSensitive(false);
              query = TextQuery.queryText(criteria)
    	    		  .sortByScore()
    	    		  .with(PageRequest.of(pageNumber, pageSize));
    		com = mongoTemplate.find(query, Company.class);
    		
    					
	}catch(Exception ex) {
    	Constant.LOGGER.error("Inside companyTextSearch CompanyService.java"+ ex.getMessage());

	}
		return com;
	}
}
