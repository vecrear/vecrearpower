package com.fidz.entr.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fidz.entr.base.model.DeviceType;

@Repository("FEBDeviceTypeRepository")
public interface DeviceTypeRepository extends MongoRepository<DeviceType, String>{
	//public Mono<DeviceType> findByName(String name);
	public DeviceType findByName(String name);
}
