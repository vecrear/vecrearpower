package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.repository.GenericBaseINterface;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.UserAppConfigNotFoundException;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.Stream;
import com.fidz.entr.base.model.UserAppConfig;
import com.fidz.entr.base.repository.UserAppConfigRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FEAUserAppConfigService")
public class UserAppConfigService {

	@Autowired
	private UserAppConfigRepository userAppConfigRepository;
	
	private GenericAppINterface<UserAppConfig> genericINterface;

	@Autowired
	public UserAppConfigService(GenericAppINterface<UserAppConfig> genericINterface) {
		this.genericINterface = genericINterface;
	}
    public List<UserAppConfig> getAllUserAppConfigs(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllUserAppConfigs UserAppConfigService.java");
    	List<UserAppConfig> userAppConfigs=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	userAppConfigs=this.genericINterface.findAll(UserAppConfig.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllUserAppConfigs UserAppConfigService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllUserAppConfigs UserAppConfigService.java");
    	return userAppConfigs;
    }
	
    public UserAppConfig createUserAppConfig(UserAppConfig userAppConfig) {
    	UserAppConfig userAppConfigs=null;
    	try {
    		Constant.LOGGER.debug(" Inside UserAppConfigService.java Value for insert userAppConfig record: createuserAppConfig  :: " + userAppConfig);
    		String schemaName=userAppConfig.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	userAppConfigs=this.userAppConfigRepository.save(userAppConfig);
    	}catch(Exception e) {
    		Constant.LOGGER.debug(" Inside UserAppConfigService.java Value for insert userAppConfig record: createuserAppConfig  :: " + userAppConfig);
    	}
    	return userAppConfigs;
    }
    public void createUserAppConfigs(UserAppConfig userAppConfig) {
    	String schemaName=userAppConfig.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	this.genericINterface.saveName(userAppConfig);
    }
    
    public UserAppConfig getUserAppConfigById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, UserAppConfig.class);
    }
	
    
    public UserAppConfig updateUserAppConfig(String id, UserAppConfig userAppConfig) {
    	String schemaName=userAppConfig.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	userAppConfig.setId(id);
    	return this.userAppConfigRepository.save(userAppConfig);
        
    }

  
    //hard delete
  	public Map<String, String> deleteUserAppConfig(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.userAppConfigRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "UserAppConfig deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftUserAppConfig(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		UserAppConfig userAppConfig = this.genericINterface.findById(id, UserAppConfig.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		userAppConfig.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		userAppConfig.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		userAppConfig.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(userAppConfig);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "UserAppConfig deleted successfully");
  		return response;

  	}
    public List<UserAppConfig> streamAllUserAppConfigs() {
        return this.userAppConfigRepository.findAll();
    }
    
    //To get all UserAppConfig paginated
    //page number starts from zero
	public List<UserAppConfig> getAllUserAppConfigsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserAppConfigsPaginated UserAppConfigService.java");
    	Query query = new Query();
    	List<UserAppConfig> userAppConfigs =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	userAppConfigs= this.genericINterface.find(query, UserAppConfig.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserAppConfigsPaginated UserAppConfigService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllUserAppConfigsPaginated "+query);
        return userAppConfigs;	
	
	}
	
	
}
