package com.fidz.entr.base.service;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.model.Topic;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.ExceptionConstant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.repository.DeviceRepository;


@Service("FEBDeviceService")
public class DeviceService {

	@Autowired
	private DeviceRepository deviceRepository;
	
	private GenericAppINterface<Device> genericINterface;
	@Autowired
	public DeviceService(GenericAppINterface<Device> genericINterface){
		this.genericINterface=genericINterface;
	}
	
	
    public List<Device> getAllDeviceData(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDeviceData DeviceService.java");
    	List<Device> devices=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	devices=this.genericINterface.findAll(Device.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllDeviceData DeviceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllDeviceData DeviceService.java");
    	return devices;
		
    }
	
    public Device createDevice(Device device) {
    	Constant.LOGGER.info("Inside createDevice DeviceService.java");
         String schemaName=device.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	String DeviceId=device.getDeviceId();
    
    	Device dev=getDeviceDataByDeviceId(DeviceId, schemaName);
    	
    	if(dev != null) {
    		System.out.println("inside iffffffff");
    		throw new NameFoundException(ExceptionConstant.SAME_DEVICE+": "+device.getDeviceId());    	
    		
    	}else {
    		System.out.println("inside elseeeee");
    		return this.deviceRepository.save(device);
    	}
    	 //return this.deviceRepository.save(device);
        //return deviceRepository.save(device);
    }
    public void createDevices(@Valid @RequestBody Device device) {
    	String schemaName=device.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	String DeviceId=device.getDeviceId();
    	Device dev=getDeviceDataByDeviceId(DeviceId, schemaName);
    	
    	if(dev!= null) {
    		throw new NameFoundException(ExceptionConstant.SAME_DEVICE+": "+device.getDeviceId());    	
    	}else {
    		this.deviceRepository.save(device);
    	}
        //this.genericINterface.saveName(device);
    }
    public Device getDeviceDataById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside getDeviceDataById DeviceService.java");
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, Device.class);
		//return deviceRepository.findById(id);
    }
    public Object getCurrentDataById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside getCurrentDataById DeviceService.java");
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Device deviceMono=this.genericINterface.findById(id, Device.class);
	    Device device = null;
		if (deviceMono != null) {
			device = deviceMono;
		}
		return device.getCurrentData();
		
	}
    
  //update the current device data
  	public  Device updateCurrentData(String id, Object currentData, String schemaName) {
    	Constant.LOGGER.info("Inside updateCurrentData DeviceService.java");
     MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		
  		Device deviceMono=this.genericINterface.findById(id, Device.class);
  	    Device device = null;
  		if (deviceMono != null) {
  			device = deviceMono;
  		}
  		device.setCurrentData(currentData);
  		return this.deviceRepository.save(device);
  		
  	}
   /// patch request for device
  
  	
	public  Device updateCurrentDataDeviceId(String deviceid, Object deviceHardwareDetails, String schemaName) {
    	Constant.LOGGER.info("Inside updateCurrentDataDeviceId DeviceService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device deviceMono=this.deviceRepository.findByDeviceId(deviceid);
  	    Device device = null;
  		if (deviceMono != null) {
  			device = deviceMono;
  		}
  		device.setDeviceHardwareDetails(deviceHardwareDetails);
  		return this.deviceRepository.save(device);
  		
  	}
	
    public Device updateDeviceData(String id, Device device) {
    Constant.LOGGER.info("Inside updateDeviceData DeviceService.java");
    String schemaName=device.getSchemaName();
    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    Device dev=getDeviceDataByDeviceId(device.getDeviceId(), schemaName);
	
	if(dev != null && (id.equals(dev.getId())==false)) {
		System.out.println("inside iffffffff");
		throw new NameFoundException(ExceptionConstant.SAME_DEVICE+": "+device.getDeviceId());

	}else {
	    return this.deviceRepository.save(device);

	}
    
    }
    
    
    //hard delete
    public Map<String, String> deleteDevice(String  id, String schemaName) {
    	Constant.LOGGER.info("Inside deleteDevice DeviceService.java");
       MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
     	 this.deviceRepository.deleteById(id);
     	 Map<String, String> response = new HashMap<String, String>();
     	    response.put("message", "Device deleted successfully");
     	    return response;
        
     }
     //soft delete
    public Map<String, String> deleteSoftCompany(String  id, String schemaName, TimeUpdate timeUpdate) {
    	Constant.LOGGER.info("Inside deleteSoftCompany DeviceService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		 Device device=this.genericINterface.findById(id, Device.class);
 		  //System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
 		device.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
 		device.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
 		device.setStatus(Status.INACTIVE);
 		 // System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
 		  MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		  this.genericINterface.saveName(device);
 	    	 
 	    	 Map<String, String> response = new HashMap<String, String>();
 	    	    response.put("message", "Device deleted successfully");
 	    	    return response;
 	       
 	    }
    
    public Device getDeviceDataByDeviceId(String deviceid, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getDeviceDataByDeviceId DeviceService.java");
         System.out.println("@@@@@@@@@@@@@@@this.deviceRepository.findByDeviceId(deviceid)"+this.deviceRepository.findByDeviceId(deviceid));
		//return this.deviceRepository.findByDeviceId(deviceid);
    	return this.deviceRepository.findByDeviceId(deviceid);
	}
    //get topic
    public Topic getTopicById(String id, String schemaName) {
    	Constant.LOGGER.info("Inside getTopicById DeviceService.java");
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		//Device device = this.deviceRepository.findById(id);
    	Device device = this.genericINterface.findById(id, Device.class);
    	Topic monotopic = null;
		if (device.getStream() != null) {

			Topic topic = new Topic(device.getStream().getTopic());
			topic.setName(device.getStream().getTopic());
			monotopic = topic;
		}

		return monotopic;
	}
    
    
    public Topic getTopicByDeviceId(String deviceid, String schemaName) {
    	Constant.LOGGER.info("Inside getTopicByDeviceId DeviceService.java");
      MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Device device = this.deviceRepository.findByDeviceId(deviceid);

		Topic monotopic = null;
		if (device.getStream() != null) {
			Topic topic = new Topic(device.getStream().getTopic());
			System.out.println("topic name:" + topic.getName());
			topic.setName(device.getStream().getTopic());
			monotopic = topic;
		}

		return monotopic;

	}
    
    public Stream<Device> getDevicesByDeviceType(String name, String schemaName) {
    	Constant.LOGGER.info("Inside getDevicesByDeviceType DeviceService.java");
        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	//List<Device> dev= this.genericINterface.findAll(Device.class);
    	return this.genericINterface.findAll(Device.class).stream().filter(data -> data.getDeviceType().equals(name));
    	//filter(dev -> dev.getDeviceType().getName().contains(name));
        //.filter(dev -> dev.getDeviceType().getName().contains(name));
  	}
  	
//  	public Stream<Device> getDevicesByDeviceTypeId(String id, String schemaName) {
//  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//        return this.deviceRepository.findAll().stream().filter(data -> data.getDeviceType().getId().contains(id));
//        		//.filter(devices -> devices.getDeviceType().getId().contains(id));
//  	}
    public List<Device> streamAllDevices() {
        return deviceRepository.findAll();
    }
    
    
    
    //To get all devices paginated
    //page number starts from zero
	public List<Device> getAllDeviceDataPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllDeviceDataPaginated DeviceService.java");
    	Query query = new Query();
    	List<Device> devices =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	devices= this.genericINterface.find(query, Device.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllDeviceDataPaginated DeviceService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllDeviceDataPaginated "+query);
        return devices;
	
	}

	public List<Device> getAllDeviceDataByComapany(String companyId, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllDeviceDataByComapany DeviceService.java");
    	Query query = new Query();
    	List<Device> devices =null;

    	try {
    	query.addCriteria(Criteria.where("company.id").in(companyId));
    	devices= this.genericINterface.find(query, Device.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllDeviceDataByComapany DeviceService.java "+ex.getMessage());
        }
    	
        return devices;
	}

	public List<Device> getAllTagUnTaggedDevices(String companyid,String variableMap, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside get all devices  Service.java");
    	Query query = new Query();
    	List<Device> devices =null;

    	try {
    		if(variableMap.equalsIgnoreCase("tagged")) {
    			query.addCriteria(Criteria.where("deviceTagged").is(true).andOperator(Criteria.where("company.id").is(companyid)));
    		}else if(variableMap.equalsIgnoreCase("untagged"))
    		{
    			query.addCriteria(Criteria.where("deviceTagged").is(false).andOperator(Criteria.where("company.id").is(companyid)));
    		}
    	
    		devices= this.genericINterface.find(query, Device.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error inside Device. Device.java "+ex.getMessage());
        }
        return devices;
	}

	public List<Device> getAllDevicesByUserName(String username, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside all devicees devicedeviceService.java");
    	Query query1 = new Query();
    	Query query2 = new Query();
    	List<Device> device =new ArrayList<Device>();
    	List<Device> deviceList =null;

    	try {
    		query1.addCriteria(Criteria.where("taggedUserName").is(username));
    		Constant.LOGGER.info("query-------------"+query1.toString());
    		
    		deviceList= this.genericINterface.find(query1, Device.class);	
    		device.addAll(deviceList);
    		query2.addCriteria(Criteria.where("deviceTagged").is(false));
    		Constant.LOGGER.info("query-------------"+query2.toString());
    		
    		deviceList= this.genericINterface.find(query2, Device.class);	
    		device.addAll(deviceList);

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error inside device. device.java "+ex.getMessage());
        }
        return device;
	}

	public List<Device>  getDeviceDataByImei(String imei, String schemaName) {
		List<Device> deviceList =null;
		Constant.LOGGER.info("imei-------------"+imei+"--"+schemaName);
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Query query1 = new Query();
		query1.addCriteria(Criteria.where("imei").is(imei));
		Constant.LOGGER.info("query-------------"+query1.toString());
		
		deviceList= this.genericINterface.find(query1, Device.class);
        return deviceList;
	}
}
