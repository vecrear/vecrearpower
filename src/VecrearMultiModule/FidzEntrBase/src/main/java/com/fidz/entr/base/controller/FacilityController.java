package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.base.exception.FacilityNotFoundException;
import com.fidz.entr.base.model.DeviceType;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.service.FacilityService;
@CrossOrigin(maxAge = 3600)
@RestController("FEBFacilityController")
@RequestMapping(value="/facilities")
public class FacilityController {
	@Autowired
	private FacilityService facilityService;
	
	
	@PostMapping("get/all")
    public List<Facility> getAllFacilities(@RequestHeader(value="schemaName") String schemaName) {
    	return facilityService.getAllFacilities(schemaName);
    }
	
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Facility> getAllFacilitiesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
    return facilityService.getAllFacilitiesPaginated(pageNumber, pageSize, schemaName);
	}
		
	@PostMapping("/creates")
    public void createFacilitys(@Valid @RequestBody Facility facility) {
        facilityService.createFacilitys(facility);
    }
	
    @PostMapping
    public Facility createFacility(@Valid @RequestBody Facility facility) {
        return facilityService.createFacility(facility);
    }
    
    @PostMapping("/get/id/{id}")
    public Facility getFacilityById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return facilityService.getFacilityById(id, schemaName);
    }
	
    @PostMapping("/get/name/facilityname/{name}")
    public Facility getFacilityByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return facilityService.getFacilityByName(name, schemaName);
    }
	
    @PostMapping("/update/id//{id}")
    public Facility updateFacility(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Facility facility) {
        return facilityService.updateFacility(id, facility);
    }
  

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteFacility(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return facilityService.deleteFacility(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteFacility(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return facilityService.deleteSoftFacility(id, schemaName, timeUpdate);
    }
   
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Facility> streamAllFacilities() {
    	
        return facilityService.streamAllFacilities();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Facility with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(FacilityNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
