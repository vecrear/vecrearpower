package com.fidz.entr.base.exception;

public class FacilityNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6929793800777130061L;
	
	public FacilityNotFoundException(String facility) {
		super("Facility not found with id/name " + facility);
	}
}
