package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.model.DeviceType;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.repository.FacilityRepository;

@Service("FEBFacilityService")
public class FacilityService {
	@Autowired
	private FacilityRepository facilityRepository;
	
	private GenericAppINterface<Facility> genericINterface;
	@Autowired
	public FacilityService(GenericAppINterface<Facility> genericINterface){
		this.genericINterface=genericINterface;
	}
	
    public List<Facility> getAllFacilities(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDeviceTypes FacilityService.java");
    	List<Facility> facilities=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	facilities=this.genericINterface.findAll(Facility.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllFacilities FacilityService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllFacilities FacilityService.java");
    	return facilities;
    }
    
    public void createFacilitys(Facility facility) {
    	Constant.LOGGER.info("Inside createFacilitys FacilityService.java");   
    	String schemaName=facility.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	List<Facility> facilities = this.genericINterface.findAll(Facility.class);
	    	 
	    	  List<String> names = facilities.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("Facility List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(facility.getName())==true)){
	    			Constant.LOGGER.info("Inside if statement");
	    			throw new NameFoundException(facility.getName()+"Facility Name Already exists"+"");
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(facility.getName())==true)){
	    				Constant.LOGGER.info("Inside else if statement");
	    	  throw new NameFoundException(facility.getName()+" Facility Name Already exists "+"with different case");
	    		}else{
	    			Constant.LOGGER.info("Successfull creation of Facility FacilityService.java");
	    			Constant.LOGGER.info("Inside else statement");
	    			this.genericINterface.saveName(facility);	    		}
    
    	
       
    }
    
    public Facility createFacility(Facility facility) {
    	Facility fac=null;
    	try {
    		Constant.LOGGER.debug(" Inside FacilityService.java Value for insert facility record:createFacility :: " + facility);
    		String schemaName=facility.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	fac=this.facilityRepository.save(facility);
    	}
    	catch (Exception e) {
			Constant.LOGGER.error("Exception inside FacilityService.java create Facility::" + e.getMessage());
		}
    	return fac;
       
    }
    
    public Facility getFacilityById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, Facility.class);
    }
	
    public Facility getFacilityByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.facilityRepository.findByName(name);
    }
    public Facility updateFacility(String id, Facility facility) {
    	String schemaName=facility.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	facility.setId(id);
    	return this.facilityRepository.save(facility);
    	
    }
    public Optional<Object> updateFacilitys(String id, Facility facility) {
    	String schemaName=facility.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//    	/facility.setId(id);
    	return this.facilityRepository.findById(id).flatMap(existingApplication -> {
    		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    		facility.setId(id);
    		return this.genericINterface.saveNames(facility);
                });
    	//return this.facilityRepository.save(facility);
     /*   return facilityRepository.findById(id)
                .flatMap(existingApplication -> {
                    return facilityRepository.save(facility);
                })
                .map(updateFacility -> new ResponseEntity<>(updateFacility, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));*/
    }
    
    //hard delete
	public Map<String, String> deleteFacility(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.facilityRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Company deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftFacility(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Facility facility = this.genericINterface.findById(id, Facility.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		facility.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		facility.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		facility.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(facility);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "Company deleted successfully");
		return response;

	}
   
    public List<Facility> streamAllFacilities() {
        return facilityRepository.findAll();
    }

    //To get all facilities paginated
    //page number starts from zero
	public List<Facility> getAllFacilitiesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllFacilitiesPaginated FacilityService.java");
    	Query query = new Query();
    	List<Facility> facilities =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	facilities= this.genericINterface.find(query, Facility.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllFacilitiesPaginated FacilityService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllFacilitiesPaginated "+query);
        return facilities;	
	}
}
