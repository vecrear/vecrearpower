package com.fidz.entr.base.exception;

public class StateNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5465936884262456818L;

	public StateNotFoundException(String state) {
		super("State not found with id/name " + state);
	}
}
