package com.fidz.entr.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.fidz.entr.base", "com.fidz.base", "com.fidz.services.location","com.fidz.entr.app"})
@EnableReactiveMongoRepositories
public class FidzEntrBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(FidzEntrBaseApplication.class, args);
	}
}
