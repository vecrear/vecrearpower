package com.fidz.entr.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductNotFoundException extends RuntimeException {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;
	
	public ProductNotFoundException(String product) {
		super("Product not found with id/name " + product);
	}
	public ProductNotFoundException(String product, String data) {
		super(data);
	}
}
