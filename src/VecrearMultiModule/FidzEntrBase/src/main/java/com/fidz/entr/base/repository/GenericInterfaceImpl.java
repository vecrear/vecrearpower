package com.fidz.entr.base.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Mono;
@Repository
public class GenericInterfaceImpl<TY> implements GenericINterface<TY>{
private final ReactiveMongoTemplate reactiveMongoTemplate;
	
    @Autowired
	public GenericInterfaceImpl(ReactiveMongoTemplate reactiveMongoTemplate) {
	
	this.reactiveMongoTemplate = reactiveMongoTemplate;
   }

	@Override
	public Mono<TY> saveName(TY model1) {
		
		return reactiveMongoTemplate.save(model1);
	}

}
