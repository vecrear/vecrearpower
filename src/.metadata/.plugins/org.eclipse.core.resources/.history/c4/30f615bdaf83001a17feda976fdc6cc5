package com.fidz.entr.app.controller;

import java.net.UnknownHostException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.entr.app.config.ClientConnectionManager;
import com.fidz.entr.app.config.DBDataNotFoundException;

import com.fidz.entr.app.model.DBData;
import com.fidz.entr.app.payload.ErrorResponse;
import com.fidz.entr.app.repository.DBDataRepository;
import com.fidz.entr.app.service.DBDataService;
import com.mongodb.DuplicateKeyException;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEADBDataController")
@RequestMapping(value="/dbdatas")
public class DBDataController {

	@Autowired
	private DBDataService  dBDataService;
	
	@GetMapping
	public Flux<DBData> getAllDBDatas(){
		return dBDataService.getAllDBDatas();
	}
	@PostMapping
	public Mono<DBData> createDBData(@Valid @RequestBody DBData dBData){
		return dBDataService.createDBData(dBData);
	}
	
	@GetMapping("/{id}")
	public Mono<DBData> getDBDataById(@PathVariable(value="id") String id){
		return dBDataService.getDBDataById(id);
		
	}
	
	@GetMapping("/schemaname/{name}")
    public Mono<DBData> getDBDataByName(@PathVariable(value = "name") String clientId) {
		return dBDataService.getDBDataByName(clientId);
    }
	
    @PutMapping("/{id}")
    public Mono<ResponseEntity<DBData>> updateDBData(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody DBData dBData) {
        return dBDataService.updateDBData(id, dBData);
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteDBData(@PathVariable(value = "id") String id) {
        return dBDataService.deleteDBData(id);
    }

    
    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<DBData> streamAllDBDatas() {
        return dBDataService.streamAllDBDatas();
    }

    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A DBData with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(DBDataNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
    
    @GetMapping("/client/{name}")
    public Mono<ResponseEntity<Void>> getMongoDBConnections(@PathVariable(value = "name") String clientId) throws UnknownHostException, UnsupportedOperationException {
   // 	Mono<DBManager> dd=new Mono<DBManager>();
    	return dBDataService.getMongoDBConnections(clientId);
    	
		
    }
}
