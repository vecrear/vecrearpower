package com.fidz.entr.app.model;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Address;
import com.fidz.base.model.Base;
import com.fidz.base.model.Contact;
import com.fidz.base.model.PersonName;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.model.Department;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import lombok.Data;

@Data
@Document(collection = "customer")
@JsonIgnoreProperties(value = { "target" })
public class Customer extends Base {
	@Id
	protected String id;
	
	@NotBlank
	@NotNull
	@Indexed(unique = true)
	protected String name;

	protected String customerId;
	
	protected PersonName contactName;
	
	protected Contact contact;
	
	protected Address address;
	
	protected CustomerType customerType;
	//@NotNull
	//@DBRef(lazy = true)
	//protected CustomerTypeField customerType;
	
	
	protected String customerDepartment;
	
	@NotNull
	@DBRef(lazy = true)
	protected Company company;
	
	@NotNull
	@DBRef(lazy = true)
	protected com.fidz.entr.base.model.Department department;
	
	@DBRef(lazy = true)
	protected Country country;
	
	@DBRef(lazy = true)
	protected Region region;
	
	@DBRef(lazy = true)
	protected State state;
	
	@DBRef(lazy = true)
	protected City city;
	
	@DBRef(lazy = true)
	protected CustomerCategory customerCategory;
	
	//optional mapping based on customer type - for regular customer this field is mandatory

	@DBRef(lazy = true)
	protected List<User> users = new ArrayList<User>();
	
	protected Map<String, Object> additionalFields;

	public Customer(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotBlank @NotNull String name, String customerId,
			PersonName contactName, Contact contact, Address address, CustomerType customerType,
			String customerDepartment, @NotNull Company company, Department department, Country country, Region region,
			State state, City city, CustomerCategory customerCategory, List<User> users,
			Map<String, Object> additionalFields) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.customerId = customerId;
		this.contactName = contactName;
		this.contact = contact;
		this.address = address;
		this.customerType = customerType;
		this.customerDepartment = customerDepartment;
		this.company = company;
		this.department = department;
		this.country = country;
		this.region = region;
		this.state = state;
		this.city = city;
		this.customerCategory = customerCategory;
		this.users = users;
		this.additionalFields = additionalFields;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", customerId=" + customerId + ", contactName=" + contactName
				+ ", contact=" + contact + ", address=" + address + ", customerType=" + customerType
				+ ", customerDepartment=" + customerDepartment + ", company=" + company + ", department=" + department
				+ ", country=" + country + ", region=" + region + ", state=" + state + ", city=" + city
				+ ", customerCategory=" + customerCategory + ", users=" + users + ", additionalFields="
				+ additionalFields + ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + "]";
	}

	
//	public Customer(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
//			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
//			@NotNull String schemaName, String id, @NotBlank @NotNull String name, String customerId,
//			PersonName contactName, Contact contact, Address address, @NotNull CustomerTypeField customerType,
//			String customerDepartment, @NotNull Company company, Department department, Country country, Region region,
//			State state, City city, CustomerCategory customerCategory, List<User> users,
//			Map<String, Object> additionalFields) {
//		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
//				schemaName);
//		this.id = id;
//		this.name = name;
//		this.customerId = customerId;
//		this.contactName = contactName;
//		this.contact = contact;
//		this.address = address;
//		this.customerType = customerType;
//		this.customerDepartment = customerDepartment;
//		this.company = company;
//		this.department = department;
//		this.country = country;
//		this.region = region;
//		this.state = state;
//		this.city = city;
//		this.customerCategory = customerCategory;
//		this.users = users;
//		this.additionalFields = additionalFields;
//	}
//
//	@Override
//	public String toString() {
//		return "Customer [id=" + id + ", name=" + name + ", customerId=" + customerId + ", contactName=" + contactName
//				+ ", contact=" + contact + ", address=" + address + ", customerType=" + customerType
//				+ ", customerDepartment=" + customerDepartment + ", company=" + company + ", department=" + department
//				+ ", country=" + country + ", region=" + region + ", state=" + state + ", city=" + city
//				+ ", customerCategory=" + customerCategory + ", users=" + users + ", additionalFields="
//				+ additionalFields + ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
//				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
//				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
//				+ schemaName + "]";
//	}



	



	
}
