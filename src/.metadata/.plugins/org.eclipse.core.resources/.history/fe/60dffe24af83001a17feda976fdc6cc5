package com.fidz.entr.app.reportattendance;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.DeviceType;
import com.fidz.base.model.Status;
import com.fidz.base.model.Stream;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
@Document(collection = "device")
public class Device {
	public Device(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, String deviceId, @NotNull DeviceType deviceType, Object schema,
			Stream stream, Object currentData, String imei, Object deviceHardwareDetails, @NotNull Object company) {
		super();
		this.createdTimestamp = createdTimestamp;
		this.createdByUser = createdByUser;
		this.updatedTimestamp = updatedTimestamp;
		this.updatedByUser = updatedByUser;
		this.deletedTimestamp = deletedTimestamp;
		this.deletedByUser = deletedByUser;
		this.status = status;
		this.schemaName = schemaName;
		this.id = id;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.schema = schema;
		this.stream = stream;
		this.currentData = currentData;
		this.imei = imei;
		this.deviceHardwareDetails = deviceHardwareDetails;
		this.company = company;
	}
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	
	@Id
	protected String id;
	@Indexed(unique = true)
	protected String deviceId;
	@JsonIgnore
	@NotNull
	@DBRef(lazy = true)
	protected DeviceType deviceType;
	@JsonIgnore
	protected Object schema;
	@JsonIgnore
	@DBRef(lazy = true)
	protected Stream  stream;
	@JsonIgnore
	protected Object currentData;
	
	protected String imei;
	@JsonIgnore
	protected Object deviceHardwareDetails;
	@JsonIgnore
	@NotNull
	@DBRef(lazy = true)
	protected Object company;
	@Override
	public String toString() {
		return "Device [createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + ", id=" + id + ", deviceId=" + deviceId + ", deviceType=" + deviceType + ", schema="
				+ schema + ", stream=" + stream + ", currentData=" + currentData + ", imei=" + imei
				+ ", deviceHardwareDetails=" + deviceHardwareDetails + ", company=" + company + "]";
	}

	

	
	
}
