package com.fidz.entr.app.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Application;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "applicationconfig")
@JsonIgnoreProperties(value = { "target" })
public class ApplicationConfig extends Base {
	@Id
	protected String id;
	
	@NotNull
	@DBRef(lazy = true)
	protected Application application;
	
	protected List<StaticField> staticUserFields;
	
	protected List<StaticField> staticCustomerFields;

	public ApplicationConfig(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull Application application, List<StaticField> staticUserFields,
			List<StaticField> staticCustomerFields) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.application = application;
		this.staticUserFields = staticUserFields;
		this.staticCustomerFields = staticCustomerFields;
	}

	@Override
	public String toString() {
		return "ApplicationConfig [id=" + id + ", application=" + application + ", staticUserFields=" + staticUserFields
				+ ", staticCustomerFields=" + staticCustomerFields + ", createdTimestamp=" + createdTimestamp
				+ ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser="
				+ updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser
				+ ", status=" + status + ", schemaName=" + schemaName + "]";
	}


	
}
