package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.DepartmentNotFoundException;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.service.DepartmentService;
import com.fidz.entr.base.model.Department;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEADepartmentController")
@RequestMapping(value="/departments")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;
	
	
	@PostMapping("/get/all")
    public List<Department> getAllDepartments(@RequestHeader(value="schemaName") String schemaName) {
    	return departmentService.getAllDepartments(schemaName);
    }
	
	//Post request to get all Department  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<Department> getAllDepartmentsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return departmentService.getAllDepartmentsPaginated(pageNumber, pageSize, schemaName);
	}
	
    @PostMapping
    public Department createDepartment(@Valid @RequestBody Department department) {
        return departmentService.createDepartment(department);
    }
    @PostMapping("/creates")
    public void createDepartments(@Valid @RequestBody Department department) {
        departmentService.createDepartment(department);
    }
    @PostMapping("/get/id/{id}")
    public Department getDepartmentById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return departmentService.getDepartmentById(id, schemaName);
    }
	
    @PostMapping("/get/name/departmentname/{name}")
    public Department getDepartmentByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return departmentService.getDepartmentByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public Department updateDepartment(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Department department) {
        return departmentService.updateDepartment(id, department);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteDepartment(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return departmentService.deleteDepartment(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteDepartment(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return departmentService.deleteSoftDepartment(id, schemaName, timeUpdate);
    }
    
    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Department> streamAllDepartments() {
        return departmentService.streamAllDepartments();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Department with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(DepartmentNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
