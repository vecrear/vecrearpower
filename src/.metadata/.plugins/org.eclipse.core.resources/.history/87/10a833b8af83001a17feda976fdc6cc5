package com.fidz.base.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Document(collection = "stream")
@JsonIgnoreProperties(value = { "target" })
public class Stream extends Base {
	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	
	@NotBlank
	@Indexed(unique = true)
	protected String topic;
	
	
	@DBRef(lazy = true)
	protected List<Application> applications;

	public Stream(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotBlank String name, String topic,
			List<Application> applications) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.topic = topic;
		this.applications = applications;
	}

	@Override
	public String toString() {
		return "Stream [id=" + id + ", name=" + name + ", topic=" + topic + ", applications=" + applications
				+ ", createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp="
				+ updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp
				+ ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	
	
	
}
