package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.CustomerCategoryNotFoundException;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.CustomerCategory;
import com.fidz.entr.app.service.CustomerCategoryService;
import com.mongodb.DuplicateKeyException;



@CrossOrigin(maxAge = 3600)
@RestController("FEACustomerCategoryController")
@RequestMapping(value="/customercategories")
public class CustomerCategoryController {
	@Autowired
	private CustomerCategoryService  customerCategoryService;
	
	@PostMapping("/get/all")
	public List<CustomerCategory> getAllCustomers(@RequestHeader(value="schemaName") String schemaName){
		return customerCategoryService.getAllCustomerCategories(schemaName);
	}
	//Post request to get all CustomerCategory  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<CustomerCategory> getAllCustomersPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return customerCategoryService.getAllCustomersPaginated(pageNumber, pageSize, schemaName);
	}
		
	@PostMapping
	public CustomerCategory createCustomerCategory(@Valid @RequestBody CustomerCategory customerCategory){
		return customerCategoryService.createCustomerCategory(customerCategory);
	}
	@PostMapping("/creates")
	public void createCustomerCategorys(@Valid @RequestBody CustomerCategory customerCategory){
		customerCategoryService.createCustomerCategorys(customerCategory);
	}
	@PostMapping("/get/id/{id}")
	public CustomerCategory getCustomerCategoryById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		return customerCategoryService.getCustomerCategoryById(id, schemaName);		
	}
	
	@PostMapping("/update/id/{id}")
    public CustomerCategory updateCustomerCategory(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody CustomerCategory customerCategory) {
        return customerCategoryService.updateCustomerCategory(id, customerCategory);
    }

   
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteCustomerCategory(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return customerCategoryService.deleteCustomerCategory(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteCustomerCategory(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return customerCategoryService.deleteSoftCustomerCategory(id, schemaName, timeUpdate);
    }
   
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<CustomerCategory> streamAllCustomers() {
        return customerCategoryService.streamAllCustomerCategories();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A CustomerCategory with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(CustomerCategoryNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
