package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.ApplicationNameFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.exception.CustomerCategoryNameFoundException;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.CustomerCategory;
import com.fidz.entr.app.repository.CustomerCategoryRepository;


@Service("FEACustomerCategoryService")
public class CustomerCategoryService {
	private GenericAppINterface<CustomerCategory> genericINterface;
	@Autowired
	public CustomerCategoryService(GenericAppINterface<CustomerCategory> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private CustomerCategoryRepository  customerCategoryRepository;
	
	public List<CustomerCategory> getAllCustomerCategories(String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllCustomerCategories CustomerCategoryService.java");
    	List<CustomerCategory> customerCategories=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	customerCategories=this.genericINterface.findAll(CustomerCategory.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllCustomerCategories CustomerCategoryService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllCustomerCategories CustomerCategoryService.java");
    	return customerCategories;
	}
	
	public CustomerCategory createCustomerCategory(CustomerCategory customerCategory){
		Constant.LOGGER.info("Inside createCustomerCategory CustomerCategoryService.java");    
		String schemaName=customerCategory.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    List<CustomerCategory> customerCategories = this.genericINterface.findAll(CustomerCategory.class);
	    List<String> names = customerCategories.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("Application List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(customerCategory.getName())==true)) {
	    			Constant.LOGGER.info("Inside if statement");
	    			throw new CustomerCategoryNameFoundException(customerCategory.getName()+" "+"");
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(customerCategory.getName())==true)) {
	    				Constant.LOGGER.info("Inside else if statement");
	    	  throw new CustomerCategoryNameFoundException(customerCategory.getName()+"  "+"with different case");
	    		}else{
	    			Constant.LOGGER.info("Inside else statement");
	    			Constant.LOGGER.info("Success Customer Category CustomerCategoryService");
	    		    return this.customerCategoryRepository.save(customerCategory);

	    	 }
	}
	
	public void createCustomerCategorys(CustomerCategory customerCategory){
	    String schemaName=customerCategory.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    
	    //return customerCategoryRepository.save(customerCategory);
	  this.genericINterface.saveName(customerCategory);
}
	public CustomerCategory getCustomerCategoryById(String id, String schemaName){
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 return this.genericINterface.findById(id, CustomerCategory.class);
		
	}
	
    public CustomerCategory updateCustomerCategory(String id, CustomerCategory customerCategory) {
    	 String schemaName=customerCategory.getSchemaName();
	    	
		 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		 customerCategory.setId(id);
		 return this.customerCategoryRepository.save(customerCategory);
        
    }

   
    //hard delete
  	public Map<String, String> deleteCustomerCategory(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.customerCategoryRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "CustomerCategory deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftCustomerCategory(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		CustomerCategory customerCategory = this.genericINterface.findById(id, CustomerCategory.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		customerCategory.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		customerCategory.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		customerCategory.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(customerCategory);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "CustomerCategory deleted successfully");
  		return response;

  	}
    public List<CustomerCategory> streamAllCustomerCategories() {
        return customerCategoryRepository.findAll();
    }
  //To get all CustomerCategory paginated
   //page number starts from zero
	public List<CustomerCategory> getAllCustomersPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllCustomersPaginated CustomerCategoryService.java");
    	Query query = new Query();
    	List<CustomerCategory> customerCategories =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	customerCategories= this.genericINterface.find(query, CustomerCategory.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllCustomersPaginated CustomerCategoryService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllCustomersPaginated "+query);
        return customerCategories;	
	}

}
