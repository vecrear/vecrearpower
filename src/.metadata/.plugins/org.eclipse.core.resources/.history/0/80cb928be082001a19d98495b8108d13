package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Country;
import com.fidz.entr.base.service.CountryService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@CrossOrigin(maxAge = 3600)
@RestController("FEntBaseCountryController")
@RequestMapping(value="/entrbase/countries")
public class CountryController {

	@Autowired
	private CountryService  countryService;
	
	@PostMapping("/get/all")
	public List<Country> getAllCountries(@RequestHeader(value="schemaName") String schemaName){
		return countryService.getAllCountries(schemaName);
	}
	
	//Post request to get all countries  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
    public List<Country> getAllCountriesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
    return countryService.getAllCountriesPaginated(pageNumber, pageSize, schemaName);
    }
	
	@PostMapping
	public Country createCountry(@Valid @RequestBody Country country){
		return countryService.createCountry(country);
	}
	@PostMapping("/creates")
	public void createCountrys(@Valid @RequestBody Country country){
		countryService.createCountrys(country);
	}
	@PostMapping("/get/id/{id}")
	public Country getCountryById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		return countryService.getCountryById(id, schemaName);
		
	}
	
	@PostMapping("/update/id/{id}")
    public Country updateCountry(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Country country) {
        return countryService.updateCountry(id, country);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteCountry(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return countryService.deleteCountry(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteCountry(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return countryService.deleteSoftCountry(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Country> streamAllCountries() {
        return countryService.streamAllCountries();
    }

   /* // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Country with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(CountryNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/
}
