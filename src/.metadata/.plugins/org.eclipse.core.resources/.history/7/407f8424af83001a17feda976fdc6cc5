package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.UserDepartmentConfigNotFoundException;
import com.fidz.entr.app.model.UserDepartmentConfig;
import com.fidz.entr.app.service.UserDepartmentConfigService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEAUserDepartmentConfigController")
@RequestMapping(value="/userdepartmentconfigs")
public class UserDepartmentConfigController {

	@Autowired
	private UserDepartmentConfigService  userDepartmentConfigService;
	
	@PostMapping("/get/all")
	public List<UserDepartmentConfig> getAllUserDepartmentConfigs(@RequestHeader(value="schemaName") String schemaName){
		return userDepartmentConfigService.getAllUserDepartmentConfigs(schemaName);
	}
	
	@PostMapping
	public UserDepartmentConfig createUserDepartmentConfig(@Valid @RequestBody UserDepartmentConfig userDepartmentConfig){
		return userDepartmentConfigService.createUserDepartmentConfig(userDepartmentConfig);
	}
	
	@PostMapping("/creates")
	public void createUserDepartmentConfigs(@Valid @RequestBody UserDepartmentConfig userDepartmentConfig){
		userDepartmentConfigService.createUserDepartmentConfigs(userDepartmentConfig);
	}
	
	@PostMapping("/get/id/{id}")
	public UserDepartmentConfig getUserDepartmentConfigById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		return userDepartmentConfigService.getUserDepartmentConfigById(id, schemaName);
		
	}
	
	@PostMapping("/update/id/{id}")
    public UserDepartmentConfig updateUserDepartmentConfig(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody UserDepartmentConfig userDepartmentConfig) {
        return userDepartmentConfigService.updateUserDepartmentConfig(id, userDepartmentConfig);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteUserDepartmentConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return userDepartmentConfigService.deleteUserDepartmentConfig(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteUserDepartmentConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return userDepartmentConfigService.deleteSoftUserDepartmentConfig(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<UserDepartmentConfig> streamAllUserDepartmentConfigs() {
        return userDepartmentConfigService.streamAllUserDepartmentConfigs();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A UserDepartmentConfig with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(UserDepartmentConfigNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
