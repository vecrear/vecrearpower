package com.fidz.entr.app.reportactivities;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import com.fidz.entr.app.reportattendance.Contact;
import com.fidz.entr.app.reportattendance.PersonName;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Customer {
	
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;
	
	@Id
	protected String id;
	
	@NotNull
	protected String name;
	
	protected String customerId;
	
	protected PersonName contactName;
	
	protected Contact contact;
	
	protected Address address;
	@JsonIgnore
	protected Object customerType;
	
	protected String customerDepartment;
	
	@NotNull
	@DBRef(lazy = true)
	protected Company company;
	
	@NotNull
	@DBRef(lazy = true)
	protected Department department;
	
	@DBRef(lazy = true)
	protected Country country;
	
	@DBRef(lazy = true)
	protected Region region;
	
	@DBRef(lazy = true)
	protected State state;
	
	@DBRef(lazy = true)
	protected City city;
	
	@DBRef(lazy = true)
	protected CustomerCategory customerCategory;
	
	//optional mapping based on customer type - for regular customer this field is mandatory
	@DBRef(lazy = true)
	protected List<User> users;
	
	protected Map<String, Object> additionalFields;

	@Override
	public String toString() {
		return "Customer [createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + ", id=" + id + ", name=" + name + ", customerId=" + customerId + ", contactName="
				+ contactName + ", contact=" + contact + ", address=" + address + ", customerType=" + customerType
				+ ", customerDepartment=" + customerDepartment + ", company=" + company + ", department=" + department
				+ ", country=" + country + ", region=" + region + ", state=" + state + ", city=" + city
				+ ", customerCategory=" + customerCategory + ", users=" + users + ", additionalFields="
				+ additionalFields + "]";
	}

	public Customer(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name, String customerId, PersonName contactName,
			Contact contact, Address address, Object customerType, String customerDepartment, @NotNull Company company,
			@NotNull Department department, Country country, Region region, State state, City city,
			CustomerCategory customerCategory, List<User> users, Map<String, Object> additionalFields) {
		super();
		this.createdTimestamp = createdTimestamp;
		this.createdByUser = createdByUser;
		this.updatedTimestamp = updatedTimestamp;
		this.updatedByUser = updatedByUser;
		this.deletedTimestamp = deletedTimestamp;
		this.deletedByUser = deletedByUser;
		this.status = status;
		this.schemaName = schemaName;
		this.id = id;
		this.name = name;
		this.customerId = customerId;
		this.contactName = contactName;
		this.contact = contact;
		this.address = address;
		this.customerType = customerType;
		this.customerDepartment = customerDepartment;
		this.company = company;
		this.department = department;
		this.country = country;
		this.region = region;
		this.state = state;
		this.city = city;
		this.customerCategory = customerCategory;
		this.users = users;
		this.additionalFields = additionalFields;
	}


	



	
}
