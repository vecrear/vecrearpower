package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.base.exception.StateNotFoundException;
import com.fidz.entr.base.model.City;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.service.StateService;

@CrossOrigin(maxAge = 3600)
@RestController("FEBStateController")
@RequestMapping(value="/states")
public class StateController {

	@Autowired
	private StateService stateService;
	
	
	@PostMapping("/get/all")
    public List<State> getAllStates(@RequestHeader(value="schemaName") String schemaName) {
    	return stateService.getAllStates(schemaName);
    }
	
	//Post request to get all States  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<State> getAllStatesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return stateService.getAllStatesPaginated(pageNumber, pageSize, schemaName);
	}
			
	
    @PostMapping
    public State createState(@Valid @RequestBody State state) {
        return stateService.createState(state);
    }
    
    @PostMapping("/creates")
    public void createStates(@Valid @RequestBody State state) {
        stateService.createStates(state);
    }
    
    @PostMapping("/get/id/{id}")
    public State getStateById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return stateService.getStateById(id, schemaName);
    }
	
    @PostMapping("/get/name/statename/{name}")
    public State getStateByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return stateService.getStateByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public State updateState(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody State state) {
        return stateService.updateState(id, state);
    }
    
    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteState(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return stateService.deleteState(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteState(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return stateService.deleteSoftState(id, schemaName, timeUpdate);
    }
    
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<State> streamAllStates() {
        return stateService.streamAllStates();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A State with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(StateNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
