package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.model.UserHierarchyLevel;
import com.fidz.entr.base.model.UserRole;
import com.fidz.entr.base.repository.UserRoleRepository;


@Service("FEntrBaseUserRoleService")
public class UserRoleService {
	private GenericAppINterface<UserRole> genericINterface;
	@Autowired
	public UserRoleService(GenericAppINterface<UserRole> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private UserRoleRepository userRoleRepository;
	
    public List<UserRole> getAllUserRoles(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllUserRoles UserRoleService.java");
    	List<UserRole> userRoles=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	userRoles=this.genericINterface.findAll(UserRole.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllUserRoles UserRoleService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllUserRoles UserRoleService.java");
    	return userRoles;
    }
	

    public UserRole createUserRole(UserRole userRole) {
    
    		Constant.LOGGER.info(" Inside UserRoleService.java Value for insert UserRole record: createUserRole  :: " + userRole);
    		String schemaName=userRole.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        
        	List<UserRole> userRoles = this.genericINterface.findAll(UserRole.class);
	    	 
	    	  List<String> names = userRoles.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("Facility List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(userRole.getName())==true)){
	    			Constant.LOGGER.info("Inside if statement");
	    			throw new NameFoundException(userRole.getName()+"UserRole Name Already exists"+"");
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(userRole.getName())==true)){
	    				Constant.LOGGER.info("Inside else if statement");
	    	  throw new NameFoundException(userRole.getName()+" UserRole Name Already exists "+"with different case");
	    		}else{
	    			Constant.LOGGER.info("Successfull creation of UserRole UserRoleService.java");
	    			Constant.LOGGER.info("Inside else statement");
	    		//	return this.genericINterface.saveName(state);	    		}

	
	    	//return this.stateRepository.save(state);
	    
        	//return this.stateRepository.save(state);
    	//	
	    		}    	
        	return  this.userRoleRepository.save(userRole);
    	
    	
        
    }
    
    public void createUserRoles(UserRole userRole) {
        String schemaName=userRole.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        //return userRoleRepository.save(userRole);
	    this.genericINterface.saveName(userRole);
    }
    public UserRole getUserRoleById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, UserRole.class);
		
    }
	
    public UserRole getUserRoleByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.userRoleRepository.findByName(name);
		
    }
	
    public UserRole updateUserRole(String id, UserRole userRole) {
        String schemaName=userRole.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    userRole.setId(id);
	    return this.userRoleRepository.save(userRole);
       
    }

   
  
    public Map<String, String> deleteUserRole(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.userRoleRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserRole deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftUserRole(String id, String schemaName, TimeUpdate timeUpdate) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		UserRole userRole = this.genericINterface.findById(id, UserRole.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		userRole.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		userRole.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		userRole.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(userRole);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserRole deleted successfully");
		return response;

	}
    public List<UserRole> streamAllUserRoles() {
        return userRoleRepository.findAll();
    }

    //To get all UserRoles paginated
    //page number starts from zero
	public List<UserRole> getAllUserRolesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserRolesPaginated UserRoleService.java");
    	Query query = new Query();
    	List<UserRole> userRoles =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	userRoles= this.genericINterface.find(query, UserRole.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserRolesPaginated UserRoleService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllUserRolesPaginated "+query);
        return userRoles;	
	
		
	}

}
