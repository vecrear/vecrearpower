package com.fidz.entr.app.model;

import java.util.Date;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "activitylog")
@JsonIgnoreProperties(value = { "target" })
public class ActivityLog extends Base {
	@Id
	protected String id;
	
	protected Map<String, Object> activityLogValues; //value is stored as Object. Key is fieldname of CustomField.

	public ActivityLog(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, Map<String, Object> activityLogValues) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.activityLogValues = activityLogValues;
	}

	@Override
	public String toString() {
		return "ActivityLog [id=" + id + ", activityLogValues=" + activityLogValues + ", createdTimestamp="
				+ createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp
				+ ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser="
				+ deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	
	
	
}
