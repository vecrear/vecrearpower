package com.fidz.entr.base.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.exception.NameFoundException;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Product;
import com.fidz.entr.base.model.Region;
import com.fidz.entr.base.model.State;
import com.fidz.entr.base.repository.StateRepository;

@Service("FEBStateService")
public class StateService {
	@Autowired
	private StateRepository stateRepository;
	
	private GenericAppINterface<State> genericINterface;
	@Autowired
	public StateService(GenericAppINterface<State> genericINterface){
		this.genericINterface=genericINterface;
	}
	
    public List<State> getAllStates(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllStates StateService.java");
    	List<State> states=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	states=this.genericINterface.findAll(State.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllStates StateService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllStates StateService.java");
    	return states;
    }
	
    public State createState(State state) {
    
    		Constant.LOGGER.info(" Inside StateService.java Value for insert State record:createState :: " + state);
    		String schemaName=state.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	List<State> states = this.genericINterface.findAll(State.class);
	    	 
	    	  List<String> names = states.stream().map(p->p.getName()).collect(Collectors.toList());
	    		Constant.LOGGER.info("Facility List data"+names);
	    	 
	    		if (names.stream().anyMatch(s -> s.equals(state.getName())==true)){
	    			Constant.LOGGER.info("Inside if statement");
	    			throw new NameFoundException(state.getName()+"State Name Already exists"+"");
	    	 }
	    		else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(state.getName())==true)){
	    				Constant.LOGGER.info("Inside else if statement");
	    	  throw new NameFoundException(state.getName()+" State Name Already exists "+"with different case");
	    		}else{
	    			Constant.LOGGER.info("Successfull creation of State StateService.java");
	    			Constant.LOGGER.info("Inside else statement");
	    		//	return this.genericINterface.saveName(state);	    		}

	
	    	return this.stateRepository.save(state);
	    
        	//return this.stateRepository.save(state);
    	//	
	    		}    	
    	
    	
    }
    
    public void createStates(State state) {
    	String schemaName=state.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	this.genericINterface.saveName(state);
    }
    
    public State getStateById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, State.class);
		
    }
	
    public State getStateByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.stateRepository.findByName(name);
    }
	
    public State updateState(String id, State state) {
    	String schemaName=state.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	state.setId(id);
    	return this.stateRepository.save(state);
    }
    public Optional<Object> updateStates(String id, State state) {
    	String schemaName=state.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.stateRepository.findById(id).flatMap(existingApplication -> {
    		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    		state.setId(id);
    		return this.genericINterface.saveNames(state);
                });
  
    }
  //hard delete
  	public Map<String, String> deleteState(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.stateRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "State deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftState(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		State state = this.genericINterface.findById(id, State.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		state.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		state.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		state.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(state);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "State deleted successfully");
  		return response;

  	}
  

    public List<State> streamAllStates() {
        return stateRepository.findAll();
    }

    //To get all states paginated
    //page number starts from zero
	public List<State> getAllStatesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllStatesPaginated StateService.java");
    	Query query = new Query();
    	List<State> states =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	states= this.genericINterface.find(query, State.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllStatesPaginated StateService.java "+ex.getMessage());
        }
        return states;	
	}
}
