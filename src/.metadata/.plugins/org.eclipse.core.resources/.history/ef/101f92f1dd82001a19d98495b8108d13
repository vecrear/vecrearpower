package com.fidz.services.location.service;

import java.util.HashMap;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.ApplicationNameFoundException;
import com.fidz.base.exception.DeviceNotFoundException;
import com.fidz.base.model.Application;
import com.fidz.base.model.Device;
import com.fidz.base.model.Status;
import com.fidz.base.repository.DeviceRepository;
import com.fidz.base.validator.Constant;
import com.fidz.services.location.exception.GeofenceNotFoundException;
import com.fidz.services.location.model.GeoFence;
import com.fidz.services.location.repository.GeofenceRepository;

@Service
public class GeofenceService {
	private GenericAppINterface<GeoFence> genericINterface;
	@Autowired
	public GeofenceService(GenericAppINterface<GeoFence> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private GeofenceRepository geofenceRepository;
	@Autowired
	private DeviceRepository deviceRepository;

	
    public List<GeoFence> getAllGeofence(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllGeofence GeofenceService.java");
    	List<GeoFence> geoFences=null;
    	try {
    	
    	geoFences=this.genericINterface.findAll(GeoFence.class);
    	}catch(Exception ex){
    	Constant.LOGGER.error("Error while getting data from getAllGeofence GeofenceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllGeofence GeofenceService.java");
    	return geoFences;
    }
	
    
    public GeoFence createGeoFence(GeoFence geofence) {
    	GeoFence geofences=null;
    	try {
    		Constant.LOGGER.debug(" Inside GeoFenceService.java Value for insert GeoFence record:createFacility :: " + geofence);
			String schemaName = geofence.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			//geofences = this.geofenceRepository.save(geofence);
			
			  List<GeoFence> applcations = this.genericINterface.findAll(GeoFence.class);
			  
			  List<String> names = applcations.stream().map(p->p.getName()).collect(Collectors.toList());
				Constant.LOGGER.info("Application List data"+names);
			 
				if (names.stream().anyMatch(s -> s.equals(geofence.getName())==true)) {
					Constant.LOGGER.info("Inside if statement");
					throw new GeofenceNotFoundException(geofence.getName()+" "+"");
			 }else if(names.stream().anyMatch(s -> s.equalsIgnoreCase(geofence.getName())==true)) {
						Constant.LOGGER.info("Inside else if statement");
			  throw new GeofenceNotFoundException(geofence.getName()+"  "+"with different case");
				}else{
					Constant.LOGGER.info("Inside else statement");
					geofences = this.geofenceRepository.save(geofence);
			 }
			
			
    	}catch(Exception e) {
    		Constant.LOGGER.debug(" Inside GeoFenceService.java Value for insert GeoFence record:createFacility :: " + geofence);
    	}
       return geofences;
    }
    
    public void createGeoFences(GeoFence geofence) {
        String schemaName=geofence.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        this.genericINterface.saveName(geofence);
    }
   
    public GeoFence getGeoFenceById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, GeoFence.class);
		
    }
	
    public Stream<GeoFence> getGeoFenceByDeviceIds(final String deviceId, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	//Device device = this.deviceRepository.findById(deviceId).block();
    	Device device = this.deviceRepository.findByDeviceId(deviceId);
    	//Device device = this.genericINterface.findById(deviceId, GeoFence.class);
    	
    	if (device == null) {
    		return (Stream<GeoFence>) new DeviceNotFoundException(deviceId);
    	}
    	
    	return this.geofenceRepository.findAll().stream().filter(geofence -> !geofence.getDeviceList().isEmpty()
    					&& geofence.getDeviceList().contains(device));    
		
    }
    public Stream<GeoFence> getGeoFenceByDeviceId(final String deviceId, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	//Device device = this.deviceRepository.findByDeviceId(deviceId).block();
    	Device device = this.deviceRepository.findByDeviceId(deviceId);
    	if (device == null) {
    		return (Stream<GeoFence>) new DeviceNotFoundException(deviceId);
    	}
    	
    	return  this.geofenceRepository.findAll().stream()
    			.filter(geofence -> !geofence.getDeviceList().isEmpty()
    					&& geofence.getDeviceList().contains(device)); 

    }
    
 /*public List<Device> getDeviceListNoGeofence() {
    	List<GeoFence> geofence=geofenceRepository.findAll();
    	HashSet<Device> deviceSet=new HashSet<Device>();
    	((Mono<Device>) geofence).subscribe(geo -> {
    		deviceSet.addAll(geo.getDeviceList());
    	});
    	return deviceRepository.findAll().filter(
    			dev -> !deviceSet.contains(dev));
    }*/
    public Stream<Device> getDeviceListNoGeofence(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	List<GeoFence> geofence=this.geofenceRepository.findAll();
    	HashSet<Device> deviceSet=new HashSet<Device>();
    	/*((Mono<Device>) geofence).subscribe(geo -> {
    		deviceSet.addAll(geo.getDeviceList());
    	});*/
    	return  this.deviceRepository.findAll().stream().filter(
    			dev -> !deviceSet.contains(dev));
    }
	
    public GeoFence updateGeoFenceData(String id,GeoFence geoFence) {
    	String schemaName=geoFence.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	geoFence.setId(id);
    	return this.geofenceRepository.save(geoFence);
    	
    }
   
    //hard delete
   	public Map<String, String> deleteGeoFenceData(String id, String schemaName) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.geofenceRepository.deleteById(id);
   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "Geofence deleted successfully");
   		return response;

   	}
        //soft delete
   	public Map<String, String> deleteSoftGeoFenceData(String id, String schemaName, TimeUpdate timeUpdate) {
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		GeoFence geoFence = this.genericINterface.findById(id, GeoFence.class);
   		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
   		geoFence.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
   		geoFence.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
   		geoFence.setStatus(Status.INACTIVE);
   		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
   		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
   		this.genericINterface.saveName(geoFence);

   		Map<String, String> response = new HashMap<String, String>();
   		response.put("message", "Company deleted successfully");
   		return response;

   	}
 
    public List<GeoFence> streamAllGeoFenceData() {
        return geofenceRepository.findAll();
    }
    
}
