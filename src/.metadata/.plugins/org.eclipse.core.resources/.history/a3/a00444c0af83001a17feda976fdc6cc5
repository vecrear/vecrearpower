package com.fidz.entr.base.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;

import lombok.Data;

@Data
@Document(collection = "country")
@JsonIgnoreProperties(value = { "target" })
public class Country extends Base {

	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;
	
	@NotNull
	protected String code;
	
	protected String description;
	
	@DBRef(lazy = true)
	protected List<State> states;
	
	@DBRef(lazy = true)
	protected List<Region> regions;

	public Country(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name, @NotNull String code, String description,
			List<State> states, List<Region> regions) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.code = code;
		this.description = description;
		this.states = states;
		this.regions = regions;
	}

	@Override
	public String toString() {
		return "Country [id=" + id + ", name=" + name + ", code=" + code + ", description=" + description + ", states="
				+ states + ", regions=" + regions + ", createdTimestamp=" + createdTimestamp + ", createdByUser="
				+ createdByUser + ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser
				+ ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status
				+ ", schemaName=" + schemaName + "]";
	}

	
	
	
}
