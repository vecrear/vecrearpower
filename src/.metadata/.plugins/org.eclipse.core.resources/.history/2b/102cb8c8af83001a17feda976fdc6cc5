package com.fidz.services.location.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.payload.ErrorResponse;
import com.fidz.services.location.exception.GpsCaptureConfigNotFoundException;
import com.fidz.services.location.model.GPSCaptureConfig;
import com.fidz.services.location.repository.GpsCaptureConfigRepository;
import com.fidz.services.location.service.GpsCaptureConfigService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value="/gpscaptureconfigs")
public class GpsCaptureConfigController {
	
	@Autowired
	private GpsCaptureConfigService gpsCaptureConfigService;
	
	@PostMapping("/get/all")
	public List<GPSCaptureConfig> getAllGpsData(@RequestHeader(value="schemaName") String schemaName) {
        return gpsCaptureConfigService.getAllGpsData(schemaName);
    }
	
	
	@PostMapping
    public GPSCaptureConfig saveGpsConfigData(@Valid @RequestBody GPSCaptureConfig gpsCaptureConfig) {
		 return gpsCaptureConfigService.saveGpsConfigData(gpsCaptureConfig);
	}
	@PostMapping("/creates")
    public void saveGpsConfigDatas(@Valid @RequestBody GPSCaptureConfig gpsCaptureConfig) {
		gpsCaptureConfigService.saveGpsConfigDatas(gpsCaptureConfig);
	}
	@PostMapping("/get/id/{id}")
    public GPSCaptureConfig getGpsConfigDataById(@PathVariable(value = "id") final String id, @RequestHeader(value="schemaName") String schemaName) {
		return gpsCaptureConfigService.getGpsConfigDataById(id, schemaName);
    }
	
	
	@PostMapping("/update/id/{id}")
    public GPSCaptureConfig updateGPSCaptureConfig(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody GPSCaptureConfig gpsCaptureConfig) {
        return gpsCaptureConfigService.updateGPSCaptureConfig(id, gpsCaptureConfig);
    }
	
	
	@PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<GPSCaptureConfig> streamAllGpsData() {
        return gpsCaptureConfigService.streamAllGpsData();
    }
	
    
/*    @GetMapping("/interval")
	public Flux<GPSCaptureConfig> getAllInterval() {
        return gpsCaptureConfigRepository.findAll();
    }
    
    @GetMapping("/retryinterval")
   	public Flux<GPSCaptureConfig> getAllretryInterval() {
           return gpsCaptureConfigRepository.findAll();
       }*/
    
	// Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse(" GPS Config details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(GpsCaptureConfigNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }   
}
