package com.fidz.entr.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.base.exception.UserAppConfigNotFoundException;
import com.fidz.entr.base.model.Stream;
import com.fidz.entr.base.model.UserAppConfig;
import com.fidz.entr.base.service.UserAppConfigService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEAUserAppConfigController")
@RequestMapping(value="/userappconfigs")
public class UserAppConfigController {

	@Autowired
	private UserAppConfigService userAppConfigService;
	
	@PostMapping("/get/all")
    public List<UserAppConfig> getAllUserAppConfigs(@RequestHeader(value="schemaName") String schemaName) {
    	return userAppConfigService.getAllUserAppConfigs(schemaName);
    }
	
	//Post request to get all UserAppConfigs  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<UserAppConfig> getAllUserAppConfigsPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
	return userAppConfigService.getAllUserAppConfigsPaginated(pageNumber, pageSize, schemaName);
	}
	
    @PostMapping
    public UserAppConfig createUserAppConfig(@Valid @RequestBody UserAppConfig userAppConfig) {
        return userAppConfigService.createUserAppConfig(userAppConfig);
    }
    @PostMapping("/creates")
    public void createUserAppConfigs(@Valid @RequestBody UserAppConfig userAppConfig) {
        userAppConfigService.createUserAppConfig(userAppConfig);
    }
    @PostMapping("/get/id/{id}")
    public UserAppConfig getUserAppConfigById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return userAppConfigService.getUserAppConfigById(id, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public UserAppConfig updateUserAppConfig(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody UserAppConfig userAppConfig) {
        return userAppConfigService.updateUserAppConfig(id, userAppConfig);
    }
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteUserAppConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return userAppConfigService.deleteUserAppConfig(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteUserAppConfig(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return userAppConfigService.deleteSoftUserAppConfig(id, schemaName, timeUpdate);
    }
   

    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<UserAppConfig> streamAllUserAppConfigs() {
        return userAppConfigService.streamAllUserAppConfigs();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A UserAppConfig with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(UserAppConfigNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
