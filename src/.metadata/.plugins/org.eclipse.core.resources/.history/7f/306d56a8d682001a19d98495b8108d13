package com.fidz.entr.app.reportactivities;

import java.util.Date;
import java.util.TimeZone;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.DistanceCalculationMethod;
import com.fidz.entr.base.model.PunchInFrequency;

import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Department  {
	
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;

	@Id
	protected String id;
	
	@NotNull
	protected String name;
	
	protected String description;
	
	@JsonIgnore
	@DBRef(lazy = true)
	protected Object company;

	@NotNull
	protected PunchInFrequency punchInFrequency;
	
	/**
	 * gpsInterval - Should be in mins
	 */
	@NotNull
	protected int gpsInterval;
	@NotNull
	protected DistanceCalculationMethod distanceCalculationMethod;
	@NotNull
	protected String forcePunchOutTime;
	@NotNull
	protected TimeZone forcePunchOutTimeZone;
	@Override
	public String toString() {
		return "Department [createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + ", id=" + id + ", name=" + name + ", description=" + description + ", company=" + company
				+ ", punchInFrequency=" + punchInFrequency + ", gpsInterval=" + gpsInterval
				+ ", distanceCalculationMethod=" + distanceCalculationMethod + ", forcePunchOutTime="
				+ forcePunchOutTime + ", forcePunchOutTimeZone=" + forcePunchOutTimeZone + "]";
	}
	public Department(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name, String description, Object company,
			@NotNull PunchInFrequency punchInFrequency, @NotNull int gpsInterval,
			@NotNull DistanceCalculationMethod distanceCalculationMethod, @NotNull String forcePunchOutTime,
			@NotNull TimeZone forcePunchOutTimeZone) {
		super();
		this.createdTimestamp = createdTimestamp;
		this.createdByUser = createdByUser;
		this.updatedTimestamp = updatedTimestamp;
		this.updatedByUser = updatedByUser;
		this.deletedTimestamp = deletedTimestamp;
		this.deletedByUser = deletedByUser;
		this.status = status;
		this.schemaName = schemaName;
		this.id = id;
		this.name = name;
		this.description = description;
		this.company = company;
		this.punchInFrequency = punchInFrequency;
		this.gpsInterval = gpsInterval;
		this.distanceCalculationMethod = distanceCalculationMethod;
		this.forcePunchOutTime = forcePunchOutTime;
		this.forcePunchOutTimeZone = forcePunchOutTimeZone;
	}

	
	

}
