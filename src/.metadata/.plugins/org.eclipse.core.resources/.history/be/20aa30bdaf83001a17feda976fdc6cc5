package com.fidz.entr.app.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Base;
import com.fidz.base.model.Status;
import com.fidz.entr.base.model.Company;
import lombok.Data;

@Data
@Document(collection = "customercategory")
@JsonIgnoreProperties(value = { "target" })
public class CustomerCategory extends Base {
	@Id
	protected String id;
	
	@NotBlank
	@Indexed(unique = true)
	protected String name;

	@NotNull
	@DBRef(lazy = true)
	protected Company company;

	public CustomerCategory(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name, @NotNull Company company) {
		super(createdTimestamp, createdByUser, updatedTimestamp, updatedByUser, deletedTimestamp, deletedByUser, status,
				schemaName);
		this.id = id;
		this.name = name;
		this.company = company;
	}

	@Override
	public String toString() {
		return "CustomerCategory [id=" + id + ", name=" + name + ", company=" + company + ", createdTimestamp="
				+ createdTimestamp + ", createdByUser=" + createdByUser + ", updatedTimestamp=" + updatedTimestamp
				+ ", updatedByUser=" + updatedByUser + ", deletedTimestamp=" + deletedTimestamp + ", deletedByUser="
				+ deletedByUser + ", status=" + status + ", schemaName=" + schemaName + "]";
	}

	
	
}
