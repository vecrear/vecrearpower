package com.fidz.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.DeviceTypeNotFoundException;
import com.fidz.base.model.DeviceType;
import com.fidz.base.payload.ErrorResponse;

import fidz.base.service.DeviceTypeService;


@CrossOrigin(maxAge = 3600)
@RestController("FBaseDeviceTypeController")
@RequestMapping(value="/base/devicetypes")
public class DeviceTypeController {

	@Autowired
	private DeviceTypeService deviceTypeService;
	
	
	@PostMapping("/get/all")
    public List<DeviceType> getAllDeviceTypes(@RequestHeader(value="schemaName") String schemaName) {
    	return deviceTypeService.getAllDeviceTypes(schemaName);
    }
	
    @PostMapping
    public DeviceType createDeviceType(@Valid @RequestBody DeviceType deviceType) {
        return deviceTypeService.createDeviceType(deviceType);
    }
    
    @PostMapping("/creates")
    public void createDeviceTypes(@Valid @RequestBody DeviceType deviceType) {
        deviceTypeService.createDeviceTypes(deviceType);
    }
    
    @PostMapping("/get/id/{id}")
    public DeviceType getDeviceTypeById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return deviceTypeService.getDeviceTypeById(id, schemaName);
    }
	
    @PostMapping("/get/name/devicetypename/{name}")
    public DeviceType getDeviceTypeByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return deviceTypeService.getDeviceTypeByName(name, schemaName);
    }
	
    @PostMapping("/update/id/{id}")
    public DeviceType updateDeviceType(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody DeviceType deviceType) {
        return deviceTypeService.updateDeviceType(id, deviceType);
    }
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteDeviceType(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return deviceTypeService.deleteDeviceType(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteDeviceType(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return deviceTypeService.deleteSoftDeviceType(id, schemaName, timeUpdate);
    }
  
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<DeviceType> streamAllDeviceTypes() {
        return deviceTypeService.streamAllDeviceTypes();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A DeviceType with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(DeviceTypeNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
}
