package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.CustomerNotFoundException;
import com.fidz.entr.app.model.Country;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.service.CustomerService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEACustomerController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/customers")
public class CustomerController {
	
	@Autowired
	private CustomerService  customerService;
	
	@Value("${customerservices.enabled}")
	private Boolean property;
	
	
	@PostMapping("/get/all")
	//@Cacheable(value = "customers")
	public List<Customer> getAllCustomers(@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return customerService.getAllCustomers(schemaName);
	}
	// getting the webList Customers
	@PostMapping("/get/all/web")
	//@Cacheable(value = "customers")
	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomersweb(@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return customerService.getAllCustomersWeb(schemaName);
	}
	
	//Post request to get all Customersweb  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<com.fidz.entr.app.reportcustomers.Customer> getAllCustomerswebPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return customerService.getAllCustomersPaginated(pageNumber, pageSize, schemaName);
	}
		
	
	@PostMapping
	public Customer createCustomer(@Valid @RequestBody Customer customer){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return customerService.createCustomer(customer);
	}
	@PostMapping("/creates")
	public void createCustomers(@Valid @RequestBody Customer customer){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		customerService.createCustomer(customer);
	}
	
	@PostMapping("/get/id/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return customerService.getCustomerById(id, schemaName);
		
		
	}
	
	@PostMapping("/get/id/userid/{id}")
	public Stream<Customer> getCustomerByUserId(@PathVariable(value="id") String userId, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return customerService.getCustomerByUserId(userId, schemaName);		
	}
	
	@PostMapping("/update/id/{id}")
    public Customer updateCustomer(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Customer customer) {
		System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return customerService.updateCustomer(id, customer);
    }

    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteCustomer(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return customerService.deleteCustomer(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteCustomer(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return customerService.deleteSoftCustomer(id, schemaName, timeUpdate);
    }
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Customer> streamAllCustomers() {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return customerService.streamAllCustomers();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Customer with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(CustomerNotFoundException ex) {
    	System.out.println("property value******"+property);
		String message="Customerservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.notFound().build();
    }

}
