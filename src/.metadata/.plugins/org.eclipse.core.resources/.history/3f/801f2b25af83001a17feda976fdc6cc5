package com.fidz.entr.app.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.ApplicationConfig;
import com.fidz.entr.app.model.Attendance;
import com.fidz.entr.app.model.Location;
import com.fidz.entr.app.model.LogOffTime;

import com.fidz.entr.app.reportattendance.PersonName;

import com.fidz.entr.app.reportattendance.User;
import com.fidz.entr.app.repository.AttendanceRepository;
import com.fidz.entr.app.repository.LocationRepository;
import com.fidz.entr.base.model.Facility;
import com.fidz.services.location.model.GpsData;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.mongodb.client.model.Collation;


		
@Service("FEAAttendanceService")
public class AttendanceService {
	private GenericAppINterface<Attendance> genericINterface;
	@Autowired
	public AttendanceService(GenericAppINterface<Attendance> genericINterface){
		this.genericINterface=genericINterface;
	}
         
	@Autowired
	private AttendanceRepository attendanceRepository;
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
    public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancesweb(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllAttendancesweb AttendanceService.java");
    	List<com.fidz.entr.app.reportattendance.Attendance> attendances=null;
    	try {
    	//Query q = new Query();
    	//q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	attendances=this.genericINterface.findAll(com.fidz.entr.app.reportattendance.Attendance.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllAttendancesweb AttendanceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllAttendancesweb AttendanceService.java");
    	return attendances;
    }
    
    
    
    public List<Attendance> getAllAttendances(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllAttendances AttendanceService.java");
    	List<Attendance> attendances=null;
    	try {
    	attendances=this.genericINterface.findAll(Attendance.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllAttendances AttendanceService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllAttendances AttendanceService.java");
    	return attendances;
    }
	
    public Attendance createAttendance(Attendance attendance) {
    	Attendance attendances=null;
    	try {
    		Constant.LOGGER.debug(" Inside AttendanceService.java Value for insert attendance record: createattendance  :: " + attendance);
    		String schemaName=attendance.getSchemaName();
        	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        	Location a = null;
        	Location location = null;
        	if (attendance.getPunchInLocation() != null && attendance.getPunchOutLocation()== null) {
        		a = createLocation(attendance.getPunchInLocation(), schemaName);
        		location = a;
        		attendance.setPunchInLocation(location);
        	}  	
     	   
        	//Mono<Attendance> b = attendanceRepository.save(attendance);
        	//Attendance b = this.attendanceRepository.save(attendance);
      //  	System.out.println("b :"+b.toString());   	
            //return b;
        	attendances=this.attendanceRepository.save(attendance);
    	}catch(Exception e) {
    		Constant.LOGGER.debug(" Inside AttendanceService.java Value for insert facility record: createattendance  :: " + attendance);
    	}
    	return attendances;
    }
    public void createAttendances(Attendance attendance) {
    	String schemaName=attendance.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Location a = null;
    	Location location = null;
    	if (attendance.getPunchInLocation() != null && attendance.getPunchOutLocation()== null) {
    		a = createLocation(attendance.getPunchInLocation(),schemaName);
    		location = a;
    		attendance.setPunchInLocation(location);
    	}  	
    	 
    	//Mono<Attendance> b = attendanceRepository.save(attendance);
    	//Attendance b = this.genericINterface.saveName(attendance);
  //  	System.out.println("b :"+b.toString());   	
        //return b;
    	this.genericINterface.saveName(attendance);
    }
    public Location createLocation(Location location, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Location a = this.locationRepository.save(location);
   // 	System.out.println("a :"+a.toString());
        return a;
    }
    
    public Attendance getAttendanceById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, Attendance.class);
    	
    }
	
    public Attendance updateAttendance(String id, Attendance attendance) {
    	String schemaName=attendance.getSchemaName();
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	attendance.setId(id);
    	return this.attendanceRepository.save(attendance);
       
    }
    
    public Attendance updateAttendancePunchOut(String id, Attendance attendanceforPunchOut, String schemaName) {
    	System.out.println("%%%%%%%%%%%%%%"+attendanceforPunchOut.toString());
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	
    	Attendance attendanceMono=this.genericINterface.findById(id, Attendance.class);   
    	Location a = null;
    	Location location = null;
    	Attendance attendance = null;
  		if (attendanceMono != null) {
  			attendance = attendanceMono;
  		}
  		a = createLocation(attendanceforPunchOut.getPunchOutLocation(), schemaName);
		location = a;
		attendance.setPunchOutLocation(location);
		attendance.setPunchOutDateTimeStamp(attendanceforPunchOut.getPunchOutDateTimeStamp());
		attendance.setWorkingDuration(attendanceforPunchOut.getWorkingDuration());
  		//return attendanceRepository.save(attendance).map(updatelogoff-> new ResponseEntity<>(updatelogoff, HttpStatus.OK));
		return this.attendanceRepository.save(attendance);
    }
    
    public Attendance updateAttendanceSpecificLogOff(String id, List<LogOffTime> logOffTime, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	
    	Attendance attendanceMono=this.genericINterface.findById(id, Attendance.class);   	
    	Attendance attendance = null;
  		if (attendanceMono != null) {
  			attendance = attendanceMono;
  		}
  		if (attendance.getLogOffTime() == null) {
  			attendance.setLogOffTime(logOffTime);
  		} else {
  			List<LogOffTime> l = attendance.getLogOffTime();
  			l.addAll(logOffTime);
  			attendance.setLogOffTime(l);
  			//attendance.setLogOffTime(l);
  		} 				
  		//return attendanceRepository.save(attendance).map(updatelogoff-> new ResponseEntity<>(updatelogoff, HttpStatus.OK));
  		return this.attendanceRepository.save(attendance);
    }

   
  //hard delete
  	public Map<String, String> deleteAttendance(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.attendanceRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Attendance deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftAttendance(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		Attendance attendance = this.genericINterface.findById(id, Attendance.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		attendance.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		attendance.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		attendance.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(attendance);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Attendance deleted successfully");
  		return response;

  	}
    
    public List<Attendance> streamAllAttendances() {
        return attendanceRepository.findAll();
    }

	public List<Attendance> getAllAttendances11(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    return attendanceRepository.findAll();
	    

	}

	public Integer getAllAttendancesCount(String schemaName) {
		List<Attendance> list = null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		list =  attendanceRepository.findAll();
		return list.size();
		
	}

	

	public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancebasedOnDate(String schemaName, String userId, String startdate,
			String enddate) {
		List<com.fidz.entr.app.reportattendance.Attendance> list = null;
		

			 Constant.LOGGER.info("Attendance"); 
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			 Query query=new Query();
	    	
	      
	    	 try { 
	    		// String date_string = "2019-10-17";  // the date format
	    		 DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	    		 Date startDate = format.parse(startdate);
	    		 Date endDate = format.parse(enddate);
	    		 
	    		 // Query query1 = new Query().addCriteria(Criteria.where("date_time").gte(startDate).lte(endDate).and("user_id").is(user));
	    		 query.addCriteria(Criteria.where("punchInDateTimeStamp").gte(startDate).lte(endDate).and("user.id").is(userId));
	    		 // query1.with(new Sort(Sort.Direction.DESC,"date_time"));
	    		 Constant.LOGGER.info("Query"+query);
	    		 list=this.mongoTemplate.find(query, com.fidz.entr.app.reportattendance.Attendance.class);  
	    	 }catch(Exception ex) {
	    		 Constant.LOGGER.error("Error occurred"+ex);
	   		}
	         return list;
	
	
		
	}


// TO get attendance list based on pagination and dates
	public List<com.fidz.entr.app.reportattendance.Attendance> getAllAttendancesPaginatedWeb(String startdate,
			String enddate, int pageNumber, int pageSize, String schemaName) throws ParseException {
		
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("inside getAllAttendancesPaginatedWeb AttendanceService.java ");
    	//String date_string = "2015-04-17 11:02:49";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date startDate = format.parse(startdate);
        Date endDate = format.parse(enddate);
        //end date as plus 1
        LocalDateTime localDateTime = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime=localDateTime.plusDays(1);
        Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        
    	final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	Query query = new Query();
    	query.addCriteria(Criteria.where("plannedStartDateTime").gte(startDate).lte(currentDatePlusOneDay));
    	query.with(pageableRequest);
    	return this.genericINterface.find(query, com.fidz.entr.app.reportattendance.Attendance.class);
	}
	
//	public List<Attendance> getAlllWebAttendances(String schemaName) {
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		Query query = new Query();
//
//		query.fields().include("createdTimestamp");
//		query.fields().include("createdByUser");
//		query.fields().include("updatedTimestamp");
//		query.fields().include("updatedByUser");
//		query.fields().include("deletedTimestamp");
//		query.fields().include("deletedByUser");
//		query.fields().include("status");
//		query.fields().include("schemaName");
//		query.fields().include("user").include("name").include("firstName");
//		query.fields().include("calenderDatestamp");
//		query.fields().include("punchInDateTimeStamp");
//		query.fields().include("punchInLocation");
//		query.fields().include("punchOutDateTimeStamp");
//		query.fields().include("punchOutLocation");
//		query.fields().include("workingDuration");
//		query.fields().include("logOffTime");
//		query.fields().include("idleTime");
//		
//		
//		return this.genericINterface.find(query,Attendance.class);
//	}
	
	

//	public List<ReportAttendance> getAlllWebAttendances(String schemaName) {
//		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//		
//		List<ReportAttendance> webAttendances = null;
//		
//		List<Attendance> userattendances = this.genericINterface.findAll(Attendance.class);
//		System.out.print(userattendances.get(0).toString());
//		
//		//Constant.LOGGER.debug(" Inside AttendanceService.java Value for insert attendance record: createattendance  :: " + attendance);
//		
//		
//		for(int i = 0; i < userattendances.size();i++ ) {
//			PersonName  personName = new PersonName();
//			
//			personName.setFirstName(userattendances.get(i).getUser().getName().getFirstName());
//			personName.setLastName(userattendances.get(i).getUser().getName().getLastName());
//			personName.setMiddleName(userattendances.get(i).getUser().getName().getMiddleName());
//			
//			Contact contact = new Contact();
//					
//					contact.setPrimary(userattendances.get(i).getUser().getContact().getPhoneNo().getPrimary());
//					contact.setEmail(userattendances.get(i).getUser().getContact().getEmail());
//			
//					
//			Device deviceatten = new Device();
//			deviceatten.setId(userattendances.get(i).getUser().getDevice().getId());
//			deviceatten.setDeviceId(userattendances.get(i).getUser().getDevice().getDeviceId());
//				
//			
//			
//			Country country =  new Country();
//			country.setId(userattendances.get(i).getUser().getCountry().getId());
//			country.setName(userattendances.get(i).getUser().getCountry().getName());
//					
//			
//			User userdata = new User();
//			userdata.setId(userattendances.get(i).getUser().getId());
//			userdata.setUserName(userattendances.get(i).getUser().getUserName());
//			userdata.setName(personName);
//			userdata.setContact(contact);
//			userdata.setDevice(deviceatten);
//			userdata.setCountry(country);
//			
//			ReportAttendance reportAttendance = new ReportAttendance();
//			//LocationAttendance locationAttendance = new LocationAttendance(); 
//			
//			
////					(userattendances.getCreatedTimestamp(), 
////					userattendances.getCreatedByUser(), userattendances.getUpdatedTimestamp(), 
////					userattendances.getUpdatedByUser(), userattendances.getDeletedTimestamp(), 
////					userattendances.getDeletedByUser(), userattendances.getStatus(), 
////					userattendances.getSchemaName(), userattendances.getId(), 
////					userdata, 
////					userattendances.getCalenderDatestamp(), 
////					userattendances.getPunchInDateTimeStamp(), 
////					userattendances.getPunchInLocation(), 
////					userattendances.getPunchOutDateTimeStamp(), 
////					userattendances.getPunchOutLocation(), 
////					userattendances.getWorkingDuration(), 
////					userattendances.getLogOffTime(), 
////					userattendances.getIdleTime());
//					
//					reportAttendance.setCreatedTimestamp(userattendances.get(i).getCreatedTimestamp()); 
//					reportAttendance.setCreatedByUser(userattendances.get(i).getCreatedByUser());	
//					reportAttendance.setUpdatedTimestamp(userattendances.get(i).getUpdatedTimestamp());
//					reportAttendance.setUpdatedByUser(userattendances.get(i).getUpdatedByUser());
//					reportAttendance.setDeletedTimestamp(userattendances.get(i).getDeletedTimestamp());
//					reportAttendance.setDeletedByUser(userattendances.get(i).getDeletedByUser());
//					reportAttendance.setStatus(userattendances.get(i).getStatus());
//					reportAttendance.setSchemaName(userattendances.get(i).getSchemaName());
//					reportAttendance.setId(userattendances.get(i).getId());
//					reportAttendance.setUser(userdata);
//					reportAttendance.setCalenderDatestamp(userattendances.get(i).getCalenderDatestamp());
//					reportAttendance.setPunchInDateTimeStamp(userattendances.get(i).getPunchInDateTimeStamp());
//					//reportAttendance.setPunchInLocationWeb(userattendances.getPunchInLocation());
//					reportAttendance.setPunchOutDateTimeStamp(userattendances.get(i).getPunchOutDateTimeStamp());
//					//reportAttendance.setPunchOutLocationWeb(userattendances.getPunchOutLocation());
//					reportAttendance.setWorkingDuration(userattendances.get(i).getWorkingDuration());
//					//reportAttendance.setLogOffTime(userattendances.getLogOffTime());
//					//reportAttendance.setIdleTime(userattendances.getIdleTime());
//
//			webAttendances.add(reportAttendance);
//
//		}
//		
//	//	attendances.forEach(userattendances-> {
//
//	//		if (userattendances.getId() != null) {
//
////				Thread geoThread = new Thread() {
////					public void run() {
////						
////						
////			
////
////					}
////				};
////
////				geoThread.start();
///*				PersonName  personName = new PersonName(userattendances.getUser().getName().getFirstName(),
//						userattendances.getUser().getName().getMiddleName(), 
//						userattendances.getUser().getName().getLastName());
//				Contact contact = new Contact(userattendances.getUser().getContact().getPhoneNo().getPrimary(),
//						userattendances.getUser().getContact().getEmail());
//				Device deviceatten = new Device(userattendances.getUser().getDevice().getId(),
//						userattendances.getUser().getDevice().getDeviceId());
//				
//				
//				Country country =  new Country(userattendances.getUser().getCountry().getId(), 
//						userattendances.getUser().getCountry().getName());
//				
//				User userdata = new User();
//				userdata.setId(userattendances.getUser().getId());
//				userdata.setUserName(userattendances.getUser().getUserName());
//				userdata.setName(personName);
//				userdata.setContact(contact);
//				userdata.setDevice(deviceatten);
//				userdata.setCountry(country);
//				
//				ReportAttendance reportAttendance = new ReportAttendance();
//				//LocationAttendance locationAttendance = new LocationAttendance(); 
//				
//				
////						(userattendances.getCreatedTimestamp(), 
////						userattendances.getCreatedByUser(), userattendances.getUpdatedTimestamp(), 
////						userattendances.getUpdatedByUser(), userattendances.getDeletedTimestamp(), 
////						userattendances.getDeletedByUser(), userattendances.getStatus(), 
////						userattendances.getSchemaName(), userattendances.getId(), 
////						userdata, 
////						userattendances.getCalenderDatestamp(), 
////						userattendances.getPunchInDateTimeStamp(), 
////						userattendances.getPunchInLocation(), 
////						userattendances.getPunchOutDateTimeStamp(), 
////						userattendances.getPunchOutLocation(), 
////						userattendances.getWorkingDuration(), 
////						userattendances.getLogOffTime(), 
////						userattendances.getIdleTime());
//						
//						reportAttendance.setCreatedTimestamp(userattendances.getCreatedTimestamp()); 
//						reportAttendance.setCreatedByUser(userattendances.getCreatedByUser());	
//						reportAttendance.setUpdatedTimestamp(userattendances.getUpdatedTimestamp());
//						reportAttendance.setUpdatedByUser(userattendances.getUpdatedByUser());
//						reportAttendance.setDeletedTimestamp(userattendances.getDeletedTimestamp());
//						reportAttendance.setDeletedByUser(userattendances.getDeletedByUser());
//						reportAttendance.setStatus(userattendances.getStatus());
//						reportAttendance.setSchemaName(userattendances.getSchemaName());
//						reportAttendance.setId(userattendances.getId());
//						reportAttendance.setUser(userdata);
//						reportAttendance.setCalenderDatestamp(userattendances.getCalenderDatestamp());
//						reportAttendance.setPunchInDateTimeStamp(userattendances.getPunchInDateTimeStamp());
//						//reportAttendance.setPunchInLocationWeb(userattendances.getPunchInLocation());
//						reportAttendance.setPunchOutDateTimeStamp(userattendances.getPunchOutDateTimeStamp());
//						//reportAttendance.setPunchOutLocationWeb(userattendances.getPunchOutLocation());
//						reportAttendance.setWorkingDuration(userattendances.getWorkingDuration());
//						//reportAttendance.setLogOffTime(userattendances.getLogOffTime());
//						//reportAttendance.setIdleTime(userattendances.getIdleTime());
//
//				webAttendances.add(reportAttendance);
//
//			} else {
//				Constant.LOGGER.error("User Device not found");
//			}*/
//
//		//});
//
//    	return webAttendances;
//	}
	 

}
