package com.fidz.entr.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.Customer;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.repository.DepartmentRepository;
import com.fidz.entr.base.model.Department;



@Service("/FEADepartmentService")
public class DepartmentService {
	private GenericAppINterface<Department> genericINterface;
	@Autowired
	public DepartmentService(GenericAppINterface<Department> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private DepartmentRepository departmentRepository;
	
	
    public List<Department> getAllDepartments(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllDepartments DepartmentService.java");
    	List<Department> departments=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	departments=this.genericINterface.findAll(Department.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllDepartments DepartmentService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllDepartments DepartmentService.java");
    	return departments;
    }
	
    public Department createDepartment(Department department) {
    	Department departments=null;
    	try {
    		Constant.LOGGER.debug(" Inside DepartmentService.java Value for insert Department record: createDepartment  :: " + department);
    		String schemaName=department.getSchemaName();
    	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	    departments=this.departmentRepository.save(department);
    	}catch(Exception e) {
    		Constant.LOGGER.debug(" Inside DepartmentService.java Value for insert Department record: createDepartment  :: " + department);
    	}
       return departments;
    }
    
    public void createDepartments(Department department) {
        String schemaName=department.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        this.genericINterface.saveName(department);
    }
    
    public Department getDepartmentById(String id, String schemaName) {
    	 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	 return this.genericINterface.findById(id, Department.class);
		
    }
	
    public Department getDepartmentByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.departmentRepository.findByName(name);
		
    }
	
    public Department updateDepartment(String id, Department department) {
        String schemaName=department.getSchemaName();
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    String departId=department.getId();
//		Stream<User> notifiedUsers=getAllUsersByDepartId(departId,schemaName);
//	    List<Notification> nds=new ArrayList<Notification>() ;
//	    notifiedUsers.forEach(user -> {
//			if (user.getNotificationId()!=null) {
//				Notification notification = new Notification();
//				notification.setMessage("Department Record Updated");
//				notification.setUniqueNotificationId(user.getNotificationId());
//				notification.setOsType("AndroidOS");
//				nds.add(notification);
//			}
//		  });
//	    System.out.println("nds details"+nds);
//    	NotificationForDeviceService.getInstance().sendNotification(nds);
	    department.setId(id);
	    return this.departmentRepository.save(department);
    }

    public Stream<User> getAllUsersByDepartId(String departId, String schemaName) {
 		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
 		
 		//List<User> users=(List<User>) this.userRepository.findAll().stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
 		Stream<User> users=this.genericINterface.findAll(User.class).stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
 		return users;
 		/*return this.genericINterface.findAll(Activity.class).filter(activity ->activity.getResource().getId().contains(userId) && activity.getActivityStatus().equals(ActivityStatus.valueOf("OPEN"))
 				&&  activity.getActivityConfigurationType().equals(ActivityConfigurationType.valueOf("PLANNED")));*/

 	}
  //hard delete
  	public Map<String, String> deleteDepartment(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.departmentRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Department deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftDepartment(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		Department department = this.genericINterface.findById(id, Department.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		department.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		department.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		department.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(department);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "Department deleted successfully");
  		return response;

  	}
    public List<Department> streamAllDepartments() {
        return departmentRepository.findAll();
    }
//TO get all department paginated
	public List<Department> getAllDepartmentsPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllDepartmentsPaginated DepartmentService.java");
    	Query query = new Query();
    	List<Department> departments =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	departments= this.genericINterface.find(query, Department.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllDepartmentsPaginated DeviceDetailsService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllDepartmentsPaginated "+query);
        return departments;
	}
}
