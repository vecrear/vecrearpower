package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.UserNotFoundException;
import com.fidz.entr.app.model.CustomerTypeField;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.service.UserService;
import com.fidz.services.authentication.model.Login;


@CrossOrigin(maxAge = 3600)
@RestController("FEAUserController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/users")
public class UserController {

	@Autowired
	private UserService  userService;
	
	@Value("${userservices.enabled}")
	private Boolean property;
	
	@PostMapping("/get/all")
	//@Cacheable(value = "users")
	public List<User> getAllUsers(@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return userService.getAllUsers(schemaName);
	}
	
	@PostMapping("/get/all/web")
	//@Cacheable(value = "users")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersWeb(@RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return userService.getAllUsersWeb(schemaName);
	}
	
	//Post request to get all Users web  paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<com.fidz.entr.app.reportusers.User> getAllUsersWebPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return userService.getAllUsersWebPaginated(pageNumber, pageSize, schemaName);
	}
	
	
	@PostMapping
	public User createUser(@Valid @RequestBody User user){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		System.out.println("user creation has been started------------------");
        return userService.createUser(user);
	}
	
	@PostMapping("/creates")
	public void createUsers(@Valid @RequestBody User user){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		userService.createUsers(user);
	}
	
	@PostMapping("/get/id/{id}")
	public User getUserById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return userService.getUserById(id, schemaName);
		
	}
	
	@PostMapping("/update/id/{id}")
    public User updateUser(@PathVariable(value = "id") String id,  @Valid @RequestBody User user) {
		System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return userService.updateUser(id, user);
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteUser(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return userService.deleteUser(id, schemaName);
    }
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteUser(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return userService.deleteSoftUser(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<User> streamAllUsers() {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return userService.streamAllUsers();
    }
    @PatchMapping("/update/id/{id}/patch")
    public User updateUserPatch(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody String schemaName) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return userService.updateUserPatch(id, schemaName);
    }
    @PostMapping("/get/password/decrypt/password")
	public String getDeycryptPassword(@Valid @RequestBody Login login, @RequestHeader(value="schemaName") String schemaName){
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return userService.getDeycryptPassword(login, schemaName);
		
	}
    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A User with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(UserNotFoundException ex) {
    	System.out.println("property value******"+property);
		String message="Userservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
    	return ResponseEntity.notFound().build();
    }
	
}
