package com.fidz.entr.base.service;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.repository.GenericBaseINterface;
import com.fidz.base.repository.UserRepository;
import com.fidz.base.validator.Constant;
import com.fidz.entr.base.model.Company;
import com.fidz.entr.base.model.Device;
import com.fidz.entr.base.model.Facility;
import com.fidz.entr.base.model.User;
import com.fidz.entr.base.model.UserRole;
import com.fidz.services.authentication.model.Login;
import com.fidz.services.authentication.security.EncriptionUtil;


import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service("FEBaseUserService")
public class UserService {
	private GenericAppINterface<User> genericINterface;
	@Autowired
	public UserService(GenericAppINterface<User> genericINterface){
		this.genericINterface=genericINterface;
	}
	@Autowired
	private UserRepository  userRepository;
	/*@Autowired
	private SuperAdminRepository  superAdminRepository;
	*/
	public List<User> getAllUsers(String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("Inside getAllUsers UserService.java");
    	List<User> users=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	users=this.genericINterface.findAll(User.class);
    	}catch(Exception ex) {
    	Constant.LOGGER.error("Error while getting data from getAllUsers UserService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllUsers UserService.java");
    	return users;
		
	}
	
	public User createUser(User user){
		System.out.println("inside ");
		User users=null;
		try {
			Constant.LOGGER.debug(" Inside UserService.java Value for insert User record: createUser  :: " + user);
			String schemaName=user.getSchemaName();
			 MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(user.getContact().getPhoneNo().getPrimary());
	    	String encreptPwdAuth = new EncriptionUtil().encrypt(user.getPassword(), securityKey);
	    	user.setPassword(encreptPwdAuth);
			//return userRepository.save(user);
	    	users=this.userRepository.save(user);
			
		}catch(Exception e) {
			Constant.LOGGER.debug(" Inside UserService.java Value for insert User record: createUser  :: " + user);
		}
		
		return users;
	}
	
	
	public void createUsers(User user){
		String schemaName = user.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(user.getContact().getPhoneNo().getPrimary());
		String encreptPwdAuth = new EncriptionUtil().encrypt(user.getPassword(), securityKey);
		user.setPassword(encreptPwdAuth);
		
		this.genericINterface.saveName(user);
	}
	public User getUserById(String id, String schemaName){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.genericINterface.findById(id, User.class);
		
	}
	
    public User updateUser(String id, User user) {
//    	String schemaName=user.getSchemaName();
//    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
//    	List<Notification> nds=new ArrayList<Notification>() ;
//    	if (user.getNotificationId()!=null) {
//			Notification notification = new Notification();
//			notification.setMessage("User Record Updated");
//			notification.setUniqueNotificationId(user.getNotificationId());
//			notification.setOsType("AndroidOS");
//			nds.add(notification);
//		}
//    	NotificationForDeviceService.getInstance().sendNotification(nds);
//    	user.setId(id);
//		SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(user.getContact().getPhoneNo().getPrimary());
//		String encreptPwdAuth = new EncriptionUtil().encrypt(user.getPassword(), securityKey);
//		user.setPassword(encreptPwdAuth);
 	return this.userRepository.save(user);
        
    }

   
  //hard delete
  	public Map<String, String> deleteUser(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.userRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "User deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftUser(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		User user = this.genericINterface.findById(id, User.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		user.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		user.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		user.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(user);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "User deleted successfully");
  		return response;

  	}
   
	 public User updateUserPatch(String id,String schemaName) {
	     	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     	User userMono=this.genericINterface.findById(id, User.class);
	     	User user = null;
	   		if (userMono != null) {
	   			user = userMono;
	   		}
	   		user.setSchemaName(schemaName);
	   		
	      return this.userRepository.save(user);
	 }
	     
	     public String getDeycryptPassword(Login login, String schemaName){
	     	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	     	SecretKeySpec securityKey = new EncriptionUtil().getSecurityKey(login.getUserName());
	     	String decryptPwdAuth = new EncriptionUtil().decrypt(login.getPassword(), securityKey);
	     	return decryptPwdAuth;
	 		
	 	}

}
