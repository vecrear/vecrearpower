package com.fidz.entr.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.configs.FCMServic;
import com.fidz.entr.app.exception.ActivityTypeNotFoundException;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityStatusChangeLog;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.model.PushNotificationRequest;
import com.fidz.entr.app.model.User;
import com.fidz.entr.app.model.UserActivityNotify;
import com.fidz.entr.app.model.UserGeoStatus;
import com.fidz.entr.app.repository.ActivityTypeRepository;
import com.fidz.entr.app.repository.UserRepository;




@Service("FEAActivityTypeService")
public class ActivityTypeService {
	private GenericAppINterface<ActivityType> genericINterface;
	@Autowired
	public ActivityTypeService(GenericAppINterface<ActivityType> genericINterface){
		this.genericINterface=genericINterface;
	}
	@Autowired
	private ActivityTypeRepository  activityTypeRepository;
	@Autowired 
	UserRepository userRepository;
	
	 @Autowired
	 private  FCMServic fcmServic;
	
	// get All Activity Type(Post Request)
	public List<ActivityType> getAllActivityType(String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		Constant.LOGGER.info("Inside getAllActivityType ActivityStatusChangeLog.java");
    	List<ActivityType> activityTypes=null;
    	try {
//    	Query q = new Query();
//    	q.addCriteria(Criteria.where("status").is("ACTIVE"));
    	activityTypes=this.genericINterface.findAll(ActivityType.class);
    	}catch(Exception ex) {
    		Constant.LOGGER.error("Error while getting data from getAllActivityType ActivityTypeService.java"+ex.getMessage());
        }
    	Constant.LOGGER.info("**************************************************************************");
    	Constant.LOGGER.info("successfully fetched data from getAllActivityType ActivityTypeService.java");
    	return activityTypes;
	}
			
	public ActivityType createActivityType(ActivityType activityType) {
		ActivityType activityTypes=null;
		try {
			Constant.LOGGER.debug(" Inside ActivityTypeService.java Value for insert ActivityType record:createActivityType :: " + activityType);
			String schemaName = activityType.getSchemaName();
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			activityTypes=this.activityTypeRepository.save(activityType);
			
			if(activityTypes != null) {
				Constant.LOGGER.error("user.getNotificationId()----"+activityTypes.toString());
			    String departId=activityType.getDepartment().getId();
				Stream<User> notifiedUsers = getAllUsersByDepartId(departId,schemaName);
				Constant.LOGGER.error("notifiedUsers----"+notifiedUsers.toString());
				List<UserActivityNotify> notifyActivityTypes=new ArrayList<UserActivityNotify>() ;
				UserActivityNotify userActivityNoty =  new UserActivityNotify();
				 PushNotificationRequest ppp = PushNotificationRequest.getInstance(); 
				 notifiedUsers.forEach(user -> {
						
						if(user.getNotificationId() !=null) { 
							Constant.LOGGER.error("user.getNotificationId()----"+user.getNotificationId());
							userActivityNoty.setNotificationId(user.getNotificationId());
							userActivityNoty.setUserActivityStatus("New Activity has been mapped to you");	
							Constant.LOGGER.error("userActivityNoty----"+userActivityNoty.toString());
	  	        		    ppp.setMessage(userActivityNoty.getUserActivityStatus());
	  	         		  	ppp.setTitle("VcGeo-Track");
	  	         		  	ppp.setToken(userActivityNoty.getNotificationId());
	  	        		  	ppp.setTopic("");
	  	        		  Constant.LOGGER.error("user.getNotificationId()----"+ppp.toString());
	  	        		  Thread geoThread = new Thread() {
	  	    			    public void run() {
	  	    			    	
	  	    			    	   try {
	  	    			    			
	  	    			    		 fcmServic.sendMessageToToken(ppp);
	  	    						Thread.sleep(3);
	  	    					} catch (InterruptedException e) {
	  	    						e.printStackTrace();
	  	    					} catch (ExecutionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}	
	  	    			    }  
	  	    			};

	  	    			geoThread.start();
	  	    			
	  	         		}else {
	  	         			Constant.LOGGER.error("User Device not found");
	  	         		}
					
					
				  });
				  

			}
			
		}catch(Exception e) {
	
		}
		return activityTypes;
		
	}
	
	public void createActivityTypes(ActivityType activityType) {
		String schemaName = activityType.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		// return activityTypeRepository.save(activityType);
		this.genericINterface.saveName(activityType);
	}
		
		/*//get All Activity Type by id(Post Request)
		public ActivityType getActivityTypeById(String id,String schemaName){
			MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
			return this.genericINterface.findById(id, ActivityType.class);
			
		}*/
	    //get All Activity Type by id(Post Request)
	public ResponseEntity<ActivityType> getActivityTypeById(String id, String schemaName) {
		ActivityType activityType = null;
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		activityType = this.genericINterface.findById(id, ActivityType.class);
		if (activityType == null) {
			throw new ActivityTypeNotFoundException(id);
		} else {
			return ResponseEntity.ok(activityType);
		}

	}
			
		
	    	
		//get All Activity Type by name(Post Request)
	public ActivityType getActivityTypeByName(String name, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		return this.activityTypeRepository.findByName(name);
	}
	
		/*public Flux<ActivityType> getAllActivityType1(){
			return activityTypeRepository.findAll();
		}*/
		
	/*public Mono<ActivityType> getActivityTypeById1(String id) {
		return activityTypeRepository.findById(id).switchIfEmpty(Mono.error(new ActivityTypeNotFoundException(id)));

	}*/
	
   /* public Mono<ActivityType> getActivityTypeByName1(String name) {
		return activityTypeRepository.findByName(name).switchIfEmpty(Mono.error(
				new ActivityTypeNotFoundException(name)));
    }*/
	   
    public ActivityType updateActivityType(String id, ActivityType activityType) {
    	String schemaName = activityType.getSchemaName();
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   // String departId=activityType.getDepartment().getId();
		//Stream<User> notifiedUsers=getAllUsersByDepartId(departId,schemaName);
		//Constant.LOGGER.error("activityType----"+activityType.toString());
		if(activityType != null) {
			Constant.LOGGER.error("user.getNotificationId()----"+activityType.toString());
		    String departId=activityType.getDepartment().getId();
			Stream<User> notifiedUsers = getAllUsersByDepartId(departId,schemaName);
			Constant.LOGGER.error("notifiedUsers----"+notifiedUsers.toString());
			List<UserActivityNotify> notifyActivityTypes=new ArrayList<UserActivityNotify>() ;
			UserActivityNotify userActivityNoty =  new UserActivityNotify();
			 PushNotificationRequest ppp = PushNotificationRequest.getInstance(); 
			 notifiedUsers.forEach(user -> {
					
					if(user.getNotificationId() !=null) { 
						Constant.LOGGER.error("user.getNotificationId()----"+user.getNotificationId());
						userActivityNoty.setNotificationId(user.getNotificationId());
						userActivityNoty.setUserActivityStatus("New Activity details has been mapped to you");	
						Constant.LOGGER.error("userActivityNoty----"+userActivityNoty.toString());
  	        		    ppp.setMessage(userActivityNoty.getUserActivityStatus());
  	         		  	ppp.setTitle("VcGeo-Track");
  	         		  	ppp.setToken(userActivityNoty.getNotificationId());
  	        		  	ppp.setTopic("");
  	        		  Constant.LOGGER.error("user.getNotificationId()----"+ppp.toString());
  	        		  Thread geoThread1 = new Thread() {
  	    			    public void run() {
  	    			    	
  	    			    	   try {
  	    			    			
  	    			    		 fcmServic.sendMessageToToken(ppp);
  	    						Thread.sleep(3);
  	    					} catch (InterruptedException e) {
  	    						e.printStackTrace();
  	    					} catch (ExecutionException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}	
  	    			    }  
  	    			};

  	    			geoThread1.start();
  	    			
  	         		}else {
  	         			Constant.LOGGER.error("User Device not found");
  	         		}
				
				
			  });
			  

		}
		  

	
//		List<Notification> nds=new ArrayList<Notification>() ;
//		notifiedUsers.forEach(user -> {
//			if (user.getNotificationId()!=null) {
//				Notification notification = new Notification();
//				notification.setMessage("Activity Record Updated");
//				notification.setUniqueNotificationId(user.getNotificationId());
//				notification.setOsType("AndroidOS");
//				nds.add(notification);
//			}
//		  });
//		  
//		
//		System.out.println("nds details"+nds);
//		NotificationForDeviceService.getInstance().sendNotification(nds);
		
		activityType.setId(id);
		return this.activityTypeRepository.save(activityType);
       
    }
   
    public Stream<User> getAllUsersByDepartId(String departId, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		
		//List<User> users=(List<User>) this.userRepository.findAll().stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
		Stream<User> users=this.genericINterface.findAll(User.class).stream().filter(userlist -> userlist.getDepartment().getId().equals(departId));
		return users;
		/*return this.genericINterface.findAll(Activity.class).filter(activity ->activity.getResource().getId().contains(userId) && activity.getActivityStatus().equals(ActivityStatus.valueOf("OPEN"))
				&&  activity.getActivityConfigurationType().equals(ActivityConfigurationType.valueOf("PLANNED")));*/

	}
    
    
    //hard delete
  	public Map<String, String> deleteActivityType(String id, String schemaName) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.activityTypeRepository.deleteById(id);
  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "ActivityType deleted successfully");
  		return response;

  	}
       //soft delete
  	public Map<String, String> deleteSoftActivityType(String id, String schemaName, TimeUpdate timeUpdate) {
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		ActivityType activityType = this.genericINterface.findById(id, ActivityType.class);
  		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
  		activityType.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
  		activityType.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
  		activityType.setStatus(Status.INACTIVE);
  		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
  		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
  		this.genericINterface.saveName(activityType);

  		Map<String, String> response = new HashMap<String, String>();
  		response.put("message", "ActivityType deleted successfully");
  		return response;

  	}
    public List<ActivityType> streamAllActivityTypes() {
        return activityTypeRepository.findAll();
    }

  //To get all ActivityTypes paginated
    //page number starts from zero
	public List<ActivityType> getAllActivityTypesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllActivityTypesPaginated ActivityTypeService.java");
    	Query query = new Query();
    	List<ActivityType> activityTypes =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	activityTypes= this.genericINterface.find(query, ActivityType.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllActivityTypesPaginated ActivityTypeService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllActivityTypesPaginated "+query);
        return activityTypes;	
	
	}

}
