package com.fidz.entr.app.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.payload.ErrorResponse;
import com.fidz.entr.app.exception.ActivityTypeNotFoundException;
import com.fidz.entr.app.model.ActivityField;
import com.fidz.entr.app.model.ActivityType;
import com.fidz.entr.app.service.ActivityTypeService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(maxAge = 3600)
@RestController("FEAActivityTypeController")
@Configuration
@PropertySource(value="classpath:vecrearconfig.properties",ignoreResourceNotFound=true)
@RequestMapping(value="/activitytypes")
public class ActivityTypeController {

	@Autowired
	private ActivityTypeService  activityTypeService;
	
	@Value("${activityservices.enabled}")
	private Boolean property;
	
	//get All Activity Type(Post Request)
	@PostMapping("/get/all")
	public List<ActivityType> getAllActivityType(@RequestHeader(value = "schemaName") String schemaName) {

		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return activityTypeService.getAllActivityType(schemaName);
	}
	
	//Post request to get all ActivityTypes paginated
	@PostMapping("get/all/web/{pageNumber}/{pageSize}")
	public List<ActivityType> getAllActivityTypesPaginated(@PathVariable(value="pageNumber") int pageNumber,@PathVariable(value="pageSize") int pageSize, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return activityTypeService.getAllActivityTypesPaginated(pageNumber, pageSize, schemaName);
	}
	
	@PostMapping
	public ActivityType createActivityType(@Valid @RequestBody ActivityType activityType){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return activityTypeService.createActivityType(activityType);
	}
	
	@PostMapping("/creates")
	public void createActivityTypes(@Valid @RequestBody ActivityType activityType){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		activityTypeService.createActivityTypes(activityType);
	}
	//get All Activity Type by id(Post Request)
	@PostMapping("/get/id/{id}")
	public ResponseEntity<ActivityType> getActivityTypeById(@PathVariable(value="id") String id, @RequestHeader(value="schemaName") String schemaName){
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return activityTypeService.getActivityTypeById(id,schemaName);
		
	}
	//get All Activity Type by name(Post Request)
	@PostMapping("/get/name/activitytypename/{name}")
    public ActivityType getActivityTypeByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		System.out.println("property value******"+property);
		String message="Activityservices are disabled";
		if(!property) 
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message);
		return activityTypeService.getActivityTypeByName(name,schemaName);
    }
	
	
	
	/*@GetMapping
	public Flux<ActivityType> getAllActivityType1(){
		
		return activityTypeService.getAllActivityType1();
	}*/
	
	
	/*@GetMapping("/{id}")
	public Mono<ActivityType> getActivityTypeById1(@PathVariable(value="id") String id){
		return activityTypeService.getActivityTypeById1(id);
		
	}*/
	
	/*@GetMapping("/activitytypename/{name}")
    public Mono<ActivityType> getActivityTypeByName1(@PathVariable(value = "name") String name) {
		return activityTypeService.getActivityTypeByName1(name);
    }*/
	
	@PostMapping("/update/id/{id}")
    public ActivityType updateActivityType(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody ActivityType activityType) {
        return activityTypeService.updateActivityType(id, activityType);
    }

    
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteActivityType(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return activityTypeService.deleteActivityType(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteActivityType(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return activityTypeService.deleteSoftActivityType(id, schemaName, timeUpdate);
    }
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<ActivityType> streamAllActivityTypes() {
        return activityTypeService.streamAllActivityTypes();
    }

    // Exception Handling Examples
 /*   @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A ActivityType with the same details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(ActivityTypeNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }*/
}
