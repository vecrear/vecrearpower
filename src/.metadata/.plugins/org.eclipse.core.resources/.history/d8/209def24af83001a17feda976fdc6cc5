package com.fidz.entr.app.reportactivities;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fidz.base.model.Status;
import lombok.Data;

@Data
@JsonIgnoreProperties(value = { "target" })
public class Company {
	
	@JsonIgnore
	@NotNull
	@CreatedDate
	//protected long createdTimestamp;
	protected Date createdTimestamp;
	@JsonIgnore
	@NotBlank
	@CreatedBy
	protected String createdByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long updatedTimestamp;
	protected Date updatedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String updatedByUser;
	@JsonIgnore
	@LastModifiedDate
	//protected long deletedTimestamp;
	protected Date deletedTimestamp;
	@JsonIgnore
	@LastModifiedBy
	protected String deletedByUser;
	@JsonIgnore
	@NotNull
	protected Status status;
	@JsonIgnore
	@NotNull
	protected String schemaName;

	@Id
	protected String id;
	
	@NotNull
	@Indexed(unique = true)
	protected String name;
	
	//Company Type: https://en.wikipedia.org/wiki/List_of_legal_entity_types_by_country
	@NotNull
	protected String type;
	
	protected String description;
	@JsonIgnore
	//International Securities Identification Number (ISIN) 
	protected String isin;
	@Override
	public String toString() {
		return "Company [createdTimestamp=" + createdTimestamp + ", createdByUser=" + createdByUser
				+ ", updatedTimestamp=" + updatedTimestamp + ", updatedByUser=" + updatedByUser + ", deletedTimestamp="
				+ deletedTimestamp + ", deletedByUser=" + deletedByUser + ", status=" + status + ", schemaName="
				+ schemaName + ", id=" + id + ", name=" + name + ", type=" + type + ", description=" + description
				+ ", isin=" + isin + "]";
	}
	public Company(@NotNull Date createdTimestamp, @NotBlank String createdByUser, Date updatedTimestamp,
			String updatedByUser, Date deletedTimestamp, String deletedByUser, @NotNull Status status,
			@NotNull String schemaName, String id, @NotNull String name, @NotNull String type, String description,
			String isin) {
		super();
		this.createdTimestamp = createdTimestamp;
		this.createdByUser = createdByUser;
		this.updatedTimestamp = updatedTimestamp;
		this.updatedByUser = updatedByUser;
		this.deletedTimestamp = deletedTimestamp;
		this.deletedByUser = deletedByUser;
		this.status = status;
		this.schemaName = schemaName;
		this.id = id;
		this.name = name;
		this.type = type;
		this.description = description;
		this.isin = isin;
	}
	


	
}
