package com.fidz.entr.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fidz.base.config.GenericAppINterface;
import com.fidz.base.config.MultiTenantMongoDbFactory;
import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.model.Status;
import com.fidz.base.validator.Constant;
import com.fidz.entr.app.model.DeviceDetails;
import com.fidz.entr.app.model.UserDepartmentConfig;
import com.fidz.entr.app.model.UserRole;
import com.fidz.entr.app.repository.UserRoleRepository;


@Service("FEBUserRoleService")
public class UserRoleService {
	private GenericAppINterface<UserRole> genericINterface;
	@Autowired
	public UserRoleService(GenericAppINterface<UserRole> genericINterface){
		this.genericINterface=genericINterface;
	}
          
	@Autowired
	private UserRoleRepository userRoleRepository;
	
    public List<UserRole> getAllUserRoles(String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	   	Constant.LOGGER.info("Inside getAllUserRoles UserRoleService.java");
	   	List<UserRole> userRoles=null;
	   	try {
	   	userRoles=this.genericINterface.findAll(UserRole.class);
	   	}catch(Exception ex) {
	   	Constant.LOGGER.error("Error while getting data from getAllUserRoles UserRoleService.java"+ex.getMessage());
	       }
	   	Constant.LOGGER.info("**************************************************************************");
	   	Constant.LOGGER.info("successfully fetched data from getAllUserRoles UserRoleService.java");
	   	return userRoles;
    }
	

    public UserRole createUserRole(UserRole userRole) {
    	UserRole userRoles=null;
    	try {
    		Constant.LOGGER.debug(" Inside UserRoleService.java Value for insert UserRole record: createUserRole  :: " + userRole);
    		String schemaName=userRole.getSchemaName();
        	
    	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
            //return userRoleRepository.save(userRole);
    	    userRoles=this.userRoleRepository.save(userRole);
    	}catch(Exception e) {
    		Constant.LOGGER.debug(" Inside UserRoleService.java Value for insert UserRole record: createUserRole  :: " + userRole);
    	}
        return userRoles;
    }
    
    public void createUserRoles(UserRole userRole) {
        String schemaName=userRole.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
        //return userRoleRepository.save(userRole);
	    this.genericINterface.saveName(userRole);
    }
    public UserRole getUserRoleById(String id, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.genericINterface.findById(id, UserRole.class);
		
    }
	
    public UserRole getUserRoleByName(String name, String schemaName) {
    	MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	return this.userRoleRepository.findByName(name);
		
    }
	
    public UserRole updateUserRole(String id, UserRole userRole) {
        String schemaName=userRole.getSchemaName();
    	
	    MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
	    userRole.setId(id);
	    return this.userRoleRepository.save(userRole);
       
    }

   
  
    public Map<String, String> deleteUserRole(String id, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.userRoleRepository.deleteById(id);
		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserRole deleted successfully");
		return response;

	}
     //soft delete
	public Map<String, String> deleteSoftUserRole(String id, String schemaName, TimeUpdate timeUpdate){
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		UserRole userRole = this.genericINterface.findById(id, UserRole.class);
		// System.out.println("timeUpdate.getDeletedByFidzUser()dattttt"+timeUpdate.getDeletedByFidzUser());
		userRole.setDeletedByUser(timeUpdate.getDeletedByFidzUser());
		userRole.setDeletedTimestamp(timeUpdate.getDeletedFidzTimestamp());
		userRole.setStatus(Status.INACTIVE);
		// System.out.println("timeUpdate.getDeletedFidzTimestamp()6666"+timeUpdate.getDeletedFidzTimestamp());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
		this.genericINterface.saveName(userRole);

		Map<String, String> response = new HashMap<String, String>();
		response.put("message", "UserRole deleted successfully");
		return response;

	}
    public List<UserRole> streamAllUserRoles() {
        return userRoleRepository.findAll();
    }

//TO get all user roles paginated
	public List<UserRole> getAllUserRolesPaginated(int pageNumber, int pageSize, String schemaName) {
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread(schemaName);
    	Constant.LOGGER.info("inside getAllUserRolesPaginated UserRoleService.java");
    	Query query = new Query();
    	List<UserRole> userRoleServices =null;

    	try {
	    final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);
    	query.with(pageableRequest);
    	userRoleServices= this.genericINterface.find(query, UserRole.class);	
    	

    	}catch(Exception ex) {
	    Constant.LOGGER.error("Error occurred inside getAllUserRolesPaginated UserRoleService.java "+ex.getMessage());
        }
    	Constant.LOGGER.info("After success getAllUserRolesPaginated "+query);
        return userRoleServices;
	}

}
