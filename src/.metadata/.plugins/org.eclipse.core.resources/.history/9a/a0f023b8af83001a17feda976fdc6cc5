package com.fidz.base.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fidz.base.controller.TimeUpdate;
import com.fidz.base.exception.StreamNotFoundException;
import com.fidz.base.model.Role;
import com.fidz.base.model.Stream;
import com.fidz.base.payload.ErrorResponse;

import fidz.base.service.StreamService;

@CrossOrigin(maxAge = 3600)
@RestController("FBaseStreamController")
@RequestMapping(value="/base/streams")
public class StreamController {
	@Autowired
	private StreamService streamService;
	
	@PostMapping("/get/all")
    public List<Stream> getAllStreams(@RequestHeader(value="schemaName") String schemaName) {
    	return streamService.getAllStreams(schemaName);
    }
	
	
    @PostMapping
    public Stream createCompany(@Valid @RequestBody Stream stream) {
        return streamService.createStream(stream);
    }
    
    @PostMapping("/creates")
    public void createCompanys(@Valid @RequestBody Stream stream) {
        streamService.createStreams(stream);
    }
    
    @PostMapping("/get/id/{id}")
    public Stream getCompanyById(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
		return streamService.getStreamById(id, schemaName);
    }
	
	@PostMapping("/get/name/streamname/{name}")
    public Stream getStreamByName(@PathVariable(value = "name") String name, @RequestHeader(value="schemaName") String schemaName) {
		return streamService.getStreamByName(name, schemaName);
    }
	
	@PostMapping("/update/id/{id}")
    public Stream updateStream(@PathVariable(value = "id") String id,
                                                   @Valid @RequestBody Stream stream) {
        return streamService.updateStream(id, stream);
    }
	
    @PostMapping("/delete/id/{id}")
    public Map<String, String> deleteStream(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName) {
        return streamService.deleteStream(id, schemaName);
    }
    
    @PostMapping("/delete/soft/id/{id}")
    public Map<String, String> softDeleteStream(@PathVariable(value = "id") String id, @RequestHeader(value="schemaName") String schemaName, @Valid @RequestBody TimeUpdate timeUpdate) {
        return streamService.deleteSoftStream(id, schemaName, timeUpdate);
    }
   
    
    @PostMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public List<Stream> streamAllStreams() {
        return streamService.streamAllStreams();
    }


    // Exception Handling Examples
    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDuplicateKeyException(DuplicateKeyException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorResponse("A Stream Topic Name details already exists"));
    }

    @ExceptionHandler
    public ResponseEntity<?> handleNotFoundException(StreamNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }
	
}
