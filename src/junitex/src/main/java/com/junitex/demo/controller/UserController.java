package com.junitex.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.junitex.demo.model.User;
import com.junitex.demo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {
   @Autowired
	UserService userservice;
	
   @PostMapping("/get/all")
	public List<User> getall(){
		return this.userservice.getall();
   }
   
   @PostMapping("/create")
	public User create(@Valid @RequestBody   User user) {
	return this.userservice.create(user);
	}
   
   @PostMapping("getbyaddress/{address}")
   public List<User> getbyAddress(@PathVariable(value = "address") String address) {
	   return this.userservice.findbyAddress(address);
   }
   
	
}
