package com.junitex.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.junitex.demo.model.User;
import com.junitex.demo.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public List<User> getall() {
     List<User> user =userRepository.findAll();
     System.out.println("Getting result from DB:"+user);
     return user;
	}
	
	
	public User create(User user) {
	 User user1 = userRepository.save(user);
	 return user1;
	}
	
	public List<User> findbyAddress(String address){
	List<User> user1=userRepository.findByAddress(address);
	return user1;	
	}
	
	

}
