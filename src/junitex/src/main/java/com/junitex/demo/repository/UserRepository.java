package com.junitex.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.junitex.demo.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String>{

	List<User> findByAddress(String address);

}
