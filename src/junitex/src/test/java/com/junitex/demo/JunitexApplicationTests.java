package com.junitex.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.junitex.demo.model.User;
import com.junitex.demo.repository.UserRepository;
import com.junitex.demo.service.UserService;

@SpringBootTest
class JunitexApplicationTests {
    @Autowired
	UserService userservice;
	
    @MockBean
    UserRepository userrepository;
    
	@Test
	public void getallusertest() {
		when(userrepository.findAll()).thenReturn(Stream
				.of(new User("1", "Ashok", "Bangalore", "9902159601"),new User("2", "Srinivas", "Telangana", "6300645425")).collect(Collectors.toList()));
		assertEquals(2, userservice.getall().size());
	}

	@Test
	public void getuserbyaddress() {
		String address="Bangalore";
		when(userrepository.findByAddress(address)).thenReturn(Stream
				.of(new User("3", "Sundar", "TamilNadu", "9956867754")).collect(Collectors.toList()));
		assertEquals(1, userservice.findbyAddress(address).size());
	}
	@Test
	public void saveuser() {
		User user = new User("4", "Nandan", "Bangalore", "56498746878");
		when(userrepository.save(user)).thenReturn(user);
		assertEquals(user, userservice.create(user));
		
	}
	
	
}
