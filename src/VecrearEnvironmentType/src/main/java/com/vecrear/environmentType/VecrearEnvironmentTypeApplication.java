package com.vecrear.environmentType;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@Configuration
@EnableAutoConfiguration
@EnableReactiveMongoRepositories
@ComponentScan(  basePackages = {"com.vecrear.environmentType","com.fidz.base",})
@EnableMongoRepositories
@SpringBootApplication
public class VecrearEnvironmentTypeApplication {

	public static void main(String[] args) {
		
		
		SpringApplication.run(VecrearEnvironmentTypeApplication.class, args);
	}

}
