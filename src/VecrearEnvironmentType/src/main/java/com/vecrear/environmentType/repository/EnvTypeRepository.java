package com.vecrear.environmentType.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.vecrear.environmentType.model.EnvType;
import com.vecrear.environmentType.model.Typeinfo;
@Repository("VEnvTypeRepository")
public interface EnvTypeRepository extends MongoRepository<EnvType, String> {
	

	public List<EnvType> findByenvtype(Typeinfo type);

}
