package com.vecrear.environmentType.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "vecrear_environment_type")
public class EnvType {
    
	private Typeinfo envtype;
	
	private String mqtt1;
	
	private String dbmongo;
	
	private String mqtt2;
	
	private String minireport;
	
	private String enterprise;
	
	private String geofence;
	
	private String powerservice;
	
	private String platform;
	
	private String locationservice;
	
	private String topicname;
	
	private String dbname;
	
	private String version;
	
	private String versioncode;

	public EnvType(Typeinfo envtype, String mqtt1, String dbmongo, String mqtt2, String minireport, String enterprise,
			String geofence, String powerservice, String platform, String locationservice, String topicname,
			String dbname, String version, String versioncode) {
		super();
		this.envtype = envtype;
		this.mqtt1 = mqtt1;
		this.dbmongo = dbmongo;
		this.mqtt2 = mqtt2;
		this.minireport = minireport;
		this.enterprise = enterprise;
		this.geofence = geofence;
		this.powerservice = powerservice;
		this.platform = platform;
		this.locationservice = locationservice;
		this.topicname = topicname;
		this.dbname = dbname;
		this.version = version;
		this.versioncode = versioncode;
	}

	@Override
	public String toString() {
		return "EnvType [envtype=" + envtype + ", mqtt1=" + mqtt1 + ", dbmongo=" + dbmongo + ", mqtt2=" + mqtt2
				+ ", minireport=" + minireport + ", enterprise=" + enterprise + ", geofence=" + geofence
				+ ", powerservice=" + powerservice + ", platform=" + platform + ", locationservice=" + locationservice
				+ ", topicname=" + topicname + ", dbname=" + dbname + ", version=" + version + ", versioncode="
				+ versioncode + "]";
	}


	
}
