package com.vecrear.environmentType.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.vecrear.environmentType.model.EnvType;
import com.vecrear.environmentType.model.Typeinfo;
import com.vecrear.environmentType.service.EnvTypeService;

@CrossOrigin(maxAge = 3600)
@RestController("VEnvtypeController")
@RequestMapping(value="/envtype")
public class EnvTypeController {
	
	@Autowired
	EnvTypeService envTypeService;
	
	@PostMapping("get/all")
	public List<EnvType> getallEnvTypes(@RequestHeader(value = "schemaName") String schemaName){
		return envTypeService.getallEnvTypes(schemaName);
	}
	
	@PostMapping
	public EnvType createEnvType(@Valid @RequestBody EnvType info,@RequestHeader(value="schemaName") String schemaName) {
		return envTypeService.createEnvType(info,schemaName);
	}
	
	@PostMapping("get/{type}")
	public List<EnvType> getbyEnvtype(@PathVariable(value="type") Typeinfo type,@RequestHeader(value="schemaName") String schemaName){
		return envTypeService.getbyEnvtype(type,schemaName);
	}
	
	@PostMapping("/update/{type}")
    public EnvType updatebyEnvtype(@PathVariable(value = "type") Typeinfo type, @RequestHeader(value="schemaName") String schemaName,@Valid @RequestBody EnvType envtype) {
        return envTypeService.updatebyEnvtype(type, schemaName,envtype);
    }


}
