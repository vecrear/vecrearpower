#!/bin/bash
totalcount=100
for (( c=1; c<=$totalcount; c++ ))
do
   echo "\n *** Running performance test $c/$totalcount ..."
   curl -i -X POST -w "@curl-format.txt" -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/allusers.dat http://13.234.130.40:8088/users/get/all/web 2>&1 >> data/perf.allusers.dat &
   curl -i -X POST -w "@curl-format.txt" -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/allactivities.dat http://13.234.130.40:8088/activities/get/all/web  2>&1 >> data/perf.allactivities.dat &
   curl -i -X POST -w "@curl-format.txt" -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/allattendance.dat http://13.234.130.40:8088/attendance/get/all/web  2>&1 >> data/perf.allattendance.dat &
   curl -i -X POST -w "@curl-format.txt" -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/allcustomer.dat http://13.234.130.40:8088/customers/get/all/web  2>&1 >> data/perf.allcustomers.dat &
   curl -i -X POST -w "@curl-format.txt" -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/allgpsdata.dat http://13.234.130.40:8088/gpsdatas/get/all  2>&1 >> data/perf.allgpsdata.dat &
   #curl -i -X POST -w "@curl-format.txt" -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/activitycountperday.dat http://13.234.130.40:8088/filter/activity/map/id/5e2a8e3bf264de0001703400/type/5e2a8a9ff264de00017033fc/sdate=2020-02-01|edate=2020-02-25  2>&1 >> data/perf.activitycountperday.dat &

 #Activity compare ratios
 curl -X POST   -w "@curl-format.txt"  -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/activitycompareratio.dat   --data '{ "userId":"5e2a8e3bf264de0001703400", "activityId":"5e2ec7ab7a0be0582cd10aca", "startDate":"2019-01-01", "endDate":"2020-12-31" }' http://13.234.130.40:8088/filter/activity/compare/ratio 2>&1 >> data/perf.activitycompareratio.dat &

 #Comparar - non ratio version
 curl -X POST   -w "@curl-format.txt"  -H  "Content-Type: application/json" -H "schemaName:vecrearTrack"  -o data/activitycomparenonratio.dat    --data '{ "userId":"5e2a8e3bf264de0001703400", "activityId":"5e2a8a9ff264de00017033fc","customerId":["5e293af620b5430001108208"], "startDate":"2020-01-01", "endDate":"2020-12-31"}' http://13.234.130.40:8088/filter/activity/compare/ 2>&1 >> data/perf.activitycomparenonratio.dat &


done
echo "--Done--"




